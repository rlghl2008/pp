#pragma once
#include "ObjectBase.h"
#include "Character.h"

class Hatchling : public ObjectBase
{
public:
	Hatchling(void);
	~Hatchling(void);

private:
	estring m_HatchlingName;
	estring m_BulletName;
	EPN_Pos m_DirectionPos;

	bool m_SettingFlag;

	short m_ShootDelayCnt;
	short m_ShootDelay;

	short m_AtkPower;
public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};
