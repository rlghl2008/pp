#include "ViewUpgrade.h"

ViewUpgrade :: ViewUpgrade(void) 
	: m_BaseFlag(false), m_ChangeFlag(false), m_ChangePosX(0), m_FailFlag(false)
{	
	m_YesNoCheck.setScaling(EPN_Scaling(0.9375, 1.5));
	m_YesNoCheck.setPos(EPN_Pos(0, 400));

	//캐릭터 총알 강화 버튼
	m_Button[BUTTON_CHARACTER_BULLET].setTexture(L"btn_medium");
	m_Button[BUTTON_CHARACTER_BULLET].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_CHARACTER_BULLET].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_CHARACTER_BULLET].setPos(EPN_Pos(80, 80));
	m_Button[BUTTON_CHARACTER_BULLET].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_CHARACTER_BULLET].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_CHARACTER_BULLET].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_CHARACTER_BULLET].setFont(NULL);
	m_Button[BUTTON_CHARACTER_BULLET].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_CHARACTER_BULLET].setSound("./Data/sound/ui_bar.wav", UI_STATE_POP);

	//하이퍼 플라이트 강화 버튼
	m_Button[BUTTON_HIPERFLIGHT].setTexture(L"btn_medium");
	m_Button[BUTTON_HIPERFLIGHT].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HIPERFLIGHT].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HIPERFLIGHT].setPos(EPN_Pos(310, 80));
	m_Button[BUTTON_HIPERFLIGHT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_HIPERFLIGHT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_HIPERFLIGHT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_HIPERFLIGHT].setFont(NULL);
	m_Button[BUTTON_HIPERFLIGHT].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_HIPERFLIGHT].setSound("./Data/sound/ui_bar.wav", UI_STATE_POP);

	//자석 강화 버튼
	m_Button[BUTTON_MAGNET].setTexture(L"btn_medium");
	m_Button[BUTTON_MAGNET].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_MAGNET].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_MAGNET].setPos(EPN_Pos(80, 180));
	m_Button[BUTTON_MAGNET].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_MAGNET].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_MAGNET].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_MAGNET].setFont(NULL);
	m_Button[BUTTON_MAGNET].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_MAGNET].setSound("./Data/sound/ui_bar.wav", UI_STATE_POP);

	//파워샷 강화 버튼
	m_Button[BUTTON_POWERSHOT].setTexture(L"btn_medium");
	m_Button[BUTTON_POWERSHOT].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_POWERSHOT].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_POWERSHOT].setPos(EPN_Pos(310, 180));
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_POWERSHOT].setFont(NULL);
	m_Button[BUTTON_POWERSHOT].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_POWERSHOT].setSound("./Data/sound/ui_bar.wav", UI_STATE_POP);

	//더블샷 강화 버튼
	m_Button[BUTTON_DOUBLESHOT].setTexture(L"btn_medium");
	m_Button[BUTTON_DOUBLESHOT].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_DOUBLESHOT].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_DOUBLESHOT].setPos(EPN_Pos(80, 280));
	m_Button[BUTTON_DOUBLESHOT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_DOUBLESHOT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_DOUBLESHOT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_DOUBLESHOT].setFont(NULL);
	m_Button[BUTTON_DOUBLESHOT].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_DOUBLESHOT].setSound("./Data/sound/ui_bar.wav", UI_STATE_POP);

	//돌아가기 버튼
	m_Button[BUTTON_BACK].setTexture(L"back");
	m_Button[BUTTON_BACK].setPos(EPN_Pos(10, 50));
	m_Button[BUTTON_BACK].setRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setTextureRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 200, 255, 200), UI_STATE_ACTIVE);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 100, 100, 100), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setFont(NULL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setScaling(EPN_Scaling(0.5, 0.5));
	m_Button[BUTTON_BACK].setSound("button_click", UI_STATE_POP);
}

ViewUpgrade :: ~ViewUpgrade(void)
{
	m_pPlayerData->SavePlayerData();
}

void ViewUpgrade :: DrawUpgrade()
{
	EPN_Rect Upgrade_on = pEPN_TI->getTextureSize(L"upgradecount_on");
	EPN_Rect Upgrade = pEPN_TI->getTextureSize(L"upgradecount");
	EPN_Rect MoneyBase = pEPN_TI->getTextureSize(L"money_base_02");

	EPN_Pos Pos = EPN_Pos(168, 108);
	EPN_Pos PlusPos;

	//X축에서 연속으로 2번 그리고 난 후 Y축을 더하기 하기위한 변수
	short Cnt = 0;

	for(int i=0; i<5; i++)
	{
		//업그레이드 카운트(점)
		for(int j=0; j<m_pPlayerData->getUpgradeCnt(i); j++)
		{
			pEPN_TI->Blt(L"upgradecount_on", (Pos - EPN_Pos(5.5, 5.5)) + PlusPos, Upgrade_on, NULL, 0, EPN_Scaling(0.6, 0.6));
			PlusPos.X += 15;
		}
		for(int j=m_pPlayerData->getUpgradeCnt(i); j<UPGRADE_MAX_SIZE; j++)
		{
			pEPN_TI->Blt(L"upgradecount", Pos + PlusPos, Upgrade, NULL, 0, EPN_Scaling(0.8, 0.8));
			PlusPos.X += 15;
		}

		//업그레이드 비용
		pEPN_TI->Blt(L"money_base_02", EPN_Pos(-2, 125) + PlusPos, MoneyBase, EPN_ARGB(255, 82, 105, 119), 0, EPN_Scaling(1.1, 0.8));
		pEPN_TI->Blt(L"money_base_02", EPN_Pos(68, 125) + PlusPos, MoneyBase, EPN_ARGB(255, 82, 105, 119), 0, EPN_Scaling(1.1, 0.8));
		pEPN_TI->Blt(L"coin", EPN_Pos(80, 128) + PlusPos, pEPN_TI->getTextureSize(L"coin"), NULL, 0, EPN_Scaling(0.8, 0.8));

		if(m_pPlayerData->getUpgradeCnt(i) < 8)
			pEPN_FI->BltText(L"바른돋움20", E_TEXT((2000 + (2000 * m_pPlayerData->getUpgradeCnt(i)))), EPN_Rect(EPN_Pos(115, 131) + PlusPos), EPN_ARGB(255, 255, 255, 255));
		else
			pEPN_FI->BltText(L"바른돋움20", "MAX", EPN_Rect(EPN_Pos(115, 131) + PlusPos), EPN_ARGB(255, 255, 255, 255));


		Cnt++;
		PlusPos.X += 110;

		if(Cnt == 2)
		{
			PlusPos.Y += 100;
			PlusPos.X = 0;
			Cnt = 0;
		}
	}
}

void ViewUpgrade :: DrawUpgradeUI(short UpgradeType)
{	
	//글자를 중간에 오도록 하기위한 변수
	estring Text, NextText;
	float TextStartPosX, NextTextStartPosX;


	//업그레이드가 풀일 때
	if(m_pPlayerData->getUpgradeCnt(m_UpgradeType) == UPGRADE_FULL)
	{
		Text = L"모든 강화가 끝난 항목입니다.";
		TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X / 2;
		pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 485), EPN_Pos(TextStartPosX + 300, 505)), EPN_ARGB(255, 255, 255, 255));
		return;
	}

	//코인이 없을때
	else if(UpgradeType == UPGRADE_FAIL)
	{
		Text = L"코인이 부족합니다.";
		TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X / 2;
		pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 485), EPN_Pos(TextStartPosX + 300, 505)), EPN_ARGB(255, 255, 255, 255));
		return;
	}

	//업그레이드 다음 단계 화살표
	pEPN_TI->Blt(L"back", EPN_Pos(260 + m_ChangePosX, 460), pEPN_TI->getTextureSize(L"back"), NULL, 0, EPN_Scaling(0.6, 0.6), true);
	//화살표 움직임
	if(!m_ChangeFlag)
	{
		if(m_ChangePosX > 10)
			m_ChangePosX += 1.5;
		else
			m_ChangePosX += 1.2;

		if(m_ChangePosX > 23)
			m_ChangeFlag = true;
	}
	else
	{
		if(m_ChangePosX > 1)
			m_ChangePosX -= 1.8;
		else
			m_ChangePosX -= 0.1;

		if(m_ChangePosX < 0)
			m_ChangeFlag = false;
	}


	//점선
	pEPN_TI->Blt(L"dot", EPN_Pos(20, 565), pEPN_TI->getTextureSize(L"dot"), EPN_ARGB(255, 143, 129, 68), 0, EPN_Scaling(0.95, 1));
	

	//업그레이드 비용
	pEPN_TI->Blt(L"coin", EPN_Pos(282, 522), pEPN_TI->getTextureSize(L"coin"), NULL, 0, EPN_Scaling(0.9, 0.9));
	pEPN_FI->BltText(L"바른돋움20", L"비용", EPN_Rect(EPN_Pos(237, 525), EPN_Pos(280, 545)), EPN_ARGB(255, 255, 255, 255));
	pEPN_FI->BltText(L"바른돋움25", E_TEXT((2000 + (2000 * m_pPlayerData->getUpgradeCnt(UpgradeType)))), EPN_Rect(EPN_Pos(317, 523), EPN_Pos(380, 547)), EPN_ARGB(255, 255, 255, 255));

	switch(UpgradeType)
	{
	case CHARACTER_BULLET:
		{
			//업그레이드 설명
			Text = L"캐릭터 공격력이 강해집니다.";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움20", Text).X / 2;
			pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(TextStartPosX, 435), EPN_Pos(TextStartPosX + 200, 455)), EPN_ARGB(255, 255, 255, 255));

			//업그레이드 효과
			Text = E_TEXT(m_pPlayerData->getUpgradeCnt(UpgradeType)) + L"단계";
			NextText = E_TEXT((1 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"단계";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X * 2;
			NextTextStartPosX = 300 + pEPN_FI->getETextAreaSize(L"바른돋움30", NextText).X;
			pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 475), EPN_Pos(TextStartPosX + 300, 495)), EPN_ARGB(255, 255, 255, 255));
			pEPN_FI->BltText(L"바른돋움30", NextText, EPN_Rect(EPN_Pos(NextTextStartPosX, 475), EPN_Pos(NextTextStartPosX + 300, 495)), EPN_ARGB(255, 211, 253, 109));
			break;
		}


	case HIPERFLIGHT:
		{
			//업그레이드 설명
			Text = L"하이퍼 플라이트 질주시간이 길어집니다.";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움20", Text).X / 2;
			pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(TextStartPosX, 435), EPN_Pos(TextStartPosX + 200, 455)), EPN_ARGB(255, 255, 255, 255));

			//업그레이드 효과
			Text = E_TEXT(1 + "." + m_pPlayerData->getUpgradeCnt(UpgradeType)) + L"초";
			NextText = E_TEXT(1 + "." + (1 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"초";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X * 2;
			NextTextStartPosX = 300 + pEPN_FI->getETextAreaSize(L"바른돋움30", NextText).X;
			pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 475), EPN_Pos(TextStartPosX + 300, 495)), EPN_ARGB(255, 255, 255, 255));
			pEPN_FI->BltText(L"바른돋움30", NextText, EPN_Rect(EPN_Pos(NextTextStartPosX, 475), EPN_Pos(NextTextStartPosX + 300, 495)), EPN_ARGB(255, 211, 253, 109));
			break;
		}


	case MAGNET:
		{	
			//업그레이드 설명
			Text = L"자석 유지 시간이 길어집니다.";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움20", Text).X / 2;
			pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(TextStartPosX, 435), EPN_Pos(TextStartPosX + 200, 455)), EPN_ARGB(255, 255, 255, 255));

			//업그레이드 효과
			Text = E_TEXT((10 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"초";
			NextText = E_TEXT((11 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"초";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X * 2;
			NextTextStartPosX = 300 + pEPN_FI->getETextAreaSize(L"바른돋움30", NextText).X;
			pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 475), EPN_Pos(TextStartPosX + 300, 495)), EPN_ARGB(255, 255, 255, 255));
			pEPN_FI->BltText(L"바른돋움30", NextText, EPN_Rect(EPN_Pos(NextTextStartPosX, 475), EPN_Pos(NextTextStartPosX + 300, 495)), EPN_ARGB(255, 211, 253, 109));
			break;
		}


	case POWERSHOT:
		{
			//업그레이드 설명
			Text = L"파워샷 유지 시간이 길어집니다.";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움20", Text).X / 2;
			pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(TextStartPosX, 435), EPN_Pos(TextStartPosX + 200, 455)), EPN_ARGB(255, 255, 255, 255));


			//소수점 자르기용 임시 변수
			estring TempText;

			//자르기
			TempText = E_TEXT(((float)1.5 + (float)(0.2 * m_pPlayerData->getUpgradeCnt(UpgradeType))));
			for(int i = 0; i < TempText.length(); ++i)
			{
				if( TempText.getMid(i, i + 1) == L"." )
				{
					Text = TempText.getMid(0, i+2) + L"초";
				}
			}

			//자르기
			TempText = E_TEXT(((float)1.7 + (float)(0.2 * m_pPlayerData->getUpgradeCnt(UpgradeType))));
			for(int i = 0; i < TempText.length(); ++i)
			{
				if( TempText.getMid(i, i + 1) == L"." )
				{
					NextText = TempText.getMid(0, i+2) + L"초";
				}
			}

			//업그레이드 효과
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X * 2;
			NextTextStartPosX = 300 + pEPN_FI->getETextAreaSize(L"바른돋움30", NextText).X;
			pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 475), EPN_Pos(TextStartPosX + 300, 495)), EPN_ARGB(255, 255, 255, 255));
			pEPN_FI->BltText(L"바른돋움30", NextText, EPN_Rect(EPN_Pos(NextTextStartPosX, 475), EPN_Pos(NextTextStartPosX + 300, 495)), EPN_ARGB(255, 211, 253, 109));
			break;
		}


	case DOUBLESHOT:
		{
			//업그레이드 설명
			Text = L"듀얼샷의 지속 시간이 길어집니다.";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움20", Text).X / 2;
			pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(TextStartPosX, 435), EPN_Pos(TextStartPosX + 200, 455)), EPN_ARGB(255, 255, 255, 255));

			//업그레이드 효과
			Text = E_TEXT((10 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"초";
			NextText = E_TEXT((11 + m_pPlayerData->getUpgradeCnt(UpgradeType))) + L"초";
			TextStartPosX = 300 - pEPN_FI->getETextAreaSize(L"바른돋움30", Text).X * 2;
			NextTextStartPosX = 300 + pEPN_FI->getETextAreaSize(L"바른돋움30", NextText).X;
			pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(TextStartPosX, 475), EPN_Pos(TextStartPosX + 300, 495)), EPN_ARGB(255, 255, 255, 255));
			pEPN_FI->BltText(L"바른돋움30", NextText, EPN_Rect(EPN_Pos(NextTextStartPosX, 475), EPN_Pos(NextTextStartPosX + 300, 495)), EPN_ARGB(255, 211, 253, 109));
			break;
		}
	}
}

VIEWSTATE ViewUpgrade :: Run()
{
	//배경
	Background :: CreateInstance()->Run();

	//플레이어 머니
	m_pPlayerData->PlayerMoneyRun();

	//업데이트 UI
	DrawUpgrade();

	CHECK_TYPE Check;
	//가격
	int price = 2000 + (2000 * m_pPlayerData->getUpgradeCnt(m_UpgradeType));


	//체크 UI
	if(m_BaseFlag)
	{
		//업그레이드가 MAX일 때
		if(m_pPlayerData->getUpgradeCnt(m_UpgradeType) == UPGRADE_FULL)
			m_FailFlag = true;

		if(!m_FailFlag)
		{
			Check = m_YesNoCheck.Run(YESNO);
			DrawUpgradeUI(m_UpgradeType);

			if(Check == YES)
			{
				//소지한 돈이 충분할 때
				if(m_pPlayerData->getMoney() >= price)
				{
					m_pPlayerData->UpgradeIncrease(m_UpgradeType);
					m_pPlayerData->MinusMoney(price);
					m_BaseFlag = false;
					pSoundManager->Play("buy_success");
				}
				//소지한 돈이 부족할 때
				else
					m_FailFlag = true;
			}
			else if(Check == NO)
				m_BaseFlag = false;
			
		}
		else
		{
			Check = m_YesNoCheck.Run(YES);
			DrawUpgradeUI(UPGRADE_FAIL);

			if(Check == YES)
			{
				m_FailFlag = false;
				m_BaseFlag = false;
			}
		}
	}


	//캐릭터 총알 강화 버튼
	m_Button[BUTTON_CHARACTER_BULLET].Run();
	pEPN_TI->Blt(L"character_power", EPN_Pos(83, 83), EPN_Rect(pEPN_TI->getTextureSize(L"character_power")), NULL, 0, EPN_Scaling(0.6, 0.6));
	pEPN_FI->BltText(L"바른돋움20", "캐릭터 강화", EPN_Rect(EPN_Pos(165, 83)), EPN_ARGB(255, 255, 255, 255));

	//하이퍼 플라이트 강화 버튼
	m_Button[BUTTON_HIPERFLIGHT].Run();
	pEPN_TI->Blt(L"hiperflight", EPN_Pos(315, 83), EPN_Rect(pEPN_TI->getTextureSize(L"hiperflight")), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움20", "하이퍼 강화", EPN_Rect(EPN_Pos(395, 83)), EPN_ARGB(255, 255, 255, 255));

	//자석 강화 버튼
	m_Button[BUTTON_MAGNET].Run();
	pEPN_TI->Blt(L"magnet", EPN_Pos(85, 183), EPN_Rect(pEPN_TI->getTextureSize(L"magnet")), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움20", "자석 강화", EPN_Rect(EPN_Pos(165, 183)), EPN_ARGB(255, 255, 255, 255));

	//파워샷 강화 버튼
	m_Button[BUTTON_POWERSHOT].Run();
	pEPN_TI->Blt(L"powershot", EPN_Pos(315, 183), EPN_Rect(pEPN_TI->getTextureSize(L"powershot")), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움20", "파워샷 강화", EPN_Rect(EPN_Pos(395, 183)), EPN_ARGB(255, 255, 255, 255));

	//더블샷 강화 버튼
	m_Button[BUTTON_DOUBLESHOT].Run();
	pEPN_TI->Blt(L"doubleshot", EPN_Pos(85, 283), EPN_Rect(pEPN_TI->getTextureSize(L"doubleshot")), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움20", "더블샷 강화", EPN_Rect(EPN_Pos(165, 283)), EPN_ARGB(255, 255, 255, 255));

	//돌아가기 버튼
	m_Button[BUTTON_BACK].Run();

	//각 업그레이드 버튼 누를시
	for(short UpgradeType=0; UpgradeType<UPGRADE_MAX; UpgradeType++)
	{
		if(m_Button[UpgradeType].getState() == UI_STATE_POP)
		{
			m_BaseFlag = true;
			m_UpgradeType = UpgradeType;
		}
	}

	//돌아가기 버튼 누를시
	if(m_Button[BUTTON_BACK].getState() == UI_STATE_POP)
		return VIEW_MENU;
	return VIEW_UPGRADE;
}