#pragma once

#include <EPN_Enpine.h>
#include <string>
#include "SoundManager.h"
using namespace EPN;
using namespace EPN::BasicFunction;

typedef char UI_STATE;

enum
{
	UI_STATE_ALL = -1,
	UI_STATE_NORMAL = 0,
	UI_STATE_ACTIVE,
	UI_STATE_PUSH,
	UI_STATE_POP,
	UI_STATE_MAX
};

class Button
{
public:
	Button();
	~Button();

private:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;
	SoundManager * pSoundManager;

private:
	UI_STATE m_State; //UI 상태
	EPN_Pos m_Pos;
	EPN_Rect m_Rect; //UI 영역 Rect
	EPN_Scaling m_Scaling;
	EPN_Rect m_TextureRect[UI_STATE_MAX]; //4가지 상태 따른 텍스쳐 렉트
	EPN_ARGB m_Color[UI_STATE_MAX];
	short m_TextureIndex[UI_STATE_MAX]; //4가지 상태에 따른 텍스쳐 인덱스
	string m_SoundName[UI_STATE_MAX];

	//Font
	short m_FontIndex; //버튼에 사용할 폰트 인덱스
	estring m_EText;
	EPN_ARGB m_FontColor[UI_STATE_MAX];

	bool m_IsSoundPlayed;

private:
	EPN_Rect m_ETextBltRect;
	EPN_Pos m_ETextBltPos;
	EPN_Pos m_ButtonRealSize;
	EPN_Pos m_ETextAreaSize;
	EPN_Pos m_MousePos;

private:
	void playSound(UI_STATE State);

public:
	void setState(UI_STATE State);
	void setPos(EPN_Pos Pos);
	void setRect(EPN_Rect Rect);
	void setTextureRect(EPN_Rect TextureRect, UI_STATE State = UI_STATE_ALL);
	void setColor(EPN_ARGB Color, UI_STATE State);
	void setScaling(EPN_Scaling Scaling);
	void setTexture(short TextureIndex, UI_STATE State = UI_STATE_ALL);
	void setTexture(estring TextureName, UI_STATE State = UI_STATE_ALL);
	void setFont(short FontIndex);
	void setFont(estring FontName);
	void setFontColor(EPN_ARGB Color, UI_STATE State = UI_STATE_ALL);
	void setEText(estring Text);
	void setSound(string SoundName, UI_STATE State);

	UI_STATE getState();
	EPN_Pos getPos();
	EPN_Rect getRect();
	EPN_Scaling getScaling();
	EPN_Rect getTextureRect(UI_STATE State);
	EPN_ARGB getColor(UI_STATE State);
	short getTextureIndex(UI_STATE State);
	short getFontIndex();
	EPN_ARGB getFontColor(UI_STATE State);
	estring getEText();

public:
	bool Run();
};

