#pragma once
#include "ViewBase.h"
#include "SoundManager.h"

class ViewMenu : public ViewBase
{
public:
	ViewMenu(void);
	~ViewMenu(void);

private:
	enum BUTTON_TYPE
	{
		BUTTON_GAMESTART = 0,
		BUTTON_HATCHLING,
		BUTTON_UPGRADE,
		BUTTON_SHOP,
		BUTTON_MAX
	};

private:
	Button m_Button[BUTTON_MAX];
	short m_TextureIndex[5];
	
private:
	short m_Alpha;
	bool m_ChangeFlag; //���̵��� �ƿ�
	bool m_TexturePosFlag; //ĳ���� �սǵսǿ�
	float m_PosY[2];
	short m_FlashCnt; //���� �����ӿ�

private:
	void StartBGM();

public:
	virtual VIEWSTATE Run();
};