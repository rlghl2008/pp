#pragma once
#include "ViewBase.h"
#include "ObjectManager.h"
#include "Button.h"
#include "Meteo.h"

class ViewInGame : public ViewBase
{
public:
	ViewInGame(void);
	~ViewInGame(void);

private:
	enum BUTTON_TYPE
	{
		BUTTON_BACK,
		BUTTON_POWERSHOT,
		BUTTON_RESULT_OK,
		BUTTON_MAX
	};

	enum INGAME_TYPE
	{
		INGAME_PLAY,
		INGAME_RESULT
	};

private:
	Button m_Button[BUTTON_MAX];

	int m_GetMoneyValue;

	int m_DistanceScore;
	int m_HuntingScore;
	short m_DistanceScoreRect;
	short m_DistanceScoreRectStd;

	short m_DragonCreateCnt;
	short m_DragonCreateDelay;

	//메테오용 변수
	short m_MeteoCreateCnt;
	short m_MeteoCreateDelay;
	short m_DustCreateCnt;
	short m_DustStartCnt;
	bool m_DustCreateFlag;
	bool m_DustStartFlag;

	short m_DragonChangeCnt; //드래곤 종류 바뀌는 카운트
	short m_DragonTextureNumber; //드래곤 텍스쳐 숫자
	int m_MoneyBeforeGame; //게임시작 전 머니(원래 있던 돈)

	//페이드인 아웃용 변수
	bool m_FadeInOutFlag;
	bool m_ChangeFlag;
	short m_Alpha;

	INGAME_TYPE m_IngameType;

private:
	ObjectManager m_ObjectMgr;

public:
	void FadeInOut();

public:
	virtual VIEWSTATE Run();

};