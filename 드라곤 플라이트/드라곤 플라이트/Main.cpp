#include <EPN_FPS.h>
#include "ViewManager.h"
#include <EPN_MemoryCheck.h>
#include "PlayerData.h"
#include "Background.h"
#include "SoundManager.h"

#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")

void LoadTextureRes();

int main()
{	
	EPN_MEMORY_CHECK
	
	EPN_DeviceInterface * pEPN_DI = NULL;
	EPN_Init_Interface(&pEPN_DI);
	pEPN_DI->setInfoPrintFlag(true);

	EPN_TextureInterface * pEPN_TI = NULL;	
	EPN_Init_Interface(&pEPN_TI);
	pEPN_TI->setInfoPrintFlag(false);
	
	EPN_FontInterface * pEPN_FI = NULL;
	EPN_Init_Interface(&pEPN_FI);
	pEPN_FI->setInfoPrintFlag(false);
	
	EPN_InputEvent * pEvent = NULL;
	EPN_Init_Interface(&pEvent);
	pEvent->setInfoPrintFlag(false);

	EPN_ParticleInterface * pEPN_PI = NULL;
	EPN_Init_Interface(&pEPN_PI);
	pEPN_PI->setInfoPrintFlag(false);

	EPN_FPS FPS;
	FPS.setInfoPrintFlag(false);

	pEPN_DI->CreateDevice(L"MainDevice", DirectX9, L"드라곤 플라이트", false, EPN_Pos(600, 700), EPN_Pos(400, 20), pEvent);
	//600 700
	
	//텍스쳐 로드
	LoadTextureRes();

	PlayerData :: CreateInstance()->LoadPlayerData();
	
	//뷰 매니저
	ViewManager VManager;

	Background :: CreateInstance();
	VManager.ChangeView(VIEW_LOGO);

	SoundManager* pSoundManager = SoundManager::CreateInstance();
	while(pEPN_DI->Run())
	{
		if(FPS.FPSLocation(60) && pEPN_DI->DeviceValidity("MainDevice") == EPN_DEVICE_OK)
		{
			pEPN_DI->EventRun("MainDevice");
			pEPN_DI->BeginScene("MainDevice", EPN_ARGB(255, 255, 255, 255));
			
			pSoundManager->Run();
			if(!VManager.Run())
				break;

			//pEPN_FI->BltText(0, E_TEXT(L"FPS : ") + FPS.getFrame(), EPN_Rect(10, 10, 100, 100) , EPN_ARGB(255,255,255,255));
			pEPN_DI->EndScene("MainDevice");
		}
	}

	PlayerData :: CreateInstance()->SavePlayerData();
	EPN_Finish_Interface();
	return 0;
}

void LoadTextureRes()
{
	EPN_TextureInterface *pEPN_TI = NULL;
	EPN_Init_Interface(&pEPN_TI);

	EPN_FontInterface * pEPN_FI = NULL;
	EPN_Init_Interface(&pEPN_FI);

	SoundManager* pSoundManager = SoundManager::CreateInstance();

	//인게임 아이템(코인, 보석, 하이퍼플라이트, 더블샷, 자석)
	pEPN_TI->LoadTexture(L"item_coin", L"MainDevice", L"./Data/item/item_coin.png");
	pEPN_TI->LoadTexture(L"item_gem_01", L"MainDevice", L"./Data/item/item_gem.png");
	pEPN_TI->LoadTexture(L"item_gem_02", L"MainDevice", L"./Data/item/item_gem_02.png");
	pEPN_TI->LoadTexture(L"item_doubleshot", L"MainDevice", L"./Data/item/item_dualshot.png");
	pEPN_TI->LoadTexture(L"item_magnet", L"MainDevice", L"./Data/item/item_magnet.png");
	pEPN_TI->LoadTexture(L"item_hiperflight", L"MainDevice", L"./Data/item/item_hiperflight.png");

	//드래곤 사라질 시 먼지, 하이퍼 플라이트 파괴효과, 자석 효과
	pEPN_TI->LoadTexture(L"dust_white", L"MainDevice", L"./Data/effect/dust_white.png");
	pEPN_TI->LoadTexture(L"shock", L"MainDevice", L"./Data/effect/shock.png");
	pEPN_TI->LoadTexture(L"booster", L"MainDevice", L"./Data/effect/booster_run_01.png");
	pEPN_TI->LoadTexture(L"invincible_01", L"MainDevice", L"./Data/effect/invincible_01.png");
	pEPN_TI->LoadTexture(L"invincible_02", L"MainDevice", L"./Data/effect/invincible_02.png");
	pEPN_TI->LoadTexture(L"magnet_effect", L"MainDevice", L"./Data/effect/circle_yellow.png");

	//메테오
	pEPN_TI->LoadTexture(L"meteo", L"MainDevice", L"./Data/meteo/meteo.png");
	pEPN_TI->LoadTexture(L"meteo_piece", L"MainDevice", L"./Data/meteo/meteo_piece.png");
	pEPN_TI->LoadTexture(L"warn_line", L"MainDevice", L"./Data/meteo/warn_line.png");
	pEPN_TI->LoadTexture(L"warn_mark", L"MainDevice", L"./Data/meteo/warn_mark.png");
	pEPN_TI->LoadTexture(L"dust_dark", L"MainDevice", L"./Data/effect/dust_dark.png");

	EPN_Pos TexturePos[3];
	estring TextureName[3];

	TextureName[0] = L"booster";	
	TextureName[1] = L"magnet_effect";
	TextureName[2] = L"warn_line";

	TexturePos[0] = pEPN_TI->getTextureSize(L"booster").Bottom;
	TexturePos[1] = pEPN_TI->getTextureSize(L"magnet_effect").Bottom;
	TexturePos[2] = pEPN_TI->getTextureSize(L"warn_line").Bottom;

	for(int Index=0; Index<3; Index++)
	{
		for(int i=0; i<TexturePos[Index].X; i++)
		{
			for(int j=0; j<TexturePos[Index].Y; j++)
			{
				if(pEPN_TI->getTextureColor(TextureName[Index], EPN_Pos(i, j)).Red < 165 &&
				   pEPN_TI->getTextureColor(TextureName[Index], EPN_Pos(i, j)).Blue < 110 &&
				   pEPN_TI->getTextureColor(TextureName[Index], EPN_Pos(i, j)).Green < 110)
				{
					pEPN_TI->EditTexture(TextureName[Index], EPN_Pos(i, j), EPN_ARGB(0, 255, 255, 255));
				}
			}
		}
	}

	//HP바
	pEPN_TI->LoadTexture(L"hp_gauge", L"MainDevice", L"./Data/ui/hp_gauge.png");

	//코인
	pEPN_TI->LoadTexture(L"coin", L"MainDevice", L"./Data/ui/coin.png");

	//업그레이드 설명, 플레이어 머니 베이스, 화살표, 점선
	pEPN_TI->LoadTexture(L"window", L"MainDevice", L"./Data/ui/window_01.png");
	pEPN_TI->LoadTexture(L"money_base_01", L"MainDevice", L"./Data/ui/money_base_01.png");
	pEPN_TI->LoadTexture(L"money_base_02", L"MainDevice", L"./Data/ui/money_base_02.png");
	pEPN_TI->LoadTexture(L"dot", L"MainDevice", L"./Data/ui/dot.png");
	pEPN_TI->LoadTexture(L"dot_turn", L"MainDevice", L"./Data/ui/dot_turn.png");

	//판매, 둥지
	pEPN_TI->LoadTexture(L"sell", L"MainDevice", L"./Data/ui/pet/sell.png");
	pEPN_TI->LoadTexture(L"nest", L"MainDevice", L"./Data/ui/pet/nest.png");

	//Yes, No 그림(X, V모양)
	pEPN_TI->LoadTexture(L"yes", L"MainDevice", L"./Data/ui/button/yes.png");
	pEPN_TI->LoadTexture(L"no", L"MainDevice", L"./Data/ui/button/no.png");

	//새끼용 일러스트
	pEPN_TI->LoadTexture(L"hatchling_dark_", L"MainDevice", L"./Data/ui/hatchling/hatchling_dark_.png");
	pEPN_TI->LoadTexture(L"hatchling_dark_s", L"MainDevice", L"./Data/ui/hatchling/hatchling_dark_s.png");
	pEPN_TI->LoadTexture(L"hatchling_fire_", L"MainDevice", L"./Data/ui/hatchling/hatchling_fire_.png");
	pEPN_TI->LoadTexture(L"hatchling_fire_s", L"MainDevice", L"./Data/ui/hatchling/hatchling_fire_s.png");
	pEPN_TI->LoadTexture(L"hatchling_lightning_", L"MainDevice", L"./Data/ui/hatchling/hatchling_lightning_.png");
	pEPN_TI->LoadTexture(L"hatchling_lightning_s", L"MainDevice", L"./Data/ui/hatchling/hatchling_lightning_s.png");
	
	//새끼용
	pEPN_TI->LoadTexture(L"hatchling_dark", L"MainDevice", L"./Data/hatchling/hatchling_dark.png");
	pEPN_TI->LoadTexture(L"hatchling_lightning", L"MainDevice", L"./Data/hatchling/hatchling_lightning.png");
	pEPN_TI->LoadTexture(L"hatchling_fire", L"MainDevice", L"./Data/hatchling/hatchling_fire.png");

	//상점 고양이, 고양이 연기, 구매 고양이
	pEPN_TI->LoadTexture(L"shop_cat", L"MainDevice", L"./Data/ui/shop/shop_cat.png");
	pEPN_TI->LoadTexture(L"shop_cat_smoke_01", L"MainDevice", L"./Data/ui/shop/shop_cat_smoke_01.png");
	pEPN_TI->LoadTexture(L"shop_cat_smoke_02", L"MainDevice", L"./Data/ui/shop/shop_cat_smoke_02.png");
	pEPN_TI->LoadTexture(L"shop_cat_smoke_03", L"MainDevice", L"./Data/ui/shop/shop_cat_smoke_03.png");
	pEPN_TI->LoadTexture(L"shop_buy", L"MainDevice", L"./Data/ui/shop/shop_buy.png");
	pEPN_TI->LoadTexture(L"explain_base", L"MainDevice", L"./Data/ui/castle_icon_bg_disable.png");

	//돌아가기, 펫, 업그레이드, 업그레이드 버튼
	pEPN_TI->LoadTexture(L"back", L"MainDevice", L"./Data/ui/button/back.png");	
	pEPN_TI->LoadTexture(L"pet", L"MainDevice", L"./Data/ui/pet/pet.png");
	pEPN_TI->LoadTexture(L"upgrade", L"MainDevice", L"./Data/ui/upgrade/upgrade.png");
	pEPN_TI->LoadTexture(L"shop", L"MainDevice", L"./Data/ui/shop/shop.png");

	//버튼, 버튼테두리
	pEPN_TI->LoadTexture(L"btn_large", L"MainDevice", L"./Data/ui/button/btn_large.png");
	pEPN_TI->LoadTexture(L"btn_medium", L"MainDevice", L"./Data/ui/button/btn_medium.png");
	pEPN_TI->LoadTexture(L"btn_medium_long", L"MainDevice", L"./Data/ui/button/btn_medium_long.png");
	pEPN_TI->LoadTexture(L"btn_medium_long_checkbox", L"MainDevice", L"./Data/ui/button/btn_medium_long_checkbox.png");
	pEPN_TI->LoadTexture(L"btn_medium_checkbox", L"MainDevice", L"./Data/ui/button/btn_medium_checkbox.png");
	pEPN_TI->LoadTexture(L"btn_medium_noshadow", L"MainDevice", L"./Data/ui/button/btn_medium_noshadow.png");

	//kakao, NextFloor 로고
	pEPN_TI->LoadTexture(L"kakao", L"MainDevice", L"./Data/ui/start/kakao.png");
	pEPN_TI->LoadTexture(L"nextfloor", L"MainDevice", L"./Data/ui/start/nextfloor.png");

	//첫화면 뒷배경들
	pEPN_TI->LoadTexture(L"start_background", L"MainDevice", L"./Data/ui/start/start_background.png");
	pEPN_TI->LoadTexture(L"login_01_bg", L"MainDevice", L"./Data/ui/start/login_01_bg.png");

	//배경 캐릭터들 01~05, 전체이용가
	pEPN_TI->LoadTexture(L"login_01_05", L"MainDevice", L"./Data/ui/start/login_01_05.png");
	pEPN_TI->LoadTexture(L"login_01_04", L"MainDevice", L"./Data/ui/start/login_01_04.png");
	pEPN_TI->LoadTexture(L"login_01_03", L"MainDevice", L"./Data/ui/start/login_01_03.png");
	pEPN_TI->LoadTexture(L"login_01_02", L"MainDevice", L"./Data/ui/start/login_01_02.png");
	pEPN_TI->LoadTexture(L"login_01_01", L"MainDevice", L"./Data/ui/start/login_01_01.png");
	pEPN_TI->LoadTexture(L"전체이용가", L"MainDevice", L"./Data/ui/start/전체이용가.png");

	//드라곤 플라이트 글자 01 ~ 04
	pEPN_TI->LoadTexture(L"logo_04", L"MainDevice", L"./Data/ui/start/logo_04.png");
	pEPN_TI->LoadTexture(L"logo_03", L"MainDevice", L"./Data/ui/start/logo_03.png");
	pEPN_TI->LoadTexture(L"logo_02", L"MainDevice", L"./Data/ui/start/logo_02.png");
	pEPN_TI->LoadTexture(L"logo_01", L"MainDevice", L"./Data/ui/start/logo_01.png");

	//메뉴화면 캐릭터
	pEPN_TI->LoadTexture(L"character_01_lv3", L"MainDevice", L"./Data/ui/character_01_lv3.png");

	//업그레이드 하이퍼플라이트, 파워샷, 자석, 캐릭터 공격력, 업그레이드 카운트
	pEPN_TI->LoadTexture(L"character_power", L"MainDevice", L"./Data/ui/upgrade/character_power.png");
	pEPN_TI->LoadTexture(L"hiperflight", L"MainDevice", L"./Data/ui/upgrade/hiperflight.png");
	pEPN_TI->LoadTexture(L"magnet", L"MainDevice", L"./Data/ui/upgrade/magnet.png");
	pEPN_TI->LoadTexture(L"powershot", L"MainDevice", L"./Data/ui/upgrade/powershot.png");
	pEPN_TI->LoadTexture(L"doubleshot", L"MainDevice", L"./Data/ui/upgrade/doubleshot.png");
	pEPN_TI->LoadTexture(L"upgradecount", L"MainDevice", L"./Data/ui/upgrade/upgradecount.png");
	pEPN_TI->LoadTexture(L"upgradecount_on", L"MainDevice", L"./Data/ui/upgrade/upgradecount_on.png");

	//총알
	pEPN_TI->LoadTexture(L"bullet_01_00", L"MainDevice", L"./Data/bullet/bullet_01_00.png");
	pEPN_TI->LoadTexture(L"bullet_01_01", L"MainDevice", L"./Data/bullet/bullet_01_01.png");
	pEPN_TI->LoadTexture(L"bullet_01_02", L"MainDevice", L"./Data/bullet/bullet_01_02.png");
	pEPN_TI->LoadTexture(L"bullet_01_03", L"MainDevice", L"./Data/bullet/bullet_01_03.png");
	pEPN_TI->LoadTexture(L"bullet_01_04", L"MainDevice", L"./Data/bullet/bullet_01_04.png");
	pEPN_TI->LoadTexture(L"bullet_01_05", L"MainDevice", L"./Data/bullet/bullet_01_05.png");
	pEPN_TI->LoadTexture(L"bullet_01_06", L"MainDevice", L"./Data/bullet/bullet_01_06.png");
	pEPN_TI->LoadTexture(L"bullet_01_07", L"MainDevice", L"./Data/bullet/bullet_01_07.png");
	pEPN_TI->LoadTexture(L"bullet_01_08", L"MainDevice", L"./Data/bullet/bullet_01_08.png");
	pEPN_TI->LoadTexture(L"hatchling_bullet_dark", L"MainDevice", L"./Data/bullet/hatchling_bullet_dark.png");
	pEPN_TI->LoadTexture(L"hatchling_bullet_fire", L"MainDevice", L"./Data/bullet/hatchling_bullet_fire.png");
	pEPN_TI->LoadTexture(L"hatchling_bullet_lightning", L"MainDevice", L"./Data/bullet/hatchling_bullet_lightning.png");

	//파워샷
	pEPN_TI->LoadTexture(L"bullet_powershot", L"MainDevice", L"./Data/bullet/bullet_powershot.png");
	pEPN_TI->LoadTexture(L"powershot_effect", L"MainDevice", L"./DAta/effect/hit_powershot.png");

	//드래곤
	pEPN_TI->LoadTexture(L"dragon_01", L"MainDevice", L"./Data/dragon/dragon_01.png");
	pEPN_TI->LoadTexture(L"dragon_02", L"MainDevice", L"./Data/dragon/dragon_02.png");
	pEPN_TI->LoadTexture(L"dragon_03", L"MainDevice", L"./Data/dragon/dragon_03.png");
	pEPN_TI->LoadTexture(L"dragon_04", L"MainDevice", L"./Data/dragon/dragon_04.png");
	pEPN_TI->LoadTexture(L"dragon_05", L"MainDevice", L"./Data/dragon/dragon_05.png");
	pEPN_TI->LoadTexture(L"dragon_06", L"MainDevice", L"./Data/dragon/dragon_06.png");
	pEPN_TI->LoadTexture(L"dragon_07", L"MainDevice", L"./Data/dragon/dragon_07.png");
	pEPN_TI->LoadTexture(L"dragon_08", L"MainDevice", L"./Data/dragon/dragon_08.png");

	//폰트
	pEPN_FI->LoadFont(L"바른돋움13", L"MainDevice", L"바른돋움 2", 13, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움13B", L"MainDevice", L"바른돋움 2", 13, EPN_FONT_BOLD);
	pEPN_FI->LoadFont(L"바른돋움20", L"MainDevice", L"바른돋움 2", 20, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움20B", L"MainDevice", L"바른돋움 2", 20, EPN_FONT_BOLD);
	pEPN_FI->LoadFont(L"바른돋움25", L"MainDevice", L"바른돋움 2", 25, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움25B", L"MainDevice", L"바른돋움 2", 25, EPN_FONT_BOLD);
	pEPN_FI->LoadFont(L"바른돋움30", L"MainDevice", L"바른돋움 2", 30, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움30B", L"MainDevice", L"바른돋움 2", 30, EPN_FONT_BOLD);
	pEPN_FI->LoadFont(L"바른돋움35", L"MainDevice", L"바른돋움 2", 35, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움35B", L"MainDevice", L"바른돋움 2", 35, EPN_FONT_BOLD);
	pEPN_FI->LoadFont(L"바른돋움40", L"MainDevice", L"바른돋움 2", 40, EPN_FONT_NOMAL);
	pEPN_FI->LoadFont(L"바른돋움40B", L"MainDevice", L"바른돋움 2", 40, EPN_FONT_BOLD);


	//배경
	pEPN_TI->LoadTexture(L"background/01", L"MainDevice", L"./Data/background/01.png");
	pEPN_TI->LoadTexture(L"background/02", L"MainDevice", L"./Data/background/02.png");
	pEPN_TI->LoadTexture(L"background/03", L"MainDevice", L"./Data/background/03.png");
	pEPN_TI->LoadTexture(L"background/04", L"MainDevice", L"./Data/background/04.png");
	pEPN_TI->LoadTexture(L"background/05", L"MainDevice", L"./Data/background/05.png");

	//게임 결과용
	pEPN_TI->CreateTexture(L"FadeInOut", L"MainDevice", EPN_Pos(600, 700), EPN_ARGB(255, 0, 0, 0));
	pEPN_TI->LoadTexture(L"character_sd", L"MainDevice", L"./Data/character/character_sd.png");

	//사운드 로드
	pSoundManager->LoadSound("dragon_flight", "./Data/sound/dragon_flight.mp3", true);
	pSoundManager->LoadSound("lobby", "./Data/sound/lobby.mp3", true);
	pSoundManager->LoadSound("buy_fail", "./Data/sound/buy_fail.wav", false);
	pSoundManager->LoadSound("buy_success", "./Data/sound/buy_success.wav", false);
	pSoundManager->LoadSound("dragon_breathe", "./Data/sound/dragon_breathe.wav", false);
	pSoundManager->LoadSound("dragon_click", "./Data/sound/dragon_click.wav", false);
	pSoundManager->LoadSound("dragon_sell", "./Data/sound/dragon_sell.wav", false);
	pSoundManager->LoadSound("equip", "./Data/sound/equip.wav", false);
	pSoundManager->LoadSound("fail", "./Data/sound/fail.wav", false);
	pSoundManager->LoadSound("get_coin", "./Data/sound/get_coin.wav", false);
	pSoundManager->LoadSound("get_gem", "./Data/sound/get_gem.wav", false);
	pSoundManager->LoadSound("get_item", "./Data/sound/get_item.wav", false);
	pSoundManager->LoadSound("hit_rush", "./Data/sound/hit_rush.wav", false);
	pSoundManager->LoadSound("meteor", "./Data/sound/meteor.wav", false);
	pSoundManager->LoadSound("meteor_ambient", "./Data/sound/meteor_ambient.wav", false);
	pSoundManager->LoadSound("meteor_warning", "./Data/sound/meteor_warning.wav", false);
	pSoundManager->LoadSound("power_shot", "./Data/sound/power_shot.wav", false);
	pSoundManager->LoadSound("button_click", "./Data/sound/button_click.wav", false);
	pSoundManager->LoadSound("button_click2", "./Data/sound/button_click2.wav", false);
	pSoundManager->LoadSound("upgrade", "./Data/sound/upgrade.wav", false);
}