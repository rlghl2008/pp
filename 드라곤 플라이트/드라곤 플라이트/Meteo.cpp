#include "Meteo.h"

Meteo :: Meteo(void) 
	: m_WarningCnt(0), m_Angle(0), m_DestroyedCnt(0), m_DustCreateCnt(0),
	  m_IsDestroyedFlag(false), m_WarningFlag(true)
{
	m_ObjectType = OBJECT_METEO;
	m_ObjectInfo.TeamFlag = TEAM_ENEMY;

	m_ObjectInfo.Pos.Y = 0;
	m_ObjectInfo.Pos.X = Character :: CreateInstance()->getPos().X - 15;

	pSoundManager->Play("meteor_warning");
}

Meteo :: ~Meteo(void)
{
}

void Meteo :: IsCollided(ObjectBase * pObject)
{
	if(pObject->getObjectType() == OBJECT_CHARACTER)
	{
		if(Character :: CreateInstance()->getHiperFlightFlag())
		{
			m_IsDestroyedFlag = true;
			m_ObjectInfo.IsCheckedFlag = true;
		}
		else if(pObject->getObjectState() != OBJECT_STATE_FREE)
		{
			pSoundManager->Play("fail");
			pObject->setObjectState(OBJECT_STATE_FREE);
		}
	}
	else if(pObject->getObjectType() == OBJECT_BULLET)
	{
		if(Character :: CreateInstance()->getPowershotFlag())
		{
			m_IsDestroyedFlag = true;
			m_ObjectInfo.IsCheckedFlag = true;
		}
	}
}

vector<Command> Meteo :: Run()
{
	vector<Command> Cmd;

	if(m_IsDestroyedFlag)
	{
		if(m_DestroyedCnt++ >= 45)
		{
			Command MeteoCmd;
			MeteoCmd.CommandType = COMMAND_DELETE;
			Cmd.push_back(MeteoCmd);
			
			return Cmd;
		}
		
		EPN_Rect MeteoSize = pEPN_TI->getTextureSize(L"meteo_piece");

		pEPN_TI->Blt(L"meteo_piece", m_ObjectInfo.Pos + m_DestroyedCnt, MeteoSize, NULL, 0, EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"meteo_piece", EPN_Pos(m_ObjectInfo.Pos.X + 20 + m_DestroyedCnt, m_ObjectInfo.Pos.Y + m_DestroyedCnt), MeteoSize, NULL, 0, EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"meteo_piece", EPN_Pos(m_ObjectInfo.Pos.X - 20 + m_DestroyedCnt, m_ObjectInfo.Pos.Y + m_DestroyedCnt), MeteoSize, NULL, 0, EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"meteo_piece", EPN_Pos(m_ObjectInfo.Pos.X + 50 + m_DestroyedCnt, m_ObjectInfo.Pos.Y + m_DestroyedCnt), MeteoSize, NULL, 0, EPN_Scaling(1, 1));
	}
	else
	{
		Character* pCharacter = Character :: CreateInstance();

		//메테오 생성되기전 경고
		if(m_WarningFlag)
		{
			m_ObjectInfo.Pos.X += EPN_GetAnglePos(EPN_GetPosAngle(m_ObjectInfo.Pos, EPN_Pos(pCharacter->getPos().X - 15, pCharacter->getPos().Y))).X * 5;

			pEPN_TI->Blt(L"warn_line", m_ObjectInfo.Pos, pEPN_TI->getTextureSize(L"warn_line"), EPN_ARGB(100, 255, 255, 255), 0, EPN_Scaling(1, 3));
			pEPN_TI->Blt(L"warn_mark", EPN_Pos(m_ObjectInfo.Pos.X - pEPN_TI->getTextureSize(L"warn_mark").Bottom.X / 2 * 0.7 + 18,
				         m_ObjectInfo.Pos.Y + 10), pEPN_TI->getTextureSize(L"warn_mark"), NULL, 0, EPN_Scaling(0.7, 0.7));

			//시간 카운트후 플래그 닫음
			if(m_WarningCnt++ >= 150)
			{
				m_WarningCnt = 0;

				m_ObjectInfo.Pos.Y += -50;
				m_ObjectInfo.Pos.X += -23;
				m_ObjectInfo.CollisionRect = pEPN_TI->getTextureSize(L"meteo") * EPN_Rect(EPN_Scaling(0.6, 0.6)) + EPN_Rect(10, 10, -10, -10);

				m_WarningFlag = false;

				pSoundManager->Play("meteor");
			}
		}
		//메테오
		else
		{
			EPN_Rect MeteoSize = pEPN_TI->getTextureSize(L"meteo");

			pEPN_TI->Blt(L"meteo", EPN_Pos(m_ObjectInfo.Pos.X, m_ObjectInfo.Pos.Y), MeteoSize, NULL, m_Angle += 2, EPN_Scaling(0.6, 0.6));	
			pEPN_TI->Blt(L"booster", EPN_Pos(m_ObjectInfo.Pos.X - 152, m_ObjectInfo.Pos.Y - 170), MeteoSize, EPN_ARGB(100, 255, 255, 255), 90, EPN_Scaling(3, 1.3));

			m_ObjectInfo.Pos.Y += 3;

			//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + m_ObjectInfo.CollisionRect);
		}
	}
	
	return Cmd;
}