#pragma once
#include "ViewMenu.h"
#include "ViewInGame.h"
#include "ViewLogo.h"
#include "ViewUpgrade.h"
#include "ViewShop.h"
#include "ViewHatchling.h"

class ViewManager
{
public:
	ViewManager(void);
	~ViewManager(void);

private:
	ViewBase * m_pView;
	VIEWSTATE m_ViewState;

public:
	bool ChangeView(VIEWSTATE View);
	bool Run();
};