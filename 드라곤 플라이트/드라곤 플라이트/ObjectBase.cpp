#include "ObjectBase.h"

ObjectBase :: ObjectBase(void) : m_ObjectState(OBJECT_STATE_ACTIVE)
{
	m_ObjectInfo.IsCheckedFlag = false;
	m_pPlayerData = PlayerData :: CreateInstance();

	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);

	pSoundManager = SoundManager::CreateInstance();
}

ObjectBase :: ~ObjectBase(void)
{

}

ObjectInfo* ObjectBase :: getObjectInfo()
{
	return &m_ObjectInfo;
}

void ObjectBase :: setObjectType(OBJECT_TYPE ObjectType)
{
	m_ObjectType = ObjectType;
}

OBJECT_TYPE ObjectBase :: getObjectType()
{
	return m_ObjectType;
}

void ObjectBase :: setObjectState(OBJECT_STATE ObjectState)
{
	m_ObjectState = ObjectState;
}

OBJECT_STATE ObjectBase :: getObjectState()
{
	return m_ObjectState;
}