#include "PlayerData.h"

PlayerData* PlayerData :: m_pPlayerData = NULL;

PlayerData :: PlayerData(void)
{
	m_LeftHatchling = HATCHLING_NULL;
	m_RightHatchling = HATCHLING_NULL;

	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);
}

PlayerData :: ~PlayerData(void)
{
	if(m_pPlayerData != NULL)
	{
		delete m_pPlayerData;
		m_pPlayerData  = NULL;
	}
}

PlayerData* PlayerData :: CreateInstance()
{
	if(!m_pPlayerData)
		m_pPlayerData = new PlayerData();
	return m_pPlayerData; 
}

void PlayerData :: UpgradeIncrease(short UpgradeType)
{
	m_UpgradeCnt[UpgradeType]++;

	if(m_UpgradeCnt[UpgradeType] > 8)
		m_UpgradeCnt[UpgradeType] = 8;
}

short PlayerData :: getUpgradeCnt(short Upgrade_type)
{
	return m_UpgradeCnt[Upgrade_type];
}

int PlayerData :: getMoney() const
{
	return m_Money;
}

void PlayerData :: PlusMoney(int money)
{
	m_Money = m_Money + money;
}

void PlayerData :: MinusMoney(int money)
{
	m_Money = m_Money - money;
}

void PlayerData :: setHatchlingState(short HatchlingType, bool State)
{
	m_Hatchling[HatchlingType] = State;
}

bool PlayerData :: getHatchlingState(short HatchlingType)
{
	return m_Hatchling[HatchlingType];
}

void PlayerData :: PlusPowershotNum()
{
	m_PowershotNum++;
}

void PlayerData :: MinusPowershotNum()
{
	m_PowershotNum--;
}

int PlayerData :: getPowershotNum()
{
	return m_PowershotNum;
}

void PlayerData :: setEquipHatchling(short HatchlingType, bool Direction)
{
	if(Direction)
	{
		if(m_LeftHatchling == HatchlingType)
			m_LeftHatchling = HATCHLING_NULL;
		m_RightHatchling = (HATCHLING_TYPE)HatchlingType;
	}
	else if(!Direction)
	{
		if(m_RightHatchling == HatchlingType)
			m_RightHatchling = HATCHLING_NULL;
		m_LeftHatchling = (HATCHLING_TYPE)HatchlingType;
	}
}

int PlayerData :: getTopScore()
{
	return m_TopScore;
}

void PlayerData :: setTopScore(int Score)
{
	m_TopScore = Score;
}

HATCHLING_TYPE PlayerData :: getEquipHatchling(bool Direction)
{
	if(Direction)
		return m_RightHatchling;
	else if(!Direction)	
		return m_LeftHatchling;
}

bool PlayerData :: PlayerMoneyRun()
{
	pEPN_TI->Blt(L"money_base_01", EPN_Pos(30, 2), pEPN_TI->getTextureSize(L"money_base_01"), NULL, 0, EPN_Scaling(0.2, 1));
	pEPN_TI->Blt(L"coin", EPN_Pos(7, 10), pEPN_TI->getTextureSize(L"coin"), NULL, 0, EPN_Scaling(0.8, 0.8));

	pEPN_FI->BltText(L"바른돋움20B", E_TEXT(m_Money), EPN_Rect(EPN_Pos(37, 13), EPN_Pos(150, 35)), EPN_ARGB(255, 253, 253, 118));

	return true;
}

void PlayerData :: LoadPlayerData()
{
	char Path[128] = { 0, };
	strcpy(Path, "./Data/PlayerData.txt");

	FILE * PlayerData = fopen(Path, "rt");

	if(!PlayerData)
	{
		cout<<"PlayerData.txt 파일이 존재하지 않아 생성하였습니다"<<endl;

		FILE * PlayerData = fopen(Path, "wt");
		fprintf(PlayerData, "Money = 0\n");
		fprintf(PlayerData, "PowershotNum = 0\n");
		fprintf(PlayerData, "TopScore = 0\n");
		fprintf(PlayerData, "Hatchling_Fire = 0");
		fprintf(PlayerData, "Hatchilng_Dark = 0");
		fprintf(PlayerData, "Hatchilng_Lightning = 0");
		fprintf(PlayerData, "Upgrade_CharacterBullet = 0");
		fprintf(PlayerData, "Upgrade_HiperFlight = 0");
		fprintf(PlayerData, "Upgrade_Magnet = 0");
		fprintf(PlayerData, "Upgrade_Powershot = 0");
		fprintf(PlayerData, "Upgrade_DoubleShot = 0");

		fclose(PlayerData);

		LoadPlayerData();
	}

	char Buffer[128] = { 0, };
	int Value;

	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%d", &Value);
	m_Money = Value;

	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%d", &Value);
	m_PowershotNum = Value;

	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%d", &Value);
	m_TopScore = Value;

	for(int i=0; i<HATCHLING_MAX; i++)
	{
		fscanf(PlayerData, "%s", Buffer);
		fscanf(PlayerData, "%s", Buffer);
		fscanf(PlayerData, "%d", &Value);

		m_Hatchling[i] = Value;
	}

	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%d", &Value);
	m_LeftHatchling = (HATCHLING_TYPE)Value;

	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%s", Buffer);
	fscanf(PlayerData, "%d", &Value);
	m_RightHatchling = (HATCHLING_TYPE)Value;

	for(int i=0; i<UPGRADE_MAX; i++)
	{
		fscanf(PlayerData, "%s", Buffer);
		fscanf(PlayerData, "%s", Buffer);
		fscanf(PlayerData, "%d", &Value);

		m_UpgradeCnt[i] = Value;
	}

	fclose(PlayerData);
}

void PlayerData :: SavePlayerData()
{
	char Path[128] = { 0, };
	strcpy(Path, "./Data/PlayerData.txt");

	FILE * PlayerData = fopen(Path, "wt");

	char Buffer[128] = { 0, };
	estring EBuffer;

	EBuffer = E_TEXT("Money = ") + m_Money;
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nPowershotNum = ") + m_PowershotNum;
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nTopScore = ") + m_TopScore;
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nHatchilng_Dark = ") + m_Hatchling[HATCHLING_FIRE];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nHatchling_Fire = ") + m_Hatchling[HATCHLING_DARK];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nHatchilng_Lightning = ") + m_Hatchling[HATCHLING_LIGHTNING];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nEquipLeftHatchling = ") + (int)m_LeftHatchling;
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nEquipRightHatchling = ") + (int)m_RightHatchling;
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nUpgrade_CharacterBullet = ") + m_UpgradeCnt[CHARACTER_BULLET];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nUpgrade_HiperFlight = ") + m_UpgradeCnt[HIPERFLIGHT];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nUpgrade_Magnet = ") + m_UpgradeCnt[MAGNET];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nUpgrade_Powershot = ") + m_UpgradeCnt[POWERSHOT];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	EBuffer = E_TEXT("\nUpgrade_DoubleShot = ") + m_UpgradeCnt[DOUBLESHOT];
	strcpy(Buffer, EBuffer.getStrA().c_str());
	fprintf(PlayerData, Buffer);

	fclose(PlayerData);
}