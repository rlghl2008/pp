#pragma once
#include "ObjectBase.h"

class Bullet : public ObjectBase
{
public:
	Bullet(void);
	~Bullet(void);

private:
	float m_MoveAngle;

public:
	void setMoveAngle(float MoveAngle);
	float getMoveAngle();

public:
	virtual vector<Command> Run();
	virtual void IsCollided(ObjectBase * pObject);
};
