#pragma once
#include "ObjectBase.h"
#include "Item.h"

class Dragon : public ObjectBase
{
public:
	Dragon(void);
	~Dragon(void);

private:
	bool m_HpbarFlag; //체력바 표시 플래그
	bool m_EyeFlag; //맞을시 눈 바뀌는 플래그
	bool m_DeadFlag; //용이 죽을시 플래그
	bool m_CharacterHiperFlag; //캐릭터 하이퍼 플라이트 상태 플래그

	short m_DustCnt;
	short m_EyeFlagCnt;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};