#include "Background.h"
#include <time.h>

Background* Background :: m_pBackground = NULL;

Background :: Background() : m_PosY(0), m_ScrollSpeed(1)
{
	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);

	srand(time(NULL));


	//배경화면 랜덤선택
	m_BackgroundIndex = rand() % 5;

	for(int Cnt = 1; Cnt <= 5; ++Cnt)
		m_TextureIndex[Cnt-1] = pEPN_TI->getTextureIndex(E_TEXT("background/0") + E_TEXT(Cnt));
}

Background :: ~Background()
{
	if(m_pBackground != NULL)
	{
		delete m_pBackground;
		m_pBackground = NULL;
	}
}

Background* Background :: CreateInstance()
{
	if(!m_pBackground)
		m_pBackground = new Background();
	return m_pBackground;
}

void Background :: setScrollSpeed(short ScrollSpeed)
{
	m_ScrollSpeed = ScrollSpeed;
}

short Background :: getScrollSpeed() const
{
	return m_ScrollSpeed;
}

void Background :: ResetBackground()
{
	m_PosY = 0;
	m_ScrollSpeed = 1;
	m_BackgroundIndex = m_BackgroundIndex = rand() % 5;
}

bool Background :: Run()
{
	//배경화면
	pEPN_TI->Blt(m_TextureIndex[m_BackgroundIndex], EPN_Pos(0, -700 + m_PosY), pEPN_TI->getTextureSize(m_TextureIndex[m_BackgroundIndex]), EPN_ARGB(255, 150, 150, 150), 0, EPN_Scaling(1.5625, 1.3671875));
	pEPN_TI->Blt(m_TextureIndex[m_BackgroundIndex], EPN_Pos(0, 0 + m_PosY), pEPN_TI->getTextureSize(m_TextureIndex[m_BackgroundIndex]), EPN_ARGB(255, 150, 150, 150), 0, EPN_Scaling(1.5625, 1.3671875));

	//배경화면 움직임
	if(m_PosY > 700 )
		m_PosY = 0;
	m_PosY += m_ScrollSpeed;

	return true;
}
