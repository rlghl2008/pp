#include "Button.h"

Button :: Button(void)
	:m_State(UI_STATE_NORMAL), m_Scaling(1), m_FontIndex(-1), m_IsSoundPlayed(false)
{
	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);
	pSoundManager = SoundManager::CreateInstance();

	for(int i = 0; i < UI_STATE_MAX; ++i)
	{
		m_TextureIndex[i] = -1;
		m_SoundName[i] = "";
	}
	
}

Button :: ~Button(void)
{
}

void Button :: setState(UI_STATE State)
{
	m_State = State;
}

void Button :: setPos(EPN_Pos Pos)
{
	m_Pos = Pos;
}

void Button :: setRect(EPN_Rect Rect)
{
	m_Rect = Rect;
}

void Button :: setScaling(EPN_Scaling Scaling)
{
	m_Rect.Bottom.X = m_Rect.Bottom.X * Scaling.X;
	m_Rect.Bottom.Y = m_Rect.Bottom.Y * Scaling.Y;

	m_Scaling = Scaling;
}

void Button :: setTextureRect(EPN_Rect TextureRect, UI_STATE State)
{
	if(State < UI_STATE_ALL || State > UI_STATE_MAX - 1 )
		State = UI_STATE_ALL;

	if(State == UI_STATE_ALL)
	{
		for(int StateCnt = 0; StateCnt < UI_STATE_MAX; ++StateCnt)
			m_TextureRect[StateCnt] = TextureRect;
	}
	else
		m_TextureRect[State] = TextureRect;
}

void Button :: setColor(EPN_ARGB Color, UI_STATE State)
{
	if(State < UI_STATE_ALL || State > UI_STATE_MAX - 1 )
		State = UI_STATE_ALL;

	if(State == UI_STATE_ALL)
	{
		for(int StateCnt = 0; StateCnt < UI_STATE_MAX; ++StateCnt)
			m_Color[StateCnt] = Color;
	}
	else
		m_Color[State] = Color;
}

void Button :: setTexture(short TextureIndex, UI_STATE State)
{
	if(State < UI_STATE_ALL || State > UI_STATE_MAX - 1 )
		State = UI_STATE_ALL;

	if(State == UI_STATE_ALL)
	{
		for(short StateCnt = 0; StateCnt < UI_STATE_MAX; ++StateCnt)
			m_TextureIndex[StateCnt] = TextureIndex;
	}
	else
		m_TextureIndex[State] = TextureIndex;
}

void Button :: setTexture(estring TextureName, UI_STATE State)
{
	if(State < UI_STATE_ALL || State > UI_STATE_MAX - 1 )
		State = UI_STATE_ALL;

	short TextureIndex = pEPN_TI->getTextureIndex(TextureName);
	if(State == UI_STATE_ALL)
	{
 		for(short StateCnt = 0; StateCnt < UI_STATE_MAX; ++StateCnt)
			m_TextureIndex[StateCnt] = TextureIndex; 
	}
	else
		m_TextureIndex[State] = TextureIndex;
}

void Button :: setFont(short FontIndex)
{
	m_FontIndex = FontIndex;
}

void Button :: setFont(estring FontName)
{
	short FontIndex = pEPN_FI->getFontIndex(FontName);
	m_FontIndex = FontIndex;
}

void Button :: setFontColor(EPN_ARGB Color, UI_STATE State)
{
	if(State < UI_STATE_ALL || State > UI_STATE_MAX - 1)
		State = UI_STATE_ALL;

	if(State == UI_STATE_ALL)
	{
		for(int StateCnt = 0; StateCnt < UI_STATE_MAX; ++StateCnt)
			m_FontColor[StateCnt] = Color;
	}
	else
		m_FontColor[State] = Color;
}
void Button :: setEText(estring Text)
{
	m_EText = Text;
}

void Button :: setSound(string SoundName, UI_STATE State)
{
	m_SoundName[State] = SoundName;
}

UI_STATE Button :: getState()
{
	return m_State;
}

EPN_Pos Button :: getPos()
{
	return m_Pos;
}

EPN_Rect Button :: getRect()
{
	return m_Rect;
}

EPN_Scaling Button :: getScaling()
{
	return m_Scaling;
}

EPN_Rect Button :: getTextureRect(UI_STATE State)
{
	if(State < UI_STATE_NORMAL || State > UI_STATE_MAX -1)
		return -1;
	return m_TextureRect[State];
}

EPN_ARGB Button :: getColor(UI_STATE State)
{
	if(State < UI_STATE_NORMAL || State > UI_STATE_MAX -1)
		return -1;
	return m_Color[State];
}

short Button :: getTextureIndex(UI_STATE State)
{
	if(State < UI_STATE_NORMAL || State > UI_STATE_MAX -1)
		return -1;
	return m_TextureIndex[State];
}

short Button :: getFontIndex()
{
	return m_FontIndex;
}

EPN_ARGB Button :: getFontColor(UI_STATE State)
{
	if(State < UI_STATE_NORMAL || State > UI_STATE_MAX -1)
		return -1;
	return m_FontColor[State];
}

estring Button :: getEText()
{
	return m_EText;
}

void Button :: playSound(UI_STATE State)
{
	if(State < UI_STATE_NORMAL || State > UI_STATE_MAX -1)
		return;
	if(m_SoundName[State] == "")
		return;

	if(m_IsSoundPlayed == false)
	{
		pSoundManager->Play(m_SoundName[State]);
		m_IsSoundPlayed = true;
	}
}

bool Button :: Run()
{
	m_MousePos = pEvent->getMousePos();

	if(m_State == UI_STATE_POP)
		m_State = UI_STATE_NORMAL;

	if(EPN_CheckCollision(m_MousePos , EPN_Rect(getPos()) + getRect()))
	{
		if( m_State == UI_STATE_NORMAL  && pEvent->getMouseStatL() == 0)
		{
			m_State = UI_STATE_ACTIVE;
			playSound(UI_STATE_ACTIVE);
		}
		else if( pEvent->getMouseStatL() == EPN_EVENT_DOWN && m_State == UI_STATE_ACTIVE)
		{
			m_State = UI_STATE_PUSH;
			playSound(UI_STATE_PUSH);
		}
		else if( pEvent->getMouseStatL() == EPN_EVENT_DOWNING && m_State == UI_STATE_PUSH)
		{
			m_State = UI_STATE_PUSH;
			m_IsSoundPlayed = false;
			playSound(UI_STATE_PUSH);
		}
		else if( pEvent->getMouseStatL() == EPN_EVENT_UP && m_State == UI_STATE_PUSH)
		{
			m_State = UI_STATE_POP;
			playSound(UI_STATE_POP);
		}
	}
	else
	{
		if( m_State == UI_STATE_ACTIVE )
			m_State = UI_STATE_NORMAL;
		else if( m_State == UI_STATE_PUSH )
		{
			if( pEvent->getMouseStatL() == EPN_EVENT_UP )
				m_State = UI_STATE_NORMAL;
		}
		m_IsSoundPlayed = false;
	}

	m_ETextAreaSize = pEPN_FI->getETextAreaSize(m_FontIndex, m_EText);
	m_ButtonRealSize = EPN_Pos(m_Rect.Bottom.X, m_Rect.Bottom.Y);
	m_ETextBltPos = (m_ButtonRealSize - m_ETextAreaSize) / 2;
	

	m_ETextBltRect = EPN_Rect(m_Pos) + EPN_Rect(m_ETextBltPos, m_ETextBltPos + m_ETextAreaSize);


	pEPN_TI->Blt(m_TextureIndex[m_State], m_Pos, m_TextureRect[m_State], m_Color[m_State], 0, m_Scaling);
	if(m_FontIndex != -1)
		pEPN_FI->BltText(m_FontIndex, m_EText, m_ETextBltRect, m_FontColor[m_State]);

	return true;
}