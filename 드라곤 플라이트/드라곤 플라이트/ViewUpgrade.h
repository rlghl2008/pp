#pragma once
#include "ViewBase.h"
#include "YesNoCheck.h"

#define UPGRADE_MAX_SIZE 8

class ViewUpgrade : public ViewBase
{
public:
	ViewUpgrade(void);
	~ViewUpgrade(void);

private:
	enum BUTTON_TYPE
	{
		BUTTON_CHARACTER_BULLET = 0,
		BUTTON_HIPERFLIGHT,
		BUTTON_MAGNET,
		BUTTON_POWERSHOT,
		BUTTON_DOUBLESHOT,
		BUTTON_YES,
		BUTTON_NO,
		BUTTON_BACK,
		BUTTON_MAX
	};

	enum UPGRADE_STATE
	{
		UPGRADE_SUCCESS = -2,
		UPGRADE_FAIL,
		UPGRADE_FULL = 8
	};

private:
	Button m_Button[BUTTON_MAX];
	YesNoCheck m_YesNoCheck;
	bool m_BaseFlag;
	bool m_ChangeFlag;
	bool m_FailFlag;
	float m_ChangePosX;
	short m_UpgradeType;

private:
	void DrawUpgrade();
	void DrawUpgradeUI(short UpgradeType);

private:
	virtual VIEWSTATE Run();
};
