#pragma once
#include "EPN.h"
#include "PlayerData.h"
#include "SoundManager.h"

enum COMMAND_TYPE
{
	COMMAND_NULL = -1,
	COMMAND_CREATE = 0,
	COMMAND_DELETE
};

enum OBJECT_TYPE
{
	OBJECT_NULL = -1,
	OBJECT_CHARACTER = 0,
	OBJECT_HATCHLING,
	OBJECT_DRAGON,
	OBJECT_CRAGON,
	OBJECT_BULLET,
	OBJECT_METEO,
	OBJECT_ITEM,
};

enum TEAM_FLAG
{
	TEAM_PLAYER,
	TEAM_ENEMY,
	TEAM_NEUTRAL
};

enum OBJECT_STATE
{
	OBJECT_STATE_ACTIVE = 0,
	OBJECT_STATE_FREE,
	OBJECT_STATE_DRAGON_DIE,
	OBJECT_STATE_MAX
};

enum ITEM_TYPE
{
	ITEM_NULL = -1,
	ITEM_COIN,
	ITEM_COIN_GEM1,
	ITEM_COIN_GEM2,
	ITEM_MAGNET,
	ITEM_DOUBLESHOT,
	ITEM_HIPERFLIGHT
};

class ObjectBase;

struct Command
{
	Command()
	{
		CommandType = COMMAND_NULL;
		pCreateObject = NULL;
	}
	COMMAND_TYPE CommandType;
	ObjectBase * pCreateObject;
};

struct ObjectInfo
{
	int AtkPower;
	int DefPower;
	int MoveSpeed;
	int HP;
	int MaxHP;
	short TextureIndex;
	bool IsCheckedFlag;
	bool Direction; //���� false ������ true
	ITEM_TYPE ItemType;

	EPN_Pos Pos;
	EPN_Rect Rect;
	EPN_Rect CollisionRect;
	EPN_Scaling Scaling;
	TEAM_FLAG TeamFlag;
	EPN_ARGB Color;

	ObjectInfo()
	{
	}
	~ObjectInfo()
	{
	}

	ObjectInfo(ObjectInfo &Info)
	{
		AtkPower = Info.AtkPower;
		DefPower = Info.DefPower;
		MoveSpeed = Info.MoveSpeed;
		HP = Info.HP;
		MaxHP = Info.MaxHP;
		TextureIndex = Info.TextureIndex;
		Pos = Info.Pos;
		Rect = Info.Rect;
		CollisionRect = Info.CollisionRect;
		Scaling = Info.Scaling;
		TeamFlag = Info.TeamFlag;
		Direction = Info.Direction;
		Color = Info.Color;
		IsCheckedFlag = Info.IsCheckedFlag;
		ItemType = Info.ItemType;
	}
};

class ObjectBase
{
public:
	ObjectBase(void);
	virtual ~ObjectBase(void);

protected:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;
	SoundManager * pSoundManager;

protected:
	OBJECT_STATE m_ObjectState;
	ObjectInfo m_ObjectInfo;
	OBJECT_TYPE m_ObjectType;
	PlayerData * m_pPlayerData;

public:
	ObjectInfo* getObjectInfo();

	void setObjectType(OBJECT_TYPE ObjectType);
	OBJECT_TYPE getObjectType();

	void setObjectState(OBJECT_STATE ObjectState);
	OBJECT_STATE getObjectState();

public:
	virtual vector<Command> Run() = 0;
	virtual void IsCollided(ObjectBase * pObject) = 0;
};

