#include "Bullet.h"

Bullet :: Bullet(void) : ObjectBase()
{
	m_ObjectType = OBJECT_BULLET;

	m_ObjectInfo.TeamFlag = TEAM_PLAYER;
	
	short UpgradeCnt = PlayerData :: CreateInstance()->getUpgradeCnt(0);

	m_ObjectInfo.TextureIndex = pEPN_TI->getTextureIndex(E_TEXT(L"bullet_01_0") + UpgradeCnt);
	m_MoveAngle = 0;
	m_ObjectInfo.Scaling = EPN_Scaling(1, 1);
	m_ObjectInfo.MoveSpeed = 15;
	m_ObjectInfo.AtkPower = 10 * (UpgradeCnt + 1);
	m_ObjectInfo.Pos.Y = 520 - pEPN_TI->getTextureSize(m_ObjectInfo.TextureIndex).Bottom.Y / 2;
	m_ObjectInfo.Rect = pEPN_TI->getTextureSize(m_ObjectInfo.TextureIndex);
	m_ObjectInfo.CollisionRect = m_ObjectInfo.Rect + EPN_Rect(20, 0, -20, -20);
}

Bullet :: ~Bullet(void)
{
}

void Bullet :: setMoveAngle(float MoveAngle)
{
	m_MoveAngle = MoveAngle;
}

float Bullet :: getMoveAngle()
{
	return m_MoveAngle;
}

vector<Command> Bullet :: Run()
{
	vector<Command> Cmd;

	if(this->getObjectState() == OBJECT_STATE_FREE)
	{
		Command DeleteCmd;

		DeleteCmd.CommandType = COMMAND_DELETE;
		Cmd.push_back(DeleteCmd);

		return Cmd;
	}


	m_ObjectInfo.Pos += EPN_GetAnglePos(m_MoveAngle) * m_ObjectInfo.MoveSpeed;
	

	//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + m_ObjectInfo.CollisionRect);
	pEPN_TI->Blt(m_ObjectInfo.TextureIndex, m_ObjectInfo.Pos, m_ObjectInfo.Rect, m_ObjectInfo.Color, 0, m_ObjectInfo.Scaling);


	Command BulletCmd;
	BulletCmd.CommandType = COMMAND_NULL;
	Cmd.push_back(BulletCmd);
	

	return Cmd;
}

void Bullet :: IsCollided(ObjectBase * pObject)
{
}