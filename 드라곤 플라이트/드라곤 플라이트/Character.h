#pragma once 
#include "ObjectBase.h"
#include "Bullet.h"
#include "Hatchling.h"
#include "Background.h"
#include "SoundBox.h"

class Character : public ObjectBase
{
private:
	Character(void);
	~Character(void);

private:
	static Character* m_pCharacter;

public:
	static Character* CreateInstance();

private:
	//총알 딜레이
	int m_ShootDelay;
	int m_ShootDelayCnt;

	//캐릭터 위치
	float m_MousePosX;
	float m_CurCharacterPosX;

	//이펙트 용 카운트, 변수
	short m_Alpha;
	short m_DrawHiperFlightCnt;
	float m_DrawHiperDestroyCnt;
	short m_PowershotEffectCnt;

	//아이템 효과 플래그
	bool m_DoubleShotFlag;
	bool m_HiperFlightFlag;
	bool m_HiperFlightDestroyFlag;
	bool m_MagnetFlag;
	bool m_DrawDestroyEffectFlag;
	bool m_PowershotFlag;

	//캐릭터 죽음시 변수
	bool m_CharacterDieFlag;
	short m_CharacterDieCnt;

	//새끼용 플래그
	bool m_RightHatchlingFlag;
	bool m_LeftHatchlingFlag;

	//각 아이템 효과 시간
	short m_DoubleShotTimeCnt;
	short m_HiperFlightTimeCnt;
	short m_MagnetTimeCnt;
	short m_PowershotTimeCnt;
	float m_DrawMagnetEffectCnt;

	//자석 효과 범위
	EPN_Rect m_MagnetColisionRect;

public:
	void Init();

	EPN_Pos getPos();
	EPN_Rect getMagnetColisionRect();

	void setPowershotFlag(bool Flag);
	bool getPowershotFlag();
	bool getHiperFlightDestroyFlag();
	bool getHiperFlightFlag();
	bool getDoubleShotFlag();
	bool getMagnetFlag();

	void ActiveHiperFlight();
	void DrawHiperDestroyEffect();
	void DrawHiperFlightEffect();

	void DrawMagnetEffect();

	void setCharacterDieFlag(bool Flag);
	short getCharacterDieCnt();

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};
