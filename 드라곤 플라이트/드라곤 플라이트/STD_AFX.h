#pragma once

#include <EPN_Enpine.h>
#include <cstring>
using namespace EPN;
using namespace EPN::BasicFunction;

//파일 저장, 열기
enum FILEDIALOGUE_OPTION
{
   OPEN_FILEDIALOGUE = 0,
   SAVE_FILEDIALOGUE
};

//경로중에 파일명만 가져오는 함수
estring getFileName(estring Path);

//파일대화상자 여는 함수
bool OpenFileDialogue(estring *Path, FILEDIALOGUE_OPTION DialogueOptionFlag);

EPN_Pos getTextureCenterPos(short Index);

int GetMax(int A, int B, int C, int *Order);

void setTextureBrightness(int TextureIndex , int BrightVal);

VOID setTextureMozaicEffect(INT32 TextureIndex, INT32 EffectLevel, EPN_Rect EffectRect);