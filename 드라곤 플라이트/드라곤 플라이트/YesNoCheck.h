#pragma once
#include "Button.h"

enum CHECK_TYPE
{
	YES = 0,
	NO,
	YESNO,
	NORMAL,
	CHECK_MAX
};

class YesNoCheck
{
public:
	YesNoCheck(void);
	~YesNoCheck(void);

private:
	Button m_Button[CHECK_MAX];
	EPN_Pos m_Pos;
	EPN_Scaling m_Scaling;

protected:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;

public:
	void setPos(EPN_Pos Pos);
	EPN_Pos getPos();

	void setScaling(EPN_Scaling Scaling);
	EPN_Scaling getScaling();
public:
	CHECK_TYPE Run(CHECK_TYPE checkType);
};
