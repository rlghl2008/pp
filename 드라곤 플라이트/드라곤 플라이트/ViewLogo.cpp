#include "ViewLogo.h"

ViewLogo :: ViewLogo(void)
{
	m_Alpha = 0;
	m_AlphaC = 0;
	m_FlashCnt = 0;
	
	memset(m_PosY, 0, sizeof(m_PosY));

	m_ChangeFlag = false;
	m_TexturePosFlag = false;

	m_ViewMenuState = STATE_KAKAO;
}

ViewLogo :: ~ViewLogo(void)
{
}

VIEWSTATE ViewLogo :: Run()
{	
	switch(m_ViewMenuState)
	{
	case STATE_KAKAO:
		{
			pEPN_TI->Blt(L"kakao", EPN_Pos(163, 255), EPN_Rect(pEPN_TI->getTextureSize(L"kakao")), EPN_ARGB(m_Alpha, 255, 255, 255));
			if(!m_ChangeFlag)
				m_Alpha += 2;
			else
				m_Alpha -= 2;

			if(m_Alpha > 255)
			{
				m_Alpha = 255;
				m_ChangeFlag = true;
			}
			else if(m_Alpha < 0)
			{
				m_Alpha = 0;
				m_ChangeFlag = false;
				m_ViewMenuState =  STATE_FLOOR;
			}
			break;
		}
	case STATE_FLOOR:
		{
			pEPN_TI->Blt(L"nextfloor", EPN_Pos(140, 266), EPN_Rect(pEPN_TI->getTextureSize(L"nextfloor")), EPN_ARGB(m_Alpha, 255, 255, 255));
			
			//패이드인 아웃
			if(!m_ChangeFlag)
				m_Alpha += 2;
			else
				m_Alpha -= 2;

			if(m_Alpha > 255)
			{
				m_Alpha = 255;
				m_ChangeFlag = true;
			}
			else if(m_Alpha < 0)
			{
				m_Alpha = 0;
				m_ViewMenuState =  STATE_BACKGROUND;
			}
			break;
		}
	case STATE_BACKGROUND:
		{
			//초기 패이드인 아웃
			if(m_ChangeFlag)
				m_Alpha += 2;
			if(m_Alpha > 255)
			{
				m_Alpha = 255;
				m_ChangeFlag = false;
			}

			//배경 1, 2
			pEPN_TI->Blt(L"start_background", EPN_Pos(0, 0), EPN_Rect(pEPN_TI->getTextureSize(L"start_background")), EPN_ARGB(m_Alpha, 255, 255, 255), 0, EPN_Scaling(0.5882352941176471, 0.5255255255255255));
			pEPN_TI->Blt(L"login_01_bg", EPN_Pos(0, 522), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_bg")), EPN_ARGB(m_Alpha, 255, 255, 255), 0, EPN_Scaling(0.5882352941176471, 0.5255255255255255));

			//캐릭터 텍스쳐
			if(!m_ChangeFlag)
			{
				//배경 캐릭터
				pEPN_TI->Blt(L"login_01_05", EPN_Pos(20, -20 + m_PosY[0]), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_05")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"login_01_04", EPN_Pos(300, 100), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_04")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"login_01_03", EPN_Pos(400, 200 + m_PosY[1]), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_03")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"login_01_02", EPN_Pos(-90, 100 + m_PosY[2]), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_02")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"login_01_01", EPN_Pos(40, 250), EPN_Rect(pEPN_TI->getTextureSize(L"login_01_01")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				//전체 이용가
				pEPN_TI->Blt(L"전체이용가", EPN_Pos(538, 10), EPN_Rect(pEPN_TI->getTextureSize(L"전체이용가")), EPN_ARGB(m_Alpha, 255, 255, 255), 0, EPN_Scaling(0.5, 0.5));
				//드라곤 플라이트 글자
				pEPN_TI->Blt(L"logo_04", EPN_Pos(80, 80), EPN_Rect(pEPN_TI->getTextureSize(L"logo_04")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"logo_03", EPN_Pos(75, 30), EPN_Rect(pEPN_TI->getTextureSize(L"logo_03")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.8, 0.8));
				pEPN_TI->Blt(L"logo_02", EPN_Pos(310, 160), EPN_Rect(pEPN_TI->getTextureSize(L"logo_02")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				pEPN_TI->Blt(L"logo_01", EPN_Pos(110, 160), EPN_Rect(pEPN_TI->getTextureSize(L"logo_01")), EPN_ARGB(m_AlphaC, 255, 255, 255), 0, EPN_Scaling(0.7, 0.7));
				
				//캐릭터 패이드인 아웃	
				if(m_AlphaC < 255)
				{
					m_AlphaC += 3;
					if(m_AlphaC > 255)
						m_AlphaC = 255;
				}

				//패이드인 아웃 끝난 후 캐릭터 둥둥 떠다니게하는 부분
				else
				{
					if(!m_TexturePosFlag)
					{
						if(m_PosY[0] < 14)
						{
							m_PosY[0] += 0.2;
							m_PosY[1] += 0.2;
							m_PosY[2] -= 0.2;
						}
						else
						{
							m_PosY[0] += 0.1;
							m_PosY[1] += 0.1;
							m_PosY[2] -= 0.1;
						}

						if(m_PosY[0] > 15)
							m_TexturePosFlag = true;
					}
					else
					{
						if(m_PosY[0] > 1)
						{
							m_PosY[0] -= 0.2;
							m_PosY[1] -= 0.2;
							m_PosY[2] += 0.2;
						}
						else
						{
							m_PosY[0] -= 0.1;
							m_PosY[1] -= 0.1;
							m_PosY[2] += 0.1;
						}
						if(m_PosY[0] < 0)
							m_TexturePosFlag = false;
					}				
					
					//글자 반짝이는 부분
					if(m_FlashCnt < 50)
						pEPN_FI->BltText(L"바른돋움20B", "Press Mouse Left Button...", EPN_Rect(EPN_Pos(160, 620), EPN_Pos(280, 640)), EPN_ARGB(255, 0, 0, 0));
					
					//클릭시 넘어가는 부분
					if(pEvent->getMouseStatL() == EPN_EVENT_UP)
					{
						m_PosY[0] = 0;
						return VIEW_MENU;
					}
				}
			}

			if(m_FlashCnt > 100)
				m_FlashCnt = 0;
			m_FlashCnt++;
			break;
		}
	}
	return VIEW_LOGO;
}
