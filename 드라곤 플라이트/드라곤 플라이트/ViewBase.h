#pragma once

#include "EPN.h"
#include "Button.h"
#include "Background.h"
#include "PlayerData.h"
#include "SoundManager.h"

enum VIEWSTATE
{
	VIEW_NULL = -1,
	VIEW_LOGO = 0,
	VIEW_MENU,
	VIEW_SHOP,
	VIEW_HATCHLING,
	VIEW_UPGRADE,
	VIEW_INGAME,
	VIEW_MAX
};

class ViewBase
{
public:
	ViewBase(void);
	virtual ~ViewBase(void);

protected:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;
	SoundManager * pSoundManager;

protected:
	PlayerData * m_pPlayerData;

public:
	virtual VIEWSTATE Run() = 0;
};