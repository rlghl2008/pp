#pragma once
#include "ObjectBase.h"

class Dust : public ObjectBase
{
public:
	Dust(void);
	~Dust(void);

private:
	short m_Alpha;
	short m_Angle;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};
