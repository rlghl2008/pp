#pragma once
#include "ObjectBase.h"
#include "Character.h"
#include <ctime>

class Item : public ObjectBase
{
public:
	Item(void);
	~Item(void);

private:
	Character* m_pCharacter;

private:
	EPN_Pos m_FirstPos;
	EPN_Pos m_PlusPos;
	short m_CoinValue;
	short m_MovingDirection;

	bool m_SetFirstPosFlag;

private:
	void AttractItem();

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};
