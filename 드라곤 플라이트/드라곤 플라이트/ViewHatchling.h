#pragma once
#include "ViewBase.h"
#include "YesNoCheck.h"
#include "Character.h"

class ViewHatchling : public ViewBase
{
public:
	ViewHatchling();
	~ViewHatchling();

private:
	enum BUTTON_TYPE
	{
		BUTTON_ESC,
		BUTTON_LEFT,
		BUTTON_RIGHT,
		BUTTON_SELL,
		BUTTON_EQUIP_HATCHLING_LEFT,
		BUTTON_EQUIP_HATCHLING_RIGHT,
		BUTTON_BACK,
		BUTTON_MAX
	};

private:
	Button m_HatchlingButton[HATCHLING_MAX];
	Button m_Button[BUTTON_MAX];
	YesNoCheck m_YesNoCheck;

private:
	bool m_ManagerFlag; //팝업창
	bool m_ButtonRunFlag; //팝업창 열리고 밑의 버튼들 작동 여부
	bool m_SellCheckFlag; //판매 확인 여부
	bool m_HatchlingClearFlag; //장착 해제 여부
	bool m_Direction;
	short m_HatchlingType;
	int m_SellPrice[3];
	
private:
	void HatchlingManager(short HatchlingType);
	void SellHatchling(short HatchlingType);
	void ClearHatchling(bool Direction);
	void DrawEquipHatchling(bool Direction); //false = 왼쪽, ture = 오른쪽

public:
	virtual VIEWSTATE Run();
};
