#pragma once
#include "ViewBase.h"

class ViewLogo : public ViewBase
{
private:
	enum VIEWMENU_STATE
	{
		STATE_KAKAO = 0,
		STATE_FLOOR,
		STATE_BACKGROUND,
	};
	VIEWMENU_STATE m_ViewMenuState;
private:
	short m_Alpha;
	short m_AlphaC;
	bool m_ChangeFlag; //패이드인 아웃
	bool m_TexturePosFlag; //캐릭터 위치
	float m_PosY[3];
	short m_FlashCnt;
public:
	ViewLogo(void);
	~ViewLogo(void);
public:
	virtual VIEWSTATE Run();
};