#include "ViewManager.h"

ViewManager :: ViewManager(void)
	: m_pView(NULL), m_ViewState(VIEW_NULL)
{
}

ViewManager :: ~ViewManager(void)
{		
	if(m_pView)
	{
		delete m_pView;
		m_pView = NULL;
	}
}

bool ViewManager :: ChangeView(VIEWSTATE View)
{
	if(View != m_ViewState)
	{
		if(m_pView)
		{
			delete m_pView;
			m_pView = NULL;
		}

		switch(View)
		{
		case VIEW_LOGO:
			m_pView = new ViewLogo();
			m_ViewState = VIEW_LOGO;
			break;
		case VIEW_MENU:
			m_pView = new ViewMenu();
			m_ViewState = VIEW_MENU;
			break;
		case VIEW_UPGRADE:
			m_pView = new ViewUpgrade();
			m_ViewState = VIEW_UPGRADE;
			break;
		case VIEW_HATCHLING:
			m_pView = new ViewHatchling();
			m_ViewState = VIEW_HATCHLING;
			break;
		case VIEW_SHOP:
			m_pView = new ViewShop();
			m_ViewState = VIEW_SHOP;
			break;
		case VIEW_INGAME:
			m_pView = new ViewInGame();
			m_ViewState = VIEW_INGAME;
			break;
		default:
			break;
		}
	}

	return true;
}

bool ViewManager :: Run()
{
	return ChangeView(m_pView->Run());
}