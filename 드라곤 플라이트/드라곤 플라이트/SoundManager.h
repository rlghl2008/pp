#pragma once
#include "fmod.hpp"
#include <map>
#include <iostream>
#include <string>
using namespace std;

#pragma comment(lib, "fmodex_vc.lib")
#define SOUND_CHANNEL_MAX 100

struct SoundInfo
{
	FMOD::Sound* pSound;
	FMOD::Channel* pChannel;

	SoundInfo()
	{
		pSound = NULL;
		pChannel = NULL;
	}
};

class SoundManager
{
private:
	SoundManager();
	virtual ~SoundManager();

	static SoundManager* m_Instance;
public:
	static SoundManager* CreateInstance();
private:
	FMOD::System * m_pSoundSystem;			//사운드를 관리하기 위한 시스템 포인터
	map<string, SoundInfo*> m_SoundDataMap;

private:
	FMOD::Channel * m_pBgmChannel;			 //BGM 채널
	FMOD::Sound * m_pCurBgm;
	std::string m_pCurBgmPath;
	int m_BgmVolume;

public:
	bool LoadSound(string Name, string Path, bool IsRepeat);
	bool Play(string Name);
	int PlayFromPath(string Path, bool IsRepeat);				//Load 된 것이아닌 경로에서 바로 읽어서 등록하고 재생합니다.
	int PlayFromPath(string Name, string Path, bool IsRepeat);	//Load 된 것이아닌 경로에서 바로 읽어서 등록하고 재생합니다
	bool Pause(string Name);
	bool Resume(string Name);
	bool Stop(string Name);
	bool IsPlaying(string Name);
	float GetVolume(string Name);
	bool SetVolume(string Name, float Volume);
	SoundInfo* GetSoundInfo(string Name);
	int GetSoundCount();
	void Run();

};
