#include "YesNoCheck.h"

YesNoCheck :: YesNoCheck(void)
{
	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);

	//Yes 버튼
	m_Button[YES].setTexture(pEPN_TI->getTextureIndex(L"btn_medium"));
	m_Button[YES].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[YES].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[YES].setPos(EPN_Pos(0, 0));
	m_Button[YES].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[YES].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[YES].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[YES].setFont(NULL);
	m_Button[YES].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[YES].setSound("button_click", UI_STATE_POP);

	//No 버튼
	m_Button[NO].setTexture(pEPN_TI->getTextureIndex(L"btn_medium"));
	m_Button[NO].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[NO].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[NO].setPos(EPN_Pos(0, 0));
	m_Button[NO].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[NO].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[NO].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[NO].setFont(NULL);
	m_Button[NO].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[NO].setSound("button_click", UI_STATE_POP);
}

YesNoCheck :: ~YesNoCheck(void)
{
}

void YesNoCheck :: setPos(EPN_Pos Pos)
{
	m_Pos = Pos;

	m_Button[YES].setPos(EPN_Pos(m_Pos.X + 355, m_Pos.Y + (85 * m_Scaling.Y*1.5)));
	m_Button[NO].setPos(EPN_Pos(m_Pos.X + 155, m_Pos.Y + (85 * m_Scaling.Y*1.5)));
}

EPN_Pos YesNoCheck :: getPos()
{
	return m_Pos;
}

void YesNoCheck :: setScaling(EPN_Scaling Scaling)
{
	m_Scaling = Scaling;
}

EPN_Scaling YesNoCheck :: getScaling()
{
	return m_Scaling;
}

CHECK_TYPE YesNoCheck :: Run(CHECK_TYPE checkType)
{
	//베이스
	pEPN_TI->Blt(pEPN_TI->getTextureIndex(L"window"), m_Pos, pEPN_TI->getTextureSize(L"window"), EPN_ARGB(255, 42, 69, 116), 0, m_Scaling);
	
	//버튼 YES, NO 두개
	if(checkType == YESNO)
	{
		//버튼 작동
		m_Button[YES].Run();
		pEPN_TI->Blt(pEPN_TI->getTextureIndex(L"yes"), m_Button[YES].getPos() + EPN_Pos(8, 13), pEPN_TI->getTextureSize(L"yes"), NULL, 0, EPN_Scaling(0.6, 0.6));
		m_Button[NO].Run();
		pEPN_TI->Blt(pEPN_TI->getTextureIndex(L"no"), m_Button[NO].getPos() + EPN_Pos(11, 11), pEPN_TI->getTextureSize(L"no"), NULL, 0, EPN_Scaling(0.6, 0.6));

		//YES 버튼 누를 시
		if(m_Button[YES].getState() == UI_STATE_POP)
			return YES;
		//NO 버튼 누를 시
		if(m_Button[NO].getState() == UI_STATE_POP)
			return NO;
	}

	//버튼 YES 한개
	else if(checkType == YES)
	{
		m_Button[YES].Run();
		pEPN_TI->Blt(pEPN_TI->getTextureIndex(L"yes"), m_Button[YES].getPos() + EPN_Pos(8, 13), pEPN_TI->getTextureSize(L"yes"), NULL, 0, EPN_Scaling(0.6, 0.6));
	
		if(m_Button[YES].getState() == UI_STATE_POP)
			return YES;
	}

	//버튼 NO 한개
	else if(checkType == NO)
	{
		m_Button[NO].Run();
		pEPN_TI->Blt(pEPN_TI->getTextureIndex(L"yes"), m_Button[YES].getPos() + EPN_Pos(8, 13), pEPN_TI->getTextureSize(L"yes"), NULL, 0, EPN_Scaling(0.6, 0.6));
	
		if(m_Button[NO].getState() == UI_STATE_POP)
			return NO;
	}

	return NORMAL;
}