#pragma once
#include "ViewBase.h"

class ViewShop : public ViewBase
{
public:
	ViewShop(void);
	~ViewShop(void);

private:
	enum BUTTON_TYPE
	{
		BUTTON_NULL = -1,
		BUTTON_HATCHLING_FIRE,
		BUTTON_HATCHLING_DARK,
		BUTTON_HATCHLING_LIGHTNING,
		BUTTON_POWERSHOT,
		BUTTON_BUY,
		BUTTON_BACK,
		BUTTON_MAX,
	};
	
	enum BUY_RESULT
	{
		BUY_SUCCESS = 10,
		BUY_FAIL,
		BUY_NULL
	};

private:
	Button m_Button[BUTTON_MAX];
	BUTTON_TYPE m_ButtonType;
	BUY_RESULT m_BuyResult;
	bool m_ChangeFlag;
	bool m_BuyFlag;

private:
	short m_Alpha;
	short m_ChangeCnt;
	short m_Cnt;
	int m_Price[4];

private:
	void ButtonHighlihgt(BUTTON_TYPE ButtonType);
	void ShopManager(BUTTON_TYPE ButtonType, BUY_RESULT State);

public:
	virtual VIEWSTATE Run();
};
