#include "SoundManager.h"
using namespace FMOD;

SoundManager* SoundManager::m_Instance = NULL;

SoundManager* SoundManager::CreateInstance()
{
	if(m_Instance == NULL)
		m_Instance = new SoundManager();
	return m_Instance;
}

SoundManager::SoundManager()
{
	//사운드 시스템 생성 및 초기화
	System_Create(&m_pSoundSystem);
	m_pSoundSystem->init(SOUND_CHANNEL_MAX, FMOD_INIT_NORMAL, NULL);
}

SoundManager::~SoundManager()
{
	m_pSoundSystem->close();
}

bool SoundManager::LoadSound(string Name, string Path, bool IsRepeat)
{
	FMOD_MODE Mode = IsRepeat == true ? (FMOD_LOOP_NORMAL | FMOD_HARDWARE) : (FMOD_LOOP_OFF | FMOD_HARDWARE);
	FMOD::Sound *pSound = NULL;
	FMOD_RESULT Result = m_pSoundSystem->createStream(Path.c_str(), Mode, NULL, &pSound);

	if (FMOD_OK != Result)
		return false;
	SoundInfo* pSoundInfo = new SoundInfo();
	pSoundInfo->pSound = pSound;

	m_SoundDataMap.insert(std::pair<string, SoundInfo*>( Name, pSoundInfo ) );

	return true;
}

bool SoundManager::Play(string Name)
{
	for(std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.begin(); iter != m_SoundDataMap.end(); iter++)
	{
		if(iter->first == Name)
		{
			FMOD::Channel* pChannel;
			FMOD_RESULT Result = m_pSoundSystem->playSound(FMOD_CHANNEL_FREE, iter->second->pSound, false, &pChannel);
			if(FMOD_RESULT::FMOD_OK != Result)
				return false;

			iter->second->pChannel = pChannel;
			return true;
		}
	}
	return false;
}

int SoundManager::PlayFromPath(string Name, string Path, bool IsRepeat)
{
	bool result = LoadSound(Name, Path, IsRepeat);
	if (false == result)
		return -1;
	return Play(Name) == true ? result : -1;
}

bool SoundManager::Pause(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;
		pChannel->setPaused(true);
		return true;
	}
}

bool SoundManager::Resume(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;
		pChannel->setPaused(false);
		return true;
	}
}

bool SoundManager::Stop(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;
		pChannel->stop();
		return true;
	}
}

bool SoundManager::IsPlaying(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		bool IsPlaying = true;
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;
		
		pChannel->isPlaying(&IsPlaying);// = false;
		return IsPlaying;
	}
}

float SoundManager::GetVolume(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;

		float volume = 0.0f;
		FMOD_RESULT Result = pChannel->getVolume(&volume);
		if(FMOD_OK != Result)
			return 0.0f;
		return volume * 100.f;
	}
}

bool SoundManager::SetVolume(string Name, float Volume)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		FMOD::Channel* pChannel = iter->second->pChannel;
		if(pChannel == NULL)
			return false;

		FMOD_RESULT Result = pChannel->setVolume(Volume);
		if(FMOD_OK != Result)
			return false;
		return true;
	}
}

SoundInfo* SoundManager::GetSoundInfo(string Name)
{
	std::map<string, SoundInfo*>::iterator iter = m_SoundDataMap.find(Name);
	if(iter == m_SoundDataMap.end())
		return false;
	else
	{
		return iter->second;
	}
}

int SoundManager::GetSoundCount()
{
	return m_SoundDataMap.size();
}

void SoundManager::Run()
{
	m_pSoundSystem->update();
}
