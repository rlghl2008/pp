#include "Character.h"

Character* Character :: m_pCharacter = NULL;

Character :: Character(void) : ObjectBase(),
	m_ShootDelay(5), m_ShootDelayCnt(0), m_CurCharacterPosX(300), m_DoubleShotTimeCnt(0), m_MagnetTimeCnt(0),
	m_HiperFlightTimeCnt(0), m_DrawHiperDestroyCnt(0), m_DrawHiperFlightCnt(1), m_DrawMagnetEffectCnt(0), m_PowershotTimeCnt(0), m_PowershotEffectCnt(0),
	m_HiperFlightDestroyFlag(false), m_MagnetFlag(false), m_DrawDestroyEffectFlag(false), m_DoubleShotFlag(false),
	m_RightHatchlingFlag(false), m_LeftHatchlingFlag(false), m_PowershotFlag(false), m_HiperFlightFlag(false), m_CharacterDieFlag(false), 
	m_CharacterDieCnt(0), m_Alpha(255)
{
	Init();
}

Character :: ~Character(void)
{
	m_pCharacter = NULL;
}

Character* Character :: CreateInstance()
{
	if(!m_pCharacter)
		m_pCharacter = new Character();
	return m_pCharacter;
}

void Character :: Init()
{
	m_ObjectType = OBJECT_CHARACTER;
	m_ObjectInfo.TeamFlag = TEAM_PLAYER;


	//새끼용 플래그
	if(m_pPlayerData->getEquipHatchling(false) != HATCHLING_NULL)
		m_LeftHatchlingFlag = true;
	if(m_pPlayerData->getEquipHatchling(true) != HATCHLING_NULL)
		m_RightHatchlingFlag = true;


	m_ObjectInfo.TextureIndex = pEPN_TI->LoadTexture("MainDevice", "./Data/character/character.png");
	m_ObjectInfo.Pos.Y = 520;
	m_ObjectInfo.Pos.X = 300;
	m_ObjectInfo.CollisionRect = EPN_Rect(-18, 0, 18, 84);

	m_MagnetColisionRect = EPN_Rect(-90, -50, 90, 130);

	pSoundManager = SoundManager::CreateInstance();
}

EPN_Pos Character :: getPos()
{
	return m_ObjectInfo.Pos;
}

EPN_Rect Character :: getMagnetColisionRect()
{
	return m_MagnetColisionRect;
}

bool Character :: getHiperFlightDestroyFlag()
{
	return m_HiperFlightDestroyFlag;
}

bool Character :: getHiperFlightFlag()
{
	return m_HiperFlightFlag;
}

bool Character :: getDoubleShotFlag()
{
	return m_DoubleShotFlag;
}

bool Character :: getMagnetFlag()
{
	return m_MagnetFlag;
}

bool Character :: getPowershotFlag()
{
	return m_PowershotFlag;
}

void Character :: setPowershotFlag(bool Flag)
{
	m_PowershotFlag = Flag;
}

void Character :: DrawHiperDestroyEffect()
{
	m_DrawHiperDestroyCnt += 0.5;

	EPN_Rect ShockRect = pEPN_TI->getTextureSize(L"shock");
	EPN_Pos ShockPos = EPN_Pos(m_ObjectInfo.Pos.X - ShockRect.Bottom.X * ((float)m_DrawHiperDestroyCnt / 2), m_ObjectInfo.Pos.Y - ShockRect.Bottom.Y * ((float)m_DrawHiperDestroyCnt / 2));

	pEPN_TI->Blt(L"shock", ShockPos, ShockRect, EPN_ARGB(150, 255, 255, 255), 0, EPN_Scaling(m_DrawHiperDestroyCnt, m_DrawHiperDestroyCnt));

	if(m_DrawHiperDestroyCnt == 7)
	{
		m_DrawDestroyEffectFlag = false;
		m_DrawHiperDestroyCnt = 0;
	}
}

void Character :: DrawHiperFlightEffect()
{
	EPN_Rect BoosterRect = pEPN_TI->getTextureSize(L"booster");
	EPN_Rect InvincibleRect_01 = pEPN_TI->getTextureSize(L"invincible_01");
	EPN_Rect InvincibleRect_02 = pEPN_TI->getTextureSize(L"invincible_02");

	short Angle;

	pEPN_TI->Blt(L"booster", EPN_Pos(m_ObjectInfo.Pos.X - BoosterRect.Bottom.X / 2, m_ObjectInfo.Pos.Y - BoosterRect.Bottom.Y), BoosterRect, EPN_ARGB(100, 255, 255, 255), 270, EPN_Scaling(1, 2));

	if(m_DrawHiperFlightCnt < 2)
		Angle = 0;
	else if(m_DrawHiperFlightCnt < 4)
		Angle = 20;
	else if(m_DrawHiperFlightCnt < 6)
		Angle = 40;

	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 10, m_ObjectInfo.Pos.Y - 50), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 335 + Angle, EPN_Scaling(0.7, 1.3));
	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 10, m_ObjectInfo.Pos.Y - 10), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 0 + Angle, EPN_Scaling(1, 1));
	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 20, m_ObjectInfo.Pos.Y + 37), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 45 + Angle, EPN_Scaling(1, 1));

	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 80, m_ObjectInfo.Pos.Y - 50), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 205 - Angle, EPN_Scaling(0.7, 1.3));
	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 110, m_ObjectInfo.Pos.Y - 10), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 180 - Angle, EPN_Scaling(1, 1));
	pEPN_TI->Blt(L"invincible_02", EPN_Pos(m_ObjectInfo.Pos.X - 110, m_ObjectInfo.Pos.Y + 37), InvincibleRect_02, EPN_ARGB(255, 255, 255, 255), 135 - Angle, EPN_Scaling(1, 1));

	pEPN_TI->Blt(L"invincible_01", EPN_Pos(m_ObjectInfo.Pos.X - 37, m_ObjectInfo.Pos.Y + 45), InvincibleRect_01, EPN_ARGB(255, 255, 255, 255), 0, EPN_Scaling(0.6, 2.3));

	m_DrawHiperFlightCnt++;

	if(m_DrawHiperFlightCnt == 6)
		m_DrawHiperFlightCnt = 0;
}

void Character :: DrawMagnetEffect()
{
	m_DrawMagnetEffectCnt += 0.1;
	
	EPN_Rect MagnetEffectRect = pEPN_TI->getTextureSize(L"magnet_effect");
	EPN_Pos MagnetEffectPos = EPN_Pos(m_ObjectInfo.Pos.X - MagnetEffectRect.Bottom.X * (m_DrawMagnetEffectCnt / 2.0), m_ObjectInfo.Pos.Y + 42 - MagnetEffectRect.Bottom.Y * (m_DrawMagnetEffectCnt / 2.0));

 	pEPN_TI->Blt(L"magnet_effect", MagnetEffectPos, MagnetEffectRect, EPN_ARGB(100, 255, 255, 255), 0, EPN_Scaling(m_DrawMagnetEffectCnt, m_DrawMagnetEffectCnt));
	//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + EPN_Rect(m_MagnetColisionRect));

	if(m_DrawMagnetEffectCnt >= 4.0)
		m_DrawMagnetEffectCnt = 0;
}

void Character :: ActiveHiperFlight()
{
	if(m_HiperFlightTimeCnt == 0)
	{
		Background :: CreateInstance()->setScrollSpeed(20);
		m_HiperFlightDestroyFlag = true;
		m_DrawDestroyEffectFlag = true;
	}
	else if(m_HiperFlightTimeCnt == 1)
	{
		m_HiperFlightDestroyFlag = false;
	}
	else if(m_HiperFlightTimeCnt == 60 + (6 * PlayerData :: CreateInstance()->getUpgradeCnt(HIPERFLIGHT)))
	{
		m_HiperFlightDestroyFlag = true;
		m_DrawDestroyEffectFlag = true;
		Background :: CreateInstance()->setScrollSpeed(1);
	}
	else if(m_HiperFlightTimeCnt == 61 + (6 * PlayerData :: CreateInstance()->getUpgradeCnt(HIPERFLIGHT)))
	{
		m_HiperFlightTimeCnt = 0;
		m_DrawHiperFlightCnt = 0;
		m_HiperFlightFlag = false;
		m_HiperFlightDestroyFlag = false;
	}
}

void Character :: setCharacterDieFlag(bool Flag)
{
	m_CharacterDieFlag = Flag;
}

short Character :: getCharacterDieCnt()
{
	return m_CharacterDieCnt;
}

void Character :: IsCollided(ObjectBase * pObject)
{
	if(pObject->getObjectType() == OBJECT_ITEM)
	{
		switch(pObject->getObjectInfo()->ItemType)
		{
		case ITEM_COIN:
			pSoundManager->Play("get_coin");
			m_pPlayerData->PlusMoney(10);
			break;
		case ITEM_COIN_GEM1:
			pSoundManager->Play("get_gem");
			m_pPlayerData->PlusMoney(50);
			break;
		case ITEM_COIN_GEM2:
			pSoundManager->Play("get_gem");
			m_pPlayerData->PlusMoney(100);
			break;
		case ITEM_DOUBLESHOT:
			pSoundManager->Play("get_item");
			m_DoubleShotFlag = true;
			m_DoubleShotTimeCnt = 0;
			break;
		case ITEM_HIPERFLIGHT:
			pSoundManager->Play("get_item");
			m_HiperFlightFlag = true;
			m_HiperFlightTimeCnt = 0;
			break;
		case ITEM_MAGNET:
			pSoundManager->Play("get_item");
			m_MagnetFlag = true;
			m_MagnetTimeCnt = 0;
			break;
		}

		pObject->setObjectState(OBJECT_STATE_FREE);
	}
}

vector<Command> Character :: Run()
{
	vector<Command> Cmd;


	//if(m_CharacterDieCnt == 79)
	//{
	//	Command DeleteCmd;

	//	DeleteCmd.CommandType = COMMAND_DELETE;
	//	Cmd.push_back(DeleteCmd);

	//	m_CharacterDieCnt = 0;
	//	m_Alpha = 0;
	//	
	//	return Cmd;
	//}


	//왼쪽펫 소지시
	if(m_LeftHatchlingFlag)
	{
		m_LeftHatchlingFlag = false;

		Command HatchlingCmd;

		HatchlingCmd.CommandType = COMMAND_CREATE;
		HatchlingCmd.pCreateObject = new Hatchling();
		HatchlingCmd.pCreateObject->getObjectInfo()->Direction = false;

		Cmd.push_back(HatchlingCmd);
	}
	//왼쪽펫 소지시
	if(m_RightHatchlingFlag)
	{
		m_RightHatchlingFlag = false;

		Command HatchlingCmd;

		HatchlingCmd.CommandType = COMMAND_CREATE;
		HatchlingCmd.pCreateObject = new Hatchling();
		HatchlingCmd.pCreateObject->getObjectInfo()->Direction = true;

		Cmd.push_back(HatchlingCmd);
	}


	//캐릭터 생존중
	if(!m_CharacterDieFlag)
	{
		//하이퍼플라이트 아이템 먹을 시
		if(m_HiperFlightFlag)
		{
			if(m_DrawDestroyEffectFlag)
				DrawHiperDestroyEffect();

			ActiveHiperFlight();
			DrawHiperFlightEffect();

			m_HiperFlightTimeCnt++;
		}
		//파워샷 사용시
		else if(m_PowershotFlag)
		{
			short Angle;
			EPN_Rect PowerEffectRect = pEPN_TI->getTextureSize(L"powershot_effect");


			switch(m_PowershotEffectCnt)
			{
			case 0:
				Angle = 0;
			case 1:
				Angle = 60;
			case 2:
				Angle = 120;
			case 3:
				Angle = 180;
			}


			pEPN_TI->Blt(L"powershot_effect", EPN_Pos(m_ObjectInfo.Pos.X - 133, m_ObjectInfo.Pos.Y - 45), PowerEffectRect, EPN_ARGB(255, 255, 255, 255), Angle, EPN_Scaling(0.82, 0.82));


			if(m_PowershotTimeCnt % 3 == 0)
			{
				Command BulletCmd;

				EPN_Rect PowershotRect = pEPN_TI->getTextureSize(L"bullet_powershot");

				BulletCmd.CommandType = COMMAND_CREATE;
				BulletCmd.pCreateObject = new Bullet();
				BulletCmd.pCreateObject->getObjectInfo()->TextureIndex = pEPN_TI->getTextureIndex(L"bullet_powershot");
				BulletCmd.pCreateObject->getObjectInfo()->Rect = PowershotRect;
				BulletCmd.pCreateObject->getObjectInfo()->CollisionRect = PowershotRect + EPN_Rect(20, 20, -84, -130);
				BulletCmd.pCreateObject->getObjectInfo()->AtkPower = 100;
				BulletCmd.pCreateObject->getObjectInfo()->MoveSpeed = 30;
				BulletCmd.pCreateObject->getObjectInfo()->Color = EPN_ARGB(200, 255, 255, 255);
				BulletCmd.pCreateObject->getObjectInfo()->Scaling = EPN_Scaling(0.75, 0.75);
				BulletCmd.pCreateObject->getObjectInfo()->Pos.X = m_ObjectInfo.Pos.X - BulletCmd.pCreateObject->getObjectInfo()->Rect.Bottom.X / 2 * 0.75;

				Cmd.push_back(BulletCmd);
			}

			m_PowershotEffectCnt++;
			m_PowershotTimeCnt++;

			if(m_PowershotTimeCnt == 90 + (12 * PlayerData :: CreateInstance()->getUpgradeCnt(POWERSHOT)))
			{
				m_PowershotTimeCnt = 0;
				m_PowershotFlag = false;
			}
			if(m_PowershotEffectCnt > 3)
				m_PowershotEffectCnt = 0;
		}
		else
		{
			if(m_ShootDelayCnt++ >= m_ShootDelay)
			{
				m_ShootDelayCnt = 0;

				//더블샷 아이템 먹을 시
				if(m_DoubleShotFlag)
				{
					Command BulletCmd1;
					Command BulletCmd2;

					BulletCmd1.CommandType = COMMAND_CREATE;
					BulletCmd1.pCreateObject = new Bullet();
					BulletCmd1.pCreateObject->getObjectInfo()->Pos.X = m_ObjectInfo.Pos.X - BulletCmd1.pCreateObject->getObjectInfo()->Rect.Bottom.X / 1.5;

					BulletCmd2.CommandType = COMMAND_CREATE;
					BulletCmd2.pCreateObject = new Bullet();
					BulletCmd2.pCreateObject->getObjectInfo()->Pos.X = m_ObjectInfo.Pos.X - BulletCmd2.pCreateObject->getObjectInfo()->Rect.Bottom.X / 3;

					Cmd.push_back(BulletCmd1);
					Cmd.push_back(BulletCmd2);
				}
				//평상 시
				else
				{
					Command BulletCmd;

					BulletCmd.CommandType = COMMAND_CREATE;
					BulletCmd.pCreateObject = new Bullet();
					BulletCmd.pCreateObject->getObjectInfo()->Pos.X = m_ObjectInfo.Pos.X - BulletCmd.pCreateObject->getObjectInfo()->Rect.Bottom.X / 2;

					Cmd.push_back(BulletCmd);
				}
			}
		}


		//캐릭터 움직임
		if(pEvent->getMouseStatL() == EPN_EVENT_DOWN)
			m_MousePosX = pEvent->getMousePosX();
		else if(pEvent->getMouseStatL() == EPN_EVENT_DOWNING)
			m_ObjectInfo.Pos.X = m_CurCharacterPosX + pEvent->getMousePosX() - m_MousePosX;
		else if(pEvent->getMouseStatL() == EPN_EVENT_UP)
			m_CurCharacterPosX = m_ObjectInfo.Pos.X;


		//캐릭터가 화면 밖 넘어갈 때
		if(m_ObjectInfo.Pos.X < 0)
			m_ObjectInfo.Pos.X = 0;
		else if(m_ObjectInfo.Pos.X > 599)
			m_ObjectInfo.Pos.X = 599;


		//더블샷 시간 카운트
		if(m_DoubleShotFlag)
		{	
			//시간 + 업그레이드된 시간 도달 시
			if(m_DoubleShotTimeCnt++ > 600 + (60 * PlayerData :: CreateInstance()->getUpgradeCnt(DOUBLESHOT)))
			{
				m_DoubleShotFlag = false;
				m_DoubleShotTimeCnt = 0;
			}
		}	


		//자석 시간 카운트
		if(m_MagnetFlag)
		{
			//시간 + 업그레이드된 시간 도달 시
			if(m_MagnetTimeCnt++ > 600 + (60 * PlayerData :: CreateInstance()->getUpgradeCnt(MAGNET)))
			{
				m_MagnetFlag = false;
				m_MagnetTimeCnt = 0;
				m_DrawMagnetEffectCnt = 0;
			}

			//자석 이펙트
			DrawMagnetEffect();
		}
	}

	//캐릭터 사망 시
	else
	{
		m_CharacterDieCnt++;
		m_Alpha -= 4;
		m_ObjectInfo.Pos.Y += 2;

		if(m_Alpha < 0)
			m_Alpha = 0;
	}


	//캐릭터
	pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X - 71, m_ObjectInfo.Pos.Y + 20), EPN_Rect(EPN_Pos(55, 2), EPN_Pos(119, 51)), EPN_ARGB(m_Alpha, 255, 255, 255));
	pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X + 8, m_ObjectInfo.Pos.Y + 20), EPN_Rect(EPN_Pos(55, 2), EPN_Pos(119, 51)), EPN_ARGB(m_Alpha, 255, 255, 255), 0, 1, 1);
	pEPN_TI->Blt(m_ObjectInfo.TextureIndex, m_ObjectInfo.Pos - 15, EPN_Rect(EPN_Pos(4, 10), EPN_Pos(44, 114)), EPN_ARGB(m_Alpha, 255, 255, 255)); //몸통


	//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + m_ObjectInfo.CollisionRect);


	return Cmd;
}