#include "Dust.h"

Dust :: Dust(void) : m_Alpha(255), m_Angle(0)
{
	float DustScaling = 1.5 + (rand() % 4 + 6) / 10;

	m_ObjectInfo.Scaling = EPN_Scaling(DustScaling, DustScaling);
	m_Angle = rand() % 361;
}

Dust :: ~Dust(void)
{
}

void Dust :: IsCollided(ObjectBase * pObject)
{
}

vector<Command> Dust :: Run()
{
	vector<Command> Cmd;

	if(m_Alpha < 0)
	{
		m_Alpha = 0;

		Command DeleteCmd;

		DeleteCmd.CommandType = COMMAND_DELETE;
		Cmd.push_back(DeleteCmd);

		return Cmd;
	}

	float DustSize = pEPN_TI->getTextureSize(L"dust_dark").Bottom.X * m_ObjectInfo.Scaling.X;

	pEPN_TI->Blt(L"dust_dark", EPN_Pos(m_ObjectInfo.Pos.X - DustSize / 2, m_ObjectInfo.Pos.Y - DustSize / 2), pEPN_TI->getTextureSize(L"dust_dark"), EPN_ARGB(m_Alpha, 255, 255, 255), m_Angle, m_ObjectInfo.Scaling);
	
	m_Alpha -= 3;

	return Cmd;
}