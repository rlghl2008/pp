#include "Item.h"
 
Item :: Item(void) : ObjectBase(),
	m_SetFirstPosFlag(true)
{
	m_pCharacter = Character :: CreateInstance();

	m_ObjectType = OBJECT_ITEM;
	m_ObjectInfo.TeamFlag = TEAM_NEUTRAL;

	//확률 변수
	short RandNum = rand() % 20 + 1;
	short ItemTextureIndex;

	//9이하면 코인이 나옴
	if(RandNum <= 19)
	{
		short RandCoin = rand() % 20 + 1;
		
		if(RandCoin <= 17)
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_coin");
			m_ObjectInfo.ItemType = ITEM_COIN;
		}
		else if(RandCoin <= 19)
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_gem_01");
			m_ObjectInfo.ItemType = ITEM_COIN_GEM1;
		}
		else
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_gem_02");
			m_ObjectInfo.ItemType = ITEM_COIN_GEM2;
		}
	}
	//나머지는 아이템이 나옴
	else
	{
		short RandItem = rand() % 3;

		if(RandItem == 0)
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_magnet");
			m_ObjectInfo.ItemType = ITEM_MAGNET;
		}
		else if(RandItem == 1)
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_doubleshot");
			m_ObjectInfo.ItemType = ITEM_DOUBLESHOT;
		}
		else
		{
			ItemTextureIndex = pEPN_TI->getTextureIndex(L"item_hiperflight");
			m_ObjectInfo.ItemType = ITEM_HIPERFLIGHT;
		}	
	}
	
	m_ObjectInfo.TextureIndex = ItemTextureIndex; 
	m_ObjectInfo.Rect = pEPN_TI->getTextureSize(m_ObjectInfo.TextureIndex);
	m_ObjectInfo.CollisionRect = EPN_Rect(m_ObjectInfo.Rect.Top, EPN_Pos(m_ObjectInfo.Rect.Bottom.X * 0.5, m_ObjectInfo.Rect.Bottom.Y * 0.5));

	//0 = 왼쪽, 1 = 오른쪽
	m_MovingDirection = rand() % 2;
}

Item :: ~Item(void)
{
}

void Item :: AttractItem()
{
	m_ObjectInfo.Pos += EPN_GetAnglePos(EPN_GetPosAngle(EPN_Pos(m_ObjectInfo.Pos.X + m_ObjectInfo.CollisionRect.Bottom.X / 2, 
						m_ObjectInfo.Pos.Y + m_ObjectInfo.CollisionRect.Bottom.Y / 2), EPN_Pos(m_pCharacter->getPos().X, m_pCharacter->getPos().Y + 40))) * 13;
}

void Item :: IsCollided(ObjectBase * pObject)
{
}

vector<Command> Item :: Run()
{
	vector<Command> Cmd;

	if(this->getObjectState() == OBJECT_STATE_FREE)
	{
		Command DeleteCmd;

		DeleteCmd.CommandType = COMMAND_DELETE;
		Cmd.push_back(DeleteCmd);

		return Cmd;
	}

	if(m_SetFirstPosFlag)
	{
		m_FirstPos = m_ObjectInfo.Pos;
		m_SetFirstPosFlag = false;
	}

	//왼쪽으로 낙하
	if(m_MovingDirection == 0)
	{
		if(m_PlusPos.X > -5)
			m_PlusPos += EPN_Pos(-1, -2);
		else if(m_PlusPos.X > -8)
			m_PlusPos += EPN_Pos(-0.5, -1);
		else if(m_PlusPos.X > -9)
			m_PlusPos += EPN_Pos(-0.6, -0.5);
		else if(m_PlusPos.X > -10)
			m_PlusPos += EPN_Pos(-0.4, -0.3);
		else if(m_PlusPos.X > -11)
			m_PlusPos += EPN_Pos(-0.3, -0.2);
		else if(m_PlusPos.X > -12)
			m_PlusPos += EPN_Pos(-0.3, 0.5);
		else if(m_PlusPos.X > -14)
			m_PlusPos += EPN_Pos(-0.5, 1.0);
		else if(m_PlusPos.X > -16)
			m_PlusPos += EPN_Pos(-0.6, 1.6);
		else if(m_PlusPos.X > -18)
			m_PlusPos += EPN_Pos(-0.5, 2.8);
		else if(m_PlusPos.X > -20)
			m_PlusPos += EPN_Pos(-0.4, 4.0);
		else if(m_PlusPos.X > -22)
			m_PlusPos += EPN_Pos(-0.3, 6.5);
		else if(m_PlusPos.X > -24)
			m_PlusPos += EPN_Pos(-0.2, 8.5);
		else
			m_PlusPos += EPN_Pos(-0.1, 9.0);

	}
	//오른쪽으로 낙하
	else
	{
		if(m_PlusPos.X < 5)
			m_PlusPos += EPN_Pos(1, -2);
		else if(m_PlusPos.X < 8)
			m_PlusPos += EPN_Pos(0.5, -1);
		else if(m_PlusPos.X < 9)
			m_PlusPos += EPN_Pos(0.6, -0.5);
		else if(m_PlusPos.X < 10)
			m_PlusPos += EPN_Pos(0.4, -0.3);
		else if(m_PlusPos.X < 11)
			m_PlusPos += EPN_Pos(0.3, -0.2);
		else if(m_PlusPos.X < 12)
			m_PlusPos += EPN_Pos(0.3, 0.5);
		else if(m_PlusPos.X < 14)
			m_PlusPos += EPN_Pos(0.5, 1.0);
		else if(m_PlusPos.X < 16)
			m_PlusPos += EPN_Pos(0.6, 1.6);
		else if(m_PlusPos.X < 18)
			m_PlusPos += EPN_Pos(0.5, 2.8);
		else if(m_PlusPos.X < 20)
			m_PlusPos += EPN_Pos(0.4, 4.0);
		else if(m_PlusPos.X < 22)
			m_PlusPos += EPN_Pos(0.3, 6.5);
		else if(m_PlusPos.X < 24)
			m_PlusPos += EPN_Pos(0.2, 8.5);
		else
			m_PlusPos += EPN_Pos(0.1, 9.0);
	}
 
	//자석 아이템 먹은상태에서 자석 범위안에 들어왓을 때
	if(m_pCharacter->getMagnetFlag() && EPN_CheckCollision(EPN_Rect(m_pCharacter->getPos()) + m_pCharacter->getMagnetColisionRect(), 
		EPN_Rect(m_ObjectInfo.Pos) + m_ObjectInfo.CollisionRect))
	{
		AttractItem();
	}
	//평소
	else
		m_ObjectInfo.Pos = m_FirstPos + m_PlusPos;


	//화면 바깥으로 나갈 때 처리
	if(m_ObjectInfo.Pos.X < 0)
		m_ObjectInfo.Pos.X = 0;
	else if(m_ObjectInfo.Pos.X + m_ObjectInfo.Rect.Bottom.X * 0.5 > 600)
		m_ObjectInfo.Pos.X = 600 - m_ObjectInfo.Rect.Bottom.X * 0.5;
	if(m_ObjectInfo.Pos.Y < 0)
		m_ObjectInfo.Pos.Y = 0;


	//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + m_ObjectInfo.CollisionRect);
	pEPN_TI->Blt(m_ObjectInfo.TextureIndex, m_ObjectInfo.Pos, m_ObjectInfo.Rect, NULL, 0, EPN_Scaling(0.5, 0.5));


	return Cmd;
}