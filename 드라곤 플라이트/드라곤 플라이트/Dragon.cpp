#include "Dragon.h"
#include "Character.h"

Dragon :: Dragon(void) : ObjectBase(),
	m_HpbarFlag(false), m_EyeFlag(false), m_DustCnt(0), m_DeadFlag(false), m_CharacterHiperFlag(false),
	m_EyeFlagCnt(0)
{
	m_ObjectType = OBJECT_DRAGON;
	m_ObjectInfo.TeamFlag = TEAM_ENEMY;


	m_ObjectInfo.Pos = EPN_Pos(35, -108);
	m_ObjectInfo.Rect = pEPN_TI->getTextureSize(L"dragon_01");
	m_ObjectInfo.MoveSpeed = 3;
	m_ObjectInfo.CollisionRect = EPN_Rect(-20, 0, 57, 105);
										   
}										

Dragon :: ~Dragon(void)
{
}

void Dragon :: IsCollided(ObjectBase * pObject)
{	 
	//�΋H�� ������Ʈ�� �÷��̾� ���� �� 
	if(pObject->getObjectInfo()->TeamFlag == TEAM_PLAYER)
	{
		//ĳ���� �϶�
		if(pObject->getObjectType() == OBJECT_CHARACTER)
		{
			//������ �ö���Ʈ ���� ��
			if(Character :: CreateInstance()->getHiperFlightFlag() || Character :: CreateInstance()->getHiperFlightDestroyFlag())
				m_ObjectInfo.HP = 0;
			else if(!Character :: CreateInstance()->getPowershotFlag() && pObject->getObjectState() != OBJECT_STATE_FREE)
			{
				pSoundManager->Play("fail");
				pObject->setObjectState(OBJECT_STATE_FREE);
			}
		}
		//�Ѿ��� ��
		else
		{
			pObject->setObjectState(OBJECT_STATE_FREE);
			pSoundManager->Play("hit_rush");
			pSoundManager->SetVolume("hit_rush", 0.5);
			m_EyeFlag = true;
			m_HpbarFlag = true;
			m_ObjectInfo.HP -= pObject->getObjectInfo()->AtkPower;
		}
	}

	//HP�� 0���ϰ� �ɶ�
	if(m_ObjectInfo.HP <= 0)
	{
		m_ObjectState = OBJECT_STATE_DRAGON_DIE;
		m_ObjectInfo.IsCheckedFlag = true;
		m_DeadFlag = true;
		m_HpbarFlag = false;
	}
}

vector<Command> Dragon :: Run()
{
	vector<Command> Cmd;

	if(m_ObjectState == OBJECT_STATE_FREE)
	{
		Command DeleteCmd;

		DeleteCmd.CommandType = COMMAND_DELETE;
		Cmd.push_back(DeleteCmd);

		return Cmd;
	}

	//������ �ö���Ʈ ������ ��� ����
	if(Character :: CreateInstance()->getHiperFlightDestroyFlag())
	{
		m_ObjectInfo.IsCheckedFlag = true;
		m_DeadFlag = true;
	}


	//�� ������
	if(!m_DeadFlag)
	{
		if(Character :: CreateInstance()->getHiperFlightFlag())
			m_ObjectInfo.Pos.Y += 30;
		else
			m_ObjectInfo.Pos.Y += m_ObjectInfo.MoveSpeed;

		//pEPN_DI->BltQuadrangle(L"MainDevice", EPN_Rect(m_ObjectInfo.Pos) + EPN_Rect(m_ObjectInfo.CollisionRect));
		//����
		pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X - 36, m_ObjectInfo.Pos.Y + 23), EPN_Rect(84, 66, 126, 123), NULL, 0, EPN_Scaling(0.9, 0.9));
		pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X + 37, m_ObjectInfo.Pos.Y + 23), EPN_Rect(84, 66, 126, 123), NULL, 0, EPN_Scaling(0.9, 0.9), true);
		//����
		pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X - 15, m_ObjectInfo.Pos.Y), EPN_Rect(4, 5, 79, 126), NULL, 0, EPN_Scaling(0.9, 0.9)); //����

		//���� ��
		if(!m_EyeFlag)
		{
			pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X + 3, m_ObjectInfo.Pos.Y + 58), EPN_Rect(108, 7, 114, 15), NULL, 0, EPN_Scaling(0.9, 0.9)); //���� ��
			pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X + 30, m_ObjectInfo.Pos.Y + 58), EPN_Rect(108, 7, 114, 15), NULL, 0, EPN_Scaling(0.9, 0.9)); //���� ��
		}
		//�ǰݽ� ��
		else
		{
			m_EyeFlagCnt++;
			if(m_EyeFlagCnt == 30)
			{
				m_EyeFlag = false;
				m_EyeFlagCnt = 0;
			}

			pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X - 2, m_ObjectInfo.Pos.Y + 52), EPN_Rect(103, 27, 129, 45), NULL, 0, EPN_Scaling(0.9, 0.9));
			pEPN_TI->Blt(m_ObjectInfo.TextureIndex, EPN_Pos(m_ObjectInfo.Pos.X + 16, m_ObjectInfo.Pos.Y + 52), EPN_Rect(103, 27, 129, 45), NULL, 0, EPN_Scaling(0.9, 0.9), true);
		}

		//ü�¹�
		if(m_HpbarFlag)
		{
			float ScalingX = (float)m_ObjectInfo.HP / (float)m_ObjectInfo.MaxHP;
			
			if(ScalingX < 0)
				ScalingX = 0;

			pEPN_TI->Blt(L"hp_gauge", EPN_Pos(m_ObjectInfo.Pos.X - 14, m_ObjectInfo.Pos.Y + 110), EPN_Rect(1, 0, 66, 12));
			pEPN_TI->Blt(L"hp_gauge", EPN_Pos(m_ObjectInfo.Pos.X - 13, m_ObjectInfo.Pos.Y + 111), EPN_Rect(2, 13, 65, 22), NULL, 0, EPN_Scaling(ScalingX, 1));
		}
	}
	//�� ���� ��
	else
	{
		EPN_Rect DustRect = pEPN_TI->getTextureSize(L"dust_white");

		pEPN_TI->Blt(L"dust_white", m_ObjectInfo.Pos + m_DustCnt, DustRect, EPN_ARGB(255 - (5.6 * (float)m_DustCnt), 255, 255, 255), 0 + (m_DustCnt * 2), EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"dust_white", m_ObjectInfo.Pos - m_DustCnt, DustRect, EPN_ARGB(255 - (5.6 * (float)m_DustCnt), 255, 255, 255), 30 + (m_DustCnt * 2), EPN_Scaling(1.1, 1.1));
		pEPN_TI->Blt(L"dust_white", EPN_Pos(m_ObjectInfo.Pos.X - m_DustCnt, m_ObjectInfo.Pos.Y + (m_DustCnt * 2)), DustRect, EPN_ARGB(255 - (5.6 * (float)m_DustCnt), 255, 255, 255), 60 + m_DustCnt, EPN_Scaling(1.2, 1.2));
		pEPN_TI->Blt(L"dust_white", m_ObjectInfo.Pos + 10 + m_DustCnt, DustRect, EPN_ARGB(255 - (5.6 * (float)m_DustCnt), 255, 255, 255), 120 + (m_DustCnt * 2), EPN_Scaling(1.4, 1.4));

		//������ ����
		if(m_DustCnt == 0)
		{
			m_ObjectState = OBJECT_STATE_ACTIVE;
			Command DragonCmd;

			DragonCmd.CommandType = COMMAND_CREATE;
			DragonCmd.pCreateObject = new Item();
			DragonCmd.pCreateObject->getObjectInfo()->Pos = m_ObjectInfo.Pos;

			Cmd.push_back(DragonCmd);
		}
		else if(m_DustCnt == 45)
			m_ObjectState = OBJECT_STATE_FREE;

		m_DustCnt++;
	}

	return Cmd;
}