#include "ViewInGame.h"

ViewInGame :: ViewInGame(void) : ViewBase(), m_GetMoneyValue(0),
	m_DistanceScore(0), m_HuntingScore(0), m_DistanceScoreRect(0), m_DistanceScoreRectStd(10), 
	m_DragonCreateCnt(150), m_DragonChangeCnt(0), m_DragonTextureNumber(1), m_DragonCreateDelay(150),
	m_DustStartCnt(0),m_MeteoCreateDelay(500), m_MeteoCreateCnt(400), m_DustCreateCnt(0), 
	m_DustStartFlag(false), m_FadeInOutFlag(false), m_ChangeFlag(false), m_Alpha(0)
{
	pSoundManager = SoundManager::CreateInstance();

	m_IngameType = INGAME_PLAY;

	pSoundManager->Stop("lobby");
	pSoundManager->Play("dragon_flight");
	pSoundManager->SetVolume("dragon_flight", 0.05f);

	Character* character = Character :: CreateInstance();
	character->Init();

	m_ObjectMgr.AddObject(character);
	Background :: CreateInstance()->ResetBackground();

	m_MoneyBeforeGame = m_pPlayerData->getMoney();

	//돌아가기 버튼
	m_Button[BUTTON_BACK].setTexture(pEPN_TI->getTextureIndex(L"back"), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setPos(EPN_Pos(0, 0));
	m_Button[BUTTON_BACK].setRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setTextureRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 100, 100, 100), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 200, 255, 200), UI_STATE_ACTIVE);
	m_Button[BUTTON_BACK].setFont(NULL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setScaling(EPN_Scaling(0.5, 0.5));
	m_Button[BUTTON_BACK].setSound("button_click", UI_STATE_POP);

	m_Button[BUTTON_POWERSHOT].setTexture(pEPN_TI->getTextureIndex(L"btn_medium_noshadow"), UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setPos(EPN_Pos(0, 630));
	m_Button[BUTTON_POWERSHOT].setRect(pEPN_TI->getTextureSize(L"btn_medium_noshadow"));
	m_Button[BUTTON_POWERSHOT].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium_noshadow"));
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(90, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(90, 100, 100, 100), UI_STATE_PUSH);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(90, 200, 255, 200), UI_STATE_ACTIVE);
	m_Button[BUTTON_POWERSHOT].setFont(NULL);
	m_Button[BUTTON_POWERSHOT].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_POWERSHOT].setScaling(EPN_Scaling(0.7, 0.7));

	m_Button[BUTTON_RESULT_OK].setPos(EPN_Pos(360, 580));
	m_Button[BUTTON_RESULT_OK].setTexture(L"btn_medium_long", UI_STATE_ALL);
	m_Button[BUTTON_RESULT_OK].setRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_RESULT_OK].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_RESULT_OK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_RESULT_OK].setColor(EPN_ARGB(255, 253, 220, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_RESULT_OK].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_RESULT_OK].setFont(L"바른돋움25B");
	m_Button[BUTTON_RESULT_OK].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_RESULT_OK].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_RESULT_OK].setScaling(EPN_Scaling(1, 0.7));
	m_Button[BUTTON_RESULT_OK].setEText("   확 인");
	m_Button[BUTTON_RESULT_OK].setSound("button_click", UI_STATE_POP);
}

ViewInGame :: ~ViewInGame(void)
{
	pSoundManager->Stop("dragon_flight");
}

void ViewInGame :: FadeInOut()
{
	pEPN_TI->Blt(L"FadeInOut", EPN_Pos(0, 0), pEPN_TI->getTextureSize(L"FadeInOut"), EPN_ARGB(m_Alpha, 255, 255, 255));

	if(!m_ChangeFlag)
	{
		m_Alpha += 4;

		if(m_Alpha > 255)
		{
			m_ChangeFlag = true;
			m_Alpha = 255;
			m_IngameType = INGAME_RESULT;
			pSoundManager->Stop("dragon_flight");
		}
	}
	else
	{
		m_Alpha -= 4;

		if(m_Alpha < 0)
		{
			m_Alpha = 0;
			m_FadeInOutFlag = false;
		}
	}
}

VIEWSTATE ViewInGame :: Run()
{
	//배경
	Background :: CreateInstance()->Run();

	switch(m_IngameType)
	{
		//게임 플레이
	case INGAME_PLAY:
		{
			//거리점수, 사냥점수
			pEPN_FI->BltText(L"바른돋움20B", E_TEXT(m_DistanceScore), EPN_Rect(575 - m_DistanceScoreRect, 15, 600, 40), EPN_ARGB(255, 208, 241, 255));

			pEPN_TI->Blt(L"dragon_01", EPN_Pos(10, 70), EPN_Rect(4, 5, 79, 126), NULL, 0, EPN_Scaling(0.2, 0.2));
			pEPN_FI->BltText(L"바른돋움20B", E_TEXT(m_HuntingScore), EPN_Rect(37, 73, 200, 103), EPN_ARGB(255, 255, 253, 254));

			if(m_DistanceScore / m_DistanceScoreRectStd == 1)
			{
				m_DistanceScoreRect += 11;
				m_DistanceScoreRectStd *= 10;
			}
			m_DistanceScore += 10;
			int a = m_ObjectMgr.getDeleteDragonCnt();
			m_HuntingScore += 1000 * m_DragonTextureNumber * m_ObjectMgr.getDeleteDragonCnt();
			m_ObjectMgr.setDeleteDragonCnt(0);
			m_DragonChangeCnt++;


			//하이퍼 플라이트 시
			if(Character :: CreateInstance()->getHiperFlightFlag())
			{
				if(m_DragonCreateCnt > 20)
					m_DragonCreateCnt = 0;

				m_DragonCreateDelay = 20;
				m_DistanceScore += 90;
			}
			else
				m_DragonCreateDelay = 150;



			//드래곤 생성
			if(m_DragonCreateCnt++ == m_DragonCreateDelay)
			{
				m_DragonCreateCnt = 0;

				for(int i = 0; i < 5; ++i)
				{
					ObjectBase *pDragon = new Dragon();

					//드래곤 텍스쳐 바꾸는 부분
					if(m_DragonChangeCnt > 1000)
					{
						m_DragonChangeCnt = 0;
						m_DragonTextureNumber++;

						if(m_DragonTextureNumber > 8)
							m_DragonTextureNumber = 8;
					}

					pDragon->getObjectInfo()->TextureIndex = pEPN_TI->getTextureIndex(E_TEXT(L"dragon_0") + m_DragonTextureNumber);
					pDragon->getObjectInfo()->HP = 100 * m_DragonTextureNumber;
					pDragon->getObjectInfo()->MaxHP = 100 * m_DragonTextureNumber;
					pDragon->getObjectInfo()->Pos.X += 123 * i;

					m_ObjectMgr.AddObject(pDragon);
				}
			}
			


			//메테오 생성
			if(m_MeteoCreateCnt++ >= m_MeteoCreateDelay)
			{
				m_MeteoCreateCnt = 0;

				if(rand() % 2 == 0)
				{
					m_DustCreateFlag = true;

					m_ObjectMgr.AddObject(new Meteo());
				}
			}

			//연기 생성
			if(m_DustCreateFlag)
			{
				if(m_DustStartFlag)
				{
					if(m_DustCreateCnt++ == 10)
					{
						m_DustCreateCnt = 0;

						ObjectBase *pDust = new Dust();
						ObjectBase *pMeteo;

						for(int i=0; i<m_ObjectMgr.getObjectCnt(); i++)
						{
							if(m_ObjectMgr.SearchObject(i)->getObjectType() == OBJECT_METEO)
							{
								pMeteo = m_ObjectMgr.SearchObject(i);
								EPN_Pos DustPos = pMeteo->getObjectInfo()->Pos;
								pDust->getObjectInfo()->Pos = EPN_Pos(DustPos.X + 38, DustPos.Y + 10);

								if(i > 1)
								{
									m_ObjectMgr.InsertObject(pDust, i - 1);
									break;
								}
								else
								{
									m_ObjectMgr.InsertObject(pDust, 1);
									break;
								}
							}

							if(i == m_ObjectMgr.getObjectCnt() - 1)
							{
								m_DustStartCnt = 0;
								m_DustCreateFlag = false;
								m_DustStartFlag = false;
							}
						}
					}
				}

				if(m_DustStartCnt++ == 250)
				{
					m_DustStartFlag = true;
					m_DustStartCnt = 0;
				}
			}



			//돌아가기 버튼
			m_Button[BUTTON_BACK].Run();



			//파워샷 버튼
			m_Button[BUTTON_POWERSHOT].Run();
			pEPN_TI->Blt(L"powershot", EPN_Pos(3, 633), pEPN_TI->getTextureSize(L"powershot"), NULL, 0, EPN_Scaling(0.7, 0.7));
			pEPN_FI->BltText(L"바른돋움30B", E_TEXT(m_pPlayerData->getPowershotNum()), EPN_Rect(80, 672, 150, 690), EPN_ARGB(255, 255, 255, 255));


			
			//돌아가기 버튼 누를 시
			if(m_Button[BUTTON_BACK].getState() == UI_STATE_POP)
			{
				Background :: CreateInstance()->ResetBackground();
				return VIEW_MENU;
			}
			//파워샷 버튼 누를 시 & 'A'키 누를 시
			else if(m_Button[BUTTON_POWERSHOT].getState() == UI_STATE_POP || 
					pEvent->IF_KEY_STATE(EPNKEY_KEY_A) == EPN_EVENT_DOWN)
			{
				Character * pCharacter = Character :: CreateInstance();

				if(m_pPlayerData->getPowershotNum() > 0 && pCharacter->getPowershotFlag() == false)
				{
					m_pPlayerData->MinusPowershotNum();
					pSoundManager->Play("power_shot");
					pCharacter->setPowershotFlag(true);
				}
			}



			//코인 획득
			m_GetMoneyValue = m_pPlayerData->getMoney() - m_MoneyBeforeGame;
			pEPN_TI->Blt(L"coin", EPN_Pos(7, 40), pEPN_TI->getTextureSize(L"coin"), NULL, 0, EPN_Scaling(0.8, 0.8));
			pEPN_FI->BltText(L"바른돋움20B", E_TEXT(m_GetMoneyValue), EPN_Rect(EPN_Pos(37, 43), EPN_Pos(150, 58)), EPN_ARGB(255, 246, 244, 245));



			//캐릭터 죽을 시
			if(m_ObjectMgr.SearchObject(0)->getObjectState() == OBJECT_STATE_FREE)
			{
				Character *pCharacter = Character :: CreateInstance();
				Background :: CreateInstance()->setScrollSpeed(1);
				
				pCharacter->setCharacterDieFlag(true);
				
				if(pCharacter->getCharacterDieCnt() >= 80)
					m_FadeInOutFlag = true;	
			}



			//오브젝트 매니저
			m_ObjectMgr.Run();



			break;
		}


		//결과창
	case INGAME_RESULT:
		pEPN_TI->Blt(L"character_sd", EPN_Pos(223, 10), pEPN_TI->getTextureSize(L"character_sd"), NULL, 0, EPN_Scaling(0.55, 0.55));

		int TotalScore = m_DistanceScore + m_HuntingScore;

		//최고기록 갱신
		if(m_pPlayerData->getTopScore() < (m_DistanceScore + m_HuntingScore))
			m_pPlayerData->setTopScore(TotalScore);


		pEPN_FI->BltText(L"바른돋움25B", L"종합 점수", EPN_Rect(80, 280, 140, 305), EPN_ARGB(255, 255, 255, 255));
		pEPN_FI->BltText(L"바른돋움40B", E_TEXT(TotalScore), EPN_Rect(240, 274, 500, 304), EPN_ARGB(255, 254, 254, 252));

		pEPN_FI->BltText(L"바른돋움20", L"거리점수", EPN_Rect(115, 400, 150, 420), EPN_ARGB(255, 184, 189, 187));
		pEPN_FI->BltText(L"바른돋움25B", E_TEXT(m_DistanceScore), EPN_Rect(240, 399, 400, 420), EPN_ARGB(255, 210, 242, 255));

		pEPN_FI->BltText(L"바른돋움20", L"사냥점수", EPN_Rect(115, 430, 150, 450), EPN_ARGB(255, 184, 189, 187));
		pEPN_FI->BltText(L"바른돋움25B", E_TEXT(m_HuntingScore), EPN_Rect(240, 429, 400, 443), EPN_ARGB(255, 215, 250, 217));

		pEPN_FI->BltText(L"바른돋움20", L"골드획득", EPN_Rect(115, 490, 150, 510), EPN_ARGB(255, 184, 189, 187));
		pEPN_TI->Blt(L"coin", EPN_Pos(231, 490), pEPN_TI->getTextureSize(L"coin"), NULL, 0, EPN_Scaling(0.7, 0.7));
		pEPN_FI->BltText(L"바른돋움25B", E_TEXT(m_GetMoneyValue), EPN_Rect(255, 489, 400, 510), EPN_ARGB(255, 255, 255, 255));

		pEPN_FI->BltText(L"바른돋움20", L"최고점수", EPN_Rect(115, 520, 150, 540), EPN_ARGB(255, 184, 189, 187));
		pEPN_FI->BltText(L"바른돋움25B", E_TEXT(m_pPlayerData->getTopScore()), EPN_Rect(240, 519, 400, 538), EPN_ARGB(255, 255, 255, 255));



		EPN_Pos DotSize = pEPN_TI->getTextureSize(L"dot_turn").Bottom;
		pEPN_TI->Blt(L"dot_turn", EPN_Pos(200, 337), EPN_Rect(0, 0, DotSize.X, DotSize.Y - 170), EPN_ARGB(255, 143, 132, 65), 0, EPN_Scaling(0.8, 0.8));
		


		//확인 버튼
		m_Button[BUTTON_RESULT_OK].Run();
		pEPN_TI->Blt(L"yes", EPN_Pos(382, 603), pEPN_TI->getTextureSize(L"yes"), NULL, 0, EPN_Scaling(0.4, 0.4));

		if(m_Button[BUTTON_RESULT_OK].getState() == UI_STATE_POP)
			return VIEW_MENU;


		break;
	}


	//페이드 인 아웃
	if(m_FadeInOutFlag)
	{
		FadeInOut();
	}

	return VIEW_INGAME;
}  