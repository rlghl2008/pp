#include "ViewShop.h"

ViewShop :: ViewShop(void)
	: m_ButtonType(BUTTON_NULL), m_BuyResult(BUY_NULL), m_ChangeFlag(false), m_Alpha(255), 
	  m_ChangeCnt(0), m_Cnt(1), m_BuyFlag(false)
{
	//펫 가격
	m_Price[BUTTON_HATCHLING_FIRE] = 100;
	m_Price[BUTTON_HATCHLING_DARK] = 50000;
	m_Price[BUTTON_HATCHLING_LIGHTNING] = 1000000;
	m_Price[BUTTON_POWERSHOT] = 1000;

	//화염 새끼용 버튼
	m_Button[BUTTON_HATCHLING_FIRE].setPos(EPN_Pos(41, 473));
	m_Button[BUTTON_HATCHLING_FIRE].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_FIRE].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_FIRE].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_FIRE].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_FIRE].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_HATCHLING_FIRE].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_HATCHLING_FIRE].setFont(NULL);
	m_Button[BUTTON_HATCHLING_FIRE].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_HATCHLING_FIRE].setSound("dragon_click", UI_STATE_POP);


	//어둠 새끼용 버튼
	m_Button[BUTTON_HATCHLING_DARK].setPos(EPN_Pos(174, 473));
	m_Button[BUTTON_HATCHLING_DARK].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_DARK].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_DARK].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_DARK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_DARK].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_HATCHLING_DARK].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_HATCHLING_DARK].setFont(NULL);
	m_Button[BUTTON_HATCHLING_DARK].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_HATCHLING_DARK].setSound("dragon_click", UI_STATE_POP);

	//번개 새끼용 버튼
	m_Button[BUTTON_HATCHLING_LIGHTNING].setPos(EPN_Pos(307, 473));
	m_Button[BUTTON_HATCHLING_LIGHTNING].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_LIGHTNING].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_LIGHTNING].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_HATCHLING_LIGHTNING].setFont(NULL);
	m_Button[BUTTON_HATCHLING_LIGHTNING].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_HATCHLING_LIGHTNING].setSound("dragon_click", UI_STATE_POP);

	//파워샷 버튼
	m_Button[BUTTON_POWERSHOT].setPos(EPN_Pos(440, 473));
	m_Button[BUTTON_POWERSHOT].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_POWERSHOT].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_POWERSHOT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_POWERSHOT].setFont(NULL);
	m_Button[BUTTON_POWERSHOT].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_POWERSHOT].setSound("button_click2", UI_STATE_POP);

	//구매 버튼
	m_Button[BUTTON_BUY].setPos(EPN_Pos(310, 350));
	m_Button[BUTTON_BUY].setTexture(L"btn_medium_long", UI_STATE_ALL);
	m_Button[BUTTON_BUY].setRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_BUY].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_BUY].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_BUY].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_BUY].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_BUY].setFont(L"바른돋움25B");
	m_Button[BUTTON_BUY].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_BUY].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_BUY].setScaling(EPN_Scaling(0.8, 0.7));
	m_Button[BUTTON_BUY].setEText("       구매");
	m_Button[BUTTON_BUY].setSound("button_click", UI_STATE_POP);

	//돌아가기 버튼
	m_Button[BUTTON_BACK].setTexture(L"back");
	m_Button[BUTTON_BACK].setPos(EPN_Pos(10, 50));
	m_Button[BUTTON_BACK].setRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setTextureRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 200, 255, 200), UI_STATE_ACTIVE);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 100, 100, 100), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setFont(NULL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setScaling(EPN_Scaling(0.5, 0.5));
	m_Button[BUTTON_BACK].setSound("button_click", UI_STATE_POP);
}

ViewShop :: ~ViewShop(void)
{
	m_pPlayerData->SavePlayerData();
}

void ViewShop :: ButtonHighlihgt(BUTTON_TYPE ButtonType)
{
	short PosX;

	if(ButtonType == BUTTON_HATCHLING_FIRE)
		PosX = 36;
	else if(ButtonType == BUTTON_HATCHLING_DARK)
		PosX = 169;
	else if(ButtonType == BUTTON_HATCHLING_LIGHTNING)
		PosX = 302;
	else if(ButtonType == BUTTON_POWERSHOT)
		PosX = 435;
	else
		return;

	//하이라이트 반짝반짝!
	pEPN_TI->Blt(L"btn_medium_checkbox", EPN_Pos(PosX, 466), pEPN_TI->getTextureSize(L"btn_medium_checkbox"), EPN_ARGB(m_Alpha, 255, 255, 255), 0, EPN_Scaling(1.5, 1.5));

	if(!m_ChangeFlag)
		m_Alpha -= 11;
	else
		m_Alpha += 11;

	if(m_Alpha > 255)
	{
		m_Alpha = 255;
		m_ChangeFlag = false;
	}
	if(m_Alpha < 0)
	{
		m_Alpha = 0;
		m_ChangeFlag = true;
	}
}

void ViewShop :: ShopManager(BUTTON_TYPE ButtonType, BUY_RESULT State)
{
	estring Name;
	estring Text;
	
	//파워샷 개수
	int PowershotNum = m_pPlayerData->getPowershotNum();


	//일반 설명
	if(ButtonType != BUTTON_NULL && State == BUY_NULL)
	{
		if(ButtonType == BUTTON_HATCHLING_FIRE)
		{
			Name = L"실버스톤";
			Text = L"겉은 투박해 보이지만, 속은 귀여움으로\n가득찬 새끼용이다옹. 심심할때 데리고\n놀면 재밌을꺼다옹. 야옹~";
		}
		else if(ButtonType == BUTTON_HATCHLING_DARK)
		{
			Name = L"밍밍이";
			Text = L"온순해 보이지만, 속은 암흑으로 가득\n차있다옹. 혹시라도 만만하게 봤다면 큰코\n다칠거다옹! [다루기에 주의바란다옹]";
		}
		else if(ButtonType == BUTTON_HATCHLING_LIGHTNING)
		{
			Name = L"플플이";
			Text = L"굉장히 귀여운 새끼용이다옹. 평소에\n뒹굴뒹굴 거리면서 귀여움을 자다낸다옹~\n하지만 머리 이외의 부분을 만지면 공격을\n시전하니 조심하라옹~";
		}
		else if(ButtonType == BUTTON_POWERSHOT)
		{
			Name = E_TEXT(L"파워샷 (보유 : ") + E_TEXT(PowershotNum) +"개)";
			Text = L"강력한 빛으로 전방의 몬스터를 제압한다옹!\n하지만 남용은 금물이다옹.\n남용한다면 가난에 시달릴거다옹!";
		}

		//펫이름, 가격
		pEPN_FI->BltText(L"바른돋움30B", Name, EPN_Rect(EPN_Pos(220, 150), EPN_Pos(580, 580)), EPN_ARGB(255, 135, 255, 0));
		pEPN_FI->BltText(L"바른돋움30", E_TEXT(m_Price[ButtonType]), EPN_Rect(EPN_Pos(255, 200), EPN_Pos(580, 580)), EPN_ARGB(255, 255, 255, 255));
		pEPN_TI->Blt(L"coin", EPN_Pos(220, 200), pEPN_TI->getTextureSize(L"coin"));
		pEPN_FI->BltText(L"바른돋움20", Text, EPN_Rect(EPN_Pos(220, 250), EPN_Pos(580, 580)), EPN_ARGB(255, 255, 255, 255));
	}

	//구매 버튼을 누른후 결과
	else if(ButtonType != BUTTON_NULL && State == BUY_SUCCESS || State == BUY_FAIL)
	{
		//구매 성공
		if(State == BUY_SUCCESS)
		{
			Text = L"구매에 성공했다옹!";
			if(ButtonType == BUTTON_POWERSHOT)
				Text = Text + L"\n파워샷 개수는\n" + E_TEXT(PowershotNum) + L"개 다옹!";

			if(m_BuyFlag)
			{
				pSoundManager->Play("buy_success");

				if(ButtonType == BUTTON_POWERSHOT)
					m_pPlayerData->PlusPowershotNum();
				else
					m_pPlayerData->setHatchlingState(ButtonType, true);
				
				m_pPlayerData->MinusMoney(m_Price[ButtonType]);
				m_BuyFlag = false;
			}
		}
		//구매 실패
		else if(ButtonType != BUTTON_NULL && State == BUY_FAIL)
		{
			//새끼용이 이미 있을 때
			if(ButtonType != BUTTON_POWERSHOT && m_pPlayerData->getHatchlingState(ButtonType))
				Text = L"이미 데리고 있는\n새끼용이다옹!\n귀엽다고 너무\n욕심부리지 마라옹!";
			//돈이 부족할 때
			else
				Text = L"돈이 없다옹!!\n돈을 더 가져오라옹!";

			if(m_BuyFlag)
			{
				pSoundManager->Play("buy_fail");
				m_BuyFlag = false;
			}
		}

		pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(220, 150), EPN_Pos(580, 580)), EPN_ARGB(255, 255, 255, 255));
	}
	else
	{
		Text = L"나옹의 상점에 온걸 \n환영한다옹~";
		pEPN_FI->BltText(L"바른돋움30", Text, EPN_Rect(EPN_Pos(220, 150), EPN_Pos(580, 580)), EPN_ARGB(255, 255, 255, 255));
	}
}

VIEWSTATE ViewShop :: Run()
{
	//배경
	Background :: CreateInstance()->Run();

	//플레이어 머니
	m_pPlayerData->PlayerMoneyRun();




	//베이스
	pEPN_TI->Blt(L"window", EPN_Pos(-320, 100), pEPN_TI->getTextureSize(L"window"), EPN_ARGB(255, 42, 69, 116), 0, EPN_Scaling(2, 3));
	pEPN_TI->Blt(L"dot", EPN_Pos(20, 450), pEPN_TI->getTextureSize(L"dot"), EPN_ARGB(255, 143, 129, 68), 0, EPN_Scaling(0.95, 1));
	pEPN_TI->Blt(L"shop", EPN_Pos(268, 60), pEPN_TI->getTextureSize(L"shop"), NULL, 0, EPN_Scaling(0.7, 0.7));



	                                                      
	//고양이, 고양이 연기움직임
	pEPN_TI->Blt(L"shop_cat", EPN_Pos(0, 120), pEPN_TI->getTextureSize(L"shop_cat"), NULL, 0, EPN_Scaling(0.6, 0.6));
	pEPN_TI->Blt(E_TEXT("shop_cat_smoke_0") + E_TEXT(m_Cnt), EPN_Pos(120, 160), pEPN_TI->getTextureSize(E_TEXT("shop_cat_smoke_0") + E_TEXT(m_Cnt)), NULL, 0, EPN_Scaling(0.6, 0.6));
	m_ChangeCnt++;
	if(m_ChangeCnt > 25)
	{
		m_Cnt++;
		if(m_Cnt > 3)
			m_Cnt = 1;

		m_ChangeCnt = 0;
	}



	//설명 베이스
	pEPN_TI->Blt(L"explain_base", EPN_Pos(200, 130), pEPN_TI->getTextureSize(L"explain_base"), EPN_ARGB(100, 0, 0, 0), 0, EPN_Scaling(5.4, 3.8));
	
	
	
	//설명, 구매 담당 매니저
	ShopManager(m_ButtonType, m_BuyResult);



	//돌아가기 버튼
	m_Button[BUTTON_BACK].Run();

	//화염 새끼용 버튼
	m_Button[BUTTON_HATCHLING_FIRE].Run();
	pEPN_TI->Blt(L"hatchling_fire_s", EPN_Pos(46, 480), pEPN_TI->getTextureSize(L"hatchling_fire_s"), NULL, 0, EPN_Scaling(0.75, 0.75));

	//어둠 새끼용 버튼
	m_Button[BUTTON_HATCHLING_DARK].Run();
	pEPN_TI->Blt(L"hatchling_dark_s", EPN_Pos(184, 480), pEPN_TI->getTextureSize(L"hatchling_dark_s"), NULL, 0, EPN_Scaling(0.75, 0.75));

	//번개 새끼용 버튼
	m_Button[BUTTON_HATCHLING_LIGHTNING].Run();
	pEPN_TI->Blt(L"hatchling_lightning_s", EPN_Pos(317, 480), pEPN_TI->getTextureSize(L"hatchling_lightning_s"), NULL, 0, EPN_Scaling(0.75, 0.75));

	//파워샷 버튼
	m_Button[BUTTON_POWERSHOT].Run();
	pEPN_TI->Blt(L"powershot", EPN_Pos(447, 478), pEPN_TI->getTextureSize(L"powershot"), NULL, 0, EPN_Scaling(1, 1));

	//구매 버튼
	m_Button[BUTTON_BUY].Run();
	pEPN_TI->Blt(L"shop_buy", EPN_Pos(325, 358), pEPN_TI->getTextureSize(L"shop_buy"), NULL, 0, EPN_Scaling(0.65, 0.65));



	//클릭된 버튼 하이라이트
	ButtonHighlihgt(m_ButtonType);



	//돌아가기 버튼 누를 시
	if(m_Button[BUTTON_BACK].getState() == UI_STATE_POP)
	{
		m_BuyResult = BUY_NULL;
		return VIEW_MENU;
	}

	//화염 새끼용 누를 시
	else if(m_Button[BUTTON_HATCHLING_FIRE].getState() == UI_STATE_POP)
	{
		m_ButtonType = BUTTON_HATCHLING_FIRE;
		m_BuyResult = BUY_NULL;
	}

	//어둠 새끼용 누를 시
	else if(m_Button[BUTTON_HATCHLING_DARK].getState() == UI_STATE_POP)
	{	
		m_ButtonType = BUTTON_HATCHLING_DARK;
		m_BuyResult = BUY_NULL;
	}

	//번개 새끼용 누를 시
	else if(m_Button[BUTTON_HATCHLING_LIGHTNING].getState() == UI_STATE_POP)
	{
		m_ButtonType = BUTTON_HATCHLING_LIGHTNING;
		m_BuyResult = BUY_NULL;
	}

	//파워샷 누를 시
	else if(m_Button[BUTTON_POWERSHOT].getState() == UI_STATE_POP)
	{
		m_ButtonType = BUTTON_POWERSHOT;
		m_BuyResult = BUY_NULL;
	}

	//구매 버튼 누를 시
	else if(m_Button[BUTTON_BUY].getState() == UI_STATE_POP)
	{
		if(m_pPlayerData->getMoney() < m_Price[m_ButtonType] || m_pPlayerData->getHatchlingState(m_ButtonType) && m_ButtonType != BUTTON_POWERSHOT)
			m_BuyResult = BUY_FAIL;
		else
			m_BuyResult = BUY_SUCCESS;
		m_BuyFlag = true;
	}

	return VIEW_SHOP;
}