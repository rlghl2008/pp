#pragma once
#include "ObjectBase.h"
#include "Character.h"
#include "Dust.h"

class Meteo : public ObjectBase
{
public:
	Meteo(void);
	~Meteo(void);

private:
	short m_WarningCnt;
	bool m_WarningFlag;

	short m_DestroyedCnt;
	bool m_IsDestroyedFlag;

	short m_DustCreateCnt;

	int m_Angle;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual vector<Command> Run();
};