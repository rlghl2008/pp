#pragma once
#include "EPN.h"

enum UPGRADE_TYPE
{
	CHARACTER_BULLET = 0,
	HIPERFLIGHT,
	MAGNET,
	POWERSHOT,
	DOUBLESHOT,
	UPGRADE_MAX
};

enum HATCHLING_TYPE
{
	HATCHLING_NULL = -1,
	HATCHLING_FIRE,
	HATCHLING_DARK,
	HATCHLING_LIGHTNING,
	HATCHLING_MAX
};

class PlayerData
{
private:
	PlayerData(void); 
	~PlayerData(void);

protected:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;

private:
	static PlayerData * m_pPlayerData;

private:
	int m_Money; //소지 돈
	int m_PowershotNum; //소지 파워샷 개수
	int m_TopScore; //역대 최고 점수
	short m_UpgradeCnt[UPGRADE_MAX]; //업그레이드 카운트
	bool m_Hatchling[HATCHLING_MAX]; //새끼용 소유 여부


	HATCHLING_TYPE m_LeftHatchling; //장착 새끼용
	HATCHLING_TYPE m_RightHatchling;

public:
	static PlayerData* CreateInstance();


public:
	void UpgradeIncrease(short Upgrade_type);
	short getUpgradeCnt(short Upgrade_type);
	
	int getMoney() const;
	void PlusMoney(int money);
	void MinusMoney(int money);

	void setHatchlingState(short HatchlingType, bool State);
	bool getHatchlingState(short HatchlingType);
	
	void PlusPowershotNum();
	void MinusPowershotNum();
	int getPowershotNum();

	int getTopScore();
	void setTopScore(int Score);

	void setEquipHatchling(short HatchlingType, bool Direction); //false = 왼쪽, true = 오른쪽
	HATCHLING_TYPE getEquipHatchling(bool Direction); //false = 왼쪽, true = 오른쪽
	
public:
	void LoadPlayerData();
	void SavePlayerData();

public:
	bool PlayerMoneyRun();
};