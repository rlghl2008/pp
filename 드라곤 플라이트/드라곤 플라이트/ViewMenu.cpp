#include "ViewMenu.h"
#include "Button.h"
#include "SoundManager.h"
#include <time.h>
ViewMenu :: ViewMenu(void)
{
	pSoundManager = SoundManager::CreateInstance();

	srand(time(NULL));

	memset(m_PosY, 0, sizeof(m_PosY));

	m_Alpha = 255;

	m_ChangeFlag = false;
	m_TexturePosFlag = false;

	if(pSoundManager->IsPlaying("lobby") == false)
	{
		pSoundManager->Play("lobby");
		pSoundManager->SetVolume("lobby", 0.05f);
	}
	
	//스타트 버튼
	m_Button[BUTTON_GAMESTART].setPos(EPN_Pos(480, 620));
	m_Button[BUTTON_GAMESTART].setTexture(L"btn_medium_long", UI_STATE_ALL);
	m_Button[BUTTON_GAMESTART].setRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_GAMESTART].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium_long"));
	m_Button[BUTTON_GAMESTART].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_GAMESTART].setColor(EPN_ARGB(255, 253, 220, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_GAMESTART].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_GAMESTART].setFont(L"바른돋움20B");
	m_Button[BUTTON_GAMESTART].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_GAMESTART].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_GAMESTART].setScaling(EPN_Scaling(0.5, 0.5));
	m_Button[BUTTON_GAMESTART].setEText("게임시작");
	m_Button[BUTTON_GAMESTART].setSound("button_click", UI_STATE_POP);
	
	//새끼용 버튼
	m_Button[BUTTON_HATCHLING].setPos(EPN_Pos(10, 600));
	m_Button[BUTTON_HATCHLING].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_HATCHLING].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_HATCHLING].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_HATCHLING].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_HATCHLING].setFont(NULL);
	m_Button[BUTTON_HATCHLING].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_HATCHLING].setSound("button_click", UI_STATE_POP);

	//업그레이드 버튼
	m_Button[BUTTON_UPGRADE].setPos(EPN_Pos(100, 600));
	m_Button[BUTTON_UPGRADE].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_UPGRADE].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_UPGRADE].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_UPGRADE].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_UPGRADE].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_UPGRADE].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_UPGRADE].setFont(NULL);
	m_Button[BUTTON_UPGRADE].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_UPGRADE].setSound("button_click", UI_STATE_POP);

	//상점 버튼
	m_Button[BUTTON_SHOP].setPos(EPN_Pos(190, 600));
	m_Button[BUTTON_SHOP].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_SHOP].setRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_SHOP].setTextureRect(pEPN_TI->getTextureSize(L"btn_medium"));
	m_Button[BUTTON_SHOP].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_SHOP].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_SHOP].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);//200 119 600 700
	m_Button[BUTTON_SHOP].setFont(NULL);
	m_Button[BUTTON_SHOP].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_SHOP].setSound("button_click", UI_STATE_POP);
}


ViewMenu :: ~ViewMenu(void)
{
}

void ViewMenu :: StartBGM(void)
{
}


VIEWSTATE ViewMenu :: Run()
{
	//배경
	Background :: CreateInstance()->Run();

	//플레이어 머니
	m_pPlayerData->PlayerMoneyRun();

	int a = 10;

	//캐릭터 일러스트
	pEPN_TI->Blt(L"character_01_lv3", EPN_Pos(50, 60 + m_PosY[0]), pEPN_TI->getTextureSize(L"character_01_lv3"), NULL, 0, EPN_Scaling(0.55, 0.55));

	//게임시작 버튼
	m_Button[BUTTON_GAMESTART].Run();
	if(m_Button[BUTTON_GAMESTART].getState() != UI_STATE_PUSH)
		pEPN_TI->Blt(L"btn_medium_long_checkbox", EPN_Pos(480, 618), pEPN_TI->getTextureSize(L"btn_medium_long_checkbox"), 
		EPN_ARGB(m_Alpha, 255, 255, 255), 0, EPN_Scaling(0.5, 0.5));

	//새끼용 버튼
	m_Button[BUTTON_HATCHLING].Run(); 
	pEPN_TI->Blt(L"pet", EPN_Pos(18, 607), pEPN_TI->getTextureSize(L"pet"), NULL, 0, EPN_Scaling(0.85, 0.85));
	pEPN_FI->BltText(L"바른돋움13", "새끼용", EPN_Rect(EPN_Pos(32, 665), EPN_Pos(45, 670)), EPN_ARGB(255, 0, 0, 0));

	//업그레이드 버튼
	m_Button[BUTTON_UPGRADE].Run();
	pEPN_TI->Blt(L"upgrade", EPN_Pos(112, 605), pEPN_TI->getTextureSize(L"upgrade"), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움13", "업그레이드", EPN_Rect(EPN_Pos(112, 665), EPN_Pos(135, 670)), EPN_ARGB(255, 0, 0, 0));
	
	//상점 버튼
	m_Button[BUTTON_SHOP].Run();
	pEPN_TI->Blt(L"shop", EPN_Pos(195, 600), pEPN_TI->getTextureSize(L"shop"), NULL, 0, EPN_Scaling(0.7, 0.7));
	pEPN_FI->BltText(L"바른돋움13", "상점", EPN_Rect(EPN_Pos(217, 665), EPN_Pos(230, 685)), EPN_ARGB(255, 0, 0, 0));
		
		
	//업그레이드 버튼 클릭 시
	if(m_Button[BUTTON_UPGRADE].getState() == UI_STATE_POP)
		return VIEW_UPGRADE;

	//게임시작 버튼 클릭 시
	if(m_Button[BUTTON_GAMESTART].getState() == UI_STATE_POP)
		return VIEW_INGAME;
	
	//새끼용 버튼 클릭 시
	if(m_Button[BUTTON_HATCHLING].getState() == UI_STATE_POP)
		return VIEW_HATCHLING;

	//상점 버튼 클릭 시
	if(m_Button[BUTTON_SHOP].getState() == UI_STATE_POP)
		return VIEW_SHOP;



	//캐릭터 떠다니는게 하는 부분
	if(!m_TexturePosFlag)
	{
		if(m_PosY[0] < 14)
			m_PosY[0] += 0.2;
		else
			m_PosY[0] += 0.1;
		if(m_PosY[0] > 15)
			m_TexturePosFlag = true;
	}
	else
	{
		if(m_PosY[0] > 1)
			m_PosY[0] -= 0.2;
		else
			m_PosY[0] -= 0.1;

		if(m_PosY[0] < 0)
			m_TexturePosFlag = false;
	}
	
	//버튼 테두리 반짝이는 부분
	if(!m_ChangeFlag)
		m_Alpha -= 11;
	else
		m_Alpha += 11;

	if(m_Alpha > 255)
	{
		m_Alpha = 255;
		m_ChangeFlag = false;
	}
	if(m_Alpha < 0)
	{
		m_Alpha = 0;
		m_ChangeFlag = true;
	}
		
	return VIEW_MENU;
}