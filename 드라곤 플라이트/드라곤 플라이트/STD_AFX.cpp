//#include "STD_AFX.h"
//
////경로에서 파일이름 찾아내는 함수
//estring getFileName(estring Path)
//{
//	int len = Path.length();
//
//	for(int i=len; i>0; i--)
//	{
//		if(Path.getMid(i-1, i) == "\\")
//			return Path.getMid(i, len);
//	}
//
//	return "파일명을 찾을 수 없습니다.";
//}
//
////파일 대화상자 여는 함수
//bool OpenFileDialogue(estring *Path, FILEDIALOGUE_OPTION DialogueOptionFlag)
//{
//	//파일 오픈
//	if(DialogueOptionFlag == OPEN_FILEDIALOGUE)
//	{
//		OPENFILENAME SFN;
//		wchar_t PathSave[256] = {0,};
//		ZeroMemory(&SFN, sizeof(OPENFILENAME));
//		SFN.lStructSize = sizeof(OPENFILENAME); 
//		SFN.lpstrTitle = L"파일 가져오기";
//		SFN.hwndOwner = NULL;
//		SFN.lpstrFilter = L"Png File(*.png*) \0*.png*\0"; 
//
//		SFN.lpstrFile = PathSave;
//		SFN.nMaxFile = sizeof(PathSave);              
//		SFN.lpstrInitialDir = L"./";
//
//		if(GetSaveFileName(&SFN))
//		{
//			*Path = PathSave;
//			return true;
//		}
//	}
//	//파일 세이브
//	else if(DialogueOptionFlag == SAVE_FILEDIALOGUE)
//	{
//		OPENFILENAME SFN;
//		wchar_t PathSave[256] = {0,};
//		ZeroMemory(&SFN, sizeof(OPENFILENAME));
//		SFN.lStructSize = sizeof(OPENFILENAME); 
//		SFN.lpstrTitle = L"저장 위치";
//		SFN.hwndOwner = NULL;
//		SFN.lpstrFilter = L"All File(*.*)\0*.*\0Png File(*.png*)\0*.png*\0";
//						  
//		SFN.lpstrFile = PathSave;
//		SFN.nMaxFile = sizeof(PathSave);              
//		SFN.lpstrInitialDir = L"./";
//		if(GetSaveFileName(&SFN))
//		{
//			*Path = PathSave;
//			return true;
//		}
//	}
//	return false;
//}
//
//// 그림 (텍스쳐)의 중앙 좌표를 얻어오는 함수
//EPN_Pos getTextureCenterPos(short Index)
//{
//	EPN_TextureInterface *pEPN_TI;
//	EPN_Init_Interface(&pEPN_TI);
//	EPN_Rect TexSize = pEPN_TI->getTextureSize(Index).Bottom.X;
//	EPN_Pos TexPos = pEPN_TI->getTextureSize(Index).Bottom.X;
//
//	return EPN_Pos(TexPos.X + (TexSize.Bottom.X / 2), TexPos.Y + (TexSize.Bottom.Y / 2));
//}
//
//int GetMax(int A, int B, int C, int *Order)
//{
//	if(A > B)
//	{
//		if(A > C)
//		{
//			*Order = 1;
//			return A;
//		}
//		else
//		{
//			*Order = 3;
//			return C;
//		}
//	}
//	else
//	{
//		if(B > C)
//		{
//			*Order = 2;
//			return B;
//		}
//		else
//		{
//			*Order = 3;
//			return C;
//		}
//	}
//}
//
//void setTextureBrightness(int TextureIndex , int BrightVal)
//{
//	EPN_TextureInterface *pEPN_TI = EPN_TextureInterface :: CreateInstance();
//
//	EPN_Texture *pTexture = pEPN_TI->getTexture(TextureIndex);
//	EPN_Pos TextureSize = pTexture->getTextureSize().Bottom;
//	for(INT32 y = 0; y < TextureSize.Y; ++y)
//	{
//		for(INT32 x = 0; x < TextureSize.X; ++x)
//		{
//			EPN_ARGB PixelColor = pTexture->getTextureColor(EPN_Pos(x,y));
//			EPN_ARGB BltColor = PixelColor;
//			int MaxSelect = 0;
//			float Max = GetMax(PixelColor.Red, PixelColor.Green , PixelColor.Blue , &MaxSelect);
//
//			float Gap = 255.0f - Max;
//			Gap /= 50.0;
//
//			if(MaxSelect == 1) //빨간색이 가장 높다. (기준 빨강)
//			{
//				BltColor.Red += (Gap*(BrightVal-50));
//				BltColor.Green = BltColor.Red * (PixelColor.Green / PixelColor.Red );
//				BltColor.Blue = BltColor.Red * (PixelColor.Blue / PixelColor.Red );
//			}
//			else if(MaxSelect == 2) //초록이 가장높다. (기준 초록)
//			{
//				BltColor.Green += (Gap*(BrightVal-50));
//				BltColor.Red = BltColor.Green * (PixelColor.Red / PixelColor.Green );
//				BltColor.Blue = BltColor.Green * (PixelColor.Blue / PixelColor.Green );
//
//			}
//			else if(MaxSelect == 3) //파랑이 가장높다. (기준 파랑)
//			{
//				BltColor.Blue += (Gap*(BrightVal-50));
//				BltColor.Red = BltColor.Blue * (PixelColor.Red / PixelColor.Blue );
//				BltColor.Green = BltColor.Blue * (PixelColor.Green / PixelColor.Blue );
//			}
//
//			pTexture->EditTexture(EPN_Pos(x,y), EPN_ARGB(255, BltColor.Red , BltColor.Green , BltColor.Blue ) );
//		}
//	}
//}
//
//VOID setTextureMozaicEffect(INT32 TextureIndex , INT32 EffectLevel  , EPN_Rect EffectRect )
//{
//   EPN_TextureInterface *pEPN_TI = EPN_TextureInterface :: CreateInstance();
//   EPN_Texture *pTexture = pEPN_TI->getTexture(TextureIndex);
//   EPN_Pos TextureSize = pTexture->getTextureSize().Bottom;
//
//   EPN_Pos MozaicRectangleSize = EPN_Pos(EffectLevel,EffectLevel);
//
//   if(EffectRect.Bottom.X < EffectRect.Top.X)
//   {
//      REAL32 Temp = EffectRect.Bottom.X;
//      EffectRect.Bottom.X = EffectRect.Top.X;
//      EffectRect.Top.X = Temp;
//   }
//   if(EffectRect.Bottom.Y < EffectRect.Top.Y)
//   {
//      REAL32 Temp = EffectRect.Bottom.Y;
//      EffectRect.Bottom.Y = EffectRect.Top.Y;
//      EffectRect.Top.Y = Temp;
//   }
//
//   EPN_Rect MozaicRect;
//   EffectRect == EPN_Rect(0,0,0,0) ? MozaicRect = EPN_Rect( EPN_Pos(0,0), TextureSize) : MozaicRect = EffectRect;
// 
//   for(INT32 y = MozaicRect.Top.Y; y < MozaicRect.Bottom.Y;)
//   {
//      INT32 rectY;
//      for(INT32 x = MozaicRect.Top.X; x < MozaicRect.Bottom.X;)
//      {
//         EPN_ARGB AverageColor = 0;
//         /* 영역내 평균값 구하기 */
//         INT32 rectX;
//         for(rectY = 0; rectY < MozaicRectangleSize.Y && rectY + y < TextureSize.Y; ++rectY)
//         {
//            for(rectX = 0; rectX < MozaicRectangleSize.X && rectX + x < TextureSize.X; ++rectX)
//            {   //여기서는 합을 구하고
//               AverageColor += pTexture->getTextureColor(EPN_Pos(rectX+x, rectY+y));
//            }
//         }
//         //평균 값 처리 시작
//         AverageColor.Alpha /= (rectX*rectY);
//         AverageColor.Red /= (rectX*rectY);
//         AverageColor.Green /= (rectX*rectY);
//         AverageColor.Blue /= (rectX*rectY);
//
//         /* 구해던 평균값으로 픽셀 색상 설정 */
//         for(rectY = 0; rectY < MozaicRectangleSize.Y && rectY + y < TextureSize.Y; ++rectY)
//         {
//            for(rectX = 0; rectX < MozaicRectangleSize.X && rectX + x < TextureSize.X; ++rectX)
//            {
//               pTexture->EditTexture(EPN_Pos(rectX + x , rectY + y), AverageColor);
//            }
//         }
//         x += rectX;
//      }
//      y += rectY;
//   }
//}
