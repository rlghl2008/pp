#pragma once
#include "EPN.h"

class Background
{
private:
	Background(void);
	~Background(void);

protected:
	EPN_DeviceInterface * pEPN_DI;
	EPN_TextureInterface * pEPN_TI;
	EPN_FontInterface * pEPN_FI;
	EPN_InputEvent * pEvent;

private:
	static Background* m_pBackground;
	short m_PosY;
	short m_TextureIndex[5];
	short m_BackgroundIndex;
	short m_ScrollSpeed;

public:
	static Background* CreateInstance();

	void setScrollSpeed(short ScrollSpeed);
	short getScrollSpeed() const;

	void ResetBackground();

public:
	bool Run();
};
