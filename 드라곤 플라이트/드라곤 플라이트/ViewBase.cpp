#include "ViewBase.h"

ViewBase :: ViewBase(void)
{
	m_pPlayerData = PlayerData :: CreateInstance();

	EPN_Init_Interface(&pEPN_DI);
	EPN_Init_Interface(&pEPN_TI);
	EPN_Init_Interface(&pEPN_FI);
	EPN_Init_Interface(&pEvent);

	pSoundManager = SoundManager::CreateInstance();
}

ViewBase :: ~ViewBase(void)

{
}
