#include "Hatchling.h"

Hatchling :: Hatchling(void) : ObjectBase(),
	m_ShootDelayCnt(0), m_ShootDelay(17), m_SettingFlag(true), m_AtkPower(0)
{
	m_ObjectInfo.IsCheckedFlag = true;
}

Hatchling :: ~Hatchling(void)
{
}

void Hatchling :: IsCollided(ObjectBase * pObject)
{

}

vector<Command> Hatchling :: Run()
{
	vector<Command> Cmd;

	if(m_SettingFlag)
	{
		HATCHLING_TYPE HatchlingType;

		if(m_ObjectInfo.Direction == false)
		{
			HatchlingType = m_pPlayerData->getEquipHatchling(false);
			m_DirectionPos = EPN_Pos(-115, 55);
		}
		else if(m_ObjectInfo.Direction == true)
		{
			HatchlingType = m_pPlayerData->getEquipHatchling(true);
			m_DirectionPos = EPN_Pos(88, 55);
		}

		switch(HatchlingType)
		{
		case HATCHLING_FIRE:
			m_HatchlingName = L"hatchling_fire";
			m_BulletName = L"hatchling_bullet_fire";
			m_AtkPower = 10 + 10 * PlayerData :: CreateInstance()->getUpgradeCnt(0);
			break;
		case HATCHLING_DARK:
			m_HatchlingName = L"hatchling_dark";
			m_BulletName = L"hatchling_bullet_dark";
			m_AtkPower = 20 + 10 * PlayerData :: CreateInstance()->getUpgradeCnt(0);
			break;
		case HATCHLING_LIGHTNING:
			m_HatchlingName = L"hatchling_lightning";
			m_BulletName = L"hatchling_bullet_lightning";
			m_AtkPower = 30 + 10 * PlayerData :: CreateInstance()->getUpgradeCnt(0);
			break;
		}

		m_SettingFlag = false;
	}

	EPN_Pos HatchlingPos = Character ::CreateInstance()->getPos() + m_DirectionPos;

	//��
	pEPN_TI->Blt(m_HatchlingName, HatchlingPos, EPN_Rect(3, 0, 31, 45));
	pEPN_TI->Blt(m_HatchlingName, EPN_Pos(HatchlingPos.X + 15, HatchlingPos.Y + 16), EPN_Rect(11, 46, 31, 62), NULL, 0, 1, true);
	pEPN_TI->Blt(m_HatchlingName, EPN_Pos(HatchlingPos.X - 10, HatchlingPos.Y + 16), EPN_Rect(11, 46, 31, 62));


	if(m_ShootDelayCnt++ >= m_ShootDelay)
	{
		m_ShootDelayCnt = 0;

		Command BulletCmd;

		EPN_Pos BulletPos;
		EPN_Rect BulletCollisionRect;
		

		BulletCmd.CommandType = COMMAND_CREATE;
		BulletCmd.pCreateObject = new Bullet();
		BulletCmd.pCreateObject->getObjectInfo()->AtkPower = m_AtkPower;
		BulletCmd.pCreateObject->getObjectInfo()->TextureIndex = pEPN_TI->getTextureIndex(m_BulletName);


		if(m_HatchlingName == L"hatchling_lightning")
		{
			BulletPos = EPN_Pos(HatchlingPos.X - BulletCmd.pCreateObject->getObjectInfo()->Rect.Bottom.X / 2 + 53, HatchlingPos.Y - 25);
			BulletCollisionRect = pEPN_TI->getTextureSize(m_BulletName) + EPN_Rect(5, 5, -5, -5);
		}
		else
		{
			BulletPos = EPN_Pos(HatchlingPos.X - BulletCmd.pCreateObject->getObjectInfo()->Rect.Bottom.X / 2 + 44, HatchlingPos.Y - 25);
			BulletCollisionRect = pEPN_TI->getTextureSize(m_BulletName) + EPN_Rect(15, 5, -15, -15);
		}
		
		BulletCmd.pCreateObject->getObjectInfo()->CollisionRect = BulletCollisionRect;
		BulletCmd.pCreateObject->getObjectInfo()->Pos = BulletPos;

		Cmd.push_back(BulletCmd);
	}

	return Cmd;
}