#include "ObjectManager.h"

ObjectManager :: ObjectManager(void) : m_DragonDeleteCnt(0)
{
}

ObjectManager :: ~ObjectManager(void)
{
	ClearObject();
}

void ObjectManager :: AddObject(ObjectBase * pObject)
{
	m_vObjectArray.push_back(pObject);
}

void ObjectManager :: InsertObject(ObjectBase * pObject, int Index)
{
	m_vObjectArray.insert(m_vObjectArray.begin() + Index, pObject);
}

bool ObjectManager :: RemoveObject(int Index)
{
	if(Index < 0 || Index > m_vObjectArray.size() - 1)
		return false;

	delete m_vObjectArray[Index];
	m_vObjectArray.erase(m_vObjectArray.begin() + Index);
	return true;
}

ObjectBase* ObjectManager :: SearchObject(int Index)
{
	if(Index < 0 || Index > m_vObjectArray.size() - 1)
		return NULL;

	return m_vObjectArray[Index];
}

int ObjectManager :: getObjectCnt()
{
	return m_vObjectArray.size();
}

void ObjectManager :: ClearObject()
{
	for(int i=0; i<getObjectCnt(); i++)
 		delete m_vObjectArray[i];
	
	m_vObjectArray.clear();
}

void ObjectManager :: DeleteFreeStateObject()
{
	for(int i=0; i<getObjectCnt(); i++)
	{
		if(SearchObject(i)->getObjectState() == OBJECT_STATE_FREE)
		{
			RemoveObject(i);
			i--;
		}
	}
}

int ObjectManager :: getDeleteDragonCnt()
{
	return m_DragonDeleteCnt;
}

void ObjectManager :: setDeleteDragonCnt(short Num)
{
	m_DragonDeleteCnt = Num;
}

void ObjectManager :: Run()
{
	for(int Cnt = 0; Cnt < m_vObjectArray.size(); Cnt++)
	{
		//충돌 확인
		for(int Cnt2 = 0; Cnt2 < m_vObjectArray.size(); Cnt2++)
		{
			if(Cnt == Cnt2)
				continue;

			ObjectBase *pObject = SearchObject(Cnt);
			ObjectBase *pSearchObject = SearchObject(Cnt2);

			if(EPN_CheckCollision(EPN_Rect(pObject->getObjectInfo()->Pos) + pObject->getObjectInfo()->CollisionRect, 
				EPN_Rect(pSearchObject->getObjectInfo()->Pos) + pSearchObject->getObjectInfo()->CollisionRect)
				&& pObject->getObjectInfo()->IsCheckedFlag == false && pSearchObject->getObjectInfo()->IsCheckedFlag == false)
			{
				pObject->IsCollided(pSearchObject);
			}
		}


		ObjectBase *pObject = SearchObject(Cnt);
		if(pObject->getObjectType() == OBJECT_DRAGON && pObject->getObjectState() == OBJECT_STATE_DRAGON_DIE)
			m_DragonDeleteCnt++;


		//커맨드에따라 생성 or 삭제
		vector<Command> ObjectCmd;
		ObjectCmd = m_vObjectArray[Cnt]->Run();

		for(int CmdCnt = 0; CmdCnt < ObjectCmd.size(); ++CmdCnt)
		{
			switch(ObjectCmd[CmdCnt].CommandType)
			{
			case COMMAND_CREATE:
				AddObject(ObjectCmd[CmdCnt].pCreateObject);
				break;
			case COMMAND_DELETE:
				RemoveObject(Cnt);
				Cnt--;
				break;
			}
		}


		if(Cnt != -1)
		{
			EPN_Pos ObjectPos = m_vObjectArray[Cnt]->getObjectInfo()->Pos;
			EPN_Rect ObjectCollisionRect = m_vObjectArray[Cnt]->getObjectInfo()->CollisionRect;

			if(!EPN_CheckCollision(EPN_Rect(0,0,600,700) , EPN_Rect(ObjectPos) + ObjectCollisionRect))
			{
				RemoveObject(Cnt);
				Cnt--;
			}
		}
	}
}