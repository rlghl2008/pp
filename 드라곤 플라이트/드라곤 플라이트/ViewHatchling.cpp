#include "ViewHatchling.h"
#include "SoundManager.h"

ViewHatchling :: ViewHatchling() 
	: m_ManagerFlag(false), m_HatchlingType(HATCHLING_NULL), m_ButtonRunFlag(true), m_SellCheckFlag(false),
	  m_HatchlingClearFlag(false)
{	
	m_SellPrice[HATCHLING_FIRE] = 50;
	m_SellPrice[HATCHLING_DARK] = 25000;
	m_SellPrice[HATCHLING_LIGHTNING] = 500000;

	m_YesNoCheck.setScaling(EPN_Scaling(1, 2));
	m_YesNoCheck.setPos(EPN_Pos(0, 150));

	EPN_Rect BtnLargeSize = pEPN_TI->getTextureSize(L"btn_large");
	EPN_Rect BtnMediumSize = pEPN_TI->getTextureSize(L"btn_medium");

	//ȭ�� ������ ��ư
	m_HatchlingButton[HATCHLING_FIRE].setPos(EPN_Pos(43, 525));
	m_HatchlingButton[HATCHLING_FIRE].setTexture(L"btn_large", UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_FIRE].setRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_FIRE].setTextureRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_FIRE].setColor(EPN_ARGB(255, 237, 239, 192), UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_FIRE].setColor(EPN_ARGB(255, 237, 250, 210), UI_STATE_ACTIVE);
	m_HatchlingButton[HATCHLING_FIRE].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_HatchlingButton[HATCHLING_FIRE].setFont(NULL);
	m_HatchlingButton[HATCHLING_FIRE].setScaling(EPN_Scaling(1.1, 1.1));
	m_HatchlingButton[HATCHLING_FIRE].setSound("dragon_click", UI_STATE_POP);

	//��� ������ ��ư
	m_HatchlingButton[HATCHLING_DARK].setPos(EPN_Pos(220, 525));
	m_HatchlingButton[HATCHLING_DARK].setTexture(L"btn_large", UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_DARK].setRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_DARK].setTextureRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_DARK].setColor(EPN_ARGB(255, 237, 239, 192), UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_DARK].setColor(EPN_ARGB(255, 237, 250, 210), UI_STATE_ACTIVE);
	m_HatchlingButton[HATCHLING_DARK].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_HatchlingButton[HATCHLING_DARK].setFont(NULL);
	m_HatchlingButton[HATCHLING_DARK].setScaling(EPN_Scaling(1.1, 1.1));
	m_HatchlingButton[HATCHLING_DARK].setSound("dragon_click", UI_STATE_POP);

	//���� ������ ��ư
	m_HatchlingButton[HATCHLING_LIGHTNING].setPos(EPN_Pos(397, 525));
	m_HatchlingButton[HATCHLING_LIGHTNING].setTexture(L"btn_large", UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_LIGHTNING].setRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_LIGHTNING].setTextureRect(BtnLargeSize);
	m_HatchlingButton[HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 237, 239, 192), UI_STATE_ALL);
	m_HatchlingButton[HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 237, 250, 210), UI_STATE_ACTIVE);
	m_HatchlingButton[HATCHLING_LIGHTNING].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_HatchlingButton[HATCHLING_LIGHTNING].setFont(NULL);
	m_HatchlingButton[HATCHLING_LIGHTNING].setScaling(EPN_Scaling(1.1, 1.1));
	m_HatchlingButton[HATCHLING_LIGHTNING].setSound("dragon_click", UI_STATE_POP);

	//X��ư
	m_Button[BUTTON_ESC].setPos(EPN_Pos(510, 90));
	m_Button[BUTTON_ESC].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_ESC].setRect(BtnMediumSize);
	m_Button[BUTTON_ESC].setTextureRect(BtnMediumSize);
	m_Button[BUTTON_ESC].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_ESC].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_ESC].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_ESC].setFont(NULL);
	m_Button[BUTTON_ESC].setScaling(EPN_Scaling(0.7, 0.7));
	m_Button[BUTTON_ESC].setSound("button_click", UI_STATE_POP);

	//������ ���� ���� ��ư
	m_Button[BUTTON_LEFT].setPos(EPN_Pos(30, 560));
	m_Button[BUTTON_LEFT].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_LEFT].setRect(BtnMediumSize);
	m_Button[BUTTON_LEFT].setTextureRect(BtnMediumSize);
	m_Button[BUTTON_LEFT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_LEFT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_LEFT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_LEFT].setFont(NULL);
	m_Button[BUTTON_LEFT].setScaling(EPN_Scaling(1, 1));

	//������ ������ ���� ��ư
	m_Button[BUTTON_RIGHT].setPos(EPN_Pos(170, 560));
	m_Button[BUTTON_RIGHT].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_RIGHT].setRect(BtnMediumSize);
	m_Button[BUTTON_RIGHT].setTextureRect(BtnMediumSize);
	m_Button[BUTTON_RIGHT].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_RIGHT].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_RIGHT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_RIGHT].setFont(NULL);
	m_Button[BUTTON_RIGHT].setScaling(EPN_Scaling(1, 1));

	//���� �������� �� ��ư
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setPos(EPN_Pos(22, 350));
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setTexture(L"btn_large", UI_STATE_ALL);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setRect(BtnLargeSize);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setTextureRect(BtnLargeSize);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setColor(EPN_ARGB(255, 237, 239, 192), UI_STATE_ALL);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setColor(EPN_ARGB(255, 237, 250, 210), UI_STATE_ACTIVE);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setFont(NULL);
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_EQUIP_HATCHLING_LEFT].setSound("button_click", UI_STATE_POP);

	//������ �������� �� ��ư
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setPos(EPN_Pos(430, 350));
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setTexture(L"btn_large", UI_STATE_ALL);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setRect(BtnLargeSize);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setTextureRect(BtnLargeSize);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setColor(EPN_ARGB(255, 237, 239, 192), UI_STATE_ALL);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setColor(EPN_ARGB(255, 237, 250, 210), UI_STATE_ACTIVE);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setFont(NULL);
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].setSound("button_click", UI_STATE_POP);

	//�Ǹ� ��ư
	m_Button[BUTTON_SELL].setPos(EPN_Pos(450, 560));
	m_Button[BUTTON_SELL].setTexture(L"btn_medium", UI_STATE_ALL);
	m_Button[BUTTON_SELL].setRect(BtnMediumSize);
	m_Button[BUTTON_SELL].setTextureRect(BtnMediumSize);
	m_Button[BUTTON_SELL].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_SELL].setColor(EPN_ARGB(255, 253, 250, 108), UI_STATE_ACTIVE);
	m_Button[BUTTON_SELL].setColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_PUSH);
	m_Button[BUTTON_SELL].setFont(NULL);
	m_Button[BUTTON_SELL].setScaling(EPN_Scaling(1, 1));
	m_Button[BUTTON_SELL].setSound("button_click", UI_STATE_POP);

	//���ư��� ��ư
	m_Button[BUTTON_BACK].setTexture(pEPN_TI->getTextureIndex(L"back"), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setPos(EPN_Pos(10, 50));
	m_Button[BUTTON_BACK].setRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setTextureRect(pEPN_TI->getTextureSize(L"back"));
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 253, 200, 108), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 200, 255, 200), UI_STATE_ACTIVE);
	m_Button[BUTTON_BACK].setColor(EPN_ARGB(255, 100, 100, 100), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setFont(NULL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 255, 255, 255), UI_STATE_ALL);
	m_Button[BUTTON_BACK].setFontColor(EPN_ARGB(255, 0, 0, 0), UI_STATE_PUSH);
	m_Button[BUTTON_BACK].setScaling(EPN_Scaling(0.5, 0.5));
	m_Button[BUTTON_BACK].setSound("button_click", UI_STATE_POP);
}

ViewHatchling :: ~ViewHatchling()
{
	m_pPlayerData->SavePlayerData();
}

void ViewHatchling :: SellHatchling(short HatchlingType)
{
	//�Ǹ� ��ư ������ Ȯ��â
	CHECK_TYPE Check = m_YesNoCheck.Run(YESNO);
	pEPN_FI->BltText(L"�ٸ�����25", E_TEXT("���� �������� �Ǹ��Ͻðڽ��ϱ�?\n         �ǸŰ��� : ") + E_TEXT(m_SellPrice[HatchlingType]) + L"��", EPN_Rect(140, 270, 400, 350), EPN_ARGB(255, 255, 255, 255));

	if(Check == YES)
	{
		//���� ���� ���� ���� ��
		if(m_pPlayerData->getEquipHatchling(false) == HatchlingType)
			m_pPlayerData->setEquipHatchling(HATCHLING_NULL, false);

		//������ ���� ���� ���� ��  
		else if(m_pPlayerData->getEquipHatchling(true) == HatchlingType)
			m_pPlayerData->setEquipHatchling(HATCHLING_NULL, true);

		m_pPlayerData->setHatchlingState(HatchlingType, false);
		m_pPlayerData->PlusMoney(m_SellPrice[HatchlingType]);

		m_ManagerFlag = false;
		m_SellCheckFlag = false;
		m_ButtonRunFlag = true;

		pSoundManager->Play("dragon_sell");
	}
	else if(Check == NO)
		m_SellCheckFlag = false;
}

void ViewHatchling :: DrawEquipHatchling(bool Direction)
{
	if(m_pPlayerData->getEquipHatchling(Direction) == HATCHLING_NULL)
		return;
	
	estring EquipHatchling;
	EPN_Pos Pos;

	switch(m_pPlayerData->getEquipHatchling(Direction))
	{
	case HATCHLING_FIRE:
		EquipHatchling = L"hatchling_fire_s";
		break;
	case HATCHLING_DARK:
		EquipHatchling = L"hatchling_dark_s";
		break;
	case HATCHLING_LIGHTNING:
		EquipHatchling = L"hatchling_lightning_s";
		break;
	}

	if(!Direction)
		Pos = EPN_Pos(25, 353);
	else
		Pos = EPN_Pos(433, 353);
	
	pEPN_TI->Blt(EquipHatchling, Pos, pEPN_TI->getTextureSize(EquipHatchling), NULL, 0, EPN_Scaling(1, 1));
}

void ViewHatchling :: ClearHatchling(bool Direction)
{
	short EquipHatchlingState = m_pPlayerData->getEquipHatchling(Direction);

	//�������� �������� ���� ��
	if(EquipHatchlingState == HATCHLING_NULL)
	{
		CHECK_TYPE Check = m_YesNoCheck.Run(YES);

		pEPN_FI->BltText(L"�ٸ�����30", L"�������� �������� �����ϴ�.", EPN_Rect(130, 290, 400, 400), EPN_ARGB(255, 255, 255, 255));

		if(Check == YES)
			m_HatchlingClearFlag = false;
	}
	else
	{
		CHECK_TYPE Check = m_YesNoCheck.Run(YESNO);

		pEPN_FI->BltText(L"�ٸ�����30", L"  ������ ��������\n���� �Ͻðڽ��ϱ�?", EPN_Rect(190, 260, 400, 300), EPN_ARGB(255, 255, 255, 255));

		if(Check == YES)
		{
			m_pPlayerData->setEquipHatchling(HATCHLING_NULL, Direction);
			m_HatchlingClearFlag = false;
			pSoundManager->Play("equip");
		}
		else if(Check == NO)
			m_HatchlingClearFlag = false;
	}

}

void ViewHatchling :: HatchlingManager(short HatchlingType)
{
	EPN_Rect HatchlingSize = pEPN_TI->getTextureSize(L"pet"); 
	EPN_Rect ArrowSize = pEPN_TI->getTextureSize(L"back");

	//�������� �����ϰ� ���� ���� ��
	if(m_pPlayerData->getHatchlingState(HatchlingType) == false)
	{
		if(m_YesNoCheck.Run(YES) == YES)
			m_ManagerFlag = false;
		pEPN_FI->BltText(L"�ٸ�����30", L"�����ϰ� ���� ���� ������ �Դϴ�.", EPN_Rect(89, 290, 450, 330), EPN_ARGB(255, 255, 255, 255));
	}
	//�����ϰ� ���� ��
	else
	{
		//�˾�â ������ ��ư�۵� ����
		m_ButtonRunFlag = false;
		//�� �ؽ����̸�, ���̸�, �꼳��
		estring HatchlingTexture, HatchlingName, HatchlingText;
		

		//������ �Ϸ���Ʈ ����
		if(HatchlingType == HATCHLING_FIRE)
		{
			HatchlingTexture = L"hatchling_fire_";
			HatchlingName = L"�ǹ�����";
			HatchlingText = L"�ǹ�����";
		}
		else if(HatchlingType == HATCHLING_DARK)
		{
			HatchlingTexture = L"hatchling_dark_";
			HatchlingName = L"�ֹ���";
			HatchlingText = L"����� �����ϱ���. \n        \"��  ��\"";
		}
		else if(HatchlingType == HATCHLING_LIGHTNING)
		{
			HatchlingTexture = L"hatchling_lightning_";
			HatchlingName = L"������";
			HatchlingText = L"�߾ƿ�?";
		}


		//���̽�
		pEPN_TI->Blt(L"window", EPN_Pos(-320, 100), pEPN_TI->getTextureSize(L"window"), EPN_ARGB(255, 42, 69, 116), 0, EPN_Scaling(2, 4));
		pEPN_TI->Blt(L"dot", EPN_Pos(20, 530), pEPN_TI->getTextureSize(L"dot"), EPN_ARGB(255, 143, 129, 68), 0, EPN_Scaling(0.95, 1));

		//������ �̸�, �ؽ�Ʈ, �׸�
		pEPN_TI->Blt(L"explain_base", EPN_Pos(280, 181), pEPN_TI->getTextureSize(L"explain_base"), EPN_ARGB(100, 0, 0, 0), 0, EPN_Scaling(4, 4));
		pEPN_TI->Blt(HatchlingTexture, EPN_Pos(10, 170), pEPN_TI->getTextureSize(HatchlingTexture), NULL, 0, EPN_Scaling(1, 1));
		pEPN_FI->BltText(L"�ٸ�����30", HatchlingName, EPN_Rect(100, 430, 200, 480), EPN_ARGB(255, 255, 255, 255));
		pEPN_FI->BltText(L"�ٸ�����30", HatchlingText, EPN_Rect(295, 200, 500, 500), EPN_ARGB(255, 255, 255, 255));



		//���� ���� ��ư
		m_Button[BUTTON_LEFT].Run();
		pEPN_TI->Blt(L"pet", EPN_Pos(63 , 575), HatchlingSize, NULL, 0, EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"back", EPN_Pos(33, 600), ArrowSize, NULL, 0, EPN_Scaling(0.45, 0.45));
		pEPN_FI->BltText(L"�ٸ�����20", L"���ʺ���", EPN_Rect(50, 643, 120, 670));


		//������ ���� ��ư
		m_Button[BUTTON_RIGHT].Run();
		pEPN_TI->Blt(L"pet", EPN_Pos(175, 575), HatchlingSize, NULL, 0, EPN_Scaling(1, 1));
		pEPN_TI->Blt(L"back", EPN_Pos(235, 600), ArrowSize, NULL, 0, EPN_Scaling(0.45, 0.45), true);
		pEPN_FI->BltText(L"�ٸ�����20", L"�����ʺ���", EPN_Rect(183, 643, 270, 670));


		//�Ǹ� ��ư
		m_Button[BUTTON_SELL].Run();
		pEPN_TI->Blt(L"sell", EPN_Pos(452, 555), pEPN_TI->getTextureSize(L"sell"), NULL, 0, EPN_Scaling(1.1, 1.1));
		pEPN_FI->BltText(L"�ٸ�����20", L"�Ǹ�", EPN_Rect(489, 643, 530, 670));


		//�ݱ� ��ư
		m_Button[BUTTON_ESC].Run();
		pEPN_TI->Blt(L"no", EPN_Pos(520, 100), pEPN_TI->getTextureSize(L"no"), NULL, 0, EPN_Scaling(0.63, 0.63));




		//���� ���� ��ư ���� ��
		if(m_Button[BUTTON_LEFT].getState() == UI_STATE_POP)
		{
			pSoundManager->Play("equip");

			m_ManagerFlag = false;
			m_ButtonRunFlag = true;
			m_pPlayerData->setEquipHatchling(HatchlingType, false);
			
		}

		//������ ���� ��ư ���� ��
		else if(m_Button[BUTTON_RIGHT].getState() == UI_STATE_POP)
		{		
			pSoundManager->Play("equip");

			m_ManagerFlag = false;
			m_ButtonRunFlag = true;
			m_pPlayerData->setEquipHatchling(HatchlingType, true);
		}

		//�Ǹ� ��ư ���� ��
		else if(m_Button[BUTTON_SELL].getState() == UI_STATE_POP)
			m_SellCheckFlag = true;

		//�ݱ��ư ���� ��
		else if(m_Button[BUTTON_ESC].getState() == UI_STATE_POP)
		{
			m_ManagerFlag = false;
			m_ButtonRunFlag = true;
		}



		//�Ǹ�Ȯ��
		if(m_SellCheckFlag)
			SellHatchling(HatchlingType);
	}
}

VIEWSTATE ViewHatchling :: Run()
{
	//���
	Background :: CreateInstance()->Run();

	//�÷��̾� �Ӵ�
	m_pPlayerData->PlayerMoneyRun();



	//����
	pEPN_TI->Blt(L"dot", EPN_Pos(20, 500), pEPN_TI->getTextureSize(L"dot"), EPN_ARGB(255, 143, 129, 68), 0, EPN_Scaling(0.95, 1));




	if(m_ButtonRunFlag)
	{
		//ȭ���Ӽ� ������
		m_HatchlingButton[HATCHLING_FIRE].Run();
		pEPN_TI->Blt(L"hatchling_fire_s", EPN_Pos(53, 537), pEPN_TI->getTextureSize(L"hatchling_fire_s"));


		//��ҼӼ� ������
		m_HatchlingButton[HATCHLING_DARK].Run();
		pEPN_TI->Blt(L"hatchling_dark_s", EPN_Pos(233, 535), pEPN_TI->getTextureSize(L"hatchling_dark_s"));


		//�����Ӽ� ������
		m_HatchlingButton[HATCHLING_LIGHTNING].Run();
		pEPN_TI->Blt(L"hatchling_lightning_s", EPN_Pos(413, 537), pEPN_TI->getTextureSize(L"hatchling_lightning_s"));


		EPN_Rect NestSize = pEPN_TI->getTextureSize(L"nest");

		//���ʿ� �������� ������ ��ư
		m_Button[BUTTON_EQUIP_HATCHLING_LEFT].Run();
		pEPN_TI->Blt(L"nest", EPN_Pos(43, 435), NestSize, NULL, 0, EPN_Scaling(0.8, 0.8));
		DrawEquipHatchling(false);

		//�����ʿ� �������� ������ ��ư
		m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].Run();
		pEPN_TI->Blt(L"nest", EPN_Pos(452, 435), NestSize, NULL, 0, EPN_Scaling(0.8, 0.8));
		DrawEquipHatchling(true);



		//�����ʿ� �������� ������ ��ư ������ �� 
		if(m_Button[BUTTON_EQUIP_HATCHLING_LEFT].getState() == UI_STATE_POP)
		{
			m_HatchlingClearFlag = true;
			m_Direction = false;
		}
		//���ʿ� �������� ������ ��ư ������ �� 
		else if(m_Button[BUTTON_EQUIP_HATCHLING_RIGHT].getState() == UI_STATE_POP)
		{
			m_HatchlingClearFlag = true;
			m_Direction = true;
		}
	}

	if(m_HatchlingClearFlag)
		ClearHatchling(m_Direction);

	//�� ������ ��ư ������
	for(short HatchlingType=0; HatchlingType<HATCHLING_MAX; HatchlingType++)
	{
		if(m_HatchlingButton[HatchlingType].getState() == UI_STATE_POP)
		{
			m_HatchlingType = HatchlingType;
			m_ManagerFlag = true;
		}
	}



	//�� �Ŵ���
	if(m_ManagerFlag)
		HatchlingManager(m_HatchlingType);



	//���ư��� ��ư
	m_Button[BUTTON_BACK].Run();

	if(m_Button[BUTTON_BACK].getState() == UI_STATE_POP)
		return VIEW_MENU;
	return VIEW_HATCHLING;
}