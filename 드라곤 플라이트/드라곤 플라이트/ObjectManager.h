#pragma once
#include "Character.h"
#include "Dragon.h"
#include <vector>

class ObjectManager
{
public:
	ObjectManager(void);
	~ObjectManager(void);

private:
	vector<ObjectBase*> m_vObjectArray;

private:
	short m_DragonDeleteCnt;

public:
	void AddObject(ObjectBase * pObject);
	void InsertObject(ObjectBase * pObject, int Index);
	bool RemoveObject(int Index);

	ObjectBase* SearchObject(int Index);
	int getObjectCnt();
	void ClearObject();
	void DeleteFreeStateObject();

	int getDeleteDragonCnt();
	void setDeleteDragonCnt(short Num);

public:
	void Run();
};