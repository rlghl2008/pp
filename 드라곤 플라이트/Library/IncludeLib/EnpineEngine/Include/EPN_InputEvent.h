#ifndef _EPN_INPUTEVENT_H_
#define _EPN_INPUTEVENT_H_

#include <Windows.h>
#include "EPN_Global.h"

#ifdef DLL_EPN_INPUTEVENT_CLASS
	#define DLLTYPE_EPN_INPUTEVENT __declspec(dllexport)
#else
	#define DLLTYPE_EPN_INPUTEVENT __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_KeyCode.h"
#include "EPN_InfoPrint.h"

enum
{
	EPN_EVENT_DOWN = 1,
	EPN_EVENT_DOWNING = 2,
	EPN_EVENT_UP = 3
};
class DLLTYPE_EPN_INPUTEVENT EPN_InputEvent
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
private:
	short KeyBoardStat[EPN_KEY_MAXCOUNT];
	short KeyBoardCheckStat[EPN_KEY_MAXCOUNT];
	EPN_Pos MousePos;
	EPN_Pos CheckMousePos;
	short MouseStatL;
	short MouseStatM;
	short MouseStatR;
	short CheckMouseStatL;
	short CheckMouseStatM;
	short CheckMouseStatR;

	
	EPN_Pos MouseMove;
	EPN_Pos MouseMoveL;
	EPN_Pos MouseMoveM;
	EPN_Pos MouseMoveR;
	EPN_Pos MovePosSave;
	EPN_Pos MovePosSaveL;
	EPN_Pos MovePosSaveM;
	EPN_Pos MovePosSaveR;
	BOOL MouseMoveFlag;
	short MouseMoveFlagL;
	short MouseMoveFlagM;
	short MouseMoveFlagR;

	BOOL CheckMouseWheelUp;
	BOOL CheckMouseWheelDown;
	BOOL MouseWheelUp;
	BOOL MouseWheelDown;

	BOOL KeyInputActiveFlag;//키입력처리 사용유무 TRUE 사용 FALSE 사용중지 셋터겟터 함수로 변경가능 기본값 TRUE
	BOOL MouseInputActiveFlag;//마우스처리 사용유무 TRUE 사용 FALSE 사용중지 셋터겟터 함수로 변경가능 기본값 TRUE

	BOOL MouseWindowPosFlag;//마우스좌푶를 디바이스기준으로 밖까지 좌표인식할지말지 기본 TRUE 이고 FALSE시 마우스 화면 밖으로 나갔을때 좌표 인식안함
private://내부 프로세스 메서드
	void InputKeyProcess(int Key = 0);
	void InputMouseProcess();
public:
	EPN_InputEvent(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_InputEvent();
public:
	BOOL EventRun();
public://사용스위치 변경
	void setKeyInputActiveFlag(BOOL Flag);
	void setMouseInputActiveFlag(BOOL Flag);

	BOOL getKeyInputActiveFlag();
	BOOL getMouseInputActiveFlag();

public://접근 메서드 
	//키보드 처리
	void InputForKey(MSG WinMsg);
	void InputForKey(short DeviceIndex);
	void ClearKeyStat(short Stat = 0);//키보드 입력상태 초기화
	
	//키입력 처리 비교
	/* 반환값
		EPN_EVENT_DOWN = 1,
		EPN_EVENT_DOWNING = 2,
		EPN_EVENT_UP = 3
	*/
	short IF_KEY_STATE(int EPN_KeyCode);
	BOOL IF_KEY_STATE(int EPN_KeyCode,short Stat);
	//전체키중 아무키나 눌렸을경우 TRUE반환
	BOOL isKeyInput();

	//마우스 처리
	void InputForMouse(MSG WinMsg, HWND hwnd);
	void InputForMouse(short DeviceIndex);
	EPN_Pos getMousePos();
	float getMousePosX();
	float getMousePosY();
	void ClearMouseStat();//마우스 입력상태 초기화
	//마우스 입력상태 얻기 L 왼쪽 R 오른쪽
	/* 반환값
		EPN_EVENT_DOWN = 1,
		EPN_EVENT_DOWNING = 2,
		EPN_EVENT_UP = 3
	*/
	short getMouseStatL();
	short getMouseStatM();
	short getMouseStatR();

	EPN_Pos getMouseMove();
	EPN_Pos getMouseMoveL();
	EPN_Pos getMouseMoveM();
	EPN_Pos getMouseMoveR();

	BOOL getMouseWheelUp();
	BOOL getMouseWheelDown();

	void setMouseWindowPosFlag(BOOL Flag);
	BOOL getMouseWindowPosFlag();
private:
	static EPN_InputEvent* m_pEPN_InputEvent;
public:
	static EPN_InputEvent* getInstance();
	static EPN_InputEvent* CreateInstance(void);
};

#endif