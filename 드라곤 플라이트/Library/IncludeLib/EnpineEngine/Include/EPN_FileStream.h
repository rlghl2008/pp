#ifndef _ULSAN_GUARD_FILE_STREAM_
#define _ULSAN_GUARD_FILE_STREAM_

#ifdef DLL_EPN_FILESTREAM_CLASS
#define DLLTYPE_EPN_FILESTREAM __declspec(dllexport)
#else
#define DLLTYPE_EPN_FILESTREAM __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#include <iostream>
#include <windows.h>

#include "EPN_Global.h"

namespace EPN
{
	enum FILE_STREAM_FLAG
	{
		CREATE_IS_NULL,		//파일이 없을 경우만 생성
		CREATE_IS_ALWAYS,	//항상 새 파일을 생성
		OPEN_IS_EXISTING,	//파일이 존재할 경우만 오픈
	};

	enum FILE_ACCESS_FLAG
	{
		FILE_ACCESS_READ,	//읽기 전용
		FILE_ACCESS_WRITE,	//쓰기 전용
		FILE_ACCESS_RW,		//읽기 쓰기 전용
		FILE_ACCESS_ADD,	//붙여 쓰기 전용
	};

};


class DLLTYPE_EPN_FILESTREAM EPN_FileStream
{
private:
	FILE* m_pFile;
	bool m_CryptFlag;
	DWORD m_BufferSize;
	BYTE* m_pBuffer;

	char m_FileFullName[128];
	char m_FileName[64];
	char m_FileExtension[64];
public:
	EPN_FileStream(bool UseCryptFlag = false);
	~EPN_FileStream(void);
public:
	bool AccessFile(const char* Path,
		EPN::FILE_ACCESS_FLAG AccessFlag,
		EPN::FILE_STREAM_FLAG StreamFlag = EPN::OPEN_IS_EXISTING, bool BinaryModeFlag = true);

	bool CloseFile();

	bool Write(char* pData, size_t Size = 0, size_t* pResult = NULL);
	bool WriteFormat(char* pFormat, ...);
	int Read(char* pData, size_t Size = 0, size_t* pResult = NULL);

public:
	FILE* getFilePtr();
	bool setFilePos(long Pos, int Flag = SEEK_SET);
	long getFilePos();
	long getFileSize();
	int IsReadEnd();
	const char* getFileFullName();
	const char* getFileName();
	const char* getFileExtension();
public:
	bool ReadSentence(char* pDest, int DestSize, bool DecryptFlag = false);
	int FindSentence(char* Str, int Size = -1, bool LineCheckFlag = true, bool DecryptFlag = false);
	int FindData(BYTE Data, bool DecryptFlag = false);
	int FindDataArray(BYTE* pData, int Size, bool DecryptFlag = false);
public:
	void setCryptFlag(bool Flag);
	bool getCryptFlag();
};

#endif