#ifndef _EPN_DX_FONT_H_
#define _EPN_DX_FONT_H_

#ifdef DLL_EPN_FONT_CLASS
	#define DLLTYPE_EPN_FONT __declspec(dllexport)
#else
	#define DLLTYPE_EPN_FONT __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#include "EPN_Font.h"

class DLLTYPE_EPN_FONT EPN_DXFont : public EPN_Font
{
private:
	LPDIRECT3DDEVICE9 DX_FontDevice;// 렌더링에 사용될 D3D 디바이스
private:
	D3DXFONT_DESC FontInfo;
	ID3DXFont *IDFont;
	HFONT hFont;
	HDC TextureHDC;
	LPDIRECT3DSURFACE9 pFontSurface; 
	short FontTextureIndex;
public:
	EPN_DXFont(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_DXFont();
public:
	virtual BOOL BltText(estring Text,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),int Buffer = -1);
	virtual BOOL BltText(short DestTextureIndex, estring Text,EPN_Rect Rect,EPN_ARGB FontColor = EPN_ARGB(255,0,0,0), EPN_ARGB BackColor = EPN_ARGB(0,0,0,0),int Buffer = -1);
public:
	virtual BOOL setFont(void* RenderDevice,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
	virtual EPN_FontData getFont();
	virtual BOOL setFontName(estring FontName);
	virtual estring getFontName();
	virtual BOOL setFontSize(int FontSize);
	virtual int getFontSize();
	virtual BOOL setFontThickness(int FontThickness);
	virtual int getFontThickness();

	virtual BOOL ClearFont(short DeviceIndex = -1);
	virtual BOOL ResetFont(void* RenderDevice,short DeviceIndex = -1);

	virtual EPN_Pos getETextAreaSize(estring EText, BOOL EndlnFlag = FALSE);
	
};

#endif