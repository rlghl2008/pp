﻿#ifndef _EPN_GLOBAL_H_
#define _EPN_GLOBAL_H_
#include "EPN_DXGlobal.h"
#include "EPN_MP.hpp"

#include <iostream>
#include <windows.h>


#define DLLAPI __declspec(dllexport)//구조체관련DLL변환

#define DLLEXPORT_EPN_POS
DLLEXPORT_EPN_POS struct DLLAPI _EPN_Pos;

#define DLLEXPORT_EPN_SCALING
DLLEXPORT_EPN_SCALING struct DLLAPI _EPN_Scaling;

#define DLLEXPORT_EPN_RECT
DLLEXPORT_EPN_RECT struct DLLAPI _EPN_Rect;

#define DLLEXPORT_EPN_ARGB
DLLEXPORT_EPN_ARGB struct DLLAPI _EPN_ARGB;

#define DLLEXPORT_EPN_CIRCLE
DLLEXPORT_EPN_CIRCLE struct DLLAPI _EPN_Circle;

#ifdef DLL_EPN_POLYGON_CLASS
	#define DLLTYPE_EPN_POLYGON __declspec(dllexport)
#else
	#define DLLTYPE_EPN_POLYGON __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#pragma warning (disable : 4251)



typedef struct _EPN_Pos
{
	float X;
	float Y;
	_EPN_Pos(int nullval = 0):X(nullval),Y(nullval){}
	_EPN_Pos(float _X,float _Y):X(_X),Y(_Y){}
	_EPN_Pos(const _EPN_Pos& Pos):X(Pos.X),Y(Pos.Y){}
	
	void CastValue_Int()
	{
		X = (int)X;
		Y = (int)Y;
	}

	_EPN_Pos operator =(const _EPN_Pos &Pos) 
	{
		return _EPN_Pos(X = Pos.X,Y = Pos.Y);
	}
	_EPN_Pos operator +(const _EPN_Pos &Pos) const 
	{
		return _EPN_Pos(X + Pos.X,Y + Pos.Y);
	}
	_EPN_Pos operator -(const _EPN_Pos &Pos) const 
	{
		return _EPN_Pos(X - Pos.X,Y - Pos.Y);
	}
	_EPN_Pos operator /(const _EPN_Pos &Pos) const 
	{
		return _EPN_Pos(X / Pos.X,Y / Pos.Y);
	}
	_EPN_Pos operator *(const _EPN_Pos &Pos) const 
	{
		return _EPN_Pos(X * Pos.X,Y * Pos.Y);
	}
	_EPN_Pos operator %(const _EPN_Pos &Pos) const 
	{
		return _EPN_Pos((int)X % (int)Pos.X,(int)Y % (int)Pos.Y);
	}

	_EPN_Pos operator +=(const _EPN_Pos &Pos) 
	{
		return _EPN_Pos(X += Pos.X,Y += Pos.Y);
	}
	_EPN_Pos operator -=(const _EPN_Pos &Pos) 
	{
		return _EPN_Pos(X -= Pos.X,Y -= Pos.Y);
	}
	_EPN_Pos operator /=(const _EPN_Pos &Pos)  
	{
		return _EPN_Pos(X /= Pos.X,Y /= Pos.Y);
	}
	_EPN_Pos operator *=(const _EPN_Pos &Pos) 
	{
		return _EPN_Pos(X *= Pos.X,Y *= Pos.Y);
	}
	_EPN_Pos operator %=(const _EPN_Pos &Pos) 
	{
		return _EPN_Pos(X = (int)X % (int)Pos.X,Y = (int)Y % (int)Pos.Y);
	}
	BOOL operator ==(const _EPN_Pos &Pos) 
	{
		if(X == Pos.X && Y == Pos.Y)
			return TRUE;
		return FALSE;
	}
	BOOL operator !=(const _EPN_Pos &Pos) 
	{
		if(X == Pos.X && Y == Pos.Y)
			return FALSE;
		return TRUE;
	}

} EPN_Pos;

typedef struct _EPN_Scaling
{
	float X;
	float Y;
	_EPN_Scaling(int nullval = 0):X(nullval),Y(nullval){}
	_EPN_Scaling(float _X,float _Y):X(_X),Y(_Y){}
	_EPN_Scaling(const _EPN_Scaling& Scal):X(Scal.X),Y(Scal.Y){}

	void CastValue_Int()
	{
		X = (int)X;
		Y = (int)Y;
	}

	_EPN_Scaling operator =(const _EPN_Scaling &Scal) 
	{
		return _EPN_Scaling(X = Scal.X,Y = Scal.Y);
	}
	_EPN_Scaling operator +(const _EPN_Scaling &Scal) const 
	{
		return _EPN_Scaling(X + Scal.X,Y + Scal.Y);
	}
	_EPN_Scaling operator -(const _EPN_Scaling &Scal) const 
	{
		return _EPN_Scaling(X - Scal.X,Y - Scal.Y);
	}
	_EPN_Scaling operator /(const _EPN_Scaling &Scal) const 
	{
		return _EPN_Scaling(X / Scal.X,Y / Scal.Y);
	}
	_EPN_Scaling operator *(const _EPN_Scaling &Scal) const 
	{
		return _EPN_Scaling(X * Scal.X,Y * Scal.Y);
	}
	_EPN_Scaling operator %(const _EPN_Scaling &Scal) const 
	{
		return _EPN_Scaling((int)X % (int)Scal.X,(int)Y % (int)Scal.Y);
	}

	_EPN_Scaling operator +=(const _EPN_Scaling &Scal) 
	{
		return _EPN_Scaling(X += Scal.X,Y += Scal.Y);
	}
	_EPN_Scaling operator -=(const _EPN_Scaling &Scal) 
	{
		return _EPN_Scaling(X -= Scal.X,Y -= Scal.Y);
	}
	_EPN_Scaling operator /=(const _EPN_Scaling &Scal)  
	{
		return _EPN_Scaling(X /= Scal.X,Y /= Scal.Y);
	}
	_EPN_Scaling operator *=(const _EPN_Scaling &Scal) 
	{
		return _EPN_Scaling(X *= Scal.X,Y *= Scal.Y);
	}
	_EPN_Scaling operator %=(const _EPN_Scaling &Scal) 
	{
		return _EPN_Scaling(X = (int)X % (int)Scal.X,Y = (int)Y % (int)Scal.Y);
	}
	BOOL operator ==(const _EPN_Scaling &Scal) 
	{
		if(X == Scal.X && Y == Scal.Y)
			return TRUE;
		return FALSE;
	}
	BOOL operator !=(const _EPN_Scaling &Scal) 
	{
		if(X == Scal.X && Y == Scal.Y)
			return FALSE;
		return TRUE;
	}

} EPN_Scaling;

typedef struct _EPN_Rect
{
	_EPN_Pos Top;
	_EPN_Pos Bottom;
	_EPN_Rect(int nullval = 0):Top(_EPN_Pos()),Bottom(_EPN_Pos())
	{
	}
	_EPN_Rect(float X,float Y,float W,float H)
	{
		Top.X = X;
		Top.Y = Y;
		Bottom.X = W;
		Bottom.Y = H;
	}

	_EPN_Rect(_EPN_Pos Top,_EPN_Pos Bottom)
	{
		this->Top = Top;
		this->Bottom = Bottom;
	}

	_EPN_Rect(_EPN_Pos Pos)
	{
		this->Top = Pos;
		this->Bottom = Pos;
	}
	_EPN_Rect(_EPN_Scaling Scal)
	{
		this->Top.X = Scal.X;
		this->Top.Y = Scal.Y;
		this->Bottom.X = Scal.X;
		this->Bottom.Y = Scal.Y;
	}

	void CastValue_Int()
	{
		Top.CastValue_Int();
		Bottom.CastValue_Int();
	}

	_EPN_Rect operator +(const _EPN_Rect &Rect) const 
	{
		return _EPN_Rect(Top + Rect.Top,Bottom + Rect.Bottom);
	}
	_EPN_Rect operator -(const _EPN_Rect &Rect) const 
	{
		return _EPN_Rect(Top - Rect.Top,Bottom - Rect.Bottom);
	}
	_EPN_Rect operator /(const _EPN_Rect &Rect) const 
	{
		return _EPN_Rect(Top / Rect.Top,Bottom / Rect.Bottom);
	}
	_EPN_Rect operator *(const _EPN_Rect &Rect) const 
	{
		return _EPN_Rect(Top * Rect.Top,Bottom * Rect.Bottom);
	}
	_EPN_Rect operator %(const _EPN_Rect &Rect) const 
	{
		return _EPN_Rect(Top % Rect.Top,Bottom % Rect.Bottom);
	}

	_EPN_Rect operator +=(const _EPN_Rect &Rect) 
	{
		return _EPN_Rect(Top += Rect.Top,Bottom += Rect.Bottom);
	}
	_EPN_Rect operator -=(const _EPN_Rect &Rect) 
	{
		return _EPN_Rect(Top -= Rect.Top,Bottom -= Rect.Bottom);
	}
	_EPN_Rect operator /=(const _EPN_Rect &Rect)  
	{
		return _EPN_Rect(Top /= Rect.Top,Bottom /= Rect.Bottom);
	}
	_EPN_Rect operator *=(const _EPN_Rect &Rect) 
	{
		return _EPN_Rect(Top *= Rect.Top,Bottom *= Rect.Bottom);
	}
	_EPN_Rect operator %=(const _EPN_Rect &Rect) 
	{
		return _EPN_Rect(Top %= Rect.Top,Bottom %= Rect.Bottom);
	}


	BOOL operator ==(const _EPN_Rect &Rect) 
	{
		if(Top == Rect.Top && Bottom == Rect.Bottom)
			return TRUE;
		return FALSE;
	}
	BOOL operator !=(const _EPN_Rect &Rect) 
	{
		if(Top == Rect.Top && Bottom == Rect.Bottom)
			return FALSE;
		return TRUE;
	}
} EPN_Rect;

typedef struct _EPN_ARGB
{
	float Alpha;
	float Red;
	float Green;
	float Blue;
	_EPN_ARGB(int nullval = 0):Alpha(255),Red(255),Green(255),Blue(255){}
	_EPN_ARGB(float A,float R,float G,float B):Alpha(A),Red(R),Green(G),Blue(B){}
	_EPN_ARGB(float R,float G,float B):Alpha(0),Red(R),Green(G),Blue(B){}

	void Repair()
	{
		if(Alpha < 0)
			Alpha = 0;
		else if(Alpha > 255)
			Alpha = 255;
		if(Red < 0)
			Red = 0;
		else if(Red > 255)
			Red = 255;
		if(Green < 0)
			Green = 0;
		else if(Green > 255)
			Green = 255;
		if(Blue < 0)
			Blue = 0;
		else if(Blue > 255)
			Blue = 255;
	}
	_EPN_ARGB operator +(const _EPN_ARGB &ARGB) const 
	{
		return _EPN_ARGB(Alpha + ARGB.Alpha, Red + ARGB.Red,Green + ARGB.Green,Blue+ ARGB.Blue);
	}
	_EPN_ARGB operator -(const _EPN_ARGB &ARGB) const 
	{
		return _EPN_ARGB(Alpha - ARGB.Alpha, Red - ARGB.Red,Green - ARGB.Green,Blue- ARGB.Blue);
	}
	_EPN_ARGB operator /(const _EPN_ARGB &ARGB) const 
	{
		return _EPN_ARGB(Alpha / ARGB.Alpha, Red / ARGB.Red,Green / ARGB.Green,Blue/ ARGB.Blue);
	}
	_EPN_ARGB operator *(const _EPN_ARGB &ARGB) const 
	{
		return _EPN_ARGB(Alpha * ARGB.Alpha, Red * ARGB.Red,Green * ARGB.Green,Blue* ARGB.Blue);
	}

	_EPN_ARGB operator +=(const _EPN_ARGB &ARGB) 
	{
		return _EPN_ARGB(Alpha += ARGB.Alpha, Red += ARGB.Red,Green += ARGB.Green,Blue+= ARGB.Blue);
	}
	_EPN_ARGB operator -=(const _EPN_ARGB &ARGB) 
	{
		return _EPN_ARGB(Alpha -= ARGB.Alpha, Red -= ARGB.Red,Green -= ARGB.Green,Blue-= ARGB.Blue);
	}
	_EPN_ARGB operator /=(const _EPN_ARGB &ARGB)  
	{
		return _EPN_ARGB(Alpha /= ARGB.Alpha, Red /= ARGB.Red,Green /= ARGB.Green,Blue/= ARGB.Blue);
	}
	_EPN_ARGB operator *=(const _EPN_ARGB &ARGB) 
	{
		return _EPN_ARGB(Alpha *= ARGB.Alpha, Red *= ARGB.Red,Green *= ARGB.Green,Blue*= ARGB.Blue);
	}

	BOOL operator ==(const _EPN_ARGB &ARGB) 
	{
		if(Alpha == ARGB.Alpha && Red == ARGB.Red && Blue == ARGB.Blue && Green == ARGB.Green)
			return TRUE;
		return FALSE;
	}
} EPN_ARGB;


typedef struct _EPN_Circle
{
	_EPN_Pos CirclePos;
	double CircleRadius;

	_EPN_Circle(int nullval = 0):CirclePos(0),CircleRadius(0){}
	_EPN_Circle(_EPN_Pos Pos,double Radius):CirclePos(Pos),CircleRadius(Radius){}


} EPN_Circle;



class DLLTYPE_EPN_POLYGON EPN_Polygon
{
private:
	EPN_MP<EPN_Pos> VertexPos;
private:
	int VertexNum;
	EPN_Pos PolygonPos;
	float PolygonAngle;
	EPN_Scaling PolygonScal;
	bool WidthReversFlag;
	bool EditFlag;
	EPN_Pos PolygonCenterPos;
public:
	EPN_Polygon(int VertexNum = 0);
	EPN_Polygon(const EPN_Polygon &Polygon);
	EPN_Polygon(EPN_Rect Rect);
	~EPN_Polygon();
private:
	void _setPolygonConvert();
	void _ReversWidth();
public:
	void Clear();

	void setVertexNum(int VertexNum);
	int getVertexNum() const;

	void setPolygonPos(EPN_Pos Pos);
	EPN_Pos getPolygonPos() const;

	void setWidthReversFlag(bool Flag);
	bool getWidthReversFlag() const;

	void setPolygonAngle(float Angle);
	float getPolygonAngle() const;

	void setPolygonCenterPos(EPN_Pos Pos);
	EPN_Pos getPolygonCenterPos() const;

	void setPolygonScal(EPN_Scaling Scal);
	EPN_Scaling getPolygonScal() const;

	bool setVertexPos(int Index, EPN_Pos Pos);
	EPN_Pos getVertexPos(int Index, bool PolygonPosFlag = true) const;
	EPN_Pos getConvertVertexPos(int Index, bool PolygonPosFlag = true);
	
public:
	EPN_Polygon operator =(const EPN_Polygon &Polygon) 
	{
		Clear();
		EditFlag = true;
		PolygonPos = Polygon.getPolygonPos();
		VertexNum = Polygon.getVertexNum();
		PolygonAngle = 0;
		PolygonScal = EPN_Scaling(1.f,1.f);
		WidthReversFlag = Polygon.getWidthReversFlag();

		if(this->VertexNum > 0)
		{
			VertexPos.PoolSizeSet(VertexNum*2);
			for(int i = 0; i < this->VertexNum*2; ++i)
			{
				if(i < this->VertexNum)
				{
					VertexPos.InsertData(new EPN_Pos(Polygon.getVertexPos(i,false)));
					PolygonCenterPos.X += (*VertexPos.SearchElement(i)).X;
					PolygonCenterPos.Y += (*VertexPos.SearchElement(i)).Y;
				}
				else
					VertexPos.InsertData(new EPN_Pos(Polygon.getVertexPos(VertexNum-i,false)));
			}
			PolygonCenterPos.X /= (float)VertexNum;
			PolygonCenterPos.Y /= (float)VertexNum;
		}

		return (*this);
	}
	EPN_Polygon operator +(const EPN_Pos &Pos) const 
	{
		EPN_Polygon ResultPolygon((*this));
		ResultPolygon.setPolygonPos(getPolygonPos()+Pos);
		ResultPolygon.setPolygonCenterPos(getPolygonCenterPos());
		return ResultPolygon;
	}
	EPN_Polygon operator -(const EPN_Pos &Pos) const 
	{
		EPN_Polygon ResultPolygon((*this));
		ResultPolygon.setPolygonPos(getPolygonPos()-Pos);
		ResultPolygon.setPolygonCenterPos(getPolygonCenterPos());
		return ResultPolygon;
	}
	EPN_Polygon operator /(const EPN_Pos &Pos) const 
	{
		EPN_Polygon ResultPolygon((*this));
		ResultPolygon.setPolygonPos(getPolygonPos()/Pos);
		ResultPolygon.setPolygonCenterPos(getPolygonCenterPos());
		return ResultPolygon;
	}
	EPN_Polygon operator *(const EPN_Pos &Pos) const 
	{
		EPN_Polygon ResultPolygon((*this));
		ResultPolygon.setPolygonPos(getPolygonPos()*Pos);
		ResultPolygon.setPolygonCenterPos(getPolygonCenterPos());
		return ResultPolygon;
	}


	EPN_Polygon operator +=(const EPN_Pos &Pos) 
	{
		setPolygonPos(getPolygonPos()+Pos);
		return (*this);
	}
	EPN_Polygon operator -=(const EPN_Pos &Pos) 
	{
		setPolygonPos(getPolygonPos()-Pos);
		return (*this);
	}
	EPN_Polygon operator /=(const EPN_Pos &Pos) 
	{
		setPolygonPos(getPolygonPos()/Pos);
		return (*this);
	}
	EPN_Polygon operator *=(const EPN_Pos &Pos) 
	{
		setPolygonPos(getPolygonPos()*Pos);
		return (*this);
	}


};



#endif