#ifndef _EPN_ACTION_H_
#define _EPN_ACTION_H_

#ifdef DLL_EPN_ACTION_CLASS
	#define DLLTYPE_EPN_ACTION __declspec(dllexport)
#else
	#define DLLTYPE_EPN_ACTION __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
using namespace std;
#include <windows.h>


#include "EPN_Global.h"
#include "EPN_Crypto.h"
#include "EPN_InfoPrint.h"
#include "EPN_CString.h"

struct EPN_ActionData
{
	EPN_Pos Pos;
	EPN_Rect Rect;
	EPN_ARGB Color;
	float Angle;
	EPN_Scaling Scaling;
	int DelayCount;

	EPN_ActionData(EPN_Pos Pos = 0, EPN_Rect Rect = 0, EPN_ARGB Color = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = 0, int DelayCount = 0)
	{
		this->Pos = Pos;
		this->Rect = Rect;
		this->Color = Color;
		this->Angle = Angle;
		this->Scaling = Scaling;
		this->DelayCount = DelayCount;
	}
};


class DLLTYPE_EPN_ACTION EPN_Action
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
	EPN_Crypto Crypto;
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	EPN_ActionData* ActionData;

	BOOL ActionFlag;
	int ActionDataCount;
private:
	int ActionDelayCount;
	int ActionNum;//현재 반환스프라이트 번호
public:
	EPN_Action(BOOL InfoPrintFlag = FALSE);
	~EPN_Action();
public:
	//직접 영역을 선택하여 원하는 기준으로 자동으로 자르고 그개수만큼 액션데이터 생성후 각각 렉트값 저장
	BOOL autoActionRect(EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);

	//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로 자르고 그개수만큼 액션데이터 생성후 각각 렉트값 저장
	BOOL autoActionRect(short TextureIndex, DWORD ScopeW, DWORD ScopeH);
	BOOL autoActionRect(estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);
	
	//툴로작업한 액션데이터 파일로드
	BOOL LoadActionData(estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력

	//만들어진 액션데이터 개수 가져오기
	int getActionDataCount();
	void setActionDataCount(int MaxValue);

	//액션기능 딜레이 카운터에 맞춰 자동으로 넘겨서 액션데이터를 반환해줌
	EPN_ActionData* Action(int DelayCount);
	EPN_ActionData* Action(int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionNum(int ActionNum);
	int getActionNum();
	int getActionDelayCount();

	//입력한 번호에 데이터 설정
	BOOL setActionData(DWORD Number,EPN_ActionData ActionData);
	//입력한 번호에 저장되어있는 데이터 가져오기
	EPN_ActionData* getActionData(DWORD Number);

	//입력한 번호에 위치 설정
	BOOL setActionPos(DWORD Number,EPN_Pos Pos);
	//입력한 번호에 저장되어있는 위치 가져오기
	EPN_Pos getActionPos(DWORD Number);

	//입력한 번호에 렉트영역 설정
	BOOL setActionRect(DWORD Number,EPN_Rect Rect);
	//입력한 번호에 저장되어있는 렉트영역 가져오기
	EPN_Rect getActionRect(DWORD Number);

	//입력한 번호에 ARGB Color 설정
	BOOL setActionColor(DWORD Number,EPN_ARGB Color);
	//입력한 번호에 ARGB Color 가져오기
	EPN_ARGB getActionColor(DWORD Number);

	//입력한 번호에 각도 설정
	BOOL setActionAngle(DWORD Number,float Angle);
	//입력한 번호에 각도 가져오기
	float getActionAngle(DWORD Number);

	//입력한 번호에 확대/축소 비율 설정
	BOOL setActionScaling(DWORD Number,EPN_Scaling Scaling);
	//입력한 번호에 확대/축소 비율 가져오기
	EPN_Scaling getActionScaling(DWORD Number);
	

	BOOL ClearAction();
};


#endif