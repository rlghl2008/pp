#ifndef _EPN_ACTIONINTERFACE_H_
#define _EPN_ACTIONINTERFACE_H_

#ifdef DLL_EPN_ACTIONINTERFACE_CLASS
	#define DLLTYPE_EPN_ACTIONINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_ACTIONINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_Action.h"
#include "EPN_InfoPrint.h"
#include "EPN_MP.hpp"
#include "EPN_SISave.h"
#include "EPN_CString_T.h"

class DLLTYPE_EPN_ACTIONINTERFACE EPN_ActionInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 입력된 모든 스프라이트 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만 변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
public:
	EPN_MP<EPN_Action> *EPN_MP_Action;
	EPN_SISave *EPN_Save;
public:
	EPN_ActionInterface();
	virtual ~EPN_ActionInterface();
public:
	//직접 영역을 선택하여 원하는 기준으로 자동으로 잘라 저장후 저장된 인덱스 반환 실패시 -1 반환

	//기준영역 Rect,잘라낼 가로,세로크기(픽셀기준)
	//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환
	short autoActionRect(EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);
	//식별번호 저장할 문자열,기준영역 Rect,잘라낼 가로,세로크기(픽셀기준)
	//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환 식별 문자열로도 검색가능
	short autoActionRect(estring SaveActionName,EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);

	//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로자름
	short autoActionRect(short TextureIndex, DWORD ScopeW, DWORD ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
	short autoActionRect(estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름
	
	short autoActionRect(estring SaveActionName,short TextureIndex, DWORD ScopeW, DWORD ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
	short autoActionRect(estring SaveActionName,estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름

	//툴로작업한 스프라이트데이터 파일을 불러와 값저장
	short LoadActionData(estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력
	short LoadActionData(estring SaveActionName,estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력


public://EPN_SISave 객체적응
	short getActionIndex(estring SaveActionName);
	estring getActionStr(short SaveActionName);

public://EPN_MP 객체적응
	EPN_Action* getAction(short ActionIndex);//인덱스로 찾아낸 스프라이트 가져오기
	EPN_Action* getAction(estring SaveActionName);//스트링으로 찾아낸 스프라이트 가져오기
	BOOL DeleteAction(short ActionIndex);
	BOOL DeleteAction(estring SaveActionName);
	BOOL AllDeleteAction();
	int getInsertCount();//현재 인터페이스에 셋되어 메모리에 올라가있는 스프라이트 개수 반환
public:
	//수동으로 액션데이터 설정
	BOOL setActionData(short ActionIndex,DWORD Number,EPN_ActionData ActionData);
	BOOL setActionData(estring SaveActionName,DWORD Number,EPN_ActionData ActionData);
	EPN_ActionData* getActionData(short ActionIndex, DWORD Number);
	EPN_ActionData* getActionData(estring SaveActionName, DWORD Number);

	BOOL setActionPos(short ActionIndex,DWORD Number,EPN_Pos Pos);
	BOOL setActionPos(estring SaveActionName,DWORD Number,EPN_Pos Pos);
	EPN_Pos getActionPos(short ActionIndex, DWORD Number);
	EPN_Pos getActionPos(estring SaveActionName, DWORD Number);

	BOOL setActionRect(short ActionIndex,DWORD Number,EPN_Rect Rect);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
	BOOL setActionRect(estring SaveActionName,DWORD Number,EPN_Rect Rect);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
	EPN_Rect getActionRect(short ActionIndex, DWORD Number);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기
	EPN_Rect getActionRect(estring SaveActionName, DWORD Number);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기

	BOOL setActionColor(short ActionIndex,DWORD Number,EPN_ARGB Color);
	BOOL setActionColor(estring SaveActionName,DWORD Number,EPN_ARGB Color);
	EPN_ARGB getActionColor(short ActionIndex, DWORD Number);
	EPN_ARGB getActionColor(estring SaveActionName, DWORD Number);

	BOOL setActionAngle(short ActionIndex,DWORD Number,float Angle);
	BOOL setActionAngle(estring SaveActionName,DWORD Number,float Angle);
	float getActionAngle(short ActionIndex, DWORD Number);
	float getActionAngle(estring SaveActionName, DWORD Number);

	BOOL setActionScaling(short ActionIndex,DWORD Number,EPN_Scaling Scaling);
	BOOL setActionScaling(estring SaveActionName,DWORD Number,EPN_Scaling Scaling);
	EPN_Scaling getActionScaling(short ActionIndex, DWORD Number);
	EPN_Scaling getActionScaling(estring SaveActionName, DWORD Number);

	int getActionDataCount(short ActionIndex);//인덱스로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 가져오기
	int getActionDataCount(estring SaveActionName);//스트링으로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 가져오기
	void setActionDataCount(short ActionIndex,int MaxValue);//인덱스로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 설정
	void setActionDataCount(estring SaveActionName,int MaxValue);//스트링으로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 설정

	//액션기능
	EPN_ActionData* Action(short ActionIndex, int DelayCount);
	EPN_ActionData* Action(short ActionIndex, int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionNum(short ActionIndex, int ActionNum);
	int getActionNum(short ActionIndex);
	int getActionDelayCount(short ActionIndex);
	EPN_ActionData* Action(estring SaveActionName, int DelayCount);
	EPN_ActionData* Action(estring SaveActionName, int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionNum(estring SaveActionName, int ActionNum);
	int getActionNum(estring SaveActionName);
	int getActionDelayCount(estring SaveActionName);

public:
	BOOL ClearAction(short ActionIndex);
	BOOL ClearAction(estring SaveActionName);
	BOOL AllClearAction();

private:
	static EPN_ActionInterface* m_pActionInterface;
public:
	static EPN_ActionInterface* getInstance();
	static EPN_ActionInterface* CreateInstance(void);


};

#endif