#ifndef _EPN_PARTICLEINTERFACE_H_
#define _EPN_PARTICLEINTERFACE_H_

#ifdef DLL_EPN_PARTICLEINTERFACE_CLASS
	#define DLLTYPE_EPN_PARTICLEINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_PARTICLEINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_MP.hpp"
#include "EPN_SISave.h"
#include "EPN_ParticleManager.h"


class DLLTYPE_EPN_PARTICLEINTERFACE EPN_ParticleInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 로드된 모든 디바이스의 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
private:
	EPN_MP<EPN_ParticleManager> *EPN_MP_PMgr;
	EPN_SISave *EPN_Save;
public://EPN_SISave 객체적응
	short getManagerIndex(estring SaveUImgrName);
	estring getManagerStr(short UImgrIndex);
public:
	EPN_ParticleInterface();
	~EPN_ParticleInterface();
public:
	short AddParticleManager(int MaxParticleNum);
	short AddParticleManager(estring SaveParticleMgrName,int MaxParticleNum);

	//기본값시 사용하는 파티클매니저 전부Run, Pos 는 파티클 출력위치 기준
	BOOL Run(int ParticleMgrIndex = -1, EPN_Pos Pos = EPN_Pos(0,0));
	BOOL Run(estring SaveParticleMgrName, EPN_Pos Pos = EPN_Pos(0,0));
public:
	//현재 인터페이스에 Add되어 메모리에 올라가있는 ParticleManager 개수 반환
	int getInsertCount();

	BOOL AllDeleteManager();
	BOOL DeleteManager(short ParticleMgrIndex);
	BOOL DeleteManager(estring SaveParticleMgrName);

	BOOL AllClearManager();
	BOOL ClearManager(short ParticleMgrIndex);
	BOOL ClearManager(estring SaveParticleMgrName);

	//인덱스로 찾아낸  가져오기
	EPN_ParticleManager* getManager(short ParticleMgrIndex);
	//스트링으로 찾아낸 파티클매니저 가져오기
	EPN_ParticleManager* getManager(estring ParticleMgrName);

public://매니저기능
	//인덱스에해당하는 매니저에대해 최대 파티클 개수 설정 
	BOOL AllsetMaxParticleNum(int ParticleNum);
	BOOL setMaxParticleNum(short ParticleMgrIndex,int ParticleNum);
	BOOL setMaxParticleNum(estring ParticleMgrName,int ParticleNum);
	//인덱스에해당하는 매니저에대해 설정된 최대 파티클 개수 반환
	BOOL getMaxParticleNum(short ParticleMgrIndex);
	BOOL getMaxParticleNum(estring ParticleMgrName);
	//인덱스에해당하는 매니저에대해 최대 파티클 개수 재설정
	BOOL AllresetMaxParticleNum(int ParticleNum);
	BOOL ResetMaxParticleNum(short ParticleMgrIndex,int ParticleNum);
	BOOL ResetMaxParticleNum(estring ParticleMgrName,int ParticleNum);
	//인덱스에해당하는 매니저에대해 설정된 파티클중 죽은파티클번호 찾아 재설정하고 해당 파티클인덱스를 반환 죽은파티클이없을시 -1반환
	short AddParticle(short ParticleMgrIndex);
	short AddParticle(estring ParticleMgrName);

private:
	static EPN_ParticleInterface* m_pParticleInterface;
public:
	static EPN_ParticleInterface* getInstance();
	static EPN_ParticleInterface* CreateInstance();
};

#endif