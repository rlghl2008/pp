#ifndef _EPN_PARTICLE_H_
#define _EPN_PARTICLE_H_

#ifdef DLL_EPN_PARTICLE_CLASS
	#define DLLTYPE_EPN_PARTICLE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_PARTICLE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_ActionInterface.h"
#include "EPN_TextureInterface.h"
#include "EPN_BasicFunction.h"
#include "EPN_CString_T.h"


class DLLTYPE_EPN_PARTICLE EPN_Particle
{
private:
	enum{MAX_AFTER_BLT_COUNT = 256};//잔상이 존재할수있는 최대개수 사용자가 수정가능
private:
	EPN_TextureInterface *EPN_TI;
	EPN_ActionInterface *pEPN_AI;
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
	public://get set mathod
EPN_InfoPrint* getInfoPrint();
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private://기본기능
	BOOL ParticleDieFlag;//파티클이 살아있으면 FALSE 죽었으면 TRUE (TRUE시 Run 동작안함) 기본값 TRUE
	BOOL DeleteCountFlag;//DeleteCount 가 0이될시 파티클 자동소멸기능 사용여부
	int DeleteCount;//파티클이 살아있을수있는 카운트수

	EPN_Pos ParticlePos;//파티클 위치
	EPN_Pos ParticleMove;//파티클이 실제 이동하는 힘의 정도

	float MoveAngle;//파티클이 움직일 각도방향
	float MoveSpeed;//파티클이 움직일 속도기준값
	BOOL AlterMoveAngleFlag;//파티클 움직일 각도방향 변화기능 사용여부
	float AlterMoveAngle;//매카운트마다 각도방향에 변화줄값
	BOOL AlterMoveSpeedFlag;//파티클 움직일 속도 변화기능 사용여부
	float AlterMoveSpeed;//매카운트마다 속도에변화를 줄값

	short TextureIndex;//출력할텍스쳐 식별인덱스
	float TextureAngle;//텍스쳐 출력 앵글
	EPN_Scaling TextureScaling;//텍스쳐 출력 확대/축소 율

	BOOL AlterTextureAngleFlag;//텍스쳐 출력앵글 변화기능 사용여부
	float AlterTextureAngle;//텍스쳐 출력앵글 매카운트마다 변화값
	BOOL AlterTextureScalingFlag;//텍스쳐 확대/축소 변화기능 사용여부
	EPN_Scaling AlterTextureScaling;//텍스쳐 확대/축소 율 매카운트마다 변화값

	EPN_Rect TextureRect;//출력할텍스쳐 사이즈(기본 텍스쳐설정시 이미지전체크기로 설정됨)
	BOOL AlterTextureRectFlag;//텍스쳐 출력사이즈 변화기능 사용여부
	EPN_Rect AlterTextureRect;//텍스쳐 출력사이즈 매카운트마다 변화값

	EPN_ARGB TextureColor;//출력할 텍스쳐 컬러
	BOOL AlterTextureColorFlag;//텍스쳐 컬러변화 기능 사용여부
	EPN_ARGB AlterTextureColor;//출력할텍스쳐 컬러 매카운트마다 변화값

	BOOL SpriteFlag;//스프라이트 기능 사용여부
	short SpriteIndex;//사용할 스프라이트 식별인덱스
	short SpriteDelay;//애니메이션 넘기는 딜레이기준( 예 : 5일시 5번의 Run실행마다 카운트가 넘어감 )

	BOOL ParticleSTOP;//기본 false값 이며 true일시 출력을제외한 모든처리가 정지됩니다

private://추가기능
	BOOL AfterBltFlag;//파티클 잔상효과 사용여부
	int AfterImageNum;//파티클 최대잔상개수
	int AfterImageCount;//파티클 잔상을 몇카운트마다 생성할것인가
	int AfterImageCountRun;
	int AfterImageDeleteCount;//잔상이 존재할수있는 카운트(낮을수록 빨리사라지고 높을수록 천천히 투명화됩니다)
	BOOL AfterParticleDieFlag[MAX_AFTER_BLT_COUNT];//파티클 잔상소멸여부
	int AfterParticleCount;//파티클 잔상 생성개수
	int AfterParticleDieCount[MAX_AFTER_BLT_COUNT];//파티클 잔상소멸카운트
	EPN_Pos AfterParticlePos[MAX_AFTER_BLT_COUNT];//파티클 잔상위치
	short AfterTextureIndex[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 식별자
	float AfterTextureAngle[MAX_AFTER_BLT_COUNT];//파티클 잔상 출력각도
	EPN_Scaling AfterTextureScaling[MAX_AFTER_BLT_COUNT];//파티클 잔상 출력 확대/축소 율
	EPN_Rect AfterTextureRect[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 출력사이즈
	EPN_ARGB AfterTextureColor[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 컬러
	EPN_ARGB AfterTextureColorSave[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 컬러


	BOOL WindUseFlag;//바람부는기능 사용여부
	float WindAngle;//바람이부는각도(0도가 위기준이며 90도일시 바람은 -> 방향으로 불어옵니다)
	float WindPower;//바람의세기

	BOOL GravityFlag;//중력적용여부( 자동으로 가속도가 적용됨 )
	float GravityAngle;//중력이 작용하는 방향각도
	float GravityMaxPower;//중력의최대힘
	float GravitySpeed;//현재중력에의한 속도
	float GravitySecCount;//1초의 기준카운트 60이면 60카운트마다 1초로취급
	float GravityCount;

	BOOL MagnetFlag;//자력사용여부( 자력이 발생하는 위치에가까울수록 큰힘이 작용합니다 하지만 MagnetRadius 가 마이너스값으로 설정되있으면 위치와상관없이 같은힘이 작용합니다 )
	EPN_Pos MagnetPos;//자력 발생 지점 가까울수록 큰힘이 작용됨
	float MagnetPower;//자력의힘 반대방향으로 적용시키고싶을시 - 값으로 설정하면 됩니다
	float MagnetRadius;//자력의힘이 영향을미칠 원의범위에대한 반지름길이(범위없이 무조건 작용한다면 마이너스값 셋팅)

public:
	EPN_Particle(BOOL InfoPrintFlag = FALSE);
	~EPN_Particle();

public://..Run
	BOOL Run(EPN_Pos Pos = EPN_Pos(0,0));
	BOOL Clear();
public:

	BOOL setParticleDieFlag(BOOL ParticleDieFlag);
	BOOL getParticleDieFlag();

	BOOL setDeleteCountFlag(BOOL DeleteCountFlag);
	BOOL getDeleteCountFlag();

	BOOL setDeleteCount(int DeleteCount);
	int getDeleteCount();

	BOOL setParticlePos(EPN_Pos ParticlePos);
	EPN_Pos getParticlePos();

	BOOL setParticleMove(EPN_Pos ParticleMove);
	EPN_Pos getParticleMove();

	BOOL setMoveAngle(float MoveAngle);
	float getMoveAngle();

	BOOL setMoveSpeed(float MoveSpeed);
	float getMoveSpeed();

	BOOL setAlterMoveAngleFlag(BOOL AlterMoveAngleFlag);
	BOOL getAlterMoveAngleFlag();

	BOOL setAlterMoveAngle(float AlterMoveAngle);
	float getAlterMoveAngle();

	BOOL setAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag);
	BOOL getAlterMoveSpeedFlag();

	BOOL setAlterMoveSpeed(float AlterMoveSpeed);
	float getAlterMoveSpeed();

	BOOL setTextureIndex(short TextureIndex);
	short getTextureIndex();

	BOOL setTextureAngle(float TextureAngle);
	float getTextureAngle();

	BOOL setTextureScaling(EPN_Scaling TextureScaling);
	EPN_Scaling getTextureScaling();

	BOOL setAlterTextureAngleFlag(BOOL AlterTextureAngleFlag);
	BOOL getAlterTextureAngleFlag();

	BOOL setAlterTextureAngle(float AlterTextureAngle);
	float getAlterTextureAngle();

	BOOL setAlterTextureScalingFlag(BOOL AlterTextureScalingFlag);
	BOOL getAlterTextureScalingFlag();

	BOOL setAlterTextureScaling(EPN_Scaling AlterTextureScaling);
	EPN_Scaling getAlterTextureScaling();

	BOOL setTextureRect(EPN_Rect TextureRect);
	EPN_Rect getTextureRect();

	BOOL setAlterTextureRectFlag(BOOL AlterTextureRectFlag);
	BOOL getAlterTextureRectFlag();

	BOOL setAlterTextureRect(EPN_Rect AlterTextureRect);
	EPN_Rect getAlterTextureRect();

	BOOL setTextureColor(EPN_ARGB TextureColor);
	EPN_ARGB getTextureColor();

	BOOL setAlterTextureColorFlag(BOOL AlterTextureColorFlag);
	BOOL getAlterTextureColorFlag();

	BOOL setAlterTextureColor(EPN_ARGB AlterTextureColor);
	EPN_ARGB getAlterTextureColor();



	BOOL setSpriteFlag(BOOL SpriteFlag);
	BOOL getSpriteFlag();

	BOOL setSpriteIndex(short SpriteIndex);
	short getSpriteIndex();

	BOOL setSpriteDelay(short SpriteDelay);
	short getSpriteDelay();


	BOOL setAfterBltFlag(BOOL AfterBltFlag);
	BOOL getAfterBltFlag();

	BOOL setAfterImageNum(int AfterBltFlag);
	int getAfterImageNum();

	BOOL setAfterImageCount(int AfterImageCount);
	int getAfterImageCount();

	BOOL setAfterImageDeleteCount(int AfterImageDeleteCount);
	int getAfterImageDeleteCount();
	

	BOOL setWindUseFlag(BOOL WindUseFlag);
	BOOL getWindUseFlag();

	BOOL setWindAngle(float WindAngle);
	float getWindAngle();

	BOOL setWindPower(float WindPower);
	float getWindPower();

	BOOL setGravityFlag(BOOL GravityFlag);
	BOOL getGravityFlag();

	BOOL setGravityAngle(float GravityAngle);
	float getGravityAngle();

	BOOL setGravityMaxPower(float GravityMaxPower);
	float getGravityMaxPower();

	BOOL setGravitySecCount(float GravitySecCount);
	float getGravitySecCount();
	
	BOOL resetGravitySpeed();

	BOOL setMagnetFlag(BOOL MagnetFlag);
	BOOL getMagnetFlag();

	BOOL setMagnetPos(EPN_Pos MagnetPos);
	EPN_Pos getMagnetPos();

	BOOL setMagnetPower(float MagnetPower);
	float getMagnetPower();

	BOOL setMagnetRadius(float MagnetRadius);
	float getMagnetRadius();

	BOOL setParticleStopFlag(BOOL ParticleStopFlag);
	BOOL getParticleStopFlag();
};

#endif