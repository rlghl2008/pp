
#ifndef _EPN_DX_DEVICE_H_
#define _EPN_DX_DEVICE_H_

#ifdef DLL_EPN_DXDEVICE_CLASS
	#define DLLTYPE_EPN_DXDEVICE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_DXDEVICE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#define DLLAPI __declspec(dllexport)//구조체관련DLL변환
#define DLLEXPORT_STRUET
DLLEXPORT_STRUET struct DLLAPI D3DXVECTOR2;

#include "EPN_Device.h"
#include "EPN_FPS.h"
class DLLTYPE_EPN_DXDEVICE EPN_DXDevice : public EPN_Device
{
private://DX
	LPDIRECT3DDEVICE9	m_pd3dDevice;	// 렌더링에 사용될 D3D 디바이스
    LPDIRECT3D9			m_pD3D;	// D3D 디바이스를 생성할 D3D 객체 변수
	LPD3DXSPRITE pSprite; // 렌더링에 사용될 스프라이트
	D3DPRESENT_PARAMETERS m_d3dpp;// 디바이스 생성에 사용할 구조체
	IDirect3DSurface9*		m_pSurface;
public://디바이스 기본 도형그리기에 사용
	D3DXVECTOR2 LineRect[4][2];//기본 도형그리기에 사용
	ID3DXLine *Line;
	BOOL BackBufferAlphaFlag;

private: //Device Option
	estring Form_Caption;
	BOOL Form_FullScreenFlag;
	EPN_Pos Form_Pos;
	EPN_Pos Form_Size;
	unsigned int Form_ExStyle;
	unsigned int Form_Style;
	EPN_Pos DeviceSize;
   
private://Win
    WNDCLASSEX			m_Wcex;
    HWND				m_hWnd;
	MSG					m_WinMsg;

private://Device statMember
	BOOL				m_Alive;
	BOOL BeginFlag;
public:
    EPN_DXDevice(BOOL InfoPrintFlag = FALSE);
    virtual ~EPN_DXDevice();

private:
    BOOL InitD3D( HWND _hWnd );
	
public://메서드
	virtual BOOL Run(BOOL Flag = TRUE);//TRUE 화면이 액티브시에만 작동 FALSE 화면이 액티브 상태가 아니더라도 작동
	virtual BOOL EventRun();

	virtual BOOL CreateDevice(estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	virtual BOOL CreateDeviceEx(estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);

	virtual BOOL ReCreateDevice();
	virtual BOOL ReCreateDevice(estring Caption,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW); 
	virtual BOOL ReCreateDeviceEx();
	virtual BOOL ReCreateDeviceEx(estring Caption,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW); 

	virtual BOOL ResetDevice();
	virtual BOOL ResetDevice(estring Caption,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL); 
	virtual BOOL ResetDeviceEx();
	virtual BOOL ResetDeviceEx(estring Caption,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL);

	virtual BOOL ClearDevice(void);
	
	virtual EPN_DEVICEVALIDITY DeviceValidity();
	virtual EPN_DEVICEVALIDITY BeginScene(EPN_ARGB BColor);
	virtual BOOL EndScene();

public://디바이스 기본 드로우기능
	virtual BOOL BltLine(EPN_Pos StartPos,EPN_Pos EndPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),float LineThickness = 1,BOOL AliasingFlag = FALSE);
	virtual BOOL BltQuadrangle(EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	virtual BOOL BltQuadrangle(EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_Pos FourthPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	virtual BOOL BltTriangle(EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);

public://접근 인터페이스
	virtual void KillDevice();
public://윈도우 메세지 관련
	virtual MSG getWinMsg();
	virtual void setWinMsg(MSG Msg);
	virtual HWND getHwnd();
	virtual WPARAM getWPARAM();
	virtual LPARAM getLPARAM();
	virtual UINT getMESSAGE();

	virtual BOOL IsMinimizeWindow();
	virtual BOOL setMinimizeWindow();
	virtual BOOL IsMaxmizeWindow() ;
	virtual BOOL setMaxmizeWindow();
	virtual BOOL IsFocusWindow();
	virtual BOOL setFocusWindow();
	virtual BOOL IsActivityWindow();//현재 디바이스 액티브 여부 반환
	virtual BOOL setActivityWindow(BOOL Flag = TRUE);//현재 디바이스를 액티브 시켜줍니다
	virtual BOOL setRestoreWindow();
public://디바이스 정보 관련
	virtual BOOL getAlive();
	virtual void* getRenderDevice();
	virtual void* getRenderData();

	virtual BOOL getBeginFlag();
	virtual void setBeginFlag(BOOL Flag);

	

	virtual estring getFormCaption();
	virtual BOOL setFormCaption(estring Caption);

	virtual EPN_Pos getFormPos();
	virtual BOOL setFormPos(EPN_Pos Pos);

	virtual float getFormPosX();
	virtual float getFormPosY();
	virtual BOOL setFormPosX(float PosX);
	virtual BOOL setFormPosY(float PosY);

	virtual EPN_Pos getFormSize();
	virtual int getFormSizeWidht();
	virtual int getFormSizeHeight();
	virtual BOOL setFormSize(EPN_Pos FormSize);
	virtual BOOL setFormSizeWidht(int Widht);
	virtual BOOL setFormSizeHeight(int Height);

	virtual EPN_Pos getDeviceSize();

	virtual unsigned int getFormStyle();
	virtual BOOL setFormStyle(unsigned int Style);

	virtual unsigned int getFormExStyle();
	virtual BOOL setFormExStyle(unsigned int ExStyle);

	virtual BOOL getFullScreenFlag();

};

#endif // _EPN_DEIVCE_H
