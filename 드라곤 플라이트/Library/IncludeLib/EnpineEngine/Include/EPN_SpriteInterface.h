#ifndef _EPN_SPRITEINTERFACE_H_
#define _EPN_SPRITEINTERFACE_H_

#ifdef DLL_EPN_SPRITEINTERFACE_CLASS
	#define DLLTYPE_EPN_SPRITEINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_SPRITEINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_Sprite.h"
#include "EPN_InfoPrint.h"
#include "EPN_MP.hpp"
#include "EPN_SISave.h"
#include "EPN_CString_T.h"

class DLLTYPE_EPN_SPRITEINTERFACE EPN_SpriteInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 입력된 모든 스프라이트 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만 변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
public:
	EPN_MP<EPN_Sprite> *EPN_MP_Sprite;
	EPN_SISave *EPN_Save;
public:
	EPN_SpriteInterface();
	virtual ~EPN_SpriteInterface();
public:
	//직접 영역을 선택하여 원하는 기준으로 자동으로 잘라 저장후 저장된 인덱스 반환 실패시 -1 반환

	//기준영역 Rect,잘라낼 가로,세로크기(픽셀기준)
	//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환
	short autoSprite(EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);
	//식별번호 저장할 문자열,기준영역 Rect,잘라낼 가로,세로크기(픽셀기준)
	//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환 식별 문자열로도 검색가능
	short autoSprite(estring SaveSpriteName,EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);

	//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로자름
	short autoSprite(short TextureIndex, DWORD ScopeW, DWORD ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
	short autoSprite(estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름
	
	short autoSprite(estring SaveSpriteName,short TextureIndex, DWORD ScopeW, DWORD ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
	short autoSprite(estring SaveSpriteName,estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름

	//툴로작업한 스프라이트데이터 파일을 불러와 값저장
	short autoSprite(estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력
	short autoSprite(estring SaveSpriteName,estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력


public://EPN_SISave 객체적응
	short getSpriteIndex(estring SaveTextureName);
	estring getSpriteStr(short TextureIndex);

public://EPN_MP 객체적응
	EPN_Sprite* getSprite(short SpriteIndex);//인덱스로 찾아낸 스프라이트 가져오기
	EPN_Sprite* getSprite(estring SaveSpriteName);//스트링으로 찾아낸 스프라이트 가져오기
	BOOL DeleteSprite(short SpriteIndex);
	BOOL DeleteSprite(estring SaveSpriteName);
	BOOL AllDeleteSprite();
	int getInsertCount();//현재 인터페이스에 셋되어 메모리에 올라가있는 스프라이트 개수 반환
public:
	//수동으로 스프라이트 렉트설정
	BOOL setSpriteRect(short SpriteIndex,DWORD Number,EPN_Rect Rect);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
	BOOL setSpriteRect(estring SaveSpriteName,DWORD Number,EPN_Rect Rect);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
	EPN_Rect getSpriteRect(short SpriteIndex, DWORD Number);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기
	EPN_Rect getSpriteRect(estring SaveSpriteName, DWORD Number);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기

	int getSpriteCount(short SpriteIndex);//인덱스로 찾아낸 스프라이트에서 영역의 개수 가져오기
	int getSpriteCount(estring SaveSpriteName);//스트링으로 찾아낸 스프라이트에서 영역의 개수 가져오기
	void setSpriteCount(short SpriteIndex,int MaxValue);//인덱스로 찾아낸 스프라이트에서 영역의 개수 설정하기
	void setSpriteCount(estring SaveSpriteName,int MaxValue);//스트링으로 찾아낸 스프라이트에서 영역의 개수 설정하기

	//액션기능
	EPN_Rect ActionSprite(short SpriteIndex, int DelayCount);
	EPN_Rect ActionSprite(short SpriteIndex, int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionSpriteNum(short SpriteIndex, int ActionSpriteNum);
	int getActionSpriteNum(short SpriteIndex);
	int getActionDelayCount(short SpriteIndex);
	EPN_Rect ActionSprite(estring SaveSpriteName, int DelayCount);
	EPN_Rect ActionSprite(estring SaveSpriteName, int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionSpriteNum(estring SaveSpriteName, int ActionSpriteNum);
	int getActionSpriteNum(estring SaveSpriteName);
	int getActionDelayCount(estring SaveSpriteName);

public:
	BOOL ClearSprite(short SpriteIndex);
	BOOL ClearSprite(estring SaveSpriteName);
	BOOL AllClearSprite();

private:
	static EPN_SpriteInterface* m_pSpriteInterface;
public:
	static EPN_SpriteInterface* getInstance();
	static EPN_SpriteInterface* CreateInstance(void);


};

#endif