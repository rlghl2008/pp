#ifndef _EPN_CSTRING_T_H_
#define _EPN_CSTRING_T_H_

#ifdef DLL_EPN_CSTRING_T_CLASS
	#define DLLTYPE_EPN_CSTRING_T __declspec(dllexport)
#else
	#define DLLTYPE_EPN_CSTRING_T __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
#include <windows.h>

#ifdef UNICODE
#define EPN_UNICODE_FLAG TRUE
	typedef std::wstring EPN_STRING;
	typedef wchar_t EPN_CHAR;
	#define estrcpy wcscpy
	#define estrcmp wcscmp
#else
#define EPN_UNICODE_FLAG FALSE
	typedef std::string EPN_STRING;
	typedef char EPN_CHAR;
	#define estrcpy strcpy
	#define estrcmp strcmp
#endif

#define DLLAPI __declspec(dllexport)//구조체관련DLL변환

#define DLLEXPORT_EPN_STRLEN
DLLEXPORT_EPN_STRLEN struct DLLAPI _EPN_Strlen;

typedef struct _EPN_Strlen
{
	int length;
	int StartIndex;
	int EndIndex;
	_EPN_Strlen()
	{
		length = 0;
		StartIndex = 0;
		EndIndex = 0;
	}
	_EPN_Strlen(int StartIndex, int EndIndex, int length)
	{
		this->length = length;
		this->StartIndex = StartIndex;
		this->EndIndex = EndIndex;
	}
} EPN_Strlen;

typedef class DLLTYPE_EPN_CSTRING_T EPN_CString_T//EPN_CString 클래스에서 정수,실수를 제외하고 문자 관련 처리만해서 _T 가붙음
{
private:
	enum{EPN_INPUT_STRLEN_MAX = 256};//문자열 받을때의 나눠서받는기준
	static BOOL UTF8_FLAG;
private:
	EPN_STRING EPN_Str;//컴파일러 설정이 유니코드면 wstring 멀티바이트면 string 으로 설정됨
	EPN_CHAR Savestr_t[EPN_INPUT_STRLEN_MAX];
public:
	EPN_CString_T();
	EPN_CString_T(const char c);
	EPN_CString_T(const char* str);
	EPN_CString_T(const std::string& str);
	EPN_CString_T(const wchar_t wc);
	EPN_CString_T(const wchar_t* wstr);
	EPN_CString_T(const std::wstring& wstr);
	EPN_CString_T(const EPN_CString_T& estr_t);
	virtual ~EPN_CString_T();
public:
	EPN_STRING EPN_TEXT(const char c);
	EPN_STRING EPN_TEXT(const char* str);
	EPN_STRING EPN_TEXT(const std::string& str);
	EPN_STRING EPN_TEXT(const wchar_t wc);
	EPN_STRING EPN_TEXT(const wchar_t* wstr);
	EPN_STRING EPN_TEXT(const std::wstring& wstr);
	EPN_STRING EPN_TEXT(const EPN_CString_T& estr_t);
	
public:
	const EPN_STRING getString() const;//컴파일러 설정이 유니코드면 wstring 멀티바이트면 string 으로 반환됨
	const std :: wstring getStrW();//무조건 유니코드형 얻기
	const std :: string getStrA();//무조건 멀티바이트형 얻기
	BOOL estrcat(EPN_CString_T estr,int length = -1);//현재 문자열중 length위치에 estr내용 이어붙이기
	
	EPN_CHAR at(int len);//컴파일러 설정이 유니코드면 wchar_t 멀티바이트면 char 으로 반환됨
	EPN_CString_T getMid(int startPos,int endPos);//시작과 끝Pos 위치 사이의 문자열을 반환
	const EPN_CHAR* c_str() const;//컴파일러 설정이 유니코드면 wchar_t* 멀티바이트면 char* 으로 반환됨
	int length() const;
	void Print() const;
	void Println() const;
	BOOL Clear();

public:
	EPN_CString_T operator =(const char c);
	EPN_CString_T operator =(const char* str);
	EPN_CString_T operator =(const std::string& str);
	EPN_CString_T operator =(const wchar_t wc);
	EPN_CString_T operator =(const wchar_t* wstr);
	EPN_CString_T operator =(const std::wstring& wstr);
	EPN_CString_T operator =(const EPN_CString_T& estr_t);
	EPN_CString_T operator +(const char c);
	EPN_CString_T operator +(const char* str);
	EPN_CString_T operator +(const std::string& str);
	EPN_CString_T operator +(const wchar_t wc);
	EPN_CString_T operator +(const wchar_t* wstr);
	EPN_CString_T operator +(const std::wstring& wstr);
	EPN_CString_T operator +(const EPN_CString_T& estr_t);
	EPN_CString_T operator +=(const char c);
	EPN_CString_T operator +=(const char* str);
	EPN_CString_T operator +=(const std::string& str);
	EPN_CString_T operator +=(const wchar_t wc);
	EPN_CString_T operator +=(const wchar_t* wstr);
	EPN_CString_T operator +=(const std::wstring& wstr);
	EPN_CString_T operator +=(const EPN_CString_T& estr_t);
	BOOL operator ==(const char c);
	BOOL operator ==(const char* str);
	BOOL operator ==(const std::string& str);
	BOOL operator ==(const wchar_t wc);
	BOOL operator ==(const wchar_t* wstr);
	BOOL operator ==(const std::wstring& wstr);
	BOOL operator ==(const EPN_CString_T& estr_t);
	BOOL operator !=(const char c);
	BOOL operator !=(const char* str);
	BOOL operator !=(const std::string& str);
	BOOL operator !=(const wchar_t wc);
	BOOL operator !=(const wchar_t* wstr);
	BOOL operator !=(const std::wstring& wstr);
	BOOL operator !=(const EPN_CString_T& estr_t);
private:
	friend std::ostream& operator <<(std::ostream &c, const EPN_CString_T& estr_t)
	{
		estr_t.Print();
		return c;
	}
	friend std::ostream& operator <<(std::ostream &c, const EPN_CString_T* pestr_t)
	{
		if(pestr_t)
			c << *pestr_t;
		else
			c << "EPN_CString_T(NULL)";
		return c;
	}

} estring_t;
#define ET_TEXT(estr_t) (estring_t)estr_t

#endif