#ifndef _EPN_DX_TEXTURE_H_
#define _EPN_DX_TEXTURE_H_

#ifdef DLL_EPN_DXTEXTURE_CLASS
	#define DLLTYPE_EPN_DXTEXTURE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_DXTEXTURE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#include "EPN_Texture.h"

class DLLTYPE_EPN_DXTEXTURE EPN_DXTexture : public EPN_Texture
{
private:
	LPDIRECT3DDEVICE9	pRenderDevice;	// 렌더링에 사용될 D3D 디바이스
	LPD3DXSPRITE pSprite; // 렌더링에 사용될 스프라이트
	IDirect3DTexture9 *Texture;
	LPDIRECT3DSURFACE9 SurfaceData;
private:
	estring TexturePath;
	BOOL ColorKeyFlag;
	EPN_ARGB ColorKey;
	BOOL PassFlag;
	estring PASS_16BYTE_KEY;
	EPN_Rect TextureSize;

	BOOL OriginalSizeDrawFlag;

public:
	EPN_DXTexture(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_DXTexture();

public:
	virtual BOOL Blt(EPN_Pos Pos,EPN_Rect Rect,EPN_ARGB BlendColor = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = EPN_Scaling(1.f,1.f), BOOL WidthRevers = FALSE);

	virtual BOOL CreateTexture(void* RenderDevice,EPN_Pos TextureSize,EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);
	virtual BOOL CopyTexture(EPN_Texture* CopyTexture,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	virtual BOOL CopyTexture(EPN_Texture* CopyTexture,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	virtual BOOL EditTexture(EPN_Pos DestPos, EPN_ARGB Color, EPN_Pos DestSize = EPN_Pos(1,1),BOOL CoverEditFlag = false);
	virtual EPN_ARGB getTextureColor(EPN_Pos PixelPos);

	virtual BOOL LoadTexture(void* RenderDevice,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	virtual BOOL LoadTexture(void* RenderDevice,BYTE* pData, DWORD DataSize, BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");

	virtual void* getTexture();

	virtual BOOL setTextureEffect1(EPN_TEXTURE_EFFECT_TYPE_1 Type, EPN_Rect Size = 0);
	virtual BOOL setTextureEffect2(EPN_TEXTURE_EFFECT_TYPE_2 Type2, float Value, EPN_Rect Size = 0);
	virtual BOOL setTextureEffect2(EPN_TEXTURE_EFFECT_TYPE_2 Type2, EPN_ARGB Value, EPN_Rect Size = 0);

	virtual EPN_ARGB getColorKey();
	
	virtual estring getPassword();
	virtual EPN_Rect getTextureSize();

	virtual BOOL SaveTextureFile(estring Path, SaveTypeOption TypeFlag);

	virtual BOOL ResetTexture(void* RenderDevice, short DeviceIndex = -1);
	virtual BOOL ClearTexture(short DeviceIndex = -1);

	virtual void* getRenderDevice();
	virtual void* getRenderData();
};

#endif
