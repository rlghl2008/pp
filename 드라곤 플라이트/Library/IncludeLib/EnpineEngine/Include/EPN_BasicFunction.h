#ifndef _EPN_BASICFUNCTION_H_
#define _EPN_BASICFUNCTION_H_

#include "EPN_Global.h"
#include "EPN_InputEvent.h"
#include "EPN_CString.h"


#ifdef M_DLL_EPN_Function
	#define DLL_EPN_Function __declspec(dllexport)
#else
	#define DLL_EPN_Function __declspec(dllimport)
#endif 



namespace EPN
{
	
	enum SaveTypeOption
	{
		SAVETYPE_BMP = 0,
		SAVETYPE_JPG,
		SAVETYPE_TGA,
		SAVETYPE_PNG,
		SAVETYPE_DDS,
		SAVETYPE_PPM,
		SAVETYPE_DIB,
		SAVETYPE_HDR,
		SAVETYPE_PFM
	};
	namespace BasicFunction
	{
		

		DLL_EPN_Function float EPN_GetPosAngle(EPN_Pos DestPos,EPN_Pos TargetPos);
		DLL_EPN_Function EPN_Pos EPN_GetAnglePos(float Angle);
		DLL_EPN_Function BOOL EPN_SaveScreen(short DeviceIndex,estring Path,EPN :: SaveTypeOption Flag);

		DLL_EPN_Function int EPN_ValueCount(int &ValueNum,int ValueMax);
		DLL_EPN_Function int EPN_ValueCount(int &ValueNum,int ValueMax,int &Count ,int CountTimer = 0);
		DLL_EPN_Function int EPN_ValueCount(int &ValueNum,int ValueMin,int ValueMax,BOOL &Flag);
		DLL_EPN_Function int EPN_ValueCount(int &ValueNum,int ValueMin,int ValueMax,int &Count ,int CountTimer ,BOOL &Flag);
		

		DLL_EPN_Function BOOL EPN_SetFormAlpha(short DeviceIndex,float AlphaKey);
		DLL_EPN_Function EPN_Pos EPN_GetMousePos();//폼크기 변경해도 정확한값 반환

		DLL_EPN_Function double EPN_GetDistance(EPN_Pos DestPos,EPN_Pos TargetPos);//두점끼리 길이 구하는 함수

		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Rect A,EPN_Rect B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Circle A, EPN_Circle B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Rect A, EPN_Circle B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Circle A, EPN_Rect B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Polygon* B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Rect B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Rect A, EPN_Polygon* B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Circle B);
		DLL_EPN_Function BOOL EPN_CheckCollision(EPN_Circle A, EPN_Polygon* B);

		DLL_EPN_Function BOOL EPN_WriteRegistry(HKEY PATH_FLAG,estring Path,estring Name,estring Value); 
		DLL_EPN_Function estring EPN_ReadRegistry(HKEY PATH_FLAG,estring Path,estring Name); 

		DLL_EPN_Function BOOL EPN_WriteClipboard(estring source);
		DLL_EPN_Function estring EPN_ReadClipboard();

		DLL_EPN_Function long EPN_GetFileSize(estring Path);
		DLL_EPN_Function BOOL EPN_CopyFile(estring ReadPath, estring WritePath);

		//파일압축 압축대상파일 경로, 압축된파일 경로
		DLL_EPN_Function BOOL EPN_CompressionFile(estring ReadPath, estring WritePath);

		//파일압축해제 압축해제 대상파일 경로, 압축풀(폴더) 경로
		DLL_EPN_Function BOOL EPN_DeCompressionFile(estring ReadPath, estring WritePath);

		//바탕화면 해상도구하기
		DLL_EPN_Function EPN_Rect EPN_GetWindowRect(HWND hwnd = GetDesktopWindow());

		//디렉토리 리스트 구하기
		//DLL_EPN_Function EPN_GetDirectory (estring Path, vector<estring> &FileList);

		DLL_EPN_Function BOOL CompareString(const char *StrA, const char *StrB);
		DLL_EPN_Function int GetStringLength(const char *pSrc);
		DLL_EPN_Function char* CopyString(char *pDast, const char* pSrc, int CopySize = -1);
		DLL_EPN_Function char* AddString(char *pDast, const char* pSrc, int CopySize = -1);

	}
}

#endif