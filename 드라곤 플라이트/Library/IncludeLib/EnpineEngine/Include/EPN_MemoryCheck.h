#ifdef _DEBUG   
	#include <crtdbg.h>
	#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
	#define EPN_MEMORY_CHECK _CrtSetDbgFlag( _CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF );
#else
	#define EPN_MEMORY_CHECK
#endif