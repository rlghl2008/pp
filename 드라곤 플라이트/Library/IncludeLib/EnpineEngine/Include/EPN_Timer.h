
#ifndef _EPN_TIMER_H_
#define _EPN_TIMER_H_

#ifdef DLL_EPN_TIMER_CLASS
	#define DLLTYPE_EPN_TIMER __declspec(dllexport)
#else
	#define DLLTYPE_EPN_TIMER __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <windows.h>
#include "EPN_InfoPrint.h"

class DLLTYPE_EPN_TIMER EPN_Timer
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	BOOL TimerStartFlag;//시간측정의 시작여부 플래스 TRUE 시작중 FALSE 시작되지않거나 측정종료됨
public:
	EPN_Timer(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_Timer();
public:
	void Start();
	void End();
	void Clear();
	const float getTime();
	const float getEndMicroSecond() const;
	const float	getEndMilliSecond() const;
	BOOL getTimerStartFlag();
protected:
	LARGE_INTEGER m_swFreq;
	LARGE_INTEGER m_swStart;
	LARGE_INTEGER m_swTimer;
	LARGE_INTEGER m_swEnd;
	float m_fTimeforDuration;
}; 

#endif