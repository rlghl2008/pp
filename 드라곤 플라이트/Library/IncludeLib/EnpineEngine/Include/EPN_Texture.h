#ifndef _EPN_TEXTURE_H_
#define _EPN_TEXTURE_H_

#ifdef DLL_EPN_TEXTURE_CLASS
	#define DLLTYPE_EPN_TEXTURE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_TEXTURE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_BasicFunction.h"
using namespace EPN;
#include "EPN_InfoPrint.h"
#include "EPN_CString.h"

enum EPN_TEXTURE_EFFECT_TYPE_1//고정적인 효과
{
	EPN_T1_BLACK_WHITE = 0,//흑백
	EPN_T1_SOFT,//부드럽게
	EPN_T1_NEGATIVE,//색반전
	EPN_T1_SWAP_R_G,//빨강 초록 색값스왑
	EPN_T1_SWAP_R_B,//빨강 파랑 색값스왑
	EPN_T1_SWAP_G_B// 초록 파랑 색값스왑

};
enum EPN_TEXTURE_EFFECT_TYPE_2//벨류에따른 상대적 효과
{
	EPN_T2_LIGHT = 0,//라이트
	EPN_T2_EMPHASIS,//색감 강조
	EPN_T2_SKETCH,//스케치
	EPN_T2_CARTOON//카툰
};

class DLLTYPE_EPN_TEXTURE EPN_Texture
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	EPN_InfoPrint* getInfoPrint();
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	short DeviceIndex;
private:
	BOOL ViewFlag;
	short ViewChannel;
	EPN_Pos ViewPos;
	EPN_Rect ViewRect;
	EPN_Scaling ViewScaling;
	float ViewAngle;
	EPN_ARGB ViewARGB;
public:
	EPN_Texture(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_Texture();
public:
	void setDeviceIndex(short Index);
	short getDeviceIndex();
public:
	virtual BOOL Blt(EPN_Pos Pos,EPN_Rect Rect,EPN_ARGB BlendColor = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = EPN_Scaling(1.f,1.f), BOOL WidthRevers = FALSE) = 0;
	virtual BOOL CreateTexture(void* RenderDevice,EPN_Pos TextureSize,EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false) = 0;
	virtual BOOL LoadTexture(void* RenderDevice,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "") = 0;
	virtual BOOL LoadTexture(void* RenderDevice,BYTE* pData, DWORD DataSize, BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "") = 0;


	virtual BOOL CopyTexture(EPN_Texture* CopyTexture,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false) = 0;
	virtual BOOL CopyTexture(EPN_Texture* CopyTexture,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false) = 0;
	virtual BOOL EditTexture(EPN_Pos DestPos, EPN_ARGB Color, EPN_Pos DestSize = EPN_Pos(1,1),BOOL CoverEditFlag = false) = 0;
	virtual EPN_ARGB getTextureColor(EPN_Pos PixelPos) = 0;

	virtual BOOL setTextureEffect1(EPN_TEXTURE_EFFECT_TYPE_1 Type, EPN_Rect Size = 0) = 0;
	//Value 값 min : -255 max : 255 
	virtual BOOL setTextureEffect2(EPN_TEXTURE_EFFECT_TYPE_2 Type2, float Value, EPN_Rect Size = 0) = 0;
	virtual BOOL setTextureEffect2(EPN_TEXTURE_EFFECT_TYPE_2 Type2, EPN_ARGB Value, EPN_Rect Size = 0) = 0;

	virtual BOOL SaveTextureFile(estring Path, SaveTypeOption TypeFlag) = 0;

	virtual BOOL ResetTexture(void* RenderDevice, short DeviceIndex = -1) = 0;
	virtual BOOL ClearTexture(short DeviceIndex = -1) = 0;


	virtual void* getTexture() = 0;
	virtual EPN_ARGB getColorKey() = 0;
	virtual estring getPassword() = 0;
	virtual EPN_Rect getTextureSize() = 0;
	virtual void* getRenderDevice() = 0;
	virtual void* getRenderData() = 0;
public:
	BOOL setViewFlag(BOOL Flag);
	BOOL getViewFlag();
	BOOL setViewChannel(short Channel);
	short getViewChannel();
	BOOL setViewPosX(float X);
	BOOL setViewPosY(float Y);
	BOOL setViewPos(EPN_Pos Pos);
	EPN_Pos getViewPos(BOOL IF_Flag = FALSE);
	BOOL setViewRect(EPN_Rect Rect);
	EPN_Rect getViewRect(BOOL IF_Flag = FALSE);
	BOOL setViewScalingW(float W);
	BOOL setViewScalingH(float H);
	BOOL setViewScaling(EPN_Scaling Scaling);
	EPN_Scaling getViewScaling(BOOL IF_Flag = FALSE);
	BOOL setViewAngle(float Angle);
	float getViewAngle(BOOL IF_Flag = FALSE);
	BOOL setViewARGB(EPN_ARGB Color);
	BOOL setViewARGB_Alpha(float Alpha);
	BOOL setViewARGB_Red(float Red);
	BOOL setViewARGB_Green(float Green);
	BOOL setViewARGB_Blue(float Blue);
	EPN_ARGB getViewARGB(BOOL IF_Flag = FALSE);
};

#endif
