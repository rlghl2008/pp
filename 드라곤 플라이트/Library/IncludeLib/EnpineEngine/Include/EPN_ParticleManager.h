#ifndef _EPN_PARTICLEMANAGER_H_
#define _EPN_PARTICLEMANAGER_H_

#ifdef DLL_EPN_PARTICLEMANAGER_CLASS
	#define DLLTYPE_EPN_PARTICLEMANAGER __declspec(dllexport)
#else
	#define DLLTYPE_EPN_PARTICLEMANAGER __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#include "EPN_Particle.h"


class DLLTYPE_EPN_PARTICLEMANAGER EPN_ParticleManager
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 로드된 모든 재료의 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 매니저만변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
private:
	//Particle 동적객체
	EPN_Particle* pParticle;
	//사용하는 파티클 최대개수
	int MaxParticleNum;
public:
	EPN_ParticleManager(BOOL InfoPrintFlag = FALSE);
	~EPN_ParticleManager();
public:
	//최대 파티클 개수 설정 
	BOOL setMaxParticleNum(int ParticleNum);
	BOOL getMaxParticleNum();
	//최대 파티클 개수 재설정
	BOOL ResetMaxParticleNum(int ParticleNum);
	//파티클매니저 초기화
	BOOL ClearManager();

	//설정된 파티클중 죽은파티클번호 찾아 재설정하고 해당 파티클인덱스를 반환 죽은파티클이없을시 -1반환
	short AddParticle();
public:
	//기본값시 사용하는파티클 전부Run 선택시 해당파티클만 Run
	BOOL Run(int ParticleIndex = -1,EPN_Pos Pos = EPN_Pos(0,0));

public:
	EPN_Particle* getParticle(int ParticleIndex);

public:
	BOOL AllsetParticleDieFlag(BOOL ParticleDieFlag);
	BOOL setParticleDieFlag(int ParticleIndex,BOOL ParticleDieFlag);
	BOOL getParticleDieFlag(int ParticleIndex);

	BOOL AllsetDeleteCountFlag(BOOL DeleteCountFlag);
	BOOL setDeleteCountFlag(int ParticleIndex,BOOL DeleteCountFlag);
	BOOL getDeleteCountFlag(int ParticleIndex);

	BOOL AllsetDeleteCount(int DeleteCount);
	BOOL setDeleteCount(int ParticleIndex,int DeleteCount);
	int getDeleteCount(int ParticleIndex);

	BOOL AllsetParticlePos(EPN_Pos ParticlePos);
	BOOL setParticlePos(int ParticleIndex,EPN_Pos ParticlePos);
	EPN_Pos getParticlePos(int ParticleIndex);

	BOOL AllsetParticleMove(EPN_Pos ParticleMove);
	BOOL setParticleMove(int ParticleIndex,EPN_Pos ParticleMove);
	EPN_Pos getParticleMove(int ParticleIndex);

	BOOL AllsetMoveAngle(float MoveAngle);
	BOOL setMoveAngle(int ParticleIndex,float MoveAngle);
	float getMoveAngle(int ParticleIndex);

	BOOL AllsetMoveSpeed(float MoveSpeed);
	BOOL setMoveSpeed(int ParticleIndex,float MoveSpeed);
	float getMoveSpeed(int ParticleIndex);

	BOOL AllsetAlterMoveAngleFlag(BOOL AlterMoveAngleFlag);
	BOOL setAlterMoveAngleFlag(int ParticleIndex,BOOL AlterMoveAngleFlag);
	BOOL getAlterMoveAngleFlag(int ParticleIndex);

	BOOL AllsetAlterMoveAngle(float AlterMoveAngle);
	BOOL setAlterMoveAngle(int ParticleIndex,float AlterMoveAngle);
	float getAlterMoveAngle(int ParticleIndex);

	BOOL AllsetAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag);
	BOOL setAlterMoveSpeedFlag(int ParticleIndex,BOOL AlterMoveSpeedFlag);
	BOOL getAlterMoveSpeedFlag(int ParticleIndex);

	BOOL AllsetAlterMoveSpeed(float AlterMoveSpeed);
	BOOL setAlterMoveSpeed(int ParticleIndex,float AlterMoveSpeed);
	float getAlterMoveSpeed(int ParticleIndex);

	BOOL AllsetTextureIndex(short TextureIndex);
	BOOL setTextureIndex(int ParticleIndex,short TextureIndex);
	short getTextureIndex(int ParticleIndex);

	BOOL AllsetTextureAngle(float TextureAngle);
	BOOL setTextureAngle(int ParticleIndex,float TextureAngle);
	float getTextureAngle(int ParticleIndex);

	BOOL AllsetTextureScaling(EPN_Scaling TextureScaling);
	BOOL setTextureScaling(int ParticleIndex,EPN_Scaling TextureScaling);
	EPN_Scaling getTextureScaling(int ParticleIndex);

	BOOL AllsetAlterTextureAngleFlag(BOOL AlterTextureAngleFlag);
	BOOL setAlterTextureAngleFlag(int ParticleIndex,BOOL AlterTextureAngleFlag);
	BOOL getAlterTextureAngleFlag(int ParticleIndex);

	BOOL AllsetAlterTextureAngle(float AlterTextureAngle);
	BOOL setAlterTextureAngle(int ParticleIndex,float AlterTextureAngle);
	float getAlterTextureAngle(int ParticleIndex);

	BOOL AllsetAlterTextureScalingFlag(BOOL AlterTextureScalingFlag);
	BOOL setAlterTextureScalingFlag(int ParticleIndex,BOOL AlterTextureScalingFlag);
	BOOL getAlterTextureScalingFlag(int ParticleIndex);

	BOOL AllsetAlterTextureScaling(EPN_Scaling AlterTextureScaling);
	BOOL setAlterTextureScaling(int ParticleIndex,EPN_Scaling AlterTextureScaling);
	EPN_Scaling getAlterTextureScaling(int ParticleIndex);

	BOOL AllsetTextureRect(EPN_Rect TextureRect);
	BOOL setTextureRect(int ParticleIndex,EPN_Rect TextureRect);
	EPN_Rect getTextureRect(int ParticleIndex);

	BOOL AllsetAlterTextureRectFlag(BOOL AlterTextureRectFlag);
	BOOL setAlterTextureRectFlag(int ParticleIndex,BOOL AlterTextureRectFlag);
	BOOL getAlterTextureRectFlag(int ParticleIndex);

	BOOL AllsetAlterTextureRect(EPN_Rect AlterTextureRect);
	BOOL setAlterTextureRect(int ParticleIndex,EPN_Rect AlterTextureRect);
	EPN_Rect getAlterTextureRect(int ParticleIndex);

	BOOL AllsetTextureColor(EPN_ARGB TextureColor);
	BOOL setTextureColor(int ParticleIndex,EPN_ARGB TextureColor);
	EPN_ARGB getTextureColor(int ParticleIndex);

	BOOL AllsetAlterTextureColorFlag(BOOL AlterTextureColorFlag);
	BOOL setAlterTextureColorFlag(int ParticleIndex,BOOL AlterTextureColorFlag);
	BOOL getAlterTextureColorFlag(int ParticleIndex);

	BOOL AllsetAlterTextureColor(EPN_ARGB AlterTextureColor);
	BOOL setAlterTextureColor(int ParticleIndex,EPN_ARGB AlterTextureColor);
	EPN_ARGB getAlterTextureColor(int ParticleIndex);

	BOOL AllsetSpriteFlag(BOOL SpriteFlag);
	BOOL setSpriteFlag(int ParticleIndex,BOOL SpriteFlag);
	BOOL getSpriteFlag(int ParticleIndex);

	BOOL AllsetSpriteIndex(short SpriteIndex);
	BOOL setSpriteIndex(int ParticleIndex,short SpriteIndex);
	short getSpriteIndex(int ParticleIndex);

	BOOL AllsetSpriteDelay(short SpriteDelay);
	BOOL setSpriteDelay(int ParticleIndex,short SpriteDelay);
	short getSpriteDelay(int ParticleIndex);


	BOOL AllsetAfterBltFlag(BOOL AfterBltFlag);
	BOOL setAfterBltFlag(int ParticleIndex,BOOL AfterBltFlag);
	BOOL getAfterBltFlag(int ParticleIndex);

	BOOL AllsetAfterImageNum(int AfterBltFlag);
	BOOL setAfterImageNum(int ParticleIndex,int AfterBltFlag);
	int getAfterImageNum(int ParticleIndex);

	BOOL AllsetAfterImageCount(int AfterImageCount);
	BOOL setAfterImageCount(int ParticleIndex,int AfterImageCount);
	int getAfterImageCount(int ParticleIndex);

	BOOL AllsetAfterImageDeleteCount(int AfterImageDeleteCount);
	BOOL setAfterImageDeleteCount(int ParticleIndex,int AfterImageDeleteCount);
	int getAfterImageDeleteCount(int ParticleIndex);
	
	BOOL AllsetWindUseFlag(BOOL WindUseFlag);
	BOOL setWindUseFlag(int ParticleIndex,BOOL WindUseFlag);
	BOOL getWindUseFlag(int ParticleIndex);

	BOOL AllsetWindAngle(float WindAngle);
	BOOL setWindAngle(int ParticleIndex,float WindAngle);
	float getWindAngle(int ParticleIndex);

	BOOL AllsetWindPower(float WindPower);
	BOOL setWindPower(int ParticleIndex,float WindPower);
	float getWindPower(int ParticleIndex);

	BOOL AllsetGravityFlag(BOOL GravityFlag);
	BOOL setGravityFlag(int ParticleIndex,BOOL GravityFlag);
	BOOL getGravityFlag(int ParticleIndex);

	BOOL AllsetGravityAngle(float GravityAngle);
	BOOL setGravityAngle(int ParticleIndex,float GravityAngle);
	float getGravityAngle(int ParticleIndex);

	BOOL AllsetGravityMaxPower(float GravityMaxPower);
	BOOL setGravityMaxPower(int ParticleIndex,float GravityMaxPower);
	float getGravityMaxPower(int ParticleIndex);

	BOOL AllsetGravitySecCount(float GravitySecCount);
	BOOL setGravitySecCount(int ParticleIndex,float GravitySecCount);
	float getGravitySecCount(int ParticleIndex);
	
	BOOL AllresetGravitySpeed();
	BOOL resetGravitySpeed(int ParticleIndex);

	BOOL AllsetMagnetFlag(BOOL MagnetFlag);
	BOOL setMagnetFlag(int ParticleIndex,BOOL MagnetFlag);
	BOOL getMagnetFlag(int ParticleIndex);

	BOOL AllsetMagnetPos(EPN_Pos MagnetPos);
	BOOL setMagnetPos(int ParticleIndex,EPN_Pos MagnetPos);
	EPN_Pos getMagnetPos(int ParticleIndex);

	BOOL AllsetMagnetPower(float MagnetPower);
	BOOL setMagnetPower(int ParticleIndex,float MagnetPower);
	float getMagnetPower(int ParticleIndex);

	BOOL AllsetMagnetRadius(float MagnetRadius);
	BOOL setMagnetRadius(int ParticleIndex,float MagnetRadius);
	float getMagnetRadius(int ParticleIndex);

	BOOL AllsetParticleStopFlag(BOOL ParticleStopFlag);
	BOOL setParticleStopFlag(int ParticleIndex,BOOL ParticleStopFlag);
	BOOL getParticleStopFlag(int ParticleIndex);
};

#endif