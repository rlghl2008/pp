#ifndef _EPN_SPRITE_H_
#define _EPN_SPRITE_H_

#ifdef DLL_EPN_SPRITE_CLASS
	#define DLLTYPE_EPN_SPRITE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_SPRITE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
using namespace std;
#include <windows.h>


#include "EPN_Global.h"
#include "EPN_Crypto.h"
#include "EPN_InfoPrint.h"
#include "EPN_CString.h"

class DLLTYPE_EPN_SPRITE EPN_Sprite
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
	EPN_Crypto Crypto;
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	EPN_Rect* SpriteRectData;
	BOOL SpriteFlag;
	int SpriteCount;
private:
	int ActionDelayCount;
	int ActionSpriteNum;//현재 반환스프라이트 번호
public:
	EPN_Sprite(BOOL InfoPrintFlag = FALSE);
	~EPN_Sprite();
public:
	//직접 영역을 선택하여 원하는 기준으로 자동으로자름
	BOOL autoSpriteRect(EPN_Rect Rect, DWORD ScopeW, DWORD ScopeH);

	//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로자름
	BOOL autoSpriteRect(short TextureIndex, DWORD ScopeW, DWORD ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
	BOOL autoSpriteRect(estring SaveTextureName, DWORD ScopeW, DWORD ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름
	
	//툴로작업한 스프라이트데이터 파일을 불러와 값저장
	BOOL autoSpriteRect(estring DataPath,BOOL PassFlag = FALSE,char Password[16] = 0);//파일경로입력

	//잘린 영역의 개수 가져오기
	int getSpriteCount();
	void setSpriteCount(int MaxValue);

	//액션기능
	EPN_Rect ActionSprite(int DelayCount);
	EPN_Rect ActionSprite(int DelayCount,int StartIndex,int EndIndex);
	BOOL setActionSpriteNum(int ActionSpriteNum);
	int getActionSpriteNum();
	int getActionDelayCount();

	//입력한 번호에 렉트영역 설정
	BOOL setSpriteRect(DWORD Number,EPN_Rect Rect);
	//입력한 번호에 저장되어있는 렉트영역 가져오기
	EPN_Rect getSpriteRect(DWORD Number);

	BOOL ClearSprite();
};


#endif