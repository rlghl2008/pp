#ifndef _EPN_FONT_H_
#define _EPN_FONT_H_

#ifdef DLL_EPN_FONT_CLASS
	#define DLLTYPE_EPN_FONT __declspec(dllexport)
#else
	#define DLLTYPE_EPN_FONT __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#define DLLAPI __declspec(dllexport)//구조체관련DLL변환
#define DLLEXPORT_EPN_FONTDATA
DLLEXPORT_EPN_FONTDATA struct DLLAPI EPN_FontData;



#include "EPN_MP.hpp"
#include "EPN_SISave.h"

#include "EPN_Global.h"
#include "EPN_InfoPrint.h"
#include "EPN_CString.h"

const int EPN_FONT_NOMAL = 400;
const int EPN_FONT_BOLD = 700;

struct EPN_FontData
{
	estring FontName;
	int FontSize;
	int FontThickness;
	EPN_FontData(estring _FontName = E_TEXT(""),int _FontSize = 0,int _FontThickness = 0)
	{
		FontName = _FontName;
		FontSize = _FontSize;
		FontThickness = _FontThickness;
	}
};

struct EPN_ETextColorData
{
	estring EText;
	EPN_ARGB Color;
	EPN_ETextColorData(estring EText = "", EPN_ARGB Color = EPN_ARGB(255,255,255,255))
	{
		this->EText = EText;
		this->Color = Color;
	}
};
class DLLTYPE_EPN_FONT EPN_Font
{
private:
	short DeviceIndex;
private:
	EPN_MP<EPN_ETextColorData> *pMP_Data;
	EPN_SISave *EPN_Save;
protected:
	EPN_MP<EPN_Pos> *pMP_SizeData;
	EPN_SISave *EPN_SizeSave;
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	EPN_InfoPrint* getInfoPrint();
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	EPN_FontData FontData;
public:
	EPN_Font(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_Font();
public:
	virtual BOOL BltText(estring Text,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),int Buffer = -1) = 0;
	virtual BOOL BltText(short DestTextureIndex, estring Text,EPN_Rect Rect,EPN_ARGB FontColor = EPN_ARGB(255,0,0,0), EPN_ARGB BackColor = EPN_ARGB(0,0,0,0),int Buffer = -1) = 0;
public:
	void setFontData(EPN_FontData FontData);
	EPN_FontData getFontData();
public:
	virtual BOOL setFont(void* RenderDevice,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL) = 0;
	virtual EPN_FontData getFont() = 0;
	virtual BOOL setFontName(estring FontName) = 0;
	virtual estring getFontName() = 0;
	virtual BOOL setFontSize(int FontSize) = 0;
	virtual int getFontSize() = 0;
	virtual BOOL setFontThickness(int FontThickness) = 0;
	virtual int getFontThickness() = 0;

	virtual BOOL ClearFont(short DeviceIndex = -1) = 0;
	virtual BOOL ResetFont(void* RenderDevice,short DeviceIndex = -1) = 0;

	virtual EPN_Pos getETextAreaSize(estring EText, BOOL EndlnFlag = FALSE) = 0;

	BOOL setETextColor(estring EText, EPN_ARGB Color);
	BOOL DeleteETextColor(estring EText);
	EPN_ARGB getETextColor(estring EText);

	int getETextColorCount();
public:
	void setDeviceIndex(short Index);
	short getDeviceIndex();
};
#endif