#ifndef _EPN_INIT_H_
#define _EPN_INIT_H_

#include "EPN_DeviceInterface.h"
#include "EPN_InputEvent.h"
#include "EPN_TextureInterface.h"
#include "EPN_ActionInterface.h"
#include "EPN_SpriteInterface.h"
#include "EPN_FontInterface.h"
#include "EPN_ParticleInterface.h"


#ifdef M_DLL_EPN_INIT
	#define DLL_EPN_INIT __declspec(dllexport)
#else
	#define DLL_EPN_INIT __declspec(dllimport)
#endif 


namespace EPN
{

	namespace BasicFunction
	{
		DLL_EPN_INIT void EPN_Init_Interface(EPN_DeviceInterface** dpEPN_DI);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_InputEvent** dpEPN_IE);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_TextureInterface** dpEPN_TI);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_ActionInterface** dpEPN_AI);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_SpriteInterface** dpEPN_SI);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_FontInterface** dpEPN_FI);
		DLL_EPN_INIT void EPN_Init_Interface(EPN_ParticleInterface** dpEPN_PI);

		DLL_EPN_INIT void EPN_Finish_Interface();
	}
}

#endif