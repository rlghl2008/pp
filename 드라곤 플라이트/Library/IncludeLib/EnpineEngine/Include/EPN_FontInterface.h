#ifndef _EPN_FONTINTERFACE_H_
#define _EPN_FONTINTERFACE_H_

#ifdef DLL_EPN_FONTINTERFACE_CLASS
	#define DLLTYPE_EPN_FONTINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_FONTINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_MP.hpp"
#include "EPN_Font.h"
#include "EPN_DeviceInterface.h"
#include "EPN_SISave.h"

class DLLTYPE_EPN_FONTINTERFACE EPN_FontInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 입력된 모든 스프라이트 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만 변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
public:
	EPN_MP<EPN_Font> *EPN_MP_Font;
	EPN_SISave *EPN_Save;
public:
	EPN_FontInterface();
	virtual ~EPN_FontInterface();
private:
	short LoadFont(EPN_Device *Device,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
	short LoadFont(estring SaveFontName,EPN_Device *Device,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
public:
	//Font 를 로드하여 인터페이스 메모리풀에 메모리등록후 위치인덱스반환
	//설정할 디바이스,폰트이름(궁서체 등등),폰트크기,폰트굵기(EPN_FONT_NOMAL,EPN_FONT_BOLD 가있음)
	short LoadFont(short DeviceIndex,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
	short LoadFont(estring SaveDeviceName,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
	//Font 를 로드하여 인터페이스 메모리풀에 메모리등록후 위치인덱스반환 인덱스 식별값은 스트링에도저장
	//식별 저장할 문자열,설정할 디바이스,폰트이름(궁서체 등등),폰트크기,폰트굵기(EPN_FONT_NOMAL,EPN_FONT_BOLD 가있음)
	short LoadFont(estring SaveFontName,short DeviceIndex,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
	short LoadFont(estring SaveFontName,estring SaveDeviceName,estring FontName,int FontSize,int FontThickness = EPN_FONT_NOMAL);
public:
	//출력한 폰트의 인덱스,출력 메세지,출력할 영역,출력 RGB정보,메시지최대길이 -1시 동적으로 끝까지처리
	BOOL BltText(short FontIndex,estring Text,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0) ,int Buffer = -1);
	//출력한 폰트의 식별 문자열,출력 메세지,출력할 영역,출력 RGB정보,메시지최대길이 -1시 동적으로 끝까지처리
	BOOL BltText(estring SaveFontName,estring Text,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),int Buffer = -1);

public://작업할것들
	
	BOOL setFontName(short FontIndex,estring FontName);
	BOOL setFontName(estring SaveFontName,estring FontName);
	estring getFontName(short FontIndex);
	estring getFontName(estring SaveFontName);

	BOOL setFontSize(short FontIndex,int FontSize);
	BOOL setFontSize(estring SaveFontName,int FontSize);
	int getFontSize(short FontIndex);
	int getFontSize(estring SaveFontName);

	BOOL setFontThickness(short FontIndex,int FontThickness);
	BOOL setFontThickness(estring SaveFontName,int FontThickness);
	int getFontThickness(short FontIndex);
	int getFontThickness(estring SaveFontName);

	EPN_Pos	getETextAreaSize(short FontIndex, estring EText, BOOL EndlnFlag = FALSE);
	EPN_Pos	getETextAreaSize(estring SaveFontName, estring EText, BOOL EndlnFlag = FALSE);

	BOOL setETextColor(short FontIndex, estring EText, EPN_ARGB Color);
	BOOL DeleteETextColor(short FontIndex, estring EText);
	EPN_ARGB getETextColor(short FontIndex, estring EText);
	int getETextColorCount(short FontIndex);

	BOOL setETextColor(estring SaveFontName, estring EText, EPN_ARGB Color);
	BOOL DeleteETextColor(estring SaveFontName, estring EText);
	EPN_ARGB getETextColor(estring SaveFontName, estring EText);
	int getETextColorCount(estring SaveFontName);

public://EPN_SISave 객체적응
	short getFontIndex(estring SaveFontName);
	estring getFontStr(short FontIndex);

public://EPN_MP 객체적응
	EPN_Font* getFont(short FontIndex);
	EPN_Font* getFont(estring SaveFontName);
	BOOL DeleteFont(short FontIndex);
	BOOL DeleteFont(estring SaveFontName);
	BOOL AllDeleteFont(short DeviceIndex = -1);
	int getInsertCount(short DeviceIndex = -1);//현재 인터페이스에 로드되어 메모리에 올라가있는 폰트 개수 반환
public:
	BOOL ResetFont(short FontIndex,void* RenderDevice);
	BOOL ResetFont(estring SaveFontName,void* RenderDevice);
	BOOL AllResetFont(void* RenderDevice,short DeviceIndex = -1);

	BOOL ClearFont(short FontIndex);
	BOOL ClearFont(estring SaveFontName);
	BOOL AllClearFont(short DeviceIndex = -1);
public://외장 폰트를 사용할려면 해당메서드를사용 (오픈시 다사용하면 클로즈필수)
	BOOL OpenFontData(estring Path);
	BOOL CloseFontData(estring Path);
private:
	static EPN_FontInterface* m_pFontInterface;
public:
	static EPN_FontInterface* getInstance();
	static EPN_FontInterface* CreateInstance(void);
};
#endif