
#ifndef _EPN_CRYPT_H_
#define _EPN_CRYPT_H_

#ifdef DLL_EPN_CRYPT_CLASS
	#define DLLTYPE_EPN_CRYPT __declspec(dllexport)
#else
	#define DLLTYPE_EPN_CRYPT __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
#include <windows.h>
#include "EPN_InfoPrint.h"


#define CRYPTO_MAX_SIZE 16

class DLLTYPE_EPN_CRYPT EPN_Crypto
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	unsigned char Passkey[16];
	unsigned char ByteSub[16][16];
	unsigned char EnByteSub[16][16];
	char* ByteShuffling(unsigned char Data[16]);
	char* ByteReversing(unsigned char Data[16]);
	char* ByteEnReversing(unsigned char Data[16]);
	VOID ByteSubing(unsigned char* Data);
	VOID EnByteSubing(unsigned char* Data);
private:
	FILE *InputData;
	FILE *OutputData;
	BOOL PassKeyFlag;
public:
	EPN_Crypto(BOOL InfoPrintFlag = FALSE); 
	virtual ~EPN_Crypto();
public:
	BOOL Init_PassKey(char password[16]);
	VOID Init_File(FILE* InData,FILE* OutData);
	FILE *GetFileInputData();
	FILE *GetFileOutputData();

	VOID Crypting();
	VOID DeCrypting();
	BOOL Crypting(const char* pData, char* pGet, int Size = CRYPTO_MAX_SIZE);
	BOOL DeCrypting(const char* pData, char* pGet, int Size = CRYPTO_MAX_SIZE);
};

#endif