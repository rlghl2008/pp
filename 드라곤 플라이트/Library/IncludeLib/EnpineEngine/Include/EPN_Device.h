#ifndef _EPN_DEVICE_H_
#define _EPN_DEVICE_H_

#ifdef DLL_EPN_DEVICE_CLASS
	#define DLLTYPE_EPN_DEVICE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_DEVICE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#define DLLAPI __declspec(dllexport)//구조체관련DLL변환
#define DLLEXPORT_STRUET
DLLEXPORT_STRUET struct DLLAPI FPS_INFO;

#include <windows.h>

#include "EPN_Global.h"
#include "EPN_InputEvent.h"
#include "EPN_CString.h"

enum EPN_DeviceOption
{
	NULL_Device = 0,
	DirectX9,
	OpenGL,
	SoftRender
};

enum EPN_DEVICEVALIDITY
{
	EPN_DEVICE_OK = 1,//디바이스가 정상적일때
	EPN_DEVICE_EXIT = 0,//디바이스가 종료되었을때
	EPN_DEVICE_WAR = -1,//디바이스가 소실되고 쉽게 복구가 가능할때
	EPN_DEVICE_ERROR = -2//디바이스가 소실되고 쉽게 복구가 불가능할때
};

class DLLTYPE_EPN_DEVICE EPN_Device 
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	EPN_InfoPrint* getInfoPrint();
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
protected:
	EPN_InputEvent* pInputEvent;//입력처리클래스
private:
	short DeviceIndex;//디바이스가 디바이스 인터페이스 에서 할당받은 인덱스 번호 
	EPN_DeviceOption	m_DeviceOption;//디바이스 옵션
public:
	EPN_Device(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_Device();
	

public://메서드
	virtual BOOL Run(BOOL Flag = TRUE) = 0;//TRUE 화면이 액티브시에만 작동 FALSE 화면이 액티브 상태가 아니더라도 작동
	virtual BOOL EventRun() = 0;

	virtual BOOL CreateDevice(estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW) = 0;
	virtual BOOL CreateDeviceEx(estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW) = 0;

	virtual BOOL ReCreateDevice() = 0;
	virtual BOOL ReCreateDevice(estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW) = 0;
	virtual BOOL ReCreateDeviceEx() = 0;
	virtual BOOL ReCreateDeviceEx(estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW) = 0;
	
	virtual BOOL ResetDevice() = 0;
	virtual BOOL ResetDevice(estring Caption,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL) = 0;
	virtual BOOL ResetDeviceEx() = 0;
	virtual BOOL ResetDeviceEx(estring Caption,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL) = 0;

	virtual BOOL ClearDevice(void) = 0;
	
	virtual EPN_DEVICEVALIDITY DeviceValidity() = 0;
	virtual EPN_DEVICEVALIDITY BeginScene(EPN_ARGB BColor) = 0;
	virtual BOOL EndScene() = 0;

	
public://디바이스 기본 드로우기능
	virtual BOOL BltLine(EPN_Pos StartPos,EPN_Pos EndPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),float LineThickness = 1,BOOL AliasingFlag = FALSE) = 0;
	virtual BOOL BltQuadrangle(EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE) = 0;
	virtual BOOL BltQuadrangle(EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_Pos FourthPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE) = 0;
	virtual BOOL BltTriangle(EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE) = 0;



public:
	void setDeviceIndex(short DeviceIndex);
	short getDeviceIndex();
	EPN_DeviceOption	getDeviceOption();
	void setDeviceOption(EPN_DeviceOption _DeviceOption);
	void setInputEvent(EPN_InputEvent* pEvent);
	EPN_InputEvent* getInputEvent();
//순수가상함수
public://접근 인터페이스
	virtual void KillDevice() = 0;
public://윈도우 메세지 관련
	virtual MSG getWinMsg() = 0;
	virtual void setWinMsg(MSG Msg) = 0;
	virtual HWND getHwnd() = 0;
	virtual WPARAM getWPARAM() = 0;
	virtual LPARAM getLPARAM() = 0;
	virtual UINT getMESSAGE() = 0;

	virtual BOOL IsMinimizeWindow() = 0;
	virtual BOOL setMinimizeWindow() = 0;
	virtual BOOL IsMaxmizeWindow() = 0;
	virtual BOOL setMaxmizeWindow() = 0;
	virtual BOOL IsFocusWindow() = 0;
	virtual BOOL setFocusWindow() = 0;
	virtual BOOL IsActivityWindow() = 0;//현재 디바이스 액티브 여부 반환
	virtual BOOL setActivityWindow(BOOL Flag = TRUE) = 0;//현재 디바이스를 액티브 시켜줍니다
	virtual BOOL setRestoreWindow() = 0;

	

public://디바이스 정보 관련
	virtual BOOL getAlive() = 0;
	virtual void* getRenderDevice() = 0;
	virtual void* getRenderData() = 0;

	virtual BOOL getBeginFlag() = 0;
	virtual void setBeginFlag(BOOL Flag) = 0;
	

	virtual estring getFormCaption() = 0;
	virtual BOOL setFormCaption(estring Caption) = 0;

	virtual EPN_Pos getFormPos() = 0;
	virtual BOOL setFormPos(EPN_Pos Pos) = 0;

	virtual float getFormPosX() = 0;
	virtual float getFormPosY() = 0;
	virtual BOOL setFormPosX(float PosX) = 0;
	virtual BOOL setFormPosY(float PosY) = 0;

	virtual EPN_Pos getFormSize() = 0;
	virtual int getFormSizeWidht() = 0;
	virtual int getFormSizeHeight() = 0;
	virtual BOOL setFormSize(EPN_Pos FormSize) = 0;
	virtual BOOL setFormSizeWidht(int Widht) = 0;
	virtual BOOL setFormSizeHeight(int Height) = 0;

	virtual EPN_Pos getDeviceSize() = 0;

	virtual unsigned int getFormStyle() = 0;
	virtual BOOL setFormStyle(unsigned int Style) = 0;

	virtual unsigned int getFormExStyle() = 0;
	virtual BOOL setFormExStyle(unsigned int ExStyle) = 0;

	virtual BOOL getFullScreenFlag() = 0;
	

};

#endif