#ifndef _EPN_TEXTUREINTERFACE_H_
#define _EPN_TEXTUREINTERFACE_H_

#ifdef DLL_EPN_TEXTUREINTERFACE_CLASS
	#define DLLTYPE_EPN_TEXTUREINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_TEXTUREINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_MP.hpp"
#include "EPN_Texture.h"
#include "EPN_Device.h"
#include "EPN_SISave.h"
#include "EPN_CString_T.h"


class DLLTYPE_EPN_TEXTUREINTERFACE EPN_TextureInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 로드된 모든 텍스쳐의 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
private:
	EPN_MP<EPN_Texture> *EPN_MP_Texture;
	EPN_SISave *EPN_Save;
	BOOL InfoPrintFlag;//정보나 에러시 콘솔창에 메시지표시 플래스 TRUE - 출력 FALSE - 출력안함
public:
    EPN_TextureInterface();
    virtual ~EPN_TextureInterface();
private:
	short LoadTexture(EPN_Device *Device,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveTextureName,EPN_Device *Device,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	
	short LoadTexture(EPN_Device *Device,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveTextureName,EPN_Device *Device,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");


	short CreateTexture(EPN_Device *Device,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);
	short CreateTexture(estring SaveTextureName,EPN_Device *Device,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);
public:
	short CreateTexture(short DeviceIndex,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);
	short CreateTexture(estring SaveDeviceName,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);

	short CreateTexture(estring SaveTextureName,short DeviceIndex,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);
	short CreateTexture(estring SaveTextureName,estring SaveDeviceName,EPN_Pos TextureSize, EPN_ARGB BasicColor = EPN_ARGB(255,255,255,255), BOOL XRGB_Flag = false);

	BOOL CopyTexture(short TextureIndex, short CapyTextureIndex,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(short TextureIndex, estring CapySaveTextureName,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(estring SaveTextureName, short CapyTextureIndex,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(estring SaveTextureName, estring CapySaveTextureName,EPN_Pos DestPos = EPN_Pos(0,0),EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);

	BOOL CopyTexture(short TextureIndex, short CapyTextureIndex,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(short TextureIndex, estring CapySaveTextureName,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(estring SaveTextureName, short CapyTextureIndex,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);
	BOOL CopyTexture(estring SaveTextureName, estring CapySaveTextureName,EPN_Pos DestPos, EPN_Rect CopySize,EPN_ARGB CapyColor = EPN_ARGB(255,255,255,255),BOOL CoverEditFlag = false);

	BOOL EditTexture(short TextureIndex, EPN_Pos DestPos, EPN_ARGB Color, EPN_Pos DestSize = EPN_Pos(1,1),BOOL CoverEditFlag = false);
	BOOL EditTexture(estring SaveTextureName, EPN_Pos DestPos, EPN_ARGB Color, EPN_Pos DestSize = EPN_Pos(1,1),BOOL CoverEditFlag = false);

	EPN_ARGB getTextureColor(short TextureIndex, EPN_Pos PixelPos);
	EPN_ARGB getTextureColor(estring SaveTextureName, EPN_Pos PixelPos);

	BOOL setTextureEffect1(short TextureIndex, EPN_TEXTURE_EFFECT_TYPE_1 Type, EPN_Rect Size = 0);
	BOOL setTextureEffect2(short TextureIndex, EPN_TEXTURE_EFFECT_TYPE_2 Type2, float Value, EPN_Rect Size = 0) ;
	BOOL setTextureEffect2(short TextureIndex, EPN_TEXTURE_EFFECT_TYPE_2 Type2, EPN_ARGB Value, EPN_Rect Size = 0);
	BOOL setTextureEffect1(estring SaveTextureName, EPN_TEXTURE_EFFECT_TYPE_1 Type, EPN_Rect Size = 0);
	BOOL setTextureEffect2(estring SaveTextureName, EPN_TEXTURE_EFFECT_TYPE_2 Type2, float Value, EPN_Rect Size = 0) ;
	BOOL setTextureEffect2(estring SaveTextureName, EPN_TEXTURE_EFFECT_TYPE_2 Type2, EPN_ARGB Value, EPN_Rect Size = 0);

	//텍스쳐가출력될 디바이스,그래픽파일경로,컬러키사용여부,걸러낼 RGB정보,복호화사용여부,비밀번호 16자리
	//텍스쳐로드후 메모리풀에 저장된 텍스쳐번호 반환
	short LoadTexture(short DeviceIndex,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveDeviceName,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");

	//텍스쳐번호 식별할 문자열,텍스쳐가출력될 디바이스,그래픽파일경로,컬러키사용여부,걸러낼 RGB정보,복호화사용여부,비밀번호 16자리
	//텍스쳐로드후 메모리풀에 저장된 텍스쳐번호 반환 식별 문자열로도 검색가능
	short LoadTexture(estring SaveTextureName,short DeviceIndex,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveTextureName,estring SaveDeviceName,estring Path,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");

	//텍스쳐가출력될 디바이스,그래픽파일경로,컬러키사용여부,걸러낼 RGB정보,복호화사용여부,비밀번호 16자리
	//텍스쳐로드후 메모리풀에 저장된 텍스쳐번호 반환
	short LoadTexture(short DeviceIndex,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveDeviceName,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");

	//텍스쳐번호 식별할 문자열,텍스쳐가출력될 디바이스,그래픽파일경로,컬러키사용여부,걸러낼 RGB정보,복호화사용여부,비밀번호 16자리
	//텍스쳐로드후 메모리풀에 저장된 텍스쳐번호 반환 식별 문자열로도 검색가능
	short LoadTexture(estring SaveTextureName,short DeviceIndex,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");
	short LoadTexture(estring SaveTextureName,estring SaveDeviceName,BYTE* pData, DWORD DataSize,BOOL ColorKeyFlag = FALSE,EPN_ARGB ColorKey = EPN_ARGB(0,0,0),BOOL PassFlag = FALSE,estring PASS_16BYTE_KEY = "");


	//텍스쳐 출력함수
	//텍스쳐정보,출력위치,출력영역,ARGB정보,출력각도,가로,세로 확대축소비율,가로반전,엘리어싱 1 3 6 9 사용 나머지값 무시
	BOOL Blt(estring SaveTextureName,EPN_Pos Pos,EPN_Rect Rect,EPN_ARGB BlendColor = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = EPN_Scaling(1.f,1.f), BOOL WidthRevers = FALSE);
	BOOL Blt(short TextureIndex,EPN_Pos Pos,EPN_Rect Rect,EPN_ARGB BlendColor = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = EPN_Scaling(1.f,1.f), BOOL WidthRevers = FALSE);
	BOOL Blt(EPN_Texture* Texture,EPN_Pos Pos,EPN_Rect Rect,EPN_ARGB BlendColor = EPN_ARGB(255,255,255,255), float Angle = 0, EPN_Scaling Scaling = EPN_Scaling(1.f,1.f), BOOL WidthRevers = FALSE);

	BOOL SaveTextureFile(short TextureIndex, estring Path, SaveTypeOption TypeFlag);
	BOOL SaveTextureFile(estring SaveTextureName, estring Path, SaveTypeOption TypeFlag);

public://EPN_SISave 객체적응
	short getTextureIndex(estring SaveTextureName);
	estring getTextureStr(short TextureIndex);

public://EPN_MP 객체적응
	EPN_Texture* getTexture(short TextureIndex);
	EPN_Texture* getTexture(estring SaveTextureName);
	BOOL DeleteTexture(short TextureIndex);
	BOOL DeleteTexture(estring SaveTextureName);
	BOOL AllDeleteTexture(short DeviceIndex = -1);

	//현재 인터페이스에 로드되어 메모리에 올라가있는 텍스쳐 개수 반환
	int getInsertCount(short DeviceIndex = -1);
public:
	BOOL ResetTexture(short TextureIndex,void* RenderDevice);
	BOOL ResetTexture(estring SaveTextureName,void* RenderDevice);
	BOOL AllResetTexture(void* RenderDevice, short DeviceIndex = -1);

	
	BOOL ClearTexture(short TextureIndex);
	BOOL ClearTexture(estring SaveTextureName);
	BOOL AllClearTexture(short DeviceIndex = -1);

public:
	
	EPN_ARGB getColorKey(short TextureIndex);
	estring getPassword(short TextureIndex);
	EPN_Rect getTextureSize(short TextureIndex);
	EPN_ARGB getColorKey(estring SaveTextureName);
	estring getPassword(estring SaveTextureName);
	EPN_Rect getTextureSize(estring SaveTextureName);
	void* getRenderDevice(short TextureIndex);
	void* getRenderDevice(estring SaveTextureName);

public:
	//보는 시각위치 관련 ( 로드되면 기본으로 TRUE 이며 TRUE 는 보는시각 위치에 의한 출력위치가 보정됩니다 )
	//IF_Flag - TRUE 일시 무조건 값 반환 - FALSE 일시 출력보정옵션 사용여부가 TRUE 일때만 값반환 이외는 NULL 반환

	//채널값을 기본값인 -1로 두면 로드된 텍스쳐 전부 옵션에 의한 출력보정옵션 사용여부 변경
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 옵션변경
	BOOL AllsetViewFlag(BOOL Flag,short Channel = -1);
	
	BOOL setViewFlag(short TextureIndex,BOOL Flag);//해당 텍스쳐 보는시각 위치에 의한 출력보정옵션 사용여부 변경
	BOOL getViewFlag(short TextureIndex);
	BOOL setViewFlag(estring SaveTextureName,BOOL Flag);
	BOOL getViewFlag(estring SaveTextureName);

	//채널값을 기본값인 -1로 두면 로드된 텍스쳐 전부 출력보정옵션 채널값 변경
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 옵션변경
	BOOL AllsetViewChannel(short SwapChannel,short Channel = -1);
	
	BOOL setViewChannel(short TextureIndex,short SwapChannel);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 채널값 변경
	short getViewChannel(short TextureIndex);//해당 텍스쳐 출력보정옵션 채널값 반환
	BOOL setViewChannel(estring SaveTextureName,short SwapChannel);
	short getViewChannel(estring SaveTextureName);

	//체널값을 기본값인 -1로 두면 로드된 텍스쳐중 출력보정옵션 사용여부가 ture 인 텍스쳐 전부 출력위치 보정
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 변경
	BOOL AllsetViewPos(EPN_Pos Pos,short Channel = -1);

	BOOL setViewPosX(short TextureIndex,float X);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력위치 보정
	BOOL setViewPosY(short TextureIndex,float Y);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력위치 보정
	BOOL setViewPos(short TextureIndex,EPN_Pos Pos);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력위치 보정
	EPN_Pos getViewPos(short TextureIndex,BOOL IF_Flag = FALSE);//해당 텍스쳐 출력보정옵션 사용여부가 IF_Flag 여부에따라 보는시각 좌표 반환 
	BOOL setViewPosX(estring SaveTextureName,float X);
	BOOL setViewPosY(estring SaveTextureName,float Y);
	BOOL setViewPos(estring SaveTextureName,EPN_Pos Pos);
	EPN_Pos getViewPos(estring SaveTextureName,BOOL IF_Flag = FALSE);

	BOOL AllsetViewRect(EPN_Rect Rect,short Channel = -1);
	BOOL setViewRect(short TextureIndex,EPN_Rect Rect);
	EPN_Rect getViewRect(short TextureIndex,BOOL IF_Flag = FALSE);
	BOOL setViewRect(estring SaveTextureName,EPN_Rect Rect);
	EPN_Rect getViewRect(estring SaveTextureName,BOOL IF_Flag = FALSE);

	//체널값을 기본값인 -1로 두면 로드된 텍스쳐중 출력보정옵션 사용여부가 ture 인 텍스쳐 전부 출력 확대/축소 보정
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 변경
	BOOL AllsetViewScaling(EPN_Scaling Scaling,short Channel = -1);

	BOOL setViewScalingW(short TextureIndex,float W);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 확대/축소 보정(가로)
	BOOL setViewScalingH(short TextureIndex,float H);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 확대/축소 보정(세로)
	BOOL setViewScaling(short TextureIndex,EPN_Scaling Scaling);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 확대/축소 보정
	EPN_Scaling getViewScaling(short TextureIndex,BOOL IF_Flag = FALSE);//해당 텍스쳐 출력보정옵션 사용여부가 IF_Flag 여부에따라 확대/축소 배율 반환
	BOOL setViewScalingW(estring SaveTextureName,float W);
	BOOL setViewScalingH(estring SaveTextureName,float H);
	BOOL setViewScaling(estring SaveTextureName,EPN_Scaling Scaling);
	EPN_Scaling getViewScaling(estring SaveTextureName,BOOL IF_Flag = FALSE);
	
	//체널값을 기본값인 -1로 두면 로드된 텍스쳐중 출력보정옵션 사용여부가 ture 인 텍스쳐 전부 출력되는 각도 보정
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 변경
	BOOL AllsetViewAngle(float Angle,short Channel = -1);
	
	BOOL setViewAngle(short TextureIndex,float Angle);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력되는 각도 보정
	float getViewAngle(short TextureIndex,BOOL IF_Flag = FALSE);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력되는 각도 보정
	BOOL setViewAngle(estring SaveTextureName,float Angle);
	float getViewAngle(estring SaveTextureName,BOOL IF_Flag = FALSE);

	//체널값을 기본값인 -1로 두면 로드된 텍스쳐중 출력보정옵션 사용여부가 ture 인 텍스쳐 전부 출력되는 색상 보정
	//채널값을 설정하면 그값으로 뷰채널 값이 설정되어있는 텍스쳐만 변경
	BOOL AllsetViewARGB(EPN_ARGB Color,short Channel = -1);
	
	BOOL setViewARGB(short TextureIndex,EPN_ARGB Color);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력되는 색상 보정
	BOOL setViewARGB_Alpha(short TextureIndex,float Alpha);
	BOOL setViewARGB_Red(short TextureIndex,float Red);
	BOOL setViewARGB_Green(short TextureIndex,float Green);
	BOOL setViewARGB_Blue(short TextureIndex,float Blue);
	EPN_ARGB getViewARGB(short TextureIndex,BOOL IF_Flag = FALSE);//해당 텍스쳐 출력보정옵션 사용여부가 ture 이면 출력되는 색상 보정
	
	BOOL setViewARGB(estring SaveTextureName,EPN_ARGB Color);
	BOOL setViewARGB_Alpha(estring SaveTextureName,float Alpha);
	BOOL setViewARGB_Red(estring SaveTextureName,float Red);
	BOOL setViewARGB_Green(estring SaveTextureName,float Green);
	BOOL setViewARGB_Blue(estring SaveTextureName,float Blue);
	EPN_ARGB getViewARGB(estring SaveTextureName,BOOL IF_Flag = FALSE);

private:
	static EPN_TextureInterface* m_pTextureInterface;
public:
	static EPN_TextureInterface* getInstance();
	static EPN_TextureInterface* CreateInstance();
};


#endif