#ifndef _EPN_INFOPRINT_H_
#define _EPN_INFOPRINT_H_

#ifdef DLL_EPN_INFOPRINT_CLASS
	#define DLLTYPE_EPN_INFOPRINT __declspec(dllexport)
#else
	#define DLLTYPE_EPN_INFOPRINT __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
using namespace std;
#include "EPN_CString.h"

class DLLTYPE_EPN_INFOPRINT EPN_InfoPrint
{
private:
	BOOL PrintFlag;//출력여부 스위치 ture 출력 FALSE 출력안함
	BOOL PrintBoxFlag;//박스출력여부 스위치 TRUE 출력 FALSE 출력안함
	estring BoxCaption;
	estring BoxMsg;

	BOOL LogSaveFlag;//출력됬던 정보들을 파일로 저장할껀지여부 ture 저장함 FALSE 저장안함
	estring LogFilePath;
public:
	EPN_InfoPrint(BOOL Flag = FALSE,BOOL BoxFlag = FALSE,estring BoxTitle = E_TEXT("InfoPrint"),BOOL LogFlag = FALSE,estring LogPath = E_TEXT("./log.txt"));
	~EPN_InfoPrint();
public:
	BOOL InfoPrint(estring Message);
	BOOL InfoPrintln(estring Message);
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
	void On_BoxPrintFlag(estring Caption);
	void Off_BoxPrintFlag();
	BOOL getBoxPrintFlag();
	void On_LogSaveFlag(estring FilePath);
	void Off_LogSaveFlag();
	BOOL getLogSaveFlag();

	estring getBoxCaption();
	estring getLogFilePath();

};
#endif