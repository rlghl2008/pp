#ifndef _STRING_INDEX_H_
#define _STRING_INDEX_H_

#ifdef DLL_EPN_SISAVE_CLASS
	#define DLLTYPE_EPN_SISAVE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_SISAVE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_CString.h"
#include <iostream>
using namespace std;
#include <map>

const int EPN_SI_STRLEN_MAX = 30;

typedef std::map<short*,estring> isMap;

typedef isMap::iterator isMapItor;

typedef isMap::value_type isMapType;

class DLLTYPE_EPN_SISAVE EPN_SISave
{
private:
	isMap ***SaveMap;
public:
	EPN_SISave();
	virtual ~EPN_SISave();
public:
	BOOL InsertData(estring Str,short Index);

	BOOL DeleteData(estring Str);//하나만 찾아 딜리트후 결과리턴
	BOOL DeleteData(short Index);
	int AllDelete();//제거된 데이터 개수 반환

	estring SearchString(short Index);
	short SearchIndex(estring Str);

	void AllPrint();
};

#endif
