#ifndef _EPN_FPS_H_
#define _EPN_FPS_H_

#ifdef DLL_EPN_FPS_CLASS
	#define DLLTYPE_EPN_FPS __declspec(dllexport)
#else
	#define DLLTYPE_EPN_FPS __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <windows.h>
#include "EPN_InfoPrint.h"
#include "EPN_Timer.h"

class DLLTYPE_EPN_FPS EPN_FPS
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	void setInfoPrintFlag(BOOL Flag);
	BOOL getInfoPrintFlag();
private:
	LARGE_INTEGER	CPU_SEC_CLOCK;
	LARGE_INTEGER	INIT_SEC_CLOCK;
	LARGE_INTEGER	TIMER_SEC_CLOCK;
	LONGLONG	VAR_FRAME;
	EPN_Timer Timer;
	DOUBLE	RUN_TIME;
	DWORD	INIT_TIME;
	DWORD	SEC_FRAME;
	BOOL	FPS_FLAG;
	BOOL	_FPS_FLAG;
public:
	EPN_FPS(BOOL InfoPrintFlag = FALSE);
	virtual ~EPN_FPS();
	
public:
	BOOL FPSLocation(DWORD setSEC_FPS = 60);
public:
	LONG Frame;
	LONG getFrame();


}; 

#endif