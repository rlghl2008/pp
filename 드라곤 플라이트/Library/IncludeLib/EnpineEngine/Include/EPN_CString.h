#ifndef _EPN_CSTRING_H_
#define _EPN_CSTRING_H_

#ifdef DLL_EPN_CSTRING_CLASS
	#define DLLTYPE_EPN_CSTRING __declspec(dllexport)
#else
	#define DLLTYPE_EPN_CSTRING __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include <iostream>
#include <windows.h>

#ifdef UNICODE
#define EPN_UNICODE_FLAG TRUE
	typedef std::wstring EPN_STRING;
	typedef wchar_t EPN_CHAR;
#define estrcpy wcscpy
#else
#define EPN_UNICODE_FLAG FALSE
	typedef std::string EPN_STRING;
	typedef char EPN_CHAR;
	#define estrcpy strcpy
#endif

#include "EPN_CString_T.h"
typedef class DLLTYPE_EPN_CSTRING EPN_CString
{
private:
	enum{EPN_INPUT_STRLEN_MAX = 256};//문자열 받을때의 나눠서받는기준 min : 2 Default : 256
	static BOOL UTF8_FLAG;
private:
	EPN_STRING EPN_Str;//컴파일러 설정이 유니코드면 wstring 멀티바이트면 string 으로 설정됨
	EPN_CHAR SaveStr[EPN_INPUT_STRLEN_MAX];//속도를 위하여 동적할당을 쓰지않음
public:
	EPN_CString();
	EPN_CString(const int val);
	EPN_CString(const long val);
	EPN_CString(const float fval);
	EPN_CString(const char c);
	EPN_CString(const char* str);
	EPN_CString(const std::string& str);
	EPN_CString(const wchar_t wc);
	EPN_CString(const wchar_t* wstr);
	EPN_CString(const std::wstring& wstr);
	EPN_CString(const EPN_CString& estr);
	EPN_CString(const EPN_CString_T& estr_t);
	virtual ~EPN_CString();
public:
	EPN_STRING EPN_TEXT(const int val);
	EPN_STRING EPN_TEXT(const long val);
	EPN_STRING EPN_TEXT(const float fval);
	EPN_STRING EPN_TEXT(const char c);
	EPN_STRING EPN_TEXT(const char* str);
	EPN_STRING EPN_TEXT(const std::string& str);
	EPN_STRING EPN_TEXT(const wchar_t wc);
	EPN_STRING EPN_TEXT(const wchar_t* wstr);
	EPN_STRING EPN_TEXT(const std::wstring& wstr);
	EPN_STRING EPN_TEXT(const EPN_CString& estr);
	EPN_STRING EPN_TEXT(const EPN_CString_T& estr_t);
public:
	const EPN_STRING getString() const;//컴파일러 설정이 유니코드면 wstring 멀티바이트면 string 으로 반환됨
	const std :: wstring getStrW();//무조건 유니코드형 얻기
	const std :: string getStrA();//무조건 멀티바이트형 얻기
	const int getValue();//무조건 정수형 얻기
	const long getLValue();//무조건 롱형 얻기
	const float getFloat();//무조건 실수형 얻기
	

	EPN_CHAR at(int len);//컴파일러 설정이 유니코드면 wchar_t 멀티바이트면 char 으로 반환됨
	EPN_CString getMid(int startPos,int endPos);//시작과 끝Pos 위치 사이의 문자열을 반환
	EPN_Strlen SearchText(EPN_CString Text);//문자열을 찾아서 길이와 위치 반환
	BOOL estrcat(EPN_CString estr,int length = -1);//현재 문자열중 length위치에 estr내용 이어붙이기
	const EPN_CHAR* c_str() const;//컴파일러 설정이 유니코드면 wchar_t* 멀티바이트면 char* 으로 반환됨
	int length() const;
	void Print() const;
	void Println() const;
	BOOL Clear();

public:
	EPN_CString operator =(const int val);
	EPN_CString operator =(const long val);
	EPN_CString operator =(const float fval);
	EPN_CString operator =(const char c);
	EPN_CString operator =(const char* str);
	EPN_CString operator =(const std::string& str);
	EPN_CString operator =(const wchar_t wc);
	EPN_CString operator =(const wchar_t* wstr);
	EPN_CString operator =(const std::wstring& wstr);
	EPN_CString operator =(const EPN_CString& estr);
	EPN_CString operator +(const int val);
	EPN_CString operator +(const long val);
	EPN_CString operator +(const float fval);
	EPN_CString operator +(const char c);
	EPN_CString operator +(const char* str);
	EPN_CString operator +(const std::string& str);
	EPN_CString operator +(const wchar_t wc);
	EPN_CString operator +(const wchar_t* wstr);
	EPN_CString operator +(const std::wstring& wstr);
	EPN_CString operator +(const EPN_CString& estr);
	EPN_CString operator +=(const int val);
	EPN_CString operator +=(const long val);
	EPN_CString operator +=(const float fval);
	EPN_CString operator +=(const char c);
	EPN_CString operator +=(const char* str);
	EPN_CString operator +=(const std::string& str);
	EPN_CString operator +=(const wchar_t wc);
	EPN_CString operator +=(const wchar_t* wstr);
	EPN_CString operator +=(const std::wstring& wstr);
	EPN_CString operator +=(const EPN_CString& estr);
	BOOL operator ==(const int val);
	BOOL operator ==(const long val);
	BOOL operator ==(const float fval);
	BOOL operator ==(const char c);
	BOOL operator ==(const char* str);
	BOOL operator ==(const std::string& str);
	BOOL operator ==(const wchar_t wc);
	BOOL operator ==(const wchar_t* wstr);
	BOOL operator ==(const std::wstring& wstr);
	BOOL operator ==(const EPN_CString& estr);
	BOOL operator !=(const int val);
	BOOL operator !=(const long val);
	BOOL operator !=(const float fval);
	BOOL operator !=(const char c);
	BOOL operator !=(const char* str);
	BOOL operator !=(const std::string& str);
	BOOL operator !=(const wchar_t wc);
	BOOL operator !=(const wchar_t* wstr);
	BOOL operator !=(const std::wstring& wstr);
	BOOL operator !=(const EPN_CString& estr);
private:
	friend std::ostream& operator <<(std::ostream &c, const EPN_CString& estr)
	{
		estr.Print();
		return c;
	}
	friend std::ostream& operator <<(std::ostream &c, const EPN_CString* pestr)
	{
		if(pestr)
			c << *pestr;
		else
			c << "EPN_CString(NULL)";
		return c;
	}



} estring;

#define E_TEXT(estr) (estring)estr

#endif