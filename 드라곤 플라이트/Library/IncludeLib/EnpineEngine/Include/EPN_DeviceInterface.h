#ifndef _EPN_DEVICEINTERFACE_H_
#define _EPN_DEVICEINTERFACE_H_

#ifdef DLL_EPN_DEVICEINTERFACE_CLASS
	#define DLLTYPE_EPN_DEVICEINTERFACE __declspec(dllexport)
#else
	#define DLLTYPE_EPN_DEVICEINTERFACE __declspec(dllimport)
#endif //DLL_EXPORT_CLASS

#include "EPN_MP.hpp"
#include "EPN_Device.h"
#include "EPN_SISave.h"

#include <iostream>
using namespace std;

struct EPN_PROC
{
	HWND hwnd;
	UINT message;
	WPARAM wParam; 
	LPARAM lParam;
};

class DLLTYPE_EPN_DEVICEINTERFACE EPN_DeviceInterface
{
private:
	EPN_InfoPrint InfoPrint;//정보메시지 출력클래스
public:
	//autoFlag 가 TRUE 시 인터페이스에 로드된 모든 디바이스의 정보출력 여부와 인터페이스의 정보출력여부가 변경됨 FALSE 시는 인터페이스만변경
	void setInfoPrintFlag(BOOL Flag,BOOL autoFlag = TRUE);
	//인터페이스 정보출력 여부 반환
	BOOL getInfoPrintFlag();
	EPN_InfoPrint* getInfoPrint();
private:
	EPN_MP<EPN_Device> *EPN_MP_Device;
	EPN_SISave *EPN_Save;
private:
	int		m_SequentialCount;	// 디바이스 순차탐색용 맴버변수
	BOOL	m_FalgKilledDevice;		// 죽은 디바이스가 존재하는지 체크해줌

public:
    EPN_DeviceInterface();
    virtual ~EPN_DeviceInterface();

public:
	BOOL Run(short DeviceIndex = -1);//인터페이스 Run 디폴트값 사용시 등록된 디바이스 모두Run
	BOOL Run(estring SaveDeviceName);
	BOOL EventRun(short DeviceIndex);
	BOOL EventRun(estring SaveDeviceName);
	//디바이스옵션,창제목,풀스크린여부,화면크기와이드,화면크기하이드,생성위치X,생성위치Y,입력처리사용여부,입력처리속도(FPS기준),윈도우폼 옵션
	//디바이스를 생성하고 메모리풀에 데이터 저장후 저장된 인덱스반환
	short CreateDevice(EPN_DeviceOption DeviceOption,estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	//디바이스번호 식별할 스트링,디바이스옵션,창제목,풀스크린여부,화면크기와이드,화면크기하이드,생성위치X,생성위치Y,입력처리사용여부,입력처리속도(FPS기준),윈도우폼 옵션
	//디바이스를 생성하고 메모리풀에 데이터 저장후 저장된 인덱스반환 식별 문자열로도 검색가능
	short CreateDevice(estring SaveDeviceName,EPN_DeviceOption DeviceOption,estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	
	short CreateDeviceEx(EPN_DeviceOption DeviceOption,estring Caption = E_TEXT("[E P N]Hello World"),BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	short CreateDeviceEx(estring SaveDeviceName,EPN_DeviceOption DeviceOption,estring Caption = L"[E P N]Hello World",BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);


	BOOL ReCreateDevice(short DeviceIndex);
	BOOL ReCreateDevice(estring SaveDeviceName);
	BOOL ReCreateDevice(short DeviceIndex,EPN_DeviceOption DeviceOption,estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	BOOL ReCreateDevice(estring SaveDeviceName,EPN_DeviceOption DeviceOption,estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	BOOL ReCreateDeviceEx(short DeviceIndex);
	BOOL ReCreateDeviceEx(estring SaveDeviceName);
	BOOL ReCreateDeviceEx(short DeviceIndex,EPN_DeviceOption DeviceOption,estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	BOOL ReCreateDeviceEx(estring SaveDeviceName,EPN_DeviceOption DeviceOption,estring Caption ,BOOL FullScreenFlag = FALSE,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL,unsigned int FormExStyle = 0,unsigned int FormStyle = WS_OVERLAPPEDWINDOW);
	
	BOOL ResetDevice(short DeviceIndex);
	BOOL ResetDevice(estring SaveDeviceName);
	BOOL ResetDevice(short DeviceIndex, estring Caption ,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL);
	BOOL ResetDevice(estring SaveDeviceName,estring Caption ,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL);
	BOOL ResetDeviceEx(short DeviceIndex);
	BOOL ResetDeviceEx(estring SaveDeviceName);
	BOOL ResetDeviceEx(short DeviceIndex ,estring Caption ,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL);
	BOOL ResetDeviceEx(estring SaveDeviceName ,estring Caption ,EPN_Pos DeviceSize = EPN_Pos(640,480),EPN_Pos DevicePos = EPN_Pos(0,0),EPN_InputEvent* pEvent = NULL);

	BOOL ClearDevice(short DeviceIndex);
	BOOL ClearDevice(estring SaveDeviceName);
	BOOL AllClearDevice();

	EPN_DEVICEVALIDITY DeviceValidity(short DeviceIndex);
	EPN_DEVICEVALIDITY DeviceValidity(estring SaveDeviceName);

	EPN_DEVICEVALIDITY BeginScene(short DeviceIndex,EPN_ARGB BColor);
	BOOL EndScene(short DeviceIndex);

	EPN_DEVICEVALIDITY BeginScene(estring SaveDeviceName,EPN_ARGB BColor);
	BOOL EndScene(estring SaveDeviceName);

public://디바이스 기본 드로우기능
	BOOL BltLine(short DeviceIndex,EPN_Pos StartPos,EPN_Pos EndPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),float LineThickness = 1,BOOL AliasingFlag = FALSE);
	BOOL BltLine(estring SaveDeviceName,EPN_Pos StartPos,EPN_Pos EndPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),float LineThickness = 1,BOOL AliasingFlag = FALSE);

	BOOL BltQuadrangle(short DeviceIndex,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	BOOL BltQuadrangle(estring SaveDeviceName,EPN_Rect Rect,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	BOOL BltQuadrangle(short DeviceIndex,EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_Pos FourthPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	BOOL BltQuadrangle(estring SaveDeviceName,EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_Pos FourthPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);

	BOOL BltTriangle(short DeviceIndex,EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);
	BOOL BltTriangle(estring SaveDeviceName,EPN_Pos FirstPos,EPN_Pos SecondPos,EPN_Pos ThirdPos,EPN_ARGB Color = EPN_ARGB(255,0,0,0),BOOL AliasingFlag = FALSE);

public://Device 접근 인터페이스
	void setInputEvent(short DeviceIndex,EPN_InputEvent* pEvent);
	EPN_InputEvent* getInputEvent(short DeviceIndex);
	void setInputEvent(estring SaveDeviceName,EPN_InputEvent* pEvent);
	EPN_InputEvent* getInputEvent(estring SaveDeviceName);

	//디바이스 인덱스로 해당 디바이스에서 관련데이터 를찾음
	MSG getWinMsg(short DeviceIndex);
	HWND getHwnd(short DeviceIndex); 
	WPARAM getWPARAM(short DeviceIndex);
	LPARAM getLPARAM(short DeviceIndex);
	UINT getMESSAGE(short DeviceIndex);
	BOOL IsMinimizeWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스가 최소화 상태인지 비교 최소화 상태면 TRUE반환
	BOOL setMinimizeWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스를 최소화 시킴
	BOOL IsMaxmizeWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스가 최대화 상태인지 비교 최소화 상태면 TRUE반환
	BOOL setMaxmizeWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스를최대화 시킴
	BOOL IsFocusWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스가 활성화(포커스) 상태인지 비교 활성화(포커스) 상태면 TRUE반환
	BOOL setFocusWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스를 활성화(포커스) 시킴
	BOOL IsActivityWindow(short DeviceIndex);//SaveDeviceName에 해당하는 디바이스가 활성화 상태인지 비교 활성화 상태면 TRUE반환
	BOOL setActivityWindow(short DeviceIndex);//SaveDeviceName에 해당하는 디바이스를 활성화 시킴
	BOOL setRestoreWindow(short DeviceIndex);//DeviceIndex에 해당하는 디바이스가 위의함수로 변형됬을시 복구시킴
	
	void* getRenderDevice(short DeviceIndex);
	void* getRenderData(short DeviceIndex);

	estring getFormCaption(short DeviceIndex);
	BOOL setFormCaption(short DeviceIndex,estring Caption);

	EPN_Pos getFormPos(short DeviceIndex);
	float getFormPosX(short DeviceIndex);
	float getFormPosY(short DeviceIndex);
	BOOL setFormPos(short DeviceIndex,EPN_Pos Pos);
	BOOL setFormPosX(short DeviceIndex,float PosX);
	BOOL setFormPosY(short DeviceIndex,float PosY);

	EPN_Pos getFormSize(short DeviceIndex);
	int getFormSizeWidht(short DeviceIndex);
	int getFormSizeHeight(short DeviceIndex);
	BOOL setFormSize(short DeviceIndex,EPN_Pos FormSize);
	BOOL setFormSizeWidht(short DeviceIndex,int Widht);
	BOOL setFormSizeHeight(short DeviceIndex,int Height);

	EPN_Pos getDeviceSize(short DeviceIndex);

	BOOL getFullScreenFlag(short DeviceIndex);

	unsigned int getFormStyle(short DeviceIndex);
	BOOL setFormStyle(short DeviceIndex,unsigned int Style);

	unsigned int getFormExStyle(short DeviceIndex);
	BOOL setFormExStyle(short DeviceIndex,unsigned int ExStyle);

	EPN_DeviceOption getDeviceOption(short DeviceIndex);
	void setDeviceOption(short DeviceIndex,EPN_DeviceOption _DeviceOption);

	//저장된 디바이스 스트링으로 해당 디바이스에서 관련데이터 를찾음
	MSG getWinMsg(estring SaveDeviceName);
	HWND getHwnd(estring SaveDeviceName);
	WPARAM getWPARAM(estring SaveDeviceName);
	LPARAM getLPARAM(estring SaveDeviceName);
	UINT getMESSAGE(estring SaveDeviceName);
	BOOL IsMinimizeWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스가 최소화 상태인지 비교 최소화 상태면 TRUE반환
	BOOL setMinimizeWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스를 최소화 시킴
	BOOL IsMaxmizeWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스가 최대화 상태인지 비교 최소화 상태면 TRUE반환
	BOOL setMaxmizeWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스를최대화 시킴
	BOOL IsFocusWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스가 포커스 상태인지 비교 포커스 상태면 TRUE반환
	BOOL setFocusWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스를 포커스 시킴
	BOOL IsActivityWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스가 활성화 상태인지 비교 활성화 상태면 TRUE반환
	BOOL setActivityWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스를 활성화 시킴
	BOOL setRestoreWindow(estring SaveDeviceName);//SaveDeviceName에 해당하는 디바이스가 위의함수로 변형됬을시 복구시킴
	
	void* getRenderDevice(estring SaveDeviceName);
	void* getRenderData(estring SaveDeviceName);

	estring getFormCaption(estring SaveDeviceName);
	BOOL setFormCaption(estring SaveDeviceName,estring Caption);

	EPN_Pos getFormPos(estring SaveDeviceName);
	float getFormPosX(estring SaveDeviceName);
	float getFormPosY(estring SaveDeviceName);
	BOOL setFormPos(estring SaveDeviceName,EPN_Pos Pos);
	BOOL setFormPosX(estring SaveDeviceName,float PosX);
	BOOL setFormPosY(estring SaveDeviceName,float PosY);

	EPN_Pos getFormSize(estring SaveDeviceName);
	int getFormSizeWidht(estring SaveDeviceName);
	int getFormSizeHeight(estring SaveDeviceName);
	BOOL setFormSize(estring SaveDeviceName,EPN_Pos FormSize);
	BOOL setFormSizeWidht(estring SaveDeviceName,int Widht);
	BOOL setFormSizeHeight(estring SaveDeviceName,int Height);

	EPN_Pos getDeviceSize(estring SaveDeviceName);

	unsigned int getFormStyle(estring SaveDeviceName);
	BOOL setFormStyle(estring SaveDeviceName,unsigned int Style);

	unsigned int getFormExStyle(estring SaveDeviceName);
	BOOL setFormExStyle(estring SaveDeviceName,unsigned int ExStyle);


	BOOL getFullScreenFlag(estring SaveDeviceName);
	EPN_DeviceOption getDeviceOption(estring SaveDeviceName);
	void setDeviceOption(estring SaveDeviceName,EPN_DeviceOption _DeviceOption);

public://EPN_MP 객체적응
	EPN_Device* getDevice(short DeviceIndex);
	EPN_Device* getDevice(estring SaveDeviceName);

	BOOL DeleteDevice(short DeviceIndex);
	BOOL DeleteDevice(estring SaveDeviceName);
	BOOL AllDeleteDevice();

	int getInsertCount();//현재 인터페이스에 크레이트되어 메모리에 올라가있는 디바이스 개수반환
public://EPN_SISAave 객체적응
	short getDeviceIndex(estring SaveDeviceName);
	estring getDeviceStr(short DeviceIndex);

public:
	BOOL IsAliveDevice(void); // 디바이스가 하나라도 살아있는지 검색하고 죽은 디바이스를 지움
	EPN_Device* SequentialgetDevice(void); // 순차적으로 디바이스를 탐색
	EPN_Device* SearchDeviceHWND(HWND _hWnd); // 디바이스를 핸들을 통해 찾음
	short SearchDeviceIndexHWND(HWND _hWnd); // 디바이스 인덱스를 핸들을 통해 찾음
	BOOL KillDevice(void); // 살아있지 않은 디바이스를 지움
	void KilledDeviceFlagSet(void);
private:
	static EPN_DeviceInterface *m_pDeviceInterface;

public:
	static EPN_DeviceInterface* getInstance();
	static EPN_DeviceInterface* CreateInstance();
	static LRESULT DeviceProc(HWND _hWnd, UINT _message,WPARAM _wParam, LPARAM _lParam);
private:
	EPN_PROC m_Proc;
public:
	EPN_PROC* getProc();
};


#endif