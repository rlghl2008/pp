#ifndef _EPN_ENPINE_H_
#define _EPN_ENPINE_H_

#include "EPN_Global.h"
#include "EPN_BasicFunction.h"
#include "EPN_Init.h"

#ifdef _DEBUG   
   #pragma comment(lib, "Debug_EnpineEngine5.4.lib")
#else
   #pragma comment(lib, "Release_EnpineEngine5.4.lib")
#endif

#endif
