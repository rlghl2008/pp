#ifndef _EPN_DX_GLOBAL_H_
#define _EPN_DX_GLOBAL_H_

// DX9를 사용하기 위한 헤더
#include <d3d9.h>
#include <d3dx9.h>


#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

#endif