#ifndef _EPN_MP_H_
#define _EPN_MP_H_


/*
Base class 인 메모리 풀.
원하는 크기만큼의 메모리를 만들어서 리스트로 연결한다.
기본 풀사이즈는 3 이다.
메모리에 대한 접근을 index로 할수 있다.
count 를 두고 index를 매긴다.
*/
template< typename TypeElement >
class EPN_MP
{
    struct Node
    {
        TypeElement **Element;
        Node* link;
        Node():Element(0),link(0){}
        ~Node()
        {
            if(Element)
            {
                delete [] Element;
                Element = 0;
            }
        }
    };
private:
	TypeElement** SearchArray(int _Index);
protected:
    Node*       m_Head;  // 노드 참조용 위치 저장소
    short       m_Size;  // 한번에 생성될 Element의 사이즈 즉 풀의 크기
    int         m_NCount; // 현재 생성되어 있는 Node의 개수(요소)
    int         m_ECount; // 현재 생성되어있는 Element의 개수(풀)
public:
    bool    PoolSizeSet(const short _Size){m_Size = _Size; return true;}
	short	GetElementCount(void){return m_ECount;}
    short   InsertData(TypeElement *_Element);
    bool    DeleteData(short _Index);
	short	PoolSizeGet(void);
	int		GetNodeCount(void);
    TypeElement* EraseData(short _Index);
    TypeElement* SearchElement(int _Index) const;
	void	SortPull();

public:
    EPN_MP():m_Head(0),m_Size(3),m_NCount(0),m_ECount(0){}
    virtual ~EPN_MP();

};

//#include "EPN_MP.h"
//#include <stdio.h>

template< typename TypeElement >
TypeElement** EPN_MP<TypeElement> :: SearchArray(int _Index)
{
	if(_Index < 0 || _Index >= (m_NCount*m_Size) )
		return 0;
	if(_Index < m_Size)
	{
		return &m_Head->Element[_Index];
	}

	int Count = _Index / m_Size;
	Node* Temp = m_Head;
	for(int i = 0 ; i < Count; i++ )
		Temp = Temp->link;
	return &Temp->Element[_Index%m_Size];
}

template< typename TypeElement >
TypeElement* EPN_MP<TypeElement> :: SearchElement(int _Index) const
{
    if(_Index < 0 || _Index >= (m_NCount*m_Size) )
        return 0;
    if(_Index < m_Size)
    {
        return m_Head->Element[_Index];
    }

    int Count = _Index / m_Size;
    Node* Temp = m_Head;
    for(int i = 0 ; i < Count; i++ )
        Temp = Temp->link;
    return Temp->Element[_Index%m_Size];
}

template< typename TypeElement >
void EPN_MP<TypeElement> :: SortPull()
{
	for(int Cnt = 0; Cnt < m_NCount*m_Size-1; ++Cnt)
	{
		TypeElement* pData = SearchElement(Cnt);
		if(!pData)
		{
			TypeElement** pDataAddr = SearchArray(Cnt);
			for(int Cnt2 = Cnt+1; Cnt2 < m_NCount*m_Size; ++Cnt2)
			{
				TypeElement* pData2 = SearchElement(Cnt2);
				if(pData2)
				{
					TypeElement** pDataAddr2 = SearchArray(Cnt2);
					*pDataAddr = *pDataAddr2;
					*pDataAddr2 = 0;
					break;
				}
			}
		}
	}
}
// 데이터 삽입 삽입된 메모리 element index를 반환 이를 가지고있으면 후에 언제든 참조가 가능
template< typename TypeElement >
short EPN_MP<TypeElement> :: InsertData(TypeElement *_Element)
{
    if(m_NCount == 0)
    {
        m_Head = new Node;
        m_Head->Element = new TypeElement* [m_Size];
        for(int initc = 0; initc < m_Size; initc++)
        {
            m_Head->Element[initc] = 0;
        }
        m_Head->link = 0;
        m_NCount++;
    }
    if( m_ECount == ( m_NCount*m_Size ))
    {
        Node* Temp = 0;
        Temp = new Node;
        Temp->Element = new TypeElement* [m_Size];
        for(int initc = 0; initc < m_Size; initc++)
        {
            Temp->Element[initc] = 0;
        }
        Temp->link = 0;

        /*링크의 가장 뒤에 노드를 삽입해야함 이는 인덱스 순서를 앞에서 부터 부여하기 때문*/
        for(Node* TempCount = m_Head; ; TempCount = TempCount->link)
        {
            if(TempCount->link == 0)
            {
                TempCount->link = Temp;
                break;
            }
        }
        m_NCount++;
    }

    int NodeCount = 0;
    for(Node* Temp = m_Head; Temp != 0; Temp = Temp->link)
    {
        for(int ElementCount = 0; ElementCount < m_Size; ElementCount++)
        {
            if(Temp->Element[ElementCount] == 0)
            {
                Temp->Element[ElementCount] = _Element;
                m_ECount++;
                return (NodeCount*m_Size) + (ElementCount);
            }
        }
        NodeCount++;
    }
    return -1;
}

template< typename TypeElement >
bool EPN_MP<TypeElement> :: DeleteData(short _Index)
{
    if(_Index < 0 || _Index >=  (m_NCount*m_Size) )
        return false;
    if(_Index < m_Size)
    {
        if(m_Head->Element[_Index] != 0)
        {
            delete m_Head->Element[_Index];
            m_Head->Element[_Index] = 0;
			m_ECount--;
            return true;
        }
        return false;
    }

    int Count = _Index / m_Size;
    Node* Temp = m_Head;
    for(int i = 0 ; i < Count; i++ )
        Temp = Temp->link;
    if(Temp->Element[_Index%m_Size] != 0)
    {
        delete Temp->Element[_Index%m_Size];
        Temp->Element[_Index%m_Size] = 0;
		m_ECount--;
        return true;
    }
    return false;
}

template< typename TypeElement >
short EPN_MP<TypeElement>  :: PoolSizeGet(void)
{
	return m_Size;
}

template< typename TypeElement >
int	EPN_MP<TypeElement>  ::	GetNodeCount(void)
{
	return m_NCount;
}
template< typename TypeElement >
TypeElement* EPN_MP<TypeElement> :: EraseData(short _Index)
{
    TypeElement* TempElement = 0;
    if(_Index < 0 || _Index >   m_ECount )
        return 0;
    if(_Index < m_Size)
    {
        TempElement = m_Head->Element[_Index];
        m_Head->Element[_Index] = 0;
        return TempElement;
    }

    int Count = _Index / m_Size;
    Node* Temp = m_Head;
    for(int i = 0 ; i < Count; i++ )
        Temp = Temp->link;
    TempElement = Temp->Element[_Index%m_Size];
    Temp->Element[_Index%m_Size] = 0;

    return TempElement;
}

template< typename TypeElement >
EPN_MP<TypeElement> :: ~EPN_MP()
{
    Node* DeleteNode = 0;
    Node* Temp = m_Head;
    while(Temp != 0)
    {
        DeleteNode = Temp;
        Temp = Temp->link;
        for(int i = 0; i < m_Size; i++)
        {
            if( DeleteNode->Element[i] )
            {
                delete (DeleteNode->Element[i]);
                DeleteNode->Element[i] = 0;
            }
        }
        delete DeleteNode;
        DeleteNode = 0;
    }
}


#endif // _EPN_MP_H_
