/////////////////////////////////////////////////////////////
/*
	IOModule SoundLib Version
	FileName : Sound.h
	Credit : 붕어빵(shgustj12)
*/
/////////////////////////////////////////////////////////////

#ifndef _SOUNDH_
#define _SOUNDH_


#ifdef DLL_SOUND_CLASS
	#define DLLTYPE_SOUND __declspec(dllexport)
#else
	#define DLLTYPE_SOUND __declspec(dllimport)
#endif //DLL_EXPORT_CLASS


#define NULL 0

#pragma comment(lib, "irrKlang.lib")
#include <irrKlang.h>

using namespace irrklang;



class DLLTYPE_SOUND IO_Sound
{
private:

	float BGMvolume;
	float EFFECTvolume;

	ISoundEngine	*EngineSound;
	ISound			*EngineBGM;
	ISound			*EngineEFFECT;

public:
	
	bool SetVolumeBGM(int Value = 100);//BGM사운드크기 0~100
	bool StopBGM();//BGM임시정지
	bool RePlayBGM();//BGM다시재생
	bool SetSpeedBGM(float Value = 1);//BGM재생속도 디폴트값 1x
	bool PlayBGM(const char* route);//반복재생

	bool SetVolumeEFFECT(int Value = 100);//BGM사운드크기 0~100
	bool StopEFFECT();//EFFECT임시정지
	bool RePlayEFFECT();//EFFECT다시재생
	bool SetSpeedEFFECT(float Value = 1);//EFFECT재생속도 디폴트값 1x
	bool PlayEFFECT(const char* route);//한번재생

	ISoundEngine* GetSoundEngine();
	ISound* GetBGM();
	ISound* GetEFFECT();
public: // 생성자 소멸자
	IO_Sound();
	virtual ~IO_Sound();
public:
	static IO_Sound* m_pSound;

	static IO_Sound* CreateInstance(void);
};

#endif