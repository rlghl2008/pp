#ifndef _IO_SOUND_LIB_H_
#define _IO_SOUND_LIB_H_

#include "IO_Sound.h"

#ifdef _DEBUG   
	#pragma comment(lib, "D_IOSoundLib1.1.lib")
#else
	#pragma comment(lib, "R_IOSoundLib1.1.lib")
#endif

#endif