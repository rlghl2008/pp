package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class Character extends ObjectBase
{
	private boolean m_atkFlag;					//애니매이션 인덱스 1일때 한번만 공격 오브젝트 생성하기위한 용도
	private final int m_timeFreezeTime = 480;   //멈추는 시간
	private int m_timeFreezeCnt;				//멈추는 시간 카운트
	private boolean m_setFreezAnimFlag;			//애니매이션 멈추는 함수 한번만 호출하기 위한 플래그
	private AnimationSet m_timeAnim;			//시간 스킬 발동 시 캐릭터 위 시계 애니매이션
	private Pos m_revisionPos;					//애니메이션 실행 시 보정 위치
	private boolean m_animSetFlag;				//명령을 받을때 애니매이션 셋할 플래그
	private boolean m_atkAnimSetFlag;			//어택과 서있는 상태 전환하기위한 플래그
	private int m_animChangeCnt;				//애니매이션 바꾸는 딜레이 카운트
	private boolean m_getMoneyFlag;				//돈을 한번만 얻기위함
	private int m_hpbarPosX;					//hp바 위치조정용

	public Character(ObjectInfo objectInfo)
	{
		m_objectInfo = objectInfo;
		if (m_objectInfo.objectType == ObjectType.NULL)
			m_objectInfo.objectType = ObjectType.CHARACTER;

		m_objectInfo.maxHp = m_objectInfo.hp;

		m_revisionPos = new Pos(0, 0);

		m_animationSet.insertAnimation(new Animation(m_objectInfo.texturePath + "_walk.png", 4, 1, 0, 3, 7, true));		//걷는 애니매이션
		m_animationSet.insertAnimation(new Animation(m_objectInfo.texturePath + "_alert.png", 5, 1, 0, 4, 7, true));	//가만히 있는 애니매이션
		m_animationSet.insertAnimation(new Animation(m_objectInfo.texturePath + "_attack.png", 3, 1, 0, 2, 3, true));	//공격 애니매이션
		m_animationSet.insertAnimation(new Animation(m_objectInfo.texturePath + "_dead.png", 1, 1, 0, 1, 1, true));		//사망 애니매이션
		m_timeAnim = new AnimationSet();
		m_timeAnim.insertAnimation(new Animation("data/skill/freezTimer.png", 9, 1, 0, 8, m_timeFreezeTime / 9, true));

		m_animationSet.setCurrentAnim(0);
		m_timeAnim.setCurrentAnim(0);

		m_atkFlag = true;
		m_timeFreezeCnt = 0;
		m_setFreezAnimFlag = false;
		m_animationSet.setCurrentAnim(0);

		m_atkFlag = true;
		m_animSetFlag = false;
		m_atkAnimSetFlag = false;
		m_animChangeCnt = 0;
		m_getMoneyFlag = true;

		m_hpbarPosX = m_objectInfo.teamFlag ? 7 : -3;
	}

	@Override
	public void run(int cameraX)
	{
		m_revisionPos.X = 0;
		m_revisionPos.Y = 0;

		m_animationSet.run();

		if (m_objectInfo.enableFlag)
		{
			m_drawManager.draw(new DrawInfo("data/ingame/hpbar_bg.png", new Pos(m_objectInfo.pos.X - cameraX + m_hpbarPosX, m_objectInfo.pos.Y - 10), new Rect(0, 0, 381, 58)));
			m_drawManager.draw(new DrawInfo("data/ingame/hpbar.png", new Pos(m_objectInfo.pos.X - cameraX + m_hpbarPosX, m_objectInfo.pos.Y - 10), new Rect(0, 0, 188, 26), (float)m_objectInfo.hp / (float)m_objectInfo.maxHp, 1.f));
		}

		//시간스킬 발동시 작동(activeFlag가 flase될 시)
		if (!m_objectInfo.activeFlag)
		{
			m_objectCmd.cmdType = ObjectCommandType.STOP;
			m_timeAnim.run();

			//캐릭터 위 시계 카운트
			m_drawManager.draw(new DrawInfo(m_timeAnim.getCurrentTexture(), new Pos(m_objectInfo.pos.X - cameraX + 1, m_objectInfo.pos.Y - 40), m_timeAnim.getCurrentAnimRect(), 0.8f, 0.8f, false, 200.f));
			
			if (!m_setFreezAnimFlag)
			{
				m_setFreezAnimFlag = true;
				m_animationSet.setAnimPause();
			}
			else if (++m_timeFreezeCnt > m_timeFreezeTime)
			{
				m_timeFreezeCnt = 0;
				m_setFreezAnimFlag = false;
				m_animationSet.setAnimResume();
				m_objectInfo.activeFlag = true;
			}
		}

		/* 명령을 받았을 때 처리 */
		if (m_objectInfo.enableFlag)
		{
			switch (m_objectCmd.cmdType)
			{
			case STOP:
				m_animSetFlag = false;
				m_atkFlag = true;
				break;
			case MOVE_LEFT:
				m_objectInfo.pos.X -= m_objectInfo.speed;
				m_animationSet.setCurrentAnim(0);
				m_animSetFlag = false;
				m_atkFlag = true;
				break;
			case MOVE_RIGHT:
				m_objectInfo.pos.X += m_objectInfo.speed;
				m_animationSet.setCurrentAnim(0);
				m_animSetFlag = false;
				m_atkFlag = true;
				break;
			case ATTCK:
				if (!m_animSetFlag)
				{
					m_animationSet.setCurrentAnim(2);
					m_animSetFlag = true;
				}
				
				//공격 모션 중 1번째 인덱스일 때 공격 오브젝트 생성 && 공격후 공격 오브젝트 생성 불가능 상태
				if (m_atkFlag && m_animationSet.getCurrentanimIndex() == 2 && m_animationSet.getCurrentAnim().animIndex == 1)
				{
					if (m_objectInfo.objectType == ObjectType.CHARACTER_ADC)
					{
						Pos bulletPos = new Pos(m_objectInfo.pos.X, m_objectInfo.pos.Y);

						if (m_objectInfo.teamFlag)
						{
							bulletPos.X += 10;
						}
						bulletPos.Y += 30;
						
						//원거리 캐릭 쏘기
						m_objectMgr.addObject(new Bullet(m_objectInfo.atk, bulletPos, m_objectInfo.teamFlag));
					}
					else
					{
						m_objectMgr.addObject(new ProximityAttack(m_objectInfo.atk, m_objectInfo.pos, m_objectInfo.teamFlag));
					}
					m_atkFlag = false;
				}
				//두번째때 다시 공격 오브젝트 생성 가능 상태로 만듬
				else if (!m_atkFlag && m_animationSet.getCurrentanimIndex() == 2 && m_animationSet.getCurrentAnim().animIndex == 2)
					m_atkFlag = true;
				break;
			}
		}


		//명령 정보 초기화(오브젝트 run 의 맨 마지막에 해주어야 한다.)
		if (!m_objectCmd.cmdRetainFlag)
		{
			m_objectCmd.cmdType = ObjectCommandType.NULL;
			m_objectCmd.cmd = null;
		}

		//죽였을시 돈 획득
		if (!m_objectInfo.enableFlag)
		{
			if (m_getMoneyFlag)
			{
				switch (m_objectInfo.texturePath)
				{
				case "data/Character/F1/f1":
					m_playerInfo.m_gold += (350 + Math.random() * 50);
					break;
				case "data/Character/F2/f2":
					m_playerInfo.m_gold += (700 + Math.random() * 50);
					break;
				case "data/Character/F3/f3":
					m_playerInfo.m_gold += (1050 + Math.random() * 50);
					break;
				case "data/Character/F4/f4":
					m_playerInfo.m_gold += (2100 + Math.random() * 50);
					break;
				case "data/Character/F5/f5":
					m_playerInfo.m_gold += (3200 + Math.random() * 50);
					break;
				case "data/Character/F6/f6":
					m_playerInfo.m_gold += (5300 + Math.random() * 50);
					break;
				default:
					break;
				}
				m_getMoneyFlag = false;
			}

			m_objectInfo.pos.Y--;
			m_animationSet.setCurrentAnim(3);
			if (m_objectInfo.alpha > 0)
			{
				m_objectInfo.alpha -= 5;
				if (m_objectInfo.alpha < 0)
					m_objectInfo.alpha = 0;
			}
			else
				m_objectInfo.dieFlag = true;
		}

		m_drawManager.draw(new DrawInfo(m_animationSet.getCurrentTexture(), new Pos(m_objectInfo.pos.X + m_revisionPos.X - cameraX, m_objectInfo.pos.Y + m_revisionPos.X),
			m_animationSet.getCurrentAnimRect(), m_objectInfo.scalingX, m_objectInfo.scalingY, m_objectInfo.reversalFlag, m_objectInfo.alpha));

		if (m_objectInfo.enableFlag)
		{
			//공격 후 서있는 상태 만드는 부분
			if (!m_atkAnimSetFlag && m_animationSet.getCurrentanimIndex() == 2 && m_animationSet.getCurrentAnim().animIndex == m_animationSet.getCurrentAnim().endIndex)
			{
				if (++m_animChangeCnt > 2)
				{
					m_animationSet.setCurrentAnim(1);
					m_animationSet.setCurrentAnimIndex(0);
					m_atkAnimSetFlag = true;
					m_animChangeCnt = 0;
				}
			}
			else if (!m_atkAnimSetFlag && m_animationSet.getCurrentanimIndex() == 1 && m_animationSet.getCurrentAnim().animIndex == m_animationSet.getCurrentAnim().endIndex)
			{
				if (++m_animChangeCnt > 6)
				{
					m_animationSet.setCurrentAnim(2);
					m_animationSet.setCurrentAnimIndex(0);
					m_atkAnimSetFlag = true;
					m_animChangeCnt = 0;
				}
			}
			if (m_atkAnimSetFlag && m_animationSet.getCurrentAnim().animIndex == m_animationSet.getCurrentAnim().endIndex)
				m_atkAnimSetFlag = false;
		}
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
		//충돌한 대상이 공격 오브젝트 계열이 아니라 그 이외의 것일 경우
		if (!collidedObject.getObjectInfo().dieFlag && (collidedObject.getObjectInfo().objectType != ObjectType.PROXIMITY_ATK &&
			collidedObject.getObjectInfo().objectType != ObjectType.BULLET))
		{
			m_objectCmd.cmdType = ObjectCommandType.ATTCK;
		}
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
		//캐릭터가 맞았을 떄

		//충돌한 대상이 공격 오브젝트 계열일 경우
		if (!collidedObject.getObjectInfo().dieFlag && (collidedObject.getObjectInfo().objectType == ObjectType.PROXIMITY_ATK ||
			collidedObject.getObjectInfo().objectType == ObjectType.BULLET))
		{
			if (collidedObject.getObjectInfo().objectType == ObjectType.BULLET)
				m_objectMgr.addObject(new BulletHit(m_objectInfo.pos, m_objectInfo.reversalFlag));
			else if (collidedObject.getObjectInfo().objectType == ObjectType.PROXIMITY_ATK)
				m_objectMgr.addObject(new SwingHit(m_objectInfo.pos, m_objectInfo.reversalFlag));

			m_objectInfo.hp -= collidedObject.getObjectInfo().atk;
			if (m_objectInfo.hp <= 0)
			{
				m_objectInfo.hp = 0;
				if (m_objectInfo.objectType == ObjectType.CHARACTER || m_objectInfo.objectType == ObjectType.CHARACTER_ADC)
				{
					m_objectInfo.enableFlag = false;
				}
			}
			collidedObject.getObjectInfo().dieFlag = true;
		}
	}
}