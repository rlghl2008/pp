package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;

enum ButtonState
{
	NORMAL,	//버튼이 아무상태도 아닐떄
	ACTIVE,	//버튼위에 마우스가 올라가 있을 떄
	PUSH,	//버튼이 눌려진 상태일 때
	POP,	//버튼이 떼진 상태일 때
}

public class EXButton
{
	private String[] m_texturePath;			//버튼의 각 상태별 이미지(텍스쳐)
	private String  m_curTexturePath;		//현재 상태 이미지
	private Rect[] m_textureRect;			//버튼의 각 상태별 출력 Rect
	private Rect m_curTextureRect;			//현재 상태 렉트
	private ButtonState m_buttonState;		//버튼의 상태
	Pos	m_pos;					//버튼의 위치
	Rect m_rect;					//버튼 영역 (이 영역안에 마우스가 오면 이벤트 생김)

	EXButton()
	{
		m_texturePath = new String[4];
		m_textureRect = new Rect[4];
		m_buttonState = ButtonState.NORMAL;
		m_pos = new Pos(0, 0);
		m_rect = new Rect(-1, -1, -1, -1);
	}

	public void setTexture(String texturePath)
	{
		for (int i = 0; i < 4; ++i)
			m_texturePath[i] = texturePath;
	}

	public boolean setTexture(String texturePath, ButtonState state)
	{
		switch (state)
		{
		case NORMAL:
			m_texturePath[0] = texturePath;
			break;
		case ACTIVE:
			m_texturePath[1] = texturePath;
			break;
		case PUSH:
			m_texturePath[2] = texturePath;
			break;
		case POP:
			m_texturePath[3] = texturePath;
			break;
		default:
			return false;
		}
		return true;
	}

	public ButtonState getState()
	{
		return m_buttonState;
	}

	public Pos getPos()
	{
		return m_pos;
	}

	public void setPos(Pos pos)
	{
		m_pos.X = pos.X;
		m_pos.Y = pos.Y;
	}

	public void setRect(Rect rect)
	{
		m_rect.topX = rect.topX;
		m_rect.topY = rect.topY;
		m_rect.bottomX = rect.bottomX;
		m_rect.bottomY = rect.bottomY;
	}

	public void setTextureRect(Rect rect)
	{
		for (int i = 0; i < 4; ++i)
			m_textureRect[i] = rect;
	}

	public boolean setTextureRect(Rect rect, ButtonState state)
	{
		switch (state)
		{
		case NORMAL:
			m_textureRect[0] = rect;
			break;
		case ACTIVE:
			m_textureRect[1] = rect;
			break;
		case PUSH:
			m_textureRect[2] = rect;
			break;
		case POP:
			m_textureRect[3] = rect;
			break;
		default:
			return false;
		}
		return true;
	}

	public void run(Pos mousePos, MouseState mouseState)
	{
		if (m_buttonState == ButtonState.POP)
			m_buttonState = ButtonState.NORMAL;

		if (BasicFunction.checkCollision(new Rect((int)mousePos.X, (int)mousePos.Y, (int)mousePos.X, (int)mousePos.Y),
			new Rect(m_rect.topX + (int)m_pos.X, m_rect.topY + (int)m_pos.Y, m_rect.bottomX + (int)m_pos.X, m_rect.bottomY + (int)m_pos.Y)))
		{
			if (m_buttonState == ButtonState.NORMAL && mouseState == MouseState.NONE)
				m_buttonState = ButtonState.ACTIVE;
			else if (m_buttonState == ButtonState.ACTIVE && mouseState == MouseState.DOWN)
				m_buttonState = ButtonState.PUSH;
			else if (m_buttonState == ButtonState.PUSH && mouseState == MouseState.DOWNING)
				m_buttonState = ButtonState.PUSH;
			else if (m_buttonState == ButtonState.PUSH && mouseState == MouseState.UP)
				m_buttonState = ButtonState.POP;
		}
		else
		{
			if (m_buttonState == ButtonState.ACTIVE)
				m_buttonState = ButtonState.NORMAL;
			else if (m_buttonState == ButtonState.PUSH)
			{
				if (mouseState == MouseState.UP)
					m_buttonState = ButtonState.NORMAL;
			}
		}
		switch (m_buttonState)
		{
		case NORMAL:
			m_curTexturePath = m_texturePath[0];
			m_curTextureRect = m_textureRect[0];
			break;
		case ACTIVE:
			m_curTexturePath = m_texturePath[1];
			m_curTextureRect = m_textureRect[1];
			break;
		case PUSH:
			m_curTexturePath = m_texturePath[2];
			m_curTextureRect = m_textureRect[2];
			break;
		case POP:
			m_curTexturePath = m_texturePath[3];
			m_curTextureRect = m_textureRect[3];
			break;
		}

		DrawManager.createIntance().draw(new DrawInfo(m_curTexturePath, m_pos, m_curTextureRect, 1.f, 1.f, false));
	}
}