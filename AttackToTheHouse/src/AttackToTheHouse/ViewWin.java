package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ViewWin extends ViewBase
{
	private float m_alpha;

	EXButton m_buttonRank;
	EXButton m_buttonExit;

	private boolean m_btnStatCheck1;
	private boolean m_btnStatCheck2;
	private boolean m_soundCheck;

	public ViewWin(JPanel panel)
	{
		super(panel);
		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);
		
		m_buttonRank = new EXButton();
		m_buttonRank.setTexture("data/menu/ranking1.png");
		m_buttonRank.setTexture("data/menu/ranking2.png", ButtonState.PUSH);
		m_buttonRank.setTexture("data/menu/ranking3.png", ButtonState.ACTIVE);
		m_buttonRank.setPos(new Pos(250, 430));
		m_buttonRank.setRect(new Rect(0, 0, 484, 55));
		m_buttonRank.setTextureRect(new Rect(0, 0, 484, 55));

		m_buttonExit = new EXButton();
		m_buttonExit.setTexture("data/menu/exit1.png");
		m_buttonExit.setTexture("data/menu/exit2.png", ButtonState.PUSH);
		m_buttonExit.setTexture("data/menu/exit3.png", ButtonState.ACTIVE);
		m_buttonExit.setPos(new Pos(300, 520));
		m_buttonExit.setRect(new Rect(0, 0, 300, 55));
		m_buttonExit.setTextureRect(new Rect(0, 0, 300, 55));

		m_alpha = 0;

		m_btnStatCheck1 = false;
		m_btnStatCheck2 = false;
		m_soundCheck = false;
	}

	private void menuSound()
	{
		if (!m_soundCheck)
		{
			EXSound.playSound("data/sound/menu/menu.wav", false);
			m_soundCheck = true;
		}
	}

	private void fadeInOut()
	{
		m_alpha += 3.5f;
		if (m_alpha > 255)
			m_alpha = 255;
	}

	@Override
	public ViewState run()
	{
        //랭킹 저장
        //SaveRanking();
		
		MouseState mouseState = m_mouseEvent.getMouseState();		//마우스 상태
		Pos mousePos = m_mouseEvent.getMousePos();			//마우스 포스
										
		if (!m_btnStatCheck1 && !m_btnStatCheck2)                       //사운드 한번만 내기 위함
			m_soundCheck = false;

		/*버튼 소리 처리*/
		if (m_buttonRank.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck1 = true;
		}
		else if (m_buttonRank.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck1 = false;
		}
		if (m_buttonExit.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck2 = true;
		}
		else if (m_buttonExit.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck2 = false;
		}

		/*마우스 이벤트 처리*/
		if (m_buttonRank.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.RANKING;
		}
		else if (m_buttonExit.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			System.exit(0);
		}

		if (m_alpha != 255)
			fadeInOut();

		m_drawManager.draw(new DrawInfo("data/ending/winview.png", new Pos(0, 0), new Rect(0, 0, 819, 460), 1, 1.5f, false, m_alpha));

		m_buttonRank.run(mousePos, mouseState);
		m_buttonExit.run(mousePos, mouseState);
		return ViewState.WIN;
	}
}