package AttackToTheHouse;

import java.util.ArrayList;
import javax.swing.*;

enum ViewState	//뷰의 상태(뷰의 종류)
{
	VIEW_NULL,
	MENU,
	INGAME,
	RANKING,
	LOGO,
	INTRO,
	WIN,
	LOSE,
	HOWTOPLAY;
}

abstract class ViewBase extends JPanel
{
	protected MouseInputEvent m_mouseEvent;
	protected ArrayList<DrawInfo> m_drawList;
	protected JPanel panel;
	protected DrawManager m_drawManager;
	protected PlayerInfo m_playerInfo;

	public ViewBase(JPanel panel)
	{
		this.panel = panel;
		m_drawList = new ArrayList<DrawInfo>();
		m_mouseEvent = new MouseInputEvent();
		m_drawManager = DrawManager.createIntance();
		m_playerInfo = PlayerInfo.createIntance();

		EXSound.allStop();
	}

	abstract ViewState run();					//실행 가상함수
}
