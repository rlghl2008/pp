package AttackToTheHouse;

import javax.swing.JPanel;

public class ViewManager
{
	private ViewBase m_view;		//현재 뷰
	private ViewState m_viewState;	//현재 뷰의 상태
	private JPanel m_panel;

	public ViewManager(JPanel panel)
	{
		m_panel = panel;

		m_view = null;
		m_viewState = ViewState.VIEW_NULL;
	}

	public ViewBase getCurView()
	{
		return m_view;
	}

	public boolean changeView(ViewState view)
	{
		if (view == m_viewState)
			return true;

		System.gc();
		m_viewState = view;
		switch (view)
		{
		case MENU:
			m_view = new ViewMenu(m_panel);
			break;
		case INGAME:
			m_view = new ViewInGame(m_panel);
			break;
		case LOGO:
			m_view = new ViewLogo(m_panel);
			break;
		case INTRO:
			m_view = new ViewIntro(m_panel);
			break;
		case WIN:
			m_view = new ViewWin(m_panel);
			break;
		case LOSE:
			m_view = new ViewLose(m_panel);
			break;
		case HOWTOPLAY:
			m_view = new ViewHowToPlay(m_panel);
			break;
		case RANKING:
			m_view = new ViewRanking(m_panel);
			break;
		}

		return true;
	}

	public boolean run()
	{
		if (!changeView(m_view.run()))
			return false;
		return true;
	}
}