package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;
import javax.swing.JPanel;

public class ViewHowToPlay extends ViewBase
{
	EXButton m_backBtn;
	EXButton m_backBtnStart;

	public ViewHowToPlay(JPanel panel)
	{
		super(panel);
		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		m_backBtn = new EXButton();
		m_backBtn.setTexture("Data/menu/x_1.png");
		m_backBtn.setTexture("Data/menu/x_2.png", ButtonState.ACTIVE);
		m_backBtn.setTexture("Data/menu/x_3.png", ButtonState.PUSH);
		m_backBtn.setPos(new Pos(710, 0));
		m_backBtn.setRect(new Rect(0, 0, 80, 80));
		m_backBtn.setTextureRect(new Rect(0, 0, 80, 80));
	}

	@Override
	public ViewState run()
	{
		MouseState mouseState = m_mouseEvent.getMouseState();		//마우스 상태
		Pos mousePos = m_mouseEvent.getMousePos();					//마우스 포스

		m_drawManager.draw(new DrawInfo("Data/menu/HOW.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1, 1));

		m_backBtn.run(mousePos, mouseState);

		//나가기 버튼 눌리면
		if (m_backBtn.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.MENU;
		}
		return ViewState.HOWTOPLAY;
	}
}
