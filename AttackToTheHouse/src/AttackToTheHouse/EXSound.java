package AttackToTheHouse;
import java.io.File;
import java.util.ArrayList;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

class EXSound
{
	static ArrayList<Clip> bgmList = new ArrayList<Clip>();
	AudioInputStream m_audioInputStream;
	public static void allStop()
	{
		try
		{
			for (int i = 0; i < bgmList.size(); ++i)
				bgmList.get(i).close();
			bgmList.clear();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void playSound(String path, boolean isBgm) //bgm이면 반복함 
	{
		try
		{
			Clip clip;

			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(path));

			clip = AudioSystem.getClip();
			clip.stop();
			clip.open(audioInputStream);
			clip.start();
			if (isBgm)
			{
				bgmList.add(clip);
				clip.loop(-1);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}