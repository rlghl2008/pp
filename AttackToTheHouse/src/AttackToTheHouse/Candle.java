package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class Candle extends ObjectBase
{
	private int m_alpha;						//덮어쓸 하양색 양초의 알파값
	private float m_bombAlpha;					//폭팔시 하얀색 그림 알파
	private final int m_bombAliveTime = 30;     //폭팔시 하얀색 그림 유지시간
	private int m_bombAliveTimeCnt;				//폭팔시 하얀색 그림 유지시간 카운트
	private boolean m_candleStopFlag;			//양초가 일정 Y에 다다르면 멈추는 플래그
	private boolean m_changeFlag;				//페이드인 아웃용
	private boolean m_candleAliveFlag;			//양초 출력 플래그
	AnimationSet m_bombAnim;					//폭발 애니매이션
	private boolean m_soundFlag;				//사운드 한번만 출력하기위한 플래그
	private int m_beepCnt;						//띠띠띠 카운트

	public Candle()
	{
		m_objectInfo.texturePath = "data/skill/candle.png";
		m_objectInfo.pos = new Pos(178, -500);
		m_objectInfo.rect = new Rect(0, 0, 256, 256);
		m_objectInfo.collideRect = new Rect(0, 0, 0, 0);
		m_objectInfo.senseRect = new Rect(0, 0, 0, 0);
		m_objectInfo.speed = 3.f;

		m_bombAnim = new AnimationSet();
		m_bombAnim.insertAnimation(new Animation("data/skill/bomb.png", 4, 1, 0, 3, 70, false));
		m_bombAnim.setCurrentAnim(0);

		m_changeFlag = false;
		m_candleStopFlag = false;
		m_alpha = 0;
		m_bombAlpha = 1;
		m_bombAliveTimeCnt = 0;
		m_candleAliveFlag = true;
		m_soundFlag = true;
		m_beepCnt = 0;
	}

	private void beepSound()
	{
		++m_beepCnt;
		if (m_beepCnt == 1)
			EXSound.playSound("data/sound/ingame/c4_beep1.wav", false);
		else if (m_beepCnt == 50)
			EXSound.playSound("data/sound/ingame/c4_beep2.wav", false);
		else if (m_beepCnt == 100)
			EXSound.playSound("data/sound/ingame/c4_beep3.wav", false);
		else if (m_beepCnt == 150)
			EXSound.playSound("data/sound/ingame/c4_beep4.wav", false);
		else if (m_beepCnt == 200)
			EXSound.playSound("data/sound/ingame/c4_beep5.wav", false);
	}

	private void fadeInOut()
	{
		if (!m_changeFlag)
		{
			m_bombAlpha += 12.5f;
			if (m_bombAlpha > 255)
			{
				if (++m_bombAliveTimeCnt > m_bombAliveTime)
				{
					m_bombAliveTimeCnt = 0;
					m_changeFlag = true;
					m_candleAliveFlag = false;
				}
				m_bombAlpha = 255;
			}
		}
		else
		{
			m_bombAlpha -= 12.5f;
			if (m_bombAlpha < 0)
			{
				m_changeFlag = false;
				m_bombAlpha = 0;
			}
		}
	}

	@Override
	public void run(int cameraX)
	{
		if (m_candleStopFlag)
		{
			m_bombAnim.run();
			//알파가 255가되면 폭발할려는 애니매이션 출력 X
			if (m_alpha != 255)
				m_drawManager.draw(new DrawInfo(m_bombAnim.getCurrentTexture(), new Pos(0, 0), m_bombAnim.getCurrentAnimRect(), 1.f, 1.f, false, m_alpha));
		}
		//양초 출력
		if (m_candleAliveFlag)
			m_drawManager.draw(new DrawInfo(m_objectInfo.texturePath, new Pos(m_objectInfo.pos.X, m_objectInfo.pos.Y), m_objectInfo.rect, 1.7f, 1.7f));

		//양초가 땅에 다다르지 않았을때
		if (!m_candleStopFlag)
		{
			m_objectInfo.pos.Y += m_objectInfo.speed;
			if (m_objectInfo.pos.Y > 100)
				m_candleStopFlag = true;
		}
		else
		{
			beepSound();
			if (++m_alpha > 255)
				m_alpha = 255;
			//양초가 살아잇을때만 흰색 양초를 덮어씌움
			if (m_candleAliveFlag)
				m_drawManager.draw(new DrawInfo("data/skill/candle_white.png", new Pos(m_objectInfo.pos.X, m_objectInfo.pos.Y), m_objectInfo.rect, 1.7f, 1.7f, false, m_alpha));
		}
		//양초가 전부 하얘지면 폭발 그림(빤짝!) 출력 
		if (m_candleStopFlag && m_alpha == 255)
		{
			if (m_bombAlpha != 0)
			{
				fadeInOut();
			}
			else
				m_objectInfo.dieFlag = true;
			//화면이 완전히 하얀색이 됬을때 적팀 모두 삭제
			if (m_bombAlpha == 255)
			{
				if (m_soundFlag)
				{
					EXSound.playSound("data/sound/ingame/candleBomb.wav", false);
					m_soundFlag = false;
				}
				for (int i = 0; i < m_objectMgr.getObjectCount(); ++i)
				{
					ObjectBase curObject = m_objectMgr.getObject(i);
					//적군일때만
					if (curObject.getObjectInfo().teamFlag == false && curObject.getObjectInfo().objectType != ObjectType.TOWER)
					{
						curObject.m_objectInfo.enableFlag = false;
					}
				}
			}
			m_drawManager.draw(new DrawInfo("data/skill/flash.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1.f, 1.f, false, m_bombAlpha));
		}

	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}
}