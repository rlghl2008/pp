package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import javax.swing.JPanel;

public class ViewIntro extends ViewBase
{
	private float m_titleAlpha;
	private float m_textAlpha;
	private int m_blinkCnt;

	public ViewIntro(JPanel panel)
	{
		super(panel);

		m_titleAlpha = 0.f;
		m_textAlpha = 0.f;
		m_blinkCnt = 0;

		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		EXSound.playSound("data/sound/menu/title.wav", true);
	}

	@Override
	public ViewState run()
	{
		//페이드 아웃
		if (m_titleAlpha < 255)
		{
			m_titleAlpha += 7.5f;
			if (m_titleAlpha >= 255)
				m_titleAlpha = 255;
		}
		//페이드 아웃이 전부 진행되야 처리 받음
		else
		{
			//마우스 눌리면 메뉴로
			if (m_mouseEvent.getMouseState() == MouseState.DOWN)
			{
				EXSound.playSound("data/sound/menu/menu_click.wav", false);
				return ViewState.MENU;
			}
			//깜빡이는 처리
			if (++m_blinkCnt > 15)
			{
				m_blinkCnt = 0;
				if (m_textAlpha == 255)
					m_textAlpha = 0;
				else
					m_textAlpha = 255;
			}
		}

		m_drawManager.draw(new DrawInfo("Data/bg.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1.f, 1.f, false, m_titleAlpha));
		m_drawManager.draw(new DrawInfo("Data/menu/title.png", new Pos(50, -120), new Rect(0, 0, 700, 700), 1.f, 1.f, false, m_titleAlpha));
		m_drawManager.draw(new DrawInfo("Data/menu/press_text.png", new Pos(160, 420), new Rect(0, 0, 800, 600), 1.f, 1.f, false, m_textAlpha));

		return ViewState.INTRO;
	}
}