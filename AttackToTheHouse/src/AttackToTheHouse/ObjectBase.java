package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

enum ObjectType     //오브젝트 타입
{
	NULL,
	TOWER,			//타워
	MINION,			//미니언
	CHARACTER,		//근접 공격 캐릭터
	CHARACTER_ADC,  //원거리 공격 캐릭터
	PROXIMITY_ATK,	//근접공격 오브젝트
	BULLET,			//탄알(원거리 공격 오브젝트)
	EFFECT,			//이펙트 오브젝트
	MAX
}

enum ObjectCommandType          //객체에 전달하는 명령어 타입
{
	NULL,
	ADDOBJECT,
	STOP,
	MOVE_LEFT,
	MOVE_RIGHT,
	ATTCK,
	DEAD

}
class ObjectCommand
{
	ObjectCommandType cmdType;	//명령 타입
	String cmd;					//명령 내용
	boolean cmdRetainFlag;		//명령 유지 플래그(이 플래그가 true이면 명령을 한번해도 초기화되지않음)

	public ObjectCommand()
	{
		cmdType = ObjectCommandType.NULL;
		cmd = null;
		cmdRetainFlag = false;
	}
	public ObjectCommand(ObjectCommandType cmdType, String cmd)
	{
		this.cmdType = cmdType;
		this.cmd = cmd;
		this.cmdRetainFlag = false;
	}
	public ObjectCommand(ObjectCommandType cmdType, String cmd, boolean cmdRetainFlag)
	{
		this.cmdType = cmdType;
		this.cmd = cmd;
		this.cmdRetainFlag = cmdRetainFlag;
	}
}

class ObjectInfo
{
	String texturePath;		//이미지 경로
	int atk;				//공격력
	int hp;					//현재체력
	int maxHp;				//최대체력	
	Pos pos;				//위치
	Rect rect;				//렉트
	Rect collideRect;		//충돌렉트
	Rect senseRect;			//감지 렉트(공격 혹은 어떠한 다른 대상을 감지하는 영역의 렉트)
	float speed;			//스피드
	float scalingX;			//가로 확대 배율
	float scalingY;			//세로 확대 배율
	boolean teamFlag;		//true 아군, false 적군
	boolean reversalFlag;	//true 반전, false 정상
	float alpha;			//투명도값 0~255
	ObjectType objectType;	//오브젝트 타입
	boolean enableFlag;		//유효성 플래그 (false 가 되면 충돌, 감지의 영향을 받지않게됨)
	boolean dieFlag;        //true되면 죽음
	boolean activeFlag;		//상대 동작 플래그(시간 스킬용)
	boolean animSetFlag;

	ObjectInfo()
	{
		texturePath = null;
		atk = 0;
		hp = 0;
		maxHp = 0;
		pos = null;
		rect = null;
		collideRect = null;
		senseRect = null;
		speed = 0;
		scalingX = 1.f;
		scalingY = 1.f;
		teamFlag = true;
		reversalFlag = false;
		alpha = 255;
		dieFlag = false;
		enableFlag = true;
		activeFlag = true;
		animSetFlag = false;
	}
}

public abstract class ObjectBase
{
	protected ObjectInfo m_objectInfo;		//오브젝트 정보
	protected AnimationSet m_animationSet;	//애니매이션용 변수
	protected ObjectCommand m_objectCmd;	//오브젝트에 전달할 명령어
	protected DrawInfo m_drawInfo;			//그리는데 필요한 정보
	protected DrawManager m_drawManager;
	protected PlayerInfo m_playerInfo;
	protected ObjectManager m_objectMgr;

	public ObjectBase()
	{
		m_objectInfo = new ObjectInfo();
		m_animationSet = new AnimationSet();
		m_objectCmd = new ObjectCommand();
		m_drawInfo = new DrawInfo();
		m_drawManager = DrawManager.createIntance();
		m_playerInfo = PlayerInfo.createIntance();
		m_objectMgr = ObjectManager.createIntance();
	}

	//명령어 설정하는 함수
	public void setCommand(ObjectCommandType cmdType)
	{
		m_objectCmd.cmdType = cmdType;
		m_objectCmd.cmd = null;
		m_objectCmd.cmdRetainFlag = false;
	}
	public void setCommand(ObjectCommandType cmdType, String cmd)
	{
		m_objectCmd.cmdType = cmdType;
		m_objectCmd.cmd = cmd;
		m_objectCmd.cmdRetainFlag = false;
	}
	public void setCommand(ObjectCommandType cmdType, String cmd, boolean cmdRetainFlag)
	{
		m_objectCmd.cmdType = cmdType;
		m_objectCmd.cmd = cmd;
		m_objectCmd.cmdRetainFlag = cmdRetainFlag;
	}

	//오브젝트 정보 받아오는 함수
	public ObjectInfo getObjectInfo()
	{
		return m_objectInfo;
	}
	public DrawInfo getDrawInfo()
	{
		return m_drawInfo;
	}

	//가상(추상) 함수
	abstract public void run(int cameraX);
	abstract public void isSensed(ObjectBase collidedObject);	//감지 되었을 때 (오브젝트의 감지 영역에 상대의 충돌영역이 충돌했을 때)
	abstract public void isCollided(ObjectBase collidedObject);     //충돌 되었을 때 (오브젝트의 충돌영역에 상대의 충돌영역이 충돌했을 때)
}
