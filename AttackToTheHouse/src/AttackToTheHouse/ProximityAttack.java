package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class ProximityAttack extends ObjectBase
{
	private int dieCnt;

	public ProximityAttack(int atk, Pos pos, boolean teamFlag)
	{
		m_objectInfo.objectType = ObjectType.PROXIMITY_ATK;
		m_objectInfo.teamFlag = teamFlag;
		m_objectInfo.atk = atk;
		m_objectInfo.pos = teamFlag ? new Pos(pos.X + 35, pos.Y) : new Pos(pos.X - 25, pos.Y);
		m_objectInfo.collideRect = teamFlag ? new Rect(0, 0, 30, 60) : new Rect(0, 0, -30, 60);
		m_objectInfo.senseRect = new Rect(0, 0, 0, 0);
		m_objectInfo.reversalFlag = teamFlag ? true : false;

		m_animationSet.insertAnimation(new Animation("data/atk/swingEffect.png", 2, 1, 0, 1, 2, true));
		m_animationSet.setCurrentAnim(0);

		dieCnt = 0;
		EXSound.playSound("data/sound/object/shot.wav", false);
	}

	@Override
	public void run(int cameraX)
	{
		//애니메이션 작동
		m_animationSet.run();

		if (++dieCnt > 10)
			m_objectInfo.dieFlag = true;

		//명령 정보 초기화
		if (!m_objectCmd.cmdRetainFlag)
		{
			m_objectCmd.cmdType = ObjectCommandType.NULL;
			m_objectCmd.cmd = null;
		}

		m_drawManager.draw(new DrawInfo(m_animationSet.getCurrentTexture(), new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y),
			m_animationSet.getCurrentAnimRect(), 0.8f, 0.8f, m_objectInfo.reversalFlag));
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
		//충돌한 대상이 캐릭터 계열인 경우 상대(캐릭터)의 피를 감소 시킨다.
		if (collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER ||
			collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER_ADC ||
			collidedObject.getObjectInfo().objectType == ObjectType.TOWER)
		{
			m_objectMgr.addObject(new SwingHit(collidedObject.getObjectInfo().pos, collidedObject.getObjectInfo().reversalFlag));
			collidedObject.getObjectInfo().hp -= m_objectInfo.atk;
			collidedObject.getObjectInfo().hp -= m_objectInfo.atk;
			if (collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER || collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER_ADC)
			{
				collidedObject.getObjectInfo().enableFlag = false;
			}
			m_objectInfo.dieFlag = true;
		}
	}
}