package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class SwingHit extends ObjectBase
{
	private int dieCnt;

	public SwingHit(Pos pos, boolean reversalFlag)
	{
		dieCnt = 0;
		m_objectInfo.pos = new Pos(pos.X + 7, pos.Y + 20);
		m_objectInfo.collideRect = new Rect(0, 0, 0, 0);
		m_objectInfo.senseRect = new Rect(0, 0, 0, 0);
		m_objectInfo.reversalFlag = !reversalFlag;
	}

	@Override
	public void run(int cameraX)
	{
		m_drawManager.draw(new DrawInfo("data/atk/swingHit.png", new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y),
			new Rect(0, 0, 63, 73), 0.5f, 0.5f, m_objectInfo.reversalFlag));
		if (++dieCnt > 6)
			m_objectInfo.dieFlag = true;
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
	}
}