/* 게임 오브젝트를 관리하는 클래스 입니다. */
package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;
import java.util.ArrayList;
import java.util.Random;

public class ObjectManager
{
	private static ObjectManager m_objectMgr = null;
	private ArrayList<ObjectBase> m_objectList;
	private boolean m_continueFlag;

	public ObjectManager()
	{
		m_objectList = new ArrayList<ObjectBase>();
		m_continueFlag = false;
	}

	//싱글톤 받기용
	static public ObjectManager createIntance()
	{
		if (m_objectMgr == null)
			m_objectMgr = new ObjectManager();
		return m_objectMgr;
	}

	public void clearObject()
	{
		if(m_objectList.size() > 0)
			m_objectList.clear();
	}

	//현재 오브젝트 개수 반환하는 함수
	public int getObjectCount()
	{
		return m_objectList.size();
	}

	//오브젝트 추가하는 함수
	public int addObject(ObjectBase object)
	{
		m_objectList.add(object);
		return m_objectList.size() - 1;
	}

	//오브젝트 받아오는 함수
	public ObjectBase getObject(int index)
	{
		if (index < 0 || index > m_objectList.size() - 1)
			return null;
		return m_objectList.get(index);
	}

	//오브젝트 삭제하는 함수
	public boolean removeObject(int index)
	{
		if (index < 0 || index > m_objectList.size() - 1)
			return false;
		m_objectList.remove(index);
		return true;
	}
	
	private ObjectInfo getCharacterInfo(String characterType)
	{
		ObjectInfo objInfo = new ObjectInfo();
		switch(characterType)
		{
			case "F1":
				objInfo.texturePath = "data/Character/F1/f1";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 110;
				objInfo.atk = 15;
				objInfo.speed = 2.f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "F2":
				objInfo.texturePath = "data/Character/F2/f2";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 200;
				objInfo.atk = 20;
				objInfo.speed = 2.f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "F3":
				objInfo.texturePath = "data/Character/F3/f3";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 150;
				objInfo.atk = 15;
				objInfo.speed = 1.5f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER_ADC;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 350, 100) : new Rect(-350, 0, 0, 100);
				break;
			case "F4":
				objInfo.texturePath = "data/Character/F4/f4";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 450;
				objInfo.atk = 35;
				objInfo.speed = 2.5f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "F5":
				objInfo.texturePath = "data/Character/F5/f5";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 200;
				objInfo.atk = 20;
				objInfo.speed = 2.f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER_ADC;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 350, 100) : new Rect(-350, 0, 0, 100);
				break;
			case "F6":
				objInfo.texturePath = "data/Character/F6/f6";
				objInfo.collideRect = new Rect(0, 0, 70, 57);
				objInfo.hp = 550;
				objInfo.atk = 40;
				objInfo.speed = 3.0f;
				objInfo.teamFlag = false;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "T1":
				objInfo.texturePath = "data/Character/T1/t1";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 100;
				objInfo.atk = 10;
				objInfo.speed = 2.f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "T2":
				objInfo.texturePath = "data/Character/T2/t2";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 100;
				objInfo.atk = 15;
				objInfo.speed = 2.f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER_ADC;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 350, 100) : new Rect(-350, 0, 0, 100);
				break;
			case "T3":
				objInfo.texturePath = "data/Character/T3/t3";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 250;
				objInfo.atk = 25;
				objInfo.speed = 1.5f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "T4":
				objInfo.texturePath = "data/Character/T4/t4";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 400;
				objInfo.atk = 25;
				objInfo.speed = 2.5f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
			case "T5":
				objInfo.texturePath = "data/Character/T5/t5";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 250;
				objInfo.atk = 35;
				objInfo.speed = 2.f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER_ADC;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 350, 100) : new Rect(-350, 0, 0, 100);
				break;
			case "T6":
				objInfo.texturePath = "data/Character/T6/t6";
				objInfo.collideRect = new Rect(0, 0, 50, 57);
				objInfo.hp = 600;
				objInfo.atk = 40;
				objInfo.speed = 3.5f;
				objInfo.teamFlag = true;
				objInfo.reversalFlag = objInfo.teamFlag ? true : false;
				objInfo.objectType = ObjectType.CHARACTER;
				objInfo.senseRect = objInfo.teamFlag ? new Rect(0, 0, 50, 100) : new Rect(-50, 0, 0, 100);
				break;
		}
		return objInfo;
	}

	public void addCharacter(String characterType)
	{
		ObjectInfo objInfo = getCharacterInfo(characterType);

		Random random = new Random();
		int range = random.nextInt(20);

		objInfo.pos = objInfo.teamFlag ? new Pos(150, 410 + range) : new Pos(1420, 410 + range);
		ObjectBase character = new Character(objInfo);
		character.setCommand(objInfo.teamFlag ? ObjectCommandType.MOVE_RIGHT : ObjectCommandType.MOVE_LEFT, null, true);
		addObject(character);
	}

	//오브젝트 동작
	public void run(int cameraX)
	{
		int objListSize = m_objectList.size();
		/* 실제 동작 루틴 */
		for (int i = 0; i < objListSize; ++i)
		{
			ObjectBase curObject = m_objectList.get(i);
			curObject.setCommand(curObject.getObjectInfo().teamFlag ? ObjectCommandType.MOVE_RIGHT : ObjectCommandType.MOVE_LEFT, null, true);
		}
                
		for (int i = 0; i < objListSize; ++i)
		{
			for (int k = 0; k < objListSize; ++k)
			{
				if (i == k)
					continue;

				ObjectBase curObject = m_objectList.get(i);			//가해자
				ObjectBase curTargetObject = m_objectList.get(k);	//피해자

																	
				Pos realPos = curObject.m_objectInfo.pos;			//가해자 오브젝트 실제 감지 영역
				Rect realSenseRect = curObject.m_objectInfo.senseRect;
				Rect realResultSenseRect = new Rect
				(
					realSenseRect.topX + (int)realPos.X,
					realSenseRect.topY + (int)realPos.Y,
					realSenseRect.bottomX + (int)realPos.X,
					realSenseRect.bottomY + (int)realPos.Y
				);

				//피해자 오브젝트 실제 충돌 감지 영역(절대 Rect)
				Pos realTargetPos = curTargetObject.m_objectInfo.pos;
				Rect realTargetCollideRect = curTargetObject.m_objectInfo.collideRect;
				Rect realTargetResultCollideRect = new Rect
				(
					realTargetCollideRect.topX + (int)realTargetPos.X,
					realTargetCollideRect.topY + (int)realTargetPos.Y,
					realTargetCollideRect.bottomX + (int)realTargetPos.X,
					realTargetCollideRect.bottomY + (int)realTargetPos.Y
				);

				//팀 플래그가 서로 다를경우 감지
				if ((curObject.getObjectInfo().teamFlag != curTargetObject.getObjectInfo().teamFlag && curObject.getObjectInfo().enableFlag && curTargetObject.getObjectInfo().enableFlag))
				{
					//감지 체크
					if (BasicFunction.checkCollision(realResultSenseRect, realTargetResultCollideRect))
						curObject.isSensed(curTargetObject);
				}
			}
		}

		/* 오브젝트 충돌처리 */
		for (int i = 0; i < objListSize; ++i)
		{
			for (int k = i + 1; k < objListSize; ++k)
			{
				ObjectBase curObject = m_objectList.get(i);			//가해자
				ObjectBase curTargetObject = m_objectList.get(k);	//피해자

																	//오브젝트 위치
				Pos realPos = curObject.m_objectInfo.pos;

				//가해자 오브젝트 실제 충돌 영역(절대 Rect)
				Rect realCollideRect = curObject.m_objectInfo.collideRect;
				Rect realResultCollideRect = new Rect
				(
					realCollideRect.topX + (int)realPos.X,
					realCollideRect.topY + (int)realPos.Y,
					realCollideRect.bottomX + (int)realPos.X,
					realCollideRect.bottomY + (int)realPos.Y
				);


				//피해자 오브젝트 실제 충돌 감지 영역(절대 Rect)
				Pos realTargetPos = curTargetObject.m_objectInfo.pos;
				Rect realTargetCollideRect = curTargetObject.m_objectInfo.collideRect;
				Rect realTargetResultCollideRect = new Rect
				(
					realTargetCollideRect.topX + (int)realTargetPos.X,
					realTargetCollideRect.topY + (int)realTargetPos.Y,
					realTargetCollideRect.bottomX + (int)realTargetPos.X,
					realTargetCollideRect.bottomY + (int)realTargetPos.Y
				);

				//모두 dieFlag가 꺼져있고 팀 플래그가 서로 다를경우 충돌 체크
				if (!curObject.getObjectInfo().dieFlag && !curTargetObject.getObjectInfo().dieFlag
					&& (curObject.getObjectInfo().teamFlag != curTargetObject.getObjectInfo().teamFlag))
				{
					//충돌 체크
					if (BasicFunction.checkCollision(realResultCollideRect, realTargetResultCollideRect) && curObject.getObjectInfo().enableFlag && curTargetObject.getObjectInfo().enableFlag)
					{
						curObject.isCollided(curTargetObject);
					}
				}
			}
		}

		/* 실제 동작 루틴 */
		for (int i = 0; i < m_objectList.size(); ++i)
		{
			//오브젝트 동작
			ObjectBase curObject = m_objectList.get(i);

			curObject.run(cameraX);

			//오브젝트 dieFlag 체크
			if (curObject.m_objectInfo.dieFlag)
			{
				removeObject(i--);
				continue;
			}
		}
	}
}