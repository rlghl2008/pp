package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import javax.swing.JPanel;

public class ViewMenu extends ViewBase
{
	EXButton m_buttonStart;			//시작 버튼
	EXButton m_buttonHow;			//게임방법 버튼
	EXButton m_buttonRank;			//랭킹 버튼
	EXButton m_buttonExit;			//나가기 버튼

	AnimationSet[] m_aniCharacter;  //캐릭터 애니매이션 배열
	AnimationSet m_aniChicken1;		//치킨1
	AnimationSet m_aniChicken2;		//치킨2

	private boolean m_soundCheck;   //사운드 한번만 내기위한 변수들
	private boolean[] m_btnStatCheck;

	public ViewMenu(JPanel panel)
	{
		super(panel);
		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		m_buttonStart = new EXButton();
		m_buttonStart.setTexture("Data/menu/start1.png");
		m_buttonStart.setTexture("Data/menu/start2.png", ButtonState.PUSH);
		m_buttonStart.setTexture("Data/menu/start3.png", ButtonState.ACTIVE);
		m_buttonStart.setPos(new Pos(270, 250));
		m_buttonStart.setRect(new Rect(0, 0, 244, 57));
		m_buttonStart.setTextureRect(new Rect(0, 0, 244, 57));

		m_buttonHow = new EXButton();
		m_buttonHow.setTexture("Data/menu/howtoplay1.png");
		m_buttonHow.setTexture("Data/menu/howtoplay2.png", ButtonState.PUSH);
		m_buttonHow.setTexture("Data/menu/howtoplay3.png", ButtonState.ACTIVE);
		m_buttonHow.setPos(new Pos(160, 320));
		m_buttonHow.setRect(new Rect(0, 0, 489, 57));
		m_buttonHow.setTextureRect(new Rect(0, 0, 489, 57));

		m_buttonRank = new EXButton();
		m_buttonRank.setTexture("Data/menu/ranking1.png");
		m_buttonRank.setTexture("Data/menu/ranking2.png", ButtonState.PUSH);
		m_buttonRank.setTexture("Data/menu/ranking3.png", ButtonState.ACTIVE);
		m_buttonRank.setPos(new Pos(250, 390));
		m_buttonRank.setRect(new Rect(0, 0, 306, 57));
		m_buttonRank.setTextureRect(new Rect(0, 0, 306, 57));

		m_buttonExit = new EXButton();
		m_buttonExit.setTexture("Data/menu/exit1.png");
		m_buttonExit.setTexture("Data/menu/exit2.png", ButtonState.PUSH);
		m_buttonExit.setTexture("Data/menu/exit3.png", ButtonState.ACTIVE);
		m_buttonExit.setPos(new Pos(295, 460));
		m_buttonExit.setRect(new Rect(0, 0, 220, 57));
		m_buttonExit.setTextureRect(new Rect(0, 0, 220, 57));

		m_aniChicken1 = new AnimationSet();
		m_aniChicken1.insertAnimation(new Animation("data/menu/chicken_stand.png", 4, 1, 0, 3, 10, true));	//가만히 있는 애니매이션
		m_aniChicken1.setCurrentAnim(0);

		m_aniChicken2 = new AnimationSet();
		m_aniChicken2.insertAnimation(new Animation("data/menu/chicken_stand.png", 4, 1, 0, 3, 10, true));	//가만히 있는 애니매이션
		m_aniChicken2.setCurrentAnim(0);

		m_aniCharacter = new AnimationSet[8];
		for (int i = 0; i < 4; ++i)
		{
			m_aniCharacter[i] = new AnimationSet();
			m_aniCharacter[i].insertAnimation(new Animation("data/character/F" + (i + 1) + "/f" + (i + 1) + "_alert.png", 5, 1, 0, 4, 10, true));	//가만히 있는 애니매이션
			m_aniCharacter[i].insertAnimation(new Animation("data/character/F" + (i + 1) + "/f" + (i + 1) + "_attack.png", 3, 1, 0, 2, 10, true));	//공격하는 애니매이션
			m_aniCharacter[i].setCurrentAnim(0);
		}
		for (int i = 0; i < 4; ++i)
		{
			m_aniCharacter[i + 4] = new AnimationSet();
			m_aniCharacter[i + 4].insertAnimation(new Animation("data/character/T" + (i + 1) + "/t" + (i + 1) + "_alert.png", 5, 1, 0, 4, 10, true));	//가만히 있는 애니매이션
			m_aniCharacter[i + 4].insertAnimation(new Animation("data/character/T" + (i + 1) + "/t" + (i + 1) + "_attack.png", 3, 1, 0, 2, 10, true));	//공격하는 있는 애니매이션
			m_aniCharacter[i + 4].setCurrentAnim(0);
		}

		//메뉴(타이틀)
		m_btnStatCheck = new boolean[4];
		for (int i = 0; i < 4; ++i)
			m_btnStatCheck[i] = false;
		m_soundCheck = false;
	}

	private void menuSound()
	{
		if (!m_soundCheck)
		{
			EXSound.playSound("data/sound/menu/menu.wav", false);
			m_soundCheck = true;
		}
	}

	@Override
	public ViewState run()
	{
		MouseState mouseState = m_mouseEvent.getMouseState();		//마우스 상태
		Pos mousePos = m_mouseEvent.getMousePos();			//마우스 포스

		for (int i = 0; i < 8; ++i)
			m_aniCharacter[i].run();
		m_aniChicken1.run();
		m_aniChicken2.run();

		//배경, 타이틀
		m_drawManager.draw(new DrawInfo("Data/bg.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1, 1));
		m_drawManager.draw(new DrawInfo("Data/menu/menu_title.png", new Pos(95, 50), new Rect(0, 0, 700, 700), 1.f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/menu/seat1.png", new Pos(400, 40), new Rect(0, 0, 44, 64), 1.f, 1.f));

		//치킨
		m_drawManager.draw(new DrawInfo(m_aniChicken1.getCurrentTexture(), new Pos(132, 10), m_aniChicken1.getCurrentAnimRect(), 0.6f, 0.6f));
		m_drawManager.draw(new DrawInfo(m_aniChicken2.getCurrentTexture(), new Pos(537, 40), m_aniChicken2.getCurrentAnimRect(), 1.f, 1.f, true));

		//캐릭터들
		m_drawManager.draw(new DrawInfo(m_aniCharacter[0].getCurrentTexture(), new Pos(650, 250), m_aniCharacter[0].getCurrentAnimRect(), 1.f, 1.f));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[1].getCurrentTexture(), new Pos(650, 300), m_aniCharacter[1].getCurrentAnimRect(), 1.f, 1.f));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[2].getCurrentTexture(), new Pos(650, 380), m_aniCharacter[2].getCurrentAnimRect(), 1.f, 1.f));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[3].getCurrentTexture(), new Pos(650, 440), m_aniCharacter[3].getCurrentAnimRect(), 1.f, 1.f));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[4].getCurrentTexture(), new Pos(100, 250), m_aniCharacter[4].getCurrentAnimRect(), 1.f, 1.f, true));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[5].getCurrentTexture(), new Pos(100, 300), m_aniCharacter[5].getCurrentAnimRect(), 1.f, 1.f, true));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[6].getCurrentTexture(), new Pos(100, 380), m_aniCharacter[6].getCurrentAnimRect(), 1.f, 1.f, true));
		m_drawManager.draw(new DrawInfo(m_aniCharacter[7].getCurrentTexture(), new Pos(100, 440), m_aniCharacter[7].getCurrentAnimRect(), 1.f, 1.f, true));

		m_buttonStart.run(mousePos, mouseState);
		m_buttonHow.run(mousePos, mouseState);
		m_buttonRank.run(mousePos, mouseState);
		m_buttonExit.run(mousePos, mouseState);

		//사운드 한번만 나기위한
		if (!m_btnStatCheck[0] && !m_btnStatCheck[1] && !m_btnStatCheck[2] && !m_btnStatCheck[3])
			m_soundCheck = false;

		/*마우스 이벤트 처리*/
		if (m_buttonStart.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck[0] = false;
			m_aniCharacter[0].setCurrentAnim(0);
			m_aniCharacter[4].setCurrentAnim(0);
		}
		else if (m_buttonStart.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck[0] = true;
			m_aniCharacter[0].setCurrentAnim(1);
			m_aniCharacter[4].setCurrentAnim(1);
		}
		if (m_buttonHow.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck[1] = false;
			m_aniCharacter[1].setCurrentAnim(0);
			m_aniCharacter[5].setCurrentAnim(0);
		}
		else if (m_buttonHow.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck[1] = true;
			m_aniCharacter[1].setCurrentAnim(1);
			m_aniCharacter[5].setCurrentAnim(1);
		}
		if (m_buttonRank.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck[2] = false;
			m_aniCharacter[2].setCurrentAnim(0);
			m_aniCharacter[6].setCurrentAnim(0);
		}
		else if (m_buttonRank.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck[2] = true;
			m_aniCharacter[2].setCurrentAnim(1);
			m_aniCharacter[6].setCurrentAnim(1);
		}
		if (m_buttonExit.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck[3] = false;
			m_aniCharacter[3].setCurrentAnim(0);
			m_aniCharacter[7].setCurrentAnim(0);
		}
		else if (m_buttonExit.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck[3] = true;
			m_aniCharacter[3].setCurrentAnim(1);
			m_aniCharacter[7].setCurrentAnim(1);
		}

		if (m_buttonStart.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.INGAME;
		}
		else if (m_buttonHow.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.HOWTOPLAY;
		}
		else if (m_buttonRank.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.RANKING;
		}
		else if (m_buttonExit.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			System.exit(0);
		}

		return ViewState.MENU;
	}
}