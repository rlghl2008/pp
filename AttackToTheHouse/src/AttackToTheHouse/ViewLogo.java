package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import javax.swing.JPanel;

public class ViewLogo extends ViewBase
{
	private float m_alpha;
	private boolean m_changeFlag;

	public ViewLogo(JPanel panel)
	{
		super(panel);
		m_alpha = 1;
		m_changeFlag = false;
		EXSound.playSound("data/sound/logo/logo.wav", true);
	}

	private void fadeInOut()
	{
		if (!m_changeFlag)
		{
			m_alpha += 2.5f;
			if (m_alpha > 255)
			{
				m_changeFlag = true;
				m_alpha = 255;
			}
		}
		else
		{
			m_alpha -= 2.5f;
			if (m_alpha < 0)
			{
				m_changeFlag = false;
				m_alpha = 0;
			}
		}
	}

	@Override
	public ViewState run()
	{
		//페이드인아웃
		fadeInOut();

		m_drawManager.draw(new DrawInfo("data/menu/back.png", new Pos(0, 0), new Rect(0, 0, 819, 460), 1, 1.5f, false, m_alpha));
		m_drawManager.draw(new DrawInfo("data/menu/product.png", new Pos(250, 140), new Rect(0, 0, 600, 300), 1, 1.5f, false, m_alpha));

		if (m_alpha == 0)
			return ViewState.INTRO;
		return ViewState.LOGO;
	}
}
