package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import javax.swing.JPanel;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.io.IOException;

class RankData
{
	String name;
	int score;

	public RankData(String name, int score)
	{
		this.name = name;
		this.score = score;
	}
}

public class ViewRanking extends ViewBase
{
	private EXButton m_buttonBack;
	private ArrayList<RankData> m_rankDataList = new ArrayList<RankData>();

	public ViewRanking(JPanel panel)
	{
		super(panel);

		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		m_buttonBack = new EXButton();
		m_buttonBack.setTexture("Data/menu/x_1.png");
		m_buttonBack.setTexture("Data/menu/x_2.png", ButtonState.ACTIVE);
		m_buttonBack.setTexture("Data/menu/x_3.png", ButtonState.PUSH);
		m_buttonBack.setPos(new Pos(710, 0));
		m_buttonBack.setRect(new Rect(0, 0, 80, 80));
		m_buttonBack.setTextureRect(new Rect(0, 0, 80, 80));

		try
		{
			FileReader fr = new FileReader("data/ranking/Score.txt");
			BufferedReader br = new BufferedReader(fr);

			String read = null;
			//읽을 데이터가 없을 때 까지 읽는다.
			while ((read = br.readLine()) != null)
			{
				//토큰을 \\ 단위로 자른다.
				StringTokenizer st = new StringTokenizer(read, "\\");

				//남은 토큰이 없을 때 까지 읽는다.
				while (st.hasMoreTokens())
				{
					//이름과 점수 토큰을 하나씩 가져와서 랭크 데이터 리스트에 추가한다.
					String name = st.nextToken();
					int score = Integer.parseInt(st.nextToken());
					m_rankDataList.add(new RankData(name, score));
				}
			}

			//스트림들 다 닫음
			if (fr != null)
				fr.close();
			if (br != null)
				br.close();

			//점수를 기준으로 랭크 데이터 리스트를 정렬
			for (int i = 0; i < m_rankDataList.size() - 1; ++i)
			{
				for (int k = 0; k < m_rankDataList.size() - 1 - i; ++k)
				{
					if (m_rankDataList.get(k).score > m_rankDataList.get(k + 1).score)
					{
						//k번째 k+1번째 인덱스 서로 스왑
						Collections.swap(m_rankDataList, k, k + 1);
					}
				}
			}
			//결과 테스트
//			for(int i = 0; i < m_rankDataList.size(); ++i)
//				System.out.println(i + " 번  이름 :  " + m_rankDataList.get(i).name + "   점수 :  " + m_rankDataList.get(i).score );

		}
		//파일스트림 중 문제발생 시 종료
		catch (IOException ex)
		{
			System.err.println(ex);
			System.exit(1);
		}
	}

	public ViewState run()
	{
		MouseState mouseState = m_mouseEvent.getMouseState();	//마우스 상태
		Pos mousePos = m_mouseEvent.getMousePos();				//마우스 포스

		m_drawManager.draw(new DrawInfo("Data/ranking/ranking_black.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1, 1));
		m_drawManager.draw(new DrawInfo("Data/ranking/rankingview.png", new Pos(150, 10), new Rect(0, 0, 500, 430), 1, 1));
		m_drawManager.draw(new DrawInfo("Data/ranking/product.png", new Pos(360, 495), new Rect(0, 0, 80, 60), 1, 1));
		m_buttonBack.run(mousePos, mouseState);

		//1st부터 10th까지 이미지
		m_drawManager.draw(new DrawInfo("data/ranking/1st.png", new Pos(200, 150 + (0 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/2nd.png", new Pos(200, 150 + (1 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/3rd.png", new Pos(200, 150 + (2 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/4th.png", new Pos(200, 150 + (3 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/5th.png", new Pos(200, 150 + (4 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/6th.png", new Pos(200, 150 + (5 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/7th.png", new Pos(200, 150 + (6 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/8th.png", new Pos(200, 150 + (7 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/9th.png", new Pos(200, 150 + (8 * 25)), new Rect(0, 0, 50, 24), 0.8f, 0.8f));
		m_drawManager.draw(new DrawInfo("data/ranking/10th.png", new Pos(199, 150 + (9 * 25)), new Rect(0, 0, 68, 24), 0.8f, 0.8f));

		//1등부터 10등까지 점수이미지(시간으로 표시)
		for (int x = 0; x<10; ++x)
		{
			if (x >= m_rankDataList.size())
				break;
			m_drawManager.draw(new DrawInfo("data/ranking/number.png", new Pos(500, 150 + (x * 25)), new Rect(((m_rankDataList.get(x).score % 60) % 10) * 21, 0, 21 + ((m_rankDataList.get(x).score % 60) % 10) * 21, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/number.png", new Pos(480, 150 + (x * 25)), new Rect(((m_rankDataList.get(x).score / 10) % 6) * 21, 0, 21 + ((m_rankDataList.get(x).score / 10) % 6) * 21, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/ss.png", new Pos(470, 150 + (x * 25)), new Rect(0, 0, 5, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/number.png", new Pos(450, 150 + (x * 25)), new Rect((m_rankDataList.get(x).score / 60 % 10) * 21, 0, 21 + (m_rankDataList.get(x).score / 60 % 10) * 21, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/number.png", new Pos(430, 150 + (x * 25)), new Rect((m_rankDataList.get(x).score / 600) * 21, 0, 21 + (m_rankDataList.get(x).score / 600) * 21, 24), 0.8f, 0.8f));
		}

		//string을 char형으로 변환후 이미지 출력
		int[] nameAsciiCode = new int[3];
		for (int x = 0; x<10; ++x)
		{
			if (x >= m_rankDataList.size())
				break;
			
			nameAsciiCode[0] = (int)m_rankDataList.get(x).name.charAt(0);
			nameAsciiCode[1] = (int)m_rankDataList.get(x).name.charAt(1);
			nameAsciiCode[2] = (int)m_rankDataList.get(x).name.charAt(2);
			
			m_drawManager.draw(new DrawInfo("data/ranking/ranking_alpha.png", new Pos(300, 150 + (x * 25)), new Rect((nameAsciiCode[0] - 65) * 18, 0, 18 + (nameAsciiCode[0] - 65) * 18, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/ranking_alpha.png", new Pos(320, 150 + (x * 25)), new Rect((nameAsciiCode[1] - 65) * 18, 0, 18 + (nameAsciiCode[1] - 65) * 18, 24), 0.8f, 0.8f));
			m_drawManager.draw(new DrawInfo("data/ranking/ranking_alpha.png", new Pos(340, 150 + (x * 25)), new Rect((nameAsciiCode[2] - 65) * 18, 0, 18 + (nameAsciiCode[2] - 65) * 18, 24), 0.8f, 0.8f));
		}

		if (m_buttonBack.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.MENU;
		}

		return ViewState.RANKING;
	}
}
