package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;

public class EnemyTower extends ObjectBase
{
	private int m_levelTimeCnt;					//적 레벨 올라가는 시간 카운트
	private int m_level;						//적의 레벨
	private final int m_maxLevel = 10;			//맥스레벨
	private int m_summonTimeCnt;				//적 생성 시간 카운트
	private int m_summonTime;					//적 생성 시간
	private final int m_timeFreezeTime = 480;   //멈추는 시간
	private int m_timeFreezeCnt;				//멈추는 시간 카운트
	private boolean m_timeFreezeFlag;

	public EnemyTower()
	{
		m_objectInfo.objectType = ObjectType.TOWER;
		m_objectInfo.texturePath = "Data/Tower/tower_blue.png";
		m_objectInfo.pos = new Pos(1435, 230);
		m_objectInfo.rect = new Rect(0, 0, 54, 87);
		m_objectInfo.collideRect = new Rect(0, 0, (int)(54.f * 3), (int)(87.f*3.f));
		m_objectInfo.senseRect = new Rect(0, 0, (int)(54.f * 3), (int)(87.f*3.f));
		m_objectInfo.atk = 0;
		m_objectInfo.hp = 3000;
		m_objectInfo.maxHp = m_objectInfo.hp;
		m_objectInfo.speed = 0;
		m_objectInfo.scalingX = 3.f;
		m_objectInfo.scalingY = 3.f;
		m_objectInfo.reversalFlag = true;
		m_objectInfo.teamFlag = false;

		m_levelTimeCnt = 0;
		m_level = 0;

		m_summonTimeCnt = 0;
		m_summonTime = 300;
		
		m_timeFreezeFlag = false;
		m_timeFreezeCnt = 0;
	}

	//적 생성 멈추는 함수
	public void pause()
	{
		m_timeFreezeFlag = true;
	}

	@Override
	public void run(int cameraX)
	{
		m_drawManager.draw(new DrawInfo("data/ingame/hpbar_bg.png", new Pos(m_objectInfo.pos.X - cameraX + 117, m_objectInfo.pos.Y - 10), new Rect(0, 0, 381, 58)));
		m_drawManager.draw(new DrawInfo("data/ingame/hpbar.png", new Pos(m_objectInfo.pos.X - cameraX + 117, m_objectInfo.pos.Y - 10), new Rect(0, 0, 188, 26), (float)m_objectInfo.hp / (float)m_objectInfo.maxHp, 1.f));

		//레벨 올리는 부분
		if (m_levelTimeCnt > 1800)
		{
			m_levelTimeCnt = 0;
			if (m_level < m_maxLevel - 1)
				m_level++;
		}
		else
			m_levelTimeCnt++;
		
		//적 생성
		if(m_timeFreezeFlag == true)
		{
			m_timeFreezeCnt++;
			if(m_timeFreezeCnt > m_timeFreezeTime)
			{
				m_timeFreezeFlag = false;
				m_timeFreezeCnt = 0;
			}
		}
		else if (m_summonTimeCnt > m_summonTime)
		{
			m_summonTimeCnt = 0;
			switch (m_level)
			{
			case 0:
				m_objectMgr.addCharacter("F1");
				break;
			case 1:
				m_objectMgr.addCharacter("F1");
				m_objectMgr.addCharacter("F1");
				m_objectMgr.addCharacter("F2");
				break;
			case 2:
				m_summonTime = 450;
				m_objectMgr.addCharacter("F1");
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F2");
				break;
			case 3:
				m_summonTime = 420;
				m_objectMgr.addCharacter("F1");
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				break;
			case 4:
				m_summonTime = 380;
				m_objectMgr.addCharacter("F1");
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				break;
			case 5:
				m_summonTime = 350;
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				break;
			case 6:
				m_summonTime = 310;
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				m_objectMgr.addCharacter("F5");
				break;
			case 7:
				m_summonTime = 290;
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				break;
			case 8:
				m_summonTime = 280;
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				m_objectMgr.addCharacter("F6");
				break;
			case 9:
				m_summonTime = 270;
				m_objectMgr.addCharacter("F2");
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				m_objectMgr.addCharacter("F6");
				break;
			case 10:
				m_summonTime = 260;
				m_objectMgr.addCharacter("F3");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F4");
				m_objectMgr.addCharacter("F5");
				m_objectMgr.addCharacter("F5");
				m_objectMgr.addCharacter("F6");
				break;
			}
		}
		else
			m_summonTimeCnt++;

		//명령 정보 초기화
		if (!m_objectCmd.cmdRetainFlag)
		{
			m_objectCmd.cmdType = ObjectCommandType.NULL;
			m_objectCmd.cmd = null;
		}

		m_drawManager.draw(new DrawInfo(m_objectInfo.texturePath, new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y), m_objectInfo.rect,
			m_objectInfo.scalingX, m_objectInfo.scalingY, m_objectInfo.reversalFlag));
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
		//충돌한 대상이 공격 오브젝트 계열일 경우
		if (!collidedObject.getObjectInfo().dieFlag && (collidedObject.getObjectInfo().objectType == ObjectType.PROXIMITY_ATK ||
			collidedObject.getObjectInfo().objectType == ObjectType.BULLET) && collidedObject.getObjectInfo().teamFlag == true)
		{
			m_objectInfo.hp -= collidedObject.getObjectInfo().atk;
			m_objectMgr.addObject(new SwingHit(new Pos(m_objectInfo.pos.X + 82, m_objectInfo.pos.Y), m_objectInfo.reversalFlag));
			if (m_objectInfo.hp <= 0)
			{
				m_objectInfo.hp = 0;
				m_objectInfo.dieFlag = true;
			}
			collidedObject.getObjectInfo().dieFlag = true;
		}
	}
}