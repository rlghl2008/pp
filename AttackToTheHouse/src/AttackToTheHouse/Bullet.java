package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;

public class Bullet extends ObjectBase
{

	Bullet(int atk, Pos pos, boolean teamFlag)
	{
		m_objectInfo.objectType = ObjectType.BULLET;
		m_objectInfo.atk = atk;
		m_objectInfo.teamFlag = teamFlag;
		m_objectInfo.speed = 12.f;

		m_objectInfo.pos = teamFlag ? new Pos(pos.X + 30, pos.Y) : new Pos(pos.X - 10, pos.Y);
		m_objectInfo.reversalFlag = teamFlag ? true : false;
		m_objectInfo.collideRect = new Rect(0, 0, 50, 50);
		m_objectInfo.senseRect = new Rect(0, 0, 0, 0);

		m_animationSet.insertAnimation(new Animation("data/atk/bullet.png", 3, 1, 0, 2, 2, true));
		m_animationSet.setCurrentAnim(0);
		EXSound.playSound("data/sound/object/shoot.wav", false);
	}

	@Override
	public void run(int cameraX)
	{
		//애니메이션 작동
		m_animationSet.run();

		m_drawManager.draw(new DrawInfo(m_animationSet.getCurrentTexture(), new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y),
			m_animationSet.getCurrentAnimRect(), m_objectInfo.scalingX, m_objectInfo.scalingY, m_objectInfo.reversalFlag));
		m_objectInfo.pos.X += m_objectInfo.teamFlag ? m_objectInfo.speed : -m_objectInfo.speed;
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
		//충돌한 대상이 캐릭터 계열인 경우 상대(캐릭터)의 피를 감소 시킨다.
		if (collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER ||
			collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER_ADC ||
			collidedObject.getObjectInfo().objectType == ObjectType.TOWER)
		{
			m_objectMgr.addObject(new BulletHit(collidedObject.getObjectInfo().pos, collidedObject.getObjectInfo().reversalFlag));

			collidedObject.getObjectInfo().hp -= m_objectInfo.atk;
			if (collidedObject.getObjectInfo().hp <= 0)
			{
				collidedObject.getObjectInfo().hp = 0;
				if (collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER || collidedObject.getObjectInfo().objectType == ObjectType.CHARACTER_ADC)
				{
					collidedObject.getObjectInfo().enableFlag = false;
				}
			}

			m_objectInfo.dieFlag = true;	//감소시킨 후 나(공격 오브젝트)는 소멸
		}
	}
}
