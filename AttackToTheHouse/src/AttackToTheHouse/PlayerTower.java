package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class PlayerTower extends ObjectBase
{
	public PlayerTower()
	{
		m_objectInfo.objectType = ObjectType.TOWER;
		m_objectInfo.texturePath = "Data/Tower/tower_red.png";
		m_objectInfo.pos = new Pos(0, 230);
		m_objectInfo.rect = new Rect(0, 0, 54, 87);
		m_objectInfo.collideRect = new Rect(0, 0, (int)(54.f * 3), (int)(87.f*3.f));
		m_objectInfo.senseRect = new Rect(0, 0, (int)(54.f * 3), (int)(87.f*3.f));
		m_objectInfo.atk = 0;
		m_objectInfo.hp = 10000;
		m_objectInfo.maxHp = 10000;
		m_objectInfo.speed = 0;
		m_objectInfo.scalingX = 3.f;
		m_objectInfo.scalingY = 3.f;
		m_objectInfo.reversalFlag = false;
		m_objectInfo.teamFlag = true;
	}

	@Override
	public void run(int cameraX)
	{
		m_drawManager.draw(new DrawInfo("data/ingame/hpbar_bg.png", new Pos(m_objectInfo.pos.X - cameraX + 1, m_objectInfo.pos.Y - 10), new Rect(0, 0, 381, 58)));
		m_drawManager.draw(new DrawInfo("data/ingame/hpbar.png", new Pos(m_objectInfo.pos.X - cameraX + 1, m_objectInfo.pos.Y - 10), new Rect(0, 0, 188, 26), (float)m_objectInfo.hp / (float)m_objectInfo.maxHp, 1.f));
		m_drawManager.draw(new DrawInfo(m_objectInfo.texturePath, new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y), m_objectInfo.rect,
		m_objectInfo.scalingX, m_objectInfo.scalingY, m_objectInfo.reversalFlag));
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
		//충돌한 대상이 공격 오브젝트 계열일 경우
		if (!collidedObject.getObjectInfo().dieFlag && (collidedObject.getObjectInfo().objectType == ObjectType.PROXIMITY_ATK ||
			collidedObject.getObjectInfo().objectType == ObjectType.BULLET))
		{
			m_objectInfo.hp -= collidedObject.getObjectInfo().atk;
			m_objectMgr.addObject(new SwingHit(m_objectInfo.pos, m_objectInfo.reversalFlag));
			if (m_objectInfo.hp <= 0)
			{
				m_objectInfo.hp = 0;
				m_objectInfo.dieFlag = true;
			}
			collidedObject.getObjectInfo().dieFlag = true;
		}
	}
}