package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;

public class BulletHit extends ObjectBase
{
	private int dieCnt;

	public BulletHit(Pos pos, boolean reversalFlag)
	{
		EXSound.playSound("data/sound/object/shot.wav", false);

		m_objectInfo.objectType = ObjectType.EFFECT;
		dieCnt = 0;
		m_objectInfo.pos = new Pos(pos.X + 10, pos.Y - 10);
		m_objectInfo.collideRect = new Rect(0, 0, 0, 0);
		m_objectInfo.senseRect = new Rect(0, 0, 0, 0);
		m_animationSet.insertAnimation(new Animation("data/atk/bulletHit.png", 3, 1, 0, 2, 2, false));
		m_animationSet.setCurrentAnim(0);
		m_objectInfo.reversalFlag = !reversalFlag;
	}

	@Override
	public void run(int cameraX)
	{
		m_animationSet.run();
		m_drawManager.draw(new DrawInfo(m_animationSet.getCurrentTexture(), new Pos(m_objectInfo.pos.X - cameraX, m_objectInfo.pos.Y),
			m_animationSet.getCurrentAnimRect(), 0.5f, 0.5f, m_objectInfo.reversalFlag));
		if (++dieCnt > 10)
			m_objectInfo.dieFlag = true;
	}

	@Override
	public void isSensed(ObjectBase collidedObject)
	{
	}

	@Override
	public void isCollided(ObjectBase collidedObject)
	{
	}
}
