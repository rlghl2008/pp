package AttackToTheHouse;

public class EXFps
{
	boolean m_StartFlag;	//시작 플래그
	EXTimer m_timer;		//타이머
	long m_runTime;	
	long m_startTime;
	double m_frameRate;

	EXFps()
	{
		m_timer = new EXTimer();
		m_StartFlag = false;
	}

	boolean fpsLock(int fps)
	{
		//첫 시작
		if (!m_StartFlag)
		{
			m_StartFlag = true;
			m_timer.start();
			m_runTime = m_timer.getCurTime();
		}

		m_timer.run();
		m_runTime = m_timer.getCurTime();

		//현재시간 - 이전 프레임 실행 시간
		if (m_runTime - m_startTime >= 1000000000.0 / fps)
		{
			m_frameRate = (double)(1000000000.0 / (m_runTime - m_startTime));
			m_startTime = m_runTime;

			return true;
		}

		return false;
	}

	double getFrameRate()
	{
		return m_frameRate;
	}
}