/* 마우스 이벤트 처리를 하는 클래스 입니다. */
package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import java.awt.event.*;

enum MouseState
{
	NONE,       //아무 상태 아님
	DOWN,       //눌렀을 때
	DOWNING,    //누르는 중일 때
	UP          //눌렀다 땟을 때
}

public class MouseInputEvent implements MouseListener, MouseMotionListener
{
	private Pos m_mousePos;					//현재 마우스 위치
	private MouseState m_mouseState;		//반환할 마우스 상태
	private MouseState m_mouseStateCheck;	//리스너에서 받은 마우스 상태

	public MouseInputEvent()
	{
		m_mousePos = new Pos(0, 0);
		m_mouseState = MouseState.NONE;
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		m_mouseStateCheck = MouseState.DOWN;
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		m_mouseStateCheck = MouseState.UP;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		m_mousePos = new Pos(e.getX(), e.getY());
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {}

	//마우스 위치 가져오는 함수
	public Pos getMousePos()
	{
		return m_mousePos;
	}

	//마우스 클릭상태 가져오는 함수
	public MouseState getMouseState()
	{
		switch (m_mouseState)
		{
		case NONE:
			if (m_mouseStateCheck == MouseState.DOWN)
				m_mouseState = MouseState.DOWN;
			break;
		case DOWN:
			m_mouseState = MouseState.DOWNING;
			break;
		case DOWNING:
			m_mouseState = MouseState.DOWNING;
			if (m_mouseStateCheck == MouseState.UP)
				m_mouseState = MouseState.UP;
			break;
		case UP:
			m_mouseState = MouseState.NONE;
			break;
		}
		return m_mouseState;
	}
}