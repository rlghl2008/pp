package AttackToTheHouse;

public class PlayerInfo
{
	static public PlayerInfo m_playerInfo = null;

	public String m_playerName;
	public int m_gold;			//소지 골드
	public long m_playTime;                 //플레이 시간

	public PlayerInfo()
	{
		m_playerName = "";
		m_gold = 99999;
		m_playTime = 0;
	}

	//싱글톤 받기용
	static public PlayerInfo createIntance()
	{
		if (m_playerInfo == null)
			m_playerInfo = new PlayerInfo();
		return m_playerInfo;
	}
}