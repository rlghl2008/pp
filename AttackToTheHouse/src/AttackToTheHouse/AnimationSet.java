package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;
import java.util.ArrayList;
import javax.swing.ImageIcon;

class Animation
{
	String path;			//이미지 경로
	int widthAnimCount;		//나누고 싶은 가로 개수
	int heightAnimCount;	//나누고 싶은 세로 개수
	int startIndex;			//시작할려는 텍스쳐 인덱스
	int endIndex;			//끝 인덱스
	int animIndex;			//저장되는 애니매이션 인덱스
	int delay;				//애니매이션 딜레이
	boolean repeatFlag;		//반복 여부
	int playInterval;		//애니메이션 반복 재생 간격
	int playCnt;			//애니메이션 반복 재생 카운트용 멤버

	Animation(String path, int widthAnimCount, int heightAnimCount, int startIndex, int endIndex, int delay, boolean repeatFlag)
	{
		this.path = path;
		this.widthAnimCount = widthAnimCount;
		this.heightAnimCount = heightAnimCount;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.delay = delay;
		this.repeatFlag = repeatFlag;
		this.playInterval = 0;
		this.playCnt = 0;
		this.animIndex = startIndex;
	}

	Animation(String path, int widthAnimCount, int heightAnimCount, int startIndex, int endIndex, int delay, boolean repeatFlag, int playInterval)
	{
		this.path = path;
		this.widthAnimCount = widthAnimCount;
		this.heightAnimCount = heightAnimCount;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.delay = delay;
		this.repeatFlag = repeatFlag;
		this.playInterval = playInterval;
		this.playCnt = 0;
		this.animIndex = startIndex;
	}
};

public class AnimationSet
{
	private ArrayList<Animation> m_animationList;	//애니메이션들을 저장할 리스트
	private int m_currentAnimation;					//현재 애니메이션
	private int m_delayCount;						//설정할 딜레이까지 카운트하는 용도
	private boolean m_pauseFlag;					//일시중지

	public AnimationSet()
	{
		m_animationList = new ArrayList<Animation>();
		m_currentAnimation = -1;
		m_delayCount = 0;
		m_pauseFlag = false;
	}

	public void insertAnimation(Animation anim)
	{
		m_animationList.add(anim);
	}

	boolean setCurrentAnim(int index)
	{
		if (index < 0 || index > m_animationList.size() - 1)
			return false;

		if (index != m_currentAnimation)
		{
			if (m_currentAnimation != -1)
				m_animationList.get(m_currentAnimation).playCnt = 0;
			m_delayCount = 0;
			m_currentAnimation = index;
		}
		return true;
	}

	void setCurrentAnimIndex(int index)
	{
		m_animationList.get(m_currentAnimation).animIndex = index;
	}

	void setAnimPause()
	{
		m_pauseFlag = true;
	}

	void setAnimResume()
	{
		m_pauseFlag = false;
	}

	Animation getCurrentAnim()
	{
		return m_animationList.get(m_currentAnimation);
	}

	String getCurrentTexture()
	{
		return m_animationList.get(m_currentAnimation).path;
	}

	int getCurrentanimIndex()
	{
		return m_currentAnimation;
	}

	int getInsertAnimCount()
	{
		return m_animationList.size();
	}

	Rect getCurrentAnimRect()
	{
		Animation curAnim = m_animationList.get(m_currentAnimation);
		if (curAnim == null)
			return null;

		int XIndex;
		int YIndex;
		//curAnim.animIndex 가 0일 경우 % 연산에 문제가 생기기때문에 추가
		if (curAnim.animIndex == 0)
		{
			XIndex = 0;
			YIndex = 0;
		}
		else
		{
			XIndex = (curAnim.animIndex) % (curAnim.widthAnimCount);  //현재 애니 인덱스 % 가로 인덱스 갯수 = 현재 가로로 몇번째 인덱스인지
			YIndex = (curAnim.animIndex) / (curAnim.heightAnimCount); //현재 애니 인덱스 / 세로 인덱스 갯수 = 현재 세로로 몇번째 인덱스인지
		}
		ImageIcon image = new ImageIcon(curAnim.path);
		Pos textureSize = new Pos(image.getIconWidth(), image.getIconHeight());
		Pos scopeWH = new Pos(textureSize.X / curAnim.widthAnimCount, textureSize.Y / curAnim.heightAnimCount);
		if (curAnim.heightAnimCount == 1)
			return new Rect((int)(scopeWH.X * XIndex), 0, (int)(scopeWH.X * XIndex + scopeWH.X), (int)scopeWH.Y);
		return new Rect((int)(scopeWH.X * XIndex), (int)(scopeWH.Y * YIndex), (int)(scopeWH.X * XIndex + scopeWH.X), (int)(scopeWH.Y * YIndex + scopeWH.Y));
	}

	int getCurrentanimIndexCount() //현재 동작중인 애니메이션의 총 애니메이션 인덱스의 갯수
	{
		Animation curAnim = m_animationList.get(m_currentAnimation);
		if (curAnim == null)
			return -1;
		int temp = (curAnim.widthAnimCount) * (curAnim.heightAnimCount);
		return temp;
	}

	void run() //동작 함수
	{
		Animation curAnim = m_animationList.get(m_currentAnimation);

		//딜레이 카운트가 현재 애니메이션에 설정된 딜레이만큼 안지났다면
		if (m_delayCount < curAnim.delay)
			m_delayCount++;
		else //현재 애니메이션에 설정된 딜레이에 도달했으면
		{
			//딜레이 카운트 초기화
			m_delayCount = 0;

			//현재 동작중인 애니메이션의 인덱스가 마지막이라면
			if (curAnim.animIndex >= curAnim.endIndex)
			{
				//현재 실행중인 애니메이션에게 반복 플래그가 있다면
				if (curAnim.repeatFlag)
				{
					if (curAnim.playCnt++ >= curAnim.playInterval)
					{
						curAnim.playCnt = 0;
						curAnim.animIndex = curAnim.startIndex;
					}
				}
			}
			//마지막이 아니면 애니메이션 인덱스를 증가
			else
			{
				if (!m_pauseFlag)
					(curAnim.animIndex)++;
			}
		}
	}
}