package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;

public class BasicFunction
{
	static private int[] m_swapArr = new int[2];

	BasicFunction()
	{
	}

	static public void Swap()
	{
		int temp = m_swapArr[0];
		m_swapArr[0] = m_swapArr[1];
		m_swapArr[1] = temp;
	}
	static public boolean checkSwap()
	{
		if (m_swapArr[0] > m_swapArr[1])
		{
			int temp = m_swapArr[0];
			m_swapArr[0] = m_swapArr[1];
			m_swapArr[1] = temp;
			return true;
		}
		return false;
	}

	//충돌 처리 체크함수
	public static boolean checkCollision(Rect a, Rect b)
	{
		//a 의 x축
		m_swapArr[0] = a.topX;
		m_swapArr[1] = a.bottomX;
		checkSwap();
		a.topX = m_swapArr[0];
		a.bottomX = m_swapArr[1];

		//b 의 x축
		m_swapArr[0] = b.topX;
		m_swapArr[1] = b.bottomX;
		checkSwap();
		b.topX = m_swapArr[0];
		b.bottomX = m_swapArr[1];

		//a 의 y축
		m_swapArr[0] = a.topY;
		m_swapArr[1] = a.bottomY;
		checkSwap();
		a.topY = m_swapArr[0];
		a.bottomY = m_swapArr[1];

		//a 의 y축
		m_swapArr[0] = b.topY;
		m_swapArr[1] = b.bottomY;
		checkSwap();
		b.topY = m_swapArr[0];
		b.bottomY = m_swapArr[1];

		if (a.bottomY <= b.topY)
			return false; /* 사각형 A가 사각형 B의 上에 있는 경우 */
		if (a.topY > b.bottomY)
			return false; /* 사각형 A가 사각형 B의 下에 있는 경우 */
		if (a.bottomX <= b.topX)
			return false; /* 사각형 A가 사각형 B의 左에 있는 경우 */
		if (a.topX > b.bottomX)
			return false; /* 사각형 A가 사각형 B의 右에 있는 경우 */
		return true;
	}
}
