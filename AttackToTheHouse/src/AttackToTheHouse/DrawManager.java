package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;

class DrawInfo
{
	Image image;
	Pos drawPos;		//그릴 위치 (좌표계 좌측 상단이 0,0 입니다.)
	Rect drawRect;		//그릴 영역 (해당 이미지파일에서 어느영역을 그릴지? 입니다.)
	int	originWidth;	//원본 이미지 가로
	int	originHeight;	//원본 이미지 세로
	float scalingX;		//그릴 시 가로 확대 배율 입니다.
	float scalingY;		//그릴 시 세로 확대 배율 입니다.
	boolean reverseFlag;    //그릴 시 좌 우 그림 반전 여부 입니다.
	float alpha;		//투명도

	DrawInfo()
	{
		image = null;
		drawPos = null;
		drawRect = null;
		originWidth = 0;
		originHeight = 0;
		scalingX = 1.f;
		scalingY = 1.f;
		reverseFlag = false;
		alpha = 255.f;
	}
	DrawInfo(String path, Pos pos, Rect rect, float scalingX, float scalingY, boolean reverseFlag, float alpha)
	{
		ImageIcon tempIconImage = new ImageIcon(path);
		image = tempIconImage.getImage();
		drawPos = pos;
		drawRect = rect;
		originWidth = tempIconImage.getIconWidth();
		originHeight = tempIconImage.getIconHeight();
		this.scalingX = scalingX;
		this.scalingY = scalingY;
		this.reverseFlag = reverseFlag;
		this.alpha = alpha;
	}
	DrawInfo(String path, Pos pos, Rect rect, float scalingX, float scalingY, boolean reverseFlag)
	{
		ImageIcon tempIconImage = new ImageIcon(path);
		image = tempIconImage.getImage();
		drawPos = pos;
		drawRect = rect;
		originWidth = tempIconImage.getIconWidth();
		originHeight = tempIconImage.getIconHeight();
		this.scalingX = scalingX;
		this.scalingY = scalingY;
		this.reverseFlag = reverseFlag;
		alpha = 255.f;
	}
	DrawInfo(String path, Pos pos, Rect rect, float scalingX, float scalingY)
	{
		ImageIcon tempIconImage = new ImageIcon(path);
		image = tempIconImage.getImage();
		drawPos = pos;
		drawRect = rect;
		originWidth = tempIconImage.getIconWidth();
		originHeight = tempIconImage.getIconHeight();
		this.scalingX = scalingX;
		this.scalingY = scalingY;
		reverseFlag = false;
		alpha = 255.f;
	}
	DrawInfo(String path, Pos pos, Rect rect, boolean reverseFlag)
	{
		ImageIcon tempIconImage = new ImageIcon(path);
		image = tempIconImage.getImage();
		drawPos = pos;
		drawRect = rect;
		originWidth = tempIconImage.getIconWidth();
		originHeight = tempIconImage.getIconHeight();
		scalingX = 1.f;
		scalingY = 1.f;
		this.reverseFlag = reverseFlag;
		alpha = 255.f;
	}
	DrawInfo(String path, Pos pos, Rect rect)
	{
		ImageIcon tempIconImage = new ImageIcon(path);
		image = tempIconImage.getImage();
		drawPos = pos;
		drawRect = rect;
		originWidth = tempIconImage.getIconWidth();
		originHeight = tempIconImage.getIconHeight();
		scalingX = 1.f;
		scalingY = 1.f;
		this.reverseFlag = reverseFlag;
		alpha = 255.f;
	}
}

public class DrawManager
{
	public static DrawManager m_drawManager = null;
	private ArrayList<DrawInfo> m_drawList;

	public DrawManager()
	{
		m_drawList = new ArrayList<DrawInfo>();
	}

	//싱글톤 받기용
	static public DrawManager createIntance()
	{
		if (m_drawManager == null)
			m_drawManager = new DrawManager();
		return m_drawManager;
	}

	//드로우 리스트 정리
	public void clearView()
	{
		m_drawList.clear();
	}

	//드로우 인포저장
	public void draw(DrawInfo drawInfo)
	{
		m_drawList.add(drawInfo);
	}

	//드로우 리스트 가져옴
	public ArrayList<DrawInfo> getDrawList()
	{
		return m_drawList;
	}
}
