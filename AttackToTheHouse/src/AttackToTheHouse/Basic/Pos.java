package AttackToTheHouse.Basic;

public class Pos 
{
	public float X;
	public float Y;
	
	public Pos(float X, float Y)
	{
		this.X = X;
		this.Y = Y;
	}
	
	public Pos(int X, int Y)
	{
		this.X = (float)X;
		this.Y = (float)Y;
	}
}
