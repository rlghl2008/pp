package AttackToTheHouse.Basic;

public class Rect 
{
	public int topX;
	public int topY;
	public int bottomX;
	public int bottomY;
	
	public Rect(int topX, int topY, int bottomX, int bottomY)
	{
		this.topX = topX;
		this.topY = topY;
		this.bottomX = bottomX;
		this.bottomY = bottomY;
	}
}
