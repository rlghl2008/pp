package AttackToTheHouse;

public class EXTimer
{
	private long m_startTime;	//시작할 때의 시간
	private long m_curTime;		//현재 시간
	private long m_pauseTime;	//일시정지 한 순간의 시간
	private long m_stopingTime;	//일시정지 하고있는 동안의 시간
	private boolean m_runFlag;	//작동 플래그

	EXTimer()
	{
		m_startTime = 0;
		m_curTime = 0;
		m_pauseTime = 0;
		m_stopingTime = 0;
		m_runFlag = false;
	}

	public void start()
	{
		m_startTime = System.nanoTime();
		m_runFlag = true;
	}

	public void pause()
	{
		if (m_runFlag)
		{
			m_pauseTime = System.nanoTime();
			m_runFlag = false;
		}
	}

	public void resume()
	{
		if (!m_runFlag)
		{
			m_startTime += m_stopingTime;
			m_runFlag = true;
		}
	}

	public long getCurTime()
	{
		return m_curTime;
	}

	public void run()
	{
		if (m_runFlag)
			m_curTime = System.nanoTime() - m_startTime;
		else
			m_stopingTime = System.nanoTime() - m_pauseTime;
	}
}
