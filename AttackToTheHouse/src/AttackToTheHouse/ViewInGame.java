package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ViewInGame extends ViewBase
{
	private EXButton m_skillCandle;			//촛불 스킬 버튼
	private EXButton m_skillTime;			//시간 스킬 버튼
	private EXButton m_randomBox;			//랜덤 박스 버튼
	private EXButton m_back;				//메뉴가기

	private EXButton[] m_soldier;			//병사 버튼

	private ObjectManager m_objectMgr;		//오브젝트 매니저
	private int m_cameraX;					//카메라 위치 이동용 변수
	private final int m_mapWidth = 1600;	//맵의 가로 크기

	ObjectBase m_playerTower;				//플레이어의 타워
	ObjectBase m_enemyTower;
	private int m_fpsCount;					//fps를 이용한 시간 확인

	private final int m_candleCool = 10800; //촛불 스킬 쿨타임
	private int m_candleCoolCnt;			//촛불 스킬 쿨타임 카운트
	private boolean m_candleCoolFlag;		//촛불 스킬 쿨타임 플래그

	private final int m_timeCool = 7200;	//시간 스킬 쿨타임
	private int m_timeCoolCnt;				//시간 스킬 쿨타임 카운트
	private boolean m_timeCoolFlag;			//시간 스킬 쿨타임 플래그

	private int[] m_soldierCool;			//솔져 생성 쿨타임
	private int[] m_soidierCoolCnt;			//솔져 생성 쿨타임 카운트
	private boolean[] m_soldierCoolFlag;	//솔져 생성 쿨타임 플래그

	private EXButton m_speedChange;			//가속 하는 버튼
	private boolean m_speedChangeFlag;		//가속 버튼 플래그

	private void SaveRanking()
    {
		EXSound.allStop();
		EXSound.playSound("data/sound/ending/win.wav", false);
		
		while(true)
		{
			String name = JOptionPane.showInputDialog(null, "이름을 입력하세요.(영어 3자)");
			if(name == null)
			{
				JOptionPane.showMessageDialog(null, "등록 취소.", "등록 취소", JOptionPane.ERROR_MESSAGE);
				break;
			}
			if(name.length() != 3)
			{
				JOptionPane.showMessageDialog(null, "3글자를 입력해주세요.", "등록 오류", JOptionPane.ERROR_MESSAGE);
				continue;
			}

			name = name.toUpperCase();
			m_playerInfo.m_playerName = name;
			try 
			{
				String str = m_playerInfo.m_playerName + "\\" + m_playerInfo.m_playTime + "\\";
				FileWriter fw = new FileWriter(new File("data/ranking/Score.txt"), true);
				fw.write(str);
				fw.flush();
			} 
			catch (IOException ex) 
			{
			}
			break;
		}
    }
		
	
	public ViewInGame(JPanel panel)
	{
		super(panel);
        
        m_playerInfo.m_playTime = 0;
        
		EXSound.allStop();
		//오브젝트 매니저 싱글턴 인스턴스 받아옴
		m_objectMgr = ObjectManager.createIntance();

		//플레이어 타워 생성
		m_playerTower = new PlayerTower();
		m_objectMgr.addObject(m_playerTower);

		//적타워 생성
		m_enemyTower = new EnemyTower();
		m_objectMgr.addObject(m_enemyTower);

		//카메라 X초기화
		m_cameraX = 0;

		//마우스 리스너 추가
		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		//랜덤박스
		m_randomBox = new EXButton();
		m_randomBox.setTexture("data/ingame/random_box_3.1.png");
		m_randomBox.setTexture("data/ingame/random_box_3.2.png", ButtonState.PUSH);
		m_randomBox.setTexture("data/ingame/random_box_3.3.png", ButtonState.ACTIVE);
		m_randomBox.setPos(new Pos(167, 7));
		m_randomBox.setRect(new Rect(0, 0, 65, 65));
		m_randomBox.setTextureRect(new Rect(0, 0, 65, 65));

		//시간스킬
		m_skillCandle = new EXButton();
		m_skillCandle.setTexture("data/skill/candle_3.1.png");
		m_skillCandle.setTexture("data/skill/candle_3.2.png", ButtonState.PUSH);
		m_skillCandle.setTexture("data/skill/candle_3.3.png", ButtonState.ACTIVE);
		m_skillCandle.setPos(new Pos(7, 7));
		m_skillCandle.setRect(new Rect(0, 0, 65, 65));
		m_skillCandle.setTextureRect(new Rect(0, 0, 65, 65));

		//시간정지
		m_skillTime = new EXButton();
		m_skillTime.setTexture("data/skill/clock_1.1.png");
		m_skillTime.setTexture("data/skill/clock_1.2.png", ButtonState.PUSH);
		m_skillTime.setTexture("data/skill/clock_1.3.png", ButtonState.ACTIVE);
		m_skillTime.setPos(new Pos(87, 7));
		m_skillTime.setRect(new Rect(0, 0, 70, 77));
		m_skillTime.setTextureRect(new Rect(0, 0, 70, 77));

		//X박스
		m_back = new EXButton();
		m_back.setTexture("data/menu/x1.png");
		m_back.setTexture("data/menu/x2.png", ButtonState.ACTIVE);
		m_back.setTexture("data/menu/x3.png", ButtonState.PUSH);
		m_back.setPos(new Pos(755, 4));
		m_back.setRect(new Rect(0, 0, 40, 40));
		m_back.setTextureRect(new Rect(0, 0, 40, 40));

		//가속 버튼
		m_speedChange = new EXButton();
		m_speedChange.setTexture("data/ingame/배속1.png");
		m_speedChange.setTexture("data/ingame/배속1_2.png", ButtonState.ACTIVE);
		m_speedChange.setTexture("data/ingame/배속1_3.png", ButtonState.PUSH);
		m_speedChange.setPos(new Pos(720, 50));
		m_speedChange.setRect(new Rect(0, 0, 75, 75));
		m_speedChange.setTextureRect(new Rect(0, 0, 75, 75));

		//병사들
		m_soldier = new EXButton[6];
		for (int i = 0; i < m_soldier.length; ++i)
		{
			int num = i + 1;
			m_soldier[i] = new EXButton();
			m_soldier[i].setTexture("data/characterBtn/soldier" + num + ".1.png");
			m_soldier[i].setTexture("data/characterBtn/soldier" + num + ".2.png", ButtonState.PUSH);
			m_soldier[i].setTexture("data/characterBtn/soldier" + num + ".3.png", ButtonState.ACTIVE);
			m_soldier[i].setPos(new Pos(200 + (70 * i), 545));
			m_soldier[i].setRect(new Rect(0, 0, 50, 50));
			m_soldier[i].setTextureRect(new Rect(0, 0, 50, 50));
		}

		m_fpsCount = 0;

		m_candleCoolFlag = false;
		m_candleCoolCnt = 0;

		m_timeCoolFlag = false;
		m_timeCoolCnt = 0;

		m_soldierCool = new int[6];
		m_soidierCoolCnt = new int[6];
		m_soldierCoolFlag = new boolean[6];
		for (int i = 0; i < m_soldierCool.length; ++i)
		{
			m_soldierCool[i] = (2 + (2 * i)) * 60;
			m_soidierCoolCnt[i] = 0;
			m_soldierCoolFlag[i] = false;
		}
		m_speedChangeFlag = false;

		//인게임 사운드
		EXSound.playSound("data/sound/ingame/ingameBG.wav", true);
	}

	private void initData()
	{
		AttackToTheHouse.m_fps = 60;
		m_speedChangeFlag = false;
		m_playerInfo.m_gold = 99999;
		m_objectMgr.clearObject();
	}

	@Override
	public ViewState run()
	{
		MouseState mouseState = m_mouseEvent.getMouseState();	//마우스 상태
		Pos mousePos = m_mouseEvent.getMousePos();

		//Y축은 100~500사이 에서만 작동
		if (mousePos.Y >= 100 && mousePos.Y <= 500)
		{
			//화면 이동하는 부분,스크롤 속도 조정
			if (mousePos.X >= 800 - 100)
			{
				m_cameraX += 5;
				if (mousePos.X >= 800 - 50)
					m_cameraX += 10;
				if (m_cameraX + 800 > m_mapWidth)
					m_cameraX = m_mapWidth - 800;
			}
			else if (mousePos.X < 100)
			{
				m_cameraX -= 5;
				if (mousePos.X < 50)
					m_cameraX -= 10;
				if (m_cameraX < 0)
					m_cameraX = 0;
			}
		}

		//배경 드로우
		m_drawManager.draw(new DrawInfo("data/Background.png", new Pos(0 - m_cameraX, 0), new Rect(0, 0, 480, 222), (1600.f / 480.f), (600.f / 222.f), false));

		//골드
		m_drawManager.draw(new DrawInfo("data/ingame/coin.png", new Pos(613, 9), new Rect(0, 0, 50, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(725, 9), new Rect(((int)m_playerInfo.m_gold % 10) * 33, 0, 33 + ((int)m_playerInfo.m_gold % 10) * 33, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(708, 9), new Rect((((int)m_playerInfo.m_gold % 100) / 10) * 33, 0, 33 + (((int)m_playerInfo.m_gold % 100) / 10) * 33, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(691, 9), new Rect((((int)m_playerInfo.m_gold % 1000) / 100) * 33, 0, 33 + (((int)m_playerInfo.m_gold % 1000) / 100) * 33, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(674, 9), new Rect((((int)m_playerInfo.m_gold % 10000) / 1000) * 33, 0, 33 + (((int)m_playerInfo.m_gold % 10000) / 1000) * 33, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(657, 9), new Rect((((int)m_playerInfo.m_gold % 100000) / 10000) * 33, 0, 33 + (((int)m_playerInfo.m_gold % 100000) / 10000) * 33, 50), 0.5f, 0.5f));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(640, 9), new Rect(((int)m_playerInfo.m_gold / 100000) * 33, 0, 33 + ((int)m_playerInfo.m_gold / 100000) * 33, 50), 0.5f, 0.5f));

		//시간 배경
		m_drawManager.draw(new DrawInfo("data/time/timeBg.png", new Pos(250, 7), new Rect(0, 0, 564, 124), 0.6f, 0.6f));

		//시간
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(540, 18), new Rect((int)((3600 - m_playerInfo.m_playTime) % 10) * 33, 0, 33 + 33 * (int)((3600 - m_playerInfo.m_playTime) % 10), 45)));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(500, 18), new Rect((int)((3600 - m_playerInfo.m_playTime) / 10 % 6) * 33, 0, 33 + 33 * (int)((3600 - m_playerInfo.m_playTime) / 10 % 6), 45)));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(420, 18), new Rect((int)((3600 - m_playerInfo.m_playTime) / 60 % 10) * 33, 0, 33 + 33 * (int)((3600 - m_playerInfo.m_playTime) / 60 % 10), 45)));
		m_drawManager.draw(new DrawInfo("data/time/time.png", new Pos(380, 18), new Rect((int)((3600 - m_playerInfo.m_playTime) / 600) * 33, 0, 33 + 33 * (int)((3600 - m_playerInfo.m_playTime) / 600), 45)));
		m_drawManager.draw(new DrawInfo("data/time/s.png", new Pos(465, 18), new Rect(0, 0, 20, 45), 1, 1));

		//캐릭터 생성 버튼 배경
		m_drawManager.draw(new DrawInfo("data/characterBtn/characterBtnBG.png", new Pos(178, 535), new Rect(0, 0, 564, 124), 1.14f, 1.5f));

		//캐릭터 생성 버튼 들
		m_skillCandle.run(mousePos, mouseState);
		m_skillTime.run(mousePos, mouseState);
		m_randomBox.run(mousePos, mouseState);
		m_back.run(mousePos, mouseState);
		m_speedChange.run(mousePos, mouseState);

		//오브젝트 매니저 동작
		m_objectMgr.run(m_cameraX);

		//나가기 버튼
		if (m_back.getState() == ButtonState.POP) // 메뉴로나감
		{
			initData();
			return ViewState.MENU;
		}
		//가속 버튼 눌리면
		else if (m_speedChange.getState() == ButtonState.POP)
		{
			if (m_speedChangeFlag == false)
			{
				AttackToTheHouse.m_fps = 120;
				m_speedChange.setTexture("data/ingame/배속2.png");
				m_speedChange.setTexture("data/ingame/배속2_2.png", ButtonState.ACTIVE);
				m_speedChange.setTexture("data/ingame/배속2_3.png", ButtonState.PUSH);
				m_speedChangeFlag = true;
			}
			else
			{
				AttackToTheHouse.m_fps = 60;
				m_speedChange.setTexture("data/ingame/배속1.png");
				m_speedChange.setTexture("data/ingame/배속1_2.png", ButtonState.ACTIVE);
				m_speedChange.setTexture("data/ingame/배속1_3.png", ButtonState.PUSH);
				m_speedChangeFlag = false;
			}
		}

		//병사 버튼들 런
		for (int i = 0; i < m_soldier.length; ++i)
			m_soldier[i].run(mousePos, mouseState);

		/*병사들 버튼이 눌리면 생성되는 처리*/
		if (m_soldier[0].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[0] == false)
			{
				if (m_playerInfo.m_gold >= 300)
				{
					m_playerInfo.m_gold -= 300;
					m_objectMgr.addCharacter("T1");
					m_soldierCoolFlag[0] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		if (m_soldier[1].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[1] == false)
			{
				if (m_playerInfo.m_gold >= 500)
				{
					m_playerInfo.m_gold -= 500;
					m_objectMgr.addCharacter("T2");
					m_soldierCoolFlag[1] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		if (m_soldier[2].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[2] == false)
			{
				if (m_playerInfo.m_gold >= 1000)
				{
					m_playerInfo.m_gold -= 1000;
					m_objectMgr.addCharacter("T3");
					m_soldierCoolFlag[2] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		if (m_soldier[3].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[3] == false)
			{
				if (m_playerInfo.m_gold >= 2000)
				{
					m_playerInfo.m_gold -= 2000;
					m_objectMgr.addCharacter("T4");
					m_soldierCoolFlag[3] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		if (m_soldier[4].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[4] == false)
			{
				if (m_playerInfo.m_gold >= 3000)
				{
					m_playerInfo.m_gold -= 3000;
					m_objectMgr.addCharacter("T5");
					m_soldierCoolFlag[4] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}

		if (m_soldier[5].getState() == ButtonState.POP)
		{
			if (m_soldierCoolFlag[5] == false)
			{
				if (m_playerInfo.m_gold >= 5000)
				{
					m_playerInfo.m_gold -= 5000;
					m_objectMgr.addCharacter("T6");
					m_soldierCoolFlag[5] = true;
				}
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}

		/*스킬버튼들이 눌리면 동작 되는 처리*/
		//촛불 스킬
		if (m_skillCandle.getState() == ButtonState.POP)
		{
			if (m_candleCoolFlag == false)
			{
				//촛불 폭탄 생성
				m_objectMgr.addObject(new Candle());
				m_candleCoolFlag = true;
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		//타임 프리즈 스킬
		else if (m_skillTime.getState() == ButtonState.POP)
		{
			if (m_timeCoolFlag == false)
			{
				//타임 프리즈
				EXSound.playSound("data/sound/ingame/timeFreeze.wav", false);
				//시간 스킬 처리
				EnemyTower enemyTower = (EnemyTower)m_objectMgr.getObject(1);	//1번째 인덱스는 는 무조건 적타워임(인게임 생성자에서 2번째로 오브젝트매니저에 넣어주기 때문)
				enemyTower.pause();
				for (int i = 0; i < m_objectMgr.getObjectCount(); ++i)	//0~현재 있는 객체(오브젝트)
				{
					ObjectBase curObject = m_objectMgr.getObject(i);
					if (curObject.getObjectInfo().teamFlag == false)		//적군 일때만
					{
						curObject.getObjectInfo().activeFlag = false;
					}
				}
				m_timeCoolFlag = true;
			}
			else
				EXSound.playSound("data/sound/ingame/btn_cool.wav", false);
		}
		//랜덤 박스
		else if (m_randomBox.getState() == ButtonState.POP)
		{
			if (m_playerInfo.m_gold >= 500)
			{
				m_playerInfo.m_gold -= 500;
				int bounce = (int)(Math.random() * 1000);
				m_playerInfo.m_gold += bounce;
				//랜덤 박스
				EXSound.playSound("data/sound/ingame/randomBox.wav", false);
			}
		}

		//촛불 스킬 쿨타임
		if (m_candleCoolFlag)
		{
			m_drawManager.draw(new DrawInfo("data/skill/countdown_black.png", new Pos(7, 7), new Rect(0, 0, 65, 65 - (65 * m_candleCoolCnt / m_candleCool)), 1.f, 1.f, false, 100.f));

			if (++m_candleCoolCnt >= m_candleCool)
			{
				m_candleCoolFlag = false;
				m_candleCoolCnt = 0;
			}
		}
		//시간 스킬 쿨타임
		if (m_timeCoolFlag)
		{
			m_drawManager.draw(new DrawInfo("data/skill/countdown_black.png", new Pos(87, 7), new Rect(0, 0, 65, 65 - (65 * m_timeCoolCnt / m_timeCool)), 1.f, 1.f, false, 100.f));

			if (++m_timeCoolCnt >= m_timeCool)
			{
				m_timeCoolFlag = false;
				m_timeCoolCnt = 0;
			}
		}
		//캐릭터 생성 쿨타임
		for (int i = 0; i < 6; ++i)
		{
			if (m_soldierCoolFlag[i])
			{
				m_drawManager.draw(new DrawInfo("data/characterBtn/cooldown_bg.png", new Pos(200 + (70 * i), 545), new Rect(0, 0, 50, 50 - (50 * m_soidierCoolCnt[i] / m_soldierCool[i])), 1.f, 1.f, false, 100.f));

				if (++m_soidierCoolCnt[i] >= m_soldierCool[i])
				{
					m_soldierCoolFlag[i] = false;
					m_soidierCoolCnt[i] = 0;
				}
			}
		}

		//플레이 시간이 60분 이상이거나 플레이어 타워가 깨지면 패배
		if (m_playerInfo.m_playTime >= 3600 || m_playerTower != m_objectMgr.getObject(0))
		{
			initData();
			return ViewState.LOSE;
		}
		//적타워를 파괴하면 승리
		else if (m_enemyTower != m_objectMgr.getObject(1))
		{
			initData();
			SaveRanking();
			return ViewState.WIN;
		}

		//fps가 60될때마다 1초씩 올라감
		if (++m_fpsCount == 60)
		{
			m_fpsCount = 0;
			m_playerInfo.m_playTime++;
		}

		return ViewState.INGAME;
	}
}