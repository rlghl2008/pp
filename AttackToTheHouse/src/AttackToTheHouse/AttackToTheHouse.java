package AttackToTheHouse;

import AttackToTheHouse.Basic.Rect;
import AttackToTheHouse.Basic.Pos;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;

public class AttackToTheHouse extends JPanel implements Runnable
{
	private Thread m_thread;
	private Image m_dbImage;
	private Graphics m_dbg;
	private ViewManager m_viewManager;		//뷰 매니저
	private DrawManager m_drawManager;	//드로우 매니저
	public static int m_fps = 60;		//fps
	
	public AttackToTheHouse()
	{
		setPreferredSize(new Dimension(800, 600));
		m_viewManager = new ViewManager(this);
		m_viewManager.changeView(ViewState.LOGO);
		m_drawManager = DrawManager.createIntance();
	}

	//시작 함수(게임 스타트)
	public void start()
	{
		m_thread = new Thread(this);
		m_thread.start();
	}

	//게임이 실제로 실행되고있는 함수
	public void run()
	{
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		EXFps fps = new EXFps();
		EXTimer timer = new EXTimer();
		timer.start();
		while (true)
		{
			timer.run();
			if (fps.fpsLock(m_fps))
			{
				//드로우 리스트 초기화
				m_drawManager.clearView();
				
				//매니저 작동
				m_viewManager.run();
				
				//그리기 (repaint() 추상함수인 paintComponent() 호출)
				//DrawManager 에서 관리하는 리스트를 기준으로 출력
				repaint();
				
				Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
			}
		}
	}

	public void initImage(Graphics g)
	{
		if (m_dbImage == null)
		{
			m_dbImage = createImage(this.getSize().width, this.getSize().height);
			m_dbg = m_dbImage.getGraphics();
		}

		m_dbg.setColor(getBackground());
		m_dbg.fillRect(0, 0, this.getSize().width, this.getSize().height);
		m_dbg.setColor(getForeground());

		g.drawImage(m_dbImage, 0, 0, this);
	}

	//그리기
	@Override
	public void paintComponent(Graphics g)
	{
		initImage(g);
		
		/* 실제 드로우 처리 */
		ArrayList<DrawInfo> curDrawList = m_drawManager.getDrawList();
		for (int i = 0; i < curDrawList.size(); ++i)
		{
			//드로우 리스트내에 있는 데이터를 전부 겟
			DrawInfo curDrawInfo = curDrawList.get(i);
			
			Image image = curDrawInfo.image;
			Pos drawPos = curDrawInfo.drawPos;
			Rect drawRect = curDrawInfo.drawRect;
			int drawEndPosX = (int)((float)(curDrawInfo.originWidth / (float)(curDrawInfo.originWidth / (float)(drawRect.bottomX - drawRect.topX))) * curDrawInfo.scalingX);
			int drawEndPosY = (int)((float)(curDrawInfo.originHeight / (float)(curDrawInfo.originHeight / (float)(drawRect.bottomY - drawRect.topY))) * curDrawInfo.scalingY);
			
			//투명도 처리
			AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, curDrawInfo.alpha / 255); //alpha값
			Graphics2D g2 = (Graphics2D)g;
			g2.setComposite(alphaComposite);
			
			//좌우 반전 처리
			if (curDrawInfo.reverseFlag)
			{
				drawPos.X += drawEndPosX;
				drawEndPosX = -drawEndPosX;
			}
			
			//그리기
			g2.drawImage(image, (int)drawPos.X, (int)drawPos.Y, (int)drawPos.X + drawEndPosX, (int)drawPos.Y + drawEndPosY,
				drawRect.topX, drawRect.topY, drawRect.bottomX, drawRect.bottomY, this);
		}
	}

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Attack To The Blue House");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AttackToTheHouse war = new AttackToTheHouse();
		frame.setContentPane(war);
		frame.setResizable(false);
		frame.setLocation(550, 200);
		frame.pack();
		frame.setVisible(true);
		war.start();
	}
}