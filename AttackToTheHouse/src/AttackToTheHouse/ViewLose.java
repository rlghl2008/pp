package AttackToTheHouse;

import AttackToTheHouse.Basic.Pos;
import AttackToTheHouse.Basic.Rect;
import javax.swing.JPanel;

public class ViewLose extends ViewBase
{
	private float m_alpha;
	private boolean m_btnStatCheck1;
	private boolean m_btnStatCheck2;
	private boolean m_soundCheck;

	EXButton m_buttonMenu;
	EXButton m_buttonExit;


	public ViewLose(JPanel panel)
	{
		super(panel);
		panel.addMouseListener(m_mouseEvent);
		panel.addMouseMotionListener(m_mouseEvent);

		m_buttonMenu = new EXButton();
		m_buttonMenu.setTexture("Data/menu/replay_1.png");
		m_buttonMenu.setTexture("Data/menu/replay_2.png", ButtonState.PUSH);
		m_buttonMenu.setTexture("Data/menu/replay_3.png", ButtonState.ACTIVE);  //추후에 메뉴 버튼으로 교체
		m_buttonMenu.setPos(new Pos(255, 430));
		m_buttonMenu.setRect(new Rect(0, 0, 484, 55));
		m_buttonMenu.setTextureRect(new Rect(0, 0, 484, 55));

		m_buttonExit = new EXButton();
		m_buttonExit.setTexture("Data/menu/exit1.png");
		m_buttonExit.setTexture("Data/menu/exit2.png", ButtonState.PUSH);
		m_buttonExit.setTexture("Data/menu/exit3.png", ButtonState.ACTIVE);
		m_buttonExit.setPos(new Pos(300, 520));
		m_buttonExit.setRect(new Rect(0, 0, 300, 55));
		m_buttonExit.setTextureRect(new Rect(0, 0, 300, 55));

		m_alpha = 0;

		m_btnStatCheck1 = false;
		m_btnStatCheck2 = false;
		m_soundCheck = false;
		
		EXSound.playSound("data/sound/ending/lose.wav", false);
	}

	//사운드
	private void menuSound()
	{
		if (!m_soundCheck)
		{
			EXSound.playSound("data/sound/menu/menu.wav", false);
			m_soundCheck = true;
		}
	}

	private void fadeInOut()
	{
		m_alpha += 3.5f;
		if (m_alpha > 255)
			m_alpha = 255;
	}

	@Override
	public ViewState run()
	{
		MouseState mouseState = m_mouseEvent.getMouseState();
		Pos mousePos = m_mouseEvent.getMousePos();

										
		if (!m_btnStatCheck1 && !m_btnStatCheck2)
			m_soundCheck = false;

		/*각 버튼들 이벤트 처리*/
		if (m_buttonMenu.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck1 = true;
		}
		else if (m_buttonMenu.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck1 = false;
		}
		if (m_buttonExit.getState() == ButtonState.ACTIVE)
		{
			menuSound();
			m_btnStatCheck2 = true;
		}
		else if (m_buttonExit.getState() == ButtonState.NORMAL)
		{
			m_btnStatCheck2 = false;
		}

		if (m_buttonMenu.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			return ViewState.MENU;
		}
		else if (m_buttonExit.getState() == ButtonState.POP)
		{
			EXSound.playSound("data/sound/menu/menu_click.wav", false);
			System.exit(0);
		}

		if (m_alpha != 255)
			fadeInOut();

		m_drawManager.draw(new DrawInfo("data/ending/loseview.png", new Pos(0, 0), new Rect(0, 0, 800, 600), 1, 1.5f, false, m_alpha));

		m_buttonMenu.run(mousePos, mouseState);
		m_buttonExit.run(mousePos, mouseState);

		return ViewState.LOSE;
	}
}
