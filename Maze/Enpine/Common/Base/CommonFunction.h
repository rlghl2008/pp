#pragma once 

#include "CommonBase.h"
#include "CommonStructure.h"


namespace EPN
{
	namespace COMMON
	{
		
		enum class EPN_LOG_FLAG : BYTE
		{
			NON = 0,
			CONSOLE = 1, //콘솔창 출력
			DEBUG = 2,		//디버그뷰 출력
			FILE = 4,			//파일로 출력
			DETAIL = 8,		//로그 내용 디테일
			MSG = 16,		//메세지 박스로 출력
		};

		//아래 enum이 여기 있으면 안될것으로 예상함. 클라단(Generic) 에서만 쓰일것으로 보이는데
		//따로 빼야함. 임시로 여기둠

		namespace FUNCTION
		{
			//UTIL
			REAL32 EPN_GetPosAngle(EPN_Pos DestPos, EPN_Pos TargetPos);
			EPN_Pos EPN_GetAnglePos(REAL32 Angle);

			REAL32 EPN_GetDistance(EPN_Pos DestPos, EPN_Pos TargetPos);//두점끼리 길이 구하는 함수
			EPN_Pos EPN_GetMousePos();//폼크기 변경해도 정확한값 반환

			BOOL EPN_CheckCollision(EPN_Rect A, EPN_Rect B);
			BOOL EPN_CheckCollision(EPN_Circle A, EPN_Circle B);
			BOOL EPN_CheckCollision(EPN_Rect A, EPN_Circle B);
			BOOL EPN_CheckCollision(EPN_Circle A, EPN_Rect B);
			BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Polygon* B);
			BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Rect B);
			BOOL EPN_CheckCollision(EPN_Rect A, EPN_Polygon* B);
			BOOL EPN_CheckCollision(EPN_Polygon* A, EPN_Circle B);
			BOOL EPN_CheckCollision(EPN_Circle A, EPN_Polygon* B);
			//LOG SYSTEM
			BOOL SET_LOG_FLAG(BYTE LogFlag);
			BOOL PRINT_LOG(CHAR* pFmt, ...);
			BOOL PRINT_LOG(BYTE LogFlag, CHAR* pFmt, ...);
			BOOL RELEASE_LOG();
			
		}
	}
}