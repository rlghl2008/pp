#include "CommonStructure.h"
#include "CommonFunction.h"


#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::STRUCTURE;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion


_EPN_Polygon::_EPN_Polygon(INT32 VertexNum)
{
	m_PolygonPos = _EPN_Pos(0, 0);
	m_EditFlag = true;
	if (m_VertexNum < 0)
		m_VertexNum = 0;
	this->m_VertexNum = VertexNum;
	m_PolygonAngle = 0;
	m_PolygonScal = _EPN_Scaling(1.f, 1.f);
	m_WidthReversFlag = false;
	m_PolygonCenterPos = _EPN_Pos(0, 0);
	if (m_VertexNum > 0)
	{
		m_mpVertexPos.SetMemoryPoolSize(m_VertexNum * 2);
		for (INT32 i = 0; i < m_VertexNum * 2; ++i)
			m_mpVertexPos.AddData(_EPN_Pos(0, 0));
	}
}

_EPN_Polygon::_EPN_Polygon(CONST _EPN_Polygon &Polygon)
{
	m_PolygonPos = Polygon.GetPolygonPos();
	m_EditFlag = true;
	m_VertexNum = Polygon.GetVertexNum();
	m_PolygonAngle = 0;
	m_PolygonScal = _EPN_Scaling(1.f, 1.f);
	m_WidthReversFlag = Polygon.GetWidthReversFlag();
	m_PolygonCenterPos = _EPN_Pos(0, 0);

	if (this->m_VertexNum > 0)
	{
		m_mpVertexPos.SetMemoryPoolSize(m_VertexNum * 2);
		for (INT32 i = 0; i < this->m_VertexNum * 2; ++i)
		{
			if (i < this->m_VertexNum)
			{
				m_mpVertexPos.AddData(_EPN_Pos(Polygon.GetVertexPosPool(i, false)));
				m_PolygonCenterPos.X += (*m_mpVertexPos.GetDataPtr(i)).X;
				m_PolygonCenterPos.Y += (*m_mpVertexPos.GetDataPtr(i)).Y;
			}
			else
				m_mpVertexPos.AddData(_EPN_Pos(Polygon.GetVertexPosPool(m_VertexNum - i, false)));
		}
		m_PolygonCenterPos.X /= (REAL32)m_VertexNum;
		m_PolygonCenterPos.Y /= (REAL32)m_VertexNum;
	}



}

_EPN_Polygon::_EPN_Polygon(_EPN_Rect Rect)
{
	m_PolygonPos = _EPN_Pos(0, 0);
	m_EditFlag = true;
	this->m_VertexNum = 4;
	m_PolygonAngle = 0;
	m_PolygonScal = _EPN_Scaling(1.f, 1.f);
	m_WidthReversFlag = false;

	m_mpVertexPos.SetMemoryPoolSize(m_VertexNum * 2);

	m_mpVertexPos.AddData(_EPN_Pos(Rect.Top.X, Rect.Top.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Bottom.X, Rect.Top.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Bottom.X, Rect.Bottom.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Top.X, Rect.Bottom.Y));

	m_mpVertexPos.AddData(_EPN_Pos(Rect.Top.X, Rect.Top.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Bottom.X, Rect.Top.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Bottom.X, Rect.Bottom.Y));
	m_mpVertexPos.AddData(_EPN_Pos(Rect.Top.X, Rect.Bottom.Y));

	m_PolygonCenterPos = (Rect.Top + Rect.Bottom) / 2;
}

_EPN_Polygon :: ~_EPN_Polygon()
{
	Clear();
}
VOID _EPN_Polygon::Clear()
{
	m_PolygonPos = _EPN_Pos(0, 0);
	m_EditFlag = true;
	m_VertexNum = 0;
	m_PolygonAngle = 0;
	m_PolygonScal = _EPN_Scaling(1.f, 1.f);
	m_WidthReversFlag = false;
	for (INT32 i = 0; i < m_mpVertexPos.GetAddDataCount();)
	{
		if (m_mpVertexPos.GetDataPtr(i))
		{
			m_mpVertexPos.EraseData(i);
			++i;
		}
	}
}

VOID _EPN_Polygon::SetVertexNum(INT32 m_VertexNum)
{
	if (this->m_VertexNum != m_VertexNum)
	{
		Clear();
		this->m_VertexNum = m_VertexNum;

		if (m_VertexNum > 0)
		{
			m_mpVertexPos.SetMemoryPoolSize(m_VertexNum * 2);
			for (INT32 i = 0; i < m_VertexNum * 2; ++i)
				m_mpVertexPos.AddData(_EPN_Pos(0, 0));
		}
	}
}

INT32 _EPN_Polygon::GetVertexNum() CONST
{
	return m_VertexNum;
}

VOID _EPN_Polygon::SetPolygonPos(_EPN_Pos Pos)
{
	m_PolygonPos = Pos;
}
_EPN_Pos _EPN_Polygon::GetPolygonPos() CONST
{
	return m_PolygonPos;
}

VOID _EPN_Polygon::_ReversWidth()
{
	if (m_WidthReversFlag)
	{


		for (INT32 i = 0; i < m_VertexNum; ++i)
		{
			REAL32 VertexAngle = EPN_GetPosAngle(m_PolygonCenterPos, (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)));
			REAL64 Distance = EPN_GetDistance(m_PolygonCenterPos, (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)));

			if (VertexAngle == 180 || VertexAngle == 0)
				continue;
			else if (VertexAngle > 180)
			{
				VertexAngle = 180 - (VertexAngle - 180);
				(*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) = m_PolygonCenterPos + EPN_GetAnglePos(VertexAngle) * _EPN_Pos(Distance, Distance);
			}
			else if (VertexAngle < 180)
			{
				VertexAngle = 180 + (180 - VertexAngle);

				(*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) = m_PolygonCenterPos + EPN_GetAnglePos(VertexAngle) * _EPN_Pos(Distance, Distance);
			}
		}

	}
}
VOID _EPN_Polygon::SetPolygonCenterPos(_EPN_Pos Pos)
{
	if (m_PolygonCenterPos != Pos)
	{
		m_EditFlag = true;
		m_PolygonCenterPos = Pos;
	}
}
_EPN_Pos _EPN_Polygon::GetPolygonCenterPos() CONST
{
	return m_PolygonCenterPos;
}
VOID _EPN_Polygon::_SetPolygonConvert()
{
	if (m_VertexNum < 2)
		return;


	for (INT32 i = 0; i < m_VertexNum; ++i)
	{
		REAL32 VertexAngle = EPN_GetPosAngle(m_PolygonCenterPos, (*m_mpVertexPos.GetDataPtr(i)));
		double Distance = EPN_GetDistance(m_PolygonCenterPos, (*m_mpVertexPos.GetDataPtr(i)));

		(*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) = EPN_GetAnglePos(VertexAngle) * _EPN_Pos(Distance, Distance);
		(*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) = (*m_mpVertexPos.GetDataPtr(i)) + ((EPN_GetAnglePos(VertexAngle + m_PolygonAngle) * _EPN_Pos(Distance, Distance)) - (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)));
	}

	_EPN_Pos CenterPos = (*m_mpVertexPos.GetDataPtr(0));

	for (INT32 i = 1; i < m_VertexNum; ++i)
	{
		if (CenterPos.X >(*m_mpVertexPos.GetDataPtr(i)).X)
			CenterPos.X = (*m_mpVertexPos.GetDataPtr(i)).X;
		if (CenterPos.Y > (*m_mpVertexPos.GetDataPtr(i)).Y)
			CenterPos.Y = (*m_mpVertexPos.GetDataPtr(i)).Y;
	}

	for (INT32 i = 0; i < m_VertexNum; ++i)
	{
		REAL32 VertexAngle = EPN_GetPosAngle(CenterPos, (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)));
		double Distance = EPN_GetDistance(CenterPos, (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)));

		_EPN_Pos TempPos = EPN_GetAnglePos(VertexAngle) * _EPN_Pos(Distance, Distance);
		(*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) = (*m_mpVertexPos.GetDataPtr(m_VertexNum + i)) + (EPN_GetAnglePos(VertexAngle) * _EPN_Pos(Distance * m_PolygonScal.X, Distance * m_PolygonScal.Y) - TempPos);
	}

	_ReversWidth();
}

REAL32 _EPN_Polygon::GetPolygonAngle() CONST
{
	return m_PolygonAngle;
}


_EPN_Scaling _EPN_Polygon::GetPolygonScal() CONST
{
	return m_PolygonScal;
}
VOID _EPN_Polygon::SetWidthReversFlag(BOOL Flag)
{
	if (m_WidthReversFlag != Flag)
	{
		m_EditFlag = true;
		m_WidthReversFlag = Flag;
	}

}
BOOL _EPN_Polygon::GetWidthReversFlag() CONST
{
	return m_WidthReversFlag;
}
VOID _EPN_Polygon::SetPolygonAngle(REAL32 Angle)
{
	if (m_PolygonAngle != Angle)
	{
		m_EditFlag = true;
		m_PolygonAngle = Angle;
	}
}

VOID _EPN_Polygon::SetPolygonScal(_EPN_Scaling Scal)
{
	if (Scal.X < 0)
		Scal.X = 0;
	if (Scal.Y < 0)
		Scal.Y = 0;
	if (m_PolygonScal != Scal)
	{
		m_EditFlag = true;
		m_PolygonScal = Scal;
	}

}


BOOL _EPN_Polygon::SetVertexPosPool(INT32 Index, _EPN_Pos Pos)
{
	if (Index < 0 || Index > GetVertexNum() - 1)
		return false;

	if ((*m_mpVertexPos.GetDataPtr(Index)) == Pos)
		return true;
	(*m_mpVertexPos.GetDataPtr(Index)) = Pos;
	m_EditFlag = true;

	for (INT32 i = 0; i < this->m_VertexNum; ++i)
	{
		m_PolygonCenterPos.X += (*m_mpVertexPos.GetDataPtr(i)).X;
		m_PolygonCenterPos.Y += (*m_mpVertexPos.GetDataPtr(i)).Y;
	}
	m_PolygonCenterPos.X /= (REAL32)m_VertexNum;
	m_PolygonCenterPos.Y /= (REAL32)m_VertexNum;


	return true;
}
_EPN_Pos _EPN_Polygon::GetVertexPosPool(INT32 Index, BOOL m_PolygonPosFlag) CONST
{
	if (Index < 0 || Index > GetVertexNum() - 1)
		return _EPN_Pos();
	if (m_PolygonPosFlag)
		return m_PolygonPos + (m_mpVertexPos.GetData(Index));
	return (_EPN_Pos)(m_mpVertexPos.GetData(Index));
}
_EPN_Pos _EPN_Polygon::GetConvertVertexPos(INT32 Index, BOOL m_PolygonPosFlag)
{
	if (Index < 0 || Index > GetVertexNum() - 1)
		return _EPN_Pos();
	if (m_EditFlag)
	{
		_SetPolygonConvert();
		m_EditFlag = false;
	}
	if (m_PolygonPosFlag)
		return (*m_mpVertexPos.GetDataPtr(GetVertexNum() + Index)) + m_PolygonPos;
	return  (*m_mpVertexPos.GetDataPtr(GetVertexNum() + Index));
}