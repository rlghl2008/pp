#pragma once 

//데이터 타입은 되도록 여기있는거만 써야함
//Win API 에 필요한 데이터 타입은 제외

#include <iostream>
#include <windows.h>

#define PATH_MAX 512

#define FOPEN_OK 0
#define NULL 0
//#define NULL_PTR nullptr
#define NULL_PTR nullptr // VS2015

typedef unsigned long long UINT64;
typedef signed long long INT64;
typedef unsigned int UINT32;
typedef signed int INT32;
typedef unsigned short UINT16;
typedef signed short INT16;
typedef unsigned char UINT8;
typedef signed char INT8;

typedef float REAL32;
typedef double 	REAL64;

typedef unsigned char BYTE;
typedef int BOOL;
typedef char CHAR;
typedef wchar_t WCHAR;

typedef errno_t ERRNO_T;


