#pragma once 

#include "CommonBase.h"
#include "../../Common/Class/EPN_MemoryPool.hpp"

namespace EPN
{
	namespace COMMON
	{
		namespace STRUCTURE
		{
			struct _EPN_Pos
			{
				REAL32 X;
				REAL32 Y;
				_EPN_Pos(REAL32 nullval = 0) :X((REAL32)nullval), Y((REAL32)nullval) {}
				_EPN_Pos(REAL64 _X, REAL64 _Y) :X((REAL32)_X), Y((REAL32)_Y) {}
				_EPN_Pos(REAL32 _X, REAL32 _Y) :X(_X), Y(_Y) {}
				_EPN_Pos(REAL32 _X, INT32 _Y) :X(_X), Y((REAL32)_Y) {}
				_EPN_Pos(INT32 _X, REAL32 _Y) :X((REAL32)_X), Y(_Y) {}
				_EPN_Pos(UINT32 _X, UINT32 _Y) :X((REAL32)_X), Y((REAL32)_Y) {}
				_EPN_Pos(INT32 _X, INT32 _Y) :X((REAL32)_X), Y((REAL32)_Y) {}
				_EPN_Pos(CONST _EPN_Pos& Pos) :X(Pos.X), Y(Pos.Y) {}

				VOID CastValue_Int()
				{
					X = (REAL32)((INT32)X);
					Y = (REAL32)((INT32)Y);
				}

				_EPN_Pos operator =(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X = Pos.X, Y = Pos.Y);
				}
				_EPN_Pos operator +(CONST _EPN_Pos &Pos) CONST
				{
					return _EPN_Pos(X + Pos.X, Y + Pos.Y);
				}
				_EPN_Pos operator -(CONST _EPN_Pos &Pos) CONST
				{
					return _EPN_Pos(X - Pos.X, Y - Pos.Y);
				}
				_EPN_Pos operator /(CONST _EPN_Pos &Pos) CONST
				{
					return _EPN_Pos(X / Pos.X, Y / Pos.Y);
				}
				_EPN_Pos operator *(CONST _EPN_Pos &Pos) CONST
				{
					return _EPN_Pos(X * Pos.X, Y * Pos.Y);
				}
				_EPN_Pos operator %(CONST _EPN_Pos &Pos) CONST
				{
					return _EPN_Pos((REAL32)((INT32)X % (INT32)Pos.X), (REAL32)((INT32)Y % (INT32)Pos.Y));
				}

				_EPN_Pos operator +=(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X += Pos.X, Y += Pos.Y);
				}
				_EPN_Pos operator -=(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X -= Pos.X, Y -= Pos.Y);
				}
				_EPN_Pos operator /=(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X /= Pos.X, Y /= Pos.Y);
				}
				_EPN_Pos operator *=(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X *= Pos.X, Y *= Pos.Y);
				}
				_EPN_Pos operator %=(CONST _EPN_Pos &Pos)
				{
					return _EPN_Pos(X = (REAL32)((INT32)X % (INT32)Pos.X), Y = (REAL32)((INT32)Y % (INT32)Pos.Y));
				}
				BOOL operator ==(CONST _EPN_Pos &Pos) const
				{
					if (X == Pos.X && Y == Pos.Y)
						return true;
					return FALSE;
				}
				BOOL operator <=(CONST _EPN_Pos &Pos)
				{
					if (X <= Pos.X && Y <= Pos.Y)
						return true;
					return FALSE;
				}
				BOOL operator >=(CONST _EPN_Pos &Pos)
				{
					if (X >= Pos.X && Y >= Pos.Y)
						return true;
					return FALSE;
				}
				BOOL operator <(CONST _EPN_Pos &Pos) const
				{
					if (X < Pos.X && Y < Pos.Y)
						return true;
					return FALSE;
				}
				BOOL operator >(CONST _EPN_Pos &Pos)
				{
					if (X > Pos.X && Y > Pos.Y)
						return true;
					return FALSE;
				}
				BOOL operator !=(CONST _EPN_Pos &Pos)
				{
					if (X == Pos.X && Y == Pos.Y)
						return FALSE;
					return true;
				}

			};

			struct _EPN_Scaling
			{
				REAL32 X;
				REAL32 Y;
				_EPN_Scaling(INT32 nullval = 0) :X((REAL32)nullval), Y((REAL32)nullval) {}
				_EPN_Scaling(REAL32 _X, REAL32 _Y) :X(_X), Y(_Y) {}
				_EPN_Scaling(CONST _EPN_Scaling& Scal) :X(Scal.X), Y(Scal.Y) {}

				VOID CastValue_Int()
				{
					X = (REAL32)((INT32)X);
					Y = (REAL32)((INT32)Y);
				}

				_EPN_Scaling operator =(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X = Scal.X, Y = Scal.Y);
				}
				_EPN_Scaling operator +(CONST _EPN_Scaling &Scal) CONST
				{
					return _EPN_Scaling(X + Scal.X, Y + Scal.Y);
				}
				_EPN_Scaling operator -(CONST _EPN_Scaling &Scal) CONST
				{
					return _EPN_Scaling(X - Scal.X, Y - Scal.Y);
				}
				_EPN_Scaling operator /(CONST _EPN_Scaling &Scal) CONST
				{
					return _EPN_Scaling(X / Scal.X, Y / Scal.Y);
				}
				_EPN_Scaling operator *(CONST _EPN_Scaling &Scal) CONST
				{
					return _EPN_Scaling(X * Scal.X, Y * Scal.Y);
				}
				_EPN_Scaling operator %(CONST _EPN_Scaling &Scal) CONST
				{
					return _EPN_Scaling((REAL32)((INT32)X % (INT32)Scal.X), (REAL32)((INT32)Y % (INT32)Scal.Y));
				}

				_EPN_Scaling operator +=(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X += Scal.X, Y += Scal.Y);
				}
				_EPN_Scaling operator -=(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X -= Scal.X, Y -= Scal.Y);
				}
				_EPN_Scaling operator /=(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X /= Scal.X, Y /= Scal.Y);
				}
				_EPN_Scaling operator *=(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X *= Scal.X, Y *= Scal.Y);
				}
				_EPN_Scaling operator %=(CONST _EPN_Scaling &Scal)
				{
					return _EPN_Scaling(X = (REAL32)((INT32)X % (INT32)Scal.X), Y = (REAL32)((INT32)Y % (INT32)Scal.Y));
				}
				BOOL operator ==(CONST _EPN_Scaling &Scal)
				{
					if (X == Scal.X && Y == Scal.Y)
						return true;
					return FALSE;
				}
				BOOL operator !=(CONST _EPN_Scaling &Scal)
				{
					if (X == Scal.X && Y == Scal.Y)
						return FALSE;
					return true;
				}

			};

			struct _EPN_Rect
			{
				_EPN_Pos Top;
				_EPN_Pos Bottom;
				_EPN_Rect(INT32 nullval = 0) :Top(_EPN_Pos()), Bottom(_EPN_Pos())
				{
				}
				_EPN_Rect(REAL32 X, REAL32 Y, REAL32 W, REAL32 H)
				{
					Top.X = X;
					Top.Y = Y;
					Bottom.X = W;
					Bottom.Y = H;
				}

				_EPN_Rect(_EPN_Pos Top, _EPN_Pos Bottom)
				{
					this->Top = Top;
					this->Bottom = Bottom;
				}

				_EPN_Rect(_EPN_Pos Pos)
				{
					this->Top = Pos;
					this->Bottom = Pos;
				}
				_EPN_Rect(_EPN_Scaling Scal)
				{
					this->Top.X = Scal.X;
					this->Top.Y = Scal.Y;
					this->Bottom.X = Scal.X;
					this->Bottom.Y = Scal.Y;
				}

				VOID CastValue_Int()
				{
					Top.CastValue_Int();
					Bottom.CastValue_Int();
				}

				_EPN_Rect operator +(CONST _EPN_Rect &Rect) CONST
				{
					return _EPN_Rect(Top + Rect.Top, Bottom + Rect.Bottom);
				}
				_EPN_Rect operator -(CONST _EPN_Rect &Rect) CONST
				{
					return _EPN_Rect(Top - Rect.Top, Bottom - Rect.Bottom);
				}
				_EPN_Rect operator /(CONST _EPN_Rect &Rect) CONST
				{
					return _EPN_Rect(Top / Rect.Top, Bottom / Rect.Bottom);
				}
				_EPN_Rect operator *(CONST _EPN_Rect &Rect) CONST
				{
					return _EPN_Rect(Top * Rect.Top, Bottom * Rect.Bottom);
				}
				_EPN_Rect operator %(CONST _EPN_Rect &Rect) CONST
				{
					return _EPN_Rect(Top % Rect.Top, Bottom % Rect.Bottom);
				}

				_EPN_Rect operator +=(CONST _EPN_Rect &Rect)
				{
					return _EPN_Rect(Top += Rect.Top, Bottom += Rect.Bottom);
				}
				_EPN_Rect operator -=(CONST _EPN_Rect &Rect)
				{
					return _EPN_Rect(Top -= Rect.Top, Bottom -= Rect.Bottom);
				}
				_EPN_Rect operator /=(CONST _EPN_Rect &Rect)
				{
					return _EPN_Rect(Top /= Rect.Top, Bottom /= Rect.Bottom);
				}
				_EPN_Rect operator *=(CONST _EPN_Rect &Rect)
				{
					return _EPN_Rect(Top *= Rect.Top, Bottom *= Rect.Bottom);
				}
				_EPN_Rect operator %=(CONST _EPN_Rect &Rect)
				{
					return _EPN_Rect(Top %= Rect.Top, Bottom %= Rect.Bottom);
				}


				BOOL operator ==(CONST _EPN_Rect &Rect)
				{
					if (Top == Rect.Top && Bottom == Rect.Bottom)
						return true;
					return FALSE;
				}
				BOOL operator !=(CONST _EPN_Rect &Rect)
				{
					if (Top == Rect.Top && Bottom == Rect.Bottom)
						return FALSE;
					return true;
				}
			};

			struct _EPN_ARGB
			{
				REAL32 Alpha;
				REAL32 Red;
				REAL32 Green;
				REAL32 Blue;
				_EPN_ARGB(INT32 nullval = 0) :Alpha(255), Red(255), Green(255), Blue(255) {}
				_EPN_ARGB(REAL32 A, REAL32 R, REAL32 G, REAL32 B) :Alpha(A), Red(R), Green(G), Blue(B) {}
				_EPN_ARGB(REAL32 R, REAL32 G, REAL32 B) :Alpha(0), Red(R), Green(G), Blue(B) {}

				VOID Repair()
				{
					if (Alpha < 0)
						Alpha = 0;
					else if (Alpha > 255)
						Alpha = 255;
					if (Red < 0)
						Red = 0;
					else if (Red > 255)
						Red = 255;
					if (Green < 0)
						Green = 0;
					else if (Green > 255)
						Green = 255;
					if (Blue < 0)
						Blue = 0;
					else if (Blue > 255)
						Blue = 255;
				}
				_EPN_ARGB operator +(CONST _EPN_ARGB &EPN_ARGB) CONST
				{
					return _EPN_ARGB(Alpha + EPN_ARGB.Alpha, Red + EPN_ARGB.Red, Green + EPN_ARGB.Green, Blue + EPN_ARGB.Blue);
				}
				_EPN_ARGB operator -(CONST _EPN_ARGB &EPN_ARGB) CONST
				{
					return _EPN_ARGB(Alpha - EPN_ARGB.Alpha, Red - EPN_ARGB.Red, Green - EPN_ARGB.Green, Blue - EPN_ARGB.Blue);
				}
				_EPN_ARGB operator /(CONST _EPN_ARGB &EPN_ARGB) CONST
				{
					return _EPN_ARGB(Alpha / EPN_ARGB.Alpha, Red / EPN_ARGB.Red, Green / EPN_ARGB.Green, Blue / EPN_ARGB.Blue);
				}
				_EPN_ARGB operator *(CONST _EPN_ARGB &EPN_ARGB) CONST
				{
					return _EPN_ARGB(Alpha * EPN_ARGB.Alpha, Red * EPN_ARGB.Red, Green * EPN_ARGB.Green, Blue* EPN_ARGB.Blue);
				}

				_EPN_ARGB operator +=(CONST _EPN_ARGB &EPN_ARGB)
				{
					return _EPN_ARGB(Alpha += EPN_ARGB.Alpha, Red += EPN_ARGB.Red, Green += EPN_ARGB.Green, Blue += EPN_ARGB.Blue);
				}
				_EPN_ARGB operator -=(CONST _EPN_ARGB &EPN_ARGB)
				{
					return _EPN_ARGB(Alpha -= EPN_ARGB.Alpha, Red -= EPN_ARGB.Red, Green -= EPN_ARGB.Green, Blue -= EPN_ARGB.Blue);
				}
				_EPN_ARGB operator /=(CONST _EPN_ARGB &EPN_ARGB)
				{
					return _EPN_ARGB(Alpha /= EPN_ARGB.Alpha, Red /= EPN_ARGB.Red, Green /= EPN_ARGB.Green, Blue /= EPN_ARGB.Blue);
				}
				_EPN_ARGB operator *=(CONST _EPN_ARGB &EPN_ARGB)
				{
					return _EPN_ARGB(Alpha *= EPN_ARGB.Alpha, Red *= EPN_ARGB.Red, Green *= EPN_ARGB.Green, Blue *= EPN_ARGB.Blue);
				}

				BOOL operator ==(CONST _EPN_ARGB &EPN_ARGB)
				{
					if (Alpha == EPN_ARGB.Alpha && Red == EPN_ARGB.Red && Blue == EPN_ARGB.Blue && Green == EPN_ARGB.Green)
						return true;
					return FALSE;
				}
				BOOL operator !=(CONST _EPN_ARGB &EPN_ARGB)
				{
					if (Alpha == EPN_ARGB.Alpha && Red == EPN_ARGB.Red && Blue == EPN_ARGB.Blue && Green == EPN_ARGB.Green)
						return FALSE;
					return true;
				}
			};


			struct _EPN_Circle
			{
				_EPN_Pos CirclePos;
				REAL32 CircleRadius;

				_EPN_Circle(INT32 nullval = 0) :CirclePos(0), CircleRadius(0) {}
				_EPN_Circle(_EPN_Pos Pos, REAL32 Radius) :CirclePos(Pos), CircleRadius(Radius) {}


			};

			class _EPN_Polygon
			{
			private:
				EPN_MemoryPool<_EPN_Pos> m_mpVertexPos;
			private:
				INT32 m_VertexNum;
				_EPN_Pos m_PolygonPos;
				REAL32 m_PolygonAngle;
				_EPN_Scaling m_PolygonScal;
				BOOL m_WidthReversFlag;
				BOOL m_EditFlag;
				_EPN_Pos m_PolygonCenterPos;
			public:
				_EPN_Polygon(INT32 VertexNum = 0);
				_EPN_Polygon(CONST _EPN_Polygon &Polygon);
				_EPN_Polygon(_EPN_Rect Rect);
				~_EPN_Polygon();
			private:
				VOID _SetPolygonConvert();
				VOID _ReversWidth();
			public:
				VOID Clear();

				VOID SetVertexNum(INT32 VertexNum);
				INT32 GetVertexNum() CONST;

				VOID SetPolygonPos(_EPN_Pos Pos);
				_EPN_Pos GetPolygonPos() CONST;

				VOID SetWidthReversFlag(BOOL Flag);
				BOOL GetWidthReversFlag() CONST;

				VOID SetPolygonAngle(REAL32 Angle);
				REAL32 GetPolygonAngle() CONST;

				VOID SetPolygonCenterPos(_EPN_Pos Pos);
				_EPN_Pos GetPolygonCenterPos() CONST;

				VOID SetPolygonScal(_EPN_Scaling Scal);
				_EPN_Scaling GetPolygonScal() CONST;

				BOOL SetVertexPosPool(INT32 Index, _EPN_Pos Pos);
				_EPN_Pos GetVertexPosPool(INT32 Index, BOOL m_PolygonPosFlag = true) CONST;
				_EPN_Pos GetConvertVertexPos(INT32 Index, BOOL m_PolygonPosFlag = true);

			public:
				_EPN_Polygon operator =(CONST _EPN_Polygon &Polygon)
				{
					Clear();
					m_EditFlag = true;
					m_PolygonPos = Polygon.GetPolygonPos();
					m_VertexNum = Polygon.GetVertexNum();
					m_PolygonAngle = 0;
					m_PolygonScal = _EPN_Scaling(1.f, 1.f);
					m_WidthReversFlag = Polygon.GetWidthReversFlag();

					if (this->m_VertexNum > 0)
					{
						m_mpVertexPos.SetMemoryPoolSize(m_VertexNum * 2);
						for (INT32 i = 0; i < this->m_VertexNum * 2; ++i)
						{
							if (i < this->m_VertexNum)
							{
								m_mpVertexPos.AddData(_EPN_Pos(Polygon.GetVertexPosPool(i, false)));
								m_PolygonCenterPos.X += m_mpVertexPos[i]->X;
								m_PolygonCenterPos.Y += m_mpVertexPos[i]->Y;
							}
							else
								m_mpVertexPos.AddData(_EPN_Pos(Polygon.GetVertexPosPool(m_VertexNum - i, false)));
						}
						m_PolygonCenterPos.X /= (REAL32)m_VertexNum;
						m_PolygonCenterPos.Y /= (REAL32)m_VertexNum;
					}

					return (*this);
				}
				_EPN_Polygon operator +(CONST _EPN_Pos &Pos) CONST
				{
					_EPN_Polygon ResultPolygon((*this));
					ResultPolygon.SetPolygonPos(GetPolygonPos() + Pos);
					ResultPolygon.SetPolygonCenterPos(GetPolygonCenterPos());
					return ResultPolygon;
				}
				_EPN_Polygon operator -(CONST _EPN_Pos &Pos) CONST
				{
					_EPN_Polygon ResultPolygon((*this));
					ResultPolygon.SetPolygonPos(GetPolygonPos() - Pos);
					ResultPolygon.SetPolygonCenterPos(GetPolygonCenterPos());
					return ResultPolygon;
				}
				_EPN_Polygon operator /(CONST _EPN_Pos &Pos) CONST
				{
					_EPN_Polygon ResultPolygon((*this));
					ResultPolygon.SetPolygonPos(GetPolygonPos() / Pos);
					ResultPolygon.SetPolygonCenterPos(GetPolygonCenterPos());
					return ResultPolygon;
				}
				_EPN_Polygon operator *(CONST _EPN_Pos &Pos) CONST
				{
					_EPN_Polygon ResultPolygon((*this));
					ResultPolygon.SetPolygonPos(GetPolygonPos()*Pos);
					ResultPolygon.SetPolygonCenterPos(GetPolygonCenterPos());
					return ResultPolygon;
				}


				_EPN_Polygon operator +=(CONST _EPN_Pos &Pos)
				{
					SetPolygonPos(GetPolygonPos() + Pos);
					return (*this);
				}
				_EPN_Polygon operator -=(CONST _EPN_Pos &Pos)
				{
					SetPolygonPos(GetPolygonPos() - Pos);
					return (*this);
				}
				_EPN_Polygon operator /=(CONST _EPN_Pos &Pos)
				{
					SetPolygonPos(GetPolygonPos() / Pos);
					return (*this);
				}
				_EPN_Polygon operator *=(CONST _EPN_Pos &Pos)
				{
					SetPolygonPos(GetPolygonPos()*Pos);
					return (*this);
				}


			};


		}
	}
}

typedef EPN::COMMON::STRUCTURE::_EPN_Pos EPN_Pos;
typedef EPN::COMMON::STRUCTURE::_EPN_Rect EPN_Rect;
typedef EPN::COMMON::STRUCTURE::_EPN_Scaling EPN_Scaling;
typedef EPN::COMMON::STRUCTURE::_EPN_ARGB EPN_ARGB;
typedef EPN::COMMON::STRUCTURE::_EPN_Circle EPN_Circle;
typedef EPN::COMMON::STRUCTURE::_EPN_Polygon EPN_Polygon;








