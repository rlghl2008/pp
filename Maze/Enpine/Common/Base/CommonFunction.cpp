#include "CommonFunction.h"

#include "../Class/EPN_LogManager.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;

#pragma endregion


#define SAME_SIGNS( a, b ) \
 (((long) ((unsigned long) a ^ (unsigned long) b)) >= 0 )


BOOL LineColCheck(long x1, long y1, long x2, long y2, long x3, long y3, long x4, long y4, long* x, long *y)
{

	long Ax, Bx, Cx, Ay, By, Cy, d, e, f;
	INT16 x1lo, x1hi, y1lo, y1hi;

	Ax = x2 - x1;
	Bx = x3 - x4;

	if (Ax<0)
	{
		x1lo = (INT16)x2; x1hi = (INT16)x1;
	}
	else {
		x1hi = (INT16)x2; x1lo = (INT16)x1;
	}
	if (Bx>0)
	{
		if (x1hi < (INT16)x4 || (INT16)x3 < x1lo) return false;
	}
	else {
		if (x1hi < (INT16)x3 || (INT16)x4 < x1lo) return false;
	}

	Ay = y2 - y1;
	By = y3 - y4;

	if (Ay<0) { /* Y bound box test*/
		y1lo = (INT16)y2; y1hi = (INT16)y1;
	}
	else {
		y1hi = (INT16)y2; y1lo = (INT16)y1;
	}
	if (By>0) {
		if (y1hi < (INT16)y4 || (INT16)y3 < y1lo) return false;
	}
	else {
		if (y1hi < (INT16)y3 || (INT16)y4 < y1lo) return false;
	}


	Cx = x1 - x3;
	Cy = y1 - y3;
	d = By*Cx - Bx*Cy;
	f = Ay*Bx - Ax*By;
	if (f>0)
	{
		if (d<0 || d>f) return false;
	}
	else {
		if (d>0 || d<f) return false;
	}

	e = Ax*Cy - Ay*Cx;
	if (f > 0)
	{
		if (e<0 || e>f)
			return 0;
	}
	else
	{
		if (e>0 || e<f) return false;
	}


	//if(f==0)
	//    return 2;

	//num = d * Ax;
	//offset = SAME_SIGNS(num,f) ? f/2 : -f/2;
	//*x = x1 + (num+offset) / f;

	//num = d*Ay;
	//offset = SAME_SIGNS(num,f) ? f/2 : -f/2;
	//*y = y1 + (num+offset) / f;

	return true;
}


REAL32 EPN::COMMON::FUNCTION::EPN_GetPosAngle(EPN_Pos DestPos, EPN_Pos TarGetPos)
{
	if (DestPos.X == TarGetPos.X && DestPos.Y == TarGetPos.Y)
		return 0;
	REAL64 C = 0, A = 0;
	REAL32 Angle = 0;

	if (DestPos.X > TarGetPos.X)
	{
		A = DestPos.Y - TarGetPos.Y;
		C = sqrt((TarGetPos.X - DestPos.X)*(TarGetPos.X - DestPos.X) + (TarGetPos.Y - DestPos.Y)*(TarGetPos.Y - DestPos.Y));
		C = -C;
		Angle = (REAL32)acos(A / C);
		Angle *= (REAL32)(180.0 / 3.141592653589793238462643383279);
		Angle += 180;
	}
	else
	{
		A = DestPos.Y - TarGetPos.Y;
		C = sqrt((TarGetPos.X - DestPos.X)*(TarGetPos.X - DestPos.X) + (TarGetPos.Y - DestPos.Y)*(TarGetPos.Y - DestPos.Y));
		Angle = (REAL32)acos(A / C);
		Angle *= (REAL32)(180.0 / 3.141592653589793238462643383279);
	}


	return Angle;
}
EPN_Pos EPN::COMMON::FUNCTION::EPN_GetAnglePos(REAL32 Angle)
{
	if (Angle < 0)
		return EPN_Pos(0);
	EPN_Pos Temp(0, 0);

	for (;;)
	{
		if (Angle >= 360)
		{
			Angle -= 360;
		}
		else
			break;
	}
	Temp.X = (REAL32)sin((Angle)*3.141592653589793238462643383279 / 180);
	Temp.Y = (REAL32)cos((Angle - 180)*3.141592653589793238462643383279 / 180);

	return Temp;
}

REAL32 EPN::COMMON::FUNCTION::EPN_GetDistance(EPN_Pos DestPos, EPN_Pos TarGetPos)
{
	return (REAL32)sqrt(pow((REAL64)(TarGetPos.X - DestPos.X), 2) + pow((REAL64)(TarGetPos.Y - DestPos.Y), 2));
}

BOOL EPN_TB_CheckSwap(REAL32 *A, REAL32 *B)
{
	if (*A > *B)
	{
		REAL32 Swap = *A;
		*A = *B;
		*B = Swap;
		return TRUE;
	}
	return FALSE;
}


BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Rect B)
{
	EPN_TB_CheckSwap(&A.Top.X, &A.Bottom.X);
	EPN_TB_CheckSwap(&B.Top.X, &B.Bottom.X);
	EPN_TB_CheckSwap(&A.Top.Y, &A.Bottom.Y);
	EPN_TB_CheckSwap(&B.Top.Y, &B.Bottom.Y);
	if (A.Bottom.Y < B.Top.Y) return FALSE; /* 사각형 A가 사각형 B의 上에 있는 경우 */
	if (A.Top.Y > B.Bottom.Y) return FALSE; /* 사각형 A가 사각형 B의 下에 있는 경우 */
	if (A.Bottom.X < B.Top.X) return FALSE; /* 사각형 A가 사각형 B의 左에 있는 경우 */
	if (A.Top.X > B.Bottom.X) return FALSE; /* 사각형 A가 사각형 B의 右에 있는 경우 */
	return TRUE;
}

BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Circle B)
{
	if (EPN_GetDistance(A.CirclePos, B.CirclePos) < (A.CircleRadius + B.CircleRadius))
	{
		return TRUE;
	}

	//If not 
	return FALSE;
}


EPN_Pos EPN::COMMON::FUNCTION::EPN_GetMousePos()
{
	POINT pos;
	pos.x = 0;
	pos.y = 0;
	GetCursorPos(&pos);
	return EPN_Pos(pos.x, pos.y);
}


//BOOL check_collision( Circle &A, std::vector &B ) 
BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Rect B)
{
	if (((A.CirclePos.Y + A.CircleRadius <= B.Top.Y) ||
		(A.CirclePos.Y - A.CircleRadius >= B.Top.Y + (B.Bottom.Y - B.Top.Y)) ||
		(A.CirclePos.X <= B.Top.X) ||
		(A.CirclePos.X >= B.Top.X + (B.Bottom.X - B.Top.X))) == false)
	{
		return TRUE;
	}

	if (((A.CirclePos.Y <= B.Top.Y) ||
		(A.CirclePos.Y - A.CircleRadius >= B.Top.Y + (B.Bottom.Y - B.Top.Y)) ||
		(A.CirclePos.X + A.CircleRadius <= B.Top.X) ||
		(A.CirclePos.X - A.CircleRadius >= B.Top.X + (B.Bottom.X - B.Top.X))) == false)
	{
		return TRUE;
	}
	if ((EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X, B.Top.Y)) < A.CircleRadius) || (EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X + B.Bottom.X - B.Top.X, B.Top.Y)) < A.CircleRadius) ||
		(EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X, B.Top.Y + B.Bottom.Y - B.Top.Y)) < A.CircleRadius) || (EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X + B.Bottom.X - B.Top.X, B.Top.Y + B.Bottom.Y - B.Top.Y)) < A.CircleRadius))
	{
		return TRUE;
	}


	return FALSE;
}

BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Circle B)
{
	if (((B.CirclePos.Y + B.CircleRadius <= A.Top.Y) ||
		(B.CirclePos.Y - B.CircleRadius >= A.Top.Y + A.Bottom.Y - A.Top.Y) ||
		(B.CirclePos.X <= A.Top.X) ||
		(B.CirclePos.X >= A.Top.X + A.Bottom.X - A.Top.X)) == false)
	{
		return TRUE;
	}

	if (((B.CirclePos.Y <= A.Top.Y) ||
		(B.CirclePos.Y - B.CircleRadius >= A.Top.Y + A.Bottom.Y - A.Top.Y) ||
		(B.CirclePos.X + B.CircleRadius <= A.Top.X) ||
		(B.CirclePos.X - B.CircleRadius >= A.Top.X + A.Bottom.X - A.Top.X)) == false)
	{
		return TRUE;
	}
	if ((EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X, A.Top.Y)) < B.CircleRadius) || (EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X + A.Bottom.X - A.Top.X, A.Top.Y)) < B.CircleRadius) ||
		(EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X, A.Top.Y + A.Bottom.Y - A.Top.Y)) < B.CircleRadius) || (EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X + A.Bottom.X - A.Top.X, A.Top.Y + A.Bottom.Y - A.Top.Y)) < B.CircleRadius))
	{
		return TRUE;
	}


	return FALSE;
}
BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Rect B)
{
	return EPN::COMMON::FUNCTION::EPN_CheckCollision(A, &EPN_Polygon(B));
}
BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Polygon* B)
{
	return EPN::COMMON::FUNCTION::EPN_CheckCollision(&EPN_Polygon(A), B);
}
BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Polygon* B)
{
	if (A == NULL || B == NULL || A->GetVertexNum() <= 0 || B->GetVertexNum() <= 0)
		return false;


	EPN_Rect BoxA = EPN_Rect(A->GetConvertVertexPos(0).X, A->GetConvertVertexPos(0).Y, A->GetConvertVertexPos(0).X, A->GetConvertVertexPos(0).Y);
	EPN_Rect BoxB = EPN_Rect(B->GetConvertVertexPos(0).X, B->GetConvertVertexPos(0).Y, B->GetConvertVertexPos(0).X, B->GetConvertVertexPos(0).Y);
	REAL32 LineDistanceA = 0;
	REAL32 LineDistanceB = 0;
	for (INT32 i = 0; i < A->GetVertexNum(); ++i)
	{
		if (BoxA.Top.X > A->GetConvertVertexPos(i).X)
		{
			BoxA.Top.X = A->GetConvertVertexPos(i).X;
		}
		if (BoxA.Top.Y > A->GetConvertVertexPos(i).Y)
		{
			BoxA.Top.Y = A->GetConvertVertexPos(i).Y;
		}
		if (BoxA.Bottom.X < A->GetConvertVertexPos(i).X)
		{
			BoxA.Bottom.X = A->GetConvertVertexPos(i).X;
		}
		if (BoxA.Bottom.Y < A->GetConvertVertexPos(i).Y)
		{
			BoxA.Bottom.Y = A->GetConvertVertexPos(i).Y;
		}
		if (i == A->GetVertexNum() - 1)
		{
			REAL32 Distance = EPN_GetDistance(A->GetConvertVertexPos(i), A->GetConvertVertexPos(0));
			if (LineDistanceA < Distance)
				LineDistanceA = Distance;
		}
		else
		{
			REAL32 Distance = EPN_GetDistance(A->GetConvertVertexPos(i), A->GetConvertVertexPos(i + 1));
			if (LineDistanceA < Distance)
				LineDistanceA = Distance;
		}
	}
	for (INT32 i = 0; i < B->GetVertexNum(); ++i)
	{
		if (BoxB.Top.X > B->GetConvertVertexPos(i).X)
		{
			BoxB.Top.X = B->GetConvertVertexPos(i).X;
		}
		if (BoxB.Top.Y > B->GetConvertVertexPos(i).Y)
		{
			BoxB.Top.Y = B->GetConvertVertexPos(i).Y;
		}
		if (BoxB.Bottom.X < B->GetConvertVertexPos(i).X)
		{
			BoxB.Bottom.X = B->GetConvertVertexPos(i).X;
		}
		if (BoxB.Bottom.Y < B->GetConvertVertexPos(i).Y)
		{
			BoxB.Bottom.Y = B->GetConvertVertexPos(i).Y;
		}
		if (i == B->GetVertexNum() - 1)
		{
			REAL32 Distance = EPN_GetDistance(B->GetConvertVertexPos(i), B->GetConvertVertexPos(0));
			if (LineDistanceB < Distance)
				LineDistanceB = Distance;
		}
		else
		{
			REAL32 Distance = EPN_GetDistance(B->GetConvertVertexPos(i), B->GetConvertVertexPos(i + 1));
			if (LineDistanceB < Distance)
				LineDistanceB = Distance;
		}
	}
	if (EPN_CheckCollision(BoxA, BoxB))
	{
		long X = 0, Y = 0;

		for (INT32 i = 0; i < A->GetVertexNum(); ++i)
		{
			EPN_Pos CheckPos = A->GetConvertVertexPos(i);
			REAL32 Angle = REAL32(rand() % 360);
			REAL32 Add = 0.5f;
			if (B->GetVertexNum() > 2)
			{
				for (;;)
				{
					BOOL CheckFlag = false;
					for (INT32 j = 0; j < B->GetVertexNum(); ++j)
					{
						if (EPN_GetPosAngle(CheckPos, B->GetConvertVertexPos(j)) == Angle)
						{
							CheckFlag = true;
							break;
						}
					}
					if (!CheckFlag)
					{
						EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * ((LineDistanceB + EPN_GetDistance(CheckPos, (BoxB.Top + BoxB.Bottom) / EPN_Pos(2.f,2.f))) *  2.f);
						INT32 ColCount = 0;
						for (INT32 j = 0; j < B->GetVertexNum(); ++j)
						{
							EPN_Pos B_Line[2];
							if (j == B->GetVertexNum() - 1)
							{
								B_Line[0] = B->GetConvertVertexPos(j);
								B_Line[1] = B->GetConvertVertexPos(0);
							}
							else
							{
								B_Line[0] = B->GetConvertVertexPos(j);
								B_Line[1] = B->GetConvertVertexPos(j + 1);
							}
							if (LineColCheck((long)CheckPos.X, (long)CheckPos.Y, (long)CheckPos2.X, (long)CheckPos2.Y, (long)B_Line[0].X, (long)B_Line[0].Y, (long)B_Line[1].X, (long)B_Line[1].Y, &X, &Y))
							{
								ColCount++;
							}
						}
						if (ColCount > 0 && ColCount % 2 == 1)
						{
							return true;
						}
						break;
					}
					Angle += Add;
					if (Angle >= 360)
					{
						Angle = 0;
						Add /= 2;
					}
				}
			}
		}
		for (INT32 i = 0; i < B->GetVertexNum(); ++i)
		{
			EPN_Pos CheckPos = B->GetConvertVertexPos(i);
			REAL32 Angle = (REAL32)(rand() % 360);
			REAL32 Add = 0.5f;

			if (A->GetVertexNum() > 2)
			{
				for (;;)
				{
					BOOL CheckFlag = false;
					for (INT32 j = 0; j < A->GetVertexNum(); ++j)
					{
						if (EPN_GetPosAngle(CheckPos, A->GetConvertVertexPos(j)) == Angle)
						{
							CheckFlag = true;
							break;
						}
					}
					if (!CheckFlag)
					{
						EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * (LineDistanceA + EPN_GetDistance(CheckPos, (BoxA.Top + BoxA.Bottom) / 2)) * 2;
						INT32 ColCount = 0;
						for (INT32 j = 0; j < A->GetVertexNum(); ++j)
						{
							EPN_Pos A_Line[2];
							if (j == A->GetVertexNum() - 1)
							{
								A_Line[0] = A->GetConvertVertexPos(j);
								A_Line[1] = A->GetConvertVertexPos(0);
							}
							else
							{
								A_Line[0] = A->GetConvertVertexPos(j);
								A_Line[1] = A->GetConvertVertexPos(j + 1);
							}
							if (LineColCheck((long)CheckPos.X, (long)CheckPos.Y, (long)CheckPos2.X, (long)CheckPos2.Y, (long)A_Line[0].X, (long)A_Line[0].Y, (long)A_Line[1].X, (long)A_Line[1].Y, &X, &Y))
							{
								ColCount++;
							}
						}
						if (ColCount > 0 && ColCount % 2 == 1)
						{
							return true;
						}
						break;
					}
					Angle += Add;
					if (Angle >= 360)
					{
						Angle = 0;
						Add /= 2;
					}
				}
			}
		}
	}

	return false;
}

BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Circle B)
{
	if (A == NULL || A->GetVertexNum() <= 0)
		return false;

	EPN_Rect BoxA = EPN_Rect(A->GetConvertVertexPos(0).X, A->GetConvertVertexPos(0).Y, A->GetConvertVertexPos(0).X, A->GetConvertVertexPos(0).Y);
	EPN_Rect BoxB = EPN_Rect(0, 0, 0, 0);
	REAL32 LineDistanceA = 0;
	REAL32 LineDistanceB = B.CircleRadius * 2;
	for (INT32 i = 0; i < A->GetVertexNum(); ++i)
	{
		if (BoxA.Top.X > A->GetConvertVertexPos(i).X)
		{
			BoxA.Top.X = A->GetConvertVertexPos(i).X;
		}
		if (BoxA.Top.Y > A->GetConvertVertexPos(i).Y)
		{
			BoxA.Top.Y = A->GetConvertVertexPos(i).Y;
		}
		if (BoxA.Bottom.X < A->GetConvertVertexPos(i).X)
		{
			BoxA.Bottom.X = A->GetConvertVertexPos(i).X;
		}
		if (BoxA.Bottom.Y < A->GetConvertVertexPos(i).Y)
		{
			BoxA.Bottom.Y = A->GetConvertVertexPos(i).Y;
		}
		if (i == A->GetVertexNum() - 1)
		{
			REAL32 Distance = EPN_GetDistance(A->GetConvertVertexPos(i), A->GetConvertVertexPos(0));
			if (LineDistanceA < Distance)
				LineDistanceA = Distance;
		}
		else
		{
			REAL32 Distance = EPN_GetDistance(A->GetConvertVertexPos(i), A->GetConvertVertexPos(i + 1));
			if (LineDistanceA < Distance)
				LineDistanceA = Distance;
		}
	}
	BoxB.Top.X = (B.CirclePos + EPN_GetAnglePos(270) * B.CircleRadius).X;
	BoxB.Top.Y = (B.CirclePos + EPN_GetAnglePos(0) * B.CircleRadius).Y;
	BoxB.Bottom.X = (B.CirclePos + EPN_GetAnglePos(90) * B.CircleRadius).X;
	BoxB.Bottom.Y = (B.CirclePos + EPN_GetAnglePos(180) * B.CircleRadius).Y;

	if (EPN_CheckCollision(BoxA, BoxB))
	{
		long X = 0, Y = 0;

		for (INT32 i = 0; i < A->GetVertexNum(); ++i)
		{
			if (EPN_GetDistance(A->GetConvertVertexPos(i), B.CirclePos) <= B.CircleRadius)
			{
				return true;
			}
		}

		for (INT32 i = 0; i < A->GetVertexNum(); ++i)
		{
			EPN_Pos A_Line[2];
			if (i == A->GetVertexNum() - 1)
			{
				A_Line[0] = A->GetConvertVertexPos(i);
				A_Line[1] = A->GetConvertVertexPos(0);
			}
			else
			{
				A_Line[0] = A->GetConvertVertexPos(i);
				A_Line[1] = A->GetConvertVertexPos(i + 1);
			}

			REAL32 Angle = EPN_GetPosAngle(A_Line[0], A_Line[1]);
			EPN_Pos B_Line[2];
			B_Line[0] = B.CirclePos + EPN_GetAnglePos(Angle - 90)*B.CircleRadius;
			B_Line[1] = B.CirclePos + EPN_GetAnglePos(Angle + 90)*B.CircleRadius;
			if (LineColCheck((long)A_Line[0].X, (long)A_Line[0].Y, (long)A_Line[1].X, (long)A_Line[1].Y, (long)B_Line[0].X, (long)B_Line[0].Y, (long)B_Line[1].X, (long)B_Line[1].Y, &X, &Y))
			{
				/*if(pColPos)
				(*pColPos) = EPN_Pos(X,Y);*/
				return true;
			}

		}

		EPN_Pos CheckPos = B.CirclePos;
		REAL32 Angle = (REAL32)(rand() % 360);
		REAL32 Add = 0.5f;

		if (A->GetVertexNum() > 2)
		{
			for (;;)
			{
				BOOL CheckFlag = false;
				for (INT32 i = 0; i < A->GetVertexNum(); ++i)
				{
					if (EPN_GetPosAngle(CheckPos, A->GetConvertVertexPos(i)) == Angle)
					{
						CheckFlag = true;
						break;
					}
				}
				if (!CheckFlag)
				{
					EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * (LineDistanceA + EPN_GetDistance(CheckPos, (BoxA.Top + BoxA.Bottom) / 2)) * 2;
					INT32 ColCount = 0;
					for (INT32 i = 0; i < A->GetVertexNum(); ++i)
					{
						EPN_Pos A_Line[2];
						if (i == A->GetVertexNum() - 1)
						{
							A_Line[0] = A->GetConvertVertexPos(i);
							A_Line[1] = A->GetConvertVertexPos(0);
						}
						else
						{
							A_Line[0] = A->GetConvertVertexPos(i);
							A_Line[1] = A->GetConvertVertexPos(i + 1);
						}
						if (LineColCheck((long)CheckPos.X, (long)CheckPos.Y, (long)CheckPos2.X, (long)CheckPos2.Y, (long)A_Line[0].X, (long)A_Line[0].Y, (long)A_Line[1].X, (long)A_Line[1].Y, &X, &Y))
						{
							ColCount++;
						}
					}
					if (ColCount > 0 && ColCount % 2 == 1)
						return true;
					break;
				}
				Angle += Add;
				if (Angle >= 360)
				{
					Angle = 0;
					Add /= 2;
				}
			}
		}
	}
	return false;
}
BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Polygon* B)
{
	return EPN::COMMON::FUNCTION::EPN_CheckCollision(B, A);
}
