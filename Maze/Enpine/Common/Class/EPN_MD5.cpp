#include "EPN_MD5.h"

#include "../Base/CommonFunction.h"
//지저분 하니까.. region 좀 사용하자..
#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion

#pragma region EPN_MD5 Define List

#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21

static BYTE MD5_PADDING[64] = 
{
	0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

#define EPN_F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define EPN_G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define EPN_H(x, y, z) ((x) ^ (y) ^ (z))
#define EPN_I(x, y, z) ((y) ^ ((x) | (~z)))

#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

#define FF(a, b, c, d, x, s, ac) { \
	(a) += EPN_F ((b), (c), (d)) + (x) + (UINT32)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
	}
#define GG(a, b, c, d, x, s, ac) { \
	(a) += EPN_G ((b), (c), (d)) + (x) + (UINT32)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
	}
#define HH(a, b, c, d, x, s, ac) { \
	(a) += EPN_H ((b), (c), (d)) + (x) + (UINT32)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
	}
#define II(a, b, c, d, x, s, ac) { \
	(a) += EPN_I ((b), (c), (d)) + (x) + (UINT32)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
	}
#pragma endregion
#pragma region EPN_MD5 Helper Function
VOID EPN::COMMON::EPN_MD5::MD5Transform(UINT32 State[4], BYTE Block[64])
{
	UINT32 a = State[0];
	UINT32 b = State[1];
	UINT32 c = State[2];
	UINT32 d = State[3];
	UINT32 x[16];

	Decode(x, Block, 64);

	/* Round 1 */
	FF(a, b, c, d, x[0], S11, 0xd76aa478); /* 1 */
	FF(d, a, b, c, x[1], S12, 0xe8c7b756); /* 2 */
	FF(c, d, a, b, x[2], S13, 0x242070db); /* 3 */
	FF(b, c, d, a, x[3], S14, 0xc1bdceee); /* 4 */
	FF(a, b, c, d, x[4], S11, 0xf57c0faf); /* 5 */
	FF(d, a, b, c, x[5], S12, 0x4787c62a); /* 6 */
	FF(c, d, a, b, x[6], S13, 0xa8304613); /* 7 */
	FF(b, c, d, a, x[7], S14, 0xfd469501); /* 8 */
	FF(a, b, c, d, x[8], S11, 0x698098d8); /* 9 */
	FF(d, a, b, c, x[9], S12, 0x8b44f7af); /* 10 */
	FF(c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
	FF(b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
	FF(a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
	FF(d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
	FF(c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
	FF(b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

											/* Round 2 */
	GG(a, b, c, d, x[1], S21, 0xf61e2562); /* 17 */
	GG(d, a, b, c, x[6], S22, 0xc040b340); /* 18 */
	GG(c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
	GG(b, c, d, a, x[0], S24, 0xe9b6c7aa); /* 20 */
	GG(a, b, c, d, x[5], S21, 0xd62f105d); /* 21 */
	GG(d, a, b, c, x[10], S22, 0x2441453); /* 22 */
	GG(c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
	GG(b, c, d, a, x[4], S24, 0xe7d3fbc8); /* 24 */
	GG(a, b, c, d, x[9], S21, 0x21e1cde6); /* 25 */
	GG(d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
	GG(c, d, a, b, x[3], S23, 0xf4d50d87); /* 27 */
	GG(b, c, d, a, x[8], S24, 0x455a14ed); /* 28 */
	GG(a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
	GG(d, a, b, c, x[2], S22, 0xfcefa3f8); /* 30 */
	GG(c, d, a, b, x[7], S23, 0x676f02d9); /* 31 */
	GG(b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

											/* Round 3 */
	HH(a, b, c, d, x[5], S31, 0xfffa3942); /* 33 */
	HH(d, a, b, c, x[8], S32, 0x8771f681); /* 34 */
	HH(c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
	HH(b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
	HH(a, b, c, d, x[1], S31, 0xa4beea44); /* 37 */
	HH(d, a, b, c, x[4], S32, 0x4bdecfa9); /* 38 */
	HH(c, d, a, b, x[7], S33, 0xf6bb4b60); /* 39 */
	HH(b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
	HH(a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
	HH(d, a, b, c, x[0], S32, 0xeaa127fa); /* 42 */
	HH(c, d, a, b, x[3], S33, 0xd4ef3085); /* 43 */
	HH(b, c, d, a, x[6], S34, 0x4881d05); /* 44 */
	HH(a, b, c, d, x[9], S31, 0xd9d4d039); /* 45 */
	HH(d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
	HH(c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
	HH(b, c, d, a, x[2], S34, 0xc4ac5665); /* 48 */

										   /* Round 4 */
	II(a, b, c, d, x[0], S41, 0xf4292244); /* 49 */
	II(d, a, b, c, x[7], S42, 0x432aff97); /* 50 */
	II(c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
	II(b, c, d, a, x[5], S44, 0xfc93a039); /* 52 */
	II(a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
	II(d, a, b, c, x[3], S42, 0x8f0ccc92); /* 54 */
	II(c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
	II(b, c, d, a, x[1], S44, 0x85845dd1); /* 56 */
	II(a, b, c, d, x[8], S41, 0x6fa87e4f); /* 57 */
	II(d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
	II(c, d, a, b, x[6], S43, 0xa3014314); /* 59 */
	II(b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
	II(a, b, c, d, x[4], S41, 0xf7537e82); /* 61 */
	II(d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
	II(c, d, a, b, x[2], S43, 0x2ad7d2bb); /* 63 */
	II(b, c, d, a, x[9], S44, 0xeb86d391); /* 64 */

	State[0] += a;
	State[1] += b;
	State[2] += c;
	State[3] += d;


	memset((BYTE*)x, 0, sizeof(x));
}

VOID EPN::COMMON::EPN_MD5::Encode(BYTE* pOutput, UINT32* pInput, UINT32 Len)
{
	UINT32 IdxA = 0;
	UINT32 IdxB = 0;

	for (;IdxB < Len;)
	{
		pOutput[IdxB] = (BYTE)(pInput[IdxA] & 0xff);
		pOutput[IdxB + 1] = (BYTE)((pInput[IdxA] >> 8) & 0xff);
		pOutput[IdxB + 2] = (BYTE)((pInput[IdxA] >> 16) & 0xff);
		pOutput[IdxB + 3] = (BYTE)((pInput[IdxA] >> 24) & 0xff);

		IdxA++;
		IdxB += 4;
	}
}

VOID EPN::COMMON::EPN_MD5::Decode(UINT32* pOutput, BYTE* pInput, UINT32 Len)
{
	UINT32 IdxA = 0;
	UINT32 IdxB = 0;

	for (;IdxB < Len;)
	{
		pOutput[IdxA] = ((UINT32)pInput[IdxB]) | (((UINT32)pInput[IdxB + 1]) << 8) | (((UINT32)pInput[IdxB + 2]) << 16) | (((UINT32)pInput[IdxB + 3]) << 24);

		IdxA++;
		IdxB += 4;
	}
}
#pragma endregion
#pragma region EPN_MD5 Function
EPN::COMMON::EPN_MD5 :: EPN_MD5()
{
	Init();
}
VOID EPN::COMMON::EPN_MD5 :: Init()
{
	m_StateKey.Count[0] = m_StateKey.Count[1] = 0;

	m_StateKey.State[0] = 0x67452301;
	m_StateKey.State[1] = 0xefcdab89;
	m_StateKey.State[2] = 0x98badcfe;
	m_StateKey.State[3] = 0x10325476;
}

VOID EPN::COMMON::EPN_MD5 :: Update(BYTE *pInput, UINT32 Len)
{
	UINT32 Idx = 0;
	UINT32 Index = (UINT32)((m_StateKey.Count[0] >> 3) & 0x3F);
	UINT32 PartLen = 0;

	if ((m_StateKey.Count[0] += ((UINT32)Len << 3)) < ((UINT32)Len << 3))
	{
		m_StateKey.Count[1]++;
	}
	m_StateKey.Count[1] += ((UINT32)Len >> 29);

	PartLen = 64 - Index;

	if (Len >= PartLen) 
	{
		memcpy((BYTE*)&m_StateKey.Buffer[Index], (BYTE*)pInput, PartLen);
		MD5Transform(m_StateKey.State, m_StateKey.Buffer);

		for (Idx = PartLen; Idx + 63 < Len; Idx += 64)
		{
			MD5Transform(m_StateKey.State, &pInput[Idx]);
		}

		Index = 0;
	}
	else
		Idx = 0;

	memcpy((BYTE*)&m_StateKey.Buffer[Index], (BYTE*)&pInput[Idx], Len - Idx);
}

VOID EPN::COMMON::EPN_MD5 :: Final()
{
	BYTE bits[8];
	UINT32 Index;
	UINT32 PadLen;

	Encode(bits, m_StateKey.Count, 8);

	Index = (UINT32)((m_StateKey.Count[0] >> 3) & 0x3f);
	PadLen = (Index < 56) ? (56 - Index) : (120 - Index);
	Update(MD5_PADDING, PadLen);

	Update(bits, 8);

	Encode(m_DigestRaw, m_StateKey.State, 16);

	memset((BYTE*)&m_StateKey, 0, sizeof(m_StateKey));

	WriteToString();
}

VOID EPN::COMMON::EPN_MD5 :: WriteToString()
{
	for (INT32 Pos = 0; Pos < 16; Pos++)
	{
		sprintf_s(m_DigestChars + (Pos * 2), 3, "%02x", m_DigestRaw[Pos]);
	}
}

BOOL EPN::COMMON::EPN_MD5 ::GetFileHash32(CHAR *FilePath, BYTE* pDestBuffer, UINT8 DestBufferSize)
{
	if (DestBufferSize < 0 || DestBufferSize > MD5_HASH_32)
	{
		goto GET_HASH_FAIL;
	}
	Init();
	FILE *pFile = NULL_PTR;
	INT32 Len = 0;
	BYTE Buffer[1024];
	
	ERRNO_T Result = fopen_s(&pFile, FilePath, "rb");
	
	if (Result != FOPEN_OK)
	{
		//printf("%s can't be opened\n", filename);
		goto GET_HASH_FAIL;
	}
	else if(pFile)
	{
		for (;;)
		{
			Len = fread(Buffer, 1, _countof(Buffer), pFile);
			if (Len == 0)
				break;
			Update(Buffer, Len);
		}

		fclose(pFile);
	}
	else
	{
		goto GET_HASH_FAIL;
	}
	Final();
	memcpy((BYTE*)pDestBuffer, m_DigestChars, DestBufferSize);

	return TRUE;
GET_HASH_FAIL :
	memset((BYTE*)m_DigestChars, 0, MD5_HASH_32 + 1);
	memset((BYTE*)pDestBuffer, 0, DestBufferSize);
	return FALSE;
}


BOOL EPN::COMMON::EPN_MD5 ::GetFilePtrHash32(FILE* pFile, BYTE* pDestBuffer, UINT8 DestBufferSize)
{
	if (DestBufferSize < 0 || DestBufferSize > MD5_HASH_32)
	{
		goto GET_HASH_FAIL;
	}

	Init();
	INT32 Len = 0;
	BYTE Buffer[1024];

	if (pFile == NULL_PTR)
	{
		//printf("can't be opened\n");
		goto GET_HASH_FAIL;
	}
	else
	{
		UINT32 CurEPN_Pos = ftell(pFile);
		fseek(pFile, 0, SEEK_SET);

		for (;;)
		{
			Len = fread(Buffer, 1, _countof(Buffer), pFile);
			if (Len == 0)
				break;
			Update(Buffer, Len);
		}

		fseek(pFile, CurEPN_Pos, SEEK_SET);
	}

	Final();
	memcpy((BYTE*)pDestBuffer, m_DigestChars, DestBufferSize);

	return TRUE;
GET_HASH_FAIL:
	memset((BYTE*)m_DigestChars, 0, MD5_HASH_32 + 1);
	memset((BYTE*)pDestBuffer, 0, DestBufferSize);
	return FALSE;
}

BOOL EPN::COMMON::EPN_MD5 :: GetFileHandleHash32(HANDLE Handle, BYTE* pDestBuffer, UINT8 DestBufferSize)
{
	if (DestBufferSize < 0 || DestBufferSize > MD5_HASH_32)
	{
		goto GET_HASH_FAIL;
	}
	Init();

	DWORD Len = 0;
	BYTE Buffer[1024];

	if (Handle == INVALID_HANDLE_VALUE)
	{
		//printf("can't be opened\n");
		goto GET_HASH_FAIL;
	}
	else
	{
		SetFilePointer(Handle, 0, 0, FILE_CURRENT);
		SetFilePointer(Handle, 0, 0, FILE_BEGIN);

		for (;;)
		{
			BOOL Check = ReadFile(Handle, Buffer, _countof(Buffer), &Len, NULL);
			if (Check == TRUE && Len == 0)
				break;
			Update(Buffer, Len);
		}
		SetFilePointer(Handle, 0, 0, FILE_BEGIN);
	}
	Final();

	memcpy((BYTE*)pDestBuffer, m_DigestChars, DestBufferSize);

	return TRUE;
GET_HASH_FAIL:
	memset((BYTE*)m_DigestChars, 0, MD5_HASH_32 + 1);
	memset((BYTE*)pDestBuffer, 0, DestBufferSize);
	return FALSE;
}


BOOL EPN::COMMON::EPN_MD5 :: GetBufferHash32(BYTE *pSrcBuffer, INT32 SrcBufferSize, BYTE* pDestBuffer, UINT8 DestBufferSize)
{
	if (DestBufferSize < 0 || DestBufferSize > MD5_HASH_32)
	{
		goto GET_HASH_FAIL;
	}

	Init();
	Update(pSrcBuffer, SrcBufferSize);
	Final();

	memcpy((BYTE*)pDestBuffer, m_DigestChars, DestBufferSize);

	return TRUE;
GET_HASH_FAIL:
	memset((BYTE*)m_DigestChars, 0, MD5_HASH_32 + 1);
	memset((BYTE*)pDestBuffer, 0, DestBufferSize);
	return FALSE;
}

BOOL EPN::COMMON::EPN_MD5::GetStringHash32(CHAR *pStr, BYTE* pDestBuffer, UINT8 DestBufferSize)
{
	if (DestBufferSize < 0 || DestBufferSize > MD5_HASH_32)
	{
		goto GET_HASH_FAIL;
	}

	Init();
	Update((BYTE*)pStr, strlen(pStr));
	Final();

	memcpy((BYTE*)pDestBuffer, m_DigestChars, DestBufferSize);

	return TRUE;
GET_HASH_FAIL:
	memset((BYTE*)m_DigestChars, 0, MD5_HASH_32 + 1);
	memset((BYTE*)pDestBuffer, 0, DestBufferSize);
	return FALSE;
}
#pragma endregion