#include "EPN_StringIndexPool.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion

EPN_StringIndexPool :: EPN_StringIndexPool()
{
	SaveMap = new isMap**[EPN_SI_STRLEN_MAX];
	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		SaveMap[i] = new isMap*[512];
		for(INT32 j = 0;j < 512;++j)
		{
			SaveMap[i][j] = new isMap;
		}
	}
}

EPN_StringIndexPool :: ~EPN_StringIndexPool()
{
	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		for(INT32 j = 0;j < 512;++j)
		{
			for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
			{
				delete Itor->first;
			}
			SaveMap[i][j]->clear();
			delete SaveMap[i][j];
		}
		delete []SaveMap[i];
	}
	delete []SaveMap;
}

BOOL EPN_StringIndexPool :: InsertData(estring Str,INT32 Index)
{
	if(Str.Length()-1 >= EPN_SI_STRLEN_MAX)
	{
		return FALSE;
	}
	INT32 i = Str.Length()-1;
	BYTE j = Str.at(Str.Length()-1) + Str.at(0);

	for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
	{
		if(Itor->second == Str || (*Itor->first) == Index)
		{
			return FALSE;
		}
	}
	SaveMap[i][j]->insert(isMapType(new INT32(Index),Str));
	return TRUE;
}


BOOL EPN_StringIndexPool :: DeleteData(estring Str)
{
	if(Str.Length()-1 >= EPN_SI_STRLEN_MAX)
		return FALSE;
	INT32 i = Str.Length()-1;
	BYTE j = Str.at(Str.Length()-1) + Str.at(0);
	for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();)
	{
		if(Itor->second == Str)
		{
			delete Itor->first;
			SaveMap[i][j]->erase(Itor);
			return TRUE;
		}
		else
			++Itor;
	}
	return FALSE;
}
BOOL EPN_StringIndexPool :: DeleteData(INT32 Index)
{
	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		for(INT32 j = 0;j < 256;++j)
		{
			for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();)
			{
				if((*Itor->first) == Index)
				{
					delete Itor->first;
					SaveMap[i][j]->erase(Itor++);
					return TRUE;
				}
				else
					++Itor;
			}
		}
	}
	

	return FALSE;
}
INT32 EPN_StringIndexPool :: AllDelete()
{
	INT32 Count = 0;

	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		for(INT32 j = 0;j < 256;++j)
		{
			for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
			{
				delete Itor->first;
				++Count;
			}
			SaveMap[i][j]->clear();
		}
		
	}

	return Count;
}
estring EPN_StringIndexPool :: SearchString(INT32 Index)
{
	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		for(INT32 j = 0;j < 256;++j)
		{
			for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
			{
				if((*Itor->first) == Index)
					return Itor->second;
			}
		}
	}
	return "";
}
INT32 EPN_StringIndexPool :: SearchIndex(estring Str)
{
	if(Str.Length() <= 0 || Str.Length()-1 >= EPN_SI_STRLEN_MAX)
		return -1;
	INT32 i = Str.Length()-1;
	BYTE j = Str.at(Str.Length()-1) + Str.at(0);
	for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
	{
		if(Itor->second == Str)
			return (*Itor->first);
	}
	return -1;
}


VOID EPN_StringIndexPool :: AllPrint()
{
	INT32 Count = 0;
	for(INT32 i = 0;i < EPN_SI_STRLEN_MAX;++i)
	{
		for(INT32 j = 0;j < 256;++j)
		{
			for(isMapItor Itor = SaveMap[i][j]->begin();Itor != SaveMap[i][j]->end();++Itor)
			{
				std::cout <<"문자열길이 : "<<i+1<<" 저장배열 : "<< ++Count << " : " << *(Itor->first) <<" , "<< Itor->second << std::endl;
				//cout <<"문자열길이 : "<<i+1<<" 저장배열 : "<< ++Count << " : " << (*Itor->second) <<" , "<< Itor->first << endl;
			}
		}
		Count = 0;
	}
}
