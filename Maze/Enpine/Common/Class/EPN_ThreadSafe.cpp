#include "EPN_ThreadSafe.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion

EPN_ThreadSafe :: EPN_ThreadSafe()
{
	m_SafeFlag = THREAD_SAFE_FLAG::NON;
	m_AccessSpinCount = 4000;

}
EPN_ThreadSafe :: ~EPN_ThreadSafe()
{

}

VOID EPN_ThreadSafe :: SetSpinCount(UINT32 Count)
{
	m_AccessSpinCount = Count;
}

UINT32 EPN_ThreadSafe :: GetSpinCount()
{
	return m_AccessSpinCount;
}

VOID EPN_ThreadSafe :: SetSafeFlag(EPN::COMMON::THREAD_SAFE_FLAG Flag)
{
	m_SafeFlag = Flag;
}
EPN::COMMON::THREAD_SAFE_FLAG EPN_ThreadSafe :: GetSafeFlag()
{
	return m_SafeFlag;
}