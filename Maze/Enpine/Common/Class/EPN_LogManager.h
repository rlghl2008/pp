#pragma once 

#include "../Base/CommonBase.h"

namespace EPN
{
	namespace COMMON
	{
		enum PRINT_LOG_FLAG
		{
			LOG_NON = 0,
			LOG_CONSOLE = 1, //콘솔창 출력
			LOG_DEBUG = 2,   //디버그뷰 출력
			LOG_FILE = 4,    //파일로 출력
			LOG_DETAIL = 8,  //로그 내용 디테일
			LOG_BOX = 16,	 //메세지 박스로 출력
		};

		class EPN_LogManager
		{
		public:
			EPN_LogManager();
			~EPN_LogManager();
		private:
			BYTE m_LogFlag;
			CHAR m_FilePath[PATH_MAX];

		public:
			BYTE GetLogFlag();
			VOID SetLogFlag(BYTE Flag);
			CONST CHAR* GetFilePath();
			BOOL SetFilePath(CONST CHAR* Path);
		private:
			BOOL SimpleLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List);
			BOOL DetailLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List);
		public:
			//m_LogFlag 값 기준으로 처리합니다.
			BOOL PrintLog(CONST CHAR* pFmt, ...);
			BOOL PrintLog(CONST CHAR* pFmt, va_list* pVA_List);

			//Flag 인자값 기준으로 처리합니다.
			//EX : PrintfLog(LOG_CONSOLE | LOG_DETAIL, "테스트 값 : %d", i);
			BOOL PrintLog(BYTE LogFlag, CONST CHAR* pFmt, ...);
			BOOL PrintLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List);

		private:
			static EPN_LogManager* m_pLogManager;
		public:
			static EPN_LogManager* GetInstance();
			static EPN_LogManager* CreateInstance(VOID);
			static BOOL ReleaseInstance();

		};
		namespace FUNCTION
		{
			//LOG SYSTEM
			BOOL SET_LOG_FLAG(BYTE LogFlag);
			BOOL PRINT_LOG(CHAR* pFmt, ...);
			BOOL PRINT_LOG(BYTE LogFlag, CHAR* pFmt, ...);
			BOOL RELEASE_LOG();

		}
	}
}
#pragma once 
