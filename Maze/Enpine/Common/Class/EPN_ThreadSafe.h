#pragma once

#include "../Base/CommonBase.h"

namespace EPN
{
	namespace COMMON
	{
		enum class THREAD_SAFE_FLAG : BYTE
		{
			NON = 0,
			MUTEX,
			SEMAPHORE,
			CRITICAL,
			ATOMIC
		};

		class  EPN_ThreadSafe
		{
		private:
			THREAD_SAFE_FLAG m_SafeFlag;
			UINT32 m_AccessSpinCount; // Defualt : 4000
		public:
			EPN_ThreadSafe();
			virtual ~EPN_ThreadSafe();
		public:
			VOID SetSpinCount(UINT32 Count);
			UINT32 GetSpinCount();

			VOID SetSafeFlag(EPN::COMMON::THREAD_SAFE_FLAG Flag);
			EPN::COMMON::THREAD_SAFE_FLAG GetSafeFlag();
		public:
			virtual VOID LockArea() = 0;
			virtual VOID UnLockArea() = 0;

		};
	}
}