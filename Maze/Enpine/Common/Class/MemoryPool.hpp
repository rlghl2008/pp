#pragma once 

#include "LogManager.h"

namespace LS
{
	namespace COMMON
	{

		enum MEMORY_STATE : BYTE
		{
			MEMORY_FREE = 0,
			MEMORY_WORK = 1
		};

		template< typename T >
		class MemoryPool
		{
		private:
			enum DEFULAT_OPTION
			{
				ALLOCATE_INFO_COUNT = 2,
				// ALLOCATE_DATA_COUNT 는 8의 배수가 효율적 입니다.
				ALLOCATE_DATA_COUNT = 32
			};
		private:
			struct MemoryInfo
			{
				INT32 DataCount = 0;
				// Bit Flags ArraySize [ (DataCount / 8 + 1) ]
				BYTE* pMemoryState;
				// Real Data ArraySize [DEFAULT_MEMORY_POOL_SIZE]
				T*	  pMemoryData;

				MemoryInfo():DataCount(0), pMemoryState(NULL_PTR), pMemoryData(NULL_PTR){}
				~MemoryInfo()
				{
					if(pMemoryState != NULL_PTR)
					{
						delete[] pMemoryState;
						delete[] pMemoryData;
						pMemoryState = NULL_PTR;
						pMemoryData = NULL_PTR;
					}
				}
			};
		private:
			// 생성된 Info 의 주소 보관
			UINT32* m_pMemoryInfoArray;
	
			// ALLOCATE_INFO_COUNT
			INT32	m_AllocateInfoCount;
			// ALLOCATE_DATA_COUNT
			INT32	m_AllocateDataCount;

			// 실제로 생성된 Memory Info 의 수
			INT32	m_InfoPoolCount;
			// 실제로 생성된 <T> Data 의 수
			INT32	m_DataPoolCount;

			// AddData() 로 추가된 Data 수
			INT32	m_AddDataCount;
			INT32	m_AddIndex;
		public:
			MemoryPool();
			~MemoryPool();
		public:
			BOOL	Clear();
			BOOL	Release();
		private:
			VOID* GetMemoryInfo(INT32 DataIndex);
			T*  GetMemoryData(INT32 DataIndex);
			VOID  SetMemoryState(INT32 DataIndex, MEMORY_STATE State);
			VOID  FindNextAdd();
		public:
			// Memory Pool 이 사용되지 않은 상태에서만 Pool 옵션 변경가능
			BOOL CustomizePoolSize(INT32 AllocateInfoCount, INT32 AllocateDataCount);	
			INT32  GetAllocateInfoCount() CONST { return m_AllocateInfoCount; }
			INT32  GetAllocateDataCount() CONST { return m_AllocateDataCount; }
		public:
			INT32  GetInfoPoolCount() CONST { return m_InfoPoolCount; }
			INT32  GetDataPoolCount() CONST { return m_DataPoolCount; }
			INT32  GetAddDataCount() CONST { return m_AddDataCount; }
		public:
			INT32	AddData(T Data);
			BOOL	EraseData(INT32 Index, BOOL DefragFlag = FALSE);// DefragFlag 가 TRUE 이면 데이터들을 당겨서 빈공간을 메꿉니다.

			T		GetData(INT32 Index) CONST;
			T*		GetDataPtr(INT32 Index) CONST;
			BOOL	IsData(INT32 Index) CONST;
			MEMORY_STATE GetDataState(INT32 Index) CONST;

			VOID	DefragData();
		public:
			T* operator [](INT32 Index)
			{
				return GetDataPtr(Index);
			}
		};

		template< typename T >
		MemoryPool< T >::MemoryPool()
		{
			m_pMemoryInfoArray = NULL_PTR;
			m_AllocateInfoCount = ALLOCATE_INFO_COUNT;
			m_AllocateDataCount = ALLOCATE_DATA_COUNT;
			m_InfoPoolCount = 0;
			m_DataPoolCount = 0;
			m_AddDataCount = 0;
			m_AddIndex = 0;
		}
		template< typename T >
		MemoryPool< T >::~MemoryPool()
		{
			Release();
		}

		template< typename T >
		BOOL MemoryPool< T >::Clear()
		{
			INT32 AddCount = GetAddDataCount();
			for (INT32 Cnt = 0; Cnt < AddCount;)
			{
				if (IsData(Cnt))
				{
					EraseData(Cnt);
					Cnt++;
				}
			}

			m_AddDataCount = 0;
			//m_AddIndex = 0;
			return TRUE;
		}
		template< typename T >
		BOOL MemoryPool< T >::Release()
		{
			if (m_pMemoryInfoArray != NULL_PTR)
			{
				for (INT32 Cnt = 0; Cnt < m_InfoPoolCount; Cnt++)
				{
					delete ((MemoryInfo*)(m_pMemoryInfoArray[Cnt]));
				}
				delete[] m_pMemoryInfoArray;
				m_pMemoryInfoArray = NULL_PTR;
			}

			m_InfoPoolCount = 0;
			m_DataPoolCount = 0;
			m_AddDataCount = 0;

			return TRUE;
		}

		template< typename T >
		VOID* MemoryPool< T >::GetMemoryInfo(INT32 DataIndex)
		{
			if (DataIndex < 0 || DataIndex >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetMemoryInfo(%d) Error", DataIndex);
				return NULL_PTR;
			}
			return ((MemoryInfo*)(m_pMemoryInfoArray[DataIndex / m_AllocateDataCount]));
		}
		template< typename T >
		T* MemoryPool< T >::GetMemoryData(INT32 DataIndex)
		{
			if (DataIndex < 0 || DataIndex >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetMemoryData(%d) Error", DataIndex);
				return NULL;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[DataIndex / m_AllocateDataCount]));
			return &(pMemoryInfo->pMemoryData[DataIndex%m_AllocateDataCount]);
		}
		template< typename T >
		VOID MemoryPool< T >::SetMemoryState(INT32 DataIndex, MEMORY_STATE State)
		{
			if (DataIndex < 0 || DataIndex >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: SetMemoryState(%d) Error", DataIndex);
				return;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[DataIndex / m_AllocateDataCount]));
			if(State == MEMORY_WORK)
				pMemoryInfo->pMemoryState[(DataIndex % m_AllocateDataCount) / 8] |= (MEMORY_WORK << (DataIndex % 8));
			else
				pMemoryInfo->pMemoryState[(DataIndex % m_AllocateDataCount) / 8] ^= (MEMORY_WORK << (DataIndex % 8));

		}
		
		template< typename T >
		VOID  MemoryPool< T >::FindNextAdd()
		{
			if ((m_AddDataCount % m_DataPoolCount) == 0)
			{
				m_AddIndex = m_AddDataCount;
			}
			else
			{
				for (INT32 Index = m_AddIndex / m_AllocateDataCount, StateIndex = 0; Index < m_InfoPoolCount;)
				{
					MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index]));
					if (pMemoryInfo->DataCount < m_AllocateDataCount)
					{
						if (Index == m_AddIndex / m_AllocateDataCount && StateIndex == 0)
							StateIndex = (m_AddIndex % m_AllocateDataCount) / 8;

						if (pMemoryInfo->pMemoryState[StateIndex] != 0xFF)
						{
							for (INT32 BitIndex = 1, BitCount = 0; BitIndex <= 128; BitCount++)
							{
								BYTE StatePool = pMemoryInfo->pMemoryState[StateIndex];
								if ((StatePool & BitIndex) == 0)
								{
									m_AddIndex = (Index * m_AllocateDataCount) + (StateIndex * 8) + BitCount;
									return;
								}

								BitIndex <<= 1;
							}
						}

						StateIndex++;
						if (StateIndex >= m_AllocateDataCount / 8)
						{
							StateIndex = 0;
							Index++;
						}
					}
					else
					{
						StateIndex = 0;
						Index++;
					}
				}
			}
		}

		template< typename T >
		BOOL MemoryPool< T >::CustomizePoolSize(INT32 AllocateInfoCount, INT32 AllocateDataCount)
		{
			if (m_pMemoryInfo != NULL_PTR)
				return FALSE;
			if (AllocateInfoCount < 1 || m_AllocateDataCount < 1)
				return FALSE;

			m_AllocateInfoCount = AllocateInfoCount;
			m_AllocateDataCount = AllocateDataCount;

			return TRUE;
		}
		template< typename T >
		INT32 MemoryPool< T >::AddData(T Data)
		{
			INT32 AllocateCount = 0;
			if(m_DataPoolCount > 0)
				AllocateCount = m_AddDataCount % m_DataPoolCount;
			if (AllocateCount == 0)
			{
				UINT32* pMemoryInfoArray = new UINT32[m_InfoPoolCount + m_AllocateInfoCount];
				if (m_pMemoryInfoArray != NULL_PTR)
				{
					memcpy(pMemoryInfoArray, m_pMemoryInfoArray, sizeof(UINT32) * m_InfoPoolCount);
					delete[] m_pMemoryInfoArray;
				}
				m_pMemoryInfoArray = pMemoryInfoArray;

				

				for (INT32 Cnt = 0; Cnt < m_AllocateInfoCount; Cnt++)
				{
					MemoryInfo* pAllocateInfo = new MemoryInfo();

					if (pAllocateInfo == NULL_PTR)
					{
						LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: AddData(T)\n(Allocate Failed)");
						return -1;
					}
					INT32 AllocateStateCount = (m_AllocateDataCount + 7) / 8;
					pAllocateInfo->pMemoryState = new BYTE[AllocateStateCount];
					memset(pAllocateInfo->pMemoryState, MEMORY_FREE, sizeof(BYTE)*AllocateStateCount);

					pAllocateInfo->pMemoryData = new T[m_AllocateDataCount];
					m_pMemoryInfoArray[m_InfoPoolCount] = (UINT32)pAllocateInfo;

					m_InfoPoolCount++;
					m_DataPoolCount += m_AllocateDataCount;
				}
			}
			
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[m_AddIndex / m_AllocateDataCount]));
			pMemoryInfo->pMemoryState[(m_AddIndex % m_AllocateDataCount) / 8] |= (MEMORY_WORK << (m_AddIndex % 8));
			pMemoryInfo->pMemoryData[m_AddIndex % m_AllocateDataCount] = Data;
			pMemoryInfo->DataCount++;
			m_AddDataCount++;

			FindNextAdd();
			
			return m_AddDataCount - 1;
		}
		template< typename T >
		T MemoryPool< T >::GetData(INT32 Index) CONST
		{
			if (Index < 0 || Index >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetData(%d) Error", Index);
				return T();
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index / m_AllocateDataCount]));
			BYTE StatePool = pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8];
			StatePool <<= 7 - (Index % 8);
			StatePool >>= 7;
			if (StatePool != MEMORY_WORK)
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetData(%d) NULL", Index);
				return T();
			}
			return (pMemoryInfo->pMemoryData[Index%m_AllocateDataCount]);
		}
		template< typename T >
		T* MemoryPool< T >::GetDataPtr(INT32 Index) CONST
		{
			if (Index < 0 || Index >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetData(%d) Error", Index);
				return NULL_PTR;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index / m_AllocateDataCount]));
			BYTE StatePool = pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8];
			StatePool <<= 7 - (Index % 8);
			StatePool >>= 7;
			if (StatePool != MEMORY_WORK)
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetData(%d) NULL", Index);
				return NULL_PTR;
			}
			return &(pMemoryInfo->pMemoryData[Index%m_AllocateDataCount]);
		}

		template< typename T >
		MEMORY_STATE MemoryPool< T >::GetDataState(INT32 Index) CONST
		{
			if (Index < 0 || Index >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: GetDataState(%d) Error", Index);
				return MEMORY_FREE;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index / m_AllocateDataCount]));
			BYTE StatePool = pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8];
			StatePool <<= 7 - (Index % 8);
			StatePool >>= 7;
			return (MEMORY_STATE)StatePool;
		}
		template< typename T >
		BOOL  MemoryPool< T >::IsData(INT32 Index) CONST
		{
			if (Index < 0 || Index >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: IsData(%d) Error", Index);
				return FALSE;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index / m_AllocateDataCount]));
			BYTE StatePool = pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8];
			StatePool <<= 7 - (Index % 8);
			StatePool >>= 7;
			if (StatePool != MEMORY_WORK)
			{
				return FALSE;
			}
			return TRUE;
		}
		template< typename T >
		BOOL MemoryPool< T >::EraseData(INT32 Index, BOOL DefragFlag)
		{
			if (Index < 0 || Index >= GetDataPoolCount())
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: EraseData(%d) Error", Index);
				return FALSE;
			}
			MemoryInfo* pMemoryInfo = ((MemoryInfo*)(m_pMemoryInfoArray[Index / m_AllocateDataCount]));
			BYTE StatePool = pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8];
			StatePool <<= 7 - (Index % 8);
			StatePool >>= 7;
			if (StatePool != MEMORY_WORK)
			{
				LS::COMMON::FUNCTION::PRINT_LOG("MemoryPool :: EraseData(%d) Free", Index);
				return FALSE;
			}

			pMemoryInfo->pMemoryState[(Index % m_AllocateDataCount) / 8] ^= (0x01 << (Index % 8));
			if (DefragFlag == TRUE)
				DefragData();
			else if (m_AddIndex > Index)
				m_AddIndex = Index;

			m_AddDataCount--;
			return TRUE;
		}

		template< typename T >
		VOID MemoryPool< T >::DefragData()
		{
			for (INT32 Cnt = 0; Cnt < m_DataPoolCount; Cnt++)
			{
				if (IsData(Cnt) == FALSE)
				{
					INT32 DestCnt = Cnt;
					for (INT32 Cnt2 = DestCnt +1; Cnt2 < m_DataPoolCount; Cnt2++)
					{
						if (IsData(Cnt2) == FALSE)
						{
							continue;
						}

						//클래스라면 깊은 복사가 있어야 제대로 동작할듯..
						(*GetMemoryData(DestCnt)) = (*GetMemoryData(Cnt2));
						SetMemoryState(DestCnt, MEMORY_WORK);
						SetMemoryState(Cnt2, MEMORY_FREE);
						DestCnt++;
						if (DestCnt >= m_AddDataCount)
							break;
					}
				}
			}
		}
	}
}