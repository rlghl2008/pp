#pragma once 


#include "../Base/CommonBase.h"


namespace EPN
{
	namespace COMMON
	{
		#define EPN_CRYPT_KEY_MAX 16
		#define EPN_BYTE_MAP_HEIGHT 16
		#define EPN_BYTE_MAP_WIDTH 16
		#define EPN_BYTE_MAP_LENGTH (EPN_BYTE_MAP_WIDTH*EPN_BYTE_MAP_HEIGHT)

		class EPN_Crypto
		{
		public:
			EPN_Crypto();
			~EPN_Crypto();
			//STATIC USE 
		/*
		private:
			static EPN_Crypto* m_pCrypto;
		public:
			static EPN_Crypto* GetInstance();
			static EPN_Crypto* CreateInstance(VOID);
			static BOOL ReleaseInstance();
		*/
		
		private:
			// 키값 기준으로 암호화 알고리즘 변경
			UINT32 m_AlgorithmKey;
			// 키값 기준으로 ByteMap 셋팅이 결정됨
			BYTE m_Passkey[EPN_CRYPT_KEY_MAX+1];
			BOOL m_PasskeyFlag;
			BYTE* m_pEnByteMap;
			BYTE* m_pDeByteMap;

		private:
			// ByteMapShuffle 
			VOID ByteMapShuffle_0(BYTE* pByteMap);
			VOID ByteMapShuffle_1(BYTE* pByteMap);
			VOID ByteMapShuffle_2(BYTE* pByteMap);
			VOID ByteMapShuffle_3(BYTE* pByteMap);
			VOID ByteMapShuffle_4(BYTE* pByteMap);
			VOID ByteMapShuffle_5(BYTE* pByteMap);
		public:
			// DataServingKey 
			// Basic : 0
			// RandArea : 0 ~ 3
			VOID DataServingKey(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// Random Algorith
			// 정방향
			VOID DataServingKey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 역방향
			VOID DataServingKey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 반 정방향
			VOID DataServingKey_2(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 반 역방향
			VOID DataServingKey_3(BYTE* pDest, BYTE* pSrc, UINT32 Size);
		public:
			// DataServingPasskey 
			// Basic : 0
			// RandArea : 0 ~ 3
			VOID DataServingPasskey(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// Random Algorith
			// 정방향
			VOID DataServingPasskey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 역방향
			VOID DataServingPasskey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 반 정방향
			VOID DataServingPasskey_2(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// 반 역방향
			VOID DataServingPasskey_3(BYTE* pDest, BYTE* pSrc, UINT32 Size);
		public:
			// DataShiftKey
			// Basic : 0
			// RandArea : 0 ~ 1
			VOID DataShiftKey(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// Right
			VOID DataShiftKey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			// Left
			VOID DataShiftKey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size);
		public:
			// BYTE MAP
			VOID EnByteMapping(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			VOID DeByteMapping(BYTE* pDest, BYTE* pSrc, UINT32 Size);
		public:
			BOOL SetPasskey(CONST BYTE Key[EPN_CRYPT_KEY_MAX]);
			BOOL IsPassKey();
		public:
			//BOOL EnCrypting(char* InputPath, char* OutputPath);
			//BOOL DeCrypting(char* InputPath, char* OutputPath);
			BOOL EnCrypting_Simple(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			BOOL DeCrypting_Simple(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			BOOL EnCrypting(BYTE* pDest, BYTE* pSrc, UINT32 Size);
			BOOL DeCrypting(BYTE* pDest, BYTE* pSrc, UINT32 Size);
		public:
			BOOL SetAlgorithmKey(UINT32 Key);
		
		public://AlgorithmKey 기준으로 암/복호화 알고리즘이 변경됨
			//BOOL EnRandCrypting_Simple(char* pDest, const char* pSrc, int Size = AES_MAX_SIZE);
			//BOOL DeRandCrypting_Simple (char* pDest, const char* pSrc, int Size = AES_MAX_SIZE);
			//BOOL EnRandCrypting(char* pDest, const char* pSrc, int Size = AES_MAX_SIZE);
			//BOOL DeRandCrypting(char* pDest, const char* pSrc, int Size = AES_MAX_SIZE);


		};

	}
}