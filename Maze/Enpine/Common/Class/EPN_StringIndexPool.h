#pragma once

#include "../Base/CommonBase.h"
#include <map>
#include "EPN_String.h"

namespace EPN
{
	namespace COMMON
	{
		CONST INT32 EPN_SI_STRLEN_MAX = 30;

		typedef std::map<INT32*, estring> isMap;
		typedef isMap::iterator isMapItor;
		typedef isMap::value_type isMapType;

		class EPN_StringIndexPool
		{
		private:
			isMap ***SaveMap;
		public:
			EPN_StringIndexPool();
			virtual ~EPN_StringIndexPool();
		public:
			BOOL InsertData(estring Str, INT32 Index);

			BOOL DeleteData(estring Str);//하나만 찾아 딜리트후 결과리턴
			BOOL DeleteData(INT32 Index);
			INT32 AllDelete();//제거된 데이터 개수 반환

			estring SearchString(INT32 Index);
			INT32 SearchIndex(estring Str);

			VOID AllPrint();
		};
	}
}