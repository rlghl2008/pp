#pragma once

#include "../Base/CommonBase.h"
#include "EPN_ThreadSafe.h"

namespace EPN
{
	namespace COMMON
	{
		class EPN_CriticalSection : public EPN_ThreadSafe
		{
		private:
			CRITICAL_SECTION m_CS;
		public:
			EPN_CriticalSection();
			virtual ~EPN_CriticalSection();
		public:
			virtual VOID LockArea();
			virtual VOID UnLockArea();


		};
	}
}