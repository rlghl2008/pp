#include "EPN_FileStream.h"

#include "../../Common/Base/CommonFunction.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN :: COMMON;
using namespace EPN :: COMMON :: FUNCTION;
#pragma endregion



EPN_FileStream :: EPN_FileStream()
{
	m_pFile = NULL_PTR;

	m_FilePath[0] = 0;
	m_FileName[0] = 0;
	m_FileExtension[0] = 0;
}

EPN_FileStream :: ~EPN_FileStream(VOID)
{
	if (m_pFile != NULL_PTR)
		fclose(m_pFile);
}

BOOL EPN_FileStream ::OpenFile(CONST CHAR* Path, FILE_ACCESS_FLAG AccessFlag, FILE_STREAM_FLAG StreamFlag, BOOL BinaryModeFlag)
{
	//_wSetlocale(LC_ALL, _T("Korean"));
	if (m_pFile != NULL_PTR)
		CloseFile();

	INT32 PathLength = (INT32)strnlen_s(Path, PATH_MAX);
	if (PathLength >= PATH_MAX)
	{
		PRINT_LOG("%s\n파일 Path 길이가 %d 를 초과 합니다.", Path, PATH_MAX);
		CloseFile();
		goto OPENFILE_FAIL;
	}

	CHAR Arg[3] = { 0, };
	switch (AccessFlag)
	{
		case FILE_ACCESS_FLAG::READ:
		{
			strcpy_s(Arg, 3, "r");
			break;
		}
		case FILE_ACCESS_FLAG::WRITE:
		{
			strcpy_s(Arg, 3, "w");
			break;
		}
		case FILE_ACCESS_FLAG::READ_WRITE:
		{
			strcpy_s(Arg, 3, "r+");
			break;
		}
		case FILE_ACCESS_FLAG::ADD:
		{
			strcpy_s(Arg, 3, "a");
			break;
		}
		default:
		{
			PRINT_LOG("존재하지 않는 AccessFlag(%d) 입니다.\n", AccessFlag);
			goto OPENFILE_FAIL;
		}
	}
	if (AccessFlag != FILE_ACCESS_FLAG::READ_WRITE)
	{
		if (BinaryModeFlag == TRUE)
			strcat_s(Arg, 3, "b");
		else
			strcat_s(Arg, 3, "b");
	}

	switch (StreamFlag)
	{
		case FILE_STREAM_FLAG::OPEN_IS_EXISTING:
		{
			ERRNO_T Result = fopen_s(&m_pFile, Path, "rb");
			if (Result != FOPEN_OK)
				goto OPENFILE_FAIL;
			if(m_pFile != NULL_PTR)
				fclose(m_pFile);
			m_pFile = NULL_PTR;
			break;
		}
		case FILE_STREAM_FLAG::CREATE_IS_NULL:
		{
			ERRNO_T Result = fopen_s(&m_pFile, Path, "rb");
			if (Result == FOPEN_OK)
			{
				if (m_pFile != NULL_PTR)
					fclose(m_pFile);
				m_pFile = NULL_PTR;
			}
			else
			{
				Result = fopen_s(&m_pFile, Path, "wb");
				if (Result != FOPEN_OK)
					goto OPENFILE_FAIL;
				if (m_pFile != NULL_PTR)
					fclose(m_pFile);
				m_pFile = NULL_PTR;
			}
			break;
		}
		case FILE_STREAM_FLAG::CREATE_IS_ALWAYS:
		{
			ERRNO_T Result = fopen_s(&m_pFile, Path, "wb");
			if (Result != FOPEN_OK)
				goto OPENFILE_FAIL;
			if (m_pFile != NULL_PTR)
				fclose(m_pFile);
			m_pFile = NULL_PTR;
			break;
		}
		default:
		{
			PRINT_LOG("존재하지 않는 StreamFlag(%d) 입니다.\n", StreamFlag);
			goto OPENFILE_FAIL;
		}
	}

	ERRNO_T Result = fopen_s(&m_pFile, Path, Arg);
	if (Result != FOPEN_OK )
	{
		m_pFile = 0;
		goto OPENFILE_FAIL;
	}

	INT32 SaveCnt = 0;
	for (INT32 Cnt = PathLength - 1; Cnt >= 0; --Cnt)
	{
		if (Path[Cnt] == '.')
		{
			SaveCnt = Cnt;
			strcpy_s(m_FileExtension, PATH_MAX, Path + Cnt);
		}
		else if (Path[Cnt] == '\\' || Path[Cnt] == '/')
		{
			strcpy_s(m_FilePath, PATH_MAX, Path + Cnt + 1);
			memcpy(m_FileName, Path + Cnt + 1, SaveCnt - (Cnt + 1));
			break;
		}
	}

	return TRUE;
OPENFILE_FAIL:
	PRINT_LOG("EPN_FileStream::OpenFile 실패", StreamFlag);
	return FALSE;
}
BOOL EPN_FileStream :: CloseFile()
{
	if (m_pFile == NULL_PTR)
		return FALSE;
	fclose(m_pFile);
	m_pFile = NULL_PTR;
	return TRUE;
}
BOOL EPN_FileStream :: WriteFormat(CHAR* pFormat, ...)
{
	va_list ArgList;
	CHAR TempBuffer[1024] = { 0, };

	va_start(ArgList, pFormat);
	INT32 Result = _vsprintf_s_l(TempBuffer, _countof(TempBuffer),  pFormat, NULL_PTR, ArgList);
	va_end(ArgList);

	return Write(TempBuffer, Result);
}
BOOL EPN_FileStream :: Write(CHAR* pData, SIZE_T Size, SIZE_T* pResult)
{
	if (m_pFile == NULL_PTR)
		return FALSE;
	SIZE_T WriteSize = Size;
	SIZE_T Result = 0;
	if (Size == 0)
	{
		WriteSize = strnlen_s(pData, PATH_MAX);
	}

	Result = fwrite(pData, WriteSize, 1, m_pFile);
	if (pResult)
		*pResult = Result;

	return TRUE;
}
FILE_READ_RESULT EPN_FileStream :: Read(CHAR* pData, SIZE_T Size, SIZE_T* pResult)
{
	if (m_pFile == NULL_PTR)
	{
		PRINT_LOG("EPN_FileStream::Read 기준 File 이 존재 하지 않습니다.\n");
		return FILE_READ_RESULT::FAIL;
	}

	
	if (pResult == NULL_PTR)
	{
		SIZE_T Result = fread(pData, Size, 1, m_pFile);
	}
	else
	{
		SIZE_T Result = fread(pData, 1, Size, m_pFile);
		*pResult = Result;
	}
		

	if (IsReadEnd() == TRUE)
		return FILE_READ_RESULT::END;

	return FILE_READ_RESULT::OK;
}

INT32 EPN_FileStream :: FindWord(CHAR* Str, INT32 Size, BOOL LineCheckFlag)
{
	if (!m_pFile || !Str)
		return -1;
	if (Size < 0)
	{
		Size = (INT32)strlen(Str);
	}

	INT32 Cnt = 0;
	for (;;)
	{
		CHAR Data = 0;
		SIZE_T Result = 0;
		FILE_READ_RESULT ReadResult = Read(&Data, 1, &Result);

		if (Cnt == Size && (!LineCheckFlag || Data == ' ' || Data == '\n' || Data == '\r' || Data == '\t' || Data == -1))
		{
			return ((INT32)GetFileEPN_Pos() - 1) - Cnt;
		}
		else if (ReadResult == FILE_READ_RESULT::FAIL || ReadResult == FILE_READ_RESULT::END)
			return -1;


		if (Data == Str[Cnt])
			Cnt++;
		else if (Data == Str[0])
			Cnt = 1;
		else
			Cnt = 0;
	}

	return -1;
}
INT32 EPN_FileStream :: FindDataArray(BYTE* pData, INT32 Size)
{
	if (!m_pFile || !pData)
		return -1;

	INT32 Cnt = 0;
	for (;;)
	{
		CHAR Data = 0;
		SIZE_T Result = 0;
		FILE_READ_RESULT ReadResult = Read(&Data, 1, &Result);
		if (ReadResult == FILE_READ_RESULT::FAIL || ReadResult == FILE_READ_RESULT::END)
			break;

		if (Data == (CHAR)pData[Cnt])
		{
			Cnt++;
			if (Cnt == Size)
			{
				return (INT32)GetFileEPN_Pos() - Cnt;
			}
		}
		else if (Data == (CHAR)pData[0])
			Cnt = 1;
		else
			Cnt = 0;
	}

	return -1;
}
INT32 EPN_FileStream :: FindData(BYTE Data)
{
	if (!m_pFile)
		return -1;

	for (INT32 Cnt = 0;; Cnt++)
	{
		CHAR ReadData = 0;
		SIZE_T Result = 0;
		FILE_READ_RESULT ReadCheck = Read(&ReadData, 1, &Result);

		if (ReadCheck == FILE_READ_RESULT::FAIL || ReadCheck == FILE_READ_RESULT::END)
			break;

		if (ReadData == Data)
		{
			return (INT32)GetFileEPN_Pos() - Cnt;
		}
		else if (ReadData == -1)
		{
			break;
		}
	}
	return -1;
}

INT32 EPN_FileStream :: ReadWord(CHAR* pDest, INT32 DestSize)
{
	if (!m_pFile)
		return 0;
	if (DestSize >= 0)
		memset(pDest, 0, DestSize);

	if (DestSize != 0)
	{
		BOOL Flag = FALSE;
		INT32 Cnt = 0;
		for (;;)
		{
			CHAR ReadData = 0;
			SIZE_T Result = 0;
			FILE_READ_RESULT ReadCheck = Read(&ReadData, 1, &Result);

			if (ReadCheck == FILE_READ_RESULT::FAIL || ReadCheck == FILE_READ_RESULT::END)
				break;

			if (ReadData == -1 || ReadData == ' ' || ReadData == '\n' || ReadData == '\r' || ReadData == '\t')
			{
				if (ReadData == -1 || Flag)
					break;
			}
			else
			{
				Flag = TRUE;
				pDest[Cnt++] = ReadData;
				if (DestSize != -1 && DestSize <= Cnt)
					break;
			}
		}
		return Cnt;
	}
	return 0;
}
CONST CHAR* EPN_FileStream :: GetFilePath()
{
	return m_FilePath;
}
CONST CHAR* EPN_FileStream :: GetFileName()
{
	return m_FileName;
}
CONST CHAR* EPN_FileStream :: GetFileExtension()
{
	return m_FileExtension;
}
BOOL EPN_FileStream ::IsReadEnd()
{
	if (m_pFile == NULL_PTR)
		return TRUE;

	return feof(m_pFile) != 0;
}
FILE* EPN_FileStream :: GetFilePtr()
{
	return m_pFile;
}
BOOL EPN_FileStream :: SetFileEPN_Pos(INT32 EPN_Pos, INT32 Flag)
{
	if (!m_pFile)
		return FALSE;
	fseek(m_pFile, EPN_Pos, Flag);

	return TRUE;
}
SIZE_T EPN_FileStream :: GetFileEPN_Pos()
{
	if (!m_pFile)
		return 0;
	return ftell(m_pFile);
}

SIZE_T EPN_FileStream :: GetFileSize()
{
	if (!m_pFile)
		return 0;
	SIZE_T FileEPN_Pos = 0;
	SIZE_T FileSize = 0;
	FileEPN_Pos = GetFileEPN_Pos();
	SetFileEPN_Pos(0, SEEK_END);
	FileSize = GetFileEPN_Pos();
	SetFileEPN_Pos((INT32)FileEPN_Pos, SEEK_SET);

	return FileSize;
}

