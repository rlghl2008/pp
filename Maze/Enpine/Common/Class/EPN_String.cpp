#include "EPN_String.h"

#include "../Base/CommonFunction.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion

BOOL EPN :: COMMON :: EPN_String :: m_UTF8_FLAG = FALSE;
EPN_MemoryPool<EPN_StringData>  EPN::COMMON::EPN_String ::m_sDataPool;
EPN_MemoryPool<INT32>  EPN::COMMON::EPN_String::m_sFreeIndexPool;

VOID EPN_String ::Init()
{
	m_RefFlag = FALSE;
	if (m_sDataPool.GetAddDataCount() == 0)
	{
		m_sDataPool.SetOverAllocateFlag(TRUE);
		m_sFreeIndexPool.SetOverAllocateFlag(TRUE);
	}

	if (m_sFreeIndexPool.GetAddDataCount() == 0)
	{
		m_ProcSize = ESTR_LEN_DEFAULT;

		m_CurIndex = m_sDataPool.AddData(EPN_StringData());
		m_sDataPool[m_CurIndex]->pData = new EPN_CHAR[m_ProcSize];
		m_sDataPool[m_CurIndex]->AllocateSize = m_ProcSize;

		m_BufferIndex = m_sDataPool.AddData(EPN_StringData());
		m_sDataPool[m_BufferIndex]->pData = new EPN_CHAR[m_ProcSize];
		m_sDataPool[m_BufferIndex]->AllocateSize = m_ProcSize;
	}
	else
	{
		for (INT32 Index = 0;;Index++)
		{
			if (m_sFreeIndexPool.IsData(Index) == TRUE)
			{
				m_CurIndex = m_sFreeIndexPool.GetData(Index);
				m_BufferIndex = m_CurIndex + 1;
				m_sFreeIndexPool.EraseData(Index);

				m_ProcSize = m_sDataPool[m_CurIndex]->AllocateSize;
				break;
			}
		}
	}
	m_pData = m_sDataPool[m_CurIndex]->pData;
	m_pBuffer = m_sDataPool[m_BufferIndex]->pData;
	m_pData[0] = 0;
	m_pBuffer[0] = 0;

}

BOOL EPN_String::OverAllocate(INT32 Size)
{
	if (m_ProcSize <= Size)
	{
		memcpy(m_sDataPool[m_BufferIndex]->pData, m_sDataPool[m_CurIndex]->pData, m_ProcSize);

		delete[] m_sDataPool[m_CurIndex]->pData;
		m_sDataPool[m_CurIndex]->pData = NULL;
		INT32 ProcSize = ((Size / ESTR_LEN_DEFAULT) + 1) * ESTR_LEN_DEFAULT;
		m_sDataPool[m_CurIndex]->pData = new EPN_CHAR[ProcSize];
		m_sDataPool[m_CurIndex]->AllocateSize = ProcSize;

		memcpy(m_sDataPool[m_CurIndex]->pData, m_sDataPool[m_BufferIndex]->pData, m_ProcSize);

		delete[] m_sDataPool[m_BufferIndex]->pData;
		m_sDataPool[m_BufferIndex]->pData = NULL;
		m_sDataPool[m_BufferIndex]->pData = new EPN_CHAR[ProcSize];
		m_sDataPool[m_BufferIndex]->AllocateSize = ProcSize;

		m_ProcSize = ProcSize;

		m_pData = m_sDataPool[m_CurIndex]->pData;
		m_pBuffer = m_sDataPool[m_BufferIndex]->pData;
		return TRUE;
	}
	return FALSE;
}

EPN :: COMMON :: EPN_String :: EPN_String()
{
	Init();
}
EPN::COMMON::EPN_String::EPN_String(CONST EPN_StringRef Ref)
{
	if(Ref.m_CurIndex < 0 || Ref.m_BufferIndex < 0 || Ref.m_ProcSize < 0)
		Init();
	else
	{
		m_CurIndex = Ref.m_CurIndex;
		m_BufferIndex = Ref.m_BufferIndex;
		m_ProcSize = Ref.m_ProcSize;
	}
}
EPN::COMMON::EPN_String :: EPN_String(CONST INT32 Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}

EPN_String :: EPN_String(CONST REAL32 Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String::EPN_String(CONST REAL64 Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}

EPN_String :: EPN_String(CONST CHAR Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String :: EPN_String(CONST CHAR* Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
	
}
EPN_String :: EPN_String(CONST std::string& Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String :: EPN_String(CONST WCHAR Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String :: EPN_String(CONST WCHAR* Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String :: EPN_String(CONST std::wstring& Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}
EPN_String :: EPN_String(CONST EPN_String& Data)
{
	Init();
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, Len);
	m_pData[Len] = 0;
}

EPN_String :: ~EPN_String()
{
	if(m_RefFlag == FALSE)
		m_sFreeIndexPool.AddData(m_CurIndex);
}
EPN_CHAR* EPN_String :: EPN_TEXT(CONST INT32 Data)
{
	if(EPN_UNICODE_FLAG)
	{	
		_itow_s(Data, (WCHAR*)m_pBuffer, m_ProcSize, 10);
	}
	else
	{
		_itoa_s(Data, (CHAR*)m_pBuffer, m_ProcSize, 10);
	}

	return m_pBuffer;
}

EPN_CHAR* EPN_String :: EPN_TEXT(CONST REAL32 Data)
{
	if (EPN_UNICODE_FLAG)
	{
		swprintf_s((WCHAR*)m_pBuffer, m_ProcSize, L"%f", Data);
	}
	else
	{
		sprintf_s((CHAR*)m_pBuffer, m_ProcSize, "%f", Data);
	}

	return m_pBuffer;
}


EPN_CHAR* EPN_String::EPN_TEXT(CONST REAL64 Data)
{
	if (EPN_UNICODE_FLAG)
	{
		swprintf_s((WCHAR*)m_pBuffer, m_ProcSize, L"%f", Data);
	}
	else
	{
		sprintf_s((CHAR*)m_pBuffer, m_ProcSize, "%f", Data);
	}

	return m_pBuffer;
}

EPN_CHAR* EPN_String :: EPN_TEXT(CONST CHAR Data)
{
	if(EPN_UNICODE_FLAG)
	{
		MultiByteToWideChar(CP_ACP, NULL, &Data,  -1, (WCHAR*)m_pBuffer, m_ProcSize -1);
	}
	else
	{
		m_pBuffer[0] = (EPN_CHAR)Data;
		m_pBuffer[1] = 0;
	}
	return m_pBuffer;
}

EPN_CHAR* EPN_String :: EPN_TEXT(CONST CHAR* Data)
{
		
	if (EPN_UNICODE_FLAG)
	{
		INT32 Len = (INT32)strlen(Data);
		OverAllocate(Len);

		MultiByteToWideChar(CP_ACP, NULL, Data, -1, (WCHAR*)m_pBuffer, Len);
	}
	else
	{
		INT32 Len = (INT32)strlen(Data);
		OverAllocate(Len);
		memcpy(m_pBuffer, Data, Len);
		m_pBuffer[Len] = 0;
	}

	return m_pBuffer;
}

EPN_CHAR* EPN_String :: EPN_TEXT(CONST std::string& Data)
{
	if(EPN_UNICODE_FLAG)
	{
		CHAR* pSrc = (CHAR*)Data.c_str();
		INT32 Len = (INT32)strlen(pSrc);
		OverAllocate(Len);
		MultiByteToWideChar(CP_ACP, NULL, pSrc, -1, (WCHAR*)m_pBuffer, Len);
	}
	else
	{
		CHAR* pSrc = (CHAR*)Data.c_str();
		INT32 Len = (INT32)strlen(pSrc);
		OverAllocate(Len);
		memcpy(m_pBuffer, pSrc, Len);
		m_pBuffer[Len] = 0;
	}
	return m_pBuffer;
}
EPN_CHAR* EPN_String :: EPN_TEXT(CONST WCHAR Data)
{
	if(EPN_UNICODE_FLAG)
	{
		m_pBuffer[0] = (EPN_CHAR)Data;
		m_pBuffer[1] = 0;
	}
	else
	{
		WideCharToMultiByte(CP_ACP, NULL, &Data, -1,(CHAR*)m_pBuffer,  m_ProcSize-1, NULL, FALSE);
	}
	return m_pBuffer;

}
EPN_CHAR* EPN_String :: EPN_TEXT(CONST WCHAR* Data)
{
	if(EPN_UNICODE_FLAG)
	{
		INT32 Len = (INT32)wcslen(Data);
		OverAllocate(Len);
		memcpy(m_pBuffer, Data, Len);
		m_pBuffer[Len] = 0;
	}
	else
	{
		INT32 Len = (INT32)wcslen(Data) * 2;
		OverAllocate(Len);
		WideCharToMultiByte(CP_ACP, NULL, Data, -1, (CHAR*)m_pBuffer, Len, NULL, FALSE);
	}
	return m_pBuffer;
}
EPN_CHAR* EPN_String :: EPN_TEXT(CONST std::wstring& Data)
{
	if(EPN_UNICODE_FLAG)
	{
		WCHAR* pSrc = (WCHAR*)Data.c_str();
		INT32 Len = (INT32)wcslen(pSrc);
		OverAllocate(Len);
		memcpy(m_pBuffer, pSrc, Len);
		m_pBuffer[Len] = 0;
	}
	else
	{
		WCHAR* pSrc = (WCHAR*)Data.c_str();
		INT32 Len = (INT32)wcslen(pSrc) * 2;
		OverAllocate(Len);
		WideCharToMultiByte(CP_ACP, NULL, pSrc, -1, (CHAR*)m_pBuffer, Len, NULL, FALSE);
	}

	return m_pBuffer;
}
EPN_CHAR* EPN_String :: EPN_TEXT(CONST EPN_String& Data)
{
	INT32 Len = Data.Length();
	OverAllocate(Len);
	memcpy(m_pBuffer, Data.GetStr(), Len);
	m_pBuffer[Len] = 0;
	return m_pBuffer;
}

CONST EPN_CHAR* EPN_String::GetStr() CONST
{
	return m_pData;
}
CONST EPN_CHAR* EPN_String :: GetBuffer() CONST
{
	return m_pBuffer;
}

CONST WCHAR* EPN_String::GetStrW()
{
	if (EPN_UNICODE_FLAG == FALSE)
	{
		INT32 Len = (INT32)strlen(m_pData) * 2;
		OverAllocate(Len);

		MultiByteToWideChar(CP_ACP, NULL, m_pData, -1, (WCHAR*)m_pBuffer, Len);
		return  (WCHAR*)m_pBuffer;
	}
	return  (WCHAR*)m_pData;
}
CONST CHAR* EPN_String::GetStrA()
{
	if (EPN_UNICODE_FLAG == TRUE)
	{
		INT32 Len = (INT32)strlen(m_pData);
		OverAllocate(Len);

		WideCharToMultiByte(CP_ACP, NULL, (WCHAR*)m_pData, -1, (CHAR*)m_pBuffer, Len, NULL, FALSE);
		return (CHAR*)m_pBuffer;
	}

	return (CHAR*)m_pData;
}

CONST INT32 EPN_String :: GetValue()
{
	return atoi(GetStrA());
}
//CONST LONG32 EPN_String :: GetLValue()
//{
//	return atol(GetStr());
//}
CONST REAL32 EPN_String :: GetFloat()
{
	return (REAL32)atof(GetStrA());
}
CONST EPN_StringRef EPN_String :: GetRef()
{
	EPN_StringRef Ref;
	Ref.m_CurIndex = m_CurIndex;
	Ref.m_BufferIndex = m_BufferIndex;
	Ref.m_ProcSize = m_ProcSize;
	m_RefFlag = TRUE;
	return Ref;
}
UINT32 EPN_String ::Length() CONST
{
	return estrlen(m_pData);
}

EPN_CHAR EPN_String :: at(INT32 len)
{
	if (len < 0 || len >= m_ProcSize)
		return NULL;
	return m_pData[len];
}
CONST EPN_CHAR* EPN_String :: GetMid(INT32 StartIndex, INT32 EndIndex)
{
	if (StartIndex < 0 || StartIndex >= m_ProcSize)
		return NULL_PTR;
	if (EndIndex < 0 || EndIndex >= m_ProcSize)
		return NULL_PTR;
	if (StartIndex <= EndIndex)
	{
		estrcpy_s(m_pBuffer, EndIndex - StartIndex, m_pData + StartIndex);
		m_pBuffer[EndIndex - StartIndex] = 0;
	}
	else
	{
		INT32 Cnt = 0;
		for (INT32 i = StartIndex; i >= EndIndex; --i)
		{
			m_pBuffer[Cnt] = m_pData[i];
			Cnt++;
		}
		m_pBuffer[Cnt] = 0;
	}
	

	return m_pBuffer;
}
EPN_Strlen EPN_String :: SearchText(EPN_String Text)
{
	/*if(Text.Length() > m_EPN_Str.Length())
		return EPN_Strlen();

	for(UINT32 i = 0;i < m_EPN_Str.Length();++i)
	{
		if(m_EPN_Str.Length()-i < Text.Length())
			break;
		INT32 Count = 0;
		for(UINT32 j = 0;j < Text.Length();++j)
		{
			if(Text.at(j) == m_EPN_Str.at(i+j))
			{
				++Count;
				if(Text.Length() == Count)
				{
					return EPN_Strlen(i,i+j,Text.Length());
				}
			}
			else
				break;
		}
	}*/

	return EPN_Strlen();
}
BOOL EPN_String :: estrcat(EPN_String estr,INT32 length)
{
	if (length < -1)
		return FALSE;

	UINT32 MyLength = Length();
	if(length == -1)
	{	
		length = estr.Length();
	}
	OverAllocate(MyLength + length);
	memcpy(&(m_pData[MyLength]), estr.GetStr(), length);
	m_pData[MyLength + length] = 0;
	return TRUE;
}
BOOL EPN_String :: Clear()
{
	memset(m_pData,0, m_ProcSize);
	memset(m_pBuffer, 0, m_ProcSize);
	return TRUE;
}
VOID EPN_String :: PrintConsole() CONST
{
	if (EPN_UNICODE_FLAG)
	{
		if (!m_UTF8_FLAG)
		{
			std::wcin.imbue(std::locale("korean"));
			std::wcout.imbue(std::locale("korean"));
			m_UTF8_FLAG = TRUE;
		}
		std::wcout << (EPN_CHAR*)m_pData;
	}
	else
	{
		std::cout << (EPN_CHAR*)m_pData;
	}
}
EPN_String EPN_String :: operator =(CONST INT32 Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
//EPN_String EPN_String :: operator =(CONST LONG32 val)
//{
//	m_EPN_Str = EPN_TEXT(val);
//	return EPN_String(EPN_TEXT(m_EPN_Str));
//}
EPN_String EPN_String :: operator =(CONST REAL32 Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST REAL64 Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST CHAR Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST CHAR* Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST std::string& Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST WCHAR Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST WCHAR* Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST std::wstring& Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator =(CONST EPN_String& Data)
{
	EPN_CHAR* pBuffer = EPN_TEXT(Data);
	INT32 Len = estrlen(pBuffer);
	memcpy(m_pData, pBuffer, estrlen(pBuffer));
	m_pData[Len] = 0;
	return EPN_String(pBuffer);
}
EPN_String EPN_String :: operator +(CONST INT32 Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
//EPN_String EPN_String :: operator +(CONST LONG32 val)
//{
//	EPN_STRING TempStr = m_EPN_Str;
//	TempStr += EPN_TEXT(val);
//	return EPN_String(EPN_TEXT(TempStr));
//}
EPN_String EPN_String :: operator +(CONST REAL32 Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST REAL64 Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST CHAR Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST CHAR* Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST std::string& Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST WCHAR Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST WCHAR* Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST std::wstring& Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}
EPN_String EPN_String :: operator +(CONST EPN_String& Data)
{
	EPN_String Result = m_pData;
	Result.estrcat(EPN_TEXT(Data));
	return Result;
}

EPN_String EPN_String :: operator +=(CONST INT32 Data)
{
	estrcat(Data);
	return GetStr();
}
//EPN_String EPN_String :: operator +=(CONST LONG32 val)
//{
//	m_EPN_Str += EPN_TEXT(val);
//	return EPN_String(EPN_TEXT(m_EPN_Str));
//}
EPN_String EPN_String :: operator +=(CONST REAL32 Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST REAL64 Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST CHAR Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST CHAR* Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST std::string& Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST WCHAR Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST WCHAR* Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST std::wstring& Data)
{
	estrcat(Data);
	return GetStr();
}
EPN_String EPN_String :: operator +=(CONST EPN_String& Data)
{
	estrcat(Data);
	return GetStr();
}
BOOL EPN_String :: operator ==(CONST INT32 Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
/*
BOOL EPN_String :: operator ==(CONST LONG32 val)
{
	EPN_STRING TempStr = EPN_TEXT(val);
	if(EPN_UNICODE_FLAG)
	{
		return (!wcscmp((CONST WCHAR*)TempStr.GetStr(),(CONST WCHAR*)m_EPN_Str.GetStr()));
	}
	else
	{
		return (!strcmp((CONST CHAR*)TempStr.GetStr(),(CONST CHAR*)m_EPN_Str.GetStr()));
	}
	return FALSE;
}*/
BOOL EPN_String :: operator ==(CONST REAL32 Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST REAL64 Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST CHAR Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST CHAR* Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST std::string& Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST WCHAR Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST WCHAR* Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST std::wstring& Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator ==(CONST EPN_String& Data)
{
	return !estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST INT32 Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
//BOOL EPN_String :: operator !=(CONST LONG32 val)
//{
//	EPN_STRING TempStr = EPN_TEXT(val);
//	if(EPN_UNICODE_FLAG)
//	{
//		return (wcscmp((CONST WCHAR*)TempStr.GetStr(),(CONST WCHAR*)m_EPN_Str.GetStr()));
//	}
//	else
//	{
//		return (strcmp((CONST CHAR*)TempStr.GetStr(),(CONST CHAR*)m_EPN_Str.GetStr()));
//	}
//	return TRUE;
//}
BOOL EPN_String :: operator !=(CONST REAL32 Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST REAL64 Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST CHAR Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST CHAR* Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST std::string& Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST WCHAR Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST WCHAR* Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST std::wstring& Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}
BOOL EPN_String :: operator !=(CONST EPN_String& Data)
{
	return estrcmp(GetStr(), EPN_TEXT(Data));
}

