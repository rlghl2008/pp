#pragma once

#include "../Base/CommonBase.h"
#include "EPN_String.h"
#include "EPN_MemoryPool.hpp"

#define EPN_UNICODE_FLAG FALSE
typedef CHAR EPN_CHAR;
#define estrcpy strcpy
#define estrcpy_s strcpy_s
#define estrcmp strcmp
#define estrlen strlen

namespace EPN
{
	namespace COMMON
	{
		typedef struct _EPN_Strlen
		{
			INT32 length;
			INT32 StartIndex;
			INT32 EndIndex;
			_EPN_Strlen()
			{
				length = 0;
				StartIndex = 0;
				EndIndex = 0;
			}
			_EPN_Strlen(INT32 StartIndex, INT32 EndIndex, INT32 length)
			{
				this->length = length;
				this->StartIndex = StartIndex;
				this->EndIndex = EndIndex;
			}
		} EPN_Strlen;
		struct EPN_StringData
		{
			EPN_CHAR* pData;
			INT32	AllocateSize;
			EPN_StringData()
			{
				pData = NULL_PTR;
				AllocateSize = 0;
			}
			~EPN_StringData()
			{
				if (pData != NULL_PTR)
					delete[] pData;
			}
		};
		struct EPN_StringRef
		{
			INT32	m_CurIndex;
			INT32	m_BufferIndex;
			INT32	m_ProcSize;
			EPN_StringRef()
			{
				m_CurIndex = -1;
				m_BufferIndex = -1;
				m_ProcSize = -1;
			}
		};
		class EPN_String
		{
		private:
			EPN_CHAR* m_pData;//실제 데이터
			EPN_CHAR* m_pBuffer;//데이터 처리용 버퍼
			INT32	m_CurIndex;
			INT32	m_BufferIndex;
			INT32	m_ProcSize; //처리 기준 크기 ESTR_LEN_DEFAULT 부터 시작
			BOOL	m_RefFlag;
		private:
			static EPN_MemoryPool<EPN_StringData> m_sDataPool;
			static EPN_MemoryPool<INT32> m_sFreeIndexPool;
		private:
			enum { ESTR_LEN_DEFAULT = 512 };//문자열 받을때의 나눠서받는기준
			static BOOL m_UTF8_FLAG;
		private:
			VOID Init();
			BOOL OverAllocate(INT32 Size);
		public:
			EPN_String();
			EPN_String(CONST EPN_StringRef Ref);
			EPN_String(CONST INT32 Data);
			//EPN_String(CONST long val);
			EPN_String(CONST REAL32 Data);
			EPN_String(CONST REAL64 Data);
			EPN_String(CONST CHAR Data);
			EPN_String(CONST CHAR* Data);
			EPN_String(CONST std::string& Data);
			EPN_String(CONST WCHAR Data);
			EPN_String(CONST WCHAR* Data);
			EPN_String(CONST std::wstring& Data);
			EPN_String(CONST EPN_String& Data);
			~EPN_String();
		public:
			EPN_CHAR* EPN_TEXT(CONST INT32 Data);
			//EPN_STRING EPN_TEXT(CONST LONG32 val);
			EPN_CHAR* EPN_TEXT(CONST REAL32 Data);
			EPN_CHAR* EPN_TEXT(CONST REAL64 Data);
			EPN_CHAR* EPN_TEXT(CONST CHAR Data);
			EPN_CHAR* EPN_TEXT(CONST CHAR* Data);
			EPN_CHAR* EPN_TEXT(CONST std::string& Data);
			EPN_CHAR* EPN_TEXT(CONST WCHAR Data);
			EPN_CHAR* EPN_TEXT(CONST WCHAR* Data);
			EPN_CHAR* EPN_TEXT(CONST std::wstring& Data);
			EPN_CHAR* EPN_TEXT(CONST EPN_String& Data);
		public:
			CONST EPN_CHAR* GetStr() CONST;//컴파일러 설정이 유니코드면 wchar* 멀티바이트면 char* 으로 반환됨
			CONST EPN_CHAR* GetBuffer() CONST;//임시 처리버퍼 내용 얻기 ( 내용이 수시로 바뀔수 있습니다. )

			CONST WCHAR* GetStrW();//무조건 WCHAR* 형 얻기 ( 복사해서 사용해야 합니다. )
			CONST CHAR* GetStrA();//무조건 CHAR* 형 얻기 ( 복사해서 사용해야 합니다. )
			CONST INT32 GetValue();//무조건 정수형 얻기
			//CONST LONG32 GetLValue();//무조건 롱형 얻기
			CONST REAL32 GetFloat();//무조건 실수형 얻기

			CONST EPN_StringRef GetRef();//Ref 용 참조 데이터 얻기


			EPN_CHAR at(INT32 len);//컴파일러 설정이 유니코드면 WCHAR 멀티바이트면 CHAR 으로 반환됨
			CONST EPN_CHAR* GetMid(INT32 StartIndex, INT32 EndIndex);//시작과 끝 위치 사이의 문자열을 반환
			EPN_Strlen SearchText(EPN_String Text);//문자열을 찾아서 길이와 위치 반환
			BOOL estrcat(EPN_String estr, INT32 length = -1);//현재 문자열중 length위치에 estr내용 이어붙이기

			UINT32 Length() CONST;
			BOOL Clear();
			VOID PrintConsole() CONST;
		public:
			EPN_String operator =(CONST INT32 Data);
			//EPN_String operator =(CONST long val);
			EPN_String operator =(CONST REAL32 Data);
			EPN_String operator =(CONST REAL64 Data);
			EPN_String operator =(CONST CHAR Data);
			EPN_String operator =(CONST CHAR* Data);
			EPN_String operator =(CONST std::string& Data);
			EPN_String operator =(CONST WCHAR Data);
			EPN_String operator =(CONST WCHAR* Data);
			EPN_String operator =(CONST std::wstring& Data);
			EPN_String operator =(CONST EPN_String& Data);
			EPN_String operator +(CONST INT32 Data);
			//EPN_String operator +(CONST long val);
			EPN_String operator +(CONST REAL32 Data);
			EPN_String operator +(CONST REAL64 Data);
			EPN_String operator +(CONST CHAR Data);
			EPN_String operator +(CONST CHAR* Data);
			EPN_String operator +(CONST std::string& Data);
			EPN_String operator +(CONST WCHAR Data);
			EPN_String operator +(CONST WCHAR* Data);
			EPN_String operator +(CONST std::wstring& Data);
			EPN_String operator +(CONST EPN_String& Data);
			EPN_String operator +=(CONST INT32 Data);
			//EPN_String operator +=(CONST long val);
			EPN_String operator +=(CONST REAL32 Data);
			EPN_String operator +=(CONST REAL64 Data);
			EPN_String operator +=(CONST CHAR Data);
			EPN_String operator +=(CONST CHAR* Data);
			EPN_String operator +=(CONST std::string& Data);
			EPN_String operator +=(CONST WCHAR Data);
			EPN_String operator +=(CONST WCHAR* Data);
			EPN_String operator +=(CONST std::wstring& Data);
			EPN_String operator +=(CONST EPN_String& Data);
			BOOL operator ==(CONST INT32 Data);
			//BOOL operator ==(CONST long val);
			BOOL operator ==(CONST REAL32 Data);
			BOOL operator ==(CONST REAL64 Data);
			BOOL operator ==(CONST CHAR Data);
			BOOL operator ==(CONST CHAR* Data);
			BOOL operator ==(CONST std::string& Data);
			BOOL operator ==(CONST WCHAR Data);
			BOOL operator ==(CONST WCHAR* Data);
			BOOL operator ==(CONST std::wstring& Data);
			BOOL operator ==(CONST EPN_String& Data);
			BOOL operator !=(CONST INT32 Data);
			//BOOL operator !=(CONST long val);
			BOOL operator !=(CONST REAL32 Data);
			BOOL operator !=(CONST REAL64 Data);
			BOOL operator !=(CONST CHAR Data);
			BOOL operator !=(CONST CHAR* Data);
			BOOL operator !=(CONST std::string& Data);
			BOOL operator !=(CONST WCHAR Data);
			BOOL operator !=(CONST WCHAR* Data);
			BOOL operator !=(CONST std::wstring& Data);
			BOOL operator !=(CONST EPN_String& Data);
		private:
			friend std::ostream& operator <<(std::ostream &c, CONST EPN_String& estr)
			{
				estr.PrintConsole();
				return c;
			}
			friend std::ostream& operator <<(std::ostream &c, CONST EPN_String* pestr)
			{
				if (pestr)
					c << *pestr;
				else
					c << "estring(NULL)";
				return c;
			}


		};

		#define E_TEXT(estr) (estring)estr
	}
}

typedef EPN::COMMON::EPN_String estring;




