#include "EPN_ThreadSafeManager.h"
#include "EPN_CriticalSection.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion

EPN_ThreadSafeManager* EPN_ThreadSafeManager ::m_pThreadSafeManager = NULL_PTR;

EPN_ThreadSafeManager* EPN_ThreadSafeManager :: GetInstance()
{
	return m_pThreadSafeManager;
}
VOID EPN_ThreadSafeManager :: ReleaseInstance()
{
	if(m_pThreadSafeManager != NULL_PTR)
		delete m_pThreadSafeManager;
	m_pThreadSafeManager = NULL_PTR;
}
EPN_ThreadSafeManager* EPN_ThreadSafeManager :: CreateInstance(VOID)
{
	if(!m_pThreadSafeManager)
	{
		m_pThreadSafeManager = new EPN_ThreadSafeManager;
	}
	return m_pThreadSafeManager;
}

EPN_ThreadSafeManager :: EPN_ThreadSafeManager()
{

}

EPN_ThreadSafeManager :: ~EPN_ThreadSafeManager()
{


}

INT32 EPN_ThreadSafeManager :: CreateThreadSafe(EPN::COMMON::THREAD_SAFE_FLAG Flag)
{
	INT32 ResultIndex = 0;
	switch(Flag)
	{
		case THREAD_SAFE_FLAG::CRITICAL:
			{
				ResultIndex = m_mpThreadSafe.AddData(new EPN_CriticalSection());
				break;
			}
		default:
			{


			}
	}
	return ResultIndex;
}

BOOL EPN_ThreadSafeManager :: DeleteThreadSafe(INT32 Index)
{
	EPN_ThreadSafe* pData = m_mpThreadSafe.GetData(Index);
	if(!pData)
	{
		return FALSE;
	}
	delete pData;
	m_mpThreadSafe.EraseData(Index);
	return TRUE;
}

EPN_ThreadSafe* EPN_ThreadSafeManager :: GetThreadSafe(INT32 Index)
{
	return m_mpThreadSafe.GetData(Index);
}

BOOL EPN_ThreadSafeManager :: LockArea(INT32 Index)
{
	EPN_ThreadSafe* pData = m_mpThreadSafe.GetData(Index);
	if(pData == NULL_PTR)
	{
		return FALSE;
	}
	pData->LockArea();

	return TRUE;
}
BOOL EPN_ThreadSafeManager :: UnLockArea(INT32 Index)
{
	EPN_ThreadSafe* pData = m_mpThreadSafe.GetData(Index);
	if(!pData)
	{
		return FALSE;
	}
	pData->UnLockArea();

	return TRUE;
}