#include "EPN_CriticalSection.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion

using namespace EPN::COMMON;

EPN_CriticalSection :: EPN_CriticalSection()
{
	SetSafeFlag(THREAD_SAFE_FLAG::CRITICAL);
	if (InitializeCriticalSectionAndSpinCount(&m_CS, GetSpinCount()) == FALSE)
	{
		// LOG
	}
}

EPN_CriticalSection :: ~EPN_CriticalSection()
{
	DeleteCriticalSection(&m_CS);
}

VOID EPN_CriticalSection :: LockArea()
{
	EnterCriticalSection(&m_CS);
}
VOID EPN_CriticalSection :: UnLockArea()
{
	LeaveCriticalSection(&m_CS);
}