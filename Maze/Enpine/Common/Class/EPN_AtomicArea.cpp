#include "EPN_AtomicArea.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion



EPN_AtomicArea :: EPN_AtomicArea()
{
	SetSafeFlag(THREAD_SAFE_FLAG::ATOMIC);
}

EPN_AtomicArea :: ~EPN_AtomicArea()
{
	
}

VOID EPN_AtomicArea :: LockArea()
{
	// Sleep(1) CPU 자원 사용을 줄이고 SpinCount 횟수를 감소 시킴으로 
	// Thread Safe 기능을 유지시키면서 속도를 향상시킬수 있음
	// 추가 검증 필요

	//while (m_AtomicFlag++ != 0) { for ( int Cnt = 0; Cnt < m_AtomicFlag.load(); ++Cnt) { Sleep(1); } }

	while (m_AtomicFlag++ != 0)
	{
		INT32 SpinCnt = m_AtomicFlag.load();
		if (SpinCnt > 1000)
		{
			m_AtomicFlag = 1;
			SpinCnt = 1;
		}
		for (INT32 Cnt = 0; Cnt < SpinCnt; ++Cnt) { Sleep(1); }
	}
}
VOID EPN_AtomicArea :: UnLockArea()
{
	m_AtomicFlag = 0;
}