#pragma once 

#include "EPN_ThreadSafe.h"
#include <atomic>

namespace EPN
{
	namespace COMMON
	{
		class EPN_AtomicArea : public EPN_ThreadSafe
		{
		private:
			std::atomic<INT32> m_AtomicFlag;
		public:
			EPN_AtomicArea();
			virtual ~EPN_AtomicArea();
		public:
			virtual VOID LockArea();
			virtual VOID UnLockArea();


		};
	}
}
