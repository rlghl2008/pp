#pragma once 

#include "../Class/EPN_LogManager.h"
#define DEFAULT_MEMORY_POOL_SIZE 32

namespace EPN
{
	namespace COMMON
	{
		enum class MEMORY_STATE : BYTE
		{
			NON = 0,
			FREE,
			WORK
		};

		template< typename T >
		class EPN_MemoryPool
		{
		private:
			struct DataInfo
			{
				T Data;
				MEMORY_STATE StateFlag;
				DataInfo() : StateFlag(MEMORY_STATE :: FREE){}
			};

			struct MemoryPoolNode
			{
				DataInfo* pDataInfo;
				MemoryPoolNode* pNodeLink;
				MemoryPoolNode():pDataInfo(0),pNodeLink(0){}
				~MemoryPoolNode()
				{
					if(pDataInfo != NULL_PTR)
					{
						delete [] pDataInfo;
						pDataInfo = 0;
					}
				}
			};
		private:
			//함수 실패시 반환되는 (T Type 에 맞춘)0 값
			T	m_FailData;

			MemoryPoolNode*	m_pHead;
			MemoryPoolNode*	m_pTail;

			INT32 m_MemoryPoolSize;
			BOOL m_OverAllocateFlag;

			INT32	m_MemoryPoolCount;	// 현재 생성되어 있는 메모리풀 개수
			INT32	m_MemoryDataCount;	// 현재 생성되어 있는 메모리 데이터 개수
			INT32	m_AddDataCount;		// 현재 입력되어 있는 실제 데이터 개수

			INT32 m_AddIndex; //다음 Add 되어야할 Index 값
			INT32 m_AllocateCount; //Memory Pool Node 가 생성된 카운트

		public:
			EPN_MemoryPool();
			virtual ~EPN_MemoryPool();

		private:
			VOID* GetDataInfo(INT32 Index) CONST;
		public:
			VOID SetMemoryPoolSize(INT32 Size)	{m_MemoryPoolSize = Size;}
			INT32  GetMemoryPoolSize() CONST {return m_MemoryPoolSize;}
			VOID SetOverAllocateFlag(BOOL Flag)	{ m_OverAllocateFlag = Flag;}
			BOOL GetOverAllocateFlag() CONST {return m_OverAllocateFlag;}

			INT32	GetMemoryPoolCount(VOID) CONST {return m_MemoryPoolCount;}
			INT32	GetMemoryDataCount(VOID) CONST	{return m_MemoryDataCount;}
			INT32	GetAddDataCount(VOID) CONST {return m_AddDataCount;}

			//실패시 -1 반환
			INT32		AddData(T Data);
			BOOL	EraseData(INT32 Index, BOOL PullFlag = FALSE);
			T		GetData(INT32 Index) CONST;
			T*		GetDataPtr(INT32 Index) CONST;
			BOOL	IsData(INT32 Index) CONST;
			MEMORY_STATE GetDataState(INT32 Index) CONST;

			VOID	PullData();
		public:
			T* operator [](INT32 Index) 
			{
				return GetDataPtr(Index);
			}
		};


		template< typename T >
		EPN_MemoryPool< T > :: EPN_MemoryPool() : m_MemoryPoolSize(DEFAULT_MEMORY_POOL_SIZE),
			m_pHead(NULL_PTR), m_pTail(NULL_PTR),
			m_MemoryPoolCount(0), m_MemoryDataCount(0), m_AddDataCount(0), m_AddIndex(0), m_AllocateCount(0),
			m_OverAllocateFlag(FALSE)
		{
			memset(&m_FailData, 0, sizeof(m_FailData));

		}

		template< typename T >
		EPN_MemoryPool< T > :: ~EPN_MemoryPool()
		{
			MemoryPoolNode* pDeleteNode = m_pHead;

			while(pDeleteNode != 0)
			{
				MemoryPoolNode* pTempNode = pDeleteNode->pNodeLink;
				if( pDeleteNode->pDataInfo != NULL_PTR )
				{
					delete [] pDeleteNode->pDataInfo;
					pDeleteNode->pDataInfo = 0;
				}
				delete pDeleteNode;
				pDeleteNode = pTempNode;
			}
		}
		template< typename T >
		VOID EPN_MemoryPool< T > ::PullData()
		{
			for(INT32 Cnt = 0; Cnt < m_MemoryPoolCount * m_MemoryPoolSize;)
			{
				MEMORY_STATE State = GetDataState(Cnt);
				if(State == MEMORY_STATE :: FREE)
				{
					BOOL FindFlag = FALSE;
					DataInfo* pDeatInfo = (DataInfo*)GetDataInfo(Cnt);
					for(INT32 Cnt2 = Cnt+1; Cnt2 < m_MemoryPoolCount * m_MemoryPoolSize; Cnt2++)
					{
						DataInfo* pSrcInfo = (DataInfo*)GetDataInfo(Cnt2);
						if(pSrcInfo->StateFlag == MEMORY_STATE :: FREE)
						{
							continue;
						}
						FindFlag = TRUE;

						//클래스라면 깊은 복사가 있어야 제대로 동작할듯..
						pDeatInfo->Data = pSrcInfo->Data;
						//memcpy(&pDeatInfo->Data, &pSrcInfo->Data, sizeof(T));

						memset((BYTE*)&pSrcInfo->Data, 0, sizeof(T));
						pDeatInfo->StateFlag = pSrcInfo->StateFlag;
						pSrcInfo->StateFlag = MEMORY_STATE :: FREE;
						pDeatInfo = pSrcInfo;
					}
					if(FindFlag == FALSE)
						break;
				}
				else
				{
					++Cnt;
				}
			}
		}

		template< typename T >
		INT32 EPN_MemoryPool< T > :: AddData(T Data)
		{
			INT32 TotalSize = m_MemoryPoolCount * m_MemoryPoolSize;

			if(TotalSize <= m_AddIndex)
			{
				if (m_OverAllocateFlag == FALSE && m_AllocateCount > 0)
				{
					//LOG
					return -1;
				}
				if(m_pHead == NULL_PTR)
				{
					m_pHead = new MemoryPoolNode();
					m_pTail = m_pHead;
				}
				else
				{
					MemoryPoolNode*	m_pMPN = new MemoryPoolNode();
					m_pTail->pNodeLink = m_pMPN;
					m_pTail = m_pMPN;
				}
				m_pTail->pDataInfo = new DataInfo[m_MemoryPoolSize];

				m_MemoryPoolCount++;
				m_MemoryDataCount += m_MemoryPoolSize;
			}

			MemoryPoolNode* pLink = m_pHead;
			for(INT32 i = 0; i < (INT32)m_AddIndex / m_MemoryPoolSize; ++i)
			{
				pLink = pLink->pNodeLink;
			}

			pLink->pDataInfo[ m_AddIndex % m_MemoryPoolSize ].Data = Data;
			//memcpy(&pLink->pDataInfo[ m_AddIndex % m_MemoryPoolSize ].Data, &Data, sizeof(T));
			pLink->pDataInfo[ m_AddIndex % m_MemoryPoolSize ].StateFlag = MEMORY_STATE :: WORK;


			INT32 ResultIndex = m_AddIndex;
			for(INT32 Index = m_AddIndex + 1;;++Index)
			{
				if(IsData(Index) == FALSE)
				{
					m_AddIndex = Index;
					break;
				}
			}


			m_AddDataCount++;
			return ResultIndex;
		}

		template< typename T >
		BOOL EPN_MemoryPool< T > :: EraseData(INT32 Index, BOOL PullFlag)
		{
			if(Index < 0 || Index >= GetMemoryDataCount())
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: EraseData(%d) Error\n", Index);
				return FALSE;

			}
			INT32 TotalSize = m_MemoryPoolCount * m_MemoryPoolSize;
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;
			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			if(m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].StateFlag == MEMORY_STATE :: FREE)
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: EraseData(%d) == NULL\n", Index);
				return FALSE;
			}

			memset((BYTE*)&m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].Data, 0, sizeof(T));
			m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].StateFlag = MEMORY_STATE :: FREE;
			if(PullFlag == TRUE)
			{
				PullData();
				m_AddIndex = m_AddDataCount-1;
			}
			else
			{
				if(m_AddIndex > Index)
					m_AddIndex = Index;
			}
			m_AddDataCount--;
			return TRUE;
		}

		template< typename T >
		BOOL EPN_MemoryPool< T > :: IsData(INT32 Index) CONST
		{
			if(Index < 0 || Index >= GetMemoryDataCount())
			{
				return FALSE;
			}
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;

			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			if(m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].StateFlag == MEMORY_STATE :: FREE)
			{
				return FALSE;
			}

			return TRUE;
		}
		template< typename T >
		T EPN_MemoryPool< T > :: GetData(INT32 Index) CONST
		{
			if(Index < 0 || Index >= GetMemoryDataCount())
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: GetData(%d) Error\n", Index);
				return m_FailData;
			}
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;

			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			if(m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].StateFlag == MEMORY_STATE :: FREE)
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: GetData(%d) == NULL\n", Index);
				return m_FailData;
			}

			return m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].Data;
		}
		template< typename T >
		T* EPN_MemoryPool< T > :: GetDataPtr(INT32 Index) CONST
		{
			if(Index < 0 || Index >= GetMemoryDataCount())
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: GetDataPtr(%d) Error\n", Index);
				return NULL_PTR;
			}
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;

			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			return &m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].Data;
		}
		template< typename T >
		EPN::COMMON::MEMORY_STATE EPN_MemoryPool< T > :: GetDataState(INT32 Index) CONST
		{
			if(Index < 0 || Index >= GetMemoryDataCount())
			{
				EPN::COMMON::FUNCTION::PRINT_LOG("EPN_MemoryPool :: GetDataState(%d) Error\n", Index);
				return MEMORY_STATE::NON;
			}
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;

			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			return m_pMPN->pDataInfo[ Index % m_MemoryPoolSize ].StateFlag;
		}
		template< typename T >
		VOID* EPN_MemoryPool< T > :: GetDataInfo(INT32 Index) CONST
		{
			INT32 FindNodeCount = Index / m_MemoryPoolSize;
			MemoryPoolNode*	m_pMPN = m_pHead;

			for(INT32 Cnt = 0; Cnt < FindNodeCount; ++Cnt)
				m_pMPN = m_pMPN->pNodeLink;

			return (VOID*)&m_pMPN->pDataInfo[Index % m_MemoryPoolSize];
		}

	}
}

