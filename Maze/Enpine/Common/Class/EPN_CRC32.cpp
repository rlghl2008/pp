#include "EPN_CRC32.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion

//기존
//0xEDB88320
//수학자 들이 정해둔..
//0x04C11DB7

#define PUBLIC_CRC_KEY_0 0x000000B7
#define PUBLIC_CRC_KEY_1 0x00001D00
#define PUBLIC_CRC_KEY_2 0x00C10000
#define PUBLIC_CRC_KEY_3 0x04000000

EPN_CRC32::EPN_CRC32()
{
	MakeTableCRC(PUBLIC_CRC_KEY_0 | PUBLIC_CRC_KEY_1 | PUBLIC_CRC_KEY_2 | PUBLIC_CRC_KEY_3);
}

UINT32 EPN_CRC32::CalcCRC(CONST BYTE* pBuffer, UINT32 Size, UINT32 CRC, UINT32* pTable)
{
	CRC = ~CRC;

	while (Size--)
		CRC = pTable[(CRC ^ *(pBuffer++)) & 0xFF] ^ (CRC >> 8);

	return ~CRC;
}


VOID EPN_CRC32::MakeTableCRC(UINT32 ID)
{
	UINT32 Value = 0;

	for (UINT32 i = 0; i < 256; ++i)
	{
		Value = i;
		for (UINT32 j = 0; j < 8; ++j)
		{
			if (Value & 1)
			{
				Value = (Value >> 1) ^ ID;
			}
			else
			{
				Value >>= 1;
			}
		}
		m_TableCRC[i] = Value;
	}
}

UINT32 EPN_CRC32 :: GetFileCRC(FILE* pFile)
{
	UINT32 CRC = 0;
	UINT32 ReadLen;

	while ((ReadLen = fread(m_ProcBuffer, 1, sizeof(m_ProcBuffer), pFile)) != NULL)
		CRC = CalcCRC(m_ProcBuffer, ReadLen, CRC, m_TableCRC);

	return CRC;
}

UINT32 EPN_CRC32 :: GetMemoryCRC(BYTE* pMemory, UINT32 Size)
{
	UINT32 CRC = 0;
	CRC = CalcCRC(pMemory, Size, CRC, m_TableCRC);
	return CRC;
}
