#include "EPN_Timer.h"

#include "../Base/CommonFunction.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion

//로그는 이제 전부 컴언 펑션함수 쓰면됨 ㅇㅋ?
//각클래스가 로그 플래그 들고있을필요없음T이해안됨?

EPN_Timer::EPN_Timer()
{
	m_swFreq.LowPart = m_swFreq.HighPart = 0;
	m_swStart = m_swFreq;
	m_swTimer = m_swFreq;
	m_swEnd = m_swFreq;
	m_fTimeforDuration = 0;
	m_TimerStartFlag = FALSE;
	if(QueryPerformanceFrequency(&m_swFreq) == FALSE)
	{
		PRINT_LOG("CPU 클럭을 얻어 올 수 없습니다.\n");
	}
}

EPN_Timer::~EPN_Timer()
{

}

CONST REAL32 EPN_Timer :: GetTime()
{
	QueryPerformanceCounter(&m_swTimer);
	return (REAL32)(m_swTimer.QuadPart - m_swStart.QuadPart) / (REAL32)m_swFreq.QuadPart;
}
CONST REAL32 EPN_Timer :: GetEndMicroSecond() CONST 
{ 
	return m_fTimeforDuration;
}
CONST REAL32	EPN_Timer :: GetEndMilliSecond() CONST 
{ 
	return m_fTimeforDuration*1000.f;
}
BOOL EPN_Timer ::IsTimerStart()
{
	return m_TimerStartFlag;
}
VOID EPN_Timer::Start()
{
	QueryPerformanceCounter(&m_swStart);
	m_TimerStartFlag = TRUE;
}

VOID EPN_Timer::End()
{
	QueryPerformanceCounter(&m_swEnd);
	m_fTimeforDuration = (REAL32)(m_swEnd.QuadPart - m_swStart.QuadPart) / (REAL32)m_swFreq.QuadPart;
	m_TimerStartFlag = FALSE;
}

VOID EPN_Timer :: Clear()
{
	m_swFreq.LowPart = m_swFreq.HighPart = 0;
	m_swStart = m_swFreq;
	m_swTimer = m_swFreq;
	m_swEnd = m_swFreq;
	m_fTimeforDuration = 0;
	m_TimerStartFlag = FALSE;
}