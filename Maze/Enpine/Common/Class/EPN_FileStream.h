#pragma once

#include "../Base/CommonBase.h"


namespace EPN
{
	namespace COMMON
	{
		enum class FILE_ACCESS_FLAG : BYTE
		{
			READ = 0,	//읽기 전용
			WRITE,	//쓰기 전용
			READ_WRITE,		//읽기 쓰기 전용
			ADD	//붙여 쓰기 전용
		};

		enum class FILE_STREAM_FLAG : BYTE
		{
			CREATE_IS_NULL = 0,		//파일이 없을 경우만 생성
			CREATE_IS_ALWAYS,	//항상 새 파일을 생성
			OPEN_IS_EXISTING	//파일이 존재할 경우만 오픈
		};

		enum class FILE_READ_RESULT : BYTE
		{
			FAIL = 0,
			OK,
			END
		};

		// 파일 관련 처리 기능을 모아둔 클래스
		// 파일 암/복호화 기능도 여기서 처리할 예정
		class EPN_FileStream
		{
		private:
			FILE* m_pFile;
			//BOOL m_CryptFlag;

			CHAR m_FilePath[PATH_MAX];
			CHAR m_FileName[PATH_MAX];
			CHAR m_FileExtension[PATH_MAX];
		public:
			EPN_FileStream(/*BOOL UseCryptFlag = FALSE*/);
			~EPN_FileStream(VOID);
		public:
			BOOL OpenFile(CONST CHAR* Path,
				FILE_ACCESS_FLAG AccessFlag,
				FILE_STREAM_FLAG StreamFlag = FILE_STREAM_FLAG::OPEN_IS_EXISTING, BOOL BinaryModeFlag = TRUE);

			BOOL CloseFile();

			BOOL Write(CHAR* pData, SIZE_T Size = 0, SIZE_T* pResult = NULL_PTR);
			BOOL WriteFormat(CHAR* pFormat, ...);
			FILE_READ_RESULT Read(CHAR* pData, SIZE_T Size, SIZE_T* pResult = NULL_PTR);

		public:
			FILE* GetFilePtr();
			BOOL SetFileEPN_Pos(INT32 EPN_Pos, INT32 Flag = SEEK_SET);
			SIZE_T GetFileEPN_Pos();
			SIZE_T GetFileSize();

			BOOL IsReadEnd();
			CONST CHAR* GetFilePath();
			CONST CHAR* GetFileName();
			CONST CHAR* GetFileExtension();
		public:
			INT32 ReadWord(CHAR* pDest, INT32 DestSize);
			INT32 FindWord(CHAR* pStr, INT32 Size = -1, BOOL LineCheckFlag = TRUE);
			INT32 FindData(BYTE Data);
			INT32 FindDataArray(BYTE* pData, INT32 Size);
		public:
			//VOID SetCryptFlag(BOOL Flag);
			//BOOL GetCryptFlag();
		};

	}
}

