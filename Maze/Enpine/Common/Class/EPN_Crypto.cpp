#include "EPN_Crypto.h"
#include "../Base/CommonFunction.h"
#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion

//EPN_Crypto* EPN_Crypto ::m_pCrypto = NULL;

__declspec(align(16)) UINT32 EPN_128_KEY[4] = { 0x2b7e1516 , 0x28aed2a6, 0xabf71588, 0x09cf4f3c };//128BitKey
__declspec(align(16)) BYTE EPN_PROC_KEY[16] = { 0, };



//EPN_Crypto* EPN::COMMON::EPN_Crypto :: GetInstance()
//{
//	return m_pCrypto;
//}
//EPN_Crypto* EPN::COMMON::EPN_Crypto :: CreateInstance(VOID)
//{
//	if (!m_pCrypto)
//		m_pCrypto = new EPN_Crypto();
//	return m_pCrypto;
//}
//BOOL EPN::COMMON::EPN_Crypto::ReleaseInstance()
//{
//	if (!m_pCrypto)
//		return FALSE;
//	delete m_pCrypto;
//	m_pCrypto = NULL;
//	return TRUE;
//}

EPN :: COMMON :: EPN_Crypto :: EPN_Crypto()
{
	memset(m_Passkey, 0, sizeof(m_Passkey));
	m_AlgorithmKey = 0;
	m_pEnByteMap = NULL_PTR;
	m_pDeByteMap = NULL_PTR;
	m_PasskeyFlag = FALSE;
}


EPN_Crypto :: ~EPN_Crypto()
{
	//Create DeByteMap
	if (m_pEnByteMap != NULL_PTR)
	{
		m_pEnByteMap = (BYTE*)((UINT32)m_pEnByteMap ^ (*((UINT32*)m_Passkey)));
		delete[] m_pEnByteMap;
	}
	if (m_pDeByteMap != NULL_PTR)
	{
		m_pDeByteMap = (BYTE*)((UINT32)m_pDeByteMap ^ (*((UINT32*)m_Passkey)));
		delete[] m_pDeByteMap;
	}
}

VOID EPN::COMMON::EPN_Crypto :: ByteMapShuffle_0(BYTE* pByteMap)
{
	BYTE Key[EPN_BYTE_MAP_WIDTH] = { 0, };
	memset(Key, EPN_PROC_KEY[0], EPN_BYTE_MAP_WIDTH);
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		_asm
		{
			mov edi, pByteMap
			movdqu xmm0, Key
			movdqu xmm1, [edi]
			pxor xmm0, xmm1
			movdqu [edi], xmm0
		}
		pByteMap += 16;
	}
	//ASM <-> C++
	//int Index = 0;
	/*for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH; ++Width)
		{
			pByteMap[Index] ^= EPN_PROC_KEY[0];
			++Index;
		}
	}*/
}
VOID EPN::COMMON::EPN_Crypto :: ByteMapShuffle_1(BYTE* pByteMap)
{
	BYTE Key[EPN_BYTE_MAP_WIDTH] = { 0, };
	memset(Key, EPN_PROC_KEY[4], EPN_BYTE_MAP_WIDTH);
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		__asm
		{
			mov edi, pByteMap
			movdqu xmm0, Key
			movdqu xmm1, [edi]
			paddb xmm0, xmm1
			movdqu[edi], xmm0
		}
		pByteMap += EPN_BYTE_MAP_WIDTH;
	}

	//ASM <-> C++
	/*
	int Index = 0;
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH; ++Width)
		{
			pByteMap[Index] += EPN_PROC_KEY[4];
			++Index;
		}
	}
	*/

}
VOID EPN::COMMON::EPN_Crypto :: ByteMapShuffle_2(BYTE* pByteMap)
{
	int Index = 0;
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		if (EPN_PROC_KEY[Height] % 2 == 1)
		{
			for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH;)
			{
				BYTE Temp = pByteMap[Index];
				pByteMap[Index] = pByteMap[Index + 1];
				pByteMap[Index + 1] = Temp;

				Width += 2;
				Index += 2;
			}
		}
	}
}

VOID EPN::COMMON::EPN_Crypto::ByteMapShuffle_3(BYTE* pByteMap)
{
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT - 1;)
	{
		if (EPN_PROC_KEY[Height] % 2 == 0)
		{
			for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH;)
			{
				BYTE Temp = pByteMap[(Height)* EPN_BYTE_MAP_WIDTH + Width];
				pByteMap[(Height)* EPN_BYTE_MAP_WIDTH + Width] = pByteMap[(Height + 1)* EPN_BYTE_MAP_WIDTH + Width];
				pByteMap[(Height + 1)* EPN_BYTE_MAP_WIDTH + Width] = Temp;

				++Width;
			}
		}
		++Height;
	}
}


VOID EPN::COMMON::EPN_Crypto::ByteMapShuffle_4(BYTE* pByteMap)
{
	BYTE Key[EPN_BYTE_MAP_WIDTH] = { 0, };
	memset(Key, EPN_PROC_KEY[8], EPN_BYTE_MAP_WIDTH);
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		__asm
		{
			mov edi, pByteMap
			movdqu xmm0, Key
			movdqu xmm1, [edi]
			psubb xmm0, xmm1
			movdqu [edi], xmm0
		}
		pByteMap += 16;
	}

	//ASM <-> C++
	/*
	int Index = 0;
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH; ++Width)
		{
			pByteMap[Index] -= EPN_PROC_KEY[8];
			++Index;
		}
	}
	*/
}


VOID EPN::COMMON::EPN_Crypto::ByteMapShuffle_5(BYTE* pByteMap)
{
	BYTE Key[EPN_BYTE_MAP_WIDTH] = { 0, };
	memset(Key, EPN_PROC_KEY[12], EPN_BYTE_MAP_WIDTH);
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		__asm
		{
			mov edi, pByteMap
			movdqu xmm0, Key
			movdqu xmm1, [edi]
			pxor xmm0, xmm1
			movdqu[edi], xmm0
		}
		pByteMap += 16;
	}

	//ASM <-> C++
	/*
	int Index = 0;
	for (UINT Height = 0; Height < EPN_BYTE_MAP_HEIGHT; ++Height)
	{
		for (UINT Width = 0; Width < EPN_BYTE_MAP_WIDTH; ++Width)
		{
			pByteMap[Index] ^= EPN_PROC_KEY[12];
			++Index;
		}
	}
	*/
}
BOOL EPN::COMMON::EPN_Crypto::IsPassKey()
{
	return m_PasskeyFlag;
}

BOOL EPN :: COMMON :: EPN_Crypto :: SetPasskey(CONST BYTE Key[EPN_CRYPT_KEY_MAX])
{
	memcpy(EPN_PROC_KEY, Key, 16);
	__asm
	{
		movdqa xmm0, EPN_PROC_KEY
		movdqa xmm1, EPN_128_KEY
		pxor xmm0, xmm1
		movdqa EPN_PROC_KEY, xmm0
	}
	memcpy(m_Passkey, EPN_PROC_KEY, 16);

	//ASM <-> C++
	//for (UINT32 Cnt = 0; Cnt < EPN_CRYPT_KEY_MAX; Cnt++)
	//{
	//	//m_Passkey[Cnt] = Key[Cnt] ^ (*(((BYTE*)(EPN_128_KEY)) + Cnt));
	//}

	BYTE ByteMap[EPN_BYTE_MAP_HEIGHT][EPN_BYTE_MAP_WIDTH] =
	{
		0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
		0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
		0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
		0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
		0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
		0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,
		0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
		0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f,
		0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,
		0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9a,0x9b,0x9c,0x9d,0x9e,0x9f,
		0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,
		0xb0,0xb1,0xb2,0xb3,0xb4,0xb5,0xb6,0xb7,0xb8,0xb9,0xba,0xbb,0xbc,0xbd,0xbe,0xbf,
		0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
		0xd0,0xd1,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xdb,0xdc,0xdd,0xde,0xdf,
		0xe0,0xe1,0xe2,0xe3,0xe4,0xe5,0xe6,0xe7,0xe8,0xe9,0xea,0xeb,0xec,0xed,0xee,0xef,
		0xf0,0xf1,0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfc,0xfd,0xfe,0xff
	};

	
	//Total 6 Round Process//

	//Round 1
	ByteMapShuffle_0((BYTE*)ByteMap);

	//Round 2
	ByteMapShuffle_1((BYTE*)&ByteMap);

	//홀수 카운트로 셔플하면 최악의 경우도 한번은 섞인다..
	for (int Cnt = 0; Cnt < 9; ++Cnt)
	{
		//Round 3
		ByteMapShuffle_2((BYTE*)&ByteMap);

		//Round 4
		ByteMapShuffle_3((BYTE*)&ByteMap);
	}
	
	//Round 5
	ByteMapShuffle_4((BYTE*)&ByteMap);

	//Round 6
	ByteMapShuffle_5((BYTE*)&ByteMap);
	

	//Create DeByteMap
	if (m_pEnByteMap != NULL_PTR)
	{
		m_pEnByteMap = (BYTE*)((UINT32)m_pEnByteMap ^ (*((UINT32*)m_Passkey)));
		delete[] m_pEnByteMap;
		m_pEnByteMap = NULL;
	}
	if (m_pDeByteMap != NULL_PTR)
	{
		m_pDeByteMap = (BYTE*)((UINT32)m_pDeByteMap ^ (*((UINT32*)m_Passkey)));
		delete[] m_pDeByteMap;
		m_pDeByteMap = NULL;
	}
	m_pEnByteMap = new BYTE[EPN_BYTE_MAP_LENGTH];
	m_pDeByteMap = new BYTE[EPN_BYTE_MAP_LENGTH];
	memcpy(m_pEnByteMap, ByteMap, EPN_BYTE_MAP_LENGTH);

	UINT8 Cnt = 0;
	for (UINT8 i = 0; i<16; ++i)
	{
		for (UINT8 j = 0; j<16; ++j)
		{
			//m_pDeByteMap[m_pEnByteMap[i][j] >> 4][m_pEnByteMap[i][j] & 0x0f] = (BYTE)((i << 4) ^ j);
			m_pDeByteMap[m_pEnByteMap[Cnt]] = (BYTE)Cnt;
			Cnt++;
		}
	}
	//Byte Map Address Encrypt 
	m_pEnByteMap = (BYTE*)((UINT32)m_pEnByteMap ^ (*((UINT32*)m_Passkey)));
	m_pDeByteMap = (BYTE*)((UINT32)m_pDeByteMap ^ (*((UINT32*)m_Passkey)));

	m_PasskeyFlag = TRUE;
	return TRUE;
}

VOID EPN::COMMON::EPN_Crypto::DataServingKey(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{

}
VOID EPN::COMMON::EPN_Crypto::DataServingKey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ ((BYTE*)EPN_128_KEY)[ Cnt % EPN_CRYPT_KEY_MAX ];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingKey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ ((BYTE*)EPN_128_KEY)[15 - (Cnt % EPN_CRYPT_KEY_MAX )];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingKey_2(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ ((BYTE*)EPN_128_KEY)[(8 + (Cnt % EPN_CRYPT_KEY_MAX)) >= 16 ? (Cnt % EPN_CRYPT_KEY_MAX) - 8 : (8 + (Cnt % EPN_CRYPT_KEY_MAX))];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingKey_3(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt <  Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ ((BYTE*)EPN_128_KEY)[(8 + (Cnt % EPN_CRYPT_KEY_MAX)) >= 16 ? 15 - ((Cnt % EPN_CRYPT_KEY_MAX) - 8) : (15 - (Cnt % EPN_CRYPT_KEY_MAX))];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingPasskey(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	
}
VOID EPN::COMMON::EPN_Crypto::DataServingPasskey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ m_Passkey[(Cnt % EPN_CRYPT_KEY_MAX)];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingPasskey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ m_Passkey[16- (Cnt % EPN_CRYPT_KEY_MAX)];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingPasskey_2(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ m_Passkey[(8 + (Cnt % EPN_CRYPT_KEY_MAX)) >= 16 ? (Cnt % EPN_CRYPT_KEY_MAX) -8 : (8 + (Cnt % EPN_CRYPT_KEY_MAX))];
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataServingPasskey_3(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pSrc[Cnt] ^ m_Passkey[(8 + (Cnt % EPN_CRYPT_KEY_MAX)) >= 16 ? 15 - ((Cnt % EPN_CRYPT_KEY_MAX) - 8) : (15 - (Cnt % EPN_CRYPT_KEY_MAX))];
	}
	//CODE_END_MARK
}
VOID EPN::COMMON::EPN_Crypto::DataShiftKey(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{

}

VOID EPN::COMMON::EPN_Crypto::DataShiftKey_0(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	UINT8 ShiftValue = (UINT8)((UINT32)(m_Passkey[0] + m_Passkey[15]) % 7)+1;
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = (pSrc[Cnt] << (8 - ShiftValue)) | (pSrc[Cnt] >> ShiftValue);
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DataShiftKey_1(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	UINT8 ShiftValue = (UINT8)((UINT32)(m_Passkey[0] + m_Passkey[15]) % 7) + 1;
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = (pSrc[Cnt] >> (8 - ShiftValue)) | (pSrc[Cnt] << ShiftValue);
	}
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::EnByteMapping(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	//EnByte Map Address Decrypt 
	BYTE* pEnByteMap = (BYTE*)((UINT32)m_pEnByteMap ^ (*((UINT32*)m_Passkey)));
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
		pDest[Cnt] = pEnByteMap[pSrc[Cnt]];
	//CODE_END_MARK
}

VOID EPN::COMMON::EPN_Crypto::DeByteMapping(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	//DeByte Map Address Decrypt 
	BYTE* pDeByteMap = (BYTE*)((UINT32)m_pDeByteMap ^ (*((UINT32*)m_Passkey)));
	for (UINT32 Cnt = 0; Cnt < Size; ++Cnt)
	{
		pDest[Cnt] = pDeByteMap[pSrc[Cnt]];
	}
	//CODE_END_MARK
}
BOOL EPN::COMMON::EPN_Crypto::EnCrypting_Simple(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	DataServingKey_3(pDest, pSrc, Size);

	return TRUE;
}
BOOL EPN::COMMON::EPN_Crypto::DeCrypting_Simple(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	DataServingKey_3(pDest, pSrc, Size);

	return TRUE;
}
BOOL EPN::COMMON::EPN_Crypto::EnCrypting(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	if (m_PasskeyFlag == FALSE)
	{
		PRINT_LOG("EnCrypting Passkey 가 설정 되지 않음\n");
		return FALSE;
	}
	DataServingKey_0(pDest, pSrc, Size);
	EnByteMapping(pDest, pDest, Size);
	DataServingPasskey_1(pDest, pDest, Size);
	DataShiftKey_0(pDest, pDest, Size);
	
	return TRUE;
}
BOOL EPN::COMMON::EPN_Crypto::DeCrypting(BYTE* pDest, BYTE* pSrc, UINT32 Size)
{
	if (m_PasskeyFlag == FALSE)
	{
		PRINT_LOG("EnCrypting Passkey 가 설정 되지 않음\n");
		return FALSE;
	}
	DataShiftKey_1(pDest, pSrc, Size);
	DataServingPasskey_1(pDest, pDest, Size);
	DeByteMapping(pDest, pDest, Size);
	DataServingKey_0(pDest, pDest, Size);

	return TRUE;
}
BOOL EPN::COMMON::EPN_Crypto::SetAlgorithmKey(UINT32 Key)
{
	m_AlgorithmKey = Key;

	return TRUE;
}