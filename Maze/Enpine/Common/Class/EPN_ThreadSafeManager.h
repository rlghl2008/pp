#pragma once

#include "../Base/CommonBase.h"

#include "EPN_MemoryPool.hpp"
#include "EPN_ThreadSafe.h"

namespace EPN
{
	namespace COMMON
	{
		class EPN_ThreadSafeManager
		{
		public:
			EPN_ThreadSafeManager();
			virtual ~EPN_ThreadSafeManager();
		private:
			EPN_MemoryPool<EPN_ThreadSafe*> m_mpThreadSafe;
		public:
			INT32 CreateThreadSafe(EPN::COMMON::THREAD_SAFE_FLAG Flag);
			BOOL DeleteThreadSafe(INT32 Index);
			EPN_ThreadSafe* GetThreadSafe(INT32 Index);

			BOOL LockArea(INT32 Index);
			BOOL UnLockArea(INT32 Index);
		private:
			static EPN_ThreadSafeManager* m_pThreadSafeManager;
		public:
			static EPN_ThreadSafeManager* GetInstance();
			static EPN_ThreadSafeManager* CreateInstance(VOID);
			static VOID ReleaseInstance();

		};
	}
}
