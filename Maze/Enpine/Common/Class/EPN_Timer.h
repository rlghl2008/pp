#pragma once

#include "../Base/CommonBase.h"
//#include "EPN_LogManager.h"

namespace EPN
{
	namespace COMMON
	{

		class EPN_Timer
		{
		public:
			EPN_Timer();
			virtual ~EPN_Timer();
		private:
			//시간 측정 Start() 여부 
			BOOL m_TimerStartFlag;
		public:
			VOID Clear();
			BOOL IsTimerStart();
		private:
			LARGE_INTEGER m_swFreq;
			LARGE_INTEGER m_swStart;
			LARGE_INTEGER m_swTimer;
			LARGE_INTEGER m_swEnd;
			REAL32 m_fTimeforDuration;
		public:
			VOID Start();
			VOID End();
			CONST REAL32 GetTime();
			CONST REAL32 GetEndMicroSecond() CONST;
			CONST REAL32	 GetEndMilliSecond() CONST;
		};
	}
}