#pragma once 

#include"../Base/CommonBase.h"


namespace EPN
{
	namespace COMMON
	{
		class EPN_CRC32
		{
		public:
			EPN_CRC32();
		private:
			BYTE m_ProcBuffer[32768];
			UINT32 m_TableCRC[256];
		private:
			UINT32 CalcCRC(CONST BYTE* pBuffer, UINT32 Size, UINT32 CRC, UINT32* pTable);
			VOID MakeTableCRC(UINT32 ID);
		public:
			UINT32 GetFileCRC(FILE* pFile);
			UINT32 GetMemoryCRC(BYTE* pMemory, UINT32 Size);

		};
	}
}
