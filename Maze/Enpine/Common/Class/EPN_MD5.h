#pragma once 

#include"../Base/CommonBase.h"


namespace EPN
{
	namespace COMMON
	{
		//EPN_MD5 Class
		//코드 정리 예정
		#define MD5_HASH_16 16
		#define MD5_HASH_32 32
		class EPN_MD5
		{
		public:
			EPN_MD5();
		private:
			struct MD5_STATE_KEY
			{
				UINT32 State[4];
				UINT32 Count[2];
				BYTE Buffer[64];
			} m_StateKey;

			BYTE m_DigestRaw[MD5_HASH_16];
			CHAR m_DigestChars[MD5_HASH_32 + 1];

			VOID MD5Transform(UINT32 State[4], BYTE Block[64]);
			VOID Encode(BYTE* pOutput, UINT32* pInput, UINT32 Len);
			VOID Decode(UINT32* pOutput, BYTE* pInput, UINT32 Len);

			VOID Init();
			VOID Update(BYTE *pInput, UINT32 Len);
			VOID Final();
			VOID WriteToString();
		
		public:
			BOOL GetFileHash32(CHAR *FilePath, BYTE* pDestBuffer, UINT8 DestBufferSize = MD5_HASH_32);
			BOOL GetFilePtrHash32(FILE* pFile, BYTE* pDestBuffer, UINT8 DestBufferSize = MD5_HASH_32);
			BOOL GetFileHandleHash32(HANDLE Handle, BYTE* pDestBuffer, UINT8 DestBufferSize = MD5_HASH_32);

			BOOL GetBufferHash32(BYTE *pSrcBuffer, INT32 SrcBufferSize, BYTE* pDestBuffer, UINT8 DestBufferSize = MD5_HASH_32);
			BOOL GetStringHash32(CHAR *pStr, BYTE* pDestBuffer, UINT8 DestBufferSize = MD5_HASH_32);
		};
	}
}