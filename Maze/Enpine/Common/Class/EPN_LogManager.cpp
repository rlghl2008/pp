#include "EPN_LogManager.h"
#include "../Base/CommonFunction.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
#pragma endregion

EPN_LogManager* EPN_LogManager ::m_pLogManager = NULL_PTR;

EPN_LogManager* EPN_LogManager :: GetInstance()
{
	return m_pLogManager;
}
BOOL EPN_LogManager :: ReleaseInstance()
{
	if(m_pLogManager == NULL_PTR)
		return FALSE;
	delete m_pLogManager;
	m_pLogManager = NULL_PTR;
	return TRUE;
}
EPN_LogManager* EPN_LogManager :: CreateInstance(VOID)
{
	if(m_pLogManager == NULL_PTR)
	{
		m_pLogManager = new EPN_LogManager;
	}
	return m_pLogManager;
}

EPN_LogManager :: EPN_LogManager()
{
#ifdef _DEBUG
	m_LogFlag = LOG_CONSOLE;
#else
	m_LogFlag = LOG_NON;
#endif

	memset(m_FilePath, 0 , sizeof(m_FilePath));
}
EPN_LogManager :: ~EPN_LogManager()
{

}


BYTE EPN_LogManager :: GetLogFlag()
{
	return m_LogFlag;
}
VOID EPN_LogManager :: SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

CONST CHAR* EPN_LogManager :: GetFilePath()
{
	return m_FilePath;
}
BOOL EPN_LogManager :: SetFilePath(CONST CHAR* Path)
{
	if(m_FilePath[0] != NULL || !Path)
		return FALSE;
	strcpy_s(m_FilePath, PATH_MAX, Path);
	return TRUE;
}
BOOL EPN_LogManager :: SimpleLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List)
{
	if (LogFlag == LOG_NON)
		return TRUE;
	CHAR Buffer[1024];

	vsprintf_s( Buffer, _countof(Buffer), pFmt, *pVA_List);
	va_end(*pVA_List);

	//API [ strcat_s ] ... Swap Schedule
	strcat_s( Buffer, 1024, "\n" );


	if(LogFlag & LOG_CONSOLE)
	{
		printf("%s", Buffer);
	}
	if(LogFlag & LOG_DEBUG)
	{
		OutputDebugStringA(Buffer);
	}
	if(LogFlag & LOG_FILE)
	{

	}
	if (LogFlag & LOG_BOX)
	{
		MessageBoxA(NULL, Buffer, "Enpine Engine", MB_OK);
	}
	return TRUE;
}
BOOL EPN_LogManager :: DetailLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List)
{
	//미구현 테스트 중
	if (LogFlag == LOG_NON)
		return TRUE;
	CHAR FmtBuffer[1024];
	CHAR PrintBuffer[1024];


	vsprintf_s( FmtBuffer, _countof(FmtBuffer), pFmt, *pVA_List);
	va_end(*pVA_List);

	strcat_s( FmtBuffer, 1024, "\n" );
	sprintf_s( PrintBuffer, 1024, "%s [ %d ] : %s", __FILE__, __LINE__, FmtBuffer);

	if(LogFlag & LOG_CONSOLE)
	{
		printf("%s\n", PrintBuffer);
	}
	if(LogFlag & LOG_DEBUG)
	{
		OutputDebugStringA(PrintBuffer);
	}
	if(LogFlag & LOG_FILE)
	{

	}
	if (LogFlag & LOG_BOX)
	{
		MessageBoxA(NULL, PrintBuffer, "LEUCO SHELL", MB_OK);
	}
	return TRUE;
}

BOOL EPN_LogManager :: PrintLog(CONST CHAR* pFmt, ...)
{
	if (m_LogFlag == LOG_NON)
		return TRUE;
	va_list Marker;
	va_start( Marker, pFmt );
	if(m_LogFlag & LOG_DETAIL)
	{
		return DetailLog(m_LogFlag, pFmt, &Marker);
	}

	return SimpleLog(m_LogFlag, pFmt, &Marker);
}

BOOL EPN_LogManager :: PrintLog(CONST CHAR* pFmt, va_list* pVA_List)
{
	if (m_LogFlag == LOG_NON)
		return TRUE;
	if(m_LogFlag & LOG_DETAIL)
	{
		return DetailLog(m_LogFlag, pFmt, pVA_List);
	}

	return SimpleLog(m_LogFlag, pFmt, pVA_List);
}
BOOL EPN_LogManager :: PrintLog(BYTE LogFlag, CONST CHAR* pFmt, ...)
{
	if (LogFlag == LOG_NON)
		return TRUE;
	va_list Marker;
	va_start( Marker, pFmt );
	if(LogFlag & LOG_DETAIL)
	{
		return DetailLog(LogFlag, pFmt, &Marker);
	}

	return SimpleLog(LogFlag, pFmt, &Marker);
}
BOOL EPN_LogManager :: PrintLog(BYTE LogFlag, CONST CHAR* pFmt, va_list* pVA_List)
{
	if (LogFlag == LOG_NON)
		return TRUE;
	if(LogFlag & LOG_DETAIL)
	{
		return DetailLog(LogFlag, pFmt, pVA_List);
	}

	return SimpleLog(LogFlag, pFmt, pVA_List);
}


BOOL EPN::COMMON::FUNCTION::SET_LOG_FLAG(BYTE LogFlag)
{
	EPN_LogManager* pLog = EPN_LogManager::CreateInstance();
	pLog->SetLogFlag(LogFlag);
	return TRUE;
}

BOOL EPN::COMMON::FUNCTION::PRINT_LOG(CHAR* pFmt, ...)
{
	EPN_LogManager* pLog = EPN_LogManager::CreateInstance();
	va_list Marker;
	va_start(Marker, pFmt);

	return  pLog->PrintLog(pFmt, &Marker);
}

BOOL EPN::COMMON::FUNCTION::PRINT_LOG(BYTE LogFlag, CHAR* pFmt, ...)
{
	if (LogFlag == (BYTE)EPN_LOG_FLAG::NON)
		return TRUE;
	EPN_LogManager* pLog = EPN_LogManager::CreateInstance();
	va_list Marker;
	va_start(Marker, pFmt);

	return  pLog->PrintLog(LogFlag, pFmt, &Marker);
}

BOOL EPN::COMMON::FUNCTION::RELEASE_LOG()
{
	return EPN_LogManager::ReleaseInstance();
}