#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "./Base/CommonBase.h"
#include "./Base/CommonStructure.h"
#include "./Base/CommonFunction.h"

#include "./Class/EPN_KeyCode.h"
#include "./Class/EPN_Timer.h"
#include "./Class/EPN_String.h"
#include "./Class/EPN_StringIndexPool.h"
#include "./Class/EPN_ThreadSafe.h"
#include "./Class/EPN_ThreadSafeManager.h"
#include "./Class/EPN_FileStream.h"
#include "./Class/EPN_CriticalSection.h"
#include "./Class/EPN_MemoryPool.hpp"