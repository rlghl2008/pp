#include "EPN_TextureInterface.h"
#include "../Device/EPN_DeviceInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
#pragma endregion

EPN_TextureInterface* EPN_TextureInterface::m_pTextureInterface = NULL_PTR;

EPN_TextureInterface* EPN_TextureInterface::GetInstance()
{
	return m_pTextureInterface;
}

EPN_TextureInterface* EPN_TextureInterface::CreateInstance()
{
	if (NULL_PTR == m_pTextureInterface)
		m_pTextureInterface = new EPN_TextureInterface();
	return m_pTextureInterface;
}

VOID EPN_TextureInterface::ReleaseInstance()
{
	if (NULL_PTR != m_pTextureInterface)
		delete m_pTextureInterface;
	m_pTextureInterface = NULL_PTR;
}

EPN_TextureInterface::EPN_TextureInterface()
{

}

EPN_TextureInterface::~EPN_TextureInterface()
{

}


VOID EPN_TextureInterface::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_TextureInterface::GetLogFlag()
{
	return m_LogFlag;
}

INT32 EPN_TextureInterface::LoadTexture(estring Path)
{
	EPN_Texture *pTexture = new EPN_Texture();
	INT32 SpriteIndex = pTexture->mpSprite.AddData(Sprite::create(Path.GetStrA()));
	INT32 TextureIndex = m_mpTexture.AddData(pTexture);
	if (SpriteIndex < 0 || TextureIndex < 0)
		return -1;

	m_mpTexture.GetData(TextureIndex)->mpSprite.GetData(SpriteIndex)->retain();

	return TextureIndex;
}

INT32 EPN_TextureInterface::LoadTexture(estring SaveTextureName, estring Path)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveTextureName);
	if (0 > Index && EPN_SI_STRLEN_MAX > SaveTextureName.Length())
	{
		Index = LoadTexture(Path);
		if (0 > Index)
			return -1;
		if (FALSE == m_StringIndexPool.InsertData(SaveTextureName, Index))
		{
			PRINT_LOG(m_LogFlag, "( InsertData Failed )\n", SaveTextureName);
			DeleteTexture(Index);
			return -1;
		}
	}
	else
	{
		PRINT_LOG(m_LogFlag, "( Error : 이미 저장된 SaveTextureName [ %s ] 문자열 길이가 [ %d ] 를 초과하였습니다. ) \n", SaveTextureName, EPN_SI_STRLEN_MAX);
	}

	return Index;
}

EPN_Texture* EPN_TextureInterface::GetTexture(INT32 Index)
{
	return (*m_mpTexture[Index]);
}
EPN_Texture* EPN_TextureInterface::GetTexture(estring SaveTextureName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveTextureName);
	if (0 > Index)
	{
		PRINT_LOG(m_LogFlag, "(Error : 저장되지않은 SaveTextureName [ %s ] 입니다. ) \n", SaveTextureName);
		return FALSE;
	}
	return GetTexture(Index);
}

EPN_Rect EPN_TextureInterface::GetTextureSize(INT32 Index)
{
	cocos2d::Size Size = GetTexture(Index)->mpSprite.GetData(0)->getTexture()->getContentSize();
	return EPN_Rect(0.f, 0.f, (REAL32)Size.width, (REAL32)Size.height);
}

EPN_Rect EPN_TextureInterface::GetTextureSize(estring SaveTextureName)
{
	cocos2d::Size Size = GetTexture(SaveTextureName)->mpSprite.GetData(0)->getTexture()->getContentSize();
	return EPN_Rect(0.f, 0.f, (REAL32)Size.width, (REAL32)Size.height);
}

INT32 EPN_TextureInterface::GetTextureIndex(estring SaveTextureName)
{
	return m_StringIndexPool.SearchIndex(SaveTextureName);
}

estring EPN_TextureInterface::GetTextureName(INT32 Index)
{
	return m_StringIndexPool.SearchString(Index);
}

BOOL EPN_TextureInterface::DeleteTexture(INT32 Index)
{
	EPN_Texture* pTexture = GetTexture(Index);
	if (NULL_PTR == pTexture)
	{
		PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", 0);
		return FALSE;
	}

	//해당 텍스쳐의 스프라이트 풀 내에 있는 모든 스프라이트 release()
	for (int Cnt = 0, RelCnt = 0; RelCnt < pTexture->mpSprite.GetAddDataCount(); ++Cnt)
	{
		//현재 인덱스에 데이터가 존재하면 릴리즈
		if (TRUE == pTexture->mpSprite.IsData(Cnt))
		{
			pTexture->mpSprite.GetData(Cnt)->release();
			++RelCnt;
		}
	}

	//텍스쳐풀과 SI풀에서 해당 텍스쳐 삭제
	m_mpTexture.EraseData(Index);
	m_StringIndexPool.DeleteData(Index);
	return TRUE;
}
BOOL EPN_TextureInterface::DeleteTexture(estring SaveTextureName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveTextureName);
	if (0 > Index)
	{
		PRINT_LOG(m_LogFlag, "(Error : 저장되지않은 SaveTextureName [ %s ] 입니다. ) \n", SaveTextureName);
		return FALSE;
	}
	return DeleteTexture(Index);
}

INT32 EPN_TextureInterface::GetInsertCount()
{
	return m_mpTexture.GetAddDataCount();
}

BOOL EPN_TextureInterface::Blt(INT32 TextureIndex, INT32 LayerIndex, EPN_Pos Pos, EPN_Rect Rect, EPN_ARGB Color, REAL32 Angle, EPN_Scaling Scale, BOOL WidthReverse, BOOL HeightReverse)
{
	EPN_Texture * pTexture = GetTexture(TextureIndex);
	if (pTexture == NULL_PTR)
		return FALSE;


	EPN_Device *pDevice = EPN_DeviceInterface::CreateInstance()->GetDevice();
	if (pDevice == NULL_PTR)
		return FALSE;
	if (pDevice->mpDeviceLayer.IsData(LayerIndex) == FALSE)
		return FALSE;

	///* 중복 Blt 일 시 처리 */
	if (pTexture->BltCount >= pTexture->mpSprite.GetAddDataCount())
	{
		/* 메모 : LoadTexture 하나 더 오버로딩하기
		이거는 같은텍스쳐있는 스프라이트를 더해주는거고
		하다 더 오버로딩할꺼는
		같은 텍스쳐인데 다른 텍스쳐를 로드하는것.

		LoadTexture(EPN_Texture *pTexture);  <-- 나중에 쓸일있을 수도있으니 ㄱㄱ
		*/
		cocos2d::Texture2D *pCCSpriteTexture = pTexture->mpSprite.GetData(0)->getTexture();
		INT32 SpriteIndex = pTexture->mpSprite.AddData(cocos2d::Sprite::createWithTexture(pCCSpriteTexture));
		//스프라이트 레퍼런스 카운트를 하나 증가
		pTexture->mpSprite.GetData(SpriteIndex)->retain();
	}

	cocos2d::Sprite *pCurBltSprite = pTexture->mpSprite.GetData(pTexture->BltCount++);

	//Texture Rect ( EPN_Rect 와 다르게 cocos2d::Rect 는 w h 를 받기때문에 맞춰서 계산 )
	cocos2d::Rect Origin = pCurBltSprite->getTextureRect();
	if (Rect == EPN_Rect(-1, -1, -1, -1))
	{
		Rect = EPN_Rect(Origin.origin.x, Origin.origin.y, Origin.origin.x + Origin.size.width, Origin.origin.y + Origin.size.height);
	}

	//Texture Rect
	cocos2d::Rect TextureRect(Rect.Top.X, Rect.Top.Y, Rect.Bottom.X - Rect.Top.X, Rect.Bottom.Y - Rect.Top.Y);
	pCurBltSprite->setTextureRect(TextureRect);

	//Texture Color
	pCurBltSprite->setColor(Color3B(Color.Red, Color.Green, Color.Blue));
	pCurBltSprite->setOpacity(Color.Alpha);

	//Texture Scale
	pCurBltSprite->setScale(Scale.X, Scale.Y);

	//Texture Reversing (Flipping)
	pCurBltSprite->setFlippedX((bool)WidthReverse);
	pCurBltSprite->setFlippedY((bool)HeightReverse);

	//Texture Angle
	pCurBltSprite->setRotation(Angle);

	//Texture Pos
	REAL32 Width = (Rect.Top.X * Scale.X) - (Rect.Bottom.X * Scale.X);
	if (Rect.Bottom.X > Rect.Top.X)
		Width = (Rect.Bottom.X * Scale.X) - (Rect.Top.X * Scale.X);
	REAL32 Height = (Rect.Top.Y * Scale.Y) - (Rect.Bottom.Y * Scale.Y);
	if (Rect.Bottom.Y > Rect.Top.Y)
		Height = (Rect.Bottom.Y * Scale.Y) - (Rect.Top.Y * Scale.Y);

	pCurBltSprite->setPosition(Vec2(Pos.X + Width / 2.f, Pos.Y - Height / 2.f));

	(*pDevice->mpDeviceLayer[LayerIndex])->addChild(pCurBltSprite);

	return TRUE;
}

BOOL EPN_TextureInterface::Blt(estring SaveTextureName, INT32 LayerIndex, EPN_Pos Pos, EPN_Rect Rect, EPN_ARGB Color, REAL32 Angle, EPN_Scaling Scale, BOOL WidthReverse, BOOL HeightReverse)
{
	return Blt(GetTextureIndex(SaveTextureName), LayerIndex, Pos, Rect, Color, Angle, Scale, WidthReverse, HeightReverse);
}