#pragma once

#include "../../../Common/Common.h"

struct EPN_Texture
{
	EPN_Texture()
	{
		BltCount = 0;
	}
	~EPN_Texture()
	{
		//전부 지우는 처리
	}
	EPN::COMMON::EPN_MemoryPool<cocos2d::Sprite*> mpSprite;
	INT32 BltCount;
};

//typedef Sprite EPN_Texture;
namespace EPN
{
	namespace GENERIC
	{
		namespace TEXTURE
		{
			class EPN_TextureInterface
			{
			public:
				EPN_TextureInterface();
				~EPN_TextureInterface();
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private:
				EPN::COMMON::EPN_MemoryPool<EPN_Texture*> m_mpTexture;
				EPN::COMMON::EPN_StringIndexPool     m_StringIndexPool;
			public:
				INT32 LoadTexture(estring Path);
				INT32 LoadTexture(estring SaveTextureName, estring Path);
				EPN_Texture* GetTexture(INT32 Index);
				EPN_Texture* GetTexture(estring SaveTextureName);
				EPN_Rect GetTextureSize(INT32 Index);
				EPN_Rect GetTextureSize(estring SaveTextureName);
				INT32 GetTextureIndex(estring SaveTextureName);
				estring GetTextureName(INT32 Index);
				BOOL DeleteTexture(INT32 Index);
				BOOL DeleteTexture(estring SaveTextureName);
				INT32 GetInsertCount();

				BOOL Blt(INT32 TextureIndex, INT32 LayerIndex = 0, EPN_Pos Pos = EPN_Pos(0, 0), EPN_Rect Rect = EPN_Rect(-1, -1, -1, -1), EPN_ARGB Color = EPN_ARGB(255, 255, 255, 255), REAL32 Angle = 0.f, EPN_Scaling Scale = EPN_Scaling(1.0f, 1.0f), BOOL WidthReverse = FALSE, BOOL HeightReverse = FALSE);
				BOOL Blt(estring SaveTextureName, INT32 LayerIndex = 0, EPN_Pos Pos = EPN_Pos(0, 0), EPN_Rect Rect = EPN_Rect(-1, -1, -1, -1), EPN_ARGB Color = EPN_ARGB(255, 255, 255, 255), REAL32 Angle = 0.f, EPN_Scaling Scale = EPN_Scaling(1.0f, 1.0f), BOOL WidthReverse = FALSE, BOOL HeightReverse = FALSE);
			private:
				static EPN_TextureInterface* m_pTextureInterface;
			public:
				static EPN_TextureInterface* GetInstance();
				static EPN_TextureInterface* CreateInstance();
				static VOID ReleaseInstance();
			};
		}
	}
}