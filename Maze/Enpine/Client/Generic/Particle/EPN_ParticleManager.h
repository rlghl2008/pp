#pragma once

#include "EPN_Particle.h"

namespace EPN
{
	namespace GENERIC
	{
		namespace PARTICLE
		{
			class EPN_ParticleManager
			{
			public:
				EPN_ParticleManager();
				~EPN_ParticleManager();
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private:
				//Particle 동적객체
				EPN_Particle* m_pParticle;
				//사용하는 파티클 최대개수
				INT32 m_MaxParticleNum;
			public:
				//최대 파티클 개수 설정 
				BOOL SetMaxParticleNum(INT32 ParticleNum);
				BOOL GetMaxParticleNum();
				//최대 파티클 개수 재설정
				BOOL ResetMaxParticleNum(INT32 ParticleNum);
				//파티클매니저 초기화
				BOOL ClearManager();

				//설정된 파티클중 죽은파티클번호 찾아 재설정하고 해당 파티클인덱스를 반환 죽은파티클이없을시 -1반환
				INT32 AddParticle();
			public:
				//기본값시 사용하는파티클 전부Run 선택시 해당파티클만 Run
				BOOL Run(INT32 ParticleIndex = -1, EPN_Pos Pos = EPN_Pos(0, 0));

			public:
				EPN_Particle* GetParticle(INT32 ParticleIndex);

			public:
				BOOL AllSetParticleDieFlag(BOOL ParticleDieFlag);
				BOOL SetParticleDieFlag(INT32 ParticleIndex, BOOL ParticleDieFlag);
				BOOL GetParticleDieFlag(INT32 ParticleIndex);

				BOOL AllSetDeleteCountFlag(BOOL DeleteCountFlag);
				BOOL SetDeleteCountFlag(INT32 ParticleIndex, BOOL DeleteCountFlag);
				BOOL GetDeleteCountFlag(INT32 ParticleIndex);

				BOOL AllSetDeleteCount(INT32 DeleteCount);
				BOOL SetDeleteCount(INT32 ParticleIndex, INT32 DeleteCount);
				INT32 GetDeleteCount(INT32 ParticleIndex);

				BOOL AllSetParticlePos(EPN_Pos ParticlePos);
				BOOL SetParticlePos(INT32 ParticleIndex, EPN_Pos ParticlePos);
				EPN_Pos GetParticlePos(INT32 ParticleIndex);

				BOOL AllSetParticleMove(EPN_Pos ParticleMove);
				BOOL SetParticleMove(INT32 ParticleIndex, EPN_Pos ParticleMove);
				EPN_Pos GetParticleMove(INT32 ParticleIndex);

				BOOL AllSetMoveAngle(REAL32 MoveAngle);
				BOOL SetMoveAngle(INT32 ParticleIndex, REAL32 MoveAngle);
				REAL32 GetMoveAngle(INT32 ParticleIndex);

				BOOL AllSetMoveSpeed(REAL32 MoveSpeed);
				BOOL SetMoveSpeed(INT32 ParticleIndex, REAL32 MoveSpeed);
				REAL32 GetMoveSpeed(INT32 ParticleIndex);

				BOOL AllSetAlterMoveAngleFlag(BOOL AlterMoveAngleFlag);
				BOOL SetAlterMoveAngleFlag(INT32 ParticleIndex, BOOL AlterMoveAngleFlag);
				BOOL GetAlterMoveAngleFlag(INT32 ParticleIndex);

				BOOL AllSetAlterMoveAngle(REAL32 AlterMoveAngle);
				BOOL SetAlterMoveAngle(INT32 ParticleIndex, REAL32 AlterMoveAngle);
				REAL32 GetAlterMoveAngle(INT32 ParticleIndex);

				BOOL AllSetAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag);
				BOOL SetAlterMoveSpeedFlag(INT32 ParticleIndex, BOOL AlterMoveSpeedFlag);
				BOOL GetAlterMoveSpeedFlag(INT32 ParticleIndex);

				BOOL AllSetAlterMoveSpeed(REAL32 AlterMoveSpeed);
				BOOL SetAlterMoveSpeed(INT32 ParticleIndex, REAL32 AlterMoveSpeed);
				REAL32 GetAlterMoveSpeed(INT32 ParticleIndex);

				BOOL AllSetTextureIndex(INT32 TextureIndex);
				BOOL SetTextureIndex(INT32 ParticleIndex, INT32 TextureIndex);
				INT32 GetTextureIndex(INT32 ParticleIndex);

				BOOL AllSetTextureAngle(REAL32 TextureAngle);
				BOOL SetTextureAngle(INT32 ParticleIndex, REAL32 TextureAngle);
				REAL32 GetTextureAngle(INT32 ParticleIndex);

				BOOL AllSetTextureScaling(EPN_Scaling TextureScaling);
				BOOL SetTextureScaling(INT32 ParticleIndex, EPN_Scaling TextureScaling);
				EPN_Scaling GetTextureScaling(INT32 ParticleIndex);

				BOOL AllSetAlterTextureAngleFlag(BOOL AlterTextureAngleFlag);
				BOOL SetAlterTextureAngleFlag(INT32 ParticleIndex, BOOL AlterTextureAngleFlag);
				BOOL GetAlterTextureAngleFlag(INT32 ParticleIndex);

				BOOL AllSetAlterTextureAngle(REAL32 AlterTextureAngle);
				BOOL SetAlterTextureAngle(INT32 ParticleIndex, REAL32 AlterTextureAngle);
				REAL32 GetAlterTextureAngle(INT32 ParticleIndex);

				BOOL AllSetAlterTextureScalingFlag(BOOL AlterTextureScalingFlag);
				BOOL SetAlterTextureScalingFlag(INT32 ParticleIndex, BOOL AlterTextureScalingFlag);
				BOOL GetAlterTextureScalingFlag(INT32 ParticleIndex);

				BOOL AllSetAlterTextureScaling(EPN_Scaling AlterTextureScaling);
				BOOL SetAlterTextureScaling(INT32 ParticleIndex, EPN_Scaling AlterTextureScaling);
				EPN_Scaling GetAlterTextureScaling(INT32 ParticleIndex);

				BOOL AllSetTextureRect(EPN_Rect TextureRect);
				BOOL SetTextureRect(INT32 ParticleIndex, EPN_Rect TextureRect);
				EPN_Rect GetTextureRect(INT32 ParticleIndex);

				BOOL AllSetAlterTextureRectFlag(BOOL AlterTextureRectFlag);
				BOOL SetAlterTextureRectFlag(INT32 ParticleIndex, BOOL AlterTextureRectFlag);
				BOOL GetAlterTextureRectFlag(INT32 ParticleIndex);

				BOOL AllSetAlterTextureRect(EPN_Rect AlterTextureRect);
				BOOL SetAlterTextureRect(INT32 ParticleIndex, EPN_Rect AlterTextureRect);
				EPN_Rect GetAlterTextureRect(INT32 ParticleIndex);

				BOOL AllSetTextureColor(EPN_ARGB TextureColor);
				BOOL SetTextureColor(INT32 ParticleIndex, EPN_ARGB TextureColor);
				EPN_ARGB GetTextureColor(INT32 ParticleIndex);

				BOOL AllSetAlterTextureColorFlag(BOOL AlterTextureColorFlag);
				BOOL SetAlterTextureColorFlag(INT32 ParticleIndex, BOOL AlterTextureColorFlag);
				BOOL GetAlterTextureColorFlag(INT32 ParticleIndex);

				BOOL AllSetAlterTextureColor(EPN_ARGB AlterTextureColor);
				BOOL SetAlterTextureColor(INT32 ParticleIndex, EPN_ARGB AlterTextureColor);
				EPN_ARGB GetAlterTextureColor(INT32 ParticleIndex);

				BOOL AllSetSpriteFlag(BOOL SpriteFlag);
				BOOL SetSpriteFlag(INT32 ParticleIndex, BOOL SpriteFlag);
				BOOL GetSpriteFlag(INT32 ParticleIndex);

				BOOL AllSetSpriteIndex(INT32 SpriteIndex);
				BOOL SetSpriteIndex(INT32 ParticleIndex, INT32 SpriteIndex);
				INT32 GetSpriteIndex(INT32 ParticleIndex);

				BOOL AllSetSpriteDelay(INT32 SpriteDelay);
				BOOL SetSpriteDelay(INT32 ParticleIndex, INT32 SpriteDelay);
				INT32 GetSpriteDelay(INT32 ParticleIndex);


				BOOL AllSetAfterBltFlag(BOOL AfterBltFlag);
				BOOL SetAfterBltFlag(INT32 ParticleIndex, BOOL AfterBltFlag);
				BOOL GetAfterBltFlag(INT32 ParticleIndex);

				BOOL AllSetAfterImageNum(INT32 AfterBltFlag);
				BOOL SetAfterImageNum(INT32 ParticleIndex, INT32 AfterBltFlag);
				INT32 GetAfterImageNum(INT32 ParticleIndex);

				BOOL AllSetAfterImageCount(INT32 AfterImageCount);
				BOOL SetAfterImageCount(INT32 ParticleIndex, INT32 AfterImageCount);
				INT32 GetAfterImageCount(INT32 ParticleIndex);

				BOOL AllSetAfterImageDeleteCount(INT32 AfterImageDeleteCount);
				BOOL SetAfterImageDeleteCount(INT32 ParticleIndex, INT32 AfterImageDeleteCount);
				INT32 GetAfterImageDeleteCount(INT32 ParticleIndex);

				BOOL AllSetWindUseFlag(BOOL WindUseFlag);
				BOOL SetWindUseFlag(INT32 ParticleIndex, BOOL WindUseFlag);
				BOOL GetWindUseFlag(INT32 ParticleIndex);

				BOOL AllSetWindAngle(REAL32 WindAngle);
				BOOL SetWindAngle(INT32 ParticleIndex, REAL32 WindAngle);
				REAL32 GetWindAngle(INT32 ParticleIndex);

				BOOL AllSetWindPower(REAL32 WindPower);
				BOOL SetWindPower(INT32 ParticleIndex, REAL32 WindPower);
				REAL32 GetWindPower(INT32 ParticleIndex);

				BOOL AllSetGravityFlag(BOOL GravityFlag);
				BOOL SetGravityFlag(INT32 ParticleIndex, BOOL GravityFlag);
				BOOL GetGravityFlag(INT32 ParticleIndex);

				BOOL AllSetGravityAngle(REAL32 GravityAngle);
				BOOL SetGravityAngle(INT32 ParticleIndex, REAL32 GravityAngle);
				REAL32 GetGravityAngle(INT32 ParticleIndex);

				BOOL AllSetGravityMaxPower(REAL32 GravityMaxPower);
				BOOL SetGravityMaxPower(INT32 ParticleIndex, REAL32 GravityMaxPower);
				REAL32 GetGravityMaxPower(INT32 ParticleIndex);

				BOOL AllSetGravitySecCount(REAL32 GravitySecCount);
				BOOL SetGravitySecCount(INT32 ParticleIndex, REAL32 GravitySecCount);
				REAL32 GetGravitySecCount(INT32 ParticleIndex);

				BOOL AllreSetGravitySpeed();
				BOOL reSetGravitySpeed(INT32 ParticleIndex);

				BOOL AllSetMagnetFlag(BOOL MagnetFlag);
				BOOL SetMagnetFlag(INT32 ParticleIndex, BOOL MagnetFlag);
				BOOL GetMagnetFlag(INT32 ParticleIndex);

				BOOL AllSetMagnetPos(EPN_Pos MagnetPos);
				BOOL SetMagnetPos(INT32 ParticleIndex, EPN_Pos MagnetPos);
				EPN_Pos GetMagnetPos(INT32 ParticleIndex);

				BOOL AllSetMagnetPower(REAL32 MagnetPower);
				BOOL SetMagnetPower(INT32 ParticleIndex, REAL32 MagnetPower);
				REAL32 GetMagnetPower(INT32 ParticleIndex);

				BOOL AllSetMagnetRadius(REAL32 MagnetRadius);
				BOOL SetMagnetRadius(INT32 ParticleIndex, REAL32 MagnetRadius);
				REAL32 GetMagnetRadius(INT32 ParticleIndex);

				BOOL AllSetParticleStopFlag(BOOL ParticleStopFlag);
				BOOL SetParticleStopFlag(INT32 ParticleIndex, BOOL ParticleStopFlag);
				BOOL GetParticleStopFlag(INT32 ParticleIndex);
			};
		}
	}
}