#pragma once

#include "../../../Common/Common.h"

#include "../Texture/EPN_TextureInterface.h"
#include "../Action/EPN_ActionInterface.h"

namespace EPN
{
	namespace GENERIC
	{
		namespace PARTICLE
		{
			class EPN_Particle
			{
			private:
				enum { MAX_AFTER_BLT_COUNT = 256 };//잔상이 존재할수있는 최대개수 사용자가 수정가능
			private:
				TEXTURE::EPN_TextureInterface* m_pTI;
				ACTION::EPN_ActionInterface* m_pAI;
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private://기본기능
				BOOL m_ParticleDieFlag;//파티클이 살아있으면 FALSE 죽었으면 TRUE (TRUE시 Run 동작안함) 기본값 TRUE
				BOOL m_DeleteCountFlag;//m_DeleteCount 가 0이될시 파티클 자동소멸기능 사용여부
				INT32 m_DeleteCount;//파티클이 살아있을수있는 카운트수

				EPN_Pos m_ParticlePos;//파티클 위치
				EPN_Pos m_ParticleMove;//파티클이 실제 이동하는 힘의 정도

				REAL32 m_MoveAngle;//파티클이 움직일 각도방향
				REAL32 m_MoveSpeed;//파티클이 움직일 속도기준값
				BOOL m_AlterMoveAngleFlag;//파티클 움직일 각도방향 변화기능 사용여부
				REAL32 m_AlterMoveAngle;//매카운트마다 각도방향에 변화줄값
				BOOL m_AlterMoveSpeedFlag;//파티클 움직일 속도 변화기능 사용여부
				REAL32 m_AlterMoveSpeed;//매카운트마다 속도에변화를 줄값

				INT32 m_TextureIndex;//출력할텍스쳐 식별인덱스
				REAL32 m_TextureAngle;//텍스쳐 출력 앵글
				EPN_Scaling m_TextureScaling;//텍스쳐 출력 확대/축소 율

				BOOL m_AlterTextureAngleFlag;//텍스쳐 출력앵글 변화기능 사용여부
				REAL32 m_AlterTextureAngle;//텍스쳐 출력앵글 매카운트마다 변화값
				BOOL m_AlterTextureScalingFlag;//텍스쳐 확대/축소 변화기능 사용여부
				EPN_Scaling m_AlterTextureScaling;//텍스쳐 확대/축소 율 매카운트마다 변화값

				EPN_Rect m_TextureRect;//출력할텍스쳐 사이즈(기본 텍스쳐설정시 이미지전체크기로 설정됨)
				BOOL m_AlterTextureRectFlag;//텍스쳐 출력사이즈 변화기능 사용여부
				EPN_Rect m_AlterTextureRect;//텍스쳐 출력사이즈 매카운트마다 변화값

				EPN_ARGB m_TextureColor;//출력할 텍스쳐 컬러
				BOOL m_AlterTextureColorFlag;//텍스쳐 컬러변화 기능 사용여부
				EPN_ARGB m_AlterTextureColor;//출력할텍스쳐 컬러 매카운트마다 변화값

				BOOL m_SpriteFlag;//스프라이트 기능 사용여부
				INT32 m_SpriteIndex;//사용할 스프라이트 식별인덱스
				INT32 m_SpriteDelay;//애니메이션 넘기는 딜레이기준( 예 : 5일시 5번의 Run실행마다 카운트가 넘어감 )

				BOOL m_ParticleSTOP;//기본 false값 이며 true일시 출력을제외한 모든처리가 정지됩니다

			private://추가기능
				BOOL m_AfterBltFlag;//파티클 잔상효과 사용여부
				INT32 m_AfterImageNum;//파티클 최대잔상개수
				INT32 m_AfterImageCount;//파티클 잔상을 몇카운트마다 생성할것인가
				INT32 m_AfterImageCountRun;
				INT32 m_AfterImageDeleteCount;//잔상이 존재할수있는 카운트(낮을수록 빨리사라지고 높을수록 천천히 투명화됩니다)
				BOOL m_AfterParticleDieFlag[MAX_AFTER_BLT_COUNT];//파티클 잔상소멸여부
				INT32 m_AfterParticleCount;//파티클 잔상 생성개수
				INT32 m_AfterParticleDieCount[MAX_AFTER_BLT_COUNT];//파티클 잔상소멸카운트
				EPN_Pos m_AfterParticlePos[MAX_AFTER_BLT_COUNT];//파티클 잔상위치
				INT32 m_AfterTextureIndex[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 식별자
				REAL32 m_AfterTextureAngle[MAX_AFTER_BLT_COUNT];//파티클 잔상 출력각도
				EPN_Scaling m_AfterTextureScaling[MAX_AFTER_BLT_COUNT];//파티클 잔상 출력 확대/축소 율
				EPN_Rect m_AfterTextureRect[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 출력사이즈
				EPN_ARGB m_AfterTextureColor[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 컬러
				EPN_ARGB m_AfterTextureColorSave[MAX_AFTER_BLT_COUNT];//파티클 잔상 텍스쳐 컬러


				BOOL m_WindUseFlag;//바람부는기능 사용여부
				REAL32 m_WindAngle;//바람이부는각도(0도가 위기준이며 90도일시 바람은 -> 방향으로 불어옵니다)
				REAL32 m_WindPower;//바람의세기

				BOOL m_GravityFlag;//중력적용여부( 자동으로 가속도가 적용됨 )
				REAL32 m_GravityAngle;//중력이 작용하는 방향각도
				REAL32 m_GravityMaxPower;//중력의최대힘
				REAL32 m_GravitySpeed;//현재중력에의한 속도
				REAL32 m_GravitySecCount;//1초의 기준카운트 60이면 60카운트마다 1초로취급
				REAL32 m_GravityCount;

				BOOL m_MagnetFlag;//자력사용여부( 자력이 발생하는 위치에가까울수록 큰힘이 작용합니다 하지만 m_MagnetRadius 가 마이너스값으로 설정되있으면 위치와상관없이 같은힘이 작용합니다 )
				EPN_Pos m_MagnetEPN_Pos;//자력 발생 지점 가까울수록 큰힘이 작용됨
				REAL32 m_MagnetPower;//자력의힘 반대방향으로 적용시키고싶을시 - 값으로 설정하면 됩니다
				REAL32 m_MagnetRadius;//자력의힘이 영향을미칠 원의범위에대한 반지름길이(범위없이 무조건 작용한다면 마이너스값 셋팅)

			public:
				EPN_Particle();
				~EPN_Particle();

			public://..Run
				BOOL Run(EPN_Pos Pos = EPN_Pos(0, 0));
				BOOL Clear();
			public:

				BOOL SetParticleDieFlag(BOOL ParticleDieFlag);
				BOOL GetParticleDieFlag();

				BOOL SetDeleteCountFlag(BOOL DeleteCountFlag);
				BOOL GetDeleteCountFlag();

				BOOL SetDeleteCount(INT32 DeleteCount);
				INT32 GetDeleteCount();

				BOOL SetParticlePos(EPN_Pos ParticlePos);
				EPN_Pos GetParticlePos();

				BOOL SetParticleMove(EPN_Pos ParticleMove);
				EPN_Pos GetParticleMove();

				BOOL SetMoveAngle(REAL32 MoveAngle);
				REAL32 GetMoveAngle();

				BOOL SetMoveSpeed(REAL32 MoveSpeed);
				REAL32 GetMoveSpeed();

				BOOL SetAlterMoveAngleFlag(BOOL AlterMoveAngleFlag);
				BOOL GetAlterMoveAngleFlag();

				BOOL SetAlterMoveAngle(REAL32 AlterMoveAngle);
				REAL32 GetAlterMoveAngle();

				BOOL SetAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag);
				BOOL GetAlterMoveSpeedFlag();

				BOOL SetAlterMoveSpeed(REAL32 AlterMoveSpeed);
				REAL32 GetAlterMoveSpeed();

				BOOL SetTextureIndex(INT32 TextureIndex);
				INT32 GetTextureIndex();

				BOOL SetTextureAngle(REAL32 TextureAngle);
				REAL32 GetTextureAngle();

				BOOL SetTextureScaling(EPN_Scaling TextureScaling);
				EPN_Scaling GetTextureScaling();

				BOOL SetAlterTextureAngleFlag(BOOL AlterTextureAngleFlag);
				BOOL GetAlterTextureAngleFlag();

				BOOL SetAlterTextureAngle(REAL32 AlterTextureAngle);
				REAL32 GetAlterTextureAngle();

				BOOL SetAlterTextureScalingFlag(BOOL AlterTextureScalingFlag);
				BOOL GetAlterTextureScalingFlag();

				BOOL SetAlterTextureScaling(EPN_Scaling AlterTextureScaling);
				EPN_Scaling GetAlterTextureScaling();

				BOOL SetTextureRect(EPN_Rect TextureRect);
				EPN_Rect GetTextureRect();

				BOOL SetAlterTextureRectFlag(BOOL AlterTextureRectFlag);
				BOOL GetAlterTextureRectFlag();

				BOOL SetAlterTextureRect(EPN_Rect AlterTextureRect);
				EPN_Rect GetAlterTextureRect();

				BOOL SetTextureColor(EPN_ARGB TextureColor);
				EPN_ARGB GetTextureColor();

				BOOL SetAlterTextureColorFlag(BOOL AlterTextureColorFlag);
				BOOL GetAlterTextureColorFlag();

				BOOL SetAlterTextureColor(EPN_ARGB AlterTextureColor);
				EPN_ARGB GetAlterTextureColor();



				BOOL SetSpriteFlag(BOOL SpriteFlag);
				BOOL GetSpriteFlag();

				BOOL SetSpriteIndex(INT32 SpriteIndex);
				INT32 GetSpriteIndex();

				BOOL SetSpriteDelay(INT32 SpriteDelay);
				INT32 GetSpriteDelay();


				BOOL SetAfterBltFlag(BOOL AfterBltFlag);
				BOOL GetAfterBltFlag();

				BOOL SetAfterImageNum(INT32 AfterBltFlag);
				INT32 GetAfterImageNum();

				BOOL SetAfterImageCount(INT32 AfterImageCount);
				INT32 GetAfterImageCount();

				BOOL SetAfterImageDeleteCount(INT32 AfterImageDeleteCount);
				INT32 GetAfterImageDeleteCount();


				BOOL SetWindUseFlag(BOOL WindUseFlag);
				BOOL GetWindUseFlag();

				BOOL SetWindAngle(REAL32 WindAngle);
				REAL32 GetWindAngle();

				BOOL SetWindPower(REAL32 WindPower);
				REAL32 GetWindPower();

				BOOL SetGravityFlag(BOOL GravityFlag);
				BOOL GetGravityFlag();

				BOOL SetGravityAngle(REAL32 GravityAngle);
				REAL32 GetGravityAngle();

				BOOL SetGravityMaxPower(REAL32 GravityMaxPower);
				REAL32 GetGravityMaxPower();

				BOOL SetGravitySecCount(REAL32 GravitySecCount);
				REAL32 GetGravitySecCount();

				BOOL ResetGravitySpeed();

				BOOL SetMagnetFlag(BOOL MagnetFlag);
				BOOL GetMagnetFlag();

				BOOL SetMagnetPos(EPN_Pos MagnetEPN_Pos);
				EPN_Pos GetMagnetPos();

				BOOL SetMagnetPower(REAL32 MagnetPower);
				REAL32 GetMagnetPower();

				BOOL SetMagnetRadius(REAL32 MagnetRadius);
				REAL32 GetMagnetRadius();

				BOOL SetParticleStopFlag(BOOL ParticleStopFlag);
				BOOL GetIsParticleStop();
			};
		}
	}
}
