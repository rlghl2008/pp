#pragma once

#include "EPN_ParticleManager.h"
#include "../../../Common/Class/EPN_MemoryPool.hpp"

namespace EPN
{
	namespace GENERIC
	{
		namespace PARTICLE
		{
			class EPN_ParticleInterface
			{
			public:
				EPN_ParticleInterface();
				~EPN_ParticleInterface();
			private:
				BYTE m_LogFlag;
			public:
				VOID		SetLogFlag(BYTE LogFlag);
				BYTE		GetLogFlag();
			private:
				EPN::COMMON::EPN_MemoryPool<EPN_ParticleManager>		m_mpParticleMgr;
				EPN::COMMON::EPN_StringIndexPool			m_StringIndexPool;
			public://EPN_StringIndexPool
				INT32 GetManagerIndex(estring SaveUImgrName);
				estring GetManagerStr(INT32 UImgrIndex);
			public:
				INT32 AddParticleManager(INT32 MaxParticleNum);
				INT32 AddParticleManager(estring SaveParticleMgrName, INT32 MaxParticleNum);

				//기본값시 사용하는 파티클매니저 전부Run, Pos 는 파티클 출력위치 기준
				BOOL Run(INT32 ParticleMgrIndex = -1, EPN_Pos Pos = EPN_Pos(0, 0));
				BOOL Run(estring SaveParticleMgrName, EPN_Pos Pos = EPN_Pos(0, 0));
			public:
				//현재 인터페이스에 Add되어 메모리에 올라가있는 ParticleManager 개수 반환
				INT32 GetAddDataCount();

				BOOL AllDeleteManager();
				BOOL DeleteManager(INT32 ParticleMgrIndex);
				BOOL DeleteManager(estring SaveParticleMgrName);

				BOOL AllClearManager();
				BOOL ClearManager(INT32 ParticleMgrIndex);
				BOOL ClearManager(estring SaveParticleMgrName);

				//인덱스로 찾아낸  가져오기
				EPN_ParticleManager* GetManager(INT32 ParticleMgrIndex);
				//스트링으로 찾아낸 파티클매니저 가져오기
				EPN_ParticleManager* GetManager(estring ParticleMgrName);

			public://매니저기능
				//인덱스에해당하는 매니저에대해 최대 파티클 개수 설정 
				BOOL AllSetMaxParticleNum(INT32 ParticleNum);
				BOOL SetMaxParticleNum(INT32 ParticleMgrIndex, INT32 ParticleNum);
				BOOL SetMaxParticleNum(estring ParticleMgrName, INT32 ParticleNum);
				//인덱스에해당하는 매니저에대해 설정된 최대 파티클 개수 반환
				BOOL GetMaxParticleNum(INT32 ParticleMgrIndex);
				BOOL GetMaxParticleNum(estring ParticleMgrName);
				//인덱스에해당하는 매니저에대해 최대 파티클 개수 재설정
				BOOL AllreSetMaxParticleNum(INT32 ParticleNum);
				BOOL ResetMaxParticleNum(INT32 ParticleMgrIndex, INT32 ParticleNum);
				BOOL ResetMaxParticleNum(estring ParticleMgrName, INT32 ParticleNum);
				//인덱스에해당하는 매니저에대해 설정된 파티클중 죽은파티클번호 찾아 재설정하고 해당 파티클인덱스를 반환 죽은파티클이없을시 -1반환
				INT32 AddParticle(INT32 ParticleMgrIndex);
				INT32 AddParticle(estring ParticleMgrName);

			private:
				static EPN_ParticleInterface* m_pParticleInterface;
			public:
				static EPN_ParticleInterface* GetInstance();
				static EPN_ParticleInterface* CreateInstance();
				static VOID ReleaseInstance();
			};
		}
	}
}