#include "EPN_Particle.h"



#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::PARTICLE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::ACTION;
#pragma endregion


EPN_Particle :: EPN_Particle()
{
	m_pTI = EPN_TextureInterface :: CreateInstance();
	m_pAI = EPN_ActionInterface :: CreateInstance();

	m_ParticleDieFlag = TRUE;
	m_DeleteCountFlag = FALSE;
	m_DeleteCount = 0;

	m_ParticlePos = 0;
	m_ParticleMove = 0;
	m_MoveAngle = 0;
	m_MoveSpeed = 0;
	m_AlterMoveAngleFlag = FALSE;
	m_AlterMoveAngle = 0;
	m_AlterMoveSpeedFlag = FALSE;
	m_AlterMoveSpeed = 0;

	m_TextureIndex = -1;
	m_TextureAngle = 0;
	m_TextureScaling.X = 1;
	m_TextureScaling.Y = 1;
	m_AlterTextureAngleFlag = FALSE;
	m_AlterTextureAngle = 0;
	m_AlterTextureScalingFlag = FALSE;
	m_AlterTextureScaling = 0;

	m_TextureRect = 0;
	m_AlterTextureRectFlag = FALSE;
	m_AlterTextureRect = 0;

	m_TextureColor = 0;
	m_AlterTextureColorFlag = FALSE;
	m_AlterTextureColor = 0;

	m_SpriteFlag = FALSE;
	m_SpriteIndex = -1;
	m_SpriteDelay = 0;



	m_AfterBltFlag = FALSE;
	m_AfterImageNum = 0;
	m_AfterImageCount = 0;
	m_AfterImageCountRun = 0;
	m_AfterImageDeleteCount = 0;
	memset(m_AfterParticleDieFlag,0,sizeof(m_AfterParticleDieFlag));
	m_AfterParticleCount = 0;
	memset(m_AfterParticleDieCount,0,sizeof(m_AfterParticleDieCount));
	memset(m_AfterParticlePos,0,sizeof(m_AfterParticlePos));
	memset(m_AfterTextureIndex,-1,sizeof(m_AfterTextureIndex));
	memset(m_AfterTextureAngle,0,sizeof(m_AfterTextureAngle));
	memset(m_AfterTextureScaling,0,sizeof(m_AfterTextureScaling));
	memset(m_AfterTextureRect,0,sizeof(m_AfterTextureRect));
	memset(m_AfterTextureColor,0,sizeof(m_AfterTextureColor));
	memset(m_AfterTextureColorSave,0,sizeof(m_AfterTextureColorSave));

	m_WindUseFlag = FALSE;
	m_WindAngle = 0;
	m_WindPower = 0;

	m_GravityFlag = FALSE;
	m_GravityAngle = 0;
	m_GravityMaxPower = 0;
	m_GravitySecCount = 0;
	m_GravityCount = 0;
	m_GravitySpeed = 0.1f;

	m_MagnetFlag = FALSE;
	m_MagnetEPN_Pos = 0;
	m_MagnetPower = 0;
	m_MagnetRadius = 0;

	m_ParticleSTOP = FALSE;
}

EPN_Particle :: ~EPN_Particle()
{
}
BOOL EPN_Particle :: Clear()
{
	m_ParticleDieFlag = TRUE;
	m_DeleteCountFlag = FALSE;
	m_DeleteCount = 0;

	m_ParticlePos = 0;
	m_ParticleMove = 0;
	m_MoveAngle = 0;
	m_MoveSpeed = 0;
	m_AlterMoveAngleFlag = FALSE;
	m_AlterMoveAngle = 0;
	m_AlterMoveSpeedFlag = FALSE;
	m_AlterMoveSpeed = 0;

	m_TextureIndex = -1;
	m_TextureAngle = 0;
	m_TextureScaling.X = 1;
	m_TextureScaling.Y = 1;
	m_AlterTextureAngleFlag = FALSE;
	m_AlterTextureAngle = 0;
	m_AlterTextureScalingFlag = FALSE;
	m_AlterTextureScaling = 0;

	m_TextureRect = 0;
	m_AlterTextureRectFlag = FALSE;
	m_AlterTextureRect = 0;

	m_TextureColor = 0;
	m_AlterTextureColorFlag = FALSE;
	m_AlterTextureColor = 0;

	m_SpriteFlag = FALSE;
	m_SpriteIndex = -1;
	m_SpriteDelay = 0;



	m_AfterBltFlag = FALSE;
	m_AfterImageNum = 0;
	m_AfterImageCount = 0;
	m_AfterImageCountRun = 0;
	m_AfterImageDeleteCount = 0;
	memset(m_AfterParticleDieFlag,0,sizeof(m_AfterParticleDieFlag));
	m_AfterParticleCount = 0;
	memset(m_AfterParticleDieCount,0,sizeof(m_AfterParticleDieCount));
	memset(m_AfterParticlePos,0,sizeof(m_AfterParticlePos));
	memset(m_AfterTextureIndex,-1,sizeof(m_AfterTextureIndex));
	memset(m_AfterTextureAngle,0,sizeof(m_AfterTextureAngle));
	memset(m_AfterTextureScaling,0,sizeof(m_AfterTextureScaling));
	memset(m_AfterTextureRect,0,sizeof(m_AfterTextureRect));
	memset(m_AfterTextureColor,0,sizeof(m_AfterTextureColor));
	memset(m_AfterTextureColorSave,0,sizeof(m_AfterTextureColorSave));

	m_WindUseFlag = FALSE;
	m_WindAngle = 0;
	m_WindPower = 0;

	m_GravityFlag = FALSE;
	m_GravityAngle = 0;
	m_GravityMaxPower = 0;
	m_GravitySecCount = 0;
	m_GravityCount = 0;
	m_GravitySpeed = 0.1f;

	m_MagnetFlag = FALSE;
	m_MagnetEPN_Pos = 0;
	m_MagnetPower = 0;
	m_MagnetRadius = 0;
	return TRUE;
}

BOOL EPN_Particle :: Run(EPN_Pos Pos)
{
	if(TRUE==m_ParticleDieFlag)
		return FALSE;

	if(TRUE == m_DeleteCountFlag && FALSE==m_ParticleSTOP)
	{
		if( 0 >= m_DeleteCount )
		{
			m_DeleteCount = 0;
			m_ParticleDieFlag = TRUE;
			return FALSE;
		}
		--m_DeleteCount;
	}
	if(0 >= m_TextureIndex )
	{	
		EPN_Rect rect = m_TextureRect;
		if(TRUE==m_SpriteFlag && -1 < m_SpriteIndex  && FALSE == m_ParticleSTOP)
		{
			EPN_ActionData* pActionData = m_pAI->Action(m_SpriteIndex,m_SpriteDelay);
			if(NULL_PTR != pActionData)
				rect = pActionData->Rect;
		}
		if(TRUE == m_AfterBltFlag && FALSE == m_ParticleSTOP)
		{
			INT32 BltCount = 0;
			BOOL BltFlag = false;
			BOOL AddFlag = false;
			if(m_AfterImageCountRun >= m_AfterImageCount)
			{
				if(m_AfterImageNum > m_AfterParticleCount)
					AddFlag = true;
				m_AfterImageCountRun = 0;
			}
			else
			 ++m_AfterImageCountRun;

			if(TRUE==AddFlag)
			{
				for(INT32 i = 0;i < MAX_AFTER_BLT_COUNT;++i)
				{

					if(FALSE==m_AfterParticleDieFlag[i])
					{
						m_AfterParticleDieFlag[i] = true;
						m_AfterTextureIndex[i] = m_TextureIndex;
						m_AfterParticlePos[i] = m_ParticlePos;
						m_AfterTextureRect[i] = rect;
						m_AfterTextureColor[i] = m_TextureColor;
						m_AfterTextureColorSave[i] = m_TextureColor;
						m_AfterTextureAngle[i] = m_TextureAngle;
						m_AfterTextureScaling[i] = m_TextureScaling;
						m_AfterParticleDieCount[i] = m_AfterImageDeleteCount;
						m_AfterParticleCount++;
						break;
					}

				}
			}
			for(INT32 i = MAX_AFTER_BLT_COUNT-1;i >= 0;--i)
			{
				
				if(FALSE==BltFlag)
				{
					if(BltCount >= m_AfterParticleCount)
					{
						break;
					}
					else if(TRUE == m_AfterParticleDieFlag[i])
					{
						m_pTI->Blt(m_AfterTextureIndex[i], Pos+m_AfterParticlePos[i],m_AfterTextureRect[i],m_AfterTextureColor[i],m_AfterTextureAngle[i],m_AfterTextureScaling[i]);
						
						if(0 > m_AfterParticleDieCount[i]-- )
						{
							m_AfterParticleDieFlag[i] = false;
							m_AfterParticleDieCount[i] = 0;
							m_AfterTextureIndex[i] = -1;
							m_AfterParticlePos[i] = 0;
							m_AfterTextureRect[i] = 0;
							m_AfterTextureColor[i] = 0;
							m_AfterTextureAngle[i] = 0;
							m_AfterTextureScaling[i] = 0;
							m_AfterParticleDieCount[i] = 0;
							m_AfterTextureColorSave[i] = 0;
							m_AfterParticleCount--;
						}
						else
						{
							m_AfterTextureColor[i].Alpha = (m_AfterTextureColorSave[i].Alpha*((REAL32)m_AfterParticleDieCount[i] / m_AfterImageDeleteCount));
							if( 255 < m_AfterTextureColor[i].Alpha)
								m_AfterTextureColor[i].Alpha =255;
							else if( 0 > m_AfterTextureColor[i].Alpha )
								m_AfterTextureColor[i].Alpha = 0;
							++BltCount;
						}
					}
				}
			}
			
		}
		m_pTI->Blt(m_TextureIndex, Pos+m_ParticlePos,rect,m_TextureColor,m_TextureAngle,m_TextureScaling);
	}
	
	

	if( 0 != m_MoveSpeed  && FALSE == m_ParticleSTOP)
	{
		EPN_Pos TempMove = (EPN_GetAnglePos(m_MoveAngle) * EPN_Pos(m_MoveSpeed,m_MoveSpeed));
		if(m_ParticleMove.X > TempMove.X)
		{
			if(m_ParticleMove.X-TempMove.X < TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X -= (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		else if(m_ParticleMove.X < TempMove.X)
		{
			if(m_ParticleMove.X+TempMove.X > TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X += (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		if(m_ParticleMove.Y > TempMove.Y)
		{
			if(m_ParticleMove.Y-TempMove.Y < TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y -= (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}
		else if(m_ParticleMove.Y < TempMove.Y)
		{
			if(m_ParticleMove.Y+TempMove.X > TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y += (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}
	}

	if(TRUE == m_WindUseFlag && FALSE == m_ParticleSTOP)
	{
		EPN_Pos TempMove = EPN_GetAnglePos(m_WindAngle) * EPN_Pos(m_WindPower,m_WindPower);
		if(m_ParticleMove.X > TempMove.X)
		{
			if(m_ParticleMove.X-TempMove.X < TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X -= (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		else if(m_ParticleMove.X < TempMove.X)
		{
			if(m_ParticleMove.X+TempMove.X > TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X += (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		if(m_ParticleMove.Y > TempMove.Y)
		{
			if(m_ParticleMove.Y-TempMove.Y < TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y -= (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}
		else if(m_ParticleMove.Y < TempMove.Y)
		{
			if(m_ParticleMove.Y+TempMove.X > TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y += (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}
	}
	if(TRUE == m_GravityFlag && FALSE == m_ParticleSTOP)
	{
		EPN_Pos TempMove = EPN_GetAnglePos(m_GravityAngle) * EPN_Pos(m_GravitySpeed,m_GravitySpeed); 
		if(m_ParticleMove.X > TempMove.X)
		{
			if(m_ParticleMove.X-TempMove.X < TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X -= (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		else if(m_ParticleMove.X < TempMove.X)
		{
			if(m_ParticleMove.X+TempMove.X > TempMove.X)
			{
				m_ParticleMove.X = TempMove.X;
			}
			else
			{
				m_ParticleMove.X += ((0 < TempMove.X ) ? TempMove.X : -TempMove.X);
			}
		}
		if(m_ParticleMove.Y > TempMove.Y)
		{
			if(m_ParticleMove.Y-TempMove.Y < TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y -= (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}
		else if(m_ParticleMove.Y < TempMove.Y)
		{
			if(m_ParticleMove.Y+TempMove.X > TempMove.Y)
			{
				m_ParticleMove.Y = TempMove.Y;
			}
			else
			{
				m_ParticleMove.Y += (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
			}
		}

			m_GravitySpeed +=  (m_GravitySpeed * 0.98f)/m_GravitySecCount;
			if(m_GravitySpeed > m_GravityMaxPower)
				m_GravitySpeed = m_GravityMaxPower;
			m_GravityCount = 0;


	}

	if( TRUE == m_MagnetFlag && 0 != m_MagnetPower && FALSE == m_ParticleSTOP)
	{
		if(0.f < m_MagnetRadius )
		{
			REAL32 Distance = EPN_GetDistance(m_MagnetEPN_Pos,m_ParticlePos);
			if(Distance < m_MagnetRadius)
			{
				EPN_Pos TempMove = (EPN_GetAnglePos((EPN_GetPosAngle(m_ParticlePos,m_MagnetEPN_Pos))) * EPN_Pos((m_MagnetPower * (1.0f - (Distance / m_MagnetRadius))),(m_MagnetPower * (1.0f - (Distance / m_MagnetRadius)))));
				
				if(m_ParticleMove.X > TempMove.X)
				{
					if(m_ParticleMove.X - TempMove.X < TempMove.X)
					{
						m_ParticleMove.X = TempMove.X;
					}
					else
					{
						m_ParticleMove.X -= ((0 < TempMove.X ) ? TempMove.X : -TempMove.X);
					}
				}
				else if(m_ParticleMove.X < TempMove.X)
				{
					if(m_ParticleMove.X+TempMove.X > TempMove.X)
					{
						m_ParticleMove.X = TempMove.X;
					}
					else
					{
						m_ParticleMove.X += ((0 < TempMove.X ) ? TempMove.X : -TempMove.X);
					}
				}
				
				if(m_ParticleMove.Y > TempMove.Y)
				{
					if(m_ParticleMove.Y-TempMove.Y < TempMove.Y)
					{
						m_ParticleMove.Y = TempMove.Y;
					}
					else
					{
						m_ParticleMove.Y -= ((0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
					}
				}
				else if(m_ParticleMove.Y < TempMove.Y)
				{
					if(m_ParticleMove.Y+TempMove.Y > TempMove.Y)
					{
						m_ParticleMove.Y = TempMove.Y;
					}
					else
					{
						m_ParticleMove.Y += ((0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
					}
				}
			}
		}
		else
		{
			EPN_Pos TempMove = EPN_GetAnglePos((EPN_GetPosAngle(m_ParticlePos,m_MagnetEPN_Pos))) * EPN_Pos(m_MagnetPower,m_MagnetPower);
			if(m_ParticleMove.X > TempMove.X)
			{
				if(m_ParticleMove.X-TempMove.X < TempMove.X)
				{
					m_ParticleMove.X = TempMove.X;
				}
				else
				{
					m_ParticleMove.X -= (( 0 < TempMove.X ) ? TempMove.X : -TempMove.X);
				}
			}
			else if(m_ParticleMove.X < TempMove.X)
			{
				if(m_ParticleMove.X+TempMove.X > TempMove.X)
				{
					m_ParticleMove.X = TempMove.X;
				}
				else
				{
					m_ParticleMove.X += ((0 < TempMove.X ) ? TempMove.X : -TempMove.X);
				}
			}
			if(m_ParticleMove.Y > TempMove.Y)
			{
				if(m_ParticleMove.Y-TempMove.Y < TempMove.Y)
				{
					m_ParticleMove.Y = TempMove.Y;
				}
				else
				{
					m_ParticleMove.Y -= (( 0 < TempMove.Y ) ? TempMove.Y : -TempMove.Y);
				}
			}
			else if(m_ParticleMove.Y < TempMove.Y)
			{
				if(m_ParticleMove.Y+TempMove.X > TempMove.Y)
				{
					m_ParticleMove.Y = TempMove.Y;
				}
				else
				{
					m_ParticleMove.Y += (( 0 < TempMove.Y) ? TempMove.Y : -TempMove.Y);
				}
			}
		}
	}

	if(TRUE == m_AlterMoveAngleFlag && FALSE == m_ParticleSTOP)
	{
		m_TextureAngle += m_AlterMoveAngle;
	}
	if(TRUE == m_AlterMoveSpeedFlag && FALSE == m_ParticleSTOP)
	{
		m_MoveSpeed += m_AlterMoveSpeed;
	}
	if(TRUE == m_AlterTextureColorFlag && FALSE == m_ParticleSTOP)
	{
		m_TextureColor += m_AlterTextureColor;
		if( 255 < m_TextureColor.Alpha )
			m_TextureColor.Alpha = 255;
		else if( 0 > m_TextureColor.Alpha )
			m_TextureColor.Alpha = 0;

		if( 255 < m_TextureColor.Red )
			m_TextureColor.Red = 255;
		else if( 0 > m_TextureColor.Red)
			m_TextureColor.Red = 0;

		if( 255 < m_TextureColor.Blue )
			m_TextureColor.Blue = 255;
		else if( 0 > m_TextureColor.Blue )
			m_TextureColor.Blue = 0;

		if( 255 < m_TextureColor.Green)
			m_TextureColor.Green = 255;
		else if( 0 > m_TextureColor.Green )
			m_TextureColor.Green = 0;
	}
	if( TRUE == m_AlterTextureScalingFlag && FALSE == m_ParticleSTOP)
	{
		m_TextureScaling += m_AlterTextureScaling;
	}
	if( TRUE == m_AlterTextureAngleFlag && FALSE == m_ParticleSTOP)
	{
		m_TextureAngle += m_AlterTextureAngle;
	}
	if( TRUE == m_AlterTextureRectFlag && FALSE == m_ParticleSTOP)
	{
		m_TextureRect += m_AlterTextureRect;
	}

	if(FALSE == m_ParticleSTOP)
		m_ParticlePos += m_ParticleMove;

	return TRUE;
}



VOID EPN_Particle::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_Particle::GetLogFlag()
{
	return m_LogFlag;
}


BOOL EPN_Particle :: SetParticleDieFlag(BOOL ParticleDieFlag)
{
	m_ParticleDieFlag = ParticleDieFlag;
	return TRUE; 
}
BOOL EPN_Particle :: GetParticleDieFlag()
{
	return m_ParticleDieFlag;
}

BOOL EPN_Particle :: SetDeleteCountFlag(BOOL DeleteCountFlag)
{
	m_DeleteCountFlag = DeleteCountFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetDeleteCountFlag()
{
	return m_DeleteCountFlag;
}

BOOL EPN_Particle :: SetDeleteCount(INT32 DeleteCount)
{
	m_DeleteCount = DeleteCount;
	return TRUE;
}

INT32 EPN_Particle :: GetDeleteCount()
{
	return m_DeleteCount;
}

BOOL EPN_Particle :: SetParticlePos(EPN_Pos ParticlePos)
{
	m_ParticlePos = ParticlePos;
	return TRUE;
}

EPN_Pos EPN_Particle :: GetParticlePos()
{
	return m_ParticlePos;
}

BOOL EPN_Particle :: SetParticleMove(EPN_Pos ParticleMove)
{
	m_ParticleMove = ParticleMove;
	return TRUE;
}
EPN_Pos EPN_Particle :: GetParticleMove()
{
	return m_ParticleMove;
}

BOOL EPN_Particle :: SetMoveAngle(REAL32 MoveAngle)
{
	m_MoveAngle = MoveAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetMoveAngle()
{
	return m_MoveAngle;
}

BOOL EPN_Particle :: SetMoveSpeed(REAL32 MoveSpeed)
{
	m_MoveSpeed = MoveSpeed;
	return TRUE;
}

REAL32 EPN_Particle :: GetMoveSpeed()
{
	return m_MoveSpeed;
}

BOOL EPN_Particle :: SetAlterMoveAngleFlag(BOOL AlterMoveAngleFlag)
{
	m_AlterMoveAngleFlag = AlterMoveAngleFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetAlterMoveAngleFlag()
{
	return m_AlterMoveAngleFlag;
}

BOOL EPN_Particle :: SetAlterMoveAngle(REAL32 AlterMoveAngle)
{
	m_AlterMoveAngle = AlterMoveAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetAlterMoveAngle()
{
	return m_AlterMoveAngle;
}

BOOL EPN_Particle :: SetAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag)
{
	m_AlterMoveSpeedFlag = AlterMoveSpeedFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetAlterMoveSpeedFlag()
{
	return m_AlterMoveSpeedFlag;
}

BOOL EPN_Particle :: SetAlterMoveSpeed(REAL32 AlterMoveSpeed)
{
	m_AlterMoveSpeed = AlterMoveSpeed;
	return TRUE;
}

REAL32 EPN_Particle :: GetAlterMoveSpeed()
{
	return m_AlterMoveSpeed;
}

BOOL EPN_Particle :: SetTextureIndex(INT32 TextureIndex)
{
	if( 0 <= m_TextureIndex )
	{
		m_TextureIndex = TextureIndex;
		m_TextureRect = m_pTI->GetTextureSize(m_TextureIndex);
		return TRUE;
	}
	PRINT_LOG(m_LogFlag, "( 잘못된 m_TextureIndex 값 입니다. )\n", TextureIndex);

	return FALSE;
}

INT32 EPN_Particle :: GetTextureIndex()
{
	return m_TextureIndex;
}

BOOL EPN_Particle :: SetTextureAngle(REAL32 TextureAngle)
{
	m_TextureAngle = TextureAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetTextureAngle()
{
	return m_TextureAngle;
}

BOOL EPN_Particle :: SetTextureScaling(EPN_Scaling TextureScaling)
{
	m_TextureScaling = TextureScaling;
	return TRUE;
}

EPN_Scaling EPN_Particle :: GetTextureScaling()
{
	return m_TextureScaling;
}

BOOL EPN_Particle :: SetAlterTextureAngleFlag(BOOL AlterTextureAngleFlag)
{
	m_AlterTextureAngleFlag = AlterTextureAngleFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetAlterTextureAngleFlag()
{
	return m_AlterTextureAngleFlag;
}

BOOL EPN_Particle :: SetAlterTextureAngle(REAL32 AlterTextureAngle)
{
	m_AlterTextureAngle = AlterTextureAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetAlterTextureAngle()
{
	return m_AlterTextureAngle;
}

BOOL EPN_Particle :: SetAlterTextureScalingFlag(BOOL AlterTextureScalingFlag)
{
	m_AlterTextureScalingFlag = AlterTextureScalingFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetAlterTextureScalingFlag()
{
	return m_AlterTextureScalingFlag;
}

BOOL EPN_Particle :: SetAlterTextureScaling(EPN_Scaling AlterTextureScaling)
{
	m_AlterTextureScaling = AlterTextureScaling;
	return TRUE;
}

EPN_Scaling EPN_Particle :: GetAlterTextureScaling()
{
	return m_AlterTextureScaling;
}

BOOL EPN_Particle :: SetTextureRect(EPN_Rect TextureRect)
{
	m_TextureRect = TextureRect;
	return TRUE;
}

EPN_Rect EPN_Particle :: GetTextureRect()
{
	return m_TextureRect;
}

BOOL EPN_Particle :: SetAlterTextureRectFlag(BOOL AlterTextureRectFlag)
{
	m_AlterTextureRectFlag = AlterTextureRectFlag;
	return TRUE;
}
BOOL EPN_Particle :: GetAlterTextureRectFlag()
{
	return m_AlterTextureRectFlag;
}

BOOL EPN_Particle :: SetAlterTextureRect(EPN_Rect AlterTextureRect)
{
	m_AlterTextureRect = AlterTextureRect;
	return TRUE;
}
EPN_Rect EPN_Particle :: GetAlterTextureRect()
{
	return m_AlterTextureRect;
}


BOOL EPN_Particle :: SetTextureColor(EPN_ARGB TextureColor)
{
	m_TextureColor = TextureColor;
	return TRUE;
}

EPN_ARGB EPN_Particle :: GetTextureColor()
{
	return m_TextureColor;
}

BOOL EPN_Particle :: SetAlterTextureColorFlag(BOOL AlterTextureColorFlag)
{
	m_AlterTextureColorFlag = AlterTextureColorFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetAlterTextureColorFlag()
{
	return m_AlterTextureColorFlag;
}

BOOL EPN_Particle :: SetAlterTextureColor(EPN_ARGB AlterTextureColor)
{
	m_AlterTextureColor = AlterTextureColor;
	return TRUE;
}

EPN_ARGB EPN_Particle :: GetAlterTextureColor()
{
	return m_AlterTextureColor;
}

BOOL EPN_Particle :: SetSpriteFlag(BOOL SpriteFlag)
{
	m_SpriteFlag = SpriteFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetSpriteFlag()
{
	return m_SpriteFlag;
}

BOOL EPN_Particle :: SetSpriteIndex(INT32 SpriteIndex)
{
	m_SpriteIndex = SpriteIndex;
	return TRUE;
}

INT32 EPN_Particle :: GetSpriteIndex()
{
	return m_SpriteIndex;
}

BOOL EPN_Particle :: SetSpriteDelay(INT32 SpriteDelay)
{
	m_SpriteDelay = SpriteDelay;
	return TRUE;
}

INT32 EPN_Particle :: GetSpriteDelay()
{
	return m_SpriteDelay;
}

BOOL EPN_Particle :: SetAfterBltFlag(BOOL AfterBltFlag)
{
	m_AfterBltFlag = AfterBltFlag;
	return TRUE;
}
BOOL EPN_Particle :: GetAfterBltFlag()
{
	return m_AfterBltFlag;
}

BOOL EPN_Particle :: SetAfterImageNum(INT32 AfterImageNum)
{
	if(MAX_AFTER_BLT_COUNT <= AfterImageNum)
		m_AfterImageNum = MAX_AFTER_BLT_COUNT;
	else
		m_AfterImageNum = m_AfterImageNum;
	return TRUE;
}
INT32 EPN_Particle :: GetAfterImageNum()
{
	return m_AfterBltFlag;
}

BOOL EPN_Particle :: SetAfterImageCount(INT32 AfterImageCount)
{
	m_AfterImageCount = AfterImageCount;
	return TRUE;
}
INT32 EPN_Particle :: GetAfterImageCount()
{
	return m_AfterImageCount;
}

BOOL EPN_Particle :: SetAfterImageDeleteCount(INT32 AfterImageDeleteCount)
{
	m_AfterImageDeleteCount = AfterImageDeleteCount;
	return TRUE;
}
INT32 EPN_Particle :: GetAfterImageDeleteCount()
{
	return m_AfterImageDeleteCount;
}

BOOL EPN_Particle :: SetWindUseFlag(BOOL WindUseFlag)
{
	m_WindUseFlag = WindUseFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetWindUseFlag()
{
	return m_WindUseFlag;
}

BOOL EPN_Particle :: SetWindAngle(REAL32 WindAngle)
{
	m_WindAngle = WindAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetWindAngle()
{
	return m_WindAngle;
}

BOOL EPN_Particle :: SetWindPower(REAL32 WindPower)
{
	m_WindPower = WindPower;
	return TRUE;
}

REAL32 EPN_Particle :: GetWindPower()
{
	return m_WindPower;
}

BOOL EPN_Particle :: SetGravityFlag(BOOL GravityFlag)
{
	m_GravityFlag = GravityFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetGravityFlag()
{
	return m_GravityFlag;
}

BOOL EPN_Particle :: SetGravityAngle(REAL32 GravityAngle)
{
	m_GravityAngle = GravityAngle;
	return TRUE;
}

REAL32 EPN_Particle :: GetGravityAngle()
{
	return m_GravityAngle;
}

BOOL EPN_Particle :: SetGravityMaxPower(REAL32 GravityMaxPower)
{
	m_GravityMaxPower = GravityMaxPower;
	return TRUE;
}

REAL32 EPN_Particle :: GetGravityMaxPower()
{
	return m_GravityMaxPower;
}

BOOL EPN_Particle :: SetGravitySecCount(REAL32 GravitySecCount)
{
	m_GravityCount = 0;
	m_GravitySecCount = GravitySecCount;
	return TRUE;
}
REAL32 EPN_Particle :: GetGravitySecCount()
{
	return m_GravitySecCount;
}

BOOL EPN_Particle :: ResetGravitySpeed()
{
	m_GravitySpeed = 0.1f;
	return TRUE;
}

BOOL EPN_Particle :: SetMagnetFlag(BOOL MagnetFlag)
{
	m_MagnetFlag = MagnetFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetMagnetFlag()
{
	return m_MagnetFlag;
}

BOOL EPN_Particle :: SetMagnetPos(EPN_Pos MagnetEPN_Pos)
{
	m_MagnetEPN_Pos = MagnetEPN_Pos;
	return TRUE;
}

EPN_Pos EPN_Particle :: GetMagnetPos()
{
	return m_MagnetEPN_Pos;
}

BOOL EPN_Particle :: SetMagnetPower(REAL32 MagnetPower)
{
	m_MagnetPower = MagnetPower;
	return TRUE;
}

REAL32 EPN_Particle :: GetMagnetPower()
{
	return m_MagnetPower;
}

BOOL EPN_Particle :: SetMagnetRadius(REAL32 MagnetRadius)
{
	m_MagnetRadius = MagnetRadius;
	return TRUE;
}

REAL32 EPN_Particle :: GetMagnetRadius()
{
	return m_MagnetRadius;
}

BOOL EPN_Particle :: SetParticleStopFlag(BOOL ParticleStopFlag)
{
	m_ParticleSTOP = ParticleStopFlag;
	return TRUE;
}

BOOL EPN_Particle :: GetIsParticleStop()
{
	return m_ParticleSTOP;
}