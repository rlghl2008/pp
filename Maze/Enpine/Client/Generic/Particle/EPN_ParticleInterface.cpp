#include "EPN_ParticleInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::PARTICLE;
#pragma endregion

EPN_ParticleInterface* EPN_ParticleInterface ::m_pParticleInterface = NULL_PTR;

EPN_ParticleInterface :: EPN_ParticleInterface()
{

}

EPN_ParticleInterface :: ~EPN_ParticleInterface()
{

}

EPN_ParticleInterface* EPN_ParticleInterface :: GetInstance()
{
	return m_pParticleInterface;
}
VOID EPN_ParticleInterface :: ReleaseInstance()
{
	if(NULL_PTR != m_pParticleInterface)
		delete m_pParticleInterface;
	m_pParticleInterface = NULL_PTR;
}
EPN_ParticleInterface* EPN_ParticleInterface :: CreateInstance()
{
	if (NULL_PTR == m_pParticleInterface)
	{
		m_pParticleInterface = new EPN_ParticleInterface;
	}
	return m_pParticleInterface;
}

VOID EPN_ParticleInterface::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
	INT32 Count = 0;
	EPN_ParticleManager* Temp = NULL_PTR;
	for (INT32 Index = 0; Count < GetAddDataCount(); ++Index)
	{
		Temp = GetManager(Index);
		if (NULL_PTR==Temp) 
			continue;
		Temp->SetLogFlag(Flag);
		++Count;
	}
}

BYTE EPN_ParticleInterface::GetLogFlag()
{
	return m_LogFlag;
}

INT32 EPN_ParticleInterface :: GetManagerIndex(estring SaveParticleMgrName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveParticleMgrName);
	if( 0 > Index )
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveParticleManagerName [ %s ] 입니다. )\n", SaveParticleMgrName);
		return -1;
	}
	return Index;
}
estring EPN_ParticleInterface :: GetManagerStr(INT32 MaxParticleNum)
{
	return m_StringIndexPool.SearchString(MaxParticleNum);
}

INT32 EPN_ParticleInterface :: AddParticleManager(INT32 MaxParticleNum)
{
	INT32 Index = m_mpParticleMgr.AddData(EPN_ParticleManager());
	EPN_ParticleManager* Temp = NULL_PTR;
	Temp = GetManager(Index);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( InsertData 문제발생! )\n");
		return -1;
	}
	if(FALSE == Temp->SetMaxParticleNum(MaxParticleNum))
	{
		PRINT_LOG(m_LogFlag, "( SetManager 문제발생! )\n");
		m_mpParticleMgr.EraseData(Index);
		return -1;
	}

	
	return Index;
}
INT32 EPN_ParticleInterface :: AddParticleManager(estring SaveParticleMgrName,INT32 MaxParticleNum)
{
	INT32 Index = AddParticleManager(MaxParticleNum);
	if(-1 < Index)
	{
		if(FALSE==m_StringIndexPool.InsertData(SaveParticleMgrName,Index))
		{
			PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveParticleMgrName [ %s ] 이거나 문자열 길이가 %d 를 초과 하였습니다. )\n", SaveParticleMgrName,EPN_SI_STRLEN_MAX);
			DeleteManager(Index);
			return -1;
		}
	}
	return Index;
}
BOOL EPN_ParticleInterface :: Run(INT32 ParticleMgrIndex,EPN_Pos Pos)
{
	EPN_ParticleManager *Temp = NULL_PTR;
	if(0 > ParticleMgrIndex)
	{
		INT32 Count = 0;
		for(INT32 Index = 0; Count < GetAddDataCount(); ++Index)
		{
			Temp = GetManager(Index);
			if(NULL_PTR == Temp) 
				continue;
			++Count;
			if(FALSE==Temp->Run(-1,Pos))
			{
				PRINT_LOG(m_LogFlag, "( [ %d ] 번 매니저 Run이 False 되었습니다. )\n", ParticleMgrIndex);
			
				return FALSE;
			}
		}
	}
	else
	{
		Temp = GetManager(ParticleMgrIndex);
		if(NULL_PTR == Temp)
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 번 매니저 얻어올 수 없습니다. )\n", ParticleMgrIndex);
			return FALSE;
		}
		if(FALSE==Temp->Run(-1,Pos))
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 번 매니저 Run이 False 되었습니다. )\n", ParticleMgrIndex);
			return FALSE;
		}
	}

	return TRUE;
}
BOOL EPN_ParticleInterface :: Run(estring SaveParticleMgrName, EPN_Pos Pos)
{
	INT32 ParticleMgrIndex = m_StringIndexPool.SearchIndex(SaveParticleMgrName);
	if(0 > ParticleMgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveParticleManagerName [ %s ] 입니다. )\n", SaveParticleMgrName);
		return FALSE;
	}
	return Run(ParticleMgrIndex,Pos);
}
BOOL EPN_ParticleInterface :: DeleteManager(INT32 ParticleMgrIndex)
{
	EPN_ParticleManager* Temp = GetManager(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return FALSE;
	}
	Temp->ClearManager();
	m_mpParticleMgr.EraseData(ParticleMgrIndex);
	m_StringIndexPool.DeleteData(ParticleMgrIndex);

	return TRUE;
}
BOOL EPN_ParticleInterface :: DeleteManager(estring SaveParticleMgrName)
{
	INT32 ParticleMgrIndex = m_StringIndexPool.SearchIndex(SaveParticleMgrName);
	if(0 > ParticleMgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveParticleManagerName [ %s ] 입니다. )\n", SaveParticleMgrName);
		return FALSE;
	}
	return DeleteManager(ParticleMgrIndex);
}
BOOL EPN_ParticleInterface :: AllDeleteManager()
{
	EPN_ParticleManager* Temp = NULL_PTR;
	INT32 Count = 0;
	
	INT32 InserCount = GetAddDataCount();
	for(INT32 Index = 0; Count < InserCount; ++Index)
	{
		Temp = GetManager(Index);
		if(NULL_PTR == Temp) 
			continue;
		++Count;
		DeleteManager(Index);
	}
	return TRUE;
}

BOOL EPN_ParticleInterface :: ClearManager(INT32 ParticleMgrIndex)
{
	EPN_ParticleManager* temp = GetManager(ParticleMgrIndex);
	if(NULL_PTR==temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return FALSE;
	}
	return temp->ClearManager();
}
BOOL EPN_ParticleInterface :: ClearManager(estring SaveParticleMgrName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveParticleMgrName);
	if( 0 > Index )
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveParticleMgrName [ %s ] 입니다. )\n", SaveParticleMgrName);
		return FALSE;
	}
	return ClearManager(Index);
}
BOOL EPN_ParticleInterface :: AllClearManager()
{
	EPN_ParticleManager *Temp = NULL_PTR;
	INT32 Count = 0;
	INT32 InserCount = GetAddDataCount();
	for(INT32 Index = 0; Count < InserCount; ++Index)
	{
		Temp = GetManager(Index);
		if(NULL_PTR == Temp) 
			continue;
		++Count;
		if(FALSE==Temp->ClearManager())
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 인덱스 번호의 ParticleManager Clear Failed! )\n( 작업을 중단 합니다! )\n", (INT32)Index);
			return FALSE;
		}
	}
	return TRUE;
}



INT32 EPN_ParticleInterface :: GetAddDataCount()
{
	return m_mpParticleMgr.GetAddDataCount();
}

EPN_ParticleManager* EPN_ParticleInterface :: GetManager(INT32 ParticleMgrIndex)
{
	EPN_ParticleManager* Temp = m_mpParticleMgr.GetDataPtr(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return NULL_PTR;
	}
	return Temp;
}
EPN_ParticleManager* EPN_ParticleInterface :: GetManager(estring ParticleMgrName)
{
	INT32 MgrIndex = m_StringIndexPool.SearchIndex(ParticleMgrName);
	if(0 > MgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 ParticleMgrName [ %s ] 입니다. )\n", ParticleMgrName);
		return NULL_PTR;
	}
	return m_mpParticleMgr.GetDataPtr(MgrIndex);
}

BOOL EPN_ParticleInterface :: AllSetMaxParticleNum(INT32 ParticleNum)
{
	EPN_ParticleManager *Temp = NULL_PTR;
	INT32 Count = 0;
	for(INT32 Index = 0; Count < GetAddDataCount(); ++Index)
	{
		Temp = GetManager(Index);
		if(NULL_PTR == Temp) 
			continue;
		++Count;
		if(FALSE == Temp->SetMaxParticleNum(ParticleNum))
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 인덱스 번호의 Set Paticle Max Number Failed! )\n( 작업을 중단 합니다! )\n", (INT32)Index);
			return FALSE;
		}
	}
	return TRUE;
}
BOOL EPN_ParticleInterface :: SetMaxParticleNum(INT32 ParticleMgrIndex,INT32 ParticleNum)
{
	EPN_ParticleManager* Temp = m_mpParticleMgr.GetDataPtr(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return FALSE;
	}
	return Temp->SetMaxParticleNum(ParticleNum);
}
BOOL EPN_ParticleInterface :: SetMaxParticleNum(estring ParticleMgrName,INT32 ParticleNum)
{
	INT32 MgrIndex = m_StringIndexPool.SearchIndex(ParticleMgrName);
	if(0 > MgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 ParticleMgrName [ %s ] 입니다. )\n", ParticleMgrName);
		return FALSE;
	}
	return SetMaxParticleNum(MgrIndex,ParticleNum);
}
BOOL EPN_ParticleInterface :: GetMaxParticleNum(INT32 ParticleMgrIndex)
{
	EPN_ParticleManager* Temp = m_mpParticleMgr.GetDataPtr(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return FALSE;
	}
	return Temp->GetMaxParticleNum();
}
BOOL EPN_ParticleInterface :: GetMaxParticleNum(estring ParticleMgrName)
{
	INT32 MgrIndex = m_StringIndexPool.SearchIndex(ParticleMgrName);
	if(0 > MgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 ParticleMgrName [ %s ] 입니다. )\n", ParticleMgrName);
		return FALSE;
	}
	return GetMaxParticleNum(MgrIndex);
}

BOOL EPN_ParticleInterface :: ResetMaxParticleNum(INT32 ParticleMgrIndex,INT32 ParticleNum)
{
	EPN_ParticleManager* Temp = m_mpParticleMgr.GetDataPtr(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return FALSE;
	}
	return Temp->ResetMaxParticleNum(ParticleNum);
}
BOOL EPN_ParticleInterface :: AllreSetMaxParticleNum(INT32 ParticleNum)
{
	EPN_ParticleManager *Temp = NULL_PTR;
	INT32 Count = 0;
	for(INT32 Index = 0; Count < this->GetAddDataCount(); ++Index)
	{
		Temp = this->GetManager(Index);
		if(NULL_PTR == Temp) 
			continue;
		++Count;
		if(FALSE == Temp->ResetMaxParticleNum(ParticleNum))
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 인덱스 번호의 Reset Paticle Max Number Failed! )\n( 작업을 중단 합니다! )\n", (INT32)Index);
			return FALSE;
		}
	}
	return TRUE;
}
BOOL EPN_ParticleInterface :: ResetMaxParticleNum(estring ParticleMgrName,INT32 ParticleNum)
{
	INT32 MgrIndex = m_StringIndexPool.SearchIndex(ParticleMgrName);
	if(0 > MgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 ParticleMgrName [ %s ] 입니다. )\n", ParticleMgrName);
		return FALSE;
	}
	return ResetMaxParticleNum(MgrIndex,ParticleNum);
}


INT32 EPN_ParticleInterface :: AddParticle(INT32 ParticleMgrIndex)
{
	EPN_ParticleManager* Temp = m_mpParticleMgr.GetDataPtr(ParticleMgrIndex);
	if(NULL_PTR == Temp)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ParticleMgrIndex [ %d ] 입니다. )\n", ParticleMgrIndex);
		return -1;
	}
	return Temp->AddParticle();
}
INT32 EPN_ParticleInterface :: AddParticle(estring ParticleMgrName)
{
	INT32 MgrIndex = m_StringIndexPool.SearchIndex(ParticleMgrName);
	if(0 > MgrIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 ParticleMgrName [ %s ] 입니다. )\n", ParticleMgrName);
		return -1;
	}
	return AddParticle(MgrIndex);
}