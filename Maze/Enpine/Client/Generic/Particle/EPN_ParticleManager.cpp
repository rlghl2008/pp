#define DLL_EPN_PARTICLEMANAGER_CLASS
#include "EPN_ParticleManager.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::PARTICLE;
#pragma endregion

EPN_ParticleManager :: EPN_ParticleManager()
{
	m_LogFlag = (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON;;
	m_pParticle = NULL_PTR;
	m_MaxParticleNum = 0;
}
EPN_ParticleManager :: ~EPN_ParticleManager()
{
	if(NULL_PTR != m_pParticle)
		delete[] m_pParticle;
}


VOID EPN_ParticleManager::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;

	if (NULL_PTR==m_pParticle || 0 >=m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
	}
	else
	{
		for (INT32 i = 0; i < m_MaxParticleNum; ++i)
			m_pParticle[i].SetLogFlag(Flag);
	}
}

BYTE EPN_ParticleManager::GetLogFlag()
{
	return m_LogFlag;
}


BOOL EPN_ParticleManager :: SetMaxParticleNum(INT32 ParticleNum)
{
	if(NULL_PTR != m_pParticle)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 이미 되어있습니다. )\n");
		return FALSE;
	}
	
	m_MaxParticleNum = ParticleNum;
	m_pParticle = new EPN_Particle[m_MaxParticleNum];
	
	return TRUE; 
}
INT32 EPN_ParticleManager :: GetMaxParticleNum()
{
	return m_MaxParticleNum;
}
BOOL EPN_ParticleManager :: ResetMaxParticleNum(INT32 ParticleNum)
{
	if(FALSE==ClearManager())
		return FALSE;
	m_pParticle = new EPN_Particle[m_MaxParticleNum];
	m_MaxParticleNum = ParticleNum;
	return TRUE;
}

BOOL EPN_ParticleManager :: ClearManager()
{
	if(NULL_PTR!=m_pParticle)
		delete[] m_pParticle;
	m_MaxParticleNum = 0;
	m_pParticle = NULL_PTR;
	return TRUE;
}
INT32 EPN_ParticleManager :: AddParticle()
{	
	if(NULL_PTR==m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return -1;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		if(m_pParticle[i].GetParticleDieFlag())
		{
			m_pParticle[i].Clear();
			m_pParticle[i].SetParticleDieFlag(FALSE);
			
			return i;
		}
	}
	return -1;
}
BOOL EPN_ParticleManager :: Run(INT32 ParticleIndex, EPN_Pos Pos)
{
	if(NULL_PTR==m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		
		return FALSE;
	}
	if( -1 == ParticleIndex )
	{
		for(INT32 i = 0;i < m_MaxParticleNum;++i)
		{
			m_pParticle[i].Run(Pos);
		}
	}
	else
	{
		if( 0 > ParticleIndex || ParticleIndex >= m_MaxParticleNum)
		{
			
			PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
			return FALSE;
		}
		m_pParticle[ParticleIndex].Run(Pos);
	}
	return TRUE;
}

EPN_Particle* EPN_ParticleManager :: GetParticle(INT32 ParticleIndex)
{
	if(NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return &m_pParticle[ParticleIndex];
}

BOOL EPN_ParticleManager :: AllSetParticleDieFlag(BOOL ParticleDieFlag)
{
	if(NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetParticleDieFlag(i,ParticleDieFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetParticleDieFlag(INT32 ParticleIndex,INT32 DeleteCount)
{
	if(NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ParticleManager :: SetParticleDieFlag \n( 설정 되지않은 ParticleIndex 입니다 ) ");
		return FALSE;
	}

	return m_pParticle[ParticleIndex].SetParticleDieFlag(DeleteCount);
}
BOOL EPN_ParticleManager :: GetParticleDieFlag(INT32 ParticleIndex)
{
	if(NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ParticleManager :: GetParticleDieFlag \n( 설정 되지않은 ParticleIndex 입니다 ) ");
		return FALSE;
	}

	return m_pParticle[ParticleIndex].GetParticleDieFlag();
}

BOOL EPN_ParticleManager :: AllSetDeleteCountFlag(BOOL DeleteCountFlag)
{
	if(NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum )
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetDeleteCountFlag(i,DeleteCountFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetDeleteCountFlag(INT32 ParticleIndex,BOOL DeleteCountFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ParticleManager :: SetDeleteCountFlag \n( 설정 되지않은 ParticleIndex 입니다 ) ");
		return FALSE;
	}

	return m_pParticle[ParticleIndex].SetDeleteCountFlag(DeleteCountFlag);
}
BOOL EPN_ParticleManager :: GetDeleteCountFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ParticleManager :: GetDeleteCountFlag \n( 설정 되지않은 ParticleIndex 입니다 ) ");
		return FALSE;
	}

	return m_pParticle[ParticleIndex].GetDeleteCountFlag();
}

BOOL EPN_ParticleManager :: AllSetDeleteCount(INT32 DeleteCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetDeleteCount(i,DeleteCount);
	}
	return TRUE;
}

BOOL EPN_ParticleManager :: SetDeleteCount(INT32 ParticleIndex,INT32 DeleteCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}

	return m_pParticle[ParticleIndex].SetDeleteCount(DeleteCount);

}
INT32 EPN_ParticleManager :: GetDeleteCount(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetDeleteCount();
}

BOOL EPN_ParticleManager :: AllSetParticlePos(EPN_Pos ParticlePos)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetParticlePos(i,ParticlePos);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetParticlePos(INT32 ParticleIndex,EPN_Pos ParticlePos)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetParticlePos(ParticlePos);
}
EPN_Pos EPN_ParticleManager :: GetParticlePos(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetParticlePos();
}

BOOL EPN_ParticleManager :: AllSetParticleMove(EPN_Pos ParticleMove)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetParticleMove(i,ParticleMove);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetParticleMove(INT32 ParticleIndex,EPN_Pos ParticleMove)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetParticleMove(ParticleMove);
}
EPN_Pos EPN_ParticleManager :: GetParticleMove(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetParticleMove();
}

BOOL EPN_ParticleManager :: AllSetMoveAngle(REAL32 MoveAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMoveAngle(i,MoveAngle);
	}
	return TRUE;
}

BOOL EPN_ParticleManager :: SetMoveAngle(INT32 ParticleIndex,REAL32 MoveAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMoveAngle(MoveAngle);
}
REAL32 EPN_ParticleManager :: GetMoveAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMoveAngle();
}

BOOL EPN_ParticleManager :: AllSetMoveSpeed(REAL32 MoveSpeed)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMoveSpeed(i,MoveSpeed);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetMoveSpeed(INT32 ParticleIndex,REAL32 MoveSpeed)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMoveSpeed(MoveSpeed);
}
REAL32 EPN_ParticleManager :: GetMoveSpeed(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMoveSpeed();
}
BOOL EPN_ParticleManager :: AllSetAlterMoveAngleFlag(BOOL AlterMoveAngleFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterMoveAngleFlag(i,AlterMoveAngleFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterMoveAngleFlag(INT32 ParticleIndex,BOOL AlterMoveAngleFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterMoveAngleFlag(AlterMoveAngleFlag);
}
BOOL EPN_ParticleManager :: GetAlterMoveAngleFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterMoveAngleFlag();
}

BOOL EPN_ParticleManager :: AllSetAlterMoveAngle(REAL32 AlterMoveAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterMoveAngle(i,AlterMoveAngle);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterMoveAngle(INT32 ParticleIndex,REAL32 AlterMoveAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterMoveAngle(AlterMoveAngle);
}
REAL32 EPN_ParticleManager :: GetAlterMoveAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterMoveAngle();
}

BOOL EPN_ParticleManager :: AllSetAlterMoveSpeedFlag(BOOL AlterMoveSpeedFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterMoveSpeedFlag(i,AlterMoveSpeedFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterMoveSpeedFlag(INT32 ParticleIndex,BOOL AlterMoveSpeedFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterMoveSpeedFlag(AlterMoveSpeedFlag);
}
BOOL EPN_ParticleManager :: GetAlterMoveSpeedFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterMoveSpeedFlag();
}
BOOL EPN_ParticleManager :: AllSetAlterMoveSpeed(REAL32 AlterMoveSpeed)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterMoveSpeed(i,AlterMoveSpeed);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterMoveSpeed(INT32 ParticleIndex,REAL32 AlterMoveSpeed)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterMoveSpeed(AlterMoveSpeed);
}
REAL32 EPN_ParticleManager :: GetAlterMoveSpeed(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterMoveSpeed();
}
BOOL EPN_ParticleManager :: AllSetTextureIndex(INT32 TextureIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetTextureIndex(i,TextureIndex);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetTextureIndex(INT32 ParticleIndex,INT32 TextureIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetTextureIndex(TextureIndex);
}
INT32 EPN_ParticleManager :: GetTextureIndex(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetTextureIndex();
}

BOOL EPN_ParticleManager :: AllSetTextureAngle(REAL32 TextureAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetTextureAngle(i,TextureAngle);
	}
	return TRUE;
}

BOOL EPN_ParticleManager :: SetTextureAngle(INT32 ParticleIndex,REAL32 TextureAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetTextureAngle(TextureAngle);
}
REAL32 EPN_ParticleManager :: GetTextureAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetTextureAngle();
}

BOOL EPN_ParticleManager :: AllSetTextureScaling(EPN_Scaling TextureScaling)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetTextureScaling(i,TextureScaling);
	}
	return TRUE;
}

BOOL EPN_ParticleManager :: SetTextureScaling(INT32 ParticleIndex,EPN_Scaling TextureScaling)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetTextureScaling(TextureScaling);
}
EPN_Scaling EPN_ParticleManager :: GetTextureScaling(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetTextureScaling();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureAngleFlag(BOOL AlterTextureAngleFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureAngleFlag(i,AlterTextureAngleFlag);
	}
	return TRUE;
}

BOOL EPN_ParticleManager :: SetAlterTextureAngleFlag(INT32 ParticleIndex,BOOL AlterTextureAngleFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureAngleFlag(AlterTextureAngleFlag);
}
BOOL EPN_ParticleManager :: GetAlterTextureAngleFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureAngleFlag();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureAngle(REAL32 AlterTextureAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureAngle(i,AlterTextureAngle);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureAngle(INT32 ParticleIndex,REAL32 AlterTextureAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureAngle(AlterTextureAngle);
}
REAL32 EPN_ParticleManager :: GetAlterTextureAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureAngle();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureScalingFlag(BOOL AlterTextureScalingFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureScalingFlag(i,AlterTextureScalingFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureScalingFlag(INT32 ParticleIndex,BOOL AlterTextureScalingFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureScalingFlag(AlterTextureScalingFlag);
}
BOOL EPN_ParticleManager :: GetAlterTextureScalingFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureScalingFlag();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureScaling(EPN_Scaling AlterTextureScaling)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureScaling(i,AlterTextureScaling);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureScaling(INT32 ParticleIndex,EPN_Scaling AlterTextureScaling)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureScaling(AlterTextureScaling);
}
EPN_Scaling EPN_ParticleManager :: GetAlterTextureScaling(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureScaling();
}

BOOL EPN_ParticleManager :: AllSetTextureRect(EPN_Rect TextureRect)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetTextureRect(i,TextureRect);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetTextureRect(INT32 ParticleIndex,EPN_Rect TextureRect)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetTextureRect(TextureRect);
}
EPN_Rect EPN_ParticleManager :: GetTextureRect(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetTextureRect();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureRectFlag(BOOL AlterTextureRectFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureRectFlag(i,AlterTextureRectFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureRectFlag(INT32 ParticleIndex,BOOL AlterTextureRectFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureRectFlag(AlterTextureRectFlag);
}
BOOL EPN_ParticleManager :: GetAlterTextureRectFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureRectFlag();
}
BOOL EPN_ParticleManager::AllSetAlterTextureRect(EPN_Rect AlterTextureRect)
{
	if (NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for (INT32 i = 0; i < m_MaxParticleNum; ++i)
	{
		SetAlterTextureRect(i, AlterTextureRect);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureRect(INT32 ParticleIndex,EPN_Rect AlterTextureRect)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureRect(AlterTextureRect);
}
EPN_Rect EPN_ParticleManager :: GetAlterTextureRect(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureRect();
}

BOOL EPN_ParticleManager :: AllSetTextureColor(EPN_ARGB TextureColor)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetTextureColor(i,TextureColor);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetTextureColor(INT32 ParticleIndex,EPN_ARGB TextureColor)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetTextureColor(TextureColor);
}
EPN_ARGB EPN_ParticleManager :: GetTextureColor(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetTextureColor();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureColorFlag(BOOL AlterTextureColorFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureColorFlag(i,AlterTextureColorFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureColorFlag(INT32 ParticleIndex,BOOL AlterTextureColorFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureColorFlag(AlterTextureColorFlag);
}
BOOL EPN_ParticleManager :: GetAlterTextureColorFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureColorFlag();
}

BOOL EPN_ParticleManager :: AllSetAlterTextureColor(EPN_ARGB AlterTextureColor)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAlterTextureColor(i,AlterTextureColor);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAlterTextureColor(INT32 ParticleIndex,EPN_ARGB AlterTextureColor)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAlterTextureColor(AlterTextureColor);
}
EPN_ARGB EPN_ParticleManager :: GetAlterTextureColor(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAlterTextureColor();
}


BOOL EPN_ParticleManager :: AllSetSpriteFlag(BOOL SpriteFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetSpriteFlag(i,SpriteFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetSpriteFlag(INT32 ParticleIndex,BOOL SpriteFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetSpriteFlag(SpriteFlag);
}

BOOL EPN_ParticleManager :: GetSpriteFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetSpriteFlag();
}

BOOL EPN_ParticleManager :: AllSetSpriteIndex(INT32 SpriteIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetSpriteIndex(i,SpriteIndex);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetSpriteIndex(INT32 ParticleIndex,INT32 SpriteIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetSpriteIndex(SpriteIndex);
}
INT32 EPN_ParticleManager :: GetSpriteIndex(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetSpriteIndex();
}

BOOL EPN_ParticleManager :: AllSetSpriteDelay(INT32 SpriteDelay)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetSpriteDelay(i,SpriteDelay);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetSpriteDelay(INT32 ParticleIndex,INT32 SpriteDelay)
{ 
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetSpriteDelay(SpriteDelay);
}
INT32 EPN_ParticleManager :: GetSpriteDelay(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetSpriteDelay();
}

BOOL EPN_ParticleManager :: AllSetAfterBltFlag(BOOL AfterBltFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAfterBltFlag(i,AfterBltFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAfterBltFlag(INT32 ParticleIndex,BOOL AfterBltFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAfterBltFlag(AfterBltFlag);
}
BOOL EPN_ParticleManager :: GetAfterBltFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAfterBltFlag();
}

BOOL EPN_ParticleManager :: AllSetAfterImageNum(INT32 AfterBltFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAfterImageNum(i,AfterBltFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAfterImageNum(INT32 ParticleIndex,INT32 AfterBltFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAfterImageNum(AfterBltFlag);
}
INT32 EPN_ParticleManager :: GetAfterImageNum(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAfterImageNum();
}

BOOL EPN_ParticleManager :: AllSetAfterImageCount(INT32 AfterImageCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAfterImageCount(i,AfterImageCount);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAfterImageCount(INT32 ParticleIndex,INT32 AfterImageCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAfterImageCount(AfterImageCount);
}
INT32 EPN_ParticleManager :: GetAfterImageCount(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAfterImageCount();
}

BOOL EPN_ParticleManager :: AllSetAfterImageDeleteCount(INT32 AfterImageDeleteCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetAfterImageDeleteCount(i,AfterImageDeleteCount);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetAfterImageDeleteCount(INT32 ParticleIndex, INT32 AfterImageDeleteCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetAfterImageDeleteCount(AfterImageDeleteCount);
}
INT32 EPN_ParticleManager :: GetAfterImageDeleteCount(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetAfterImageDeleteCount();
}


BOOL EPN_ParticleManager :: AllSetWindUseFlag(BOOL WindUseFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetWindUseFlag(i,WindUseFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetWindUseFlag(INT32 ParticleIndex,BOOL WindUseFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetWindUseFlag(WindUseFlag);
}
BOOL EPN_ParticleManager :: GetWindUseFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetWindUseFlag();
}

BOOL EPN_ParticleManager :: AllSetWindAngle(REAL32 WindAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetWindAngle(i,WindAngle);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetWindAngle(INT32 ParticleIndex,REAL32 WindAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetWindAngle(WindAngle);
}
REAL32 EPN_ParticleManager :: GetWindAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetWindAngle();
}

BOOL EPN_ParticleManager :: AllSetWindPower(REAL32 WindPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetWindPower(i,WindPower);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetWindPower(INT32 ParticleIndex,REAL32 WindPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetWindPower(WindPower);
}
REAL32 EPN_ParticleManager :: GetWindPower(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetWindPower();
}

BOOL EPN_ParticleManager :: AllSetGravityFlag(BOOL GravityFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetGravityFlag(i,GravityFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetGravityFlag(INT32 ParticleIndex,BOOL GravityFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetGravityFlag(GravityFlag);
}
BOOL EPN_ParticleManager :: GetGravityFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetGravityFlag();
}

BOOL EPN_ParticleManager :: AllSetGravityAngle(REAL32 GravityAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetGravityAngle(i,GravityAngle);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetGravityAngle(INT32 ParticleIndex,REAL32 GravityAngle)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetGravityAngle(GravityAngle);
}
REAL32 EPN_ParticleManager :: GetGravityAngle(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetGravityAngle();
}

BOOL EPN_ParticleManager :: AllSetGravityMaxPower(REAL32 GravityMaxPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetGravityMaxPower(i,GravityMaxPower);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetGravityMaxPower(INT32 ParticleIndex,REAL32 GravityMaxPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetGravityMaxPower(GravityMaxPower);
}
REAL32 EPN_ParticleManager :: GetGravityMaxPower(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetGravityMaxPower();
}

BOOL EPN_ParticleManager :: AllSetGravitySecCount(REAL32 GravitySecCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetGravitySecCount(i,GravitySecCount);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetGravitySecCount(INT32 ParticleIndex,REAL32 GravitySecCount)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetGravitySecCount(GravitySecCount);
}
REAL32 EPN_ParticleManager :: GetGravitySecCount(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetGravitySecCount();
}

BOOL EPN_ParticleManager :: AllreSetGravitySpeed()
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		reSetGravitySpeed(i);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: reSetGravitySpeed(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].ResetGravitySpeed();
}

BOOL EPN_ParticleManager :: AllSetMagnetFlag(BOOL MagnetFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMagnetFlag(i,MagnetFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetMagnetFlag(INT32 ParticleIndex,BOOL MagnetFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMagnetFlag(MagnetFlag);
}
BOOL EPN_ParticleManager :: GetMagnetFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMagnetFlag();
}

BOOL EPN_ParticleManager :: AllSetMagnetPos(EPN_Pos MagnetPos)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMagnetPos(i,MagnetPos);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetMagnetPos(INT32 ParticleIndex,EPN_Pos MagnetPos)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMagnetPos(MagnetPos);
}
EPN_Pos EPN_ParticleManager :: GetMagnetPos(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMagnetPos();
}

BOOL EPN_ParticleManager :: AllSetMagnetPower(REAL32 MagnetPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMagnetPower(i,MagnetPower);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetMagnetPower(INT32 ParticleIndex,REAL32 MagnetPower)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMagnetPower(MagnetPower);
}
REAL32 EPN_ParticleManager :: GetMagnetPower(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMagnetPower();
}

BOOL EPN_ParticleManager :: AllSetMagnetRadius(REAL32 MagnetRadius)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetMagnetRadius(i,MagnetRadius);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetMagnetRadius(INT32 ParticleIndex,REAL32 MagnetRadius)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetMagnetRadius(MagnetRadius);
}
REAL32 EPN_ParticleManager :: GetMagnetRadius(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetMagnetRadius();
}

BOOL EPN_ParticleManager :: AllSetParticleStopFlag(BOOL ParticleStopFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	for(INT32 i = 0;i < m_MaxParticleNum;++i)
	{
		SetParticleStopFlag(i,ParticleStopFlag);
	}
	return TRUE;
}
BOOL EPN_ParticleManager :: SetParticleStopFlag(INT32 ParticleIndex,BOOL ParticleStopFlag)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].SetParticleStopFlag(ParticleStopFlag);
}
BOOL EPN_ParticleManager :: GetParticleStopFlag(INT32 ParticleIndex)
{
	if( NULL_PTR == m_pParticle || 0 >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( Particle 설정이 되어 있지않습니다. )\n");
		return FALSE;
	}
	if( 0 > ParticleIndex  || ParticleIndex >= m_MaxParticleNum)
	{
		PRINT_LOG(m_LogFlag, "( 설정 되지않은 ParticleIndex 입니다. )\n");
		return FALSE;
	}
	return m_pParticle[ParticleIndex].GetIsParticleStop();
}