#pragma once

#include "../Global/Base/GlobalBase.h"

#include "../../Common/Class/EPN_LogManager.h"
#include "../../Common/Class/EPN_ThreadSafeManager.h"

#include "Device/EPN_DeviceInterface.h"
#include "InputEvent/EPN_InputEvent.h"
#include "Texture/EPN_TextureInterface.h"
#include "Font/EPN_FontInterface.h"
#include "FPS/EPN_FPS.h"
#include "Particle/EPN_ParticleInterface.h"
#include "Action/EPN_ActionInterface.h"

namespace EPN
{
	namespace GENERIC
	{
		DEVICE::EPN_DeviceInterface* GET_DI()
		{
			return DEVICE::EPN_DeviceInterface::GetInstance();
		}

		VOID FINISH_INTERFACE()
		{
			DEVICE::EPN_DeviceInterface::ReleaseInstance();

			INPUTEVENT::EPN_InputEvent::ReleaseInstance();

			FONT::EPN_FontInterface::ReleaseInstance();

			TEXTURE::EPN_TextureInterface::ReleaseInstance();

			ACTION::EPN_ActionInterface::ReleaseInstance();

			//PARTICLE::EPN_ParticleInterface::ReleaseInstance();

			COMMON::EPN_LogManager::ReleaseInstance();

			//COMMON::EPN_ThreadSafeManager::ReleaseInstance();
		}
		struct ENPINE_AUTO_INIT
		{
			ENPINE_AUTO_INIT()
			{
				DEVICE::EPN_DeviceInterface::CreateInstance();
				INPUTEVENT::EPN_InputEvent::CreateInstance();
				TEXTURE::EPN_TextureInterface::CreateInstance();
				ACTION::EPN_ActionInterface::CreateInstance();
				FONT::EPN_FontInterface::CreateInstance();
				//   PARTICLE::EPN_ParticleInterface::CreateInstance();
				COMMON::EPN_LogManager::CreateInstance();
				//   COMMON::EPN_ThreadSafeManager::CreateInstance();
			}
			~ENPINE_AUTO_INIT()
			{
				FINISH_INTERFACE();
			}
		};
#define ENPINE_INITIALIZE ENPINE_AUTO_INIT EPN_INIT;
	}
}

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::INPUTEVENT;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::ACTION;
using namespace EPN::GENERIC::UTIL;
#pragma endregion