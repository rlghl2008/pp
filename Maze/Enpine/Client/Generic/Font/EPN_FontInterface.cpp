#include "EPN_FontInterface.h"
#include "../Device/EPN_DeviceInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::FONT;
#pragma endregion

EPN_FontInterface* EPN_FontInterface::m_pFontInterface = NULL_PTR;
EPN_FontInterface* EPN_FontInterface::GetInstance()
{
	return m_pFontInterface;
}
EPN_FontInterface* EPN_FontInterface::CreateInstance(VOID)
{
	if (NULL_PTR == m_pFontInterface)
		m_pFontInterface = new EPN_FontInterface();
	return m_pFontInterface;
}
VOID EPN_FontInterface::ReleaseInstance()
{
	if (NULL_PTR != m_pFontInterface)
		delete m_pFontInterface;
	m_pFontInterface = NULL_PTR;
}

EPN_FontInterface::EPN_FontInterface()
{

}

EPN_FontInterface::~EPN_FontInterface()
{

}

VOID EPN_FontInterface::SetLogFlag(BYTE LogFlag, BOOL AutoFlag)
{
	m_LogFlag = LogFlag;
	/*if (TRUE == AutoFlag)
	{
	INT32 Count = 0;
	EPN_Font *pFont = NULL_PTR;
	for (INT32 Index = 0; Count < GetAddDataCount(); ++Index)
	{
	pFont = GetFont(Index);
	if (NULL_PTR == pFont)
	continue;
	pFont->SetLogFlag(LogFlag);
	++Count;
	}
	}*/
}
BYTE EPN_FontInterface::GetLogFlag()
{
	return m_LogFlag;
}

INT32 EPN_FontInterface::LoadSystemFont(estring FontName, INT32 FontSize)
{
	if (NULL_PTR == EPN_DeviceInterface::CreateInstance()->GetDevice())
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Device 입니다. )\n");
		return -1;
	}
	//폰트 생성
	EPN_Font *pFont = new EPN_Font();
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 폰트 적재 문제 발생 : EPN_Font 생성 실패 )\n");
		return -1;
	}
	//폰트에 cocos2d::레이블 생성후 추가
	INT32 LabelIndex = pFont->m_mpLabel.AddData(cocos2d::Label::createWithSystemFont("", FontName.GetStrA(), (REAL32)FontSize));
	pFont->m_mpLabel.GetData(LabelIndex)->retain();
	if (-1 == LabelIndex)
	{
		PRINT_LOG(m_LogFlag, "( 폰트 적재 문제 발생 : cocos2d::Label 생성 실패 )\n");
		return -1;
	}
	//폰트를 폰트 풀에 추가
	INT32 FontIndex = m_mpFont.AddData(pFont);

	return FontIndex;
}
INT32 EPN_FontInterface::LoadSystemFont(estring SaveFontName, estring FontName, INT32 FontSize)
{
	INT32 Index = LoadSystemFont(FontName, FontSize);
	if (0 > Index)
		return -1;

	if (FALSE == m_StringIndexPool.InsertData(SaveFontName, Index))
	{
		PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveFontName [ %s ] 이거나 문자열 길이가 %d 를 초과 하였습니다. )\n", SaveFontName, EPN_SI_STRLEN_MAX);
		DeleteFont(Index);
	}
	return Index;
}
INT32 EPN_FontInterface::LoadFont(estring FontName, estring Path, INT32 FontSize)
{
	if (NULL_PTR == EPN_DeviceInterface::CreateInstance()->GetDevice())
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Device 입니다. )\n");
		return -1;
	}
	//폰트 생성
	EPN_Font *pFont = new EPN_Font(FONT_TYPE::TTF);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 폰트 적재 문제 발생 : EPN_Font 생성 실패 )\n");
		return -1;
	}
	//폰트에 cocos2d::레이블 생성 후 추가
	INT32 LabelIndex = pFont->m_mpLabel.AddData(cocos2d::Label::createWithTTF(FontName.GetStrA(), Path.GetStrA(), (REAL32)FontSize));
	pFont->m_mpLabel.GetData(LabelIndex)->retain();
	if (-1 == LabelIndex)
	{
		PRINT_LOG(m_LogFlag, "( 폰트 적재 문제 발생 : cocos2d::Label 생성 실패 )\n");
		return -1;
	}

	//폰트를 폰트 풀에 추가
	INT32 FontIndex = m_mpFont.AddData(pFont);


	pFont->FontPath = Path;
	pFont->m_mpLabel.GetData(FontIndex)->retain();
	return FontIndex;
}
INT32 EPN_FontInterface::LoadFont(estring SaveFontName, estring FontName, estring Path, INT32 FontSize)
{
	INT32 Index = LoadFont(SaveFontName, FontName, FontSize);
	if (0 > Index)
		return -1;

	if (FALSE == m_StringIndexPool.InsertData(SaveFontName, Index))
	{
		PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveFontName [ %s ] 이거나 문자열 길이가 %d 를 초과 하였습니다. )\n", SaveFontName, EPN_SI_STRLEN_MAX);
		DeleteFont(Index);
	}
	return Index;
}


BOOL EPN_FontInterface::BltText(INT32 FontIndex, INT32 LayerIndex, estring Text, EPN_Pos Pos, EPN_ARGB Color)
{
	EPN_Font* pFont = GetFont(FontIndex);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Font Index [ %d ] 입니다. )\n", FontIndex);
		return FALSE;
	}

	INT32 Index = -1;
	if (pFont->BltCount >= pFont->m_mpLabel.GetAddDataCount())
	{
		FontDefinition FontDef = pFont->m_mpLabel.GetData(0)->getFontDefinition();
		estring str = pFont->FontPath;

		if (pFont->FontType == FONT_TYPE::SYSTEM)
			Index = pFont->m_mpLabel.AddData(cocos2d::Label::createWithSystemFont("", FontDef._fontName, FontDef._fontSize));
		else
			Index = pFont->m_mpLabel.AddData(cocos2d::Label::createWithTTF("", pFont->FontPath.GetStrA(), FontDef._fontSize));

		//폰트 레퍼런스 카운트를 하나 증가
		pFont->m_mpLabel.GetData(pFont->BltCount)->retain();
	}


	cocos2d::Label *pCurLabel = pFont->m_mpLabel.GetData(pFont->BltCount++);

	/* BltText시 설정할 속성 */

	pCurLabel->setColor(cocos2d::Color3B(Color.Red, Color.Green, Color.Blue));
	pCurLabel->setOpacity(Color.Alpha);


	//Blt Rect
	//pCurLabel->setContentSize(cocos2d)
	//Text
	//Text.GetStrA()
	pCurLabel->setString(Text.GetStrA());

	//Pos
	REAL32 Width = pCurLabel->getContentSize().width;
	REAL32 Height = pCurLabel->getContentSize().height;
	pCurLabel->setPosition(Vec2(Pos.X + Width / 2.f, Pos.Y - Height / 2.f));

	/* Blt Text */
	EPN_Device *pDevice = EPN_DeviceInterface::CreateInstance()->GetDevice();
	(*pDevice->mpDeviceLayer[LayerIndex])->addChild(pCurLabel);

	return TRUE;
}
BOOL EPN_FontInterface::BltText(estring SaveFontName, INT32 LayerIndex, estring Text, EPN_Pos Pos, EPN_ARGB Color)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveFontName);
	if (-1 == Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);
		return FALSE;
	}
	return BltText(Index, LayerIndex, Text, Pos, Color);
}

BOOL EPN_FontInterface::SetFontSize(INT32 FontIndex, INT32 FontSize)
{
	EPN_Font* pFont = GetFont(FontIndex);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Font Index [ %d ] 입니다. )\n", FontIndex);
		return FALSE;
	}
	FontDefinition FontDef = pFont->m_mpLabel.GetData(0)->getFontDefinition();
	FontDef._fontSize = FontSize;

	//전체 레이블 모두에게 적용
	for (int Cnt = 0, SearchCnt = 0; SearchCnt < pFont->m_mpLabel.GetAddDataCount(); ++Cnt)
	{
		if (TRUE == pFont->m_mpLabel.IsData(Cnt))
		{
			pFont->m_mpLabel.GetData(Cnt)->setFontDefinition(FontDef);
			++SearchCnt;
		}
	}

	return TRUE;
}
BOOL EPN_FontInterface::SetFontSize(estring SaveFontName, INT32 FontSize)
{
	INT32 Index = GetFontIndex(SaveFontName);
	if (-1 == Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);

		return FALSE;
	}

	return SetFontSize(Index, FontSize);
}
INT32 EPN_FontInterface::GetFontSize(INT32 FontIndex)
{
	EPN_Font* pFont = GetFont(FontIndex);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Font Index [ %d ] 입니다. )\n", FontIndex);
		return FALSE;
	}
	return (INT32)pFont->m_mpLabel.GetData(0)->getFontDefinition()._fontSize;
}
INT32 EPN_FontInterface::GetFontSize(estring SaveFontName)
{
	INT32 Index = GetFontIndex(SaveFontName);
	if (-1 == Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);
		return FALSE;
	}
	return (INT32)GetFontSize(Index);
}

INT32 EPN_FontInterface::GetFontIndex(estring SaveFontName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveFontName);
	if (0 > Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);

	}
	return Index;
}

estring EPN_FontInterface::GetFontName(INT32 FontIndex)
{
	return m_StringIndexPool.SearchString(FontIndex);
}

EPN_Font* EPN_FontInterface::GetFont(INT32 FontIndex)
{
	EPN_Font* pFont = m_mpFont.GetData(FontIndex);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Font Index [ %d ] 입니다. )\n", FontIndex);
	}
	return pFont;
}
EPN_Font* EPN_FontInterface::GetFont(estring SaveFontName)
{
	INT32 FontIndex = m_StringIndexPool.SearchIndex(SaveFontName);
	if (0 > FontIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);

		return NULL;
	}
	return m_mpFont.GetData(FontIndex);
}
BOOL EPN_FontInterface::DeleteFont(INT32 FontIndex)
{
	EPN_Font* pFont = GetFont(FontIndex);
	if (NULL_PTR == pFont)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지 않는 Font Index [ %d ] 입니다. )\n", FontIndex);

		return FALSE;
	}
	//폰트 내에 Label 모두 release()
	for (int Cnt = 0, ReleaseCnt = 0; ReleaseCnt < pFont->m_mpLabel.GetAddDataCount(); ++Cnt)
	{
		if (TRUE == pFont->m_mpLabel.IsData(Cnt))
		{
			pFont->m_mpLabel.GetData(Cnt)->release();
			++ReleaseCnt;
		}
	}

	//폰트 풀과 SI 풀에서 삭제
	m_mpFont.EraseData(FontIndex);
	m_StringIndexPool.DeleteData(FontIndex);

	return TRUE;
}
BOOL EPN_FontInterface::DeleteFont(estring SaveFontName)
{
	INT32 FontIndex = m_StringIndexPool.SearchIndex(SaveFontName);
	if (0 > FontIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지 않은 SaveFontName [ %s ] 입니다. )\n", SaveFontName);

		return FALSE;
	}
	return DeleteFont(FontIndex);
}
BOOL EPN_FontInterface::AllDeleteFont()
{
	EPN_Font* pFont = NULL_PTR;
	INT32 Count = 0;

	INT32 InsertCount = GetAddDataCount();
	for (INT32 Index = 0; Count < InsertCount; ++Index)
	{
		pFont = GetFont(Index);
		if (NULL_PTR == pFont)
			continue;
		++Count;
		DeleteFont(Index);
	}
	return TRUE;
}
INT32 EPN_FontInterface::GetAddDataCount()
{
	if (NULL_PTR == EPN_DeviceInterface::CreateInstance()->GetDevice())
	{
		PRINT_LOG(m_LogFlag, "( 디바이스가 존재하지 않습니다. )\n");
		return -1;
	}
	return m_mpFont.GetAddDataCount();
}

EPN_Pos EPN_FontInterface::GetETextAreaSize(INT32 FontIndex, estring EText)
{
	EPN_Font *pFont = m_mpFont.GetData(FontIndex);
	estring OriginText = pFont->m_mpLabel.GetData(0)->getString();

	pFont->m_mpLabel.GetData(0)->setString(EText.GetStrA());
	cocos2d::Rect TextRect = pFont->m_mpLabel.GetData(0)->getBoundingBox();

	pFont->m_mpLabel.GetData(0)->setString(OriginText.GetStrA());

	return EPN_Pos(TextRect.size.width, TextRect.size.height);
}
EPN_Pos EPN_FontInterface::GetETextAreaSize(estring SaveFontName, estring EText)
{
	return GetETextAreaSize(m_StringIndexPool.SearchIndex(SaveFontName), EText);
}