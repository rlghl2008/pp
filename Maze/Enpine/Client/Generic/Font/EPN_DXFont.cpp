#include "EPN_DXFont.h"
#include "../Device/EPN_DeviceInterface.h"
#include "../Texture/EPN_TextureInterface.h"
#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::TEXTURE;
#pragma endregion
#include <tchar.h>

//윈도우즈에서 COLORREF 로 색상값 표현할떄 쓰고 8비트씩 끊어씀  그래서 8비트씩 시프트함  
//특히나 원래는 a b g r 순서인데, 시프트연산으로 a r g b 로 만들어준 것
#define ARGB(a,r,g,b)          ((COLORREF)((((BYTE)(a)) | ((BYTE)(r))<<8 | ((WORD)((BYTE)(g))<<16)) | (((INT32)(BYTE)(b))<<24)))

EPN_DXFont::EPN_DXFont()
{
	memset(&m_FontInfo, 0, sizeof(&m_FontInfo));
	m_pID3DFont = NULL_PTR;
	m_pHFont = NULL_PTR;
	m_pD3DFontDevice = NULL_PTR;

	m_pTextureHDC = NULL_PTR;
	m_pFontSurface = NULL_PTR;
	m_FontTextureIndex = -1;
}
EPN_DXFont::~EPN_DXFont()
{

}
BOOL EPN_DXFont::BltText(estring Text, EPN_Rect Rect, EPN_ARGB Color, INT32 Buffer)
{
	EPN_DeviceInterface *pEPN_DI = EPN_DeviceInterface::CreateInstance();

	EPN_Device* pTempDevice = pEPN_DI->GetDevice(GetDeviceIndex());
	if (NULL_PTR == pTempDevice)
	{
		PRINT_LOG(m_LogFlag, "( 디바이스가 Lost 됬거나 설정되지 않았습니다. )\n");
		return FALSE;
	}
	if (TRUE == pTempDevice->GetBeginFlag())
	{
		((LPD3DXSPRITE)pTempDevice->GetRenderData())->End();
		pTempDevice->SetBeginFlag(FALSE);
	}
	//폰트 사용 가능일때
	if (NULL_PTR != m_pID3DFont)
	{
		//등록된 ETextColor 가 있으면 처리
		if (1 <= GetETextColorCount())
		{
			estring EText = L"";
			//폰트 그릴영역
			EPN_Rect ETextRect = EPN_Rect(Rect.Top.X, Rect.Top.Y, Rect.Top.X + Rect.Bottom.X, Rect.Top.Y + Rect.Bottom.Y);
			EPN_ARGB ETextColor = Color;

			//메모 : 그리기 처리를 할떄 한덩어리씩 따로따로 출력을 해준다.
			for (UINT32 i = 0; i < Text.Length(); ++i)
			{
				wchar_t wc = Text.at(i);
				//개행이나 공백을 만난다면
				if (wc == '\n' || wc == ' ')
				{
					ETextColor = Color;
					EPN_ARGB TempColor = GetETextColor(EText);
					if (TempColor.Alpha >= 0)
					{
						REAL32 Alpha = ETextColor.Alpha;
						ETextColor = TempColor;
						ETextColor.Alpha -= (255 - Alpha);
						if (ETextColor.Alpha < 0)
							ETextColor.Alpha = 0;
					}
					RECT tempRect = { (LONG)ETextRect.Top.X , (LONG)ETextRect.Top.Y, (LONG)ETextRect.Bottom.X , (LONG)ETextRect.Bottom.Y };

					//스프라이트 , 문자열, 길이 (-1일시 자동) , 영역 , 포맷 , 색상
					m_pID3DFont->DrawText(NULL, EText.GetStr(), Buffer, &tempRect, 0, D3DXCOLOR(ETextColor.Red / 255, ETextColor.Green / 255, ETextColor.Blue / 255, ETextColor.Alpha / 255));
					//가로 사이즈 계산
					ETextRect.Top.X += GetETextAreaSize(EText).X;

					//개행이면 
					if (wc == '\n')
					{
						ETextRect.Top.Y += GetETextAreaSize(L" ").Y;
						ETextRect.Top.X = Rect.Top.X;
						EText = L""; //다음 덩어리 처리를 위해서 초기화
					}
					//공백 이면
					else
					{
						ETextRect.Top.X += GetETextAreaSize(L" ").X;
						EText = L""; //다음 덩어리 처리를 위해서 초기화
					}
				}
				//개행이나 공백이 아니면 아직 한덩어리가 안끝난것이므로 계속 누적
				else
				{
					EText += wc;
				}
			}

			if (0 < EText.Length())
			{
				ETextColor = Color;
				EPN_ARGB TempColor = GetETextColor(EText);
				if (0 <= TempColor.Alpha)
					ETextColor = TempColor;
				RECT tempRect = { (LONG)ETextRect.Top.X , (LONG)ETextRect.Top.Y , (LONG)ETextRect.Bottom.X, (LONG)ETextRect.Bottom.Y };
				m_pID3DFont->DrawText(NULL, EText.GetStr(), Buffer, &tempRect, 0, D3DXCOLOR(ETextColor.Red / 255, ETextColor.Green / 255, ETextColor.Blue / 255, ETextColor.Alpha / 255));
			}
		}
		//등록된 ETextColor 가 하나도 없어서 그냥 그리면 되는경우
		else
		{
			RECT tempRect = { (LONG)Rect.Top.X, (LONG)Rect.Top.Y, (LONG)(Rect.Top.X + Rect.Bottom.X), (LONG)(Rect.Top.Y + Rect.Bottom.Y) };
			m_pID3DFont->DrawText(NULL, Text.GetStr(), Buffer, &tempRect, 0, D3DXCOLOR(Color.Red / 255, Color.Green / 255, Color.Blue / 255, Color.Alpha / 255));

		}
	}
	else
	{
		PRINT_LOG(m_LogFlag, "( 사용할 Font 설정이 되지않았습니다. (SetFont Error) )\n");
		return FALSE;
	}
	return TRUE;
}
BOOL EPN_DXFont::BltText(INT32 DestTextureIndex, estring Text, EPN_Rect Rect, EPN_ARGB FontColor, EPN_ARGB BackColor, INT32 Buffer)
{
	EPN_DeviceInterface *pEPN_DI = EPN_DeviceInterface::CreateInstance();

	EPN_Device* pTempDevice = pEPN_DI->GetDevice(GetDeviceIndex());

	if (NULL_PTR == pTempDevice)
	{
		PRINT_LOG(m_LogFlag, "( 디바이스가 Lost 됬거나 설정되지 않았습니다. )\n");
		return FALSE;
	}
	EPN_TextureInterface *pEPN_TI = EPN_TextureInterface::CreateInstance();
	EPN_Texture* TempTexture = pEPN_TI->GetTexture(DestTextureIndex);
	if (NULL_PTR == TempTexture)
	{
		PRINT_LOG(m_LogFlag, "( 텍스쳐 정보가 존재하지않습니다. )\n");
		return FALSE;
	}
	if (NULL_PTR != m_pHFont)
	{
		HDC hdc = NULL_PTR;

		LPDIRECT3DSURFACE9 pSurface = NULL_PTR;

		((IDirect3DTexture9*)TempTexture->GetTexture())->GetSurfaceLevel(0, &pSurface);

		SelectObject(hdc, m_pHFont);

		if (0 == BackColor.Alpha )
			SetBkMode(hdc, TRANSPARENT);
		else
			SetBkColor(hdc, RGB(BackColor.Red, BackColor.Green, BackColor.Blue));

		RECT rect;
		rect.left = (LONG)Rect.Top.X;
		rect.top = (LONG)Rect.Top.Y;
		rect.right = (LONG)Rect.Bottom.X;
		rect.bottom = (LONG)Rect.Bottom.Y;

		if ( 1 <= GetETextColorCount() )
		{

			estring EText = L"";
			EPN_Rect ETextRect = EPN_Rect(Rect.Top.X, Rect.Top.Y, Rect.Top.X + Rect.Bottom.X, Rect.Top.Y + Rect.Bottom.Y);
			EPN_ARGB ETextColor = FontColor;
			for (UINT32 i = 0; i < Text.Length(); ++i)
			{
				wchar_t wchar = Text.at(i);
				if (wchar == '\n' || wchar == ' ')
				{

					ETextColor = FontColor;
					EPN_ARGB TempColor = GetETextColor(EText);
					if (0 <= TempColor.Alpha )
					{
						REAL32 Alpha = ETextColor.Alpha;
						ETextColor = TempColor;
						ETextColor.Alpha -= (255 - Alpha);
						if (ETextColor.Alpha < 0)
							ETextColor.Alpha = 0;
					}
					RECT tempRect =
					{ 
						(LONG)ETextRect.Top.X,
						(LONG)ETextRect.Top.Y,
						(LONG)ETextRect.Bottom.X,
						(LONG)ETextRect.Bottom.Y 
					};

					SetTextColor(hdc, RGB(ETextColor.Red, ETextColor.Green, ETextColor.Blue));
					DrawTextW(hdc, EText.GetStrW(), wcslen(EText.GetStrW()), &tempRect, DT_LEFT);

					ETextRect.Top.X += GetETextAreaSize(EText).X;
					if (wchar == '\n')
					{
						ETextRect.Top.Y += GetETextAreaSize(L" ").Y;
						ETextRect.Top.X = Rect.Top.X;
						EText = L"";
					}
					else
					{
						ETextRect.Top.X += GetETextAreaSize(L" ").X;
						EText = L"";
					}

				}
				else
				{
					EText += wchar;
				}
			}
			if (0 < EText.Length() )
			{
				ETextColor = FontColor;
				EPN_ARGB TempColor = GetETextColor(EText);
				if (0 <= TempColor.Alpha )
					ETextColor = TempColor;
				SetTextColor(hdc, RGB(ETextColor.Red, ETextColor.Green, ETextColor.Blue));

				RECT tempRect =
				{ 
					(LONG)ETextRect.Top.X,
					(LONG)ETextRect.Top.Y,
					(LONG)ETextRect.Bottom.X,
					(LONG)ETextRect.Bottom.Y };

				DrawTextW(hdc, EText.GetStrW(), EText.Length(), &tempRect, DT_LEFT);
			}
		}
		else
		{
			SetTextColor(hdc, RGB(FontColor.Red, FontColor.Green, FontColor.Blue));
			DrawTextW(hdc, Text.GetStrW() ,Text.Length(), &rect, DT_LEFT);
		}


		pSurface->ReleaseDC(hdc);
		pSurface->Release();
	}
	else
	{
		PRINT_LOG(m_LogFlag, "( setFont 설정이 되지않았습니다. )\n");
		return FALSE;
	}

	return TRUE;
}

BOOL EPN_DXFont::SetFont(VOID* RenderDevice, estring FontName, INT32 FontSize, UINT32 FontThickness)
{
	if (NULL_PTR == EPN_DeviceInterface::CreateInstance()->GetDevice(GetDeviceIndex()))
	{
		PRINT_LOG(m_LogFlag, "( 디바이스 설정이 되지않았습니다. )\n");
		return FALSE;
	}
	if (NULL_PTR != m_pID3DFont)
	{
		ClearFont();
	}

	//폰트 설정
	D3DXFONT_DESC tempFont =
	{
		FontSize, 0, FontThickness, 1,
		FALSE, HANGUL_CHARSET,
		OUT_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,
		FF_DONTCARE,
		TEXT("")
	};

	estrcpy_s(tempFont.FaceName, FontName.GetStr());
	//strcpy_s(tempFont.FaceName, sizeof(tempFont.FaceName), FontName.GetStr());
	m_FontInfo = tempFont;
	if (NULL_PTR == RenderDevice)
	{
		PRINT_LOG(m_LogFlag, "( 잘못된 RenderDevice 입니다. )\n");
		memset(&m_FontInfo, 0, sizeof(&m_FontInfo));
		SetFontData(EPN_FontData());
		m_pID3DFont = NULL_PTR;
		return FALSE;
	}

	m_pD3DFontDevice = (LPDIRECT3DDEVICE9)RenderDevice;
	//장치와 폰트용의 폰트 객체를 간접적으로 생성한다.
	D3DXCreateFontIndirect(m_pD3DFontDevice, &m_FontInfo, &m_pID3DFont);
	SetFontData(EPN_FontData(FontName, FontSize, FontThickness));


	//LOGFONT 글꼴 구조체
	LOGFONT lf;
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = tempFont.Height;
	lf.lfWeight = tempFont.Weight;
	lf.lfWidth = tempFont.Width;
	//아래 원본 : wcscpy(lf.lfFaceName, tempFont.FaceName); 
	strcpy_s(lf.lfFaceName, sizeof(lf.lfFaceName), tempFont.FaceName);
	lf.lfItalic = tempFont.Italic;
	lf.lfUnderline = false;
	lf.lfStrikeOut = false;
	lf.lfCharSet = tempFont.CharSet;

	/*논리 폰트 : CreateFont 함수에 의해 만들어지는 폰트
	  물리 폰트 : OS 나 장치에 실제로 존재하는 폰트
	   스톡 폰트 : OS에 의해 제공되는 폰트(GetStockObejct)
	*/
	//논리 폰트 생성 이다. LOGFONT라는 구조체를 사용해서 폰트 정보를 지정함.
	m_pHFont = CreateFontIndirect(&lf);

	
	m_FontTextureIndex = EPN_TextureInterface::CreateInstance()->CreateTexture(GetDeviceIndex(), EPN_DeviceInterface::CreateInstance()->GetDevice(GetDeviceIndex())->GetDeviceProperty()->DeviceSize, EPN_ARGB(255, 255, 255, 255), true);
	//폰트에 사용할 텍스쳐를 생성하는데 실패하였을 시 
	if (0 > m_FontTextureIndex)
	{
		PRINT_LOG(m_LogFlag, "( FontTexture 생성 실패! )\n");
		return FALSE;
	}

	((IDirect3DTexture9*)EPN_TextureInterface::CreateInstance()->GetTexture(m_FontTextureIndex)->GetTexture())->GetSurfaceLevel(0, &m_pFontSurface);
	m_pFontSurface->GetDC(&m_pTextureHDC);

	//SelectObject 로 텍스쳐 HDC 에  GDI오브젝트의 핸들(두번째 인자) 오브젝트를 전달하여 DC에 해당 오브젝트를 선택해준다.
	//이후부터 m_pTextureHDC 는 해당 오브젝트(m_pHFont)를 사용하게 된다.
	//메모 : DC 라는 화가에게 명령을 하기전에 해당 명령을 수행할 도구를 선택해주는 역할이라고 보면된다.
	//예를 좀 더 들어보면 DC라는 화가가 기존에 다른도구를 들고있는데,  빨간선을 그려 라고 명령하기위해서는 DC가 빨간펜이란 도구를 들도록 선택해줘야함
	SelectObject(m_pTextureHDC, m_pHFont);

	return TRUE;
}
EPN_FontData* EPN_DXFont::GetFont()
{
	return GetFontData();
}
BOOL EPN_DXFont::SetFontName(estring FontName)
{
	return SetFont(m_pD3DFontDevice, FontName, GetFont()->FontSize, GetFont()->FontThickness);
}
estring EPN_DXFont::GetFontName()
{
	return GetFont()->FontName;
}
BOOL EPN_DXFont::SetFontSize(INT32 FontSize)
{
	return SetFont(m_pD3DFontDevice, GetFont()->FontName, FontSize, GetFont()->FontThickness);
}
INT32 EPN_DXFont::GetFontSize()
{
	return GetFont()->FontSize;
}
BOOL EPN_DXFont::SetFontThickness(INT32 FontThickness)
{
	EPN_CHAR temp[32] = { 0, };
	estrcpy_s(temp, GetFont()->FontName.GetStr());
	//strcpy_s(temp, sizeof(temp), GetFont()->FontName.GetStr());
	return SetFont(m_pD3DFontDevice, GetFont()->FontName, GetFont()->FontSize, FontThickness);
}
INT32 EPN_DXFont::GetFontThickness()
{
	return GetFont()->FontThickness;
}

BOOL EPN_DXFont::ClearFont(INT32 DeviceIndex)
{

	if (0 > DeviceIndex)
	{
		memset(&m_FontInfo, 0, sizeof(&m_FontInfo));

		//ID3DXFont  소멸
		if (NULL_PTR != m_pID3DFont)
			m_pID3DFont->Release();
		m_pID3DFont = NULL_PTR;

		//폰트 데이터 초기화
		SetFontData(EPN_FontData());
		//HFONT 객체도 소멸 
		DeleteObject(m_pHFont);
		m_pHFont = NULL_PTR;

		//폰트 서페이스도 존재하면 소멸
		if (NULL_PTR != m_pFontSurface)
		{
			//셋 폰트 때 m_pFontSurface->GetDC(&m_pTextureHDC); 해서 HDC 얻어왔으니 돌려준다. (GetDC해서 갖다쓴 HDC는 RELEASE DC로 반환)
			m_pFontSurface->ReleaseDC(m_pTextureHDC);
			//서페이스 소멸
			m_pFontSurface->Release();
		}
		m_pTextureHDC = NULL_PTR;
		m_pFontSurface = NULL_PTR;

		//폰트 출력에 사용한 텍스쳐 역시 삭제한다.
		EPN_TextureInterface::CreateInstance()->DeleteTexture(m_FontTextureIndex);
		m_FontTextureIndex = -1; //삭제 후 -1로 다시 초기화

		return TRUE;
	}
	else if (GetDeviceIndex() == DeviceIndex)
	{

		memset(&m_FontInfo, 0, sizeof(&m_FontInfo));
		//ID3DXFont  소멸
		if (NULL_PTR != m_pID3DFont)
			m_pID3DFont->Release();
		m_pID3DFont = NULL_PTR;

		//폰트 데이터 초기화
		SetFontData(EPN_FontData());
		//HFONT 객체도 소멸 
		DeleteObject(m_pHFont);
		m_pHFont = NULL_PTR;

		//폰트 서페이스도 존재하면 소멸
		if (NULL_PTR != m_pFontSurface)
		{
			//셋 폰트 때 m_pFontSurface->GetDC(&m_pTextureHDC); 해서 HDC 얻어왔으니 돌려준다. (GetDC해서 갖다쓴 HDC는 RELEASE DC로 반환)
			m_pFontSurface->ReleaseDC(m_pTextureHDC);
			//서페이스 소멸
			m_pFontSurface->Release();
		}
		m_pTextureHDC = NULL_PTR;
		m_pFontSurface = NULL_PTR;

		//폰트 출력에 사용한 텍스쳐 역시 삭제한다.
		EPN_TextureInterface::CreateInstance()->DeleteTexture(m_FontTextureIndex);
		m_FontTextureIndex = -1; //삭제 후 -1로 다시 초기화
		return TRUE;
	}
	
	return FALSE;
}
BOOL EPN_DXFont::ResetFont(VOID* RenderDevice, INT32 DeviceIndex)
{
	if (0 > DeviceIndex)
	{
		EPN_CHAR temp[32] = { 0, };
		estrcpy_s(temp, GetFont()->FontName.GetStr());
		//strcpy_s(temp, sizeof(temp), GetFont()->FontName.GetStr());
		return SetFont(RenderDevice, temp, GetFont()->FontSize, GetFont()->FontThickness);
	}
	else if (DeviceIndex == GetDeviceIndex())
	{
		//PRINT_LOG
		EPN_CHAR temp[32] = { 0, };
		estrcpy_s(temp, GetFont()->FontName.GetStr());
		//strcpy_s(temp, sizeof(temp), GetFont()->FontName.GetStr());
		return SetFont(RenderDevice, temp, GetFont()->FontSize, GetFont()->FontThickness);
	}

	return FALSE;
}

EPN_Pos EPN_DXFont::GetETextAreaSize(estring EText, BOOL EndlnFlag)
{
	EPN_Pos ResultSize = EPN_Pos(0, 0);

	if (NULL_PTR != m_pHFont)
	{
		if (0 >= EText.Length())
			return EPN_Pos(0.f, GetETextAreaSize(L" ", TRUE).Y);

		for (UINT32 i = 0; i < EText.Length(); ++i)
		{
			wchar_t wc[2] = { EText.GetStrW()[i] , 0 };
			INT32 Index = m_SizeDataSI.SearchIndex(wc);
			//사이즈가 있으면
			if (0 <= Index)
			{
				EPN_Pos Size = m_mpSizeData.GetData(Index);
				//개행 일 시
				if (wc[0] == L'\n')
				{
					ResultSize.Y += Size.Y;
					if (TRUE == EndlnFlag)
						ResultSize.X = 0;
				}
				if (0 == ResultSize.Y)
					ResultSize.Y += Size.Y;

				ResultSize.X += Size.X;
			}
			else
			{
				if (wc[0] == L'\n')
				{
					SIZE Size;
					//GetTextExtentPoint(m_pTextureHDC, L" ", 1, &Size);
					//주의 : 호출전 반드시 SelectObject로 폰트를 DC에 설정한 후 사용할 것
					GetTextExtentPoint(m_pTextureHDC, " ", 1, &Size);
					INT32 SaveIndex = m_mpSizeData.AddData(EPN_Pos(Size.cx, Size.cy));
					if (0 <= SaveIndex)
					{
						if (TRUE == m_SizeDataSI.InsertData(wc, SaveIndex))
						{
							ResultSize.Y += Size.cy;
							if (TRUE == EndlnFlag)
								ResultSize.X = 0;

							ResultSize.X += Size.cx;
						}
						else
						{
							m_mpSizeData.EraseData(SaveIndex); //이건 포인터로 안넣었으니까 delete 없이 그냥 풀내에서만 지움
							PRINT_LOG(m_LogFlag, "( 폰트 문자열 분석실패 )\n");
						}

					}
					else
					{
						PRINT_LOG(m_LogFlag, "( 폰트 문자열 분석실패 )\n");
					}
				}
				else
				{
					SIZE Size;
					//GetTextExtentPoint(m_pTextureHDC, L" ", 1, &Size);
					//주의 : 호출전 반드시 SelectObject로 폰트를 DC에 설정한 후 사용할 것
					GetTextExtentPoint(m_pTextureHDC, " ", 1, &Size);
					INT32 SaveIndex = m_mpSizeData.AddData(EPN_Pos(Size.cx, Size.cy));
					if (0 <= SaveIndex)
					{
						if (TRUE == m_SizeDataSI.InsertData(wc, SaveIndex))
						{
							ResultSize.X += Size.cx;
							if (0 == ResultSize.Y)
								ResultSize.Y += Size.cy;
						}
						else
						{
							m_mpSizeData.EraseData(SaveIndex); //포인터가 저장된게아니니 그냥 erase
							PRINT_LOG(m_LogFlag, "( 폰트 문자열 분석실패 )\n");
						}
					}
					else
					{
						PRINT_LOG(m_LogFlag, "( 폰트 문자열 분석실패 )\n");
					}
				}

			}
		}
	}

	return ResultSize;
}