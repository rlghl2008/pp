#pragma once

#include "../../../Common/Common.h"

using namespace EPN::COMMON::STRUCTURE;

namespace EPN
{
	namespace GENERIC
	{
		namespace FONT
		{
			CONST INT32 EPN_FONT_NOMAL = 400;
			CONST INT32 EPN_FONT_BOLD = 700;

			struct EPN_FontData
			{
				estring FontName;
				INT32 FontSize;
				INT32 FontThickness;
				EPN_FontData(estring _FontName = E_TEXT(""), INT32 _FontSize = 0, INT32 _FontThickness = 0)
				{
					FontName = _FontName;
					FontSize = _FontSize;
					FontThickness = _FontThickness;
				}
			};

			struct EPN_ETextColorData
			{
				estring EText;
				EPN_ARGB Color;
				EPN_ETextColorData(estring EText = "", EPN_ARGB Color = EPN_ARGB(255, 255, 255, 255))
				{
					this->EText = EText;
					this->Color = Color;
				}
			};

			class EPN_Font
			{
			public:
				EPN_Font();
				virtual ~EPN_Font();
			protected:
				BYTE m_LogFlag;
			public:
				VOID		SetLogFlag(BYTE LogFlag);
				BYTE		GetLogFlag();

			/* 폰트 관련 */
			private:
				INT32 m_DeviceIndex;
				EPN_FontData m_FontData;

			public:
				VOID SetDeviceIndex(INT32 Index);
				INT32 GetDeviceIndex();

			public:
				VOID SetFontData(EPN_FontData FontData);
				EPN_FontData* GetFontData();

			/* ETextColor 관련*/
			private:
				EPN::COMMON::EPN_MemoryPool<EPN_ETextColorData*>		m_mpTextColor;
				EPN::COMMON::EPN_StringIndexPool						m_TextColorSI;	
			public:
				BOOL SetETextColor(estring EText, EPN_ARGB Color);
				BOOL DeleteETextColor(estring EText);
				EPN_ARGB GetETextColor(estring EText);

				INT32 GetETextColorCount();

			/* ETextColor 관련*/
			protected:
				EPN::COMMON::EPN_MemoryPool<EPN_Pos> m_mpSizeData;
				EPN::COMMON::EPN_StringIndexPool m_SizeDataSI;

			/* 순수 가상 함수*/
			public:
				virtual BOOL BltText(estring Text, EPN_Rect Rect, EPN_ARGB Color = EPN_ARGB(255, 0, 0, 0), INT32 Buffer = -1) = 0;
				virtual BOOL BltText(INT32 DestTextureIndex, estring Text, EPN_Rect Rect, EPN_ARGB FontColor = EPN_ARGB(255, 0, 0, 0), EPN_ARGB BackColor = EPN_ARGB(0, 0, 0, 0), INT32 Buffer = -1) = 0;

				virtual BOOL SetFont(VOID* RenderDevice, estring FontName, INT32 FontSize, UINT32 FontThickness = EPN_FONT_NOMAL) = 0;
				virtual EPN_FontData* GetFont() = 0;
				virtual BOOL SetFontName(estring FontName) = 0;
				virtual estring GetFontName() = 0;
				virtual BOOL SetFontSize(INT32 FontSize) = 0;
				virtual INT32 GetFontSize() = 0;
				virtual BOOL SetFontThickness(INT32 FontThickness) = 0;
				virtual INT32 GetFontThickness() = 0;

				virtual BOOL ClearFont(INT32 DeviceIndex = -1) = 0;
				virtual BOOL ResetFont(VOID* RenderDevice, INT32 DeviceIndex = -1) = 0;

				virtual EPN_Pos GetETextAreaSize(estring EText, BOOL EndlnFlag = FALSE) = 0;
			};
		}
	}
}