#pragma once

//#include "EPN_Font.h" //나머지 요소는 이안에 어차피 다있음
#include "../../../Common/Common.h"
#include "../Device/EPN_DeviceInterface.h"

//typedef cocos2d::Label EPN_Font;
#define EPN_FONT_NORMAL 400

namespace EPN
{
	namespace GENERIC
	{
		namespace FONT
		{
			enum class FONT_TYPE
			{
				SYSTEM,
				TTF,
			};
			struct EPN_Font
			{
				EPN::COMMON::EPN_MemoryPool<Label*> m_mpLabel;
				INT32 BltCount;
				FONT_TYPE FontType;
				estring FontPath;

				EPN_Font()
				{
					BltCount = 0;
					FontType = FONT_TYPE::SYSTEM;
				}
				EPN_Font(FONT_TYPE _FontType)
				{
					BltCount = 0;
					FontType = _FontType;
				}
				~EPN_Font()
				{
					//전부 지우는 처리
					for (INT32 Cnt = 0, Cnt2 = 0; Cnt2 < m_mpLabel.GetAddDataCount(); ++Cnt)
					{
						if (FALSE == m_mpLabel.IsData(Cnt))
							continue;
						m_mpLabel.GetData(Cnt)->release();
						++Cnt2;
					}
				}
			};

			class EPN_FontInterface
			{
			private:
				static EPN_FontInterface* m_pFontInterface;
			public:
				static EPN_FontInterface* GetInstance();
				static EPN_FontInterface* CreateInstance(VOID);
				static VOID ReleaseInstance();

			public:
				EPN_FontInterface();
				virtual ~EPN_FontInterface();

				/* 로그 관련 */
			private:
				BYTE m_LogFlag;
			public:
				VOID      SetLogFlag(BYTE LogFlag, BOOL AutoFlag = TRUE);
				BYTE      GetLogFlag();


				/* 풀 */
			private:
				EPN::COMMON::EPN_MemoryPool<EPN_Font*> m_mpFont;
				EPN::COMMON::EPN_StringIndexPool m_StringIndexPool;

				/* 사용 */
			public:
				//Font 를 로드하여 인터페이스 메모리풀에 메모리등록후 위치인덱스반환
				//설정할 디바이스,폰트이름(궁서체 등등),폰트크기,폰트굵기(EPN_FONT_NOMAL,EPN_FONT_BOLD 가있음)
				INT32 LoadSystemFont(estring FontName, INT32 FontSize);
				INT32 LoadSystemFont(estring SaveFontName, estring FontName, INT32 FontSize);

				INT32 LoadFont(estring FontName, estring Path, INT32 FontSize);
				INT32 LoadFont(estring SaveFontName, estring Path, estring FontName, INT32 FontSize);

			public:
				//출력한 폰트의 인덱스,출력 메세지,출력할 영역,출력 RGB정보,메시지최대길이 -1시 동적으로 끝까지처리
				BOOL BltText(INT32 FontIndex, INT32 LayerIndex, estring Text, EPN_Pos Pos, EPN_ARGB Color = EPN_ARGB(255, 0, 0, 0));
				//출력한 폰트의 식별 문자열,출력 메세지,출력할 영역,출력 RGB정보,메시지최대길이 -1시 동적으로 끝까지처리
				BOOL BltText(estring SaveFontName, INT32 LayerIndex, estring Text, EPN_Pos Pos, EPN_ARGB Color = EPN_ARGB(255, 0, 0, 0));

			public://작업할것들
				BOOL SetFontSize(INT32 FontIndex, INT32 FontSize);
				BOOL SetFontSize(estring SaveFontName, INT32 FontSize);
				INT32 GetFontSize(INT32 FontIndex);
				INT32 GetFontSize(estring SaveFontName);

			public://EPN_StringIndexPool 객체적응
				INT32 GetFontIndex(estring SaveFontName);
				estring GetFontName(INT32 FontIndex);

			public://EPN_MP 객체적응
				EPN_Font* GetFont(INT32 FontIndex);
				EPN_Font* GetFont(estring SaveFontName);
				BOOL DeleteFont(INT32 FontIndex);
				BOOL DeleteFont(estring SaveFontName);
				BOOL AllDeleteFont();
				INT32 GetAddDataCount();//현재 인터페이스에 로드되어 메모리에 올라가있는 폰트 개수 반환

				EPN_Pos GetETextAreaSize(INT32 FontIndex, estring EText);
				EPN_Pos GetETextAreaSize(estring SaveFontName, estring EText);
			};

		}
	}
}