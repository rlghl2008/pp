#pragma once
#include "../../../Common/Common.h"
#include "../../Global/Global.h"
#include "EPN_Font.h"

namespace EPN
{
	namespace GENERIC
	{
		namespace FONT
		{
			class EPN_DXFont : public EPN_Font
			{
			public:
				EPN_DXFont();
				~EPN_DXFont();
			private:
				LPDIRECT3DDEVICE9				m_pD3DFontDevice; //렌더링에 사용될 D3D 디바이스 객체
			private:
				D3DXFONT_DESC					m_FontInfo;
				ID3DXFont *						m_pID3DFont;
				HFONT								m_pHFont;
				HDC									m_pTextureHDC;
				LPDIRECT3DSURFACE9			m_pFontSurface;
				INT32									m_FontTextureIndex;

			/*순수 가상 함수*/
			public:
				virtual BOOL BltText(estring Text, EPN_Rect Rect, EPN_ARGB Color = EPN_ARGB(255, 0, 0, 0), INT32 Buffer = -1);
				virtual BOOL BltText(INT32 DestTextureIndex, estring Text, EPN_Rect Rect, EPN_ARGB FontColor = EPN_ARGB(255, 0, 0, 0), EPN_ARGB BackColor = EPN_ARGB(0, 0, 0, 0), INT32 Buffer = -1);

				virtual BOOL SetFont(VOID* RenderDevice, estring FontName, INT32 FontSize, UINT32 FontThickness = EPN_FONT_NOMAL);
				virtual EPN_FontData* GetFont();
				virtual BOOL SetFontName(estring FontName);
				virtual estring GetFontName();
				virtual BOOL SetFontSize(INT32 FontSize);
				virtual INT32 GetFontSize();
				virtual BOOL SetFontThickness(INT32 FontThickness);
				virtual INT32 GetFontThickness();

				virtual BOOL ClearFont(INT32 DeviceIndex = -1);
				virtual BOOL ResetFont(VOID* RenderDevice, INT32 DeviceIndex = -1);

				virtual EPN_Pos GetETextAreaSize(estring EText, BOOL EndlnFlag = FALSE);
			};
		}
	}
}