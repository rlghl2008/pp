#include "EPN_Font.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::FONT;
#pragma endregion

EPN_Font::EPN_Font()
{
	m_LogFlag = (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON;

	m_DeviceIndex = -1;


}
EPN_Font::~EPN_Font()
{

}

VOID EPN_Font::SetLogFlag(BYTE LogFlag)
{
	m_LogFlag = LogFlag;
}
BYTE EPN_Font::GetLogFlag()
{
	return m_LogFlag;
}
VOID EPN_Font::SetDeviceIndex(INT32 Index)
{
	m_DeviceIndex = Index;
}
INT32 EPN_Font::GetDeviceIndex()
{
	return m_DeviceIndex;
}
VOID EPN_Font::SetFontData(EPN_FontData FontData)
{
	m_FontData = FontData;
}
EPN_FontData* EPN_Font::GetFontData()
{
	return &m_FontData;
}

BOOL EPN_Font::SetETextColor(estring EText, EPN_ARGB Color)
{
	INT32 Index = m_TextColorSI.SearchIndex(EText);
	//해당 글자를 못찾으면 추가한다.
	if (-1 == Index)
	{
		Index = m_mpTextColor.AddData(new EPN_ETextColorData(EText, Color));
		if (FALSE == m_TextColorSI.InsertData(EText, Index))
		{
			delete m_mpTextColor.GetData(Index);
			m_mpTextColor.EraseData(Index);
			PRINT_LOG(m_LogFlag, "( InsertData 실패! )\n");
			return FALSE;
		}
	}
	//이미 그 문장(혹은 단어가) 있으면 그냥 바꿔준다.
	else
	{
		EPN_ETextColorData* pData = m_mpTextColor.GetData(Index);
		if (NULL_PTR == pData)
		{
			PRINT_LOG(m_LogFlag, "( SearchData 실패! )\n");
			return FALSE;
		}
		pData->Color = Color;
	}
	return TRUE;
}
BOOL EPN_Font::DeleteETextColor(estring EText)
{
	INT32 Index = m_TextColorSI.SearchIndex(EText);
	if (0 <= Index)
	{
		m_TextColorSI.DeleteData(Index);
		delete m_mpTextColor.GetData(Index);
		m_mpTextColor.EraseData(Index);
		return TRUE;
	}
	return FALSE;
}
EPN_ARGB EPN_Font::GetETextColor(estring EText)
{
	INT32 Index = m_TextColorSI.SearchIndex(EText);
	EPN_ETextColorData* pData = m_mpTextColor.GetData(Index);
	if (NULL_PTR == pData)
		return EPN_ARGB(-1, -1, -1, -1);

	return pData->Color;
}

INT32 EPN_Font::GetETextColorCount()
{
	return m_mpTextColor.GetAddDataCount();
}