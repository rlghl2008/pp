#include <iostream>
using namespace std;

#include "EPN_FPS.h"
#include <ctime>

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::UTIL;
#pragma endregion

EPN_FPS :: EPN_FPS()
{
	m_LogFlag = (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON;

	memset(&m_SecClock, 0, sizeof(m_SecClock));
	memset(&m_InitSecClock, 0 ,sizeof(m_InitSecClock));
	memset(&m_TimerSecClock, 0 ,sizeof(m_TimerSecClock));
	memset(&m_VarFrame, 0, sizeof(m_VarFrame));
	m_RunTime = 0;
	m_InitTime = 0;
	m_SecFrame = 0;
	m_Frame = 0;
	m_TimerUseableFlag = FALSE;
}
EPN_FPS :: ~EPN_FPS()
{
}

VOID EPN_FPS::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}
BYTE EPN_FPS::GetLogFlag()
{
	return m_LogFlag;
}
	
BOOL EPN_FPS::Run(UINT32 SetFPS)
{
	if (TRUE == m_TimerUseableFlag)
	{
		QueryPerformanceCounter(&m_SecClock);
		//NON 플래그가 설정이 안되있는 경우
		if ((BYTE)EPN::COMMON::EPN_LOG_FLAG::NON != m_LogFlag)
		{
			//1초 마다 프레임을 재서 업데이트
			if (m_SecClock.QuadPart >= m_TimerSecClock.QuadPart + m_InitSecClock.QuadPart)
			{
				if (FALSE == m_Timer.IsTimerStart())
					m_Timer.Start();
				m_RunTime = m_Timer.GetTime();

				PRINT_LOG(m_LogFlag, "( Sec_Frame [ %d ] Sec [ %d ] _M_Sec [ %d ] )\n", (INT32)m_SecFrame, (INT32)m_RunTime, (INT32)m_RunTime * 1000);

				m_Frame = m_SecFrame;
				m_SecFrame = 0;
				m_TimerSecClock = m_SecClock;

			}
		}
	}
	else
	{
		//타이머 사용이 가능할 경우
		//
		if(TRUE == QueryPerformanceFrequency(&m_InitSecClock))
		{
			QueryPerformanceCounter(&m_TimerSecClock);
			m_TimerUseableFlag = TRUE; //타이머 사용가능
		}
		else
		{
			PRINT_LOG(m_LogFlag, "( CPU 클럭을 얻어올수 없습니다. )\n");
		}
	}
	
	if(m_SecClock.QuadPart >= m_VarFrame)
	{
		//로그 플래그가 NON 으로 설정 안된경우에만
		if( (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON != m_LogFlag )
			m_SecFrame++;

		m_VarFrame = m_SecClock.QuadPart + m_InitSecClock.QuadPart / SetFPS;
		return TRUE;
	}

	return FALSE;
}

UINT32 EPN_FPS :: GetFrame()
{
	return m_Frame;
}
