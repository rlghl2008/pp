#pragma once

#include <windows.h>
#include "../../../Common/Class/EPN_Timer.h"
#include "../../../Common/Base/CommonFunction.h"

namespace EPN
{
	namespace GENERIC
	{
		namespace UTIL
		{
			class EPN_FPS
			{
			public:
				EPN_FPS();
				~EPN_FPS();
			private:
				BYTE m_LogFlag;
			public:
				VOID		SetLogFlag(BYTE LogFlag);
				BYTE		GetLogFlag();
			private:
				EPN::COMMON::EPN_Timer		m_Timer;
			private:
				LARGE_INTEGER	m_SecClock;
				LARGE_INTEGER	m_InitSecClock;
				LARGE_INTEGER	m_TimerSecClock;
				INT64					m_VarFrame;

				REAL64		m_RunTime;
				INT32			m_InitTime;
				INT32			m_SecFrame;
				INT64			m_TimerUseableFlag;
			public:
				BOOL		Run(UINT32 FPS = 60);

			private:
				UINT32	m_Frame;
			public:
				UINT32	GetFrame();
			};
		}
	}
}