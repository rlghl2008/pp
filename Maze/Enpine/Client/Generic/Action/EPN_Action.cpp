#include "EPN_Action.h"
#include "../Texture/EPN_TextureInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::COMMON::STRUCTURE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::ACTION;
#pragma endregion


EPN_Action::EPN_Action()
{
	m_pActionData = NULL_PTR;
	m_ActionFlag = FALSE;
	m_ActionDataCount = 0;
	m_ActionDelayCount = 0;
	m_ActionNum = 0;
	m_LogFlag = m_LogFlag = (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON;;
}
EPN_Action :: ~EPN_Action()
{
	if (TRUE == m_ActionFlag)
	{
		delete[]m_pActionData;
	}
}
VOID EPN_Action::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_Action::GetLogFlag()
{
	return m_LogFlag;
}


BOOL EPN_Action::AutoAction(EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH)
{
	m_ActionDataCount = 0;
	if (TRUE == m_ActionFlag)
	{
		delete[]m_pActionData;
		m_pActionData = NULL;
	}
	m_ActionFlag = TRUE;

	m_ActionDataCount = (((INT32)Rect.Bottom.Y - (INT32)Rect.Top.Y) / ScopeH) * (((INT32)Rect.Bottom.X - (INT32)Rect.Top.X) / ScopeW);

	m_pActionData = new EPN_ActionData[m_ActionDataCount];
	INT32 Cnt = 0;
	for (INT32 j = (INT32)Rect.Top.Y; j < (INT32)Rect.Bottom.Y;)
	{
		for (INT32 k = (INT32)Rect.Top.X; k < (INT32)Rect.Bottom.X;)
		{
			m_pActionData[Cnt].Rect.Top.X = (REAL32)k;
			m_pActionData[Cnt].Rect.Top.Y = (REAL32)j;
			m_pActionData[Cnt].Rect.Bottom.X = (REAL32)k + ScopeW;
			m_pActionData[Cnt].Rect.Bottom.Y = (REAL32)j + ScopeH;
			k += ScopeW;
			Cnt++;
		}
		j += ScopeH;
	}
	return TRUE;
}
BOOL EPN_Action::AutoAction(INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* Temp = EPN_TextureInterface::CreateInstance();
	EPN_Texture* EPN_Texture = Temp->GetTexture(TextureIndex);
	if (NULL_PTR == EPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 EPN_Texture Index [ %d ] 입니다. )", TextureIndex);

		return FALSE;
	}
	return AutoAction(Temp->GetTextureSize(TextureIndex), ScopeW, ScopeH);
}
BOOL EPN_Action::AutoAction(estring SaveTextureName, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* Temp = EPN_TextureInterface::CreateInstance();
	EPN_Texture* EPN_Texture = Temp->GetTexture(SaveTextureName);
	if (NULL_PTR == EPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveTextureName [ %s ] 입니다. )", SaveTextureName.GetStr());

		return FALSE;
	}
	return AutoAction(Temp->GetTextureSize(SaveTextureName), ScopeW, ScopeH); //GetTextureSize
}

BOOL EPN_Action::SetActionData(INT32 Index, EPN_ActionData m_pActionData)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");

		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);

		return FALSE;
	}
	this->m_pActionData[Index] = m_pActionData;
	return TRUE;
}

EPN_ActionData* EPN_Action::GetActionData(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 데이터는 존재하지 않습니다. )", Index);
		return NULL;
	}

	return &m_pActionData[Index];
}
BOOL EPN_Action::SetActionPos(INT32 Index, EPN_Pos Pos)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");
		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);
		return FALSE;
	}
	m_pActionData[Index].Pos = Pos;
	return TRUE;
}
EPN_Pos EPN_Action::GetActionPos(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 데이터는 존재하지 않습니다. )", Index);

		return EPN_Pos(0, 0);
	}

	return m_pActionData[Index].Pos;
}

BOOL EPN_Action::SetActionRect(INT32 Index, EPN_Rect Rect)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");

		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);
		return FALSE;
	}
	m_pActionData[Index].Rect = Rect;
	return TRUE;
}
EPN_Rect EPN_Action::GetActionRect(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 데이터는 존재하지 않습니다. )", Index);

		return EPN_Rect(0, 0, 0, 0);
	}

	return m_pActionData[Index].Rect;
}

BOOL EPN_Action::SetActionScaling(INT32 Index, EPN_Scaling Scaling)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");
		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);
		return FALSE;
	}
	m_pActionData[Index].Scaling = Scaling;
	return TRUE;
}
EPN_Scaling EPN_Action::GetActionScaling(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 데이터는 존재하지 않습니다. )", Index);
		return EPN_Scaling(0, 0);
	}

	return m_pActionData[Index].Scaling;
}
BOOL EPN_Action::SetActionAngle(INT32 Index, REAL32 Angle)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");
		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);
		return FALSE;
	}
	m_pActionData[Index].Angle = Angle;
	return TRUE;
}
REAL32 EPN_Action::GetActionAngle(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 데이터는 존재하지 않습니다. )", Index);
		return 0;
	}

	return m_pActionData[Index].Angle;
}

BOOL EPN_Action::SetActionColor(INT32 Index, EPN_ARGB Color)
{
	if (FALSE == m_ActionFlag)
	{
		PRINT_LOG(m_LogFlag, "( m_ActionDataCount 가 설정되지 않았습니다. )");
		return FALSE;
	}
	if (0 > Index || Index >= m_ActionDataCount)
	{
		PRINT_LOG(m_LogFlag, "( Index [ %d ] 가 0 미만 이거나\nActionDataCount [ %d ] 를 초과 하였습니다. )", Index, m_ActionDataCount);
		return FALSE;
	}
	m_pActionData[Index].Color = Color;
	return TRUE;
}

EPN_ARGB EPN_Action::GetActionColor(INT32 Index)
{
	if (0 > Index || m_ActionDataCount <= Index)
	{
		PRINT_LOG(m_LogFlag, "( 인덱스 [ %d ] 영역은 존재하지 않습니다. )", Index);
		return 0;
	}

	return m_pActionData[Index].Color;
}
INT32 EPN_Action::GetActionDataCount()
{
	return m_ActionDataCount;
}
VOID EPN_Action::SetActionDataCount(INT32 MaxValue)
{
	if (m_ActionDataCount != MaxValue)
	{
		if (TRUE == m_ActionFlag)
		{
			delete[]m_pActionData;
			m_pActionData = NULL_PTR;
		}
		m_ActionDataCount = MaxValue;
		m_pActionData = new EPN_ActionData[m_ActionDataCount];
		memset(m_pActionData, 0, sizeof(m_pActionData));
		m_ActionFlag = TRUE;
	}
}
EPN_ActionData* EPN_Action::Action(INT32 DelayCount)
{
	if (DelayCount <= m_ActionDelayCount)
	{
		m_ActionDelayCount = 0;
		m_ActionNum++;
		if (m_ActionNum >= m_ActionDataCount)
		{
			m_ActionNum = 0;
		}
	}
	else
		m_ActionDelayCount++;
	return GetActionData(m_ActionNum);
}
EPN_ActionData* EPN_Action::Action(INT32 DelayCount, INT32 StartIndex, INT32 EndIndex)
{
	if (StartIndex == EndIndex)
	{
		m_ActionNum = StartIndex;
		return GetActionData(StartIndex);
	}
	else if (StartIndex < EndIndex)
	{
		if (m_ActionNum < StartIndex || m_ActionNum > EndIndex)
		{
			m_ActionNum = StartIndex;
			m_ActionDelayCount = 0;
		}
	}
	else if (StartIndex > EndIndex)
	{
		if (m_ActionNum > StartIndex || m_ActionNum < EndIndex)
		{
			m_ActionNum = StartIndex;
			m_ActionDelayCount = 0;
		}
	}
	if (DelayCount <= m_ActionDelayCount)
	{
		m_ActionDelayCount = 0;

		if (StartIndex < EndIndex)
		{
			m_ActionNum++;
			if (m_ActionNum >= m_ActionDataCount || 0 > m_ActionNum || m_ActionNum < StartIndex || m_ActionNum > EndIndex)
			{
				m_ActionNum = StartIndex;
			}
		}
		else if (StartIndex > EndIndex)
		{
			m_ActionNum--;
			if (m_ActionNum >= m_ActionDataCount || 0 > m_ActionNum || m_ActionNum > StartIndex || m_ActionNum < EndIndex)
			{
				m_ActionNum = StartIndex;
			}
		}

	}
	else
		m_ActionDelayCount++;
	return GetActionData(m_ActionNum);
}
BOOL EPN_Action::SetActionNum(INT32 m_ActionNum)
{
	m_ActionDelayCount = 0;
	this->m_ActionNum = m_ActionNum;
	return TRUE;
}
INT32 EPN_Action::GetActionNum()
{
	return m_ActionNum;
}
VOID EPN_Action::SetActionDelayCount(INT32 DelayCount)
{
	m_ActionDelayCount = DelayCount;
}
INT32 EPN_Action::GetActionDelayCount()
{
	return m_ActionDelayCount;
}
BOOL EPN_Action::ClearAction()
{
	if (TRUE == m_ActionFlag)
	{
		delete[]m_pActionData;
		m_pActionData = NULL_PTR;
		m_ActionFlag = FALSE;
	}
	m_ActionDataCount = 0;
	return TRUE;
}