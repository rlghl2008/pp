#pragma once
#include "../../../Common/Common.h"

namespace EPN
{
	namespace GENERIC
	{
		namespace ACTION
		{
			struct EPN_ActionData;

			class EPN_Action
			{
			public:
				EPN_Action();
				~EPN_Action();
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private:
				EPN_ActionData* m_pActionData;
				BOOL m_ActionFlag;
				INT32 m_ActionDataCount;
			private:
				INT32 m_ActionDelayCount;
				INT32 m_ActionNum;//현재 반환스프라이트 번호
			public:
				//직접 영역을 선택하여 원하는 기준으로 자동으로 자르고 그개수만큼 액션데이터 생성후 각각 렉트값 저장
				BOOL AutoAction(EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH);

				//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로 자르고 그개수만큼 액션데이터 생성후 각각 렉트값 저장
				BOOL AutoAction(INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH);
				BOOL AutoAction(estring SaveTextureName, INT32 ScopeW, INT32 ScopeH);

				//만들어진 액션데이터 개수 가져오기
				INT32 GetActionDataCount();
				VOID SetActionDataCount(INT32 MaxValue);

				//액션기능 딜레이 카운터에 맞춰 자동으로 넘겨서 액션데이터를 반환해줌
				EPN_ActionData* Action(INT32 DelayCount);
				EPN_ActionData* Action(INT32 DelayCount, INT32 StartIndex, INT32 EndIndex);
				BOOL SetActionNum(INT32 m_ActionNum);
				INT32 GetActionNum();
				VOID SetActionDelayCount(INT32 DelayCount);
				INT32 GetActionDelayCount();

				//입력한 번호에 데이터 설정
				BOOL SetActionData(INT32 Index, EPN_ActionData m_pActionData);
				//입력한 번호에 저장되어있는 데이터 가져오기
				EPN_ActionData* GetActionData(INT32 Index);

				//입력한 번호에 위치 설정
				BOOL SetActionPos(INT32 Index, EPN_Pos Pos);
				//입력한 번호에 저장되어있는 위치 가져오기
				EPN_Pos GetActionPos(INT32 Index);

				//입력한 번호에 렉트영역 설정
				BOOL SetActionRect(INT32 Index, EPN_Rect Rect);
				//입력한 번호에 저장되어있는 렉트영역 가져오기
				EPN_Rect GetActionRect(INT32 Index);

				//입력한 번호에 EPN_ARGB Color 설정
				BOOL SetActionColor(INT32 Index, EPN_ARGB Color);
				//입력한 번호에 EPN_ARGB Color 가져오기
				EPN_ARGB GetActionColor(INT32 Index);

				//입력한 번호에 각도 설정
				BOOL SetActionAngle(INT32 Index, REAL32 Angle);
				//입력한 번호에 각도 가져오기
				REAL32 GetActionAngle(INT32 Index);

				//입력한 번호에 확대/축소 비율 설정
				BOOL SetActionScaling(INT32 Index, EPN_Scaling Scaling);
				//입력한 번호에 확대/축소 비율 가져오기
				EPN_Scaling GetActionScaling(INT32 Index);


				BOOL ClearAction();
			};

			struct EPN_ActionData
			{
				EPN_Pos Pos;
				EPN_Rect Rect;
				EPN_ARGB Color;
				REAL32 Angle;
				EPN_Scaling Scaling;
				INT32 DelayCount;

				EPN_ActionData(EPN_Pos Pos = 0, EPN_Rect Rect = 0, EPN_ARGB Color = EPN_ARGB(255, 255, 255, 255), REAL32 Angle = 0, EPN_Scaling Scaling = 0, INT32 DelayCount = 0)
				{
					this->Pos = Pos;
					this->Rect = Rect;
					this->Color = Color;
					this->Angle = Angle;
					this->Scaling = Scaling;
					this->DelayCount = DelayCount;
				}
			};
		}
	}
}