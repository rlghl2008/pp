#include "EPN_ActionInterface.h"
#include "../Texture/EPN_TextureInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::COMMON::STRUCTURE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::ACTION;
#pragma endregion

EPN_ActionInterface* EPN_ActionInterface::m_pActionInterface = NULL_PTR;

EPN_ActionInterface::EPN_ActionInterface()
{
}

EPN_ActionInterface :: ~EPN_ActionInterface()
{
}

EPN_ActionInterface* EPN_ActionInterface::GetInstance()
{
	return m_pActionInterface;
}

VOID EPN_ActionInterface::ReleaseInstance(VOID)
{
	delete m_pActionInterface;
	m_pActionInterface = NULL_PTR;
}
EPN_ActionInterface* EPN_ActionInterface::CreateInstance(VOID)
{
	if (NULL_PTR == m_pActionInterface)
	{
		m_pActionInterface = new EPN_ActionInterface;
	}
	return m_pActionInterface;
}
VOID EPN_ActionInterface::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_ActionInterface::GetLogFlag()
{
	return m_LogFlag;
}

INT32 EPN_ActionInterface::AutoAction(EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH)
{
	INT32 Index = m_mpAction.AddData(EPN_Action());
	EPN_Action* pAction = NULL_PTR;
	pAction = m_mpAction[Index];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 EPN_Action Index [ %d ] 입니다. )\n", Index);
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ActionInterface :: AutoAction \n( InsertData 문제발생 )");
		return -1;
	}
	if (FALSE == pAction->AutoAction(Rect, ScopeW, ScopeH))
	{
		//InfoPrint.InfoPrintln("[E P N] Error - EPN_ActionInterface :: AutoAction \n( AutoAction 문제발생 )");
		m_mpAction.EraseData(Index);
		return -1;
	}
	return Index;
}
INT32 EPN_ActionInterface::AutoAction(estring SaveActionName, EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH)
{
	INT32 Index = AutoAction(Rect, ScopeW, ScopeH);
	if (-1 < Index)
	{

		if (FALSE == m_StringIndexPool.InsertData(SaveActionName, Index))
		{
			PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveActionName [ %s ] 이거나 문자열길이가 %d 를 초과 하였습니다. )\n", SaveActionName, EPN_SI_STRLEN_MAX);
			DeleteAction(Index);
			return -1;
		}
	}
	return Index;
}
INT32 EPN_ActionInterface::AutoAction(INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* pTI = EPN_TextureInterface::CreateInstance();
	EPN_Texture* pEPN_Texture = pTI->GetTexture(TextureIndex);
	if (NULL_PTR == pEPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 TextureIndex [ %d ] 입니다. )\n", TextureIndex);
		return -1;
	}

	return AutoAction(pTI->GetTextureSize(TextureIndex), ScopeW, ScopeH);
}
INT32 EPN_ActionInterface::AutoAction(estring SaveTextureName, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* pTI = EPN_TextureInterface::CreateInstance();
	EPN_Texture* pEPN_Texture = pTI->GetTexture(SaveTextureName);
	if (NULL_PTR == pEPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveTextureName [ %s ] 입니다. )\n", SaveTextureName);
		return -1;
	}
	return AutoAction(pTI->GetTextureSize(SaveTextureName), ScopeW, ScopeH);
}
INT32 EPN_ActionInterface::AutoAction(estring SaveActionName, INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* pTI = EPN_TextureInterface::CreateInstance();
	EPN_Texture* pEPN_Texture = pTI->GetTexture(TextureIndex);
	if (NULL_PTR == pEPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 TextureIndex [ %d ] 입니다. )\n", TextureIndex);
		return -1;
	}
	INT32 Index = AutoAction(pTI->GetTextureSize(TextureIndex), ScopeW, ScopeH);
	if (Index < 0)
	{
		return -1;
	}
	if (FALSE == m_StringIndexPool.InsertData(SaveActionName, Index))
	{
		PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveActionName [ %s ] 이거나 문자열길이가 %d 를 초과 하였습니다. )\n", SaveActionName, EPN_SI_STRLEN_MAX);
		DeleteAction(Index);
		return -1;
	}
	return Index;
}
INT32 EPN_ActionInterface::AutoAction(estring SaveActionName, estring SaveTextureName, INT32 ScopeW, INT32 ScopeH)
{
	EPN_TextureInterface* pTI = EPN_TextureInterface::CreateInstance();
	EPN_Texture* pEPN_Texture = pTI->GetTexture(SaveTextureName);
	if (NULL_PTR == pEPN_Texture)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveTextureName [ %s ] 입니다. )\n", SaveTextureName);
		return FALSE;
	}
	INT32 Index = AutoAction(pTI->GetTextureSize(SaveTextureName), ScopeW, ScopeH);
	if (Index < 0)
	{
		return -1;
	}
	if (FALSE == m_StringIndexPool.InsertData(SaveActionName, Index))
	{
		PRINT_LOG(m_LogFlag, "( 이미 저장된 SaveActionName [ %s ] 이거나 문자열길이가 %d 를 초과 하였습니다. )\n", SaveActionName, EPN_SI_STRLEN_MAX);
		DeleteAction(Index);
		return -1;
	}
	return Index;
}

BOOL EPN_ActionInterface::SetActionData(INT32 ActionIndex, INT32 Index, EPN_ActionData ActionData)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionData(Index, ActionData);
}
BOOL EPN_ActionInterface::SetActionData(estring SaveActionName, INT32 Index, EPN_ActionData ActionData)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionData(Index, ActionData);
}
EPN_ActionData* EPN_ActionInterface::GetActionData(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return NULL_PTR;
	}
	return pAction->GetActionData(Index);
}
EPN_ActionData* EPN_ActionInterface::GetActionData(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return NULL_PTR;
	}
	return pAction->GetActionData(Index);
}


BOOL EPN_ActionInterface::SetActionPos(INT32 ActionIndex, INT32 Index, EPN_Pos Pos)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionPos(Index, Pos);
}
BOOL EPN_ActionInterface::SetActionPos(estring SaveActionName, INT32 Index, EPN_Pos Pos)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionPos(Index, Pos);
}
EPN_Pos EPN_ActionInterface::GetActionPos(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return EPN_Pos(0, 0);
	}
	return pAction->GetActionPos(Index);
}
EPN_Pos EPN_ActionInterface::GetActionPos(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return EPN_Pos(0, 0);
	}
	return pAction->GetActionPos(Index);
}

BOOL EPN_ActionInterface::SetActionRect(INT32 ActionIndex, INT32 Index, EPN_Rect Rect)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionRect(Index, Rect);
}
BOOL EPN_ActionInterface::SetActionRect(estring SaveActionName, INT32 Index, EPN_Rect Rect)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionRect(Index, Rect);
}
EPN_Rect EPN_ActionInterface::GetActionRect(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return EPN_Rect(0, 0, 0, 0);
	}
	return pAction->GetActionRect(Index);
}
EPN_Rect EPN_ActionInterface::GetActionRect(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return EPN_Rect(0, 0, 0, 0);
	}
	return pAction->GetActionRect(Index);
}


BOOL EPN_ActionInterface::SetActionScaling(INT32 ActionIndex, INT32 Index, EPN_Scaling Scaling)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionScaling(Index, Scaling);
}
BOOL EPN_ActionInterface::SetActionScaling(estring SaveActionName, INT32 Index, EPN_Scaling Scaling)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionScaling(Index, Scaling);
}
EPN_Scaling EPN_ActionInterface::GetActionScaling(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return EPN_Scaling(0, 0);
	}
	return pAction->GetActionScaling(Index);
}
EPN_Scaling EPN_ActionInterface::GetActionScaling(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return EPN_Scaling(0, 0);
	}
	return pAction->GetActionScaling(Index);
}



BOOL EPN_ActionInterface::SetActionAngle(INT32 ActionIndex, INT32 Index, REAL32 Angle)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionAngle(Index, Angle);
}
BOOL EPN_ActionInterface::SetActionAngle(estring SaveActionName, INT32 Index, REAL32 Angle)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionAngle(Index, Angle);
}
REAL32 EPN_ActionInterface::GetActionAngle(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->GetActionAngle(Index);
}
REAL32 EPN_ActionInterface::GetActionAngle(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return 0;
	}
	return pAction->GetActionAngle(Index);
}

BOOL EPN_ActionInterface::SetActionColor(INT32 ActionIndex, INT32 Index, EPN_ARGB Color)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionColor(Index, Color);
}
BOOL EPN_ActionInterface::SetActionColor(estring SaveActionName, INT32 Index, EPN_ARGB Color)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionColor(Index, Color);
}
EPN_ARGB EPN_ActionInterface::GetActionColor(INT32 ActionIndex, INT32 Index)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->GetActionColor(Index);
}
EPN_ARGB EPN_ActionInterface::GetActionColor(estring SaveActionName, INT32 Index)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return 0;
	}
	return pAction->GetActionColor(Index);
}

EPN_Action* EPN_ActionInterface::GetAction(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return NULL_PTR;
	}
	return pAction;
}
EPN_Action* EPN_ActionInterface::GetAction(estring SaveActionName)
{
	INT32 ActionIndex = m_StringIndexPool.SearchIndex(SaveActionName);
	if (0 > ActionIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return NULL_PTR;
	}
	return m_mpAction[ActionIndex];
}
INT32 EPN_ActionInterface::GetActionDataCount(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->GetActionDataCount();
}
INT32 EPN_ActionInterface::GetActionDataCount(estring SaveActionName)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return 0;
	}
	return pAction->GetActionDataCount();
}
VOID EPN_ActionInterface::SetActionDataCount(INT32 ActionIndex, INT32 MaxValue)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return;
	}
	return pAction->SetActionDataCount(MaxValue);
}
VOID EPN_ActionInterface::SetActionDataCount(estring SaveActionName, INT32 MaxValue)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return;
	}
	return pAction->SetActionDataCount(MaxValue);
}
EPN_ActionData* EPN_ActionInterface::Action(INT32 ActionIndex, INT32 DelayCount)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->Action(DelayCount);
}
EPN_ActionData* EPN_ActionInterface::Action(INT32 ActionIndex, INT32 DelayCount, INT32 StartIndex, INT32 EndIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->Action(DelayCount, StartIndex, EndIndex);
}
BOOL EPN_ActionInterface::SetActionNum(INT32 ActionIndex, INT32 ActionNum)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->SetActionNum(ActionIndex);
}
INT32 EPN_ActionInterface::GetActionNum(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->GetActionNum();
}
INT32 EPN_ActionInterface::GetActionDelayCount(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return 0;
	}
	return pAction->GetActionDelayCount();
}
EPN_ActionData* EPN_ActionInterface::Action(estring SaveActionName, INT32 DelayCount)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return 0;
	}
	return pAction->Action(DelayCount);
}
EPN_ActionData* EPN_ActionInterface::Action(estring SaveActionName, INT32 DelayCount, INT32 StartIndex, INT32 EndIndex)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return 0;
	}
	return pAction->Action(DelayCount, StartIndex, EndIndex);
}
BOOL EPN_ActionInterface::SetActionNum(estring SaveActionName, INT32 ActionNum)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->SetActionNum(ActionNum);
}
INT32 EPN_ActionInterface::GetActionNum(estring SaveActionName)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->GetActionNum();
}
INT32 EPN_ActionInterface::GetActionDelayCount(estring SaveActionName)
{
	EPN_Action* pAction = GetAction(SaveActionName);
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return pAction->GetActionDelayCount();
}
INT32 EPN_ActionInterface::GetActionIndex(estring SaveActionName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveActionName);
	if (0 > Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
	}
	return Index;
}
estring EPN_ActionInterface::GetActionStr(INT32 ActionIndex)
{
	return m_StringIndexPool.SearchString(ActionIndex);
}

BOOL EPN_ActionInterface::DeleteAction(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	pAction->ClearAction();
	m_mpAction.EraseData(ActionIndex);
	m_StringIndexPool.DeleteData(ActionIndex);
	return TRUE;
}
BOOL EPN_ActionInterface::DeleteAction(estring SaveActionName)
{
	INT32 ActionIndex = m_StringIndexPool.SearchIndex(SaveActionName);
	if (0 > ActionIndex)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return DeleteAction(ActionIndex);
}
BOOL EPN_ActionInterface::AllDeleteAction()
{
	EPN_Action* pAction = NULL_PTR;
	INT32 Count = 0;
	INT32 InserCount = GetAddDataCount();
	for (INT32 Index = 0; Count < InserCount; ++Index)
	{
		pAction = GetAction(Index);
		if (NULL_PTR == pAction) continue;
		++Count;
		DeleteAction(Index);
	}
	return TRUE;
}
INT32 EPN_ActionInterface::GetAddDataCount()
{
	return m_mpAction.GetAddDataCount();
}

BOOL EPN_ActionInterface::ClearAction(INT32 ActionIndex)
{
	EPN_Action* pAction = m_mpAction[ActionIndex];
	if (NULL_PTR == pAction)
	{
		PRINT_LOG(m_LogFlag, "( 존재하지않는 ActionIndex [ %d ] 입니다. )\n", ActionIndex);
		return FALSE;
	}
	return pAction->ClearAction();
}
BOOL EPN_ActionInterface::ClearAction(estring SaveActionName)
{
	INT32 Index = m_StringIndexPool.SearchIndex(SaveActionName);
	if (0 > Index)
	{
		PRINT_LOG(m_LogFlag, "( 저장되지않은 SaveActionName [ %s ] 입니다. )\n", SaveActionName);
		return FALSE;
	}
	return ClearAction(Index);
}
BOOL EPN_ActionInterface::AllClearAction()
{
	EPN_Action* pAction = 0;
	INT32 Count = 0;
	for (INT32 Index = 0; Count < GetAddDataCount(); ++Index)
	{
		pAction = GetAction(Index);
		if (NULL_PTR == pAction)
			continue;
		++Count;
		if (FALSE == pAction->ClearAction())
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 인덱스 번호의 스프라이트 Clear Failed! \n( 작업을 중단 합니다! )\n", (INT32)Index);
			return FALSE;
		}
	}
	return TRUE;
}