#pragma once

#include "EPN_Action.h"
#include "../../../Common/Class/EPN_MemoryPool.hpp"

namespace EPN
{
	namespace GENERIC
	{
		namespace ACTION
		{
			class EPN_ActionInterface
			{
			public:
				EPN_ActionInterface();
				virtual ~EPN_ActionInterface();
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private:
				EPN::COMMON::EPN_MemoryPool<EPN_Action>		m_mpAction;
				EPN::COMMON::EPN_StringIndexPool			m_StringIndexPool;
			public:
				//직접 영역을 선택하여 원하는 기준으로 자동으로 잘라 저장후 저장된 인덱스 반환 실패시 -1 반환

				//기준영역 EPN_Rect,잘라낼 가로,세로크기(픽셀기준)
				//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환
				INT32 AutoAction(EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH);
				//식별번호 저장할 문자열,기준영역 EPN_Rect,잘라낼 가로,세로크기(픽셀기준)
				//셋팅된 스프라이트 데이터를 메모리풀에 저장후 식별번호 반환 식별 문자열로도 검색가능
				INT32 AutoAction(estring SaveActionName, EPN_Rect Rect, INT32 ScopeW, INT32 ScopeH);

				//텍스쳐인터페이스에 저장된 텍스쳐이미지 크기를불러와 원하는 기준으로 자동으로자름
				INT32 AutoAction(INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
				INT32 AutoAction(estring SaveTextureName, INT32 ScopeW, INT32 ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름

				INT32 AutoAction(estring SaveActionName, INT32 TextureIndex, INT32 ScopeW, INT32 ScopeH);//텍스쳐 인덱스로 불러와 기준으로자름
				INT32 AutoAction(estring SaveActionName, estring SaveTextureName, INT32 ScopeW, INT32 ScopeH);//저장된 텍스쳐 스트링으로 불러와 기준으로자름

			public://EPN_StringIndexPool 객체적응
				INT32 GetActionIndex(estring SaveActionName);
				estring GetActionStr(INT32 SaveActionName);

			public://EPN_MP 객체적응
				EPN_Action* GetAction(INT32 ActionIndex);//인덱스로 찾아낸 스프라이트 가져오기
				EPN_Action* GetAction(estring SaveActionName);//스트링으로 찾아낸 스프라이트 가져오기
				BOOL DeleteAction(INT32 ActionIndex);
				BOOL DeleteAction(estring SaveActionName);
				BOOL AllDeleteAction();
				INT32 GetAddDataCount();//현재 인터페이스에 셋되어 메모리에 올라가있는 스프라이트 개수 반환

			public:
				//수동으로 액션데이터 설정
				BOOL SetActionData(INT32 ActionIndex, INT32 Index, EPN_ActionData ActionData);
				BOOL SetActionData(estring SaveActionName, INT32 Index, EPN_ActionData ActionData);
				EPN_ActionData* GetActionData(INT32 ActionIndex, INT32 Index);
				EPN_ActionData* GetActionData(estring SaveActionName, INT32 Index);

				BOOL SetActionPos(INT32 ActionIndex, INT32 Index, EPN_Pos Pos);
				BOOL SetActionPos(estring SaveActionName, INT32 Index, EPN_Pos Pos);
				EPN_Pos GetActionPos(INT32 ActionIndex, INT32 Index);
				EPN_Pos GetActionPos(estring SaveActionName, INT32 Index);

				BOOL SetActionRect(INT32 ActionIndex, INT32 Index, EPN_Rect Rect);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
				BOOL SetActionRect(estring SaveActionName, INT32 Index, EPN_Rect Rect);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 렉트영역 설정
				EPN_Rect GetActionRect(INT32 ActionIndex, INT32 Index);//인덱스로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기
				EPN_Rect GetActionRect(estring SaveActionName, INT32 Index);//스트링으로 찾아낸 스프라이트에서 입력한 번호에 저장되어있는 렉트영역 가져오기

				BOOL SetActionColor(INT32 ActionIndex, INT32 Index, EPN_ARGB Color);
				BOOL SetActionColor(estring SaveActionName, INT32 Index, EPN_ARGB Color);
				EPN_ARGB GetActionColor(INT32 ActionIndex, INT32 Index);
				EPN_ARGB GetActionColor(estring SaveActionName, INT32 Index);

				BOOL SetActionAngle(INT32 ActionIndex, INT32 Index, REAL32 Angle);
				BOOL SetActionAngle(estring SaveActionName, INT32 Index, REAL32 Angle);
				REAL32 GetActionAngle(INT32 ActionIndex, INT32 Index);
				REAL32 GetActionAngle(estring SaveActionName, INT32 Index);

				BOOL SetActionScaling(INT32 ActionIndex, INT32 Index, EPN_Scaling Scaling);
				BOOL SetActionScaling(estring SaveActionName, INT32 Index, EPN_Scaling Scaling);
				EPN_Scaling GetActionScaling(INT32 ActionIndex, INT32 Index);
				EPN_Scaling GetActionScaling(estring SaveActionName, INT32 Index);

				INT32 GetActionDataCount(INT32 ActionIndex);//인덱스로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 가져오기
				INT32 GetActionDataCount(estring SaveActionName);//스트링으로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 가져오기
				VOID SetActionDataCount(INT32 ActionIndex, INT32 MaxValue);//인덱스로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 설정
				VOID SetActionDataCount(estring SaveActionName, INT32 MaxValue);//스트링으로 찾아낸 액션클래스 에서 생성된 액션데이터 개수 설정

				//액션기능
				EPN_ActionData* Action(INT32 ActionIndex, INT32 DelayCount);
				EPN_ActionData* Action(INT32 ActionIndex, INT32 DelayCount, INT32 StartIndex, INT32 EndIndex);
				BOOL SetActionNum(INT32 ActionIndex, INT32 ActionNum);
				INT32 GetActionNum(INT32 ActionIndex);
				INT32 GetActionDelayCount(INT32 ActionIndex);
				EPN_ActionData* Action(estring SaveActionName, INT32 DelayCount);
				EPN_ActionData* Action(estring SaveActionName, INT32 DelayCount, INT32 StartIndex, INT32 EndIndex);
				BOOL SetActionNum(estring SaveActionName, INT32 ActionNum);
				INT32 GetActionNum(estring SaveActionName);
				INT32 GetActionDelayCount(estring SaveActionName);

			public:
				BOOL ClearAction(INT32 ActionIndex);
				BOOL ClearAction(estring SaveActionName);
				BOOL AllClearAction();

			private:
				static EPN_ActionInterface* m_pActionInterface;
			public:
				static EPN_ActionInterface* GetInstance();
				static EPN_ActionInterface* CreateInstance(VOID);
				static VOID ReleaseInstance(VOID);
			};

		}
	}
}
