﻿#include "EPN_InputEvent.h"
#include "../Device/EPN_DeviceInterface.h"
#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::INPUTEVENT;
#pragma endregion

EPN_InputEvent* EPN_InputEvent::m_pEPN_InputEvent = NULL_PTR;

EPN_InputEvent *EPN_InputEvent::GetInstance()
{
	return m_pEPN_InputEvent;
}

EPN_InputEvent *EPN_InputEvent::CreateInstance()
{
	if (NULL_PTR == m_pEPN_InputEvent)
		m_pEPN_InputEvent = new EPN_InputEvent();
	return m_pEPN_InputEvent;
}

VOID EPN_InputEvent::ReleaseInstance()
{
	if (NULL_PTR != m_pEPN_InputEvent)
		delete m_pEPN_InputEvent;
	m_pEPN_InputEvent = NULL_PTR;
}

EPN_InputEvent::EPN_InputEvent()
{
	m_LogFlag = (BYTE)EPN::COMMON::EPN_LOG_FLAG::NON;

	ClearKeyStat();
	m_MousePos = EPN_Pos(0, 0);
	m_CheckMousePos = EPN_Pos(0, 0);
	m_MouseStatL = EPN_INPUT_STATE::NONE;
	m_MouseStatR = EPN_INPUT_STATE::NONE;
	m_MouseStatM = EPN_INPUT_STATE::NONE;
	m_CheckMouseStatL = EPN_INPUT_STATE::NONE;
	m_CheckMouseStatM = EPN_INPUT_STATE::NONE;
	m_CheckMouseStatR = EPN_INPUT_STATE::NONE;

	m_MouseWheelUp = FALSE;
	m_MouseWheelDown = FALSE;
	m_CheckMouseWheelUp = FALSE;
	m_CheckMouseWheelDown = FALSE;
	m_KeyInputActiveFlag = TRUE;
	m_MouseInputActiveFlag = TRUE;
	m_MouseMove = EPN_Pos(0, 0);
	m_MouseMoveL = EPN_Pos(0, 0);
	m_MouseMoveM = EPN_Pos(0, 0);
	m_MouseMoveR = EPN_Pos(0, 0);
	m_MovePosSave = EPN_Pos(0, 0);
	m_MovePosSaveL = EPN_Pos(0, 0);
	m_MovePosSaveM = EPN_Pos(0, 0);
	m_MovePosSaveR = EPN_Pos(0, 0);
	m_MouseMoveFlag = FALSE;
	m_MouseMoveFlagL = EPN_INPUT_STATE::NONE;
	m_MouseMoveFlagM = EPN_INPUT_STATE::NONE;
	m_MouseMoveFlagR = EPN_INPUT_STATE::NONE;

	m_MouseWindowPosFlag = FALSE;

	m_pKeyEvent = NULL_PTR;
	m_pMouseEvent = NULL_PTR;
}
EPN_InputEvent::~EPN_InputEvent()
{

}

VOID EPN_InputEvent::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_InputEvent::GetLogFlag()
{
	return m_LogFlag;
}

VOID EPN_InputEvent::InputKeyProcess(BYTE Key)
{
	//이전에 Key를 누르지 않았을 때
	if (EPN_INPUT_STATE::NONE == (EPN_INPUT_STATE)m_KeyBoardStat[Key])
	{
		//이번에는 Key를 입력한 경우
		if (EPN_INPUT_STATE::DOWN == (EPN_INPUT_STATE)m_KeyBoardCheckStat[Key])
		{
			PRINT_LOG(m_LogFlag, "( [ %d ] 키 입력 ) \n", Key);
			m_KeyBoardStat[Key] = EPN_INPUT_STATE::DOWN;
		}
	}
	//이전에 Key를 누른 상태 였을 떄
	else if (EPN_INPUT_STATE::DOWN == (EPN_INPUT_STATE)m_KeyBoardStat[Key] || EPN_INPUT_STATE::DOWNING == m_KeyBoardStat[Key])
	{
		//현재는 누르는중일때
		if (EPN_INPUT_STATE::UP == m_KeyBoardCheckStat[Key] )
		{
			m_KeyBoardStat[Key] = EPN_INPUT_STATE::UP;
		}
		else
			m_KeyBoardStat[Key] = EPN_INPUT_STATE::DOWNING;
		PRINT_LOG(m_LogFlag, "( [ %d ] 키 입력 중 )\n", Key);
	}
	else if (EPN_INPUT_STATE::UP == m_KeyBoardStat[Key] )
	{
		PRINT_LOG(m_LogFlag, "( [ %d ] 키 입력 해제 )\n", Key);
		m_KeyBoardStat[Key] = EPN_INPUT_STATE::NONE;
		m_KeyBoardCheckStat[Key] = EPN_INPUT_STATE::NONE;
	}

}
BOOL EPN_InputEvent::GetKeyInputActiveFlag()
{
	return m_KeyInputActiveFlag;
}

VOID EPN_InputEvent::PushForKey(EventKeyboard::KeyCode Code, Event* pEvent)
{
	//안켜져있으면 처리하는 의미 X
	if (FALSE == m_KeyInputActiveFlag)
		return;
	m_KeyBoardCheckStat[(INT32)Code] = EPN_INPUT_STATE::DOWN;

}

VOID EPN_InputEvent::PopForKey(EventKeyboard::KeyCode Code, Event* pEvent)
{
	//안켜져있으면 처리하는 의미 X
	if (FALSE == m_KeyInputActiveFlag)
		return;
	if (EPN_INPUT_STATE::DOWN == m_KeyBoardCheckStat[(INT32)Code] || EPN_INPUT_STATE::DOWNING == m_KeyBoardCheckStat[(INT32)Code])
	{
		m_KeyBoardCheckStat[(INT32)Code] = EPN_INPUT_STATE::UP;
	}

}

VOID EPN_InputEvent::ClearKeyStat(EPN_INPUT_STATE Stat)
{
	for (INT32 KeyCnt = 0; KeyCnt < (INT32)EPN::COMMON::EPN_KEY_CODE::KEY_MAX; ++KeyCnt)
	{
		m_KeyBoardStat[KeyCnt] = Stat;
		m_KeyBoardCheckStat[KeyCnt] = EPN_INPUT_STATE::NONE;
	}
}

EPN_INPUT_STATE EPN_InputEvent::IF_KEY_STATE(EPN::COMMON::EPN_KEY_CODE EPN_KeyCode)
{
	if ((EPN::COMMON::EPN_KEY_CODE)0 < EPN_KeyCode && EPN::COMMON::EPN_KEY_CODE::KEY_MAX > EPN_KeyCode  )
	{
		return m_KeyBoardStat[(INT32)EPN_KeyCode];
	}
	return EPN_INPUT_STATE::NONE;
}

BOOL EPN_InputEvent::IF_KEY_STATE(EPN::COMMON::EPN_KEY_CODE EPN_KeyCode, EPN_INPUT_STATE Stat)
{
	//0은 명시적으로 해줬음 KEY_LBUTTON라고하면 알아보기힘듬
	if ((EPN::COMMON::EPN_KEY_CODE)0 < EPN_KeyCode && EPN::COMMON::EPN_KEY_CODE::KEY_MAX > EPN_KeyCode  && Stat == m_KeyBoardStat[(INT32)EPN_KeyCode]  )
	{
		return TRUE;
	}
	return FALSE;
}

BOOL EPN_InputEvent::IsKeyEvent(EPN_INPUT_STATE Stat)
{
	//전체키중 이벤트 발생시 TRUE반환
	const EPN::COMMON::EPN_KEY_CODE EPN_KEY_CODE_START = EPN::COMMON::EPN_KEY_CODE::KEY_NONE;
	const EPN::COMMON::EPN_KEY_CODE EPN_KEY_CODE_MAX = EPN::COMMON::EPN_KEY_CODE::KEY_MAX;

	for (INT32 KeyCnt = (INT32)EPN_KEY_CODE_START; (EPN::COMMON::EPN_KEY_CODE)KeyCnt < EPN_KEY_CODE_MAX; ++KeyCnt)
	{
		if (Stat == EPN_INPUT_STATE::ALL)
			if ( EPN_INPUT_STATE::NONE < IF_KEY_STATE( (EPN::COMMON::EPN_KEY_CODE)KeyCnt) )
				return TRUE;
			else
				if ( TRUE == IF_KEY_STATE((EPN::COMMON::EPN_KEY_CODE)KeyCnt, Stat) )
					return TRUE;

	}
	return FALSE;
}

VOID EPN_InputEvent::SetKeyInputActiveFlag(BOOL Flag)
{
	m_KeyInputActiveFlag = Flag;
}

VOID EPN_InputEvent::InputForMouse()
{
	//마우스 좌표
	POINT pos;
	pos.x = 0;
	pos.y = 0;
	GetCursorPos(&pos);
	EPN_DeviceInterface*pDI = EPN_DeviceInterface::CreateInstance();
	HWND DeviceHwnd = pDI->GetDevice()->pGLDevice->getWin32Window();
	ScreenToClient(DeviceHwnd, &pos);
	m_CheckMousePos = EPN_Pos(pos.x, pos.y);

	RECT rect;
	GetClientRect(DeviceHwnd, &rect); //윈도우 작업영역 크기만 구함

	Vec2 tempSize = pDI->GetDevice()->pDirector->getWinSize();
	EPN_Pos FormSize = EPN_Pos(tempSize.x, tempSize.y); //폼 사이즈
	//EPN_Pos FormSize2 = EPN_Pos(rect.right - rect.left, rect.bottom - rect.top); //작업영역의 실제 크기
	//if (m_CheckMousePos.X >= 0 && m_CheckMousePos.X <= rect.right && m_CheckMousePos.Y >= 0 && m_CheckMousePos.Y <= rect.bottom)
	//{		
	//	FormSize = FormSize / FormSize2;
	//	m_CheckMousePos *= FormSize;
	//}

	//윈도우 기준 좌표처리 여부
	if (FALSE == m_MouseWindowPosFlag)
	{
		//주의 : 

		//마우스 좌표 X가 음수면 0으로 고정
		if (m_CheckMousePos.X < 0)
			m_CheckMousePos.X = 0;
		//마우스 좌표 X가 폼 사이즈를 넘어가버리면 폼사이즈 -1 로 고정
		else if (m_CheckMousePos.X >= FormSize.X)
			m_CheckMousePos.X = FormSize.X - 1;

		////마우스 좌표 Y가 음수면 0으로 고정
		if (m_CheckMousePos.Y < 0)
			m_CheckMousePos.Y = 0;
		////마우스 좌표 Y가 폼사이즈 를 넘어가면 마우스 좌표는 FormSize.Y - 1;
		else if (m_CheckMousePos.Y >= FormSize.Y)
			m_CheckMousePos.Y = FormSize.Y - 1;
	}
	//Y좌표 Windows 좌표계에서 GL 로 변환
	m_CheckMousePos.Y = (FormSize.Y - m_CheckMousePos.Y) - 1; //-1을 해줘야함
}
VOID EPN_InputEvent::InputMouseProcess()
{
	
	//
	/* 마우스 좌측 버튼 처리 */
	if (EPN_INPUT_STATE::NONE == m_MouseStatL)
	{
		if (EPN_INPUT_STATE::DOWN == m_CheckMouseStatL)
		{
			PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 왼쪽 버튼 입력 )\n", m_MousePos.X , m_MousePos.Y);
			m_MouseStatL = EPN_INPUT_STATE::DOWN;
		}
	}
	else if (EPN_INPUT_STATE::DOWN == m_MouseStatL )
	{
		m_MouseStatL = EPN_INPUT_STATE::DOWNING;
	}
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatL )
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 왼쪽 버튼 입력 중 )\n", m_MousePos.X, m_MousePos.Y);

		if (EPN_INPUT_STATE::UP == m_CheckMouseStatL )
		{
			m_MouseStatL = EPN_INPUT_STATE::UP;
		}

	}
	else if (EPN_INPUT_STATE::UP == m_MouseStatL )
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 왼쪽 버튼 입력 해제 )\n", m_MousePos.X, m_MousePos.Y);
		m_CheckMouseStatL = EPN_INPUT_STATE::NONE;
		m_MouseStatL = EPN_INPUT_STATE::NONE;
	}


	/* 마우스 우측 버튼 처리 */

	if (EPN_INPUT_STATE::NONE == m_MouseStatR)
	{
		if (EPN_INPUT_STATE::DOWN == m_CheckMouseStatR)
		{
			PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 오른쪽 버튼 입력 )\n", m_MousePos.X, m_MousePos.Y);
			m_MouseStatR = EPN_INPUT_STATE::DOWN;
		}
	}
	else if (EPN_INPUT_STATE::DOWN == m_MouseStatR)
	{
		if (EPN_INPUT_STATE::DOWNING == m_CheckMouseStatR)
		{
			m_MouseStatR = EPN_INPUT_STATE::UP;
		}
		else
			m_MouseStatR = EPN_INPUT_STATE::DOWNING;
	}
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatR)
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 오른쪽 버튼 입력 중 )\n", m_MousePos.X, m_MousePos.Y);

		if (EPN_INPUT_STATE::UP == m_CheckMouseStatR)
		{
			m_MouseStatR = EPN_INPUT_STATE::UP;
		}

	}
	else if (EPN_INPUT_STATE::UP == m_MouseStatR)
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 오른쪽 버튼 입력 해제 )\n", m_MousePos.X, m_MousePos.Y);
		m_CheckMouseStatR = EPN_INPUT_STATE::NONE;
		m_MouseStatR = EPN_INPUT_STATE::NONE;
	}


	/* 마우스 중간 버튼 처리 */

	if (EPN_INPUT_STATE::NONE == m_MouseStatM)
	{
		if (EPN_INPUT_STATE::DOWN == m_CheckMouseStatM)
		{
			PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 중간 버튼 입력 )\n", m_MousePos.X, m_MousePos.Y);
			m_MouseStatM = EPN_INPUT_STATE::DOWN;
		}
	}
	else if (EPN_INPUT_STATE::DOWN == m_MouseStatR)
	{
		if (EPN_INPUT_STATE::DOWNING == m_CheckMouseStatM)
		{
			m_MouseStatM = EPN_INPUT_STATE::UP;
		}
		else
			m_MouseStatM = EPN_INPUT_STATE::DOWNING;
	}
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatM)
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 중간 버튼 입력 중 )\n", m_MousePos.X, m_MousePos.Y);

		if (EPN_INPUT_STATE::UP == m_CheckMouseStatM)
		{
			m_MouseStatM = EPN_INPUT_STATE::UP;
		}

	}
	else if (EPN_INPUT_STATE::UP == m_MouseStatM)
	{
		PRINT_LOG(m_LogFlag, "( 좌표 : X [ %f ] , Y [ %f ]  - 마우스 중간 버튼 입력 해제 )\n", m_MousePos.X, m_MousePos.Y);
		m_CheckMouseStatM = EPN_INPUT_STATE::NONE;
		m_MouseStatM = EPN_INPUT_STATE::NONE;
	}


	/*      */
	if ( FALSE == m_MouseMoveFlag)
	{
		m_MovePosSave = m_MousePos;
		m_MouseMoveFlag = TRUE;
	}
	else
	{
		m_MouseMove = m_MousePos - m_MovePosSave;
		m_MovePosSave = m_MousePos;
	}

	/* 마우스 왼쪽 이동 체크 처리 */

	//왼쪽버튼 누를때
	if (EPN_INPUT_STATE::DOWN == m_MouseStatL )
	{
		m_MovePosSaveL = m_MousePos; //눌렀는 당시 마우스 좌표 저장
		m_MouseMoveL = m_MousePos - m_MovePosSaveL; // 여기로썬 0 넣는거나 다름없음
	}
	//왼쪽버튼 누르는 중일 때
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatL)
	{
		m_MouseMoveL = m_MousePos - m_MovePosSaveL; //움직인 거리 = 현재마우스 좌표 - 이전 프레임에 저장한 좌표
		m_MovePosSaveL = m_MousePos; //이전 프레임에 저장한 좌표 변수 에다가 다시 현재 마우스 좌표를 넣어줌
	}
	else
		m_MouseMoveL = EPN_Pos();

	/* 마우스 오른쪽 이동 체크 처리 */

	//오른쪽버튼 누를때
	if (EPN_INPUT_STATE::DOWN == m_MouseStatR)
	{
		m_MovePosSaveR = m_MousePos; //눌렀는 당시 마우스 좌표 저장
		m_MouseMoveR = m_MousePos - m_MovePosSaveR; // 여기로썬 0 넣는거나 다름없음
	}
	//오른쪽버튼 누르는 중일 때
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatR)
	{
		m_MouseMoveR = m_MousePos - m_MovePosSaveR; //움직인 거리 = 현재마우스 좌표 - 이전 프레임에 저장한 좌표
		m_MovePosSaveR = m_MousePos; //이전 프레임에 저장한 좌표 변수 에다가 다시 현재 마우스 좌표를 넣어줌
	}
	else
		m_MouseMoveR = EPN_Pos();


	/* 마우스 중간 이동 체크 처리 */

	//중간버튼 누를때
	if (EPN_INPUT_STATE::DOWN == m_MouseStatM)
	{
		m_MovePosSaveM = m_MousePos; //눌렀는 당시 마우스 좌표 저장
		m_MouseMoveM = m_MousePos - m_MovePosSaveM; // 여기로썬 0 넣는거나 다름없음
	}
	//중간버튼 누르는 중일 때
	else if (EPN_INPUT_STATE::DOWNING == m_MouseStatM)
	{
		m_MouseMoveM = m_MousePos - m_MovePosSaveM; //움직인 거리 = 현재마우스 좌표 - 이전 프레임에 저장한 좌표
		m_MovePosSaveM = m_MousePos; //이전 프레임에 저장한 좌표 변수 에다가 다시 현재 마우스 좌표를 넣어줌
	}
	else
		m_MouseMoveM = EPN_Pos();
}


BOOL EPN_InputEvent::GetMouseInputActiveFlag()
{
	return m_MouseInputActiveFlag;
}
VOID EPN_InputEvent::SetMouseInputActiveFlag(BOOL Flag)
{
	m_MouseInputActiveFlag = Flag;
}


VOID EPN_InputEvent::PushForMouse(EventMouse* pEvent)
{
	//안켜져있으면 처리하는 의미 X
	if (FALSE == m_MouseInputActiveFlag)
		return;

	//0 : 좌 ,		1 : 우  ,	 2 :휠
	if (pEvent->getMouseButton() == 0)
		m_CheckMouseStatL = EPN_INPUT_STATE::DOWN;
	else if (pEvent->getMouseButton() == 1)
		m_CheckMouseStatR = EPN_INPUT_STATE::DOWN;
	else if (pEvent->getMouseButton() == 2)
		m_CheckMouseStatM = EPN_INPUT_STATE::DOWN;

}

VOID EPN_InputEvent::PopForMouse(EventMouse* pEvent)
{
	//안켜져있으면 처리하는 의미 X
	if (FALSE == m_MouseInputActiveFlag)
		return;

	//0 : 좌 ,		1 : 우  ,	 2 :휠
	if (pEvent->getMouseButton() == 0)
		m_CheckMouseStatL = EPN_INPUT_STATE::UP;
	else if (pEvent->getMouseButton() == 1)
		m_CheckMouseStatR = EPN_INPUT_STATE::UP;
	else if (pEvent->getMouseButton() == 2)
		m_CheckMouseStatM = EPN_INPUT_STATE::UP;

}

VOID	EPN_InputEvent::ScrollForMouse(EventMouse* pEvent)
{
	/* 일단은 y축 스크롤만 구현합니다. */
	INT32 ScrollY = pEvent->getScrollY();
	//위로 마우스 휠 돌림
	if (ScrollY < 0)
		m_CheckMouseWheelUp = TRUE;
	//아래로 마우스 휠 돌림
	else
		m_CheckMouseWheelDown = TRUE;

	m_CheckMouseScrollPos = EPN_Pos(0, ScrollY);
}

VOID EPN_InputEvent::ClearMouseStat()
{
	m_MousePos = EPN_Pos(0, 0);
	m_MouseStatL = EPN_INPUT_STATE::NONE;
	m_MouseStatM = EPN_INPUT_STATE::NONE;
	m_MouseStatR = EPN_INPUT_STATE::NONE;
	m_MouseWheelUp = FALSE;
	m_MouseWheelDown = FALSE;
}
			
EPN_Pos EPN_InputEvent::GetMousePos()
{
	return m_MousePos;
}

EPN_INPUT_STATE EPN_InputEvent::GetMouseStatL()
{
	return m_MouseStatL;
}
EPN_INPUT_STATE EPN_InputEvent::GetMouseStatM()
{
	return m_MouseStatM;
}
EPN_INPUT_STATE EPN_InputEvent::GetMouseStatR()
{
	return m_MouseStatR;
}

EPN_Pos EPN_InputEvent::GetMouseMove()
{
	return m_MouseMove;
}
EPN_Pos EPN_InputEvent::GetMouseMoveL()
{
	return m_MouseMoveL;
}
EPN_Pos EPN_InputEvent::GetMouseMoveM()
{
	return m_MouseMoveM;
}
EPN_Pos EPN_InputEvent::GetMouseMoveR()
{
	return m_MouseMoveR;
}

BOOL EPN_InputEvent::GetMouseWheelUp()
{
	return m_MouseWheelUp;
}
BOOL EPN_InputEvent::GetMouseWheelDown()
{
	return m_MouseWheelDown;
}

EPN_Pos EPN_InputEvent::GetMouseScrollPos()
{
	return m_MouseScrollPos;
}

INT32 EPN_InputEvent::GetMouseScrollX()
{
	return (INT32)m_MouseScrollPos.X;
}

INT32 EPN_InputEvent::GetMouseScrollY()
{
	return (INT32)m_MouseScrollPos.Y;
}

VOID EPN_InputEvent::SetMouseWindowPosFlag(BOOL Flag)
{
	m_MouseWindowPosFlag = Flag;
}

BOOL EPN_InputEvent::GetMouseWindowPosFlag()
{
	return m_MouseWindowPosFlag;
}
 
BOOL EPN_InputEvent::EventRun()
{
	//키 이벤트 없을 시
	if (m_pKeyEvent == NULL_PTR)
	{
		EPN_DeviceInterface* pEPN_DI = EPN_DeviceInterface::GetInstance();
		if (pEPN_DI == NULL_PTR || pEPN_DI->GetDevice() == NULL_PTR)
			return FALSE;

		m_pKeyEvent = EventListenerKeyboard::create();
		

		m_pKeyEvent->onKeyPressed = CC_CALLBACK_2(EPN_InputEvent::PushForKey, this);
		m_pKeyEvent->onKeyReleased = CC_CALLBACK_2(EPN_InputEvent::PopForKey, this);

		pEPN_DI->GetDevice()->pDirector->getEventDispatcher()->addEventListenerWithSceneGraphPriority(m_pKeyEvent, *pEPN_DI->GetDevice()->mpDeviceLayer[0]);
	}
	//마우스 이벤트 없을 시
	if (m_pMouseEvent == NULL_PTR)
	{
		EPN_DeviceInterface* pEPN_DI = EPN_DeviceInterface::GetInstance();
		if (pEPN_DI == NULL_PTR || pEPN_DI->GetDevice() == NULL_PTR)
			return FALSE;

		m_pMouseEvent = EventListenerMouse::create();

		m_pMouseEvent->onMouseDown = CC_CALLBACK_1(EPN_InputEvent::PushForMouse, this);
		m_pMouseEvent->onMouseUp = CC_CALLBACK_1(EPN_InputEvent::PopForMouse, this);
		m_pMouseEvent->onMouseScroll = CC_CALLBACK_1(EPN_InputEvent::ScrollForMouse, this);
		pEPN_DI->GetDevice()->pDirector->getEventDispatcher()->addEventListenerWithSceneGraphPriority(m_pMouseEvent, *pEPN_DI->GetDevice()->mpDeviceLayer[0]);
	}

	InputForMouse();
	
	for (INT32 i = 0; i < (INT32)EPN::COMMON::EPN_KEY_CODE::KEY_MAX; ++i)
	{
		if (EPN_INPUT_STATE::NONE < m_KeyBoardCheckStat[i] )
			InputKeyProcess(i);
	}
	InputMouseProcess();

	m_MousePos = m_CheckMousePos;
	m_MouseScrollPos = m_CheckMouseScrollPos;

	if (TRUE == m_MouseWheelUp)
	{
		m_MouseWheelUp = FALSE;
		m_CheckMouseWheelUp = FALSE;
		m_MouseScrollPos.Y = 0.f;
		m_CheckMouseScrollPos.Y = 0.f;
	}
	else
	{
		m_MouseWheelUp = m_CheckMouseWheelUp;
		m_MouseScrollPos = m_CheckMouseScrollPos;
	}
	if (TRUE == m_MouseWheelDown)
	{
		m_MouseWheelDown = FALSE;
		m_CheckMouseWheelDown = FALSE;
		m_MouseScrollPos.Y = 0.f;
		m_CheckMouseScrollPos.Y = 0.f;
	}
	else
	{
		m_MouseWheelDown = m_CheckMouseWheelDown;
		m_MouseScrollPos = m_CheckMouseScrollPos;
	}
	return TRUE;
}