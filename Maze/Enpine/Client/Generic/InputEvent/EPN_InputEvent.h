#pragma once

#include "../../../Common/Common.h"

using namespace EPN::COMMON::STRUCTURE;

namespace EPN
{
	namespace GENERIC
	{
		namespace INPUTEVENT
		{
			/* 입력 이벤트 상태*/
			enum class EPN_INPUT_STATE
			{
				ALL = -1,
				NONE = 0,			//아무 입력도 안 한 상태
				DOWN = 1,			//입력을 한 상태( 막 입력을 한 그 상태일 떄)
				DOWNING = 2,		//입력을 계속 하고 있는 상태
				UP = 3				//입력을 한 후 뗀 상태
			};

			class EPN_InputEvent
			{
			//COCOS
			private:
				EventListenerKeyboard* m_pKeyEvent;
				EventListenerMouse*	   m_pMouseEvent;
			/* 싱글톤 인스턴스 관련 */
			private:
				static EPN_InputEvent* m_pEPN_InputEvent;
			public:
				static EPN_InputEvent* GetInstance();
				static EPN_InputEvent* CreateInstance();
				static VOID ReleaseInstance();

			/* 생성자 소멸자*/
			private:
				EPN_InputEvent();
			public:
				virtual ~EPN_InputEvent();

			/* 로그 관련 */
			private:
				BYTE m_LogFlag;
			public:
				VOID		SetLogFlag(BYTE LogFlag);
				BYTE		GetLogFlag();

			/* 공통 */
				BOOL m_KeyInputActiveFlag;//키입력처리 사용유무 TRUE 사용 FALSE 사용중지 셋터겟터 함수로 변경가능 기본값 TRUE

			/* 키보드 이벤트 */
			private:
				EPN_INPUT_STATE m_KeyBoardStat[ (BYTE)EPN::COMMON::EPN_KEY_CODE::KEY_MAX ];
				EPN_INPUT_STATE m_KeyBoardCheckStat[ (BYTE)EPN::COMMON::EPN_KEY_CODE::KEY_MAX ];

			private:
				VOID InputKeyProcess(BYTE Key = 0);

			public:
				//키보드 처리
				BOOL GetKeyInputActiveFlag();
				VOID SetKeyInputActiveFlag(BOOL Flag);

				VOID PushForKey(EventKeyboard::KeyCode Code, Event* pEvent);
				VOID PopForKey(EventKeyboard::KeyCode Code, Event* pEvent);

				VOID ClearKeyStat(EPN_INPUT_STATE Stat = EPN_INPUT_STATE::NONE);//키보드 입력상태 초기화

				EPN_INPUT_STATE IF_KEY_STATE(EPN::COMMON::EPN_KEY_CODE EPN_KeyCode);
				BOOL IF_KEY_STATE(EPN::COMMON::EPN_KEY_CODE EPN_KeyCode, EPN_INPUT_STATE Stat);
				//전체키중 이벤트 발생시 TRUE반환
				BOOL IsKeyEvent(EPN_INPUT_STATE Stat = EPN_INPUT_STATE::ALL);



			/* 마우스 이벤트 */
			private:
				EPN_Pos m_MousePos;
				EPN_Pos m_CheckMousePos;
				EPN_INPUT_STATE m_MouseStatL;
				EPN_INPUT_STATE m_MouseStatM;
				EPN_INPUT_STATE m_MouseStatR;
				EPN_INPUT_STATE m_CheckMouseStatL;
				EPN_INPUT_STATE m_CheckMouseStatM;
				EPN_INPUT_STATE m_CheckMouseStatR;

				EPN_Pos m_MouseMove;
				EPN_Pos m_MouseMoveL;
				EPN_Pos m_MouseMoveM;
				EPN_Pos m_MouseMoveR;
				EPN_Pos m_MovePosSave;
				EPN_Pos m_MovePosSaveL;
				EPN_Pos m_MovePosSaveM;
				EPN_Pos m_MovePosSaveR;
				BOOL m_MouseMoveFlag;
				EPN_INPUT_STATE m_MouseMoveFlagL;
				EPN_INPUT_STATE m_MouseMoveFlagM;
				EPN_INPUT_STATE m_MouseMoveFlagR;
			
				BOOL m_CheckMouseWheelUp;
				BOOL m_CheckMouseWheelDown;
				BOOL m_MouseWheelUp;
				BOOL m_MouseWheelDown;
				EPN_Pos m_MouseScrollPos;
				EPN_Pos m_CheckMouseScrollPos;

				BOOL m_MouseInputActiveFlag;//마우스처리 사용유무 TRUE 사용 FALSE 사용중지 셋터겟터 함수로 변경가능 기본값 TRUE

				BOOL m_MouseWindowPosFlag;//마우스좌표를 디바이스기준으로 밖까지 좌표인식할지말지 기본 TRUE 이고 FALSE시 마우스 화면 밖으로 나갔을때 좌표 인식안함
			
			private: 			
				VOID InputMouseProcess();
				VOID InputForMouse();

			public://사용스위치 변경
				BOOL GetMouseInputActiveFlag();
				VOID SetMouseInputActiveFlag(BOOL Flag);

			public:
				VOID PushForMouse(EventMouse* pEvent);
				VOID PopForMouse(EventMouse* pEvent);
				VOID	ScrollForMouse(EventMouse* pEvent);

				VOID ClearMouseStat();//마우스 입력상태 초기화
			public:
				EPN_Pos GetMousePos();
				
				EPN_INPUT_STATE GetMouseStatL();
				EPN_INPUT_STATE GetMouseStatM();
				EPN_INPUT_STATE GetMouseStatR();

				EPN_Pos GetMouseMove();
				EPN_Pos GetMouseMoveL();
				EPN_Pos GetMouseMoveM();
				EPN_Pos GetMouseMoveR();

				BOOL GetMouseWheelUp();
				BOOL GetMouseWheelDown();

				EPN_Pos GetMouseScrollPos();
				INT32 GetMouseScrollX();
				INT32 GetMouseScrollY();
				
				VOID SetMouseWindowPosFlag(BOOL Flag);
				BOOL GetMouseWindowPosFlag();

			/* 인풋 이벤트 공통 */
			public:
				BOOL EventRun(); //입력 이벤트 동작 함수

			};
		}
	}
}

