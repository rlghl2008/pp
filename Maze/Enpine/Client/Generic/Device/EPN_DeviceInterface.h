#pragma once

#include "../../../Common/Common.h"
#include "../../../Common/Class/EPN_Timer.h"



namespace EPN
{
	namespace GENERIC
	{
		namespace DEVICE
		{
			/*class EPN_Layer : public Layer
			{
			public:
				EPN_Layer() {}
				virtual ~EPN_Layer() = 0;
			public:
				VOID ClearChild()
				{
					for (INT32 Cnt = 0; Cnt < _children.size(); Cnt++)
					{
						_children.at(Cnt)->setParent(NULL_PTR);
					}
					_children.clear();
				}
			};*/
			struct EPN_Device
			{
				Director* pDirector;
				GLView* pGLDevice;
				Scene*	pDeviceScene;
				EPN::COMMON::EPN_MemoryPool<Layer*>	mpDeviceLayer;
				EPN_Device() : pDirector(NULL_PTR), pGLDevice(NULL_PTR), pDeviceScene(NULL_PTR) {}
			};
			class EPN_DeviceInterface : private Application
			{
				//COCOS Class
			private:
				virtual void initGLContextAttrs();
				virtual bool applicationDidFinishLaunching();
				virtual void applicationDidEnterBackground();
				virtual void applicationWillEnterForeground();
			private:
				Director* m_pDirector;
			private:
				EPN_DeviceInterface();
			public:
				virtual ~EPN_DeviceInterface();
			private:
				BYTE m_LogFlag;
			public:
				VOID SetLogFlag(BYTE LogFlag);
				BYTE GetLogFlag();
			private:
				// 추후 다중 디바이스를 위하여 메모리풀로 선언해둠
				EPN::COMMON::EPN_MemoryPool<EPN_Device>	m_mpDevice;
				EPN::COMMON::EPN_StringIndexPool		m_StringIndexPool;
				EPN::COMMON::EPN_Timer m_Timer;
			public:
				BOOL CreateDevice(estring Caption = E_TEXT("[E P N]Hello World"), BOOL FullScreenFlag = FALSE, EPN_Pos DeviceSize = EPN_Pos(640, 480), EPN_Pos DevicePos = EPN_Pos(0, 0));
				//INT32 CreateDevice(estring Caption = E_TEXT("[E P N]Hello World"), BOOL FullScreenFlag = FALSE, EPN_Pos DeviceSize = EPN_Pos(640, 480), EPN_Pos DevicePos = EPN_Pos(0, 0));	
				//INT32 CreateDevice(estring SaveDeviceName, estring Caption = E_TEXT("[E P N]Hello World"), BOOL FullScreenFlag = FALSE, EPN_Pos DeviceSize = EPN_Pos(640, 480), EPN_Pos DevicePos = EPN_Pos(0, 0));
			public:
				EPN_Device* GetDevice();
				BOOL DeleteDevice();
				/*EPN_Device* GetDevice(INT32 DeviceIndex);
				EPN_Device* GetDevice(estring SaveDeviceName);
				BOOL DeleteDevice(INT32 DeviceIndex);
				BOOL DeleteDevice(estring SaveDeviceName);*/

				INT32 AddLayer();

				BOOL BeginScene();
				BOOL EndScene();
			public:
				BOOL Run();
				//BOOL Run(INT32 DeviceIndex);
				//BOOL Run(estring SaveDeviceName);
			private:
				static EPN_DeviceInterface* m_pDeviceInterface;
			public:
				static EPN_DeviceInterface* GetInstance();
				static EPN_DeviceInterface* CreateInstance();
				static VOID ReleaseInstance();
			};
		}
	}
}