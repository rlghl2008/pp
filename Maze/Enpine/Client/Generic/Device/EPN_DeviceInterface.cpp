#include "EPN_DeviceInterface.h"
#include "../Texture/EPN_TextureInterface.h"
#include "../Font/EPN_FontInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::FONT;
#pragma endregion


EPN_DeviceInterface* EPN_DeviceInterface::m_pDeviceInterface = NULL_PTR;

EPN_DeviceInterface* EPN_DeviceInterface::GetInstance()
{
	return m_pDeviceInterface;
}

EPN_DeviceInterface* EPN_DeviceInterface::CreateInstance()
{
	if (NULL_PTR == m_pDeviceInterface)
		m_pDeviceInterface = new EPN_DeviceInterface();
	return m_pDeviceInterface;
}

VOID EPN_DeviceInterface::ReleaseInstance()
{
	if (NULL_PTR != m_pDeviceInterface)
		delete m_pDeviceInterface;
	m_pDeviceInterface = NULL_PTR;
}

EPN_DeviceInterface::EPN_DeviceInterface()
{
	m_pDirector = NULL_PTR;
}

EPN_DeviceInterface::~EPN_DeviceInterface()
{
	INT32 addDataCount = m_mpDevice.GetAddDataCount();
	for (INT32 Cnt = 0; Cnt < addDataCount; Cnt++)
	{
		EPN_Device* pDevice = m_mpDevice[Cnt];
		if (pDevice)
		{
			pDevice->pGLDevice->release();
		}
	}
	if (m_pDirector)
	{
		m_pDirector->end();
		m_pDirector->mainLoop();
		m_pDirector = NULL_PTR;
	}

}
void EPN_DeviceInterface::initGLContextAttrs()
{
	// set OpenGL context attributes: red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };
	GLView::setGLContextAttrs(glContextAttrs);
}
bool EPN_DeviceInterface::applicationDidFinishLaunching()
{
	return false;
}
void EPN_DeviceInterface::applicationDidEnterBackground()
{
	m_pDirector->stopAnimation();
}
void EPN_DeviceInterface::applicationWillEnterForeground()
{
	m_pDirector->startAnimation();
}

VOID EPN_DeviceInterface::SetLogFlag(BYTE Flag)
{
	m_LogFlag = Flag;
}

BYTE EPN_DeviceInterface::GetLogFlag()
{
	return m_LogFlag;
}


BOOL EPN_DeviceInterface::CreateDevice(estring Caption, BOOL FullScreenFlag, EPN_Pos DeviceSize, EPN_Pos DevicePos)
{
	//하나만 생성 가능하도록
	if (m_mpDevice.GetAddDataCount() > 0)
	{
		PRINT_LOG(m_LogFlag, "이미 디바이스가 생성 되었습니다.");
		return FALSE;
	}

	INT32 Index = m_mpDevice.AddData(EPN_Device());
	EPN_Device* pDevice = m_mpDevice[Index];
	if (pDevice == NULL_PTR)
	{
		goto FAILED_LOOP;
	}

	pDevice->pGLDevice = GLViewImpl::createWithRect(Caption.GetStrA(), cocos2d::Rect(0, 0, DeviceSize.X, DeviceSize.Y));
	if (pDevice->pGLDevice == NULL_PTR)
	{
		goto FAILED_LOOP;
	}
	if (m_pDirector == NULL_PTR)
		m_pDirector = Director::getInstance();
	if (m_pDirector == NULL_PTR)
	{
		goto FAILED_LOOP;
	}
	m_pDirector->setOpenGLView(pDevice->pGLDevice);

	m_pDirector->setAnimationInterval(1.f/60.f);
	m_pDirector->getOpenGLView()->setFrameSize((INT32)DeviceSize.X, (INT32)DeviceSize.Y);
	m_pDirector->getOpenGLView()->setDesignResolutionSize((INT32)DeviceSize.X, (INT32)DeviceSize.Y, kResolutionShowAll);


	//register_all_packages();


	pDevice->pDeviceScene = Scene::create();
	if (pDevice->pDeviceScene == NULL_PTR)
	{
		goto FAILED_LOOP;
	}

	Layer* pLayer = LayerColor::create(Color4B(255, 0, 0, 0));
	if (pLayer == NULL_PTR)
	{
		goto FAILED_LOOP;
	}
	INT32 LayerIndex = pDevice->mpDeviceLayer.AddData(pLayer);
	if (LayerIndex < 0)
	{
		goto FAILED_LOOP;
	}
	pDevice->pDeviceScene->addChild(pLayer);

	m_pDirector->runWithScene(pDevice->pDeviceScene);
	//PVRFrameEnableControlWindow(false);
	initGLContextAttrs();
	// Initialize instance and cocos2d.
	if (TRUE == applicationDidFinishLaunching())
	{
		goto FAILED_LOOP;
	}
	pDevice->pGLDevice->retain();
	pDevice->pDirector = m_pDirector;

	PRINT_LOG(m_LogFlag, "디바이스 생성성공 정보)\n");
	PRINT_LOG(m_LogFlag, "- Print Start -\n");
	PRINT_LOG("Device Index - [ %d ] \n", Index);
	PRINT_LOG("Renderer - [ OpenGL ] \n");
	PRINT_LOG("Device Pos - X [ %d ] Y [ %d ] \n", (INT32)DevicePos.X, (INT32)DevicePos.Y);
	PRINT_LOG("Device Size - W [ %d ] H [ %d ] \n", (INT32)DeviceSize.X, (INT32)DeviceSize.Y);
	PRINT_LOG("- Print End - \n");


	return TRUE;
FAILED_LOOP:

	PRINT_LOG("CreateDevice Failed");
	return FALSE;
}
//
//INT32 EPN_DeviceInterface::CreateDevice(estring SaveDeviceName, estring Caption, BOOL FullScreenFlag, EPN_Pos DeviceSize, EPN_Pos DevicePos)
//{
//   INT32 DeviceIndex = m_StringIndexPool.SearchIndex(SaveDeviceName);
//   if (0 > DeviceIndex  && EPN_SI_STRLEN_MAX > SaveDeviceName.Length())
//   {
//      DeviceIndex = CreateDevice(Caption, FullScreenFlag, DeviceSize, DevicePos);
//      if (0 > DeviceIndex)
//         return -1;
//      if (FALSE == m_StringIndexPool.InsertData(SaveDeviceName, DeviceIndex))
//      {
//         PRINT_LOG(m_LogFlag, "( InsertData Failed )\n", SaveDeviceName);
//         //DeleteDevice(DeviceIndex);
//         return -1;
//      }
//      return DeviceIndex;
//   }
//   else
//   {
//      PRINT_LOG(m_LogFlag, "( Error : 이미 저장된 SaveDeviceName [ %s ] 문자열 길이가 [ %d ] 를 초과하였습니다. ) \n", SaveDeviceName, EPN_SI_STRLEN_MAX);
//   }
//
//   return -1;
//
//
EPN_Device* EPN_DeviceInterface::GetDevice()
{
	EPN_Device* pDevice = m_mpDevice[0];
	if (NULL_PTR == pDevice)
	{
		PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. )\n", 0);
		return NULL_PTR;
	}
	return pDevice;
}
//EPN_Device* EPN_DeviceInterface::GetDevice(INT32 DeviceIndex)
//{
//   EPN_Device* pDevice = m_mpDevice[DeviceIndex];
//   if (NULL_PTR == pDevice)
//   {
//      PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. )\n", DeviceIndex);
//      return NULL_PTR;
//   }
//   return pDevice;
//}
//
//EPN_Device* EPN_DeviceInterface::GetDevice(estring SaveDeviceName)
//{
//   INT32 DeviceIndex = m_StringIndexPool.SearchIndex(SaveDeviceName);
//   if (0 > DeviceIndex)
//   {
//      PRINT_LOG(m_LogFlag, "( Error : 저장되지않은 SaveDeviceName [ %s ] 입니다. ) \n", SaveDeviceName);
//      return NULL_PTR;
//   }
//   return m_mpDevice[DeviceIndex];
//}

BOOL EPN_DeviceInterface::DeleteDevice()
{
	EPN_Device* pDevice = GetDevice();
	if (NULL_PTR == pDevice)
	{
		PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", 0);
		return FALSE;
	}

	delete m_mpDevice[0];
	m_mpDevice.EraseData(0);
	m_StringIndexPool.DeleteData(0);
	return TRUE;
}

INT32 EPN_DeviceInterface::AddLayer()
{
	EPN_Device* pDevice = GetDevice();
	if (NULL_PTR == pDevice)
	{
		PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", 0);
		return FALSE;
	}

	Layer* pLayer = Layer::create();
	if (pLayer == NULL_PTR)
		return -1;

	pDevice->pDeviceScene->addChild(pLayer);
	return pDevice->mpDeviceLayer.AddData(pLayer);
}
BOOL EPN_DeviceInterface::BeginScene()
{
	EPN_Device* pDevice = GetDevice();
	EPN_TextureInterface *pEPN_TI = EPN_TextureInterface::CreateInstance();
	EPN_FontInterface *pEPN_FI = EPN_FontInterface::CreateInstance();

	if (NULL_PTR == pDevice)
	{
		PRINT_LOG(m_LogFlag, "BeginScene Failed");
		return FALSE;
	}
	//모든 텍스쳐 BltCount 초기화
	for (INT32 Cnt = 0, Cnt2 = 0; Cnt2 < pEPN_TI->GetInsertCount(); ++Cnt)
	{
		EPN_Texture *pTexture = pEPN_TI->GetTexture(Cnt);
		if (pTexture == NULL_PTR)
			continue;
		pTexture->BltCount = 0;
		++Cnt2;
	}
	//모든 폰트 BltCount 초기화
	for (INT32 Cnt = 0, Cnt2 = 0; Cnt2 < pEPN_FI->GetAddDataCount(); ++Cnt)
	{
		EPN_Font *pFont = pEPN_FI->GetFont(Cnt);
		if (pFont == NULL_PTR)
			continue;
		pFont->BltCount = 0;
		++Cnt2;
	}



	for (INT32 Cnt = 0; Cnt < pDevice->mpDeviceLayer.GetAddDataCount();)
	{
		if (pDevice->mpDeviceLayer.IsData(Cnt) == TRUE)
		{

			pDevice->mpDeviceLayer.GetData(Cnt)->removeAllChildren();
			//EPN_Layer* pLayer = pDevice->mpDeviceLayer.GetData(Cnt);
			////pLayer->setParent(NULL_PTR);
			//pLayer->ClearChild();
			Cnt++;
		}
	}

	return TRUE;
}
BOOL EPN_DeviceInterface::EndScene()
{
	if (m_pDirector == NULL_PTR)
	{
		PRINT_LOG(m_LogFlag, "EndScene Failed");
		return FALSE;
	}
	m_pDirector->mainLoop();
	return TRUE;
}
//BOOL EPN_DeviceInterface::DeleteDevice(INT32 DeviceIndex)
//{
//   EPN_Device* pDevice = GetDevice(DeviceIndex);
//   if (NULL_PTR == pDevice)
//   {
//      PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", DeviceIndex);
//      return FALSE;
//   }
//
//   delete m_mpDevice[DeviceIndex];
//   m_mpDevice.EraseData(DeviceIndex);
//   m_StringIndexPool.DeleteData(DeviceIndex);
//   return TRUE;
//}
//
//BOOL EPN_DeviceInterface::DeleteDevice(estring SaveDeviceName)
//{
//   INT32 DeviceIndex = m_StringIndexPool.SearchIndex(SaveDeviceName);
//   if (0 > DeviceIndex)
//   {
//      PRINT_LOG(m_LogFlag, "(Error : 저장되지않은 SaveDeviceName [ %s ] 입니다. ) \n", SaveDeviceName);
//      return FALSE;
//   }
//   return DeleteDevice(DeviceIndex);
//}
//
BOOL EPN_DeviceInterface::Run()
{
	EPN_Device* pDevice = GetDevice();
	if (NULL_PTR == pDevice)
	{
		PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", 0);
		return FALSE;
	}
	GLView* pGLDevice = pDevice->pGLDevice;
	m_pDirector->setOpenGLView(pGLDevice);
	m_pDirector->setDisplayStats(false);
	m_pDirector->setAnimationInterval(0);

	if (pGLDevice->windowShouldClose() == TRUE)
		return FALSE;

	//if (m_Timer.IsTimerStart() == FALSE)
	//   m_Timer.Start();
	//if (m_Timer.GetTime() >= (1.f / 60.f))
	//{
	pGLDevice->pollEvents();

	//m_Timer.Start();
	//}
	return TRUE;
}
//BOOL EPN_DeviceInterface::Run(INT32 DeviceIndex)
//{
//   EPN_Device* pDevice = GetDevice(DeviceIndex);
//   if (NULL_PTR == pDevice)
//   {
//      PRINT_LOG(m_LogFlag, "( Error : 존재하지 않는 Device Index [ %d ] 입니다. ) \n", DeviceIndex);
//      return FALSE;
//   }
//   GLView* pGLDevice = pDevice->pGLDevice;
//   m_pDirector->setOpenGLView(pGLDevice);
//
//   if (pGLDevice->windowShouldClose() == TRUE)
//      return FALSE;
//
//   if (m_Timer.IsTimerStart() == FALSE)
//      m_Timer.Start();
//   if (m_Timer.GetTime() >= (1.f / 60.f))
//   {
//      m_pDirector->mainLoop();
//      pGLDevice->pollEvents();
//      m_Timer.Start();
//   }
//   return TRUE;
//}
//BOOL EPN_DeviceInterface::Run(estring SaveDeviceName)
//{
//   INT32 DeviceIndex = m_StringIndexPool.SearchIndex(SaveDeviceName);
//   if (0 > DeviceIndex)
//   {
//      PRINT_LOG(m_LogFlag, "(Error : 저장되지않은 SaveDeviceName [ %s ] 입니다. ) \n", SaveDeviceName);
//      return FALSE;
//   }
//   return Run(DeviceIndex);
//}