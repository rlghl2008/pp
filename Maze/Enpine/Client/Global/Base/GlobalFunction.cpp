//#include "GlobalFunction.h"
//
//#pragma region EPN Namespace
//using namespace EPN::COMMON::STRUCTURE
//using namespace EPN::GLOBAL::FUNCTION;
//#pragma endregion
//
//#include <iostream>
//using namespace std;
//
//
//#define SAME_SIGNS( a, b ) \
// (((long) ((unsigned long) a ^ (unsigned long) b)) >= 0 )
//
//
//BOOL LineColCheck(long x1, long y1, long x2, long y2, long x3, long y3, long x4, long y4, long* x, long *y)
//{
//
//	long Ax, Bx, Cx, Ay, By, Cy, d, e, f;
//	INT16 x1lo, x1hi, y1lo, y1hi;
//
//	Ax = x2 - x1;
//	Bx = x3 - x4;
//
//	if (Ax<0)
//	{
//		x1lo = (INT16)x2; x1hi = (INT16)x1;
//	}
//	else {
//		x1hi = (INT16)x2; x1lo = (INT16)x1;
//	}
//	if (Bx>0)
//	{
//		if (x1hi < (INT16)x4 || (INT16)x3 < x1lo) return false;
//	}
//	else {
//		if (x1hi < (INT16)x3 || (INT16)x4 < x1lo) return false;
//	}
//
//	Ay = y2 - y1;
//	By = y3 - y4;
//
//	if (Ay<0) { /* Y bound box test*/
//		y1lo = (INT16)y2; y1hi = (INT16)y1;
//	}
//	else {
//		y1hi = (INT16)y2; y1lo = (INT16)y1;
//	}
//	if (By>0) {
//		if (y1hi < (INT16)y4 || (INT16)y3 < y1lo) return false;
//	}
//	else {
//		if (y1hi < (INT16)y3 || (INT16)y4 < y1lo) return false;
//	}
//
//
//	Cx = x1 - x3;
//	Cy = y1 - y3;
//	d = By*Cx - Bx*Cy;
//	f = Ay*Bx - Ax*By;
//	if (f>0)
//	{
//		if (d<0 || d>f) return false;
//	}
//	else {
//		if (d>0 || d<f) return false;
//	}
//
//	e = Ax*Cy - Ay*Cx;
//	if (f > 0)
//	{
//		if (e<0 || e>f)
//			return 0;
//	}
//	else
//	{
//		if (e>0 || e<f) return false;
//	}
//
//
//	//if(f==0)
//	//    return 2;
//
//	//num = d * Ax;
//	//offset = SAME_SIGNS(num,f) ? f/2 : -f/2;
//	//*x = x1 + (num+offset) / f;
//
//	//num = d*Ay;
//	//offset = SAME_SIGNS(num,f) ? f/2 : -f/2;
//	//*y = y1 + (num+offset) / f;
//
//	return true;
//}
//
//
//
//REAL32 EPN::COMMON::FUNCTION::EPN_GetPosAngle(EPN_Pos DestPos, EPN_Pos TargetPos)
//{
//	if (DestPos.X == TargetPos.X && DestPos.Y == TargetPos.Y)
//		return 0;
//	double C = 0, A = 0;
//	REAL32 Angle = 0;
//
//	if (DestPos.X > TargetPos.X)
//	{
//		A = DestPos.Y - TargetPos.Y;
//		C = sqrt((TargetPos.X - DestPos.X)*(TargetPos.X - DestPos.X) + (TargetPos.Y - DestPos.Y)*(TargetPos.Y - DestPos.Y));
//		C = -C;
//		Angle = acos(A / C);
//		Angle *= 180.0 / 3.141592653589793238462643383279;
//		Angle += 180;
//	}
//	else
//	{
//		A = DestPos.Y - TargetPos.Y;
//		C = sqrt((TargetPos.X - DestPos.X)*(TargetPos.X - DestPos.X) + (TargetPos.Y - DestPos.Y)*(TargetPos.Y - DestPos.Y));
//		Angle = acos(A / C);
//		Angle *= 180.0 / 3.141592653589793238462643383279;
//	}
//
//
//	return Angle;
//}
//EPN_Pos EPN::COMMON::FUNCTION::EPN_GetAnglePos(REAL32 Angle)
//{
//	if (Angle < 0)
//		return EPN_Pos(0);
//	EPN_Pos Temp(0, 0);
//
//	for (;;)
//	{
//		if (Angle >= 360)
//		{
//			Angle -= 360;
//		}
//		else
//			break;
//	}
//	Temp.X = (REAL32)sin((Angle)*3.141592653589793238462643383279 / 180);
//	Temp.Y = (REAL32)cos((Angle - 180)*3.141592653589793238462643383279 / 180);
//
//	return Temp;
//}
//INT32 EPN::COMMON::FUNCTION::EPN_ValueCount(INT32 &ValueNum, INT32 ValueMax)
//{
//	if (ValueNum++ >= ValueMax)
//	{
//		ValueNum = 0;
//	}
//	return ValueNum;
//}
//
//INT32 EPN::COMMON::FUNCTION::EPN_ValueCount(INT32 &ValueNum, INT32 ValueMax, INT32 &Count, INT32 CountTimer)
//{
//	if (Count++ >= CountTimer)
//	{
//		if (ValueNum++ >= ValueMax)
//		{
//			ValueNum = 0;
//		}
//		Count = 0;
//	}
//	return ValueNum;
//}
//INT32 EPN::COMMON::FUNCTION::EPN_ValueCount(INT32 &ValueNum, INT32 ValueMin, INT32 ValueMax, BOOL &Flag)
//{
//	if (ValueMin == ValueMax || ValueMin > ValueMax)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: IO_ValueCount \n( ValueMin , ValueMax 인자값에 문제가 있습니다 )" << endl;
//		return 0;
//	}
//	if (Flag)
//	{
//		if (ValueNum-- <= ValueMin)
//		{
//			ValueNum += 2;
//			Flag = FALSE;
//		}
//	}
//	else
//	{
//		if (ValueNum++ >= ValueMax)
//		{
//			ValueNum -= 2;
//			Flag = TRUE;
//		}
//	}
//	return ValueNum;
//}
//INT32 EPN::COMMON::FUNCTION::EPN_ValueCount(INT32 &ValueNum, INT32 ValueMin, INT32 ValueMax, INT32 &Count, INT32 CountTimer, BOOL &Flag)
//{
//	if (ValueMin == ValueMax || ValueMin > ValueMax)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: IO_ValueCount \n( ValueMin , ValueMax 인자값에 문제가 있습니다 )" << endl;
//		return 0;
//	}
//	if (Count++ >= CountTimer)
//	{
//
//		if (Flag)
//		{
//			if (ValueNum-- <= ValueMin)
//			{
//				ValueNum += 2;
//				Flag = FALSE;
//			}
//		}
//		else
//		{
//			if (ValueNum++ >= ValueMax)
//			{
//				ValueNum -= 2;
//				Flag = TRUE;
//			}
//		}
//		Count = 0;
//
//	}
//	return ValueNum;
//}
//
//
//BOOL EPN_TB_CheckSwap(REAL32 *A, REAL32 *B)
//{
//	if (*A > *B)
//	{
//		INT32 Swap = *A;
//		*A = *B;
//		*B = Swap;
//		return TRUE;
//	}
//	return FALSE;
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Rect B)
//{
//	EPN_TB_CheckSwap(&A.Top.X, &A.Bottom.X);
//	EPN_TB_CheckSwap(&B.Top.X, &B.Bottom.X);
//	EPN_TB_CheckSwap(&A.Top.Y, &A.Bottom.Y);
//	EPN_TB_CheckSwap(&B.Top.Y, &B.Bottom.Y);
//	if (A.Bottom.Y <= B.Top.Y) return FALSE; /* 사각형 A가 사각형 B의 上에 있는 경우 */
//	if (A.Top.Y > B.Bottom.Y) return FALSE; /* 사각형 A가 사각형 B의 下에 있는 경우 */
//	if (A.Bottom.X <= B.Top.X) return FALSE; /* 사각형 A가 사각형 B의 左에 있는 경우 */
//	if (A.Top.X > B.Bottom.X) return FALSE; /* 사각형 A가 사각형 B의 右에 있는 경우 */
//	return TRUE;
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Circle B)
//{
//	if (EPN_GetDistance(A.CirclePos, B.CirclePos) < (A.CircleRadius + B.CircleRadius))
//	{
//		return TRUE;
//	}
//
//	//If not 
//	return FALSE;
//}
//
//
//EPN_Pos EPN::COMMON::FUNCTION::EPN_GetMousePos()
//{
//	POINT pos;
//	pos.x = 0;
//	pos.y = 0;
//	GetCursorPos(&pos);
//	return EPN_Pos(pos.x, pos.y);
//}
//
//double EPN::COMMON::FUNCTION::EPN_GetDistance(EPN_Pos DestPos, EPN_Pos TargetPos)
//{
//	return sqrt(pow((double)(TargetPos.X - DestPos.X), 2) + pow((double)(TargetPos.Y - DestPos.Y), 2));
//}
////BOOL check_collision( Circle &A, std::vector &B ) 
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Rect B)
//{
//	if (((A.CirclePos.Y + A.CircleRadius <= B.Top.Y) ||
//		(A.CirclePos.Y - A.CircleRadius >= B.Top.Y + (B.Bottom.Y - B.Top.Y)) ||
//		(A.CirclePos.X <= B.Top.X) ||
//		(A.CirclePos.X >= B.Top.X + (B.Bottom.X - B.Top.X))) == false)
//	{
//		return TRUE;
//	}
//
//	if (((A.CirclePos.Y <= B.Top.Y) ||
//		(A.CirclePos.Y - A.CircleRadius >= B.Top.Y + (B.Bottom.Y - B.Top.Y)) ||
//		(A.CirclePos.X + A.CircleRadius <= B.Top.X) ||
//		(A.CirclePos.X - A.CircleRadius >= B.Top.X + (B.Bottom.X - B.Top.X))) == false)
//	{
//		return TRUE;
//	}
//	if ((EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X, B.Top.Y)) < A.CircleRadius) || (EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X + B.Bottom.X - B.Top.X, B.Top.Y)) < A.CircleRadius) ||
//		(EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X, B.Top.Y + B.Bottom.Y - B.Top.Y)) < A.CircleRadius) || (EPN_GetDistance(A.CirclePos, EPN_Pos(B.Top.X + B.Bottom.X - B.Top.X, B.Top.Y + B.Bottom.Y - B.Top.Y)) < A.CircleRadius))
//	{
//		return TRUE;
//	}
//
//
//	return FALSE;
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Circle B)
//{
//	if (((B.CirclePos.Y + B.CircleRadius <= A.Top.Y) ||
//		(B.CirclePos.Y - B.CircleRadius >= A.Top.Y + A.Bottom.Y - A.Top.Y) ||
//		(B.CirclePos.X <= A.Top.X) ||
//		(B.CirclePos.X >= A.Top.X + A.Bottom.X - A.Top.X)) == false)
//	{
//		return TRUE;
//	}
//
//	if (((B.CirclePos.Y <= A.Top.Y) ||
//		(B.CirclePos.Y - B.CircleRadius >= A.Top.Y + A.Bottom.Y - A.Top.Y) ||
//		(B.CirclePos.X + B.CircleRadius <= A.Top.X) ||
//		(B.CirclePos.X - B.CircleRadius >= A.Top.X + A.Bottom.X - A.Top.X)) == false)
//	{
//		return TRUE;
//	}
//	if ((EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X, A.Top.Y)) < B.CircleRadius) || (EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X + A.Bottom.X - A.Top.X, A.Top.Y)) < B.CircleRadius) ||
//		(EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X, A.Top.Y + A.Bottom.Y - A.Top.Y)) < B.CircleRadius) || (EPN_GetDistance(B.CirclePos, EPN_Pos(A.Top.X + A.Bottom.X - A.Top.X, A.Top.Y + A.Bottom.Y - A.Top.Y)) < B.CircleRadius))
//	{
//		return TRUE;
//	}
//
//
//	return FALSE;
//}
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Rect B)
//{
//	return EPN::COMMON::FUNCTION::EPN_CheckCollision(A, &EPN_Polygon(B));
//}
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Rect A, EPN_Polygon* B)
//{
//	return EPN::COMMON::FUNCTION::EPN_CheckCollision(&EPN_Polygon(A), B);
//}
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Polygon* B)
//{
//	if (A == NULL || B == NULL || A->getVertexNum() <= 0 || B->getVertexNum() <= 0)
//		return false;
//
//
//	EPN_Rect BoxA = EPN_Rect(A->getConvertVertexPos(0).X, A->getConvertVertexPos(0).Y, A->getConvertVertexPos(0).X, A->getConvertVertexPos(0).Y);
//	EPN_Rect BoxB = EPN_Rect(B->getConvertVertexPos(0).X, B->getConvertVertexPos(0).Y, B->getConvertVertexPos(0).X, B->getConvertVertexPos(0).Y);
//	REAL32 LineDistanceA = 0;
//	REAL32 LineDistanceB = 0;
//	for (INT32 i = 0; i < A->getVertexNum(); ++i)
//	{
//		if (BoxA.Top.X > A->getConvertVertexPos(i).X)
//		{
//			BoxA.Top.X = A->getConvertVertexPos(i).X;
//		}
//		if (BoxA.Top.Y > A->getConvertVertexPos(i).Y)
//		{
//			BoxA.Top.Y = A->getConvertVertexPos(i).Y;
//		}
//		if (BoxA.Bottom.X < A->getConvertVertexPos(i).X)
//		{
//			BoxA.Bottom.X = A->getConvertVertexPos(i).X;
//		}
//		if (BoxA.Bottom.Y < A->getConvertVertexPos(i).Y)
//		{
//			BoxA.Bottom.Y = A->getConvertVertexPos(i).Y;
//		}
//		if (i == A->getVertexNum() - 1)
//		{
//			REAL32 Distance = EPN_GetDistance(A->getConvertVertexPos(i), A->getConvertVertexPos(0));
//			if (LineDistanceA < Distance)
//				LineDistanceA = Distance;
//		}
//		else
//		{
//			REAL32 Distance = EPN_GetDistance(A->getConvertVertexPos(i), A->getConvertVertexPos(i + 1));
//			if (LineDistanceA < Distance)
//				LineDistanceA = Distance;
//		}
//	}
//	for (INT32 i = 0; i < B->getVertexNum(); ++i)
//	{
//		if (BoxB.Top.X > B->getConvertVertexPos(i).X)
//		{
//			BoxB.Top.X = B->getConvertVertexPos(i).X;
//		}
//		if (BoxB.Top.Y > B->getConvertVertexPos(i).Y)
//		{
//			BoxB.Top.Y = B->getConvertVertexPos(i).Y;
//		}
//		if (BoxB.Bottom.X < B->getConvertVertexPos(i).X)
//		{
//			BoxB.Bottom.X = B->getConvertVertexPos(i).X;
//		}
//		if (BoxB.Bottom.Y < B->getConvertVertexPos(i).Y)
//		{
//			BoxB.Bottom.Y = B->getConvertVertexPos(i).Y;
//		}
//		if (i == B->getVertexNum() - 1)
//		{
//			REAL32 Distance = EPN_GetDistance(B->getConvertVertexPos(i), B->getConvertVertexPos(0));
//			if (LineDistanceB < Distance)
//				LineDistanceB = Distance;
//		}
//		else
//		{
//			REAL32 Distance = EPN_GetDistance(B->getConvertVertexPos(i), B->getConvertVertexPos(i + 1));
//			if (LineDistanceB < Distance)
//				LineDistanceB = Distance;
//		}
//	}
//	if (EPN_CheckCollision(BoxA, BoxB))
//	{
//		long X = 0, Y = 0;
//
//		for (INT32 i = 0; i < A->getVertexNum(); ++i)
//		{
//			EPN_Pos CheckPos = A->getConvertVertexPos(i);
//			REAL32 Angle = rand() % 360;
//			REAL32 Add = 0.5f;
//			if (B->getVertexNum() > 2)
//			{
//				for (;;)
//				{
//					BOOL CheckFlag = false;
//					for (INT32 j = 0; j < B->getVertexNum(); ++j)
//					{
//						if (EPN_GetPosAngle(CheckPos, B->getConvertVertexPos(j)) == Angle)
//						{
//							CheckFlag = true;
//							break;
//						}
//					}
//					if (!CheckFlag)
//					{
//						EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * (LineDistanceB + EPN_GetDistance(CheckPos, (BoxB.Top + BoxB.Bottom) / 2)) * 2;
//						INT32 ColCount = 0;
//						for (INT32 j = 0; j < B->getVertexNum(); ++j)
//						{
//							EPN_Pos B_Line[2];
//							if (j == B->getVertexNum() - 1)
//							{
//								B_Line[0] = B->getConvertVertexPos(j);
//								B_Line[1] = B->getConvertVertexPos(0);
//							}
//							else
//							{
//								B_Line[0] = B->getConvertVertexPos(j);
//								B_Line[1] = B->getConvertVertexPos(j + 1);
//							}
//							if (LineColCheck(CheckPos.X, CheckPos.Y, CheckPos2.X, CheckPos2.Y, B_Line[0].X, B_Line[0].Y, B_Line[1].X, B_Line[1].Y, &X, &Y))
//							{
//								ColCount++;
//							}
//						}
//						if (ColCount > 0 && ColCount % 2 == 1)
//						{
//							return true;
//						}
//						break;
//					}
//					Angle += Add;
//					if (Angle >= 360)
//					{
//						Angle = 0;
//						Add /= 2;
//					}
//				}
//			}
//		}
//		for (INT32 i = 0; i < B->getVertexNum(); ++i)
//		{
//			EPN_Pos CheckPos = B->getConvertVertexPos(i);
//			REAL32 Angle = rand() % 360;
//			REAL32 Add = 0.5f;
//
//			if (A->getVertexNum() > 2)
//			{
//				for (;;)
//				{
//					BOOL CheckFlag = false;
//					for (INT32 j = 0; j < A->getVertexNum(); ++j)
//					{
//						if (EPN_GetPosAngle(CheckPos, A->getConvertVertexPos(j)) == Angle)
//						{
//							CheckFlag = true;
//							break;
//						}
//					}
//					if (!CheckFlag)
//					{
//						EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * (LineDistanceA + EPN_GetDistance(CheckPos, (BoxA.Top + BoxA.Bottom) / 2)) * 2;
//						INT32 ColCount = 0;
//						for (INT32 j = 0; j < A->getVertexNum(); ++j)
//						{
//							EPN_Pos A_Line[2];
//							if (j == A->getVertexNum() - 1)
//							{
//								A_Line[0] = A->getConvertVertexPos(j);
//								A_Line[1] = A->getConvertVertexPos(0);
//							}
//							else
//							{
//								A_Line[0] = A->getConvertVertexPos(j);
//								A_Line[1] = A->getConvertVertexPos(j + 1);
//							}
//							if (LineColCheck(CheckPos.X, CheckPos.Y, CheckPos2.X, CheckPos2.Y, A_Line[0].X, A_Line[0].Y, A_Line[1].X, A_Line[1].Y, &X, &Y))
//							{
//								ColCount++;
//							}
//						}
//						if (ColCount > 0 && ColCount % 2 == 1)
//						{
//							return true;
//						}
//						break;
//					}
//					Angle += Add;
//					if (Angle >= 360)
//					{
//						Angle = 0;
//						Add /= 2;
//					}
//				}
//			}
//		}
//	}
//
//	return false;
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Polygon* A, EPN_Circle B)
//{
//	if (A == NULL || A->getVertexNum() <= 0)
//		return false;
//
//	EPN_Rect BoxA = EPN_Rect(A->getConvertVertexPos(0).X, A->getConvertVertexPos(0).Y, A->getConvertVertexPos(0).X, A->getConvertVertexPos(0).Y);
//	EPN_Rect BoxB = EPN_Rect(0, 0, 0, 0);
//	REAL32 LineDistanceA = 0;
//	REAL32 LineDistanceB = B.CircleRadius * 2;
//	for (INT32 i = 0; i < A->getVertexNum(); ++i)
//	{
//		if (BoxA.Top.X > A->getConvertVertexPos(i).X)
//		{
//			BoxA.Top.X = A->getConvertVertexPos(i).X;
//		}
//		if (BoxA.Top.Y > A->getConvertVertexPos(i).Y)
//		{
//			BoxA.Top.Y = A->getConvertVertexPos(i).Y;
//		}
//		if (BoxA.Bottom.X < A->getConvertVertexPos(i).X)
//		{
//			BoxA.Bottom.X = A->getConvertVertexPos(i).X;
//		}
//		if (BoxA.Bottom.Y < A->getConvertVertexPos(i).Y)
//		{
//			BoxA.Bottom.Y = A->getConvertVertexPos(i).Y;
//		}
//		if (i == A->getVertexNum() - 1)
//		{
//			REAL32 Distance = EPN_GetDistance(A->getConvertVertexPos(i), A->getConvertVertexPos(0));
//			if (LineDistanceA < Distance)
//				LineDistanceA = Distance;
//		}
//		else
//		{
//			REAL32 Distance = EPN_GetDistance(A->getConvertVertexPos(i), A->getConvertVertexPos(i + 1));
//			if (LineDistanceA < Distance)
//				LineDistanceA = Distance;
//		}
//	}
//	BoxB.Top.X = (B.CirclePos + EPN_GetAnglePos(270) * B.CircleRadius).X;
//	BoxB.Top.Y = (B.CirclePos + EPN_GetAnglePos(0) * B.CircleRadius).Y;
//	BoxB.Bottom.X = (B.CirclePos + EPN_GetAnglePos(90) * B.CircleRadius).X;
//	BoxB.Bottom.Y = (B.CirclePos + EPN_GetAnglePos(180) * B.CircleRadius).Y;
//
//	if (EPN_CheckCollision(BoxA, BoxB))
//	{
//		long X = 0, Y = 0;
//
//		for (INT32 i = 0; i < A->getVertexNum(); ++i)
//		{
//			if (EPN_GetDistance(A->getConvertVertexPos(i), B.CirclePos) <= B.CircleRadius)
//			{
//				return true;
//			}
//		}
//
//		for (INT32 i = 0; i < A->getVertexNum(); ++i)
//		{
//			EPN_Pos A_Line[2];
//			if (i == A->getVertexNum() - 1)
//			{
//				A_Line[0] = A->getConvertVertexPos(i);
//				A_Line[1] = A->getConvertVertexPos(0);
//			}
//			else
//			{
//				A_Line[0] = A->getConvertVertexPos(i);
//				A_Line[1] = A->getConvertVertexPos(i + 1);
//			}
//
//			REAL32 Angle = EPN_GetPosAngle(A_Line[0], A_Line[1]);
//			EPN_Pos B_Line[2];
//			B_Line[0] = B.CirclePos + EPN_GetAnglePos(Angle - 90)*B.CircleRadius;
//			B_Line[1] = B.CirclePos + EPN_GetAnglePos(Angle + 90)*B.CircleRadius;
//			if (LineColCheck(A_Line[0].X, A_Line[0].Y, A_Line[1].X, A_Line[1].Y, B_Line[0].X, B_Line[0].Y, B_Line[1].X, B_Line[1].Y, &X, &Y))
//			{
//				/*if(pColPos)
//				(*pColPos) = EPN_Pos(X,Y);*/
//				return true;
//			}
//
//		}
//
//		EPN_Pos CheckPos = B.CirclePos;
//		REAL32 Angle = rand() % 360;
//		REAL32 Add = 0.5f;
//
//		if (A->getVertexNum() > 2)
//		{
//			for (;;)
//			{
//				BOOL CheckFlag = false;
//				for (INT32 i = 0; i < A->getVertexNum(); ++i)
//				{
//					if (EPN_GetPosAngle(CheckPos, A->getConvertVertexPos(i)) == Angle)
//					{
//						CheckFlag = true;
//						break;
//					}
//				}
//				if (!CheckFlag)
//				{
//					EPN_Pos CheckPos2 = CheckPos + EPN_GetAnglePos(Angle) * (LineDistanceA + EPN_GetDistance(CheckPos, (BoxA.Top + BoxA.Bottom) / 2)) * 2;
//					INT32 ColCount = 0;
//					for (INT32 i = 0; i < A->getVertexNum(); ++i)
//					{
//						EPN_Pos A_Line[2];
//						if (i == A->getVertexNum() - 1)
//						{
//							A_Line[0] = A->getConvertVertexPos(i);
//							A_Line[1] = A->getConvertVertexPos(0);
//						}
//						else
//						{
//							A_Line[0] = A->getConvertVertexPos(i);
//							A_Line[1] = A->getConvertVertexPos(i + 1);
//						}
//						if (LineColCheck(CheckPos.X, CheckPos.Y, CheckPos2.X, CheckPos2.Y, A_Line[0].X, A_Line[0].Y, A_Line[1].X, A_Line[1].Y, &X, &Y))
//						{
//							ColCount++;
//						}
//					}
//					if (ColCount > 0 && ColCount % 2 == 1)
//						return true;
//					break;
//				}
//				Angle += Add;
//				if (Angle >= 360)
//				{
//					Angle = 0;
//					Add /= 2;
//				}
//			}
//		}
//	}
//	return false;
//}
//BOOL EPN::COMMON::FUNCTION::EPN_CheckCollision(EPN_Circle A, EPN_Polygon* B)
//{
//	return EPN::COMMON::FUNCTION::EPN_CheckCollision(B, A);
//}
//BOOL EPN::COMMON::FUNCTION::EPN_WriteRegistry(HKEY PATH_FLAG, estring Path, estring Name, estring Value)
//{
//	HKEY hk;
//	RegCreateKeyW(PATH_FLAG, Path.getStrW().GetStr(), &hk);
//	RegSetValueEx(hk, Name.getStrW().GetStr(), 0, REG_SZ, (LPBYTE)(Value.getStrW().GetStr()), MAX_PATH);
//
//	RegCloseKey(hk);
//	if (EPN_ReadRegistry(PATH_FLAG, Path, Name) != Value)
//		return FALSE;
//
//	return TRUE;
//}
//estring EPN::COMMON::FUNCTION::EPN_ReadRegistry(HKEY PATH_FLAG, estring Path, estring Name)
//{
//	HKEY hk;
//	wchar_t DataValue[MAX_PATH] = L"";
//	DWORD DataSize = MAX_PATH;
//
//	RegOpenKeyEx(PATH_FLAG, Path.getStrW().GetStr(), 0, KEY_ALL_ACCESS, &hk);
//	INT32 ret = RegQueryValueEx(hk, Name.getStrW().GetStr(), 0, NULL, (LPBYTE)DataValue, &DataSize);
//	RegCloseKey(hk);
//	return E_TEXT(DataValue);
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_WriteClipboard(estring source)
//{
//	BOOL check = OpenClipboard(NULL);
//	if (!check) return FALSE;
//
//	HGLOBAL clipbuffer;
//	CHAR *buffer = NULL;
//
//	EmptyClipboard();
//	clipbuffer = GlobalAlloc(GMEM_DDESHARE, source.Length() + 1);
//	buffer = (CHAR*)GlobalLock(clipbuffer);
//	if (buffer)
//	{
//		strcpy(buffer, source.getStrA().GetStr());
//		GlobalUnlock(clipbuffer);
//		SetClipboardData(CF_TEXT, clipbuffer);
//		CloseClipboard();
//		return TRUE;
//	}
//	return FALSE;
//}
//estring EPN::COMMON::FUNCTION::EPN_ReadClipboard()
//{
//	if (!OpenClipboard(NULL))
//		return "";
//
//	HANDLE hData = GetClipboardData(CF_UNICODETEXT);
//	if (hData == NULL)
//		return "";
//
//	wchar_t *pszText = static_cast<wchar_t*>(GlobalLock(hData));
//	if (pszText == NULL)
//		return "";
//
//	estring result(pszText);
//
//
//	GlobalUnlock(hData);
//
//	CloseClipboard();
//
//	return result;
//}
//
//long EPN::COMMON::FUNCTION::EPN_GetFileSize(estring Path)
//{
//	FILE *File = fopen(Path.getStrA().GetStr(), "rb");
//	if (!File)
//		return -1;
//	fseek(File, 0, SEEK_END);
//	long Size = ftell(File);
//	fclose(File);
//	return Size;
//}
//
//BOOL EPN::COMMON::FUNCTION::EPN_CopyFile(estring ReadPath, estring WritePath)
//{
//	FILE *in = NULL, *out = NULL;
//	CHAR* buf = NULL;
//	size_t len = 0;
//
//	if (ReadPath == WritePath)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CopyFile \n( 같은경로 복사불가능 )" << endl;
//
//		return FALSE;
//	}
//
//	if ((in = fopen(ReadPath.getStrA().GetStr(), "rb")) == NULL)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CopyFile \n( 파일읽기 실패 )" << endl;
//		return FALSE;
//	}
//	if ((out = fopen(WritePath.getStrA().GetStr(), "wb")) == NULL)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CopyFile \n( 파일쓰기 실패 )" << endl;
//		fclose(in);
//		return FALSE;
//	}
//
//	if ((buf = (CHAR *)malloc(16777216 * 2)) == NULL)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CopyFile \n( 파일복사중 동적할당 실패 )" << endl;
//
//		fclose(in);
//		fclose(out);
//		return FALSE;
//	}
//
//	while ((len = fread(buf, sizeof(CHAR), sizeof(buf), in)) != NULL)
//	{
//		if (fwrite(buf, sizeof(CHAR), len, out) == 0)
//		{
//			cout << "[E P N] Error - EPN_Enpine :: EPN_CopyFile \n( 파일복사 실패 )" << endl;
//
//			fclose(in); fclose(out);
//			free(buf);
//			_unlink(WritePath.getStrA().GetStr());
//			return FALSE;
//		}
//	}
//	fclose(in);
//	fclose(out);
//	free(buf);
//
//	return TRUE;
//}
//BOOL EPN::COMMON::FUNCTION::EPN_CompressionFile(estring ReadPath, estring WritePath)
//{
//	long FileSize = EPN_GetFileSize("./EPN_7z.dll");
//	if (FileSize < 0)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( EPN_7z.dll 파일을 찾을수 없습니다 )" << endl;
//		return FALSE;
//	}
//	if (FileSize != 1078272)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( EPN_7z.dll 파일이 변조되었습니다 )" << endl;
//		return FALSE;
//	}
//
//	FileSize = EPN_GetFileSize(ReadPath.getStrA().GetStr());
//	if (FileSize < 0)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( 압축할 파일을 찾을수 없습니다 )" << endl;
//		return FALSE;
//	}
//
//
//	FILE* DLL_File = fopen("./EPN_7z.dll", "rb");
//	if (DLL_File)
//	{
//		FILE* DLL_7z = fopen("./7z.dll", "wb");
//		if (!DLL_7z)
//		{
//			cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( 압축 실패 )" << endl;
//			return FALSE;
//		}
//		FILE* EXE_7z = fopen("./Compress.exe", "wb");
//		if (!EXE_7z)
//		{
//			_unlink("./7z.dll");
//			cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( 압축 실패 )" << endl;
//			return FALSE;
//		}
//		long Count = 0;
//		while (1)
//		{
//			INT32 Byte = fgetc(DLL_File);
//			if (Byte == -1)
//				break;
//			if (Count++ < 914432)
//			{
//				fputc(Byte, DLL_7z);
//			}
//			else
//			{
//				fputc(Byte, EXE_7z);
//			}
//		}
//		fclose(DLL_7z);
//		fclose(EXE_7z);
//
//		_unlink(WritePath.getStrA().GetStr());
//
//		system((estring(".\\Compress.exe a ") + WritePath + " " + ReadPath).getStrA().GetStr());
//
//		_unlink("./7z.dll");
//		_unlink("./Compress.exe");
//
//		return TRUE;
//	}
//
//	cout << "[E P N] Error - EPN_Enpine :: EPN_CompressionFile \n( 압축 실패 )" << endl;
//
//	return FALSE;
//}
//BOOL EPN::COMMON::FUNCTION::EPN_DeCompressionFile(estring ReadPath, estring WritePath)
//{
//	long FileSize = EPN_GetFileSize("./EPN_7z.dll");
//	if (FileSize < 0)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( EPN_7z.dll 파일을 찾을수 없습니다 )" << endl;
//		return FALSE;
//	}
//	if (FileSize != 1078272)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( EPN_7z.dll 파일이 변조되었습니다 )" << endl;
//		return FALSE;
//	}
//
//
//	FileSize = EPN_GetFileSize(ReadPath.getStrA().GetStr());
//	if (FileSize < 0)
//	{
//		cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( 압축해제할 파일을 찾을수 없습니다 )" << endl;
//		return FALSE;
//	}
//
//
//	FILE* DLL_File = fopen("./EPN_7z.dll", "rb");
//	if (DLL_File)
//	{
//		FILE* DLL_7z = fopen("./7z.dll", "wb");
//		if (!DLL_7z)
//		{
//			cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( 압축해제 실패 )" << endl;
//			return FALSE;
//		}
//		FILE* EXE_7z = fopen("./Compress.exe", "wb");
//		if (!EXE_7z)
//		{
//			_unlink("./7z.dll");
//			cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( 압축해제 실패 )" << endl;
//			return FALSE;
//		}
//		long Count = 0;
//		while (1)
//		{
//			INT32 Byte = fgetc(DLL_File);
//			if (Byte == -1)
//				break;
//			if (Count++ < 914432)
//			{
//				fputc(Byte, DLL_7z);
//			}
//			else
//			{
//				fputc(Byte, EXE_7z);
//			}
//		}
//		fclose(DLL_7z);
//		fclose(EXE_7z);
//
//		system((estring(".\\Compress.exe x ") + ReadPath + " -o" + WritePath + " -aoa").getStrA().GetStr());
//
//		_unlink("./7z.dll");
//		_unlink("./Compress.exe");
//
//		return TRUE;
//	}
//
//	cout << "[E P N] Error - EPN_Enpine :: EPN_DeCompressionFile \n( 압축해제 실패 )" << endl;
//
//	return FALSE;
//}
//
//
//
////BOOL EPN :: COMMON :: FUNCTION :: EPN_GetDirectory (estring Path, vector<estring> &FileList)
////{
////    DIR *dp = NULL;
////    struct dirent *dirp;
////    if((dp  = opendir(Path.getStrA().GetStr())) == NULL) {
////        cout << "Error(" << errno << ") opening " << dir << endl;
////        return FALSE;
////    }
////
////    while ((dirp = readdir(dp)) != NULL) {
////        getdir.push_back(string(dirp->d_name));
////    }
////    closedir(dp);
////    return TRUE;
////}
//
//
////double distance( INT32 x1, INT32 y1, INT32 x2, INT32 y2 ) 
////{ 
////	//Return the distance between the two points 
////	return sqrt( pow( (double)(x2 - x1), 2 ) + pow( (double)(y2 - y1), 2 ) ); 
////}  
////
////BOOL check_collision( Circle &A, Circle &B ) 
////{ 
////	//If the distance between the centers of the circles is less than the sum of their radii 
////	if( distance( A.x, A.y, B.x, B.y ) < ( A.r + B.r ) ) 
////	{ 
////		//The circles have collided 
////		return true; 
////	} 
////
////	//If not 
////	return false; 
////} 
////
////BOOL check_collision( Circle &A, std::vector &B ) 
////{ 
////	//The sides of the shapes 
////	INT32 leftAv, leftAh, leftB; 
////	INT32 rightAv, rightAh, rightB; 
////	INT32 topAv, topAh, topB; 
////	INT32 bottomAv, bottomAh, bottomB; 
////
////	//The corners of box B 
////	INT32 Bx1, By1; 
////	INT32 Bx2, By2; 
////	INT32 Bx3, By3; 
////	INT32 Bx4, By4; 
////	//Calculate the sides of A 
////	leftAv = A.x; 
////	rightAv = A.x; 
////	topAv = A.y - A.r; 
////	bottomAv = A.y + A.r; 
////
////	leftAh = A.x - A.r; 
////	rightAh = A.x + A.r; 
////	topAh = A.y; 
////	bottomAh = A.y; 
////	//Go through the B boxes 
////	for( INT32 Bbox = 0; Bbox < B.size(); Bbox++ ) 
////	{ 
////		//Calculate the sides of rect B 
////		leftB = B[ Bbox ].x; 
////		rightB = B[ Bbox ].x + B[ Bbox ].w; 
////		topB = B[ Bbox ].y; 
////		bottomB = B[ Bbox ].y + B[ Bbox ].h; 
////
////		//Calculate the corners of B 
////		Bx1 = B[ Bbox ].x, By1 = B[ Bbox ].y; 
////		Bx2 = B[ Bbox ].x + B[ Bbox ].w, By2 = B[ Bbox ].y; 
////		Bx3 = B[ Bbox ].x, By3 = B[ Bbox ].y + B[ Bbox ].h; 
////		Bx4 = B[ Bbox ].x + B[ Bbox ].w, By4 = B[ Bbox ].y + B[ Bbox ].h; 
////		//If no sides from vertical A are outside of B 
////		if( ( ( bottomAv <= topB ) || ( topAv >= bottomB ) || ( rightAv <= leftB ) || ( leftAv >= rightB ) ) == false ) 
////		{ 
////			//A collision is detected 
////			return true; 
////		} 
////
////		//If no sides from horizontal A are outside of B 
////		if( ( ( bottomAh <= topB ) || ( topAh >= bottomB ) || ( rightAh <= leftB ) || ( leftAh >= rightB ) ) == false ) 
////		{ 
////			//A collision is detected 
////			return true; 
////		} 
////		//If any of the corners from B are inside A 
////		if( ( distance( A.x, A.y, Bx1, By1 ) < A.r ) || ( distance( A.x, A.y, Bx2, By2 ) < A.r ) || 
////			( distance( A.x, A.y, Bx3, By3 ) < A.r ) || ( distance( A.x, A.y, Bx4, By4 ) < A.r ) ) 
////		{ 
////			//A collision is detected 
////			return true; 
////		} 
////	} 
////
////	//If the shapes have not collided 
////	return false; 
////}  
////