#include "ViewInGame.h"
#include "MonsterArcher.h"
#include "MonsterSpecial.h"
#include "MonsterClose.h"
#include "Boss1.h"
#include "Boss2.h"
#include "Boss3.h"
#include "ViewLose.h"

ViewInGame::ViewInGame() :/*
	m_BulletDelay(10),
	m_BulletDelayCnt(0),*/
	//m_ShootEnbleFlag(true),
	m_IsCanOpenPortal(false),
	m_MapChangeFlag(true),
	m_IsEnd(false),
	m_EndingCnt(0)
{
	m_pObjManager = ObjectManager::CreateInstance();
	m_pCharacter = new Character();
	m_pObjManager->AddObject(m_pCharacter);
	m_pMap = MapManager::CreateInstance();
	m_pMap->SetStage(0);

	m_pEPN_SI->PlayBgm("Sound/InGame/Bgm.mp3", true);
	m_pEPN_SI->SetBgmVolume(50);
	m_pEPN_SI->SetEffectVolume(50);

	m_SettingButton.SetPos(EPN_Pos(771, 593));
	m_SettingButton.SetTexture("BtnSetting", BUTTON_STATE::ALL);
	m_SettingButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_SettingButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_SettingButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_SettingButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_SettingButton.SetScaling(EPN_Scaling(0.5f, 0.5f));
	
	m_SettingWindow.SetPopUpType(false);
	m_SettingWindow.SetActive(false);
	m_SettingWindow.SetPos(EPN_Pos(253, 450));
	
	m_HelpWindow.SetPopUpType(true);
	m_HelpWindow.SetActive(false);
	m_HelpWindow.SetPos(EPN_Pos(201, 500));
}

ViewInGame::~ViewInGame()
{
	m_pObjManager->ClearObject();
	m_pEPN_SI->StopBgm();
}

void ViewInGame::PortalRun()
{
	//포탈
	for (int i = 0; i < 2; ++i)
	{
		//현재 포탈 정보
		PortalInfo CurPortInfo = m_CurMapInfo->PortInfo[i];

		//포탈 렉트가 -1, -1, -1, -1이면 없는것으로 취급
		if (CurPortInfo.PortalArea != EPN_Rect(-1, -1, -1, -1))
		{
			if (EPN_CheckCollision(EPN_Rect(EPN_Rect(m_pCharacter->GetObjectInfo()->Pos) + EPN_Rect(0, 0, 30, -30)), CurPortInfo.PortalArea))
			{
				m_pMap->SetStage(CurPortInfo.TargetMapStage);
				m_pCharacter->GetObjectInfo()->Pos = CurPortInfo.LinkedPos;
				m_IsCanOpenPortal = false;
				m_MapChangeFlag = true;
				m_pObjManager->ClearItem();
			}
		}
	}
}

void ViewInGame::AddMonster(MONSTER_TYPE MonsterType, EPN_Pos MonsterPos)
{
	EPN_Pos Pos = EPN_Pos((int)(MonsterPos.X / 32) * 32, (int)(MonsterPos.Y / 32) * 32);
	MonsterBase* Monster;
	switch (MonsterType)
	{
	case MONSTER_TYPE::CLOSE:
		Monster = new MonsterClose(Pos);
		break;
	case MONSTER_TYPE::ARCHER:
		Monster = new MonsterArcher(Pos);
		break;
	case MONSTER_TYPE::SPECIAL:
		Monster = new MonsterSpecial(Pos);
		break;
	}
	m_pObjManager->AddObject(Monster);
}
	
void ViewInGame::CreateMonster()
{
	int OriginVolumn;
	switch (m_pMap->GetCurStageIndex())
	{
	case 0:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(600, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 500));
		break;
	case 1:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 100));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(300, 500));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(300, 400));
		break;
	case 2:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(450, 120));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(420, 140));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 450));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(500, 500));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(400, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(420, 500));
		break;
	case 3:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(300, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 250));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 100));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(550, 250));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(500, 500));
		break;
	case 4:
		OriginVolumn = m_pEPN_SI->GetBgmVolume();
		m_pEPN_SI->StopBgm();
		m_pEPN_SI->PlayBgm("Sound/InGame/Boss.mp3");
		m_pEPN_SI->SetBgmVolume(OriginVolumn);
		m_pObjManager->AddObject(new Boss1());
		break;
	case 5:
		OriginVolumn = m_pEPN_SI->GetBgmVolume();
		m_pEPN_SI->StopBgm();
		m_pEPN_SI->PlayBgm("Sound/InGame/Bgm.mp3", true);
		m_pEPN_SI->SetBgmVolume(OriginVolumn);
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(250, 200));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(100, 200));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(200, 250));
		break;
	case 6:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(130, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(250, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(130, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(100, 250));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(200, 300));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(250, 250));
		break;
	case 7:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 400));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(120, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(200, 250));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(230, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(300, 350));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(320, 300));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(300, 250));
		break;
	case 8:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 250));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(500, 100));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(600, 200));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(400, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(550, 350));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(580, 200));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(300, 250));
		break;
	case 9:
		OriginVolumn = m_pEPN_SI->GetBgmVolume();
		m_pEPN_SI->StopBgm();
		m_pEPN_SI->PlayBgm("Sound/InGame/Boss.mp3");
		m_pEPN_SI->SetBgmVolume(OriginVolumn);
		m_pObjManager->AddObject(new Boss2());
		break;
	case 10:
		OriginVolumn = m_pEPN_SI->GetBgmVolume();
		m_pEPN_SI->StopBgm();
		m_pEPN_SI->PlayBgm("Sound/InGame/Bgm.mp3", true);
		m_pEPN_SI->SetBgmVolume(OriginVolumn);
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 250));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(120, 300));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(220, 350));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(140, 200));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(600, 450));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(700, 250));
		break;
	case 11:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 500));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(220, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 450));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(250, 120));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(230, 140));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(300, 150));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(600, 350));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(710, 410));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(450, 550));
		break;
	case 12:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(180, 100));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 300));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 350));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(500, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 250));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(500, 400));

		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(480, 120));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(300, 220));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 330));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 320));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(500, 280));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(430, 100));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(620, 500));
		break;
	case 13:
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(100, 450));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(600, 200));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(600, 370));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(400, 350));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(200, 200));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(700, 550));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(300, 340));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(720, 450));

		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(300, 220));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(330, 240));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(390, 230));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(700, 450));
		AddMonster(MONSTER_TYPE::CLOSE, EPN_Pos(680, 530));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(720, 480));
		AddMonster(MONSTER_TYPE::ARCHER, EPN_Pos(230, 510));
		AddMonster(MONSTER_TYPE::SPECIAL, EPN_Pos(250, 510));
		break;
	case 14:
		OriginVolumn = m_pEPN_SI->GetBgmVolume();
		m_pEPN_SI->StopBgm();
		m_pEPN_SI->PlayBgm("Sound/InGame/Boss.mp3");
		m_pEPN_SI->SetBgmVolume(OriginVolumn);
		m_Boss3 = new Boss3();
		m_pObjManager->AddObject(m_Boss3);
		break;
	}
}

void ViewInGame::DisplayUI()
{
	//HP바
	m_pEPN_TI->Blt("CharacterHpBarBg", 0, EPN_Pos(32, 19), EPN_Rect(-1, -1, -1, -1));
	m_pEPN_TI->Blt("CharacterHpBar", 0, EPN_Pos(32, 19), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f,
		EPN_Scaling(m_pCharacter->GetObjectInfo()->Hp / m_pCharacter->GetObjectInfo()->MaxHp, 1.f));
	m_pEPN_FI->BltText("OldFont15", 0, "HP ", EPN_Pos(67, 19), EPN_ARGB(255, 0, 0, 0));

	//캐릭터 정보(파워, 스피드)
	estring Power = m_pCharacter->GetObjectInfo()->Atk;
	estring Speed = m_pCharacter->GetObjectInfo()->Speed;
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("STAGE ") + (m_pMap->GetCurStageIndex() + 1), EPN_Pos(200, 19), EPN_ARGB(255, 255, 255, 255));
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("POWER ") + Power.at(0) + Power.at(1) + Power.at(2) + Power.at(3), EPN_Pos(350, 19), EPN_ARGB(255, 255, 255, 255));
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("SPEED ") + Speed.at(0) + Speed.at(1) + Speed.at(2), EPN_Pos(550, 19), EPN_ARGB(255, 255, 255, 255));
}

VIEW_STATE ViewInGame::Run()
{
	//맵 바꿀때만 동작
	if (m_MapChangeFlag)
	{
		m_CurMapInfo = m_pMap->GetCurMapInfo();
		m_CurMapTexture = m_CurMapInfo->TextureName;

		//몬스터 생성
		CreateMonster();

		m_MapChangeFlag = false;
	}

	//캐릭터 HP가 0이되면 게임 오버
	if (m_pCharacter->GetObjectInfo()->Hp == 0)
		return VIEW_STATE::LOSE;
	
	//마지막 스테이지 보스를 잡으면 게임 클리어
	if (m_pMap->GetCurStageIndex() == 14)
	{
		if (m_Boss3->GetObjectInfo()->IsEnable == false)
			m_IsEnd = true;
	}
	if (m_IsEnd)
	{
		if (++m_EndingCnt >= 420)
			return VIEW_STATE::WIN;
	}

	//캐릭터보다 랜더링이 낮은 레이어 텍스쳐
	m_pEPN_TI->Blt(m_CurMapTexture + "1", 0, EPN_Pos(0, 600), EPN_Rect(-1, -1, -1, -1));
	m_pEPN_TI->Blt(m_CurMapTexture + "2", 0, EPN_Pos(0, 600), EPN_Rect(-1, -1, -1, -1));

	//오브젝트 런(캐릭터와 몬스터들)
	m_pObjManager->Run();

	//캐릭터보다 랜더링이 높은 레이어 텍스쳐
	for(int i = 3; i <= m_CurMapInfo->LayerNum; ++i)
		m_pEPN_TI->Blt(m_CurMapTexture + i, 0, EPN_Pos(0, 600), EPN_Rect(-1, -1, -1, -1));

	//UI 출력
	DisplayUI();

	////몬스터 없을때만 포탈 실행
	if(!m_pObjManager->IsExistMonster())
		m_IsCanOpenPortal = true;

	//포탈
	if(m_IsCanOpenPortal)
		PortalRun();

	m_SettingButton.Run();
	m_SettingWindow.Run();
	m_HelpWindow.Run();

	if (m_SettingButton.GetState() == BUTTON_STATE::POP)
		m_SettingWindow.SetActive(true);
	if (m_SettingWindow.GetHelpButtonState() == BUTTON_STATE::POP)
		m_HelpWindow.SetActive(true);
	if (m_SettingWindow.GetReturnButtonState() == BUTTON_STATE::POP)
		return VIEW_STATE::MENU;

	return VIEW_STATE::INGAME;
}