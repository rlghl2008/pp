#include "ViewGuide.h"
#include "MonsterBase.h"

ViewGuide::ViewGuide():
	m_BulletDelay(10),
	m_BulletDelayCnt(0),
	m_ShootEnbleFlag(true)
{
	memset(m_IsItemDisappeared, false, (int)ITEM_TYPE::MAX);
	memset(m_ItemDisappearedTime, 0, (int)ITEM_TYPE::MAX);

	m_pObjManager = ObjectManager::CreateInstance();
	m_pCharacter = new Character();
	m_pCharacter->GetObjectInfo()->Hp = 50;
	m_pObjManager->AddObject(m_pCharacter);
	m_pObjManager->AddObject(new Item(EPN_Pos(120, 500), ITEM_TYPE::FIRE));
	m_pObjManager->AddObject(new Item(EPN_Pos(220, 500), ITEM_TYPE::ICE));
	m_pObjManager->AddObject(new Item(EPN_Pos(320, 500), ITEM_TYPE::THUNDER));
	m_pObjManager->AddObject(new Item(EPN_Pos(420, 500), ITEM_TYPE::RECOVERY));
	m_pObjManager->AddObject(new Item(EPN_Pos(520, 500), ITEM_TYPE::POWER_UP));
	m_pObjManager->AddObject(new Item(EPN_Pos(620, 500), ITEM_TYPE::SPEED_UP));

	//되돌아가기 버튼
	m_BackButton.SetPos(EPN_Pos(10, 590));
	m_BackButton.SetTexture("BtnBack", BUTTON_STATE::ALL);
	m_BackButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_BackButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_BackButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_BackButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_BackButton.SetScaling(EPN_Scaling(1.f, 1.f));

	//도움말 버튼
	m_HelpButton.SetPos(EPN_Pos(725, 525));
	m_HelpButton.SetTexture("BtnHelp", BUTTON_STATE::ALL);
	m_HelpButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_HelpButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_HelpButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_HelpButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_HelpButton.SetScaling(EPN_Scaling(0.7f, 0.7f));

	m_GuideWindow.SetPos(EPN_Pos(201, 500));
	m_pEPN_SI->SetEffectVolume(50);
	MapManager::CreateInstance()->SetStage(15);
}

ViewGuide::~ViewGuide()
{
	m_pObjManager->ClearObject();
}

VIEW_STATE ViewGuide::Run()
{
	m_pEPN_TI->Blt("Map_1_4_1", 0, EPN_Pos(0, 600), EPN_Rect(-1, -1, -1, -1));

	m_BackButton.Run();
	m_HelpButton.Run();

	//총알 가능 상태 계산
	if (m_ShootEnbleFlag == false)
	{
		if (++m_BulletDelayCnt >= m_BulletDelay)
			m_ShootEnbleFlag = true;
	}
	//총알 발사
	if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_SPACE) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_SPACE) == EPN_INPUT_STATE::DOWNING)
	{
		if (m_ShootEnbleFlag == true)
		{
			m_pObjManager->AddObject(new Bullet());
			m_BulletDelayCnt = 0;
			m_ShootEnbleFlag = false;
		}
	}

	m_pEPN_TI->Blt("CharacterHpBarBg", 0, EPN_Pos(32, 19), EPN_Rect(-1, -1, -1, -1));
	m_pEPN_TI->Blt("CharacterHpBar", 0, EPN_Pos(32, 19), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f,
		EPN_Scaling(m_pCharacter->GetObjectInfo()->Hp / m_pCharacter->GetObjectInfo()->MaxHp, 1.f));
	m_pEPN_FI->BltText("OldFont15", 0, "HP ", EPN_Pos(67, 19), EPN_ARGB(255, 0, 0, 0));

	estring Power = m_pCharacter->GetObjectInfo()->Atk;
	estring Speed = m_pCharacter->GetObjectInfo()->Speed;
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("GUIDE "), EPN_Pos(200, 19), EPN_ARGB(255, 255, 255, 255));
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("POWER ") + Power.at(0) + Power.at(1) + Power.at(2) + Power.at(3), EPN_Pos(350, 19), EPN_ARGB(255, 255, 255, 255));
	m_pEPN_FI->BltText("OldFont15", 0, E_TEXT("SPEED ") + Speed.at(0) + Speed.at(1) + Speed.at(2), EPN_Pos(550, 19), EPN_ARGB(255, 255, 255, 255));

	m_pEPN_FI->BltText("OldFont15", 0, "FIRE", EPN_Pos(104, 525), EPN_ARGB(255, 0, 0, 0));
	m_pEPN_FI->BltText("OldFont15", 0, "ICE", EPN_Pos(209, 525), EPN_ARGB(255, 0, 0, 0));
	m_pEPN_FI->BltText("OldFont15", 0, "THUNDER", EPN_Pos(280, 525), EPN_ARGB(255, 0, 0, 0));
	m_pEPN_FI->BltText("OldFont15", 0, "HP", EPN_Pos(417, 525), EPN_ARGB(255, 0, 0, 0));
	m_pEPN_FI->BltText("OldFont15", 0, "POWER", EPN_Pos(500, 525), EPN_ARGB(255, 0, 0, 0));
	m_pEPN_FI->BltText("OldFont15", 0, "SPEED", EPN_Pos(600, 525), EPN_ARGB(255, 0, 0, 0));

	m_pObjManager->Run();

	if (m_BackButton.GetState() == BUTTON_STATE::POP)
	{
		m_pObjManager->ClearObject();
		return VIEW_STATE::MENU;
	}
	if (m_HelpButton.GetState() == BUTTON_STATE::POP)
		m_GuideWindow.SetActive(true);

	m_GuideWindow.Run();

	return VIEW_STATE::GUIDE;
}