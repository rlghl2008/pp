#ifndef __MONSTER_CLOSE_H__
#define __MONSTER_CLOSE_H__

#include "MonsterBase.h"

class MonsterClose : public MonsterBase
{
public:
	MonsterClose();
	MonsterClose(EPN_Pos Pos);
	~MonsterClose();

public:
	virtual void IsSensed(ObjectBase* pObject);
	virtual void IsCollided(ObjectBase* pObject);
	virtual bool Run();
};

#endif // !__MONSTER_CLOSE_H__