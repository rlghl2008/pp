#ifndef __VIEW_INGAME_H__
#define __VIEW_INGAME_H__

#include <stdlib.h>
#include "ViewBase.h"
#include "Bullet.h"
#include "Boss3.h"
#include "ScrollBar.h"
#include "PopUpWindow.h"
#include "MonsterBase.h"

class ViewInGame : public ViewBase
{
public:
	ViewInGame();
	~ViewInGame();

private:
	Character * m_pCharacter;		//캐릭터
	ObjectManager * m_pObjManager;	//오브젝트 매니저
	MapManager * m_pMap;
	Button m_SettingButton;
	PopUpWindow m_SettingWindow;
	PopUpWindow m_HelpWindow;

private:
	//int m_BulletDelay;				//총알 딜레이
	//int m_BulletDelayCnt;			//총알 딜레이 카운트
	//bool m_ShootEnbleFlag;			//총알 발사 가능 플래그

	bool m_IsCanOpenPortal;			//포탈 동작 플래그
	bool m_MapChangeFlag;			//맵 체인지 플래그

private:
	estring m_CurMapTexture;		//현재 맵 텍스쳐
	MapInfo * m_CurMapInfo;			//현재 맵 정보
	Boss3 * m_Boss3;				//보스3

	bool m_IsEnd;
	int m_EndingCnt;

private:
	void PortalRun();
	void CreateMonster();
	void DisplayUI();
	void AddMonster(MONSTER_TYPE MonsterType, EPN_Pos PosIndex);

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_INGAME_H__