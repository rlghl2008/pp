#include "PathFinder.h"

//#include "MapManager.h"
using namespace std;

PathFinder::PathFinder()
{
}

PathFinder::PathFinder(bool dpMapColisionData[MAP_SIZE_Y][MAP_SIZE_X])
{
	memcpy(m_dpMapColisionData, dpMapColisionData, MAP_SIZE_Y * MAP_SIZE_X);
}

PathFinder::~PathFinder()
{
	for (int i = 0; i < m_OpenedList.size(); i++)
		delete m_OpenedList[i];
	for (int i = 0; i < m_ClosedList.size(); i++)
		delete m_ClosedList[i];
}

bool PathFinder::PreCheckPath(EPN_Pos StartPos, EPN_Pos DestPos, vector<EPN_Pos>* PathList)
{
	int StepX = 1;
	if (StartPos.X > DestPos.X)
		StepX = -1;

	int stepY = 1;
	if (StartPos.Y > DestPos.Y)
		stepY = -1;

	//ㄱ자로 갈 수 있는지 체크한다.
	if (StartPos.X > DestPos.X)
	{
		for (int X = StartPos.X; X >= DestPos.X; X += StepX)
		{
			if (m_dpMapColisionData[(int)StartPos.Y][X] == true)
				goto FindNextSimplePath;
		}
	}
	else
	{
		for (int X = StartPos.X; X <= DestPos.X; X += StepX)
		{
			if (m_dpMapColisionData[(int)StartPos.Y][X] == true)
				goto FindNextSimplePath;
		}
	}

	if (StartPos.Y > DestPos.Y)
	{
		for (int Y = StartPos.Y; Y >= DestPos.Y; Y += stepY)
		{
			if (m_dpMapColisionData[Y][(int)DestPos.X] == true)
				goto FindNextSimplePath;
		}
	}
	else
	{
		for (int Y = StartPos.Y; Y <= DestPos.Y; Y += stepY)
		{
			if (m_dpMapColisionData[Y][(int)DestPos.X] == true)
				goto FindNextSimplePath;
		}
	}

	if (StartPos.X > DestPos.X)
	{
		for (int X = StartPos.X; X > DestPos.X; X += StepX)
			PathList->push_back(EPN_Pos(X, StartPos.Y));
	}
	else
	{
		for (int X = StartPos.X; X < DestPos.X; X += StepX)
			PathList->push_back(EPN_Pos(X, StartPos.Y));
	}

	if (StartPos.Y > DestPos.Y)
	{
		for (int Y = StartPos.Y; Y >= DestPos.Y; Y += stepY)
			PathList->push_back(EPN_Pos(DestPos.X, Y));
	}
	else
	{
		for (int Y = StartPos.Y; Y <= DestPos.Y; Y += stepY)
			PathList->push_back(EPN_Pos(DestPos.X, Y));
	}
	return true;

FindNextSimplePath:
	//ㄴ자로 갈 수 있는지 체크한다.
	if (StartPos.Y > DestPos.Y)
	{
		for (int Y = StartPos.Y; Y >= DestPos.Y; Y += stepY)
		{
			if (m_dpMapColisionData[Y][(int)StartPos.X] == true)
				return false;
		}
	}
	else
	{
		for (int Y = StartPos.Y; Y <= DestPos.Y; Y += stepY)
		{
			if (m_dpMapColisionData[Y][(int)StartPos.X] == true)
				return false;
		}
	}

	if (StartPos.X > DestPos.X)
	{
		for (int X = StartPos.X; X >= DestPos.X; ++X)
		{
			if (m_dpMapColisionData[(int)DestPos.Y][X] == true)
				return false;
		}
	}
	else
	{
		for (int X = StartPos.X; X <= DestPos.X; ++X)
		{
			if (m_dpMapColisionData[(int)DestPos.Y][X] == true)
				return false;
		}
	}

	if (StartPos.Y > DestPos.Y)
	{
		for (int Y = StartPos.Y; Y > DestPos.Y; Y += stepY)
			PathList->push_back(EPN_Pos(StartPos.X, Y));
	}
	else
	{
		for (int Y = StartPos.Y; Y < DestPos.Y; Y += stepY)
			PathList->push_back(EPN_Pos(StartPos.X, Y));
	}

	if (StartPos.X > DestPos.X)
	{
		for (int X = StartPos.X; X >= DestPos.X; X += StepX)
			PathList->push_back(EPN_Pos(X, DestPos.Y));
	}
	else
	{
		for (int X = StartPos.X; X <= DestPos.X; X += StepX)
			PathList->push_back(EPN_Pos(X, DestPos.Y));
	}

	return true;
}

bool PathFinder::CheckValidNode(EPN_Pos Pos)
{
	//Edit : 지금은 충돌만 체크하지만 나중에 맵 밖인지도 체크하는 처리 추가하자
	return m_dpMapColisionData[(int)(Pos.Y)][(int)(Pos.X)] == false;
}
//자식(인접) 노드 유효성 체크
bool PathFinder::CheckChildNode(EPN_Pos CurPos, PATH_DIRECTION Direction, EPN_Pos* ChildNodePos)
{
	switch (Direction)
	{
	case PATH_DIRECTION::UP:
		CurPos.Y += 1;
		break;
	case PATH_DIRECTION::RIGHT:
		CurPos.X += 1;
		break;
	case PATH_DIRECTION::DOWN:
		CurPos.Y -= 1;
		break;
	case PATH_DIRECTION::LEFT:
		CurPos.X -= 1;
		break;
	}
	*ChildNodePos = CurPos;

	return CheckValidNode(CurPos);
}
bool PathFinder::InsertOpenList(PathNode* pNode)
{
	//닫힌 목록에 먼저 동일한 노드가 있는지 검색
	for (int i = 0; i < m_ClosedList.size(); ++i)
	{
		//동일한 좌표의 노드가 있다면 따로 처리할 필요X
		if (pNode->NodePos == m_ClosedList[i]->NodePos)
		{
			return false;
		}
	}

	//열린 목록에 먼저 동일한 노드가 있는지 검색
	for (int i = 0; i < m_OpenedList.size(); ++i)
	{
		//동일한 좌표의 노드가 있다면
		if (pNode->NodePos == m_OpenedList[i]->NodePos)
		{
			//현재 G코스트와 비교해서 더 낮다면 기존의 부모와 G코스트를 현재것으로 교체
			if (pNode->CostG < m_OpenedList[i]->CostG)
			{
				m_OpenedList[i]->CostG = pNode->CostG;
				m_OpenedList[i]->CostF = m_OpenedList[i]->CostG + m_OpenedList[i]->CostH;
				m_OpenedList[i]->pParentNode = pNode->pParentNode;
			}
			return false;
		}
	}

	//닫힌 목록과 열린 목록에서 동일한 좌표를 가진 노드가 없으면 열린 목록에 푸쉬
	m_OpenedList.push_back(pNode);

	return true;
}

PathNode* PathFinder::FindPath(PathNode* pParentNode, EPN_Pos DestPos)
{
	++m_CallStackDepth;

	//길을 찾았을 경우
	if (pParentNode->NodePos == DestPos)
	{
		--m_CallStackDepth;
		return pParentNode;
	}

	//각 방향대로 길 검사
	for (int direction = PATH_DIRECTION::UP; direction < PATH_DIRECTION::DIRECTION_MAX; ++direction)
	{
		EPN_Pos ChildNodePos;
		if (CheckChildNode(pParentNode->NodePos, (PATH_DIRECTION)direction, &ChildNodePos) == true)
		{
			//4방향 이기때문에 항상 CostG(현재 위치에서 다음위치까지의 거리)의 값은 항상 + 1
			PathNode* pChildNode = new PathNode(ChildNodePos, (pParentNode->CostG + 1), DestPos, pParentNode);
			InsertOpenList(pChildNode);
		}
	}
	//열린 목록안에 노드들 중 가장 총 비용(F)이 작은 노드를 찾는다.
	if (m_OpenedList.size() > 0)
	{
		PathNode* pLeastCostNode = m_OpenedList[0];

		int leastCostNodeIndex = 0;
		for (int i = 0; i < m_OpenedList.size(); ++i)
		{
			if (pLeastCostNode->CostF >= m_OpenedList[i]->CostF)
			{
				pLeastCostNode = m_OpenedList[i];
				leastCostNodeIndex = i;
			}
		}

		//열린 목록에서 가장 작은노드를 찾았으니 열린목록에서 제외한다.
		m_OpenedList.erase(m_OpenedList.begin() + leastCostNodeIndex);
		//그리고 이 노드는 이제 경로로써 확정이 되었으니 닫힌 목록에 추가한다.
		m_ClosedList.push_back(pLeastCostNode);

		//그 노드 기반으로 다시 시작
		return FindPath(pLeastCostNode, DestPos);
	}

	--m_CallStackDepth;
	return pParentNode;
}

bool PathFinder::GetPath(EPN_Pos StartPos, EPN_Pos DestPos, vector<EPN_Pos>* FoundPath)
{
	FoundPath->clear();

	//끝점이 갈 수 있는 곳인지 체크
	if (CheckValidNode(DestPos) == false)
		return false;

	//시작점과 끝점이 같은지 체크
	if (StartPos == DestPos)
		return true;

	//굳이 복잡한 길찾기를 하지 않아도 되는경우인지 체크 (일자 또는 ㄱ자)
	if (PreCheckPath(StartPos, DestPos, FoundPath) == true)
		return true;

	//초기화
	if (m_OpenedList.size() != 0)
		m_OpenedList.clear();
	if (m_ClosedList.size() != 0)
		m_ClosedList.clear();

	//시작 지점 노드 (시작 지점, 
	PathNode startNode = PathNode(StartPos, 0, DestPos, nullptr);
	PathNode* FoundPathNode = FindPath(&startNode, DestPos);
	if (FoundPathNode == nullptr)
		return false;

	while (FoundPathNode != nullptr)
	{
		//길이 차례대로 있기때문에 첫번째 경로의 뒤에 계속 삽입
		FoundPath->insert(FoundPath->begin(), (FoundPathNode->NodePos));
		FoundPathNode = FoundPathNode->pParentNode;
	}

	//FoundPathNode 경로에 첫번째 출발지도 포함되어있기때문에 1번째를 삭제
	if (FoundPath->size() > 0)
		FoundPath->erase(FoundPath->begin());

	return true;
}

void PathFinder::SetColisionData(bool dpMapColisionData[MAP_SIZE_Y][MAP_SIZE_X])
{
	for (int y = 0; y < MAP_SIZE_Y; y++)
	{
		for (int x = 0; x < MAP_SIZE_X; x++)
		{
			m_dpMapColisionData[MAP_SIZE_Y - y - 1][x] = dpMapColisionData[y][x];
		}
	}
}
