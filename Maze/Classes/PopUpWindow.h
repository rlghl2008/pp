#ifndef __GUIDE_POP_UP_WINDOW_H__
#define __GUIDE_POP_UP_WINDOW_H__

#include "Button.h"
#include "ScrollBar.h"
#include "ViewBase.h"

class PopUpWindow
{
public:
	PopUpWindow();
	~PopUpWindow();

private:
	EPN_DeviceInterface* m_pEPN_DI;
	EPN_TextureInterface * m_pEPN_TI;
	EPN_FontInterface *	m_pEPN_FI;
	EPN_InputEvent * m_pEvent;

private:
	Button m_CloseButton;
	Button m_ReturnButton;
	Button m_HelpButton;
	ScrollBar m_BgmScrollBar;
	ScrollBar m_EffectScrollBar;
	EPN_Pos m_Pos;
	VIEW_STATE m_ViewState;

private:
	bool m_IsActive;
	bool m_IsGuide;		//true = 가이드창 false = 설정창
	bool m_SettingGuideFlag;

public:
	void SetActive(bool IsActive);
	void SetPopUpType(bool IsGuide);
	void SetPos(EPN_Pos Pos);
	BUTTON_STATE GetReturnButtonState();
	BUTTON_STATE GetHelpButtonState();

public:
	bool Run();
};

#endif // !__GUIDE_POP_UP_WINDOW_H__