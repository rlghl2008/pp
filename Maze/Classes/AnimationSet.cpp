#include "AnimationSet.h"

AnimationSet::AnimationSet() : m_CurAnimIndex(-1)
{
}

AnimationSet :: ~AnimationSet()
{
	for (int Cnt = 0, RemovedCnt = 0; RemovedCnt < m_mpAnimationInfo.GetAddDataCount(); ++Cnt)
	{
		if (m_mpAnimationInfo.IsData(Cnt) == true)
		{
			delete m_mpAnimationInfo.GetData(Cnt);
			m_mpAnimationInfo.EraseData(Cnt);
			++RemovedCnt;
		}
	}
}

int AnimationSet::AddAnimationData(AnimationInfo *_pAnimationInfo)
{
	return m_mpAnimationInfo.AddData(_pAnimationInfo);
}

int AnimationSet::GetAnimationDataCount()
{
	return m_mpAnimationInfo.GetAddDataCount();
}

int AnimationSet::GetCurrentAnim()
{
	return m_CurAnimIndex;
}

BOOL AnimationSet::SetCurrentAni(INT32 Index)
{
	if (Index < 0 || Index > m_mpAnimationInfo.GetAddDataCount() - 1)
		return false;

	//맨 처음 설정 하는것이 아니라면
	if (m_CurAnimIndex != -1)
	{
		AnimationInfo *pAnimInfo = m_mpAnimationInfo.GetData(m_CurAnimIndex);
		//설정할 애니메이션이 현재 애니매이션과 다르고 동시에 
		//기존 애니메이션이 리셋플래그가 있다면 바꾸기전에 인덱스를 초기화 시킨다.
		if (m_CurAnimIndex != Index && pAnimInfo->m_ResetFlag)
		{
			pAnimInfo->m_Action.SetActionNum(pAnimInfo->m_StartIndex);
			pAnimInfo->m_Action.SetActionDelayCount(0);
		}
		if (pAnimInfo->m_CompletePlayFlag)
		{
			if (GetIsCurAnimationFinished())
				m_CurAnimIndex = Index;
		}
		else
			m_CurAnimIndex = Index;
	}
	else
		m_CurAnimIndex = Index;

	return TRUE;
}

AnimationInfo * AnimationSet::GetAnimationData(INT32 Index)
{
	if (Index < 0 || Index > m_mpAnimationInfo.GetAddDataCount() - 1)
		return NULL;
	return m_mpAnimationInfo.GetData(Index);
}

AnimationInfo * AnimationSet::GetCurAnimationData()
{
	return m_mpAnimationInfo.GetData(m_CurAnimIndex);
}

INT32 AnimationSet::GetCurAniTexture()
{
	return m_mpAnimationInfo.GetData(m_CurAnimIndex)->m_TextureIndex;
}

EPN_Rect AnimationSet::GetCurAniActionRect()
{
	EPN_ActionData* pActionData = m_mpAnimationInfo.GetData(m_CurAnimIndex)->m_Action.GetActionData(m_mpAnimationInfo.GetData(m_CurAnimIndex)->m_Action.GetActionNum());
	return pActionData->Rect;
}

BOOL AnimationSet::GetIsCurAnimationFinished()
{
	AnimationInfo *pAni = m_mpAnimationInfo.GetData(m_CurAnimIndex);


	INT32 DelayCount = pAni->m_Action.GetActionDelayCount();

	//액션(애니)의 인덱스가 마지막이면서 동시에 딜레이도 마지막일때면 애니메이션이 끝난것
	if (pAni->m_Action.GetActionNum() == pAni->m_EndIndex && DelayCount >= pAni->m_Delay)
		return TRUE;
	return false;
}

VOID AnimationSet::Run()
{
	AnimationInfo *pAni = m_mpAnimationInfo.GetData(m_CurAnimIndex);

	//애니메이션이 끝난 경우
	if (GetIsCurAnimationFinished() == true && pAni->m_RepeatFlag == false)
	{
		return;
	}
	else
	{
		int EndIndex = pAni->m_EndIndex;
		if (pAni->m_EndIndex == -1)
			EndIndex = pAni->m_Action.GetActionDataCount();

		//현재 애니메이션의 딜레이, 시작 인덱스, 마지막인덱스까지 설정
		//Action 함수에서 설정값대로 루프를 돌려줌
		pAni->m_Action.Action(pAni->m_Delay, pAni->m_StartIndex, pAni->m_EndIndex);
	}
}