#include "EPN_SoundInterface.h"
#include <iostream>
using namespace std;
EPN_SoundInterface * EPN_SoundInterface::m_pEPN_SI= NULL_PTR;

EPN_SoundInterface *EPN_SoundInterface::CreateInstance()
{
	if (NULL_PTR == m_pEPN_SI)
		m_pEPN_SI = new EPN_SoundInterface;
	return m_pEPN_SI;
}
EPN_SoundInterface *EPN_SoundInterface::GetInstance()
{
	return m_pEPN_SI;
}
VOID EPN_SoundInterface::ReleaseInstance()
{
	if (NULL_PTR != m_pEPN_SI)
		delete m_pEPN_SI;
	m_pEPN_SI = NULL_PTR;
}

EPN_SoundInterface::EPN_SoundInterface()
{
	m_BgmVolume = 100;
	m_EffectVolume = 100;

	//사운드 시스템 생성 및 초기화
	System_Create(&m_SoundSystem);
	m_SoundSystem->init(SOUND_CHANNEL_MAX, FMOD_INIT_NORMAL, NULL_PTR);

	m_pCurBgm = NULL_PTR;
}


EPN_SoundInterface::~EPN_SoundInterface()
{
	//BGM 소멸
	if (NULL_PTR != m_pCurBgm)
	{
		m_pCurBgm->release();
		m_pCurBgm = NULL_PTR;
	}

	//Effect 소멸
	for (int Cnt = 0, SearchCnt = 0; SearchCnt < m_mpEffect.GetAddDataCount(); ++Cnt)
	{
		if (m_mpEffect.IsData(Cnt) == TRUE)
		{
			delete m_mpEffect.GetData(Cnt); //사운드 소멸
			m_mpEffect.EraseData(Cnt); //메모리 풀에서 제거
			m_StringIndexPool.DeleteData(Cnt); //SI 풀에서도 제거

			++SearchCnt;
		}
	}

	//사운드 시스템 소멸
	m_SoundSystem->close();
}
INT32 EPN_SoundInterface::GetBgmVolume()
{
	REAL32 Volume;
	m_pBgmChannel->getVolume((float*)&Volume);
	return (REAL32)Volume*100.f;
}

BOOL EPN_SoundInterface::SetBgmVolume(INT32 Volume)
{
	if (m_pBgmChannel == NULL_PTR || m_pCurBgm == NULL_PTR)
		return FALSE;

	if (Volume < 0)
		Volume = 0;
	else if (Volume > 100)
		Volume = 100;

	m_pBgmChannel->setVolume(Volume / 100.f);
	return TRUE;
}


BOOL EPN_SoundInterface::PlayBgm(std::string Path , BOOL RepeatFlag)
{
	Sound* pSound = NULL_PTR;
	//현재 BGM이 존재하거나 혹은 이전과 경로가 다를경우
	if (m_pCurBgm == NULL_PTR || m_pCurBgmPath != Path)
	{
		//현재 BGM이 존재안할 경우 (이전과 경로만 다를경우)
		if (NULL_PTR != m_pCurBgm)
		{
			m_pCurBgm->release();
			m_pCurBgm = NULL_PTR;
		}
		//사운드 생성
		FMOD_MODE Mode = RepeatFlag == TRUE ? (FMOD_LOOP_NORMAL | FMOD_HARDWARE) : (FMOD_LOOP_OFF | FMOD_HARDWARE);
		
		m_SoundSystem->createStream(Path.c_str(), Mode, NULL_PTR, &pSound);
		if (NULL_PTR == pSound)
			return FALSE;

		m_pCurBgm = pSound;
	}

	//재생 (빈 채널 자동으로 찾아서 재생)
	m_SoundSystem->playSound(FMOD_CHANNEL_FREE, pSound, FALSE, &m_pBgmChannel);
	m_pBgmChannel->setVolume((REAL32)m_BgmVolume / 100.f);


	return TRUE;
}

BOOL EPN_SoundInterface::PauseBgm()
{
	if (m_pBgmChannel == NULL_PTR || m_pCurBgm == NULL_PTR )
		return FALSE;

	BOOL IsPaused = FALSE;
	m_pBgmChannel->getPaused((bool*)&IsPaused);
	if (IsPaused == TRUE)
		return FALSE;

	m_pBgmChannel->setPaused(TRUE);
	return TRUE;
}

BOOL EPN_SoundInterface::ResumeBgm()
{
	if (m_pBgmChannel == NULL_PTR || m_pCurBgm == NULL_PTR)
		return FALSE;

	BOOL IsPaused = TRUE;
	m_pBgmChannel->getPaused((bool*)&IsPaused);
	if (IsPaused == FALSE)
		return FALSE;

	m_pBgmChannel->setPaused(FALSE);
	return TRUE;
}

BOOL EPN_SoundInterface::StopBgm()
{
	if (m_pBgmChannel == NULL_PTR || m_pCurBgm == NULL_PTR)
		return FALSE;
	BOOL Result = FALSE;
	m_pBgmChannel->stop();
}

BOOL EPN_SoundInterface::IsBgmPlaying()
{
	if (m_pBgmChannel == NULL_PTR || m_pCurBgm == NULL_PTR)
		return FALSE;
	BOOL Result = FALSE;
	m_pBgmChannel->isPlaying((bool*)&Result);
	return Result;
}

BOOL EPN_SoundInterface::PlayEffect(std::string Path)
{
	EPN_Sound *pEPN_Sound = new EPN_Sound;
	Sound* pFmodSound = NULL_PTR;
	
	//사운드 생성
	FMOD_RESULT CreateResult = m_SoundSystem->createSound(Path.c_str(), FMOD_LOOP_OFF | FMOD_HARDWARE, NULL_PTR, &pFmodSound);
	if (NULL_PTR == pFmodSound)
		return FALSE;

	//메모리 풀과 SI 풀에 저장
	pEPN_Sound->pSound = pFmodSound;
	INT32 Index = m_mpEffect.AddData(pEPN_Sound);
	m_StringIndexPool.InsertData(Path, Index);


	//Fmod 사운드가 없을 시 
	if (pFmodSound == NULL_PTR)
	{
		delete pEPN_Sound;
		return FALSE;
	}

	//사운드 재생
	Channel *pChannel = NULL_PTR;
	m_SoundSystem->playSound(FMOD_CHANNEL_FREE, pFmodSound, FALSE, &pChannel);
	pEPN_Sound->pChannel = pChannel;
	//cout << "생성 시 채널 : " << pEPN_Sound->pChannel << endl;
	
	//볼륨 조절
	pEPN_Sound->pChannel->setVolume((REAL32)m_EffectVolume / 100.f);

	return TRUE;
}

INT32 EPN_SoundInterface::GetEffectVolume()
{
	return m_EffectVolume;
}

VOID EPN_SoundInterface::SetEffectVolume(INT32 Volume)
{
	if (Volume < 0)
		Volume = 0;
	else if (Volume > 100)
		Volume = 100;
	m_EffectVolume = Volume;

	//현재 동작하는 모든 이펙트에게 설정
	for (int Cnt = 0, SearchCnt = 0; SearchCnt < m_mpEffect.GetAddDataCount(); ++SearchCnt)
	{
		if (m_mpEffect.IsData(Cnt) == TRUE)
		{
			m_mpEffect.GetData(Cnt)->pChannel->setVolume( (REAL32)m_EffectVolume / 100.f);
			++SearchCnt;
		}
	}
}

VOID EPN_SoundInterface::Run()
{
	//사운드 시스템 Update
	m_SoundSystem->update();

	//계속해서 메모리상에 들고 있으면 메모리를 많이 차지하므로 끝난 Effect를 해제한다.
	//이 처리 자체도 부담이 될거라 예상되므로 차후 매프레임이 아닌 일정 시간 마다 처리하도록 바꿀예정
	for (int Cnt = 0, SearchCnt = 0; SearchCnt < m_mpEffect.GetAddDataCount(); ++Cnt)
	{
		if (m_mpEffect.IsData(Cnt) == TRUE)
		{
			EPN_Sound *pSound = m_mpEffect.GetData(Cnt);
			
			//재생 중이 아니면 해제한다.
			BOOL IsPlaying = FALSE;
			pSound->pChannel->isPlaying((bool*)&IsPlaying);
			if (IsPlaying == FALSE)
			{
				//cout << "이펙트 사운드 소멸" << endl;
				//cout << pSound->pChannel << endl;
				delete pSound; //사운드 소멸
				m_mpEffect.EraseData(Cnt); //메모리 풀에서 제거
				m_StringIndexPool.DeleteData(Cnt); //SI 풀에서도 제거
			}
			++SearchCnt;
		}
	}
}