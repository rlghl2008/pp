#ifndef __ANIMATION_SET_H__
#define __ANIMATION_SET_H__

#include "../../Enpine/Client/Global/Global.h"
#include "../../Enpine/Client/Generic/Device/EPN_DeviceInterface.h"
#include "../../Enpine/Client/Generic/Texture/EPN_TextureInterface.h"
#include "../../Enpine/Client/Generic/InputEvent/EPN_InputEvent.h"
#include "../../Enpine/Client/Generic/Action/EPN_ActionInterface.h"
#include "../../Enpine/Client/Generic/Font/EPN_FontInterface.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::INPUTEVENT;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::ACTION;
#pragma endregion

struct AnimationInfo;

class AnimationSet
{
public:
	AnimationSet();
	~AnimationSet();

public:
	EPN_MemoryPool<AnimationInfo*> m_mpAnimationInfo;
	INT32 m_CurAnimIndex;

public:
	INT32 AddAnimationData(AnimationInfo *_pAnimationInfo);
	INT32 GetAnimationDataCount();

	INT32 GetCurrentAnim();
	BOOL SetCurrentAni(INT32 Index);

	AnimationInfo * GetAnimationData(INT32 Index);
	AnimationInfo * GetCurAnimationData();

	INT32 GetCurAniTexture();
	EPN_Rect GetCurAniActionRect();

	BOOL GetIsCurAnimationFinished();
	VOID Run();
};


struct AnimationInfo
{
	EPN_Action m_Action;
	INT32 m_TextureIndex;
	INT32 m_Delay;				
	BOOL m_CompletePlayFlag;	//한번 실행하면 반드시 중간에 바뀌지않고 끝까지 실행
	BOOL m_RepeatFlag;			//반복 재생 여부
	BOOL m_ResetFlag;			//애니메이션을 중간에 전환 할 시 스프라이트 인덱스를 초기화 할지 여부
	INT32 m_StartIndex;	
	INT32 m_EndIndex;

	AnimationInfo()
	{
		m_TextureIndex = -1;
		m_Delay = 0;
		m_CompletePlayFlag = FALSE;
		m_RepeatFlag = FALSE;
		m_ResetFlag = FALSE;
		m_StartIndex = 0;
		m_EndIndex = -1;
	}
	AnimationInfo(INT32 _TextureIndex, EPN_Pos _Scope, INT32 _Delay, BOOL _CompletePlayFlag, BOOL _RepeatFlag, BOOL _ResetFlag, INT32 StartIndex = 0, INT32 EndIndex = -1)
	{
		m_TextureIndex = _TextureIndex;
		m_Action.AutoAction(m_TextureIndex, _Scope.X, _Scope.Y);
		m_Delay = _Delay;
		m_CompletePlayFlag = _CompletePlayFlag;
		m_RepeatFlag = _RepeatFlag;
		m_ResetFlag = _ResetFlag;
		m_StartIndex = StartIndex;
		m_EndIndex = EndIndex;
	}
	AnimationInfo(estring _TextureName, EPN_Pos _Scope, INT32 _Delay, BOOL _CompletePlayFlag, BOOL _RepeatFlag, BOOL _ResetFlag, INT32 StartIndex = 0, INT32 EndIndex = -1)
	{
		EPN_TextureInterface *pEPN_TI = EPN_TextureInterface::CreateInstance();
		m_TextureIndex = pEPN_TI->GetTextureIndex(_TextureName);
		m_Action.AutoAction(m_TextureIndex, _Scope.X, _Scope.Y);
		m_Delay = _Delay;
		m_CompletePlayFlag = _CompletePlayFlag;
		m_RepeatFlag = _RepeatFlag;
		m_ResetFlag = _ResetFlag;
		m_StartIndex = StartIndex;
		m_EndIndex = EndIndex;
	}
	AnimationInfo(const AnimationInfo& _AnimationInfo)
	{
		m_TextureIndex = _AnimationInfo.m_TextureIndex;
		m_Action = _AnimationInfo.m_Action;
		m_Delay = _AnimationInfo.m_Delay;
		m_CompletePlayFlag = _AnimationInfo.m_CompletePlayFlag;
		m_RepeatFlag = _AnimationInfo.m_RepeatFlag;
		m_ResetFlag = _AnimationInfo.m_ResetFlag;
		m_StartIndex = _AnimationInfo.m_StartIndex;
		m_EndIndex = _AnimationInfo.m_EndIndex;
	}
};

#endif