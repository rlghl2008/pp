#include "MonsterClose.h"

MonsterClose::MonsterClose()
{
}

MonsterClose::MonsterClose(EPN_Pos Pos)
{
	m_MonsterType = MONSTER_TYPE::CLOSE;

	m_TextureName = "MonsterClose";
	m_ObjectInfo.Atk = 20;
	m_ObjectInfo.Hp = 50;
	m_ObjectInfo.Speed = 1.0f;
	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	m_ObjectInfo.CollisionRect = EPN_Rect(5, 5, 27, 43);
	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;
	m_OriginSpeed = m_ObjectInfo.Speed;

	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 12, 15));	//WalkUp
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 3));	//WalkDown
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 4, 7));	//SideWalk
	m_Animation.SetCurrentAni(ANIMATION_TYPE::WALK_DOWN);

	MonsterBase::MonsterBase();
}


MonsterClose::~MonsterClose()
{
}

void MonsterClose::IsSensed(ObjectBase * pObject)
{
}

void MonsterClose::IsCollided(ObjectBase * pObject)
{
	MonsterBase::IsCollided(pObject);
}

bool MonsterClose::Run()
{
	MonsterBase::Run();
	return false;
}
