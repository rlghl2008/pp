#ifndef __VIEW_GUIDE_H__
#define __VIEW_GUIDE_H__

#include "ViewBase.h"
#include "Bullet.h"
#include "Button.h"
#include "Item.h"
#include "PathFinder.h"
#include "PopUpWindow.h"

class ViewGuide : public ViewBase
{
public:
	ViewGuide();
	~ViewGuide();

private:
	ObjectManager * m_pObjManager;
	Character * m_pCharacter;
	Button m_BackButton;
	Button m_HelpButton;
	PopUpWindow m_GuideWindow;

private:
	int m_BulletDelay;		//�Ѿ� ������
	int m_BulletDelayCnt;	//�Ѿ� ������ ī��Ʈ
	bool m_ShootEnbleFlag;	//�Ѿ� �߻� ���� �÷���

private:
	bool m_IsItemDisappeared[(int)ITEM_TYPE::MAX];
	int m_ItemDisappearedTime[(int)ITEM_TYPE::MAX];

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_GUIDE_H__