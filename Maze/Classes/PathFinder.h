#ifndef __PATH_FINDER_H__
#define __PATH_FINDER_H__

#include <vector>
#include <map>
#include "PathNode.h"
#include "MapManager.h"

//struct CaculatedPathInfo
//{
//	EPN_Pos StartPos;
//	EPN_Pos DestPos;
//
//	CaculatedPathInfo(EPN_Pos StartPos, EPN_Pos DestPos)
//	{
//		this->StartPos = StartPos;
//		this->DestPos = DestPos;
//	}
//
//	bool operator<(const CaculatedPathInfo& o) const
//	{
//		if (StartPos == DestPos)
//			return false;
//		return true;
//	}
//};

class PathFinder
{
	enum PATH_DIRECTION
	{
		UP = 0,
		RIGHT,
		DOWN,
		LEFT,
		DIRECTION_MAX
	};

public:
	PathFinder();
	PathFinder(bool dpMapColisionData[MAP_SIZE_Y][MAP_SIZE_X]);
	~PathFinder();
private:
	std::vector<PathNode*> m_OpenedList;
	std::vector<PathNode*> m_ClosedList;
	/*std::map <CaculatedPathInfo, vector<EPN_Pos>> m_CaculatedPathMap;*/
	bool m_dpMapColisionData[MAP_SIZE_Y][MAP_SIZE_X];
	int m_CallStackDepth = 0;
private:
	bool PreCheckPath(EPN_Pos StartPos, EPN_Pos DestPos, std::vector<EPN_Pos>* PathList);
	bool CheckValidNode(EPN_Pos Pos);
	bool CheckChildNode(EPN_Pos CurPos, PATH_DIRECTION Direction, EPN_Pos* ChildNodePos);
	bool InsertOpenList(PathNode* pNode);
	PathNode* FindPath(PathNode* pParentNode, EPN_Pos DestPos);

public:
	bool GetPath(EPN_Pos StartPos, EPN_Pos DestPos, std::vector<EPN_Pos>* FoundPath);
	void SetColisionData(bool dpMapColisionData[MAP_SIZE_Y][MAP_SIZE_X]);
};
#endif