#include "ObjectBase.h"
#include "Bullet.h"

ObjectBase::ObjectBase() :
	m_MapChangeFlag(true)
{
	m_pEPN_DI = EPN_DeviceInterface::GetInstance();
	m_pEPN_TI = EPN_TextureInterface::GetInstance();
	m_pEPN_AI = EPN_ActionInterface::GetInstance();
	m_pEPN_FI = EPN_FontInterface::GetInstance();
	m_pEPN_SI = EPN_SoundInterface::GetInstance();
	m_pEvent = EPN_InputEvent::GetInstance();

	m_pMap = MapManager::CreateInstance();
	m_pCurMapInfo = m_pMap->GetCurMapInfo();
}

ObjectBase::~ObjectBase()
{
}

ObjectInfo* ObjectBase::GetObjectInfo()
{
	return &m_ObjectInfo;
}

bool ObjectBase::Run()
{
	m_pCurMapInfo = m_pMap->GetCurMapInfo();

	//int CIndexTopX = (m_ObjectInfo.Pos.X + m_ObjectInfo.FootRect.Top.X) / 32;
	//int CIndexTopY = (600 - (m_ObjectInfo.Pos.Y - 18 - m_ObjectInfo.FootRect.Top.Y)) / 32;
	//int CIndexBottomX = (m_ObjectInfo.Pos.X + 1 + m_ObjectInfo.FootRect.Bottom.X) / 32;
	//int CIndexBottomY = (600 - (m_ObjectInfo.Pos.Y - 19 - m_ObjectInfo.FootRect.Bottom.Y)) / 32;

	//오브젝트가 총알일 때,
	if (m_ObjectInfo.ObjectType == OBJECT_TYPE::BULLET && m_ObjectInfo.IsEnable == true)
	{
		/* 현재 오브젝트의 위치의 충돌배열 인덱스 */
		int CIndexTopX = (m_ObjectInfo.Pos.X + m_ObjectInfo.CollisionRect.Top.X) / 32;
		int CIndexTopY = (600 - m_ObjectInfo.Pos.Y + m_ObjectInfo.CollisionRect.Top.Y) / 32;
		int CIndexBottomX = (m_ObjectInfo.Pos.X + m_ObjectInfo.CollisionRect.Bottom.X) / 32;
		int CIndexBottomY = (600 - m_ObjectInfo.Pos.Y + m_ObjectInfo.CollisionRect.Bottom.Y) / 32;

		//왼쪽 위, 오른쪽 위, 왼쪽 밑, 오른쪽 밑
		if (m_pCurMapInfo->ColisionData[CIndexTopY][CIndexTopX] == true ||
			m_pCurMapInfo->ColisionData[CIndexTopY][CIndexBottomX] == true ||
			m_pCurMapInfo->ColisionData[CIndexBottomY][CIndexTopX] == true ||
			m_pCurMapInfo->ColisionData[CIndexBottomY][CIndexBottomX] == true)
		{
			//총알 타입에 따라 부딫혔을때 소리가 다름
			Bullet* CurBullet = (Bullet*)this;
			switch (CurBullet->GetBulletType())
			{
			case BULLET_TYPE::NORMAL:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackNormal.mp3");
				break;
			case BULLET_TYPE::FIRE:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackFire.mp3");
				break;
			case BULLET_TYPE::ICE:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackIce.mp3");
				break;
			case BULLET_TYPE::THUNDER:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackThunder.mp3");
				break;
			default:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackFire.mp3");
				break;
			}
			m_ObjectInfo.IsEnable = false;
		}
	}
	//캐릭터일 때
	else
	{
		/* 현재 오브젝트의 위치의 충돌배열 인덱스 */
		int CIndexTopX = (m_ObjectInfo.Pos.X + m_ObjectInfo.FootRect.Top.X) / 32;
		int CIndexTopY = (600 - (m_ObjectInfo.Pos.Y - 18 - m_ObjectInfo.FootRect.Top.Y)) / 32;
		int CIndexBottomX = (m_ObjectInfo.Pos.X + 1 + m_ObjectInfo.FootRect.Bottom.X) / 32;
		int CIndexBottomY = (600 - (m_ObjectInfo.Pos.Y - 19 - m_ObjectInfo.FootRect.Bottom.Y)) / 32;

		//왼쪽 위, 오른쪽 위, 왼쪽 밑, 오른쪽 밑
		if (m_pCurMapInfo->ColisionData[CIndexTopY][CIndexTopX] == true ||
			m_pCurMapInfo->ColisionData[CIndexTopY][CIndexBottomX] == true ||
			m_pCurMapInfo->ColisionData[CIndexBottomY][CIndexTopX] == true ||
			m_pCurMapInfo->ColisionData[CIndexBottomY][CIndexBottomX] == true)
		{
			EPN_Pos MovedPos = m_ObjectInfo.Pos - m_PrevPos;

			//왼쪽으로 움직였을 때
			if (MovedPos.X < 0)
				m_ObjectInfo.Pos = EPN_Pos((CIndexTopX + 1) * 32, m_ObjectInfo.Pos.Y);
			//오른쪽으로 움직였을 때
			else if (MovedPos.X > 0)
				m_ObjectInfo.Pos = EPN_Pos(CIndexTopX * 32, m_ObjectInfo.Pos.Y);
			//위쪽으로 움직였을 때
			else if (MovedPos.Y > 0)
				m_ObjectInfo.Pos = EPN_Pos(m_ObjectInfo.Pos.X, ((MAP_SIZE_Y - 1) - CIndexTopY) * 32 + 10);
			//밑쪽으로 움직였을 때
			else if (MovedPos.Y < 0)
				m_ObjectInfo.Pos = EPN_Pos(m_ObjectInfo.Pos.X, (MAP_SIZE_Y - CIndexTopY) * 32 + 10);

			return true;
		}
	}

	return false;
}