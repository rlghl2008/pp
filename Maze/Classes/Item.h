#ifndef __ITEM_H__
#define __ITEM_H__

#include "Character.h"
#include <stdlib.h>

enum class ITEM_TYPE //아이템 타입(종류)
{
	NONE = -1,
	RECOVERY,
	POWER_UP,
	SPEED_UP,
	FIRE,
	ICE,
	THUNDER,
	MAX
};

class Item : public ObjectBase
{
public:
	Item(EPN_Pos Pos, ITEM_TYPE ItemType);
	Item(EPN_Pos Pos);
	~Item();

private:
	ITEM_TYPE m_ItemType;	//아이템 타입
	estring m_TextureName;	//아이템 텍스쳐 이름

public:
	ITEM_TYPE GetItemType();

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual void IsSensed(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__ITEM_H__
