#ifndef __BOSS3_H__
#define __BOSS3_H__

#include "MonsterBase.h"

class Boss3 : public MonsterBase
{
private:
	enum BOSS_PATTERN
	{
		PATTERN_NORMAL,
		PATTERN_ATTACK1,
		PATTERN_ATTACK2,
		PATTERN_ATTACK3,
		PATTERN_ATTACK4,
		PATTERN_ATTACK5
	};
public:
	Boss3();
	~Boss3();

private:
	Character * m_pCharacter;
	ObjectManager *m_pObjectMgr;

private:
	BOSS_PATTERN m_Pattern;
	int m_PatternTimeCnt;
	int m_ShootDelayCnt;
	float m_Alpha;
	int m_ShootAngle;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__BOSS3_H__