#ifndef __BOSS1_H__
#define __BOSS1_H__

#include "MonsterBase.h"

class Boss1 : public MonsterBase
{
private:
	enum BOSS_PATTERN
	{
		PATTERN_NORMAL,
		PATTERN_ATTACK1,
		PATTERN_ATTACK2,
		PATTERN_ATTACK3
	};
public:
	Boss1();
	~Boss1();

private:
	Character * m_pCharacter;
	ObjectManager *m_pObjectMgr;

private:
	BOSS_PATTERN m_Pattern;
	int m_PatternTimeCnt;
	float m_Alpha;
	int m_ShootTimeCnt;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__BOSS1_H__