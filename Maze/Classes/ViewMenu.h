#ifndef __VIEW_MENU_H__
#define __VIEW_MENU_H__

#include "ViewBase.h"
#include "Button.h"
#include "Box2D//Box2D.h"

class ViewMenu : public ViewBase
{
#define PTM_RATIO 60

	enum BUTTON_TYPE
	{
		NONE = -1,
		START = 0,
		GUIDE,
		STORY,
		EXIT,
		MAX
	};

	enum TITLE_SPELL
	{
		S_NONE = -1,
		M = 0,
		A,
		Z,
		E,
		S_MAX
	};

public:
	ViewMenu();
	~ViewMenu();

private:
	b2World* m_B2World;
	b2Body* m_TitleBody[4];
	EPN_Pos m_TitlePos[4];

	Button m_Button[BUTTON_TYPE::MAX];
	bool m_IsBtnActived;

private:
	void BounceTitle();	//타이틀 떨어지는 효과
	void InitB2World();	//b2 초기화
	void SetTitleBody(TITLE_SPELL Spell, EPN_Pos Pos, EPN_Pos BoxSize);
	void SoundButton();	//버튼 소리

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_MENU_H__