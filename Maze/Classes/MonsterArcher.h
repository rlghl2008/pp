#ifndef __MONSTER_ARCHER_H__
#define __MONSTER_ARCHER_H__

#include "MonsterBase.h"

class MonsterArcher : public MonsterBase
{
public:
	MonsterArcher();
	MonsterArcher(EPN_Pos Pos);

	~MonsterArcher();

private:
	ObjectManager* m_pObjectMgr;

private:
	int m_ShootDelay;
	int m_ShootDelayCount;
	bool m_ShootFlag;

public:
	virtual void IsSensed(ObjectBase* pObject);
	virtual void IsCollided(ObjectBase* pObject);
	virtual bool Run();
};

#endif// !__MONSTER_ARCHER_H__