#ifndef __ENEMY_BULLET_H__
#define __ENEMY_BULLET_H__

#include "ObjectManager.h"

enum class ENEMYBULLET_TYPE
{
	NONE = -1,
	BOSS,
	ARCHER,
	FAKEBULLET,
	MAX
};

class EnemyBullet : public ObjectBase
{
public:
	EnemyBullet(ObjectBase *pOwner , int Angle , ENEMYBULLET_TYPE Type);
	~EnemyBullet();

private:
	EPN_Pos m_NomalizedPos;
	int m_FakeTimeCnt;
	ENEMYBULLET_TYPE m_Type;
	int m_Angle;
	int m_BulletDieCnt;					//총알 없어지는 시간 카운트(폭발)
	int m_BulletDieTime;				//총알 없어지는 시간

	bool m_AniSetFlag;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual void IsSensed(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__ENEMY_BULLET_H__