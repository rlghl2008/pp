#include "ObjectManager.h"

ObjectManager * ObjectManager::m_pObjectManager = NULL;

ObjectManager* ObjectManager::CreateInstance()
{
	if (!m_pObjectManager)
		m_pObjectManager = new ObjectManager();
	return m_pObjectManager;
}

ObjectManager::ObjectManager()
{
}

Character* ObjectManager::GetCharacter()
{
	//캐릭터는 무조건 맨처음 넣기때문에 0번째 리턴하면 캐릭터 타입이 나옴
	return (Character*)m_mpObject.GetData(0);
}

ObjectManager::~ObjectManager()
{
	for (int Cnt = 0, Index = 0; Index < m_mpObject.GetAddDataCount(); ++Cnt)
	{
		if (m_mpObject.IsData(Cnt) == true)
		{
			delete m_mpObject.GetData(Cnt);
			Index++;
		}
	}
}

int ObjectManager::AddObject(ObjectBase* pObject)
{
	return m_mpObject.AddData(pObject);
}

bool ObjectManager::RemoveObject(int Index)
{
	ObjectBase *pObject = m_mpObject.GetData(Index);
	if (!pObject)
		return false;
	delete m_mpObject.GetData(Index);
	return m_mpObject.EraseData(Index);
}

int ObjectManager::SearchObject(ObjectBase * pObject)
{
	for (int Cnt = 0, SearchCnt = 0; SearchCnt < m_mpObject.GetAddDataCount(); ++Cnt)
	{
		if (m_mpObject.IsData(Cnt) == true)
		{
			++SearchCnt;
			if (m_mpObject.GetData(Cnt) == pObject)
				return Cnt;
		}
	}
	return -1;
}

ObjectBase * ObjectManager::SearchObject(int Index)
{
	return m_mpObject.GetData(Index);
}

void ObjectManager::ClearObject()
{
	for (int Cnt = 0; Cnt < m_mpObject.GetAddDataCount(); ++Cnt)
	{
		if (m_mpObject.IsData(Cnt) == true)
		{
			delete m_mpObject.GetData(Cnt);
			m_mpObject.EraseData(Cnt, true);
			--Cnt;
		}
	}
}
int ObjectManager::GetObjectNum()
{
	return m_mpObject.GetAddDataCount();
}

bool ObjectManager::IsExistMonster()
{
	for (int Cnt = 0, Cnt2 = 0; Cnt2 < m_mpObject.GetAddDataCount(); ++Cnt)
	{
		if (m_mpObject.IsData(Cnt))
		{
			if (m_mpObject.GetData(Cnt)->GetObjectInfo()->ObjectType == OBJECT_TYPE::MONSTER)
				return true;
			Cnt2++;
		}
	}
	return false;
}

void ObjectManager::ClearItem()
{
	for (int Cnt = 1; Cnt < m_mpObject.GetAddDataCount(); ++Cnt)
	{
		if (m_mpObject.IsData(Cnt) == true)
		{
			delete m_mpObject.GetData(Cnt);
			m_mpObject.EraseData(Cnt, true);
			--Cnt;
		}
	}
}

void ObjectManager::Run()
{
	/* 오브젝트 충돌처리 */
	for (int i = 0; i < m_mpObject.GetAddDataCount(); ++i)
	{
		for (int k = i + 1; k < m_mpObject.GetAddDataCount(); ++k)
		{
			ObjectBase * CurObject = m_mpObject.GetData(i);					//충돌 주체
			ObjectBase * CurTargetObject = m_mpObject.GetData(k);			//충돌 피체
			ObjectInfo * CurObjInfo = CurObject->GetObjectInfo();			//충돌 주체 정보
			ObjectInfo * CurTarObjInfo = CurTargetObject->GetObjectInfo();	//충돌 피체 정보

			//충돌 주체 충돌 렉트
			EPN_Rect CollisionRect = EPN_Rect
			(
				CurObjInfo->Pos.X + CurObjInfo->CollisionRect.Top.X,
				CurObjInfo->Pos.Y - CurObjInfo->CollisionRect.Top.Y,
				CurObjInfo->Pos.X + CurObjInfo->CollisionRect.Bottom.X,
				CurObjInfo->Pos.Y - CurObjInfo->CollisionRect.Bottom.Y
			);
			//충돌 주체 감지 렉트
			EPN_Rect SensorRect = EPN_Rect
			(
				CurObjInfo->Pos.X + CurObjInfo->SensorRect.Top.X,
				CurObjInfo->Pos.Y - CurObjInfo->SensorRect.Top.Y,
				CurObjInfo->Pos.X + CurObjInfo->SensorRect.Bottom.X,
				CurObjInfo->Pos.Y - CurObjInfo->SensorRect.Bottom.Y
			);
			//충돌 피체 충돌 렉트
			EPN_Rect TargetCollisionRect = EPN_Rect
			(
				CurTarObjInfo->Pos.X + CurTarObjInfo->CollisionRect.Top.X,
				CurTarObjInfo->Pos.Y - CurTarObjInfo->CollisionRect.Top.Y,
				CurTarObjInfo->Pos.X + CurTarObjInfo->CollisionRect.Bottom.X,
				CurTarObjInfo->Pos.Y - CurTarObjInfo->CollisionRect.Bottom.Y
			);
			EPN_Rect TargetSensorRect = EPN_Rect
			(
				CurTarObjInfo->Pos.X + CurTarObjInfo->SensorRect.Top.X,
				CurTarObjInfo->Pos.Y - CurTarObjInfo->SensorRect.Top.Y,
				CurTarObjInfo->Pos.X + CurTarObjInfo->SensorRect.Bottom.X,
				CurTarObjInfo->Pos.Y - CurTarObjInfo->SensorRect.Bottom.Y
			);

			//팀 플래그가 서로 다를경우 충돌 체크
			if (CurObjInfo->TeamFlag != CurTarObjInfo->TeamFlag)
			{
				//충돌 체크
				if (EPN_CheckCollision(CollisionRect, TargetCollisionRect) && CurObjInfo->IsEnable && CurTarObjInfo->IsEnable)
				{
					CurObject->IsCollided(CurTargetObject);
					CurTargetObject->IsCollided(CurObject);
				}
			}
			if (EPN_CheckCollision(SensorRect, TargetCollisionRect) && CurObjInfo->IsEnable && CurTarObjInfo->IsEnable)
			{
				CurObject->IsSensed(CurTargetObject);
				CurTargetObject->IsSensed(CurObject);
			}
			if (EPN_CheckCollision(TargetSensorRect, CollisionRect) && CurObjInfo->IsEnable && CurTarObjInfo->IsEnable)
			{
				CurObject->IsSensed(CurTargetObject);
				CurTargetObject->IsSensed(CurObject);
			}
		}
	}

	//동작 처리
	for (int i = 0; i < m_mpObject.GetAddDataCount(); ++i)
	{
		//다이플래그가 트루면 삭제
		if (m_mpObject.GetData(i)->GetObjectInfo()->IsDied)
		{
			m_mpObject.EraseData(i, true);
			--i;
			continue;
		}
		m_mpObject.GetData(i)->Run();
	}
}