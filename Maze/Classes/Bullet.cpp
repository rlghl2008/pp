#include "Bullet.h"

Bullet::Bullet() :
	m_BulletDieTime(8),
	m_BulletDieCnt(0),
	m_SetBombPosFlag(true)
{
	m_pEPN_SI->PlayEffect("Sound/InGame/Shot.mp3");

	//기본 정보 설정
	m_ObjectInfo.TeamFlag = TEAM_FLAG::PLAYER;
	m_ObjectInfo.ObjectType = OBJECT_TYPE::BULLET;

	m_pCharacter = ObjectManager::CreateInstance()->GetCharacter();		//매니저에서 캐릭터를 반환

	EPN_Pos CharacterPos = m_pCharacter->GetObjectInfo()->Pos;			//총알의 속성, 방향등 캐릭터에서 정보를 받아와서 설정
	EPN_Rect BulletColRect[MAX];										//각 총알 방향 충돌렉트
	EPN_Pos BulletBombSize;												//총알 폭발 애니매이션 그림 크기
	int BulletBombNum;													//총알 폭발 애니매이션 개수

	CHARACTER_TYPE CharacterType = m_pCharacter->GetCharacterType();	//현재 캐릭터 속성마다 공격력, 스피드, 텍스쳐가 바뀜
	switch (CharacterType)
	{
	case CHARACTER_TYPE::NORMAL:
		m_TextureName = "BulletNormal";
		m_BulletSize = EPN_Pos(7, 17);
		m_BulletType = BULLET_TYPE::NORMAL;
		m_ObjectInfo.Atk = 10;
		m_ObjectInfo.Speed = 3.5f;
		BulletColRect[UP] = EPN_Rect(2, 2, 12, 12);
		BulletColRect[DOWN] = EPN_Rect(2, 20, 12, 30);
		BulletColRect[LEFT] = EPN_Rect(-5, 12, 5, 22);
		BulletColRect[RIGHT] = EPN_Rect(10, 12, 20, 22);
		m_ObjectInfo.Pos = EPN_Pos(CharacterPos.X + 9, CharacterPos.Y - 14);
		BulletBombSize = EPN_Pos(32, 32);
		BulletBombNum = 4;
		break;
	case CHARACTER_TYPE::FIRE:
		m_TextureName = "BulletFire";
		m_BulletSize = EPN_Pos(8, 15);
		m_BulletType = BULLET_TYPE::FIRE;
		m_ObjectInfo.Atk = 10;
		m_ObjectInfo.Speed = 3.5f;
		BulletColRect[UP] = EPN_Rect(3, 2, 13, 12);
		BulletColRect[DOWN] = EPN_Rect(3, 16, 13, 26);
		BulletColRect[LEFT] = EPN_Rect(-5, 9, 5, 19);
		BulletColRect[RIGHT] = EPN_Rect(11, 10, 21, 20);
		m_ObjectInfo.Pos = EPN_Pos(CharacterPos.X + 8, CharacterPos.Y - 14);
		BulletBombSize = EPN_Pos(64, 64);
		BulletBombNum = 16;
		m_BulletDieTime = 16;
		break;
	case CHARACTER_TYPE::ICE:
		m_TextureName = "BulletIce";
		m_BulletSize = EPN_Pos(8, 17);
		m_BulletType = BULLET_TYPE::ICE;
		m_ObjectInfo.Atk = 10;
		m_ObjectInfo.Speed = 3.5f;
		BulletColRect[UP] = EPN_Rect(2, 2, 12, 22);
		BulletColRect[DOWN] = EPN_Rect(2, 10, 12, 30);
		BulletColRect[LEFT] = EPN_Rect(-5, 12, 15, 22);
		BulletColRect[RIGHT] = EPN_Rect(0, 12, 20, 22);
		m_ObjectInfo.Pos = EPN_Pos(CharacterPos.X + 8, CharacterPos.Y - 8);
		BulletBombSize = EPN_Pos(32, 32);
		BulletBombNum = 4;
		break;
	case CHARACTER_TYPE::THUNDER:
		m_TextureName = "BulletThunder";
		m_BulletSize = EPN_Pos(15, 16);
		m_BulletType = BULLET_TYPE::THUNDER;
		m_ObjectInfo.Atk = 15;
		m_ObjectInfo.Speed = 4.5f;
		BulletColRect[UP] = EPN_Rect(9, 11, 21, 23);
		BulletColRect[DOWN] = EPN_Rect(9, 10, 21, 22);
		BulletColRect[LEFT] = EPN_Rect(8, 10, 20, 22);
		BulletColRect[RIGHT] = EPN_Rect(9, 11, 21, 23);
		m_ObjectInfo.Pos = EPN_Pos(CharacterPos.X + 1, CharacterPos.Y - 10);
		BulletBombSize = EPN_Pos(32, 32);
		BulletBombNum = 4;
		break;
	}

	//캐릭터 방향에따라 각도, 충돌 렉트, 이동위치가 달라짐
	m_Direction = m_pCharacter->GetDirection();
	switch (m_Direction)
	{
	case CHARACTER_DIRECTION::UP:
		m_MovingPos = EPN_Pos(0, m_ObjectInfo.Speed);
		m_ObjectInfo.CollisionRect = BulletColRect[UP];
		m_Angle = 0.f;
		break;
	case CHARACTER_DIRECTION::DOWN:
		m_MovingPos = EPN_Pos(0, -m_ObjectInfo.Speed);
		m_ObjectInfo.CollisionRect = BulletColRect[DOWN];
		m_Angle = 180.f;
		break;
	case CHARACTER_DIRECTION::LEFT:
		m_MovingPos = EPN_Pos(-m_ObjectInfo.Speed, 0);
		m_ObjectInfo.CollisionRect = BulletColRect[LEFT];
		m_Angle = 270.f;
		break;
	case CHARACTER_DIRECTION::RIGHT:
		m_MovingPos = EPN_Pos(m_ObjectInfo.Speed, 0);
		m_ObjectInfo.CollisionRect = BulletColRect[RIGHT];
		m_Angle = 90.f;
		break;
	}
	m_ObjectInfo.Scaling = EPN_Scaling(2.f, 2.f);

	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, m_BulletSize, 5, false, true, false, 0, 2));
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName + "Bomb", BulletBombSize, 2, true, false, false, 0, BulletBombNum - 1));
	m_Animation.SetCurrentAni(ANIMATION_TYPE::NORMAL);
}

Bullet::~Bullet()
{
}

BULLET_TYPE Bullet::GetBulletType()
{
	return m_BulletType;
}

void Bullet::IsCollided(ObjectBase * pObject)
{
}

void Bullet::IsSensed(ObjectBase * pObject)
{
	int a = 10;

}

bool Bullet::Run()
{
	m_Animation.Run();

	//총알의 작동 플래그가 false가되면 삭제모션 시작
	if (!m_ObjectInfo.IsEnable)
	{
		if(m_BulletType == BULLET_TYPE::FIRE)
			m_ObjectInfo.Scaling = EPN_Scaling(0.5f, 0.5f);
		else
			m_ObjectInfo.Scaling = EPN_Scaling(1.f, 1.f);
		m_Animation.SetCurrentAni(ANIMATION_TYPE::BOMB);
		if (m_SetBombPosFlag)
		{
			switch (m_BulletType)
			{
			case BULLET_TYPE::NORMAL:
				m_ObjectInfo.Pos = EPN_Pos(m_ObjectInfo.Pos.X - 8, m_ObjectInfo.Pos.Y);
				break;
			case BULLET_TYPE::FIRE:
				m_ObjectInfo.Pos = EPN_Pos(m_ObjectInfo.Pos.X - 8, m_ObjectInfo.Pos.Y + 2);
				break;
			case BULLET_TYPE::ICE:
				m_ObjectInfo.Pos = EPN_Pos(m_ObjectInfo.Pos.X - 8, m_ObjectInfo.Pos.Y + 1);
				break;
			case BULLET_TYPE::THUNDER:
				break;
			}
			m_SetBombPosFlag = false;
		}
		//일정 시간이 지나면 총알 삭제
		if (++m_BulletDieCnt >= m_BulletDieTime)
		{
			m_BulletDieCnt = 0;
			m_ObjectInfo.IsDied = true;
		}
	}
	else
	{
		m_ObjectInfo.Pos += m_MovingPos;
		ObjectBase::Run();
	}
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(), EPN_ARGB(255, 255, 255, 255), m_Angle, m_ObjectInfo.Scaling);

	return true;
}