#include "ViewBase.h"

ViewBase::ViewBase()
{
	m_pEPN_DI = EPN_DeviceInterface::GetInstance();
	m_pEPN_TI = EPN_TextureInterface::GetInstance();
	m_pEPN_AI = EPN_ActionInterface::GetInstance();
	m_pEPN_FI = EPN_FontInterface::GetInstance();
	m_pEPN_SI = EPN_SoundInterface::GetInstance();
	m_pEvent  = EPN_InputEvent::GetInstance();
}

ViewBase::~ViewBase()
{
}