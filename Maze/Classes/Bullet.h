#ifndef __BULLET_H__
#define __BULLET_H__

#include "ObjectManager.h"

enum class BULLET_TYPE			//총알 타입
{
	NONE = -1,
	NORMAL,
	FIRE,
	ICE,
	THUNDER,
	MAX
};

class Bullet : public ObjectBase
{
	enum ANIMATION_TYPE
	{
		ANIMATION_TYPE_NONE = -1,
		NORMAL,
		BOMB,
		ANIMATION_TYPE_MAX
	};

	enum BULLET_DIRECTION
	{
		NONE = -1,
		UP,
		DOWN,
		LEFT,
		RIGHT,
		MAX
	};

public:
	Bullet();
	~Bullet();

private:
	Character * m_pCharacter;			//캐릭터

private:
	AnimationSet m_Animation;			//애니매이션
	BULLET_TYPE m_BulletType;			//총알 타입
	CHARACTER_DIRECTION m_Direction;	//총알 방향

	estring m_TextureName;				//총알 텍스쳐 이름
	EPN_Pos m_BulletSize;				//총알 크기
	EPN_Pos m_MovingPos;				//움직일 위치

	float m_Angle;						//총알 각도
	int m_BulletDieCnt;					//총알 없어지는 시간 카운트(폭발)
	int m_BulletDieTime;				//총알 없어지는 시간
	bool m_SetBombPosFlag;				//총알 폭발 텍스쳐 위치 잡기용

public:
	BULLET_TYPE GetBulletType();

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual void IsSensed(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__BULLET_H__