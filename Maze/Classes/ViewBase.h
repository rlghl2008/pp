#ifndef __VIEW_BASE_H__
#define __VIEW_BASE_H__

#include "../../Enpine/Client/Generic/InputEvent/EPN_InputEvent.h"
#include "../../Enpine/Client/Generic/Texture/EPN_TextureInterface.h"
#include "../../Enpine/Client/Generic/Font/EPN_FontInterface.h"
#include "../../Enpine/Client/Generic/Device/EPN_DeviceInterface.h"
#include "../../Enpine/Client/Generic/Action/EPN_ActionInterface.h"
#include "EPN_SoundInterface.h"
using namespace EPN;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::INPUTEVENT;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::ACTION;
using namespace std;

enum class VIEW_STATE
{
	NONE = -1,
	LOGO,
	MENU,
	GUIDE,
	STORY,
	INGAME,
	LOSE,
	WIN,
	MAX
};

class ViewBase
{
public:
	ViewBase();
	virtual ~ViewBase();

protected:
	EPN_DeviceInterface* m_pEPN_DI; 
	EPN_TextureInterface * m_pEPN_TI;
	EPN_ActionInterface * m_pEPN_AI;
	EPN_FontInterface *	m_pEPN_FI;
	EPN_SoundInterface * m_pEPN_SI;
	EPN_InputEvent * m_pEvent;

public:
	virtual VIEW_STATE Run() = 0;
};

#endif // !__VIEW_BASE_H__