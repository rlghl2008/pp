#pragma once

#include "../Enpine/Common/Common.h"

#pragma region EPN Namespace
using namespace EPN;
using namespace EPN::COMMON;
#pragma endregion


#include "fmod.hpp"
#pragma comment(lib, "fmodex_vc.lib")
using namespace FMOD;

#define SOUND_CHANNEL_MAX 300

struct EPN_Sound
{
	EPN_Sound()
	{
		pSound = NULL_PTR;
		pChannel = NULL_PTR;
	}
	~EPN_Sound()
	{
		if(pSound != NULL_PTR)
			pSound->release();
	}
	Sound * pSound;
	Channel * pChannel;
};

class EPN_SoundInterface
{
private:
	static EPN_SoundInterface * m_pEPN_SI;
public:
	static EPN_SoundInterface * CreateInstance();
	static EPN_SoundInterface * GetInstance();
	static VOID ReleaseInstance();
private:
	EPN_SoundInterface();
	~EPN_SoundInterface();
private: //FMOD
	System * m_SoundSystem; //사운드를 관리하기 위한 시스템 포인터
	
	EPN_MemoryPool<EPN_Sound*> m_mpEffect; //이펙트
	EPN_StringIndexPool m_StringIndexPool;

/*	 BGM	 */
private:

	Channel * m_pBgmChannel; //BGM 채널
	Sound * m_pCurBgm;
	std::string m_pCurBgmPath;
	INT32 m_BgmVolume;
public:
	INT32 GetBgmVolume();
	BOOL SetBgmVolume(INT32 Volume);
	BOOL PlayBgm(std::string Path , BOOL RepeatFlag = TRUE);
	BOOL PauseBgm();
	BOOL ResumeBgm();
	BOOL StopBgm();
	BOOL IsBgmPlaying();

/*	EFFECT	 */
private:	
	INT32 m_EffectVolume;
	INT32 m_CurBgmID;
	INT32 m_CurEffectID;
public:
	BOOL PlayEffect(std::string Path);

	INT32 GetEffectVolume();
	VOID SetEffectVolume(INT32 Volume);

public:
	VOID Run(); //시스템 채널 업데이트를 하지않으면 64번 정도만 재생됨
};

