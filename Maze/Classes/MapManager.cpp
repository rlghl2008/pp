#include "MapManager.h"

MapManager * MapManager::m_pMapManager = NULL;

MapManager* MapManager::CreateInstance()
{
	if (!m_pMapManager)
		m_pMapManager = new MapManager();
	return m_pMapManager;
}

MapManager::MapManager():
	m_CurStage(0),
	m_MapChangeFlag(true)
{
	char tempBuff[256] = { 0, };

	/*맵 충돌 데이터, 레이어수, 맵이름*/
	FILE * pFile = fopen("Map/MapData.txt", "rt");
	for (int Stage = 0; Stage < STAGE_MAX; ++Stage)
	{
		fscanf(pFile, "%s", tempBuff);
		m_MapInfo[Stage].TextureName = tempBuff;			//텍스쳐 이름
		fscanf(pFile, "%d", &m_MapInfo[Stage].LayerNum);	//레이어 수
		for (int Y = 0; Y < MAP_SIZE_Y; ++Y)
		{
			for (int X = 0; X < MAP_SIZE_X; ++X)
			{
				//충돌 데이터(1이면 충돌하는 타일)
				fscanf(pFile, "%d", &m_MapInfo[Stage].ColisionData[Y][X]);
			}
		}
	}
	fclose(pFile);

	/* 포탈 정보 */
	pFile = fopen("Map/PortalData.txt", "rt");
	for (int Stage = 0; Stage < STAGE_MAX; ++Stage)
	{
		int PortCount = 0;
		/* 포탈 데이터 실제 읽는 처리 */
		fscanf(pFile, "%s", tempBuff);
		fscanf(pFile, "%d", &PortCount);

		for (int Cnt = 0; Cnt < PortCount; ++Cnt)
		{
			//포탈의 현재 영역
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].PortalArea.Top.X);
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].PortalArea.Top.Y);
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].PortalArea.Bottom.X);
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].PortalArea.Bottom.Y);

			//다음 맵 인덱스
			fscanf(pFile, "%d", &m_MapInfo[Stage].PortInfo[Cnt].TargetMapStage);

			//포탈과 연결된 위치
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].LinkedPos.X);
			fscanf(pFile, "%f", &m_MapInfo[Stage].PortInfo[Cnt].LinkedPos.Y);
		}
	}
	fclose(pFile);
}

MapManager::~MapManager()
{
}

MapInfo* MapManager::GetMapInfo(int Stage)
{
	return &m_MapInfo[Stage];
}

MapInfo* MapManager::GetCurMapInfo()
{
	return &m_MapInfo[m_CurStage];
}

void MapManager::SetStage(int Stage)
{
	m_MapChangeFlag = true;
	m_CurStage = Stage;
}

int MapManager::GetCurStageIndex()
{
	return m_CurStage;
}

bool** MapManager::GetMapColisionData(int Stage)
{
	//return *(m_MapInfo[Stage].ColisionData);
	return nullptr;
}

int MapManager::GetMapSizeX()
{
	return 0;
}

int MapManager::GetMapSizeY()
{
	return 0;
}