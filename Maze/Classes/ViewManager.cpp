#include "ViewManager.h"

ViewManager::ViewManager()
{
	m_pView = NULL;
	m_ViewState = VIEW_STATE::NONE;
}

ViewManager::~ViewManager()
{
	if (m_pView)
	{
		delete m_pView;
		m_pView = NULL;
	}
}

bool ViewManager::ChangeView(VIEW_STATE View)
{
	if (View != m_ViewState)
	{
		if (m_pView)
		{
			delete m_pView;
			m_pView = NULL;
		}

		switch (View)
		{
		case VIEW_STATE::LOGO:
			m_pView = new ViewLogo();
			m_ViewState = VIEW_STATE::LOGO;
			break;
		case VIEW_STATE::MENU:
			m_pView = new ViewMenu();
			m_ViewState = VIEW_STATE::MENU;
			break;
		case VIEW_STATE::GUIDE:
			m_pView = new ViewGuide();
			m_ViewState = VIEW_STATE::GUIDE;
			break;
		case VIEW_STATE::STORY:
			m_pView = new ViewStory();
			m_ViewState = VIEW_STATE::STORY;
			break;
		case VIEW_STATE::INGAME:
			m_pView = new ViewInGame();
			m_ViewState = VIEW_STATE::INGAME;
			break;
		case VIEW_STATE::LOSE:
			m_pView = new ViewLose();
			m_ViewState = VIEW_STATE::LOSE;
			break;
		case VIEW_STATE::WIN:
			m_pView = new ViewWin();
			m_ViewState = VIEW_STATE::WIN;
			break;
		default:
			break;
		}
	}
	return true;
}

bool ViewManager::Run()
{
	return ChangeView(m_pView->Run());
}