#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include "ObjectBase.h"
#include "AnimationSet.h"

enum class CHARACTER_TYPE			//캐릭터 타입(변신)
{
	NONE = -1,
	NORMAL,
	FIRE,
	ICE,
	THUNDER,
	MAX
};

enum class CHARACTER_DIRECTION
{
	NONE = -1,
	UP,
	DOWN,
	LEFT,
	RIGHT,
	MAX
};

class Character : public ObjectBase
{
	enum ANIMATION_TYPE				//애니매이션 타입
	{
		ANIMATION_TYPE_NONE = -1,
		MIZ_NORMAL_STAND,
		MIZ_NORMAL_WALK_UP,
		MIZ_NORMAL_WALK_DOWN,
		MIZ_NORMAL_SIDE_WALK,
		MIZ_FIRE_STAND,
		MIZ_FIRE_WALK_UP,
		MIZ_FIRE_WALK_DOWN,
		MIZ_FIRE_SIDE_WALK,
		MIZ_ICE_STAND,
		MIZ_ICE_WALK_UP,
		MIZ_ICE_WALK_DOWN,
		MIZ_ICE_SIDE_WALK,
		MIZ_THUNDER_STAND,
		MIZ_THUNDER_WALK_UP,
		MIZ_THUNDER_WALK_DOWN,
		MIZ_THUNDER_SIDE_WALK,
		ANIMATION_TYPE_MAX
	};

	enum MOVEMENT_TYPE				//행동 타입
	{
		MOVEMENT_TYPE_NONE,
		STAND,
		WALK_UP,
		WALK_DOWN,
		SIDE_WALK,
		MOVEMENT_TYPE_MAX
	};

public:
	Character();
	~Character();

private:

	ANIMATION_TYPE m_AnimationType[MOVEMENT_TYPE_MAX];	//애니매이션 타입
	CHARACTER_TYPE m_CharacterType;						//캐릭터 타입(속성)
	CHARACTER_DIRECTION m_Direction;					//캐릭터가 마지막으로 쳐다본 방향

	int m_BulletDelay;			//총알 딜레이
	int m_BulletDelayCnt;		//총알 딜레이 카운트
	bool m_ShootEnbleFlag;		//총알 발사 가능 플래그

	bool m_TypeChangeFlag;		//타입 체인지할때 플래그
	bool m_ReversalFlag;		//캐릭터 반전 플래그

	bool m_InvincibleFlag;		//무적 플래그
	int m_InvincibleCnt;		//무적 시간 카운트
	int m_InvincibleTime;		//무적 시간

	bool m_BlinkFlag;			//깜박임 플래그
	int m_BlinkCnt;				//깜박임 카운트
	int m_BlinkDelay;			//깜박임 주기
	float m_Alpha;				//알파값

private:
	void CharacterTypeChange();

public:
	CHARACTER_TYPE GetCharacterType();
	CHARACTER_DIRECTION GetDirection();

	void SetCharacterType(CHARACTER_TYPE CharacterType);

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual void IsSensed(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__CHARACTER_H__