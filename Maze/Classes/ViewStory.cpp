#include "ViewStory.h"

ViewStory::ViewStory() :
	m_NextPageFlag(false),
	m_PageChangeFlag(false),
	m_MoveFlag(false),
	m_MaxPagePosX(0),
	m_CurPagePosX(0.f),
	m_CurPageNumber(1),
	m_SubtitleAlpha(0)
{
	//되돌아가기 버튼
	m_Button[BUTTON_TYPE::BACK].SetPos(EPN_Pos(10, 590));
	m_Button[BUTTON_TYPE::BACK].SetTexture("Back", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::BACK].SetRect(EPN_Rect(0, 0, 50, 50));
	m_Button[BUTTON_TYPE::BACK].SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::BACK].SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::BACK].SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::BACK].SetScaling(EPN_Scaling(1.f, 1.f));

	//이전 페이지 버튼
	m_Button[BUTTON_TYPE::PREVIOUS].SetPos(EPN_Pos(673, 61));
	m_Button[BUTTON_TYPE::PREVIOUS].SetTexture("Next", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::PREVIOUS].SetRect(EPN_Rect(0, 0, 50, 50));
	m_Button[BUTTON_TYPE::PREVIOUS].SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::PREVIOUS].SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::PREVIOUS].SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::PREVIOUS].SetScaling(EPN_Scaling(1.f, 1.f));
	m_Button[BUTTON_TYPE::PREVIOUS].SetReversal(true);

	//다음 페이지 버튼
	m_Button[BUTTON_TYPE::NEXT].SetPos(EPN_Pos(738, 61));
	m_Button[BUTTON_TYPE::NEXT].SetTexture("Next", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::NEXT].SetRect(EPN_Rect(0, 0, 50, 50));
	m_Button[BUTTON_TYPE::NEXT].SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::NEXT].SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::NEXT].SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::NEXT].SetScaling(EPN_Scaling(1.f, 1.f));

	m_pEPN_SI->PlayBgm("Sound/Story/StoryViewSound.mp3");
	m_pEPN_SI->SetBgmVolume(50);
}

ViewStory::~ViewStory()
{
	m_pEPN_SI->StopBgm();
}

void ViewStory::MovePage()
{
	//무브 플래그
	if (m_MoveFlag)
	{
		//이전 페이지(Previous Page)
		if (!m_NextPageFlag)
		{
			if (m_PageChangeFlag)
			{
				//최대 증가할 수 있는 페이지 X포스 증가
				m_MaxPagePosX += 800;
				if (m_MaxPagePosX > 0)
					m_MaxPagePosX = 0;
				m_PageChangeFlag = false;
			}

			m_CurPagePosX += 20.f;

			//현재 X위치가 맥스X위치에 다다르면 무브플래그 닫음
			if (m_CurPagePosX > m_MaxPagePosX)
			{
				m_CurPagePosX = m_MaxPagePosX;
				m_MoveFlag = false;
			}
		}
		//다음 페이지(Next Page)
		else
		{
			if (m_PageChangeFlag)
			{
				//최소로 감소할 수 있는 페이지 X포스 증가
				m_MaxPagePosX -= 800;
				if (m_MaxPagePosX < -2400)
					m_MaxPagePosX = -2400;
				m_PageChangeFlag = false;
			}

			m_CurPagePosX -= 20.5f;

			//현재 X위치가 맥스X위치에 다다르면 무브플래그 닫음
			if (m_CurPagePosX < m_MaxPagePosX)
			{
				m_CurPagePosX = m_MaxPagePosX;
				m_MoveFlag = false;
			}
		}
	}
}

void ViewStory::WriteSubtitle()
{
	estring Text;
	EPN_Pos Pos;

	switch (m_CurPageNumber)
	{
	case 1:
		Text = "             어느 한 연구소\n     동물에게 초능력을 부여하여\n군대를 양성하려는 프로젝트 Miz의 실험이\n비밀리에 진행되고 있었다.";
		Pos = EPN_Pos(180, 150);
		break;
	case 2:
		Text = "36번째 실험체 Miz는\n약물에 던져지게되고..";
		Pos = EPN_Pos(170, 150);
		break;
	case 3:
		Text = "Miz의 의식은 약물속에서\n서서히 사라져간다.";
		Pos = EPN_Pos(50, 150);
		break;
	case 4:
		Text = "의식이 사라지려던 그때!\nMiz의 눈이 붉게 물들며 힘을 각성하게되고\nMiz는 폭주하며 연구소 탈출을 시도한다!";
		Pos = EPN_Pos(50, 150);
		break;
	}

	if (m_SubtitleAlpha++ >= 255)
		m_SubtitleAlpha = 255;
	m_pEPN_FI->BltText("맑은고딕20", 0, Text, Pos, EPN_ARGB(m_SubtitleAlpha, 255, 255, 255));
}

VIEW_STATE ViewStory::Run()
{
	//페이지 움직이는 동작
	MovePage();

	//스토리 그림
	m_pEPN_TI->Blt("Story", 0, EPN_Pos(m_CurPagePosX, 600), EPN_Rect(0, 0, 3200, 600));

	m_Button[BUTTON_TYPE::BACK].Run();
	m_Button[BUTTON_TYPE::PREVIOUS].Run();
	m_Button[BUTTON_TYPE::NEXT].Run();

	//페이지 넘기기 버튼은 무브플래그에 닫혀있을때만 동작
	if (!m_MoveFlag)
	{
		if (m_Button[BUTTON_TYPE::PREVIOUS].GetState() == BUTTON_STATE::POP)
		{
			m_MoveFlag = true;
			m_NextPageFlag = false;
			m_PageChangeFlag = true;

			if (--m_CurPageNumber < 1)
				m_CurPageNumber = 1;
			else
				m_SubtitleAlpha = 0;
		}
		else if (m_Button[BUTTON_TYPE::NEXT].GetState() == BUTTON_STATE::POP)
		{
			m_MoveFlag = true;
			m_NextPageFlag = true;
			m_PageChangeFlag = true;

			if (++m_CurPageNumber > 4)
				m_CurPageNumber = 4;
			else
				m_SubtitleAlpha = 0;
		}
	}

	//자막
	WriteSubtitle();

	if (m_Button[BUTTON_TYPE::BACK].GetState() == BUTTON_STATE::POP)
		return VIEW_STATE::MENU;

	return VIEW_STATE::STORY;
}