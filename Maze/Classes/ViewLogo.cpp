#include "ViewLogo.h"

ViewLogo::ViewLogo()
{
	m_FadeChangeFlag = false;
	m_FadeChangeCnt = 0;
	m_Alpha = 0.0f;
}

ViewLogo::~ViewLogo()
{
}

void ViewLogo::FadeInOut()
{
	if (!m_FadeChangeFlag)
	{
		m_Alpha -= 1.5;

		if (m_Alpha < 0)
		{
			m_Alpha = 0;
			m_FadeChangeFlag = true;
			m_FadeChangeCnt++;
		}
	}
	else
	{
		m_Alpha += 1.5;

		if (m_Alpha > 255)
		{
			m_Alpha = 255;
			m_FadeChangeFlag = false;
			m_FadeChangeCnt++;
		}
	}
}

VIEW_STATE ViewLogo::Run()
{ 
	if (m_FadeChangeCnt > 2)
		return VIEW_STATE::MENU;

	//로고 출력 & 페이드인아웃
	m_pEPN_TI->Blt("Maze", 0, EPN_Pos(110, 450), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(m_Alpha, 255, 255, 255));
	FadeInOut();

	return VIEW_STATE::LOGO;
}