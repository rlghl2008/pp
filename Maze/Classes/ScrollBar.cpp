#include "ScrollBar.h"
using namespace std;

ScrollBar::ScrollBar() :
	m_IsClicked(false),
	m_VolumeType(false)
{
	m_pEPN_DI = EPN_DeviceInterface::GetInstance();
	m_pEPN_TI = EPN_TextureInterface::GetInstance();
	m_pEPN_FI = EPN_FontInterface::GetInstance();
	m_pEvent = EPN_InputEvent::GetInstance();
	m_pEPN_SI = EPN_SoundInterface::GetInstance();

	m_CircleRect = m_pEPN_TI->GetTextureSize("ScrollBarCircle");
}


ScrollBar::~ScrollBar()
{
}

void ScrollBar::SetPos(EPN_Pos Pos)
{
	m_Pos = Pos;
	m_CirclePos = EPN_Pos(m_Pos.X, m_Pos.Y - 61);
}

void ScrollBar::SetVolumeType(bool Type)
{
	m_VolumeType = Type;
}

bool ScrollBar::Run()
{
	m_MousePos = m_pEvent->GetMousePos();

	m_pEPN_TI->Blt("ScrollBarCircle", 0, m_CirclePos);

	if (EPN_CheckCollision(m_MousePos, EPN_Rect(m_CirclePos.X, m_CirclePos.Y, m_CirclePos.X + m_CircleRect.Bottom.X, m_CirclePos.Y - m_CircleRect.Bottom.Y)))
	{
		if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::DOWN)
			m_IsClicked = true;
	}

	if (m_IsClicked == true)
	{
		if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::DOWN)
		{
			m_DownMousePos = m_MousePos;
			m_DownCirclePos = m_CirclePos;
		}
		else if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::DOWNING)
		{
			m_CirclePos.X = m_DownCirclePos.X + (m_MousePos.X - m_DownMousePos.X);
			if (m_CirclePos.X < 300)
				m_CirclePos.X = 300;
			else if (m_CirclePos.X > 465)
				m_CirclePos.X = 465;

			int Volumn = ((m_CirclePos.X - 300) / 165) * 100;
			if (m_VolumeType == true)
				m_pEPN_SI->SetBgmVolume(Volumn);
			else
				m_pEPN_SI->SetEffectVolume(Volumn);
		}
		else
		{
			m_IsClicked = false;
		}
	}
	return true;
}
