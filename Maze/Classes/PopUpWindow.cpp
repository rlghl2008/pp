#include "PopUpWindow.h"

PopUpWindow::PopUpWindow() :
	m_IsGuide(true),
	m_IsActive(true),
	m_SettingGuideFlag(false)
{
	m_pEPN_DI = EPN_DeviceInterface::GetInstance();
	m_pEPN_TI = EPN_TextureInterface::GetInstance();
	m_pEPN_FI = EPN_FontInterface::GetInstance();
	m_pEvent = EPN_InputEvent::GetInstance();

	m_CloseButton.SetPos(EPN_Pos(0, 0));
	m_CloseButton.SetTexture("BtnBack", BUTTON_STATE::ALL);
	m_CloseButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_CloseButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_CloseButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_CloseButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_CloseButton.SetScaling(EPN_Scaling(0.6f, 0.6f));

	m_ReturnButton.SetPos(EPN_Pos(299, 200));
	m_ReturnButton.SetTexture("BtnReturn", BUTTON_STATE::ALL);
	m_ReturnButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_ReturnButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_ReturnButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_ReturnButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_ReturnButton.SetScaling(EPN_Scaling(1.f, 1.f));

	m_HelpButton.SetPos(EPN_Pos(440, 200));
	m_HelpButton.SetTexture("BtnHelp", BUTTON_STATE::ALL);
	m_HelpButton.SetRect(EPN_Rect(0, 0, 50, 50));
	m_HelpButton.SetTextureRect(EPN_Rect(0, 0, 50, 50), BUTTON_STATE::ALL);
	m_HelpButton.SetTextureRect(EPN_Rect(50, 0, 100, 50), BUTTON_STATE::ACTIVE);
	m_HelpButton.SetTextureRect(EPN_Rect(100, 0, 150, 50), BUTTON_STATE::PUSH);
	m_HelpButton.SetScaling(EPN_Scaling(1.f, 1.f));

	m_BgmScrollBar.SetVolumeType(true);
	m_EffectScrollBar.SetVolumeType(false);
}

PopUpWindow::~PopUpWindow()
{

}

void PopUpWindow::SetActive(bool IsActive)
{
	m_IsActive = IsActive;
}

void PopUpWindow::SetPopUpType(bool IsGuide)
{
	m_IsGuide = IsGuide;
}

void PopUpWindow::SetPos(EPN_Pos Pos)
{
	m_Pos = Pos;

	if(m_IsGuide == true)
		m_CloseButton.SetPos(EPN_Pos(m_Pos.X + 336, m_Pos.Y - 13));
	else
	{
		m_CloseButton.SetPos(EPN_Pos(m_Pos.X + 248, m_Pos.Y - 8));
		m_CloseButton.SetScaling(EPN_Scaling(0.4f, 0.4f));
		
		m_BgmScrollBar.SetPos(EPN_Pos(m_Pos.X + 130, m_Pos.Y - 52));
		m_EffectScrollBar.SetPos(EPN_Pos(m_Pos.X + 130, m_Pos.Y - 104));
	}
}

BUTTON_STATE PopUpWindow::GetReturnButtonState()
{
	return m_ReturnButton.GetState();
}

BUTTON_STATE PopUpWindow::GetHelpButtonState()
{
	return m_HelpButton.GetState();
}

bool PopUpWindow::Run()
{
	if (m_IsActive == false)
		return false;

	if (m_IsGuide == true)
	{
		m_pEPN_TI->Blt("GuidePopUp", 0, m_Pos, EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(0.7f, 0.7f));
	}
	else
	{
		m_pEPN_TI->Blt("OptionsWindow", 0, m_Pos);
		m_BgmScrollBar.Run();
		m_ReturnButton.Run();
		m_HelpButton.Run();
		m_EffectScrollBar.Run();
	}

	m_CloseButton.Run();

	if (m_CloseButton.GetState() == BUTTON_STATE::POP)
		m_IsActive = false;

	return true;
}