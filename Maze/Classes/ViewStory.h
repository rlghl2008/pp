#ifndef __VIEW_STORY_H__
#define __VIEW_STORY_H__

#include "ViewBase.h"
#include "Button.h"
#include "Item.h"

class ViewStory : public ViewBase
{
	enum BUTTON_TYPE
	{
		NONE = -1,
		NEXT,
		PREVIOUS,
		BACK,
		MAX
	};

public:
	ViewStory();
	~ViewStory();

private:
	Button m_Button[BUTTON_TYPE::MAX];

	float m_CurPagePosX;	//현재 페이지 X 위치
	int m_MaxPagePosX;		//최대로 이동할 수 있는 X 위치
	bool m_NextPageFlag;	//true(다음 페이지), false(이전 페이지)
	bool m_MoveFlag;		//페이지 이동 플래그
	bool m_PageChangeFlag;	//페이지 바꾼다는 플래그
	int m_CurPageNumber;	//현재 페이지 넘버
	
	float m_SubtitleAlpha;

private:
	void MovePage();		//페이지 움직이는 동작
	void WriteSubtitle();	//스토리 자막 출력

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_STORY_H__