#ifndef __BOSS2_H__
#define __BOSS2_H__

#include "MonsterBase.h"

class Boss2 : public MonsterBase
{
private:
	enum BOSS_PATTERN
	{
		PATTERN_NORMAL,
		PATTERN_ATTACK1,
		PATTERN_ATTACK2,
		PATTERN_ATTACK3
	};
public:
	Boss2();
	~Boss2();

private:
	Character * m_pCharacter;
	ObjectManager *m_pObjectMgr;

private:
	BOSS_PATTERN m_Pattern;
	int m_PatternTimeCnt;
	int m_ShootDelayCnt;
	int m_ShootAngle;
	float m_Alpha;

public:
	virtual void IsCollided(ObjectBase * pObject);
	virtual bool Run();
};

#endif // !__BOSS2_H__