#ifndef __OBJECT_MANAGER_H__
#define __OBJECT_MANAGER_H__

#include "Character.h"

class ObjectManager
{
private:
	ObjectManager();

public:
	~ObjectManager();

private:
	static ObjectManager * m_pObjectManager;

public:
	static ObjectManager * CreateInstance();

private:
	EPN::COMMON::EPN_MemoryPool<ObjectBase*> m_mpObject; //오브젝트 풀

public:
	int AddObject(ObjectBase * pObject);				//오브젝트 추가	
	bool RemoveObject(int Index);						//오브젝트 삭제
	ObjectBase * SearchObject(int Index);				//인덱스의 오브젝트 반환
	int SearchObject(ObjectBase * pObject);				//오브젝트의 인덱스 반환
	void ClearObject();									//오브젝트 전부 삭제
	Character * GetCharacter();							//캐릭터 가져오기(0번째 인덱스는 무조건 캐릭터가 되도록 넣어놨음)
	int GetObjectNum();									//현제 오브젝트 수 반환
	bool IsExistMonster();
	void ClearItem();

public:
	void Run();
};

#endif // !__OBJECT_MANAGER_H__