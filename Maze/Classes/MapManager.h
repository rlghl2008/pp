#ifndef __EX_MAP_H__
#define __EX_MAP_H__

#include "../../Enpine/Common/Base/CommonStructure.h"
#include "../../Enpine/Common/Class/EPN_String.h"
using namespace std;

#define	STAGE_MAX 16
#define	MAP_SIZE_X 25
#define	MAP_SIZE_Y 19

struct PortalInfo
{
	EPN_Rect PortalArea;		//포탈 위치
	EPN_Pos LinkedPos;			//포탈 이동후 캐릭터 위치
	int TargetMapStage;			//목적지 스테이지 인덱스

	PortalInfo()
	{
		//-1 -1 이면 없는 데이터
		PortalArea = EPN_Rect(-1, -1, -1, -1);
		LinkedPos = EPN_Pos(-1, -1);
	}
};

struct MapInfo
{
	estring TextureName;						//맵 텍스쳐 이름
	int LayerNum;								//현재 스테이지의 레이어 수
	bool ColisionData[MAP_SIZE_Y][MAP_SIZE_X];	//맵 충돌 정보
	PortalInfo PortInfo[2];
};

class MapManager
{
private:
	MapManager();

public:
	~MapManager();

private:
	static MapManager* m_pMapManager;

	MapInfo m_MapInfo[STAGE_MAX];	//각 스테이지의 맵 정보
	int m_CurStage;					//현재 스테이지
	bool m_MapChangeFlag;			//맵 체인지 플래그

public:
	static MapManager* CreateInstance();

	MapInfo* GetMapInfo(int Stage);//원하는 스테이지 정보 가져오기
	MapInfo* GetCurMapInfo();		//현재 스테이지 정보 가져오기
	void SetStage(int Stage);		//스테이지 설정
	int GetCurStageIndex();
	bool** GetMapColisionData(int Stage);
	int GetMapSizeX();
	int GetMapSizeY();
};

#endif // !__EX_MAP_H__