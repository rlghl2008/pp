#ifndef __MONSTER_SPECIAL_H__
#define __MONSTER_SPECIAL_H__

#include "MonsterBase.h"

class MonsterSpecial : public MonsterBase
{
public:
	MonsterSpecial();
	MonsterSpecial(EPN_Pos Pos);
	~MonsterSpecial();

private:
	float m_HealValue;
	bool m_HealFlag;

	AnimationSet m_HealEffect;

public:
	virtual void IsSensed(ObjectBase* pObject);
	virtual void IsCollided(ObjectBase* pObject);
	virtual bool Run();
};

#endif // !__MONSTER_SPECIAL_H__