#ifndef __OBJECT_BASE_H__
#define __OBJECT_BASE_H__

#include "../../Enpine/Client/Generic/InputEvent/EPN_InputEvent.h"
#include "../../Enpine/Client/Generic/Texture/EPN_TextureInterface.h"
#include "../../Enpine/Client/Generic/Font/EPN_FontInterface.h"
#include "../../Enpine/Client/Generic/Device/EPN_DeviceInterface.h"
#include "../../Enpine/Client/Generic/Action/EPN_ActionInterface.h"
#include "EPN_SoundInterface.h"
#include "MapManager.h"
using namespace EPN;
using namespace EPN::GENERIC;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::INPUTEVENT;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::ACTION;
#include "AnimationSet.h"
enum class OBJECT_TYPE //오브젝트 타입(종류)
{
	NONE = -1,
	CHRACTER,
	BULLET,
	MONSTER,
	ITEM,
	MAX
};

enum class TEAM_FLAG //팀
{
	NONE = -1,
	PLAYER,
	ENEMY,
	NEUTRAL,
	MAX
};

struct ObjectInfo //오브젝트 베이스를 상속받는 클래스의 기본 정보
{
	TEAM_FLAG TeamFlag;		//팀 플래그
	OBJECT_TYPE ObjectType;	//오브젝트 타입

	EPN_Pos Pos;			//위치
	EPN_Rect Rect;			//그리는 렉트
	EPN_Rect CollisionRect;	//충돌 렉트
	EPN_Rect SensorRect;	//감지 렉트
	EPN_Rect FootRect;		//맵 충돌 렉트
	EPN_Scaling Scaling;	//크기비율
	EPN_ARGB BlendColor;	//섞을 색깔
	
	float Hp;				//현재 HP
	float MaxHp;			//최대 HP
	float Atk;				//공격력
	float Speed;			//스피드
	bool IsDied;			//죽음 확인용 플래그(이 플래그가 true가 될때만 오브젝트를 삭제함)
	bool IsEnable;			//충돌 가능 체크 플래그

	ObjectInfo()
	{
		TeamFlag = TEAM_FLAG::NONE;
		ObjectType = OBJECT_TYPE::NONE;

		Pos = EPN_Pos(0, 0);
		Rect = EPN_Rect(-1, -1, -1, -1);
		CollisionRect = EPN_Rect(-1, -1, -1, -1);
		SensorRect = EPN_Rect(-1, -1, -1, -1);
		FootRect = EPN_Rect(-1, -1, -1, -1);
		Scaling = EPN_Scaling(1.f, 1.f);
		BlendColor = EPN_ARGB(255, 255, 255, 255);

		Hp = 0.f;
		MaxHp = 0.f;
		Atk = 0.f;
		Speed = 0.f;
		IsDied = false;
		IsEnable = true;
	}
};

class ObjectBase
{
public:
	ObjectBase();
	virtual ~ObjectBase();

protected:
	EPN_DeviceInterface* m_pEPN_DI;
	EPN_TextureInterface* m_pEPN_TI;
	EPN_FontInterface* m_pEPN_FI;
	EPN_ActionInterface* m_pEPN_AI;
	EPN_SoundInterface* m_pEPN_SI;
	EPN_InputEvent* m_pEvent;

private:
	MapManager* m_pMap;
	MapInfo* m_pCurMapInfo;

	bool m_MapChangeFlag;

protected:
	ObjectInfo m_ObjectInfo;	//오브젝트 정보
	EPN_Pos m_PrevPos;			//이동하기 전 위치	
	AnimationSet m_Animation;	//애니매이션

public:
	ObjectInfo* GetObjectInfo();

public:
	virtual void IsSensed(ObjectBase* pObject) = 0;
	virtual void IsCollided(ObjectBase * pObject) = 0;
	virtual bool Run();
};

#endif //!__OBJECT_BASE_H__