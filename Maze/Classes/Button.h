#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "../../Enpine/Client/Generic/InputEvent/EPN_InputEvent.h"
#include "../../Enpine/Client/Generic/Texture/EPN_TextureInterface.h"
#include "../../Enpine/Client/Generic/Font/EPN_FontInterface.h"
#include "../../Enpine/Client/Generic/Device/EPN_DeviceInterface.h"
#include "../../Enpine/Client/Generic/Action/EPN_ActionInterface.h"
using namespace EPN;
using namespace EPN::GENERIC;
using namespace EPN::COMMON::FUNCTION;
using namespace EPN::GENERIC::DEVICE;
using namespace EPN::GENERIC::TEXTURE;
using namespace EPN::GENERIC::INPUTEVENT;
using namespace EPN::GENERIC::FONT;
using namespace EPN::GENERIC::ACTION;

enum class BUTTON_STATE
{
	ALL = -1,
	NORMAL,
	ACTIVE,
	PUSH,
	POP,
	MAX
};

class Button
{
public:
	Button();
	~Button();

private:
	EPN_DeviceInterface* m_pEPN_DI;
	EPN_TextureInterface * m_pEPN_TI;
	EPN_FontInterface *	m_pEPN_FI;
	EPN_InputEvent * m_pEvent;

private:
	BUTTON_STATE m_State; //UI 상태
	EPN_Pos	m_Pos;
	EPN_Rect m_OriginRect;
	EPN_Rect m_Rect; //UI 영역 Rect
	EPN_Scaling m_Scaling;
	EPN_Rect m_TextureRect[(INT16)BUTTON_STATE::MAX]; //4가지 상태 따른 텍스쳐 렉트
	EPN_ARGB m_Color[(INT16)BUTTON_STATE::MAX];
	short m_TextureIndex[(INT16)BUTTON_STATE::MAX]; //4가지 상태에 따른 텍스쳐 인덱스

	//Font
	short m_FontIndex; //버튼에 사용할 폰트 인덱스
	estring	m_EText;
	EPN_ARGB m_FontColor[(INT16)BUTTON_STATE::MAX];

	bool m_ReversalFlag;

private:
	EPN_Pos m_ETextBltRect;
	EPN_Pos m_ETextBltPos;
	EPN_Pos m_ETextAreaSize;
	EPN_Pos m_ButtonRealSize;
	EPN_Pos m_MousePos;

public:
	void SetState(BUTTON_STATE State);
	void SetPos(EPN_Pos Pos);
	void SetRect(EPN_Rect Rect);
	void SetTextureRect(EPN_Rect TextureRect, BUTTON_STATE State = BUTTON_STATE::ALL);
	void SetColor(EPN_ARGB Color, BUTTON_STATE State);
	void SetScaling(EPN_Scaling Scaling);
	void SetTexture(short TextureIndex, BUTTON_STATE State = BUTTON_STATE::ALL);
	void SetTexture(estring TextureName, BUTTON_STATE State = BUTTON_STATE::ALL);
	void SetFont(short FontIndex);
	void SetFont(estring FontName);
	void SetFontColor(EPN_ARGB Color, BUTTON_STATE State = BUTTON_STATE::ALL);
	void SetEText(estring Text);
	void SetReversal(bool reveralFlag);

	BUTTON_STATE GetState();
	EPN_Pos GetPos();
	EPN_Rect GetRect();
	EPN_Scaling GetScaling();
	EPN_Rect GetTextureRect(BUTTON_STATE State);
	EPN_ARGB GetColor(BUTTON_STATE State);
	short GetTextureIndex(BUTTON_STATE State);
	short GetFontIndex();
	EPN_ARGB GetFontColor(BUTTON_STATE State);
	estring GetEText();

public:
	bool Run();
};

#endif // !__BUTTON_H__