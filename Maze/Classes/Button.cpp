#include "Button.h"

Button::Button(void)
{
	m_pEPN_DI = EPN_DeviceInterface::GetInstance();
	m_pEPN_TI = EPN_TextureInterface::GetInstance();
	m_pEPN_FI = EPN_FontInterface::GetInstance();
	m_pEvent = EPN_InputEvent::GetInstance();

	m_State = BUTTON_STATE::NORMAL;
	m_Scaling = 1.0f;
	m_FontIndex = -1;
	m_ReversalFlag = false;
	memset(m_TextureIndex, -1,  sizeof(m_TextureIndex));
}

Button :: ~Button(void)
{
}

void Button::SetState(BUTTON_STATE State)
{
	m_State = State;
}

void Button::SetPos(EPN_Pos Pos)
{
	m_Pos = Pos;
}

void Button::SetRect(EPN_Rect Rect)
{
	m_Rect = Rect;
	m_OriginRect = m_Rect;
}

void Button::SetScaling(EPN_Scaling Scaling)
{
	m_Rect.Bottom.X = m_OriginRect.Bottom.X * Scaling.X;
	m_Rect.Bottom.Y = m_OriginRect.Bottom.Y * Scaling.Y;
	m_Rect.Top.X = m_OriginRect.Top.X * Scaling.X;
	m_Rect.Top.Y = m_OriginRect.Top.Y * Scaling.Y;

	m_Scaling = Scaling;
}

void Button::SetTextureRect(EPN_Rect TextureRect, BUTTON_STATE State)
{
	if (State < BUTTON_STATE::ALL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		State = BUTTON_STATE::ALL;

	if (State == BUTTON_STATE::ALL)
	{
		for (int StateCnt = 0; StateCnt < (INT16)BUTTON_STATE::MAX; ++StateCnt)
			m_TextureRect[StateCnt] = TextureRect;
	}
	else
		m_TextureRect[(INT16)State] = TextureRect;
}

void Button::SetColor(EPN_ARGB Color, BUTTON_STATE State)
{
	if (State < BUTTON_STATE::ALL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		State = BUTTON_STATE::ALL;

	if (State == BUTTON_STATE::ALL)
	{
		for (int StateCnt = 0; StateCnt < (INT16)BUTTON_STATE::MAX; ++StateCnt)
			m_Color[StateCnt] = Color;
	}
	else
		m_Color[(INT16)State] = Color;
}

void Button::SetTexture(short TextureIndex, BUTTON_STATE State)
{
	if (State < BUTTON_STATE::ALL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		State = BUTTON_STATE::ALL;

	if (State == BUTTON_STATE::ALL)
	{
		for (short StateCnt = 0; StateCnt < (INT16)BUTTON_STATE::MAX; ++StateCnt)
			m_TextureIndex[StateCnt] = TextureIndex;
	}
	else
		m_TextureIndex[(INT16)State] = TextureIndex;
}

void Button::SetTexture(estring TextureName, BUTTON_STATE State)
{
	if (State < BUTTON_STATE::ALL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		State = BUTTON_STATE::ALL;

	short TextureIndex = m_pEPN_TI->GetTextureIndex(TextureName);
	if (State == BUTTON_STATE::ALL)
	{
		for (short StateCnt = 0; StateCnt < (INT16)BUTTON_STATE::MAX; ++StateCnt)
			m_TextureIndex[StateCnt] = TextureIndex;
	}
	else
		m_TextureIndex[(INT16)State] = TextureIndex;
}

void Button::SetFont(short FontIndex)
{
	m_FontIndex = FontIndex;
}

void Button::SetFont(estring FontName)
{
	short FontIndex = m_pEPN_FI->GetFontIndex(FontName);
	m_FontIndex = FontIndex;
}

void Button::SetFontColor(EPN_ARGB Color, BUTTON_STATE State)
{
	if (State < BUTTON_STATE::ALL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		State = BUTTON_STATE::ALL;

	if (State == BUTTON_STATE::ALL)
	{
		for (int StateCnt = 0; StateCnt < (INT16)BUTTON_STATE::MAX; ++StateCnt)
			m_FontColor[StateCnt] = Color;
	}
	else
		m_FontColor[(INT16)State] = Color;
}
void Button::SetEText(estring Text)
{
	m_EText = Text;
}

BUTTON_STATE Button::GetState()
{
	return m_State;
}

EPN_Pos Button::GetPos()
{
	return m_Pos;
}

EPN_Rect Button::GetRect()
{
	return m_Rect;
}

EPN_Scaling Button::GetScaling()
{
	return m_Scaling;
}

EPN_Rect Button::GetTextureRect(BUTTON_STATE State)
{
	if (State < BUTTON_STATE::NORMAL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		return -1;
	return m_TextureRect[(INT16)State];
}

EPN_ARGB Button::GetColor(BUTTON_STATE State)
{
	if (State < BUTTON_STATE::NORMAL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		return -1;
	return m_Color[(INT16)State];
}

short Button::GetTextureIndex(BUTTON_STATE State)
{
	if (State < BUTTON_STATE::NORMAL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		return -1;
	return m_TextureIndex[(INT16)State];
}

short Button::GetFontIndex()
{
	return m_FontIndex;
}

EPN_ARGB Button::GetFontColor(BUTTON_STATE State)
{
	if (State < BUTTON_STATE::NORMAL || (INT16)State > (INT16)BUTTON_STATE::MAX - 1)
		return -1;
	return m_FontColor[(INT16)State];
}

estring Button::GetEText()
{
	return m_EText;
}

void Button::SetReversal(bool ReveralFlag)
{
	m_ReversalFlag = ReveralFlag;
}

bool Button::Run()
{
	m_MousePos = m_pEvent->GetMousePos();

	//팝 했다면 다시 일반상태로 변경
	if (m_State == BUTTON_STATE::POP)
		m_State = BUTTON_STATE::NORMAL;

	//마우스 위치가 현재 버튼에 들어왔을 때
	if (EPN_CheckCollision(m_MousePos, EPN_Rect(m_Pos.X, m_Pos.Y, m_Pos.X + m_Rect.Bottom.X, m_Pos.Y - m_Rect.Bottom.Y)))
	{
		if (m_State == BUTTON_STATE::NORMAL && m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::NONE)
			m_State = BUTTON_STATE::ACTIVE;
		else if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::DOWN && m_State == BUTTON_STATE::ACTIVE)
		{
			m_State = BUTTON_STATE::PUSH;
		}
		else if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::DOWNING && m_State == BUTTON_STATE::PUSH)
		{
			m_State = BUTTON_STATE::PUSH;
		}
		else if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::UP && m_State == BUTTON_STATE::PUSH)
		{
			m_State = BUTTON_STATE::POP;
		}
	}
	//밖으로 나갔을 때
	else
	{
		if (m_State == BUTTON_STATE::ACTIVE)
			m_State = BUTTON_STATE::NORMAL;
		else if (m_State == BUTTON_STATE::PUSH)
		{
			if (m_pEvent->GetMouseStatL() == EPN_INPUT_STATE::UP)
				m_State = BUTTON_STATE::NORMAL;
		}
	}

	m_pEPN_TI->Blt(m_TextureIndex[(INT16)m_State], 0, m_Pos, m_TextureRect[(INT16)m_State], 
		m_Color[(INT16)m_State], 0, m_Scaling, m_ReversalFlag);

	//폰트가 있다면
	if (m_FontIndex != -1)
	{
		m_ETextAreaSize = m_pEPN_FI->GetETextAreaSize(m_FontIndex, m_EText);
		m_ButtonRealSize = EPN_Pos(m_Rect.Bottom.X, m_Rect.Bottom.Y);
		m_ETextBltPos = EPN_Pos(m_ButtonRealSize - m_ETextAreaSize) / 2;
		m_ETextBltRect = m_Pos + EPN_Pos(m_ETextBltPos.X, -m_ETextBltPos.Y);
		m_pEPN_FI->BltText(m_FontIndex, 0, m_EText, m_ETextBltRect, m_FontColor[(INT16)m_State]);
	}	
	return true;
}