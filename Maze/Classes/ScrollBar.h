#ifndef __SCROLL_BAR_H__
#define __SCROLL_BAR_H__

#include "Button.h"
#include "EPN_SoundInterface.h"

class ScrollBar
{
public:
	ScrollBar();
	~ScrollBar();

private:
	EPN_DeviceInterface* m_pEPN_DI;
	EPN_TextureInterface * m_pEPN_TI;
	EPN_FontInterface *	m_pEPN_FI;
	EPN_InputEvent * m_pEvent;
	EPN_SoundInterface * m_pEPN_SI;

private:
	EPN_Pos m_Pos;
	EPN_Pos m_CirclePos;
	EPN_Rect m_CircleRect;
	EPN_Pos m_MousePos;

	EPN_Pos m_DownMousePos;
	EPN_Pos m_DownCirclePos;

	bool m_IsClicked;
	bool m_VolumeType;			   //true = BGM false = Effect

public:
	void SetPos(EPN_Pos Pos);
	void SetVolumeType(bool Type); //true = BGM false = Effect

public:
	bool Run();
};

#endif // !__SCROLL_BAR_H__