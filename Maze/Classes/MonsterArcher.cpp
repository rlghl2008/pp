#include "MonsterArcher.h"
#include "EnemyBullet.h"

MonsterArcher::MonsterArcher()
{
}

MonsterArcher::MonsterArcher(EPN_Pos Pos) :
	m_ShootDelay(180),
	m_ShootDelayCount(0),
	m_ShootFlag(false)
{
	MonsterBase::MonsterBase();

	m_pObjectMgr = ObjectManager::CreateInstance();
	m_MonsterType = MONSTER_TYPE::ARCHER;

	m_TextureName = "MonsterArcher";
	m_ObjectInfo.Atk = 20;
	m_ObjectInfo.Hp = 50;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;
	m_ObjectInfo.Speed = 1.0f;
	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	m_ObjectInfo.CollisionRect = EPN_Rect(5, 5, 27, 43);
	m_ObjectInfo.SensorRect = EPN_Rect(-100, -100, 100, 100);
	m_ObjectInfo.Pos = Pos;
	m_OriginSpeed = m_ObjectInfo.Speed;

	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 12, 15));	//WalkUp
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 3));	//WalkDown
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 4, 7));	//SideWalk
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 0));	//Idle
	m_Animation.SetCurrentAni(ANIMATION_TYPE::WALK_DOWN);
}


MonsterArcher::~MonsterArcher()
{
}

void MonsterArcher::IsSensed(ObjectBase * pObject)
{
	if (pObject->GetObjectInfo()->ObjectType == OBJECT_TYPE::CHRACTER)
		m_ShootFlag = true;
}

void MonsterArcher::IsCollided(ObjectBase * pObject)
{
	MonsterBase::IsCollided(pObject);
}

bool MonsterArcher::Run()
{
	if (m_ObjectInfo.IsEnable == true)
	{
		if (m_ShootFlag == true)
		{
			if (m_ShootDelayCount >= m_ShootDelay)
			{
				m_ShootFlag = false;
				m_ShootDelayCount = 0;

				EPN_Pos PlayerPos = m_pCharacter->GetObjectInfo()->Pos;
				int ShootAngle = EPN_GetPosAngle(m_ObjectInfo.Pos, PlayerPos);
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle, ENEMYBULLET_TYPE::ARCHER));
			}
			m_IsArcherShooting = false;
			m_Animation.SetCurrentAni(ANIMATION_TYPE::IDLE);
		}
		else
		{
			m_IsArcherShooting = true;
		}

		m_ShootDelayCount++;
	}

	MonsterBase::Run();

	return true;
}
