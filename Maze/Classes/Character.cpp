#include "Character.h"
#include "Item.h"
#include "Bullet.h"

Character::Character() :
	m_CharacterType(CHARACTER_TYPE::NORMAL),
	m_TypeChangeFlag(true),
	m_InvincibleFlag(false),
	m_InvincibleCnt(0),
	m_InvincibleTime(70),
	m_BlinkFlag(true),
	m_BlinkCnt(0),
	m_BlinkDelay(5),
	m_Alpha(255),
	m_BulletDelay(10),
	m_BulletDelayCnt(0),
	m_ShootEnbleFlag(true)
{

	m_ObjectInfo.ObjectType = OBJECT_TYPE::CHRACTER;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::PLAYER;

	m_ObjectInfo.Pos = EPN_Pos(120, 110);
	m_ObjectInfo.Speed = 2.5f;
	m_ObjectInfo.Hp = 100;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;
	m_ObjectInfo.Atk = 10.f;
	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.CollisionRect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	
	m_Direction = CHARACTER_DIRECTION::DOWN;
	m_ReversalFlag = false;

	/* 각 걷는 방향들의 애니매이션 추가 */
	m_Animation.AddAnimationData(new AnimationInfo("MizNormalStand", EPN_Pos(32, 48), 20, false, true, true, 0, 3));	//MIZ_NORMAL_STAND
	m_Animation.AddAnimationData(new AnimationInfo("MizNormalWalkUp", EPN_Pos(32, 48), 3, false, true, true, 0, 7));	//MIZ_NORMAL_WALK_UP
	m_Animation.AddAnimationData(new AnimationInfo("MizNormalWalkDown", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_NORMAL_WALK_DOWN
	m_Animation.AddAnimationData(new AnimationInfo("MizNormalSideWalk", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_NORMAL_SIDE_WALK

	m_Animation.AddAnimationData(new AnimationInfo("MizFireStand", EPN_Pos(32, 48), 20, false, true, true, 0, 3));		//MIZ_FIRE_STAND
	m_Animation.AddAnimationData(new AnimationInfo("MizFireWalkUp", EPN_Pos(32, 48), 5, false, true, true, 0, 7));		//MIZ_FIRE_WALK_UP
	m_Animation.AddAnimationData(new AnimationInfo("MizFireWalkDown", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_FIRE_WALK_DOWN
	m_Animation.AddAnimationData(new AnimationInfo("MizFireSideWalk", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_FIRE_SIDE_WALK

	m_Animation.AddAnimationData(new AnimationInfo("MizIceStand", EPN_Pos(32, 48), 20, false, true, true, 0, 3));		//MIZ_ICE_STAND
	m_Animation.AddAnimationData(new AnimationInfo("MizIceWalkUp", EPN_Pos(32, 48), 5, false, true, true, 0, 7));		//MIZ_ICE_WALK_UP
	m_Animation.AddAnimationData(new AnimationInfo("MizIceWalkDown", EPN_Pos(32, 48), 5, false, true, true, 0, 7));		//MIZ_ICE_WALK_DOWN
	m_Animation.AddAnimationData(new AnimationInfo("MizIceSideWalk", EPN_Pos(32, 48), 5, false, true, true, 0, 7));		//MIZ_ICE_SIDE_WALK

	m_Animation.AddAnimationData(new AnimationInfo("MizThunderStand", EPN_Pos(32, 48), 20, false, true, true, 0, 3));	//MIZ_THUNDER_STAND
	m_Animation.AddAnimationData(new AnimationInfo("MizThunderWalkUp", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_THUNDER_WALK_UP
	m_Animation.AddAnimationData(new AnimationInfo("MizThunderWalkDown", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_THUNDER_WALK_DOWN
	m_Animation.AddAnimationData(new AnimationInfo("MizThunderSideWalk", EPN_Pos(32, 48), 5, false, true, true, 0, 7));	//MIZ_THUNDER_SIDE_WALK

	m_Animation.SetCurrentAni(ANIMATION_TYPE::MIZ_NORMAL_STAND);
}

Character::~Character()
{
}

void Character::CharacterTypeChange()
{
	//캐릭터 타입(속성)
	switch (m_CharacterType)
	{
	case CHARACTER_TYPE::NORMAL:
		m_AnimationType[MOVEMENT_TYPE::STAND] = ANIMATION_TYPE::MIZ_NORMAL_STAND;
		m_AnimationType[MOVEMENT_TYPE::WALK_UP] = ANIMATION_TYPE::MIZ_NORMAL_WALK_UP;
		m_AnimationType[MOVEMENT_TYPE::WALK_DOWN] = ANIMATION_TYPE::MIZ_NORMAL_WALK_DOWN;
		m_AnimationType[MOVEMENT_TYPE::SIDE_WALK] = ANIMATION_TYPE::MIZ_NORMAL_SIDE_WALK;
		m_ObjectInfo.Atk = 10.f + (m_ObjectInfo.Atk - 10.f);
		break;
	case CHARACTER_TYPE::FIRE:
		m_AnimationType[MOVEMENT_TYPE::STAND] = ANIMATION_TYPE::MIZ_FIRE_STAND;
		m_AnimationType[MOVEMENT_TYPE::WALK_UP] = ANIMATION_TYPE::MIZ_FIRE_WALK_UP;
		m_AnimationType[MOVEMENT_TYPE::WALK_DOWN] = ANIMATION_TYPE::MIZ_FIRE_WALK_DOWN;
		m_AnimationType[MOVEMENT_TYPE::SIDE_WALK] = ANIMATION_TYPE::MIZ_FIRE_SIDE_WALK;
		m_ObjectInfo.Atk = 10.f + (m_ObjectInfo.Atk - 10.f);;
		break;
	case CHARACTER_TYPE::ICE:
		m_AnimationType[MOVEMENT_TYPE::STAND] = ANIMATION_TYPE::MIZ_ICE_STAND;
		m_AnimationType[MOVEMENT_TYPE::WALK_UP] = ANIMATION_TYPE::MIZ_ICE_WALK_UP;
		m_AnimationType[MOVEMENT_TYPE::WALK_DOWN] = ANIMATION_TYPE::MIZ_ICE_WALK_DOWN;
		m_AnimationType[MOVEMENT_TYPE::SIDE_WALK] = ANIMATION_TYPE::MIZ_ICE_SIDE_WALK;
		m_ObjectInfo.Atk = 10.f + (m_ObjectInfo.Atk - 10.f);;
		break;
	case CHARACTER_TYPE::THUNDER:
		m_AnimationType[MOVEMENT_TYPE::STAND] = ANIMATION_TYPE::MIZ_THUNDER_STAND;
		m_AnimationType[MOVEMENT_TYPE::WALK_UP] = ANIMATION_TYPE::MIZ_THUNDER_WALK_UP;
		m_AnimationType[MOVEMENT_TYPE::WALK_DOWN] = ANIMATION_TYPE::MIZ_THUNDER_WALK_DOWN;
		m_AnimationType[MOVEMENT_TYPE::SIDE_WALK] = ANIMATION_TYPE::MIZ_THUNDER_SIDE_WALK;
		m_ObjectInfo.Atk = 15.f + (m_ObjectInfo.Atk - 15.f);;
		break;
	}
	m_TypeChangeFlag = false;
}

CHARACTER_TYPE Character::GetCharacterType()
{
	return m_CharacterType;
}

CHARACTER_DIRECTION Character::GetDirection()
{
	return m_Direction;
}

void Character::SetCharacterType(CHARACTER_TYPE CharacterType)
{
	m_CharacterType = CharacterType;
	m_TypeChangeFlag = true;
}

void Character::IsCollided(ObjectBase * pObject)
{
	//무적 상태이면 오브젝트간 충돌처리 X
	if (m_InvincibleFlag && pObject->GetObjectInfo()->ObjectType != OBJECT_TYPE::ITEM)
		return;

	ObjectInfo * pObjectInfo = pObject->GetObjectInfo();

	//충돌한 오브젝트가 적일때
	if (pObjectInfo->TeamFlag == TEAM_FLAG::ENEMY)
	{
		switch (pObjectInfo->ObjectType)
		{
		case OBJECT_TYPE::MONSTER:
			m_ObjectInfo.Hp -= pObjectInfo->Atk;
			if (m_ObjectInfo.Hp <= 0)
			{
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsDied = true;
			}
			m_InvincibleFlag = true;
			break;
		case OBJECT_TYPE::BULLET:
			m_ObjectInfo.Hp -= pObjectInfo->Atk;
			if (m_ObjectInfo.Hp <= 0)
			{
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsDied = true;
			}
			m_InvincibleFlag = true;
			pObjectInfo->IsEnable = false;
			break;
		}
	}
	else if (pObjectInfo->ObjectType == OBJECT_TYPE::ITEM)
	{
		m_pEPN_SI->PlayEffect("Sound/InGame/GetItem.mp3");
		Item * pItem = (Item*)pObject;
		switch (pItem->GetItemType())
		{
		case ITEM_TYPE::RECOVERY:
			m_ObjectInfo.Hp += 30;
			if (m_ObjectInfo.Hp >= 100)
				m_ObjectInfo.Hp = 100;
			break;
		case ITEM_TYPE::POWER_UP:
			m_ObjectInfo.Atk += 1;
			if (m_ObjectInfo.Atk >= 20)
				m_ObjectInfo.Atk = 20;
			break;
		case ITEM_TYPE::SPEED_UP:
			if (m_ObjectInfo.Speed <= 5)
				m_ObjectInfo.Speed += 0.25f;
			break;
		case ITEM_TYPE::FIRE:
			m_CharacterType = CHARACTER_TYPE::FIRE;
			m_TypeChangeFlag = true;
			break;
		case ITEM_TYPE::ICE:
			m_CharacterType = CHARACTER_TYPE::ICE;
			m_TypeChangeFlag = true;
			break;
		case ITEM_TYPE::THUNDER:
			m_CharacterType = CHARACTER_TYPE::THUNDER;
			m_TypeChangeFlag = true;
			break;
		}
		pObjectInfo->IsDied = true;
	}
}

void Character::IsSensed(ObjectBase * pObject)
{
}

bool Character::Run()
{
	m_Animation.Run();

	//캐릭터 속성 변경
	if (m_TypeChangeFlag)
		CharacterTypeChange();

	//무적 상태카운트
	if (m_InvincibleFlag)
	{
		if (++m_BlinkCnt >= m_BlinkDelay)
		{
			m_BlinkCnt = 0;

			if (m_BlinkFlag)
			{
				m_Alpha = 100.f;
				m_BlinkFlag = false;
			}
			else
			{
				m_Alpha = 255.f;
				m_BlinkFlag = true;
			}
		}
		if (++m_InvincibleCnt >= m_InvincibleTime)
		{
			m_InvincibleCnt = 0;
			m_InvincibleFlag = false;
		}
	}

	//총알 발사 가능 상태 계산
	if (!m_ShootEnbleFlag)
	{
		if (++m_BulletDelayCnt >= m_BulletDelay)
			m_ShootEnbleFlag = true;
	}
	//총알 발사
	if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_SPACE) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_SPACE) == EPN_INPUT_STATE::DOWNING)
	{
		if (m_ShootEnbleFlag)
		{
			ObjectManager::CreateInstance()->AddObject(new Bullet());
			m_ShootEnbleFlag = false;
			m_BulletDelayCnt = 0;
		}
	}

	//충돌용 이전방향 저장(내가 이동하기전 어느쪽으로 이동했는지 알기위해서 저장)
	m_PrevPos = m_ObjectInfo.Pos;

	/* 이동 */
	if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_DOWN) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_DOWN) == EPN_INPUT_STATE::DOWNING)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::WALK_DOWN]);
		m_Direction = CHARACTER_DIRECTION::DOWN;
		m_ReversalFlag = false;	
		m_ObjectInfo.Pos.Y -= m_ObjectInfo.Speed;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_DOWN) == EPN_INPUT_STATE::UP)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::STAND]);
		m_ReversalFlag = false;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_UP) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_UP) == EPN_INPUT_STATE::DOWNING)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::WALK_UP]);
		m_Direction = CHARACTER_DIRECTION::UP;
		m_ReversalFlag = false;
		m_ObjectInfo.Pos.Y += m_ObjectInfo.Speed;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_UP) == EPN_INPUT_STATE::UP)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::STAND]);
		m_ReversalFlag = false;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_LEFT) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_LEFT) == EPN_INPUT_STATE::DOWNING)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::SIDE_WALK]);
		m_Direction = CHARACTER_DIRECTION::LEFT;
		m_ReversalFlag = true;
		m_ObjectInfo.Pos.X -= m_ObjectInfo.Speed;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_LEFT) == EPN_INPUT_STATE::UP)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::STAND]);
		m_ReversalFlag = false;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_RIGHT) == EPN_INPUT_STATE::DOWN || m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_RIGHT) == EPN_INPUT_STATE::DOWNING)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::SIDE_WALK]);
		m_Direction = CHARACTER_DIRECTION::RIGHT;
		m_ReversalFlag = false;
		m_ObjectInfo.Pos.X += m_ObjectInfo.Speed;
	}
	else if (m_pEvent->IF_KEY_STATE(EPN_KEY_CODE::KEY_ARROW_RIGHT) == EPN_INPUT_STATE::UP)
	{
		m_Animation.SetCurrentAni(m_AnimationType[MOVEMENT_TYPE::STAND]);
		m_ReversalFlag = false;
	}
	
	//맵 충돌 처리
	ObjectBase::Run();




	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(), 
		EPN_ARGB(m_Alpha, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f), m_ReversalFlag);




	return true;
}