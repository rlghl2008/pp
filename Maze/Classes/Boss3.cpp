#include "Boss3.h"
#include "ObjectManager.h"
#include "EnemyBullet.h"
#include "Bullet.h"
#include "Item.h"

Boss3::Boss3()
{
	estring TextureName = "Boss3";
	EPN_Pos Pos = EPN_Pos(384, 460);

	m_pCharacter = ObjectManager::CreateInstance()->GetCharacter();

	m_Pattern = PATTERN_NORMAL;
	m_ShootDelayCnt = 0;
	m_PatternTimeCnt = 1000;
	m_ShootAngle = 0;

	m_Alpha = 255;

	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	m_ObjectInfo.CollisionRect = EPN_Rect(5, 5, 27, 43);
	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.Hp = 1500;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;

	m_Animation.AddAnimationData(new AnimationInfo(TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 0));	//WalkUp
	m_Animation.SetCurrentAni(0);
}

Boss3::~Boss3()
{
	m_ObjectInfo.ObjectType = OBJECT_TYPE::MONSTER;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::ENEMY;
}

void Boss3::IsCollided(ObjectBase * pObject)
{
	ObjectInfo * pObjectInfo = pObject->GetObjectInfo();

	//충돌한 객체가 상대팀 이었을 때
	if (pObjectInfo->TeamFlag == TEAM_FLAG::PLAYER)
	{
		Bullet * CurBullet = (Bullet*)pObject;
		switch (CurBullet->GetBulletType())
		{
		case BULLET_TYPE::NORMAL:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackNormal.mp3");
			break;
		case BULLET_TYPE::FIRE:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackFire.mp3");
			break;
		case BULLET_TYPE::ICE:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackIce.mp3");
			break;
		case BULLET_TYPE::THUNDER:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackThunder.mp3");
			break;
		}
		if (pObjectInfo->ObjectType == OBJECT_TYPE::BULLET)
		{
			pObject->GetObjectInfo()->IsEnable = false;	//총알이면 삭제
			m_ObjectInfo.Hp -= m_pCharacter->GetObjectInfo()->Atk;

			if (m_ObjectInfo.Hp <= 0)						//HP가 0이하가 되면 활동 플래그 false
			{
				m_pEPN_SI->PlayEffect("Sound/InGame/Die.mp3");
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsEnable = false;
			}
		}
	}
}

bool Boss3::Run()
{
	ObjectManager *pObjectMgr = ObjectManager::CreateInstance();

	if (!m_ObjectInfo.IsEnable)
	{
		m_Alpha -= 3.5f;
		if (m_Alpha <= 0)
		{
			m_Alpha = 0;
			m_ObjectInfo.IsDied = true;
		}
	}
	{
		switch (m_Pattern)
		{
		case PATTERN_NORMAL:
			if (m_PatternTimeCnt++ >= 210)
			{
				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK1;
			}
			break;
		case PATTERN_ATTACK1:
			if (m_ShootDelayCnt++ >= 25)
			{
				m_ShootDelayCnt = 0;
				pObjectMgr->AddObject(new EnemyBullet(this, m_ShootAngle + 180, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, m_ShootAngle, ENEMYBULLET_TYPE::BOSS));
				m_ShootAngle += 20;
			}
			if (m_ShootAngle >= 360)
			{
				m_ShootDelayCnt = 0;
				m_ShootAngle = 0;
				m_Pattern = PATTERN_ATTACK2;
			}
			break;
		case PATTERN_ATTACK2:
			if (m_PatternTimeCnt++ >= 60)
			{
				pObjectMgr->AddObject(new EnemyBullet(this, 0, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 90, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 180, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 270, ENEMYBULLET_TYPE::BOSS));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK3;
			}
			break;
		case PATTERN_ATTACK3:
			if (m_PatternTimeCnt++ >= 80)
			{
				pObjectMgr->AddObject(new EnemyBullet(this, 0, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 90, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 180, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 270, ENEMYBULLET_TYPE::FAKEBULLET));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK4;
			}
			break;
		case PATTERN_ATTACK4:
			if (m_PatternTimeCnt++ >= 120)
			{
				pObjectMgr->AddObject(new EnemyBullet(this, 45, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 135, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 225, ENEMYBULLET_TYPE::FAKEBULLET));
				pObjectMgr->AddObject(new EnemyBullet(this, 315, ENEMYBULLET_TYPE::FAKEBULLET));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK5;
			}
			break;
		case PATTERN_ATTACK5:
			if (m_PatternTimeCnt++ >= 90)
			{
				pObjectMgr->AddObject(new EnemyBullet(this, 0, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 40, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 80, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 120, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 160, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 200, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 240, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 280, ENEMYBULLET_TYPE::BOSS));
				pObjectMgr->AddObject(new EnemyBullet(this, 320, ENEMYBULLET_TYPE::BOSS));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_NORMAL;
			}
			break;
		}
		m_Animation.Run();
	}
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(), EPN_ARGB(m_Alpha, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f), false);
	m_pEPN_TI->Blt("HpBar", 0, EPN_Pos(m_ObjectInfo.Pos.X + 5, m_ObjectInfo.Pos.Y + 2), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(m_ObjectInfo.Hp / m_ObjectInfo.MaxHp, 1.f));

	return true;
}