#ifndef __PATH_NODE_H__
#define __PATH_NODE_H__

#include <math.h>
#include "../Enpine/Common/Base/CommonStructure.h"


struct PathNode
{
public:
	EPN_Pos NodePos;
	int CostG;			//부모 노드로부터 현재 노드까지의 거리
	int CostH;			//현재 노드부터 도착지까지의 거리
	int CostF;			//G + H

	PathNode* pParentNode;

	PathNode()
	{
		CostG = 0;
		CostH = 0;
		CostF = 0;
		pParentNode = nullptr;
	}

	PathNode(EPN_Pos NodePos, int CostG, EPN_Pos DestPos, PathNode* pParentNode)
	{
		this->NodePos = NodePos;
		this->CostG = CostG;
		this->CostH = abs(DestPos.X - NodePos.X) + abs(DestPos.Y - NodePos.Y);
		this->CostF = CostG + CostH;
		this->pParentNode = pParentNode;
	}
};
#endif