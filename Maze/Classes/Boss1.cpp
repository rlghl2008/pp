#include "Boss1.h"
#include "ObjectManager.h"
#include "EnemyBullet.h"
#include "Bullet.h"
#include "Item.h"

Boss1::Boss1()
{
	m_pObjectMgr = ObjectManager::CreateInstance();

	//임시
	estring TextureName = "Boss1";
	EPN_Pos Pos = EPN_Pos(380, 400);
	
	m_pCharacter = ObjectManager::CreateInstance()->GetCharacter();

	m_Pattern = PATTERN_NORMAL;
	m_PatternTimeCnt = 0;
	m_ShootTimeCnt = 0;

	m_Alpha = 255;

	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	m_ObjectInfo.CollisionRect = EPN_Rect(5, 5, 27, 43);
	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.Hp = 1000;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;

	m_Animation.AddAnimationData(new AnimationInfo(TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 0));	//WalkUp
	m_Animation.SetCurrentAni(0);
}

Boss1::~Boss1()
{
	m_ObjectInfo.ObjectType = OBJECT_TYPE::MONSTER;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::ENEMY;
}

void Boss1::IsCollided(ObjectBase * pObject)
{
	ObjectInfo * pObjectInfo = pObject->GetObjectInfo();

	//충돌한 객체가 상대팀 이었을 때
	if (pObjectInfo->TeamFlag == TEAM_FLAG::PLAYER)
	{
		Bullet * CurBullet = (Bullet*)pObject;
		switch (CurBullet->GetBulletType())
		{
		case BULLET_TYPE::NORMAL:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackNormal.mp3");
			break;
		case BULLET_TYPE::FIRE:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackFire.mp3");
			break;
		case BULLET_TYPE::ICE:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackIce.mp3");
			break;
		case BULLET_TYPE::THUNDER:
			m_pEPN_SI->PlayEffect("Sound/InGame/AttackThunder.mp3");
			break;
		}
		if (pObjectInfo->ObjectType == OBJECT_TYPE::BULLET)
		{
			pObject->GetObjectInfo()->IsEnable = false;	//총알이면 삭제
			m_ObjectInfo.Hp -= m_pCharacter->GetObjectInfo()->Atk;

			if (m_ObjectInfo.Hp <= 0)						//HP가 0이하가 되면 활동 플래그 false
			{
				m_pEPN_SI->PlayEffect("Sound/InGame/Die.mp3");
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsEnable = false;
			}
		}
	}
}

bool Boss1::Run()
{

	if (!m_ObjectInfo.IsEnable)
	{
		m_Alpha -= 3.5f;
		if (m_Alpha <= 0)
		{
			m_Alpha = 0;
			m_ObjectInfo.IsDied = true;
			m_pObjectMgr->AddObject(new Item(m_ObjectInfo.Pos));
		}
	}
	else
	{
		switch (m_Pattern)
		{
		case PATTERN_NORMAL:
			if (m_PatternTimeCnt++ >= 170)
			{
				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK1;
			}
			break;
		case PATTERN_ATTACK1:
			if (m_PatternTimeCnt++ >= 150)
			{
				m_pObjectMgr->AddObject(new EnemyBullet(this, 0, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 90, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 180, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 270, ENEMYBULLET_TYPE::BOSS));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK2;
			}
			break;
		case PATTERN_ATTACK2:
			if (m_PatternTimeCnt++ >= 60)
			{
				m_pObjectMgr->AddObject(new EnemyBullet(this, 45, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 135, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 225, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, 315, ENEMYBULLET_TYPE::BOSS));

				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_ATTACK3;
			}
		case PATTERN_ATTACK3:
			if (m_PatternTimeCnt++ >= 70)
			{
				EPN_Pos PlayerPos = m_pObjectMgr->GetCharacter()->GetObjectInfo()->Pos;
				int ShootAngle = EPN_GetPosAngle(m_ObjectInfo.Pos, PlayerPos);
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle + 25, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle - 25, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle + 50, ENEMYBULLET_TYPE::BOSS));
				m_pObjectMgr->AddObject(new EnemyBullet(this, ShootAngle - 50, ENEMYBULLET_TYPE::BOSS));
				m_PatternTimeCnt = 0;
				m_Pattern = PATTERN_NORMAL;
			}
			break;
		}
		m_Animation.Run();
	}
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(), EPN_ARGB(m_Alpha, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f), false);
	m_pEPN_TI->Blt("HpBar", 0, EPN_Pos(m_ObjectInfo.Pos.X + 5, m_ObjectInfo.Pos.Y + 2), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(m_ObjectInfo.Hp / m_ObjectInfo.MaxHp, 1.f));

	return true;
}