#include "ViewMenu.h"

ViewMenu::ViewMenu() :
	m_B2World(NULL_PTR),
	m_IsBtnActived(false)
{
	float MidPosX = 400 - (205.f * 1.3f / 2);

	InitB2World();

	m_pEPN_SI->SetBgmVolume(0);
	m_pEPN_SI->SetEffectVolume(0);


	//스타트 버튼
	m_Button[BUTTON_TYPE::START].SetPos(EPN_Pos(MidPosX, 360));
	m_Button[BUTTON_TYPE::START].SetTexture("BtnStart", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::START].SetRect(EPN_Rect(17, 0, 183, 50));
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::START].SetScaling(EPN_Scaling(1.3f, 1.3f));

	//게임방법 버튼
	m_Button[BUTTON_TYPE::GUIDE].SetPos(EPN_Pos(MidPosX, 280));
	m_Button[BUTTON_TYPE::GUIDE].SetTexture("BtnGuide", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::GUIDE].SetRect(EPN_Rect(15, 0, 185, 50));
	m_Button[BUTTON_TYPE::GUIDE].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::GUIDE].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::GUIDE].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::GUIDE].SetScaling(EPN_Scaling(1.3f, 1.3f));

	//게임스토리 버튼
	m_Button[BUTTON_TYPE::STORY].SetPos(EPN_Pos(MidPosX, 200));
	m_Button[BUTTON_TYPE::STORY].SetTexture("BtnStory", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::STORY].SetRect(EPN_Rect(12, 0, 188, 50));
	m_Button[BUTTON_TYPE::STORY].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::STORY].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::STORY].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::STORY].SetScaling(EPN_Scaling(1.3f, 1.3f));

	//게임종료 버튼
	m_Button[BUTTON_TYPE::EXIT].SetPos(EPN_Pos(MidPosX, 120));
	m_Button[BUTTON_TYPE::EXIT].SetTexture("BtnExit", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::EXIT].SetRect(EPN_Rect(36, 0, 164, 50));
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::EXIT].SetScaling(EPN_Scaling(1.3f, 1.3f));
}

ViewMenu::~ViewMenu()
{
	if (m_B2World != nullptr)
		delete m_B2World;
}

void ViewMenu::BounceTitle()
{
	m_B2World->Step(1.f / 60.f, 8, 3);

	m_TitlePos[TITLE_SPELL::M] = EPN_Pos(m_TitleBody[TITLE_SPELL::M]->GetPosition().x * PTM_RATIO, m_TitleBody[TITLE_SPELL::M]->GetPosition().y * PTM_RATIO);
	m_TitlePos[TITLE_SPELL::A] = EPN_Pos(m_TitleBody[TITLE_SPELL::A]->GetPosition().x * PTM_RATIO, m_TitleBody[TITLE_SPELL::A]->GetPosition().y * PTM_RATIO);
	m_TitlePos[TITLE_SPELL::Z] = EPN_Pos(m_TitleBody[TITLE_SPELL::Z]->GetPosition().x * PTM_RATIO, m_TitleBody[TITLE_SPELL::Z]->GetPosition().y * PTM_RATIO);
	m_TitlePos[TITLE_SPELL::E] = EPN_Pos(m_TitleBody[TITLE_SPELL::E]->GetPosition().x * PTM_RATIO, m_TitleBody[TITLE_SPELL::E]->GetPosition().y * PTM_RATIO);

	m_pEPN_TI->Blt("MenuTitle", 0, m_TitlePos[TITLE_SPELL::M], EPN_Rect(59, 0, 226, 135), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f));
	m_pEPN_TI->Blt("MenuTitle", 0, m_TitlePos[TITLE_SPELL::A], EPN_Rect(226, 0, 314, 135), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f));
	m_pEPN_TI->Blt("MenuTitle", 0, m_TitlePos[TITLE_SPELL::Z], EPN_Rect(314, 0, 373, 135), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f));
	m_pEPN_TI->Blt("MenuTitle", 0, m_TitlePos[TITLE_SPELL::E], EPN_Rect(373, 0, 440, 135), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f));
}

void ViewMenu::InitB2World()
{
	//월드 중력설정
	b2Vec2 Gravity = b2Vec2(0.f, -9.8f);
	m_B2World = new b2World(Gravity);

	b2BodyDef GroundBodyDef;
	GroundBodyDef.position.Set(0.f, 480.f / PTM_RATIO);
	b2Body *GroundBody = m_B2World->CreateBody(&GroundBodyDef);
	b2PolygonShape GroundBox;
	GroundBox.SetAsBox(800.f / PTM_RATIO, 0.f);
	GroundBody->CreateFixture(&GroundBox, 0.f);

	SetTitleBody(TITLE_SPELL::M, EPN_Pos(204.f, 700.f), EPN_Pos(167.f, 135.f));
	SetTitleBody(TITLE_SPELL::A, EPN_Pos(371.f, 800.f), EPN_Pos(89.f, 135.f));
	SetTitleBody(TITLE_SPELL::Z, EPN_Pos(461.f, 830.f), EPN_Pos(59.f, 135.f));
	SetTitleBody(TITLE_SPELL::E, EPN_Pos(519.f, 860.f), EPN_Pos(59.f, 135.f));
}

void ViewMenu::SetTitleBody(TITLE_SPELL Spell, EPN_Pos Pos, EPN_Pos BoxSize)
{
	b2BodyDef TitleBodyDef;
	TitleBodyDef.type = b2_dynamicBody;
	TitleBodyDef.position.Set(Pos.X / PTM_RATIO, Pos.Y / PTM_RATIO);
	m_TitleBody[Spell] = m_B2World->CreateBody(&TitleBodyDef);

	b2PolygonShape TitlePolygon;
	TitlePolygon.SetAsBox(BoxSize.X / 2 / PTM_RATIO, BoxSize.Y / 2 / PTM_RATIO);

	b2FixtureDef TitleFixtureDef;
	TitleFixtureDef.shape = &TitlePolygon;
	TitleFixtureDef.restitution = 0.4f;

	m_TitleBody[Spell]->CreateFixture(&TitleFixtureDef);
}

void ViewMenu::SoundButton()
{
	//각 버튼을 눌렀을 시
	if (m_Button[BUTTON_TYPE::START].GetState() == BUTTON_STATE::ACTIVE ||
		m_Button[BUTTON_TYPE::GUIDE].GetState() == BUTTON_STATE::ACTIVE ||
		m_Button[BUTTON_TYPE::STORY].GetState() == BUTTON_STATE::ACTIVE ||
		m_Button[BUTTON_TYPE::EXIT].GetState() == BUTTON_STATE::ACTIVE)
	{
		if (m_IsBtnActived == false)
			m_pEPN_SI->PlayEffect("Sound/UI/ButtonActived.mp3");
		m_IsBtnActived = true;
	}
	else
		m_IsBtnActived = false;

	//각 버튼을 눌렀을 시
	if (m_Button[BUTTON_TYPE::START].GetState() == BUTTON_STATE::POP ||
		m_Button[BUTTON_TYPE::GUIDE].GetState() == BUTTON_STATE::POP ||
		m_Button[BUTTON_TYPE::STORY].GetState() == BUTTON_STATE::POP ||
		m_Button[BUTTON_TYPE::EXIT].GetState() == BUTTON_STATE::POP)
	{
		m_pEPN_SI->PlayEffect("Sound/UI/ButtonClicked.mp3");
	}
}

VIEW_STATE ViewMenu::Run()
{
	m_pEPN_TI->Blt("MenuBg", 0, EPN_Pos(0, 600), EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(1.f, 1.f));

	//타이틀 튕기는 효과
	BounceTitle();

	//버튼 동작
	m_Button[BUTTON_TYPE::START].Run();
	m_Button[BUTTON_TYPE::GUIDE].Run();
	m_Button[BUTTON_TYPE::STORY].Run();
	m_Button[BUTTON_TYPE::EXIT].Run();

	//버튼 소리
	SoundButton();

	//각 버튼을 눌렀을 시
	if (m_Button[BUTTON_TYPE::START].GetState() == BUTTON_STATE::POP)
		return VIEW_STATE::INGAME;
	else if (m_Button[BUTTON_TYPE::GUIDE].GetState() == BUTTON_STATE::POP)
		return VIEW_STATE::GUIDE;
	else if (m_Button[BUTTON_TYPE::STORY].GetState() == BUTTON_STATE::POP)
		return VIEW_STATE::STORY;
	else if (m_Button[BUTTON_TYPE::EXIT].GetState() == BUTTON_STATE::POP)
		exit(0);
	return VIEW_STATE::MENU;
}