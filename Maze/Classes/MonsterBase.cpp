#include "MonsterBase.h"
#include "Item.h"
#include "Bullet.h"
#include <cstdlib>
#include <ctime>


MonsterBase::MonsterBase() :
	m_DirChangeFlag(true),
	m_MoveXFlag(true),
	m_ColisionFlag(false),
	m_ReversalFlag(false),
	m_AnimChangeFlag(true),
	m_DirChangeCnt(0),
	m_DirChangeDelay(30),
	m_Alpha(255.f),
	m_FreezeFlag(false),
	m_FreezeTime(80),
	m_FreezeTimeCnt(0),
	m_BurnFlag(false),
	m_BurnTime(80),
	m_BurnTimeCnt(0),
	m_IsArcherShooting(true),
	m_HealDelayTime(180),
	m_HeadDelayCount(0),
	m_PathIndex(0),
	m_PathFindDelay(180),
	m_PathFindDelayCount(m_PathFindDelay),
	m_IsPathFinished(true)
{
	m_ObjectInfo.ObjectType = OBJECT_TYPE::MONSTER;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::ENEMY;

	m_PathFinder.SetColisionData(MapManager::CreateInstance()->GetCurMapInfo()->ColisionData);
	m_pObjectManager = ObjectManager::CreateInstance();
	m_pCharacter = m_pObjectManager->GetCharacter();

}

MonsterBase::MonsterBase(MONSTER_TYPE MonsterType, EPN_Pos Pos)
{
}

MonsterBase::~MonsterBase()
{
}

void MonsterBase::IsSensed(ObjectBase * pObject)
{
}

void MonsterBase::IsCollided(ObjectBase * pObject)
{
	ObjectInfo * pObjectInfo = pObject->GetObjectInfo();

	//충돌한 객체가 상대팀 이었을 때
	if (pObjectInfo->TeamFlag == TEAM_FLAG::PLAYER)
	{
		if (pObjectInfo->ObjectType == OBJECT_TYPE::BULLET)
		{
			Bullet * CurBullet = (Bullet*)pObject;
			switch (CurBullet->GetBulletType())
			{
			case BULLET_TYPE::NORMAL:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackNormal.mp3");
				break;
			case BULLET_TYPE::FIRE:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackFire.mp3");
				m_BurnFlag = true;
				break;
			case BULLET_TYPE::ICE:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackIce.mp3");
				m_FreezeFlag = true;
				break;
			case BULLET_TYPE::THUNDER:
				m_pEPN_SI->PlayEffect("Sound/InGame/AttackThunder.mp3");
				break;
			}
			pObject->GetObjectInfo()->IsEnable = false;	//총알이면 삭제
			m_ObjectInfo.Hp -= m_pCharacter->GetObjectInfo()->Atk;

			if (m_ObjectInfo.Hp <= 0)						//HP가 0이하가 되면 활동 플래그 false
			{
				m_pEPN_SI->PlayEffect("Sound/InGame/Die.mp3");
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsEnable = false;
			}
		}
	}
}

void MonsterBase::FindPath()
{
	if (m_PathFindDelayCount++ > m_PathFindDelay && m_IsPathFinished == true)
	{
		m_PathFindDelayCount = 0;
		m_PathIndex = 0;

		EPN_Pos StartPos = EPN_Pos(m_ObjectInfo.Pos.X + m_ObjectInfo.Rect.Bottom.X / 2, m_ObjectInfo.Pos.Y - m_ObjectInfo.Rect.Bottom.Y + m_ObjectInfo.FootRect.Bottom.Y);
		int StartIndexX = (int)(StartPos.X / 32);
		int StartIndexY = (int)(StartPos.Y / 32);

		ObjectInfo* CharacterInfo = m_pCharacter->GetObjectInfo();
		EPN_Pos DestPos = EPN_Pos(CharacterInfo->Pos.X + CharacterInfo->Rect.Bottom.X / 2, CharacterInfo->Pos.Y - CharacterInfo->Rect.Bottom.Y + CharacterInfo->FootRect.Bottom.Y);
		int DestIndexX = (int)(DestPos.X / 32);
		int DestIndexY = (int)(DestPos.Y / 32);

		m_PathFinder.GetPath(EPN_Pos(StartIndexX, StartIndexY), EPN_Pos(DestIndexX, DestIndexY), &m_PathQueue);
		if (m_PathQueue.size() <= 0)
			return;

		if (EPN_Pos(StartIndexX, StartIndexY) == m_PathQueue[0])
			m_PathIndex++;
	}
}

void MonsterBase::Move()
{
	if (m_PathQueue.size() <= 0)
		return;
	if (m_PathIndex > m_PathQueue.size() - 1)
		return;


	if (m_IsArcherShooting == false)
		return;

	EPN_Pos Dest = m_PathQueue[m_PathIndex] * 32;
	EPN_Pos DirVec = GetDirection(m_ObjectInfo.Pos + EPN_Pos(0, -40), Dest);
	
	m_ObjectInfo.Pos += (DirVec * m_ObjectInfo.Speed);
	m_IsPathFinished = false;

	if (DirVec == EPN_Pos(0, 1))
	{
		if (m_ObjectInfo.Pos.Y >= Dest.Y + 40)
		{
			m_ObjectInfo.Pos.Y = Dest.Y + 40;
			m_IsPathFinished = true;
			m_PathIndex++;
		}
	}
	else if(DirVec == EPN_Pos(0, -1))
	{
		if (m_ObjectInfo.Pos.Y <= Dest.Y + 40)
		{
			m_ObjectInfo.Pos.Y = Dest.Y + 40;
			m_IsPathFinished = true;
			m_PathIndex++;
		}
	}
	else if(DirVec == EPN_Pos(1, 0))
	{
		if (m_ObjectInfo.Pos.X >= Dest.X)
		{
			m_ObjectInfo.Pos.X = Dest.X;
			m_IsPathFinished = true;
			m_PathIndex++;
		}
	}
	else if(DirVec == EPN_Pos(-1, 0))
	{
		if (m_ObjectInfo.Pos.X <= Dest.X)
		{
			m_ObjectInfo.Pos.X = Dest.X;
			m_IsPathFinished = true;
			m_PathIndex++;
		}
	}

	if (m_PathIndex >= m_PathQueue.size())
		m_PathFindDelayCount = m_PathFindDelay;

	SetAniDirection(DirVec);
}

EPN_Pos MonsterBase::GetDirection(EPN_Pos Start, EPN_Pos Dest)
{
	EPN_Pos DirectionPos = Dest - Start;

	if (DirectionPos.X != 0)
	{
		if (DirectionPos.X > 0)
			return EPN_Pos(1, 0);
		else
			return EPN_Pos(-1, 0);
	}
	else
	{
		if (DirectionPos.Y > 0)
			return EPN_Pos(0, 1);
		else
			return EPN_Pos(0, -1);
	}
}

void MonsterBase::SetAniDirection(EPN_Pos Dir)
{
	ANIMATION_TYPE AniType;
	if (Dir == EPN_Pos(0, 1))
	{
		AniType = ANIMATION_TYPE::WALK_UP;
		m_ReversalFlag = false;
	}
	else if (Dir == EPN_Pos(0, -1))
	{
		AniType = ANIMATION_TYPE::WALK_DOWN;
		m_ReversalFlag = false;
	}
	else if (Dir == EPN_Pos(1, 0))
	{
		AniType = ANIMATION_TYPE::SIDE_WALK;
		m_ReversalFlag = true;
	}
	else if (Dir == EPN_Pos(-1, 0))
	{
		AniType = ANIMATION_TYPE::SIDE_WALK;
		m_ReversalFlag = false;
	}
	m_Animation.SetCurrentAni(AniType);
}

void MonsterBase::OnFreezeState()
{
	m_FreezeFlag = true;
	m_FreezeTimeCnt = 0;
}

void MonsterBase::OnBurnState()
{
	m_BurnFlag = true;
	m_BurnTimeCnt = 0;
}

void MonsterBase::SetHealState(bool State)
{
	m_IsHealed = State;
}

bool MonsterBase::GetHealState()
{
	return m_IsHealed;
}

bool MonsterBase::Run()
{
	//죽었을시 서서히 사라지고 다사리면 오브젝트 다이플래그 true
	if (m_ObjectInfo.IsEnable == false)
	{
		m_Alpha -= 3.5f;
		if (m_Alpha <= 0)
		{
			m_Alpha = 0;
			m_ObjectInfo.IsDied = true;
			if (rand() % 100 < 45)
				m_pObjectManager->AddObject(new Item(m_ObjectInfo.Pos));
		}
	}
	else
	{
		m_Animation.Run();
		m_PrevPos = m_ObjectInfo.Pos;

		if (m_HeadDelayCount++ >= m_HealDelayTime)
			m_IsHealed = false;

		//얼음 총알을 맞았을 시
		if (m_FreezeFlag)
		{
			m_ObjectInfo.Speed = 0.5f;
			m_ObjectInfo.BlendColor = EPN_ARGB(255, 0, 0, 255);
			if (++m_FreezeTimeCnt >= m_FreezeTime)
			{
				m_FreezeFlag = false;
				m_FreezeTimeCnt = 0;
				m_ObjectInfo.Speed = m_OriginSpeed;
				m_ObjectInfo.BlendColor = EPN_ARGB(255, 255, 255, 255);
			}
		}
		//불 속성 총알을 맞았을 시
		else if (m_BurnFlag)
		{
			m_ObjectInfo.Hp -= 0.15f;
			//HP가 0이하가 되면 활동 플래그 false
			if (m_ObjectInfo.Hp <= 0)							
			{
				m_pEPN_SI->PlayEffect("Sound/InGame/Die.mp3");
				m_ObjectInfo.Hp = 0;
				m_ObjectInfo.IsEnable = false;
			}
			m_ObjectInfo.BlendColor = EPN_ARGB(255, 255, 0, 0);
			if (++m_BurnTimeCnt >= m_BurnTime)
			{
				m_BurnFlag = false;
				m_BurnTimeCnt = 0;
				m_ObjectInfo.Speed = m_OriginSpeed;
				m_ObjectInfo.BlendColor = EPN_ARGB(255, 255, 255, 255);
			}
		}

		//길찾기
		FindPath();
		Move();

		//체력바
		m_pEPN_TI->Blt("HpBar", 0, EPN_Pos(m_ObjectInfo.Pos.X + 5, m_ObjectInfo.Pos.Y + 2), 
			EPN_Rect(-1, -1, -1, -1), EPN_ARGB(255, 255, 255, 255), 0.f, EPN_Scaling(m_ObjectInfo.Hp / m_ObjectInfo.MaxHp, 1.f));
	}
	
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(),
		EPN_ARGB(m_Alpha, m_ObjectInfo.BlendColor.Red, m_ObjectInfo.BlendColor.Green, m_ObjectInfo.BlendColor.Blue),
		0.f, EPN_Scaling(1.f, 1.f), m_ReversalFlag);
	return true;
}