#include "ViewWin.h"


ViewWin::ViewWin()
{
	float MidPosX = 400 - (205.f * 1.3f / 2);

	//스타트 버튼
	m_Button[BUTTON_TYPE::START].SetPos(EPN_Pos(MidPosX, 200));
	m_Button[BUTTON_TYPE::START].SetTexture("BtnReStart", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::START].SetRect(EPN_Rect(17, 0, 183, 50));
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::START].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::START].SetScaling(EPN_Scaling(1.3f, 1.3f));

	//게임종료 버튼
	m_Button[BUTTON_TYPE::EXIT].SetPos(EPN_Pos(MidPosX, 120));
	m_Button[BUTTON_TYPE::EXIT].SetTexture("BtnExit", BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::EXIT].SetRect(EPN_Rect(36, 0, 164, 50));
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(0, 0, 200, 50), BUTTON_STATE::ALL);
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(200, 0, 400, 50), BUTTON_STATE::ACTIVE);
	m_Button[BUTTON_TYPE::EXIT].SetTextureRect(EPN_Rect(400, 0, 600, 50), BUTTON_STATE::PUSH);
	m_Button[BUTTON_TYPE::EXIT].SetScaling(EPN_Scaling(1.3f, 1.3f));

	m_Animation.AddAnimationData(new AnimationInfo("ViewWin", EPN_Pos(800, 600), 60, true, false, false, 0, 3));
	m_Animation.SetCurrentAni(0);

	m_pEPN_SI->PlayBgm("Sound/WinLose/Win.mp3", true);
}

ViewWin::~ViewWin()
{
	m_pEPN_SI->StopBgm();
}

VIEW_STATE ViewWin::Run()
{
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, EPN_Pos(0, 600), m_Animation.GetCurAniActionRect());

	m_Button[BUTTON_TYPE::START].Run();
	m_Button[BUTTON_TYPE::EXIT].Run();

	if (m_Button[BUTTON_TYPE::START].GetState() == BUTTON_STATE::POP)
	{
		m_pEPN_SI->PlayEffect("Sound/Button.mp3");
		return VIEW_STATE::MENU;
	}
	else if (m_Button[BUTTON_TYPE::EXIT].GetState() == BUTTON_STATE::POP)
	{
		m_pEPN_SI->PlayEffect("Sound/Button.mp3");
		exit(0);
	}

	m_Animation.Run();

	return VIEW_STATE::WIN;
}