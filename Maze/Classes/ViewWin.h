#ifndef __VIEW_WIN_H__
#define __VIEW_WIN_H__

#include "ViewBase.h"
#include "AnimationSet.h"
#include "Button.h"

class ViewWin : public ViewBase
{
	enum BUTTON_TYPE
	{
		NONE = -1,
		START,
		EXIT,
		MAX
	};

public:
	ViewWin();
	~ViewWin();
	
private:
	AnimationSet m_Animation;
	Button m_Button[BUTTON_TYPE::MAX];

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_WIN_H__