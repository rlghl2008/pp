#ifndef __VIEW_MANAGER_H__
#define __VIEW_MANAGER_H__

#include "ViewLogo.h"
#include "ViewStory.h"
#include "ViewInGame.h"
#include "ViewMenu.h"
#include "ViewGuide.h"
#include "ViewLose.h"
#include "ViewWin.h"

class ViewManager
{
public:
	ViewManager();
	~ViewManager();

private:
	ViewBase * m_pView;
	VIEW_STATE m_ViewState;

public:
	bool ChangeView(VIEW_STATE View);
	bool Run();
};

#endif // !__VIEW_MANAGER_H__