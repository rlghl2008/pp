#ifndef __MONSTER_BASE_H__
#define __MONSTER_BASE_H__

#include "ObjectManager.h"
#include "PathFinder.h"

enum class MONSTER_TYPE	//몬스터 타입
{
	NONE = -1,
	CLOSE,				//근접
	ARCHER,				//원거리
	SPECIAL,			//특수능력
	MAX
};

enum ANIMATION_TYPE				//애니매이션 타입
{
	ANIMATION_TYPE_NONE = -1,
	WALK_UP,
	WALK_DOWN,
	SIDE_WALK,
	IDLE,
	ANIMATION_TYPE_MAX
};

class MonsterBase : public ObjectBase
{	

public:
	MonsterBase();
	MonsterBase(MONSTER_TYPE MonsterType, EPN_Pos Pos);
	~MonsterBase();

protected:
	Character * m_pCharacter;
	ObjectManager* m_pObjectManager;

protected:
	vector<EPN_Pos> m_PathQueue;
	PathFinder m_PathFinder;

protected:
	estring m_TextureName;		//몬스터 텍스쳐 이름
	MONSTER_TYPE m_MonsterType;	//몬스터 타입
	
	bool m_DirChangeFlag;		//방향 바꾸는 플래그
	int m_DirChangeCnt;			//방향 바꾸는 주기 카운트
	int m_DirChangeDelay;		//방향 바꾸는 주기

	EPN_Pos m_ToChaPos;			//캐릭터 까지의 정규화된 벡터값
	bool m_MoveXFlag;			//true면 X축으로만 이동, false면 Y축으로만 이동
	bool m_ColisionFlag;		//몬스터가 충돌하고있다는 플래그

	bool m_ReversalFlag;		//반전 플래그
	bool m_AnimChangeFlag;		//애니매이션 체인지 플래그
	float m_Alpha;				//알파값

	int m_FreezeTime;			//얼음 속성 총알 맞았을때 느려지는 시간
	int m_FreezeTimeCnt;		//시간 카운트
	bool m_FreezeFlag;			//얼음상태 플래그
	float m_OriginSpeed;		//원래 스피드

	int m_BurnTime;				//불속성 총알 맞았을때 느려지는 시간
	int m_BurnTimeCnt;			//시간 카운트
	bool m_BurnFlag;			//화상상태 카운트

	bool m_IsArcherShooting;	//원거리 몹이 총알을 발사하고있는가
	bool m_IsPathFinished;		//현재 내 길찾기를 완료했는가

	int m_HealDelayTime;		//특수몹 힐 딜레이
	int m_HeadDelayCount;		//특수몹 힐 딜레이 카운트
	bool m_IsHealed;				//힐 하고있는가
		
	int m_PathIndex;			//길찾기 인덱스
	int m_PathFindDelay;
	int m_PathFindDelayCount;

protected:
	void FindPath();
	void Move();
	EPN_Pos GetDirection(EPN_Pos Start, EPN_Pos Dest);
	void SetAniDirection(EPN_Pos Dir);

public:
	void OnFreezeState();			//빙결 상태
	void OnBurnState();				//화상 상태
	void SetHealState(bool State);	
	bool GetHealState();

public:
	virtual void IsSensed(ObjectBase* pObject);
	virtual void IsCollided(ObjectBase* pObject);
	virtual bool Run();
};

#endif // !__MONSTER_BASE_H__