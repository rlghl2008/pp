#ifndef __VIEW_LOGO_H__
#define __VIEW_LOGO_H__

#include "ViewBase.h"


class ViewLogo : public ViewBase
{

public:
	ViewLogo();
	~ViewLogo();

private:

	int m_FadeChangeCnt;	//페이드인아웃 카운트
	bool m_FadeChangeFlag;	//페이드, 아웃 플래그
	float m_Alpha;			//ARGB Alpha

private:
	void FadeInOut();

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_LOGO_H__