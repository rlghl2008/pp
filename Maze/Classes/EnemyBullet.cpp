#include "EnemyBullet.h"

EnemyBullet::EnemyBullet(ObjectBase *pOwner, int Angle, ENEMYBULLET_TYPE Type) :
	m_AniSetFlag(true),
	m_Angle(Angle),
	m_FakeTimeCnt(0),
	m_Type(Type),
	m_BulletDieCnt(0),
	m_BulletDieTime(60)
{
	estring TextureName;
	EPN_Pos BulletSize;
	switch (m_Type)
	{
	case ENEMYBULLET_TYPE::FAKEBULLET:
	case ENEMYBULLET_TYPE::BOSS:
		TextureName = "BulletFireBomb";
		BulletSize = EPN_Pos(64, 64);
		m_ObjectInfo.CollisionRect = EPN_Rect(0, 0, 64, 64);
		m_ObjectInfo.Atk = 20.f;
		m_ObjectInfo.Speed = 7;
		m_ObjectInfo.Pos = EPN_Pos(pOwner->GetObjectInfo()->Pos.X - 16, pOwner->GetObjectInfo()->Pos.Y + 7);
		break;
	case ENEMYBULLET_TYPE::ARCHER:
		TextureName = "BulletFireBomb";
		BulletSize = EPN_Pos(64, 64);
		m_ObjectInfo.CollisionRect = EPN_Rect(0, 0, 32, 32);
		m_ObjectInfo.Scaling = EPN_Scaling(0.5f, 0.5f);
		m_ObjectInfo.Atk = 10.f;
		m_ObjectInfo.Speed = 2;
		m_ObjectInfo.Pos = EPN_Pos(pOwner->GetObjectInfo()->Pos.X, pOwner->GetObjectInfo()->Pos.Y);
		break;

	}

	m_ObjectInfo.ObjectType = OBJECT_TYPE::BULLET;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::ENEMY;

	m_Animation.AddAnimationData(new AnimationInfo(TextureName, BulletSize, 1, false, true, true, 0, 4));
	m_Animation.AddAnimationData(new AnimationInfo(TextureName, BulletSize, 1, false, false, false, 0, 15));
	m_Animation.SetCurrentAni(0);

	//총알 각도
	m_NomalizedPos = EPN_GetAnglePos(m_Angle);
}

EnemyBullet::~EnemyBullet()
{
}

void EnemyBullet::IsCollided(ObjectBase * pObject)
{
}

void EnemyBullet::IsSensed(ObjectBase * pObject)
{
}

bool EnemyBullet::Run()
{
	if (!m_ObjectInfo.IsEnable)
	{
		if (m_AniSetFlag)
		{
			m_Animation.SetCurrentAni(1);
			m_AniSetFlag = false;
		}
		//일정 시간이 지나면 총알 삭제
		if (++m_BulletDieCnt >= m_BulletDieTime)
		{
			m_BulletDieCnt = 0;
			m_ObjectInfo.IsDied = true;
		}
	}
	else
	{
		if (m_Type == ENEMYBULLET_TYPE::FAKEBULLET)
		{
			m_FakeTimeCnt++;
			if (m_FakeTimeCnt >= 25 && m_FakeTimeCnt < 100)
			{
				m_ObjectInfo.Speed = 0.f;
			}
			else if (m_FakeTimeCnt == 100)
			{
				m_ObjectInfo.Speed = 7.f;
				EPN_Pos CharacterPos = ObjectManager::CreateInstance()->GetCharacter()->GetObjectInfo()->Pos;
				int TargetAngle = EPN_GetPosAngle(m_ObjectInfo.Pos, CharacterPos);
				m_NomalizedPos = EPN_GetAnglePos(TargetAngle);
			}
		}
		m_ObjectInfo.Pos += m_NomalizedPos * m_ObjectInfo.Speed;

		ObjectBase::Run();
	}

	m_Animation.Run();
	m_pEPN_TI->Blt(m_Animation.GetCurAniTexture(), 0, m_ObjectInfo.Pos, m_Animation.GetCurAniActionRect(), EPN_ARGB(255, 255, 255, 255), m_Angle, m_ObjectInfo.Scaling);

	return true;
}