#ifndef __VIEW_LOSE_H__
#define __VIEW_LOSE_H__

#include "ViewBase.h"
#include "Button.h"

class ViewLose : public ViewBase
{
	enum BUTTON_TYPE
	{
		NONE = -1,
		START,
		EXIT,
		MAX
	};
public:
	ViewLose();
	~ViewLose();

private:
	Button m_Button[BUTTON_TYPE::MAX];

public:
	virtual VIEW_STATE Run();
};

#endif // !__VIEW_LOSE_H__

