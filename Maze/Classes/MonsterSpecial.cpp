#include "MonsterSpecial.h"



MonsterSpecial::MonsterSpecial()
{
}

MonsterSpecial::MonsterSpecial(EPN_Pos Pos) :
	m_HealValue(0.1f),
	m_HealFlag(false)
{
	m_MonsterType = MONSTER_TYPE::SPECIAL;

	m_TextureName = "MonsterSpecial";
	m_ObjectInfo.Atk = 20;
	m_ObjectInfo.Hp = 50;
	m_ObjectInfo.Speed = 1.0f;
	m_ObjectInfo.Rect = EPN_Rect(0, 0, 32, 48);
	m_ObjectInfo.FootRect = EPN_Rect(0, 0, 30, 30);
	m_ObjectInfo.CollisionRect = EPN_Rect(5, 5, 27, 43);
	m_ObjectInfo.SensorRect = EPN_Rect(-75, -75, 75, 75);
	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.MaxHp = m_ObjectInfo.Hp;
	m_OriginSpeed = m_ObjectInfo.Speed;

	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 12, 15));	//WalkUp
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 0, 3));	//WalkDown
	m_Animation.AddAnimationData(new AnimationInfo(m_TextureName, EPN_Pos(32, 48), 5, false, true, false, 4, 7));	//SideWalk
	m_Animation.SetCurrentAni(ANIMATION_TYPE::WALK_DOWN);

	m_HealEffect.AddAnimationData(new AnimationInfo("MonsterSpecialEffect", EPN_Pos(300, 300), 10, true, true, false, 0, 3));
	m_HealEffect.SetCurrentAni(0);
	MonsterBase::MonsterBase();
}


MonsterSpecial::~MonsterSpecial()
{
}

void MonsterSpecial::IsSensed(ObjectBase * pObject)
{
	//감지된 타입이 몬스터일 경우
	if (pObject->GetObjectInfo()->ObjectType == OBJECT_TYPE::MONSTER)
	{
		//힐을 받을 수 있다면
		MonsterBase* SensedMonster = (MonsterBase*)pObject;
		if (SensedMonster->GetHealState() == false)
		{
			ObjectInfo* SensedObjInfo = SensedMonster->GetObjectInfo();
			SensedObjInfo->Hp += m_HealValue;
			if (SensedObjInfo->Hp > SensedObjInfo->MaxHp)
				SensedObjInfo->Hp = SensedObjInfo->MaxHp;
			SensedMonster->SetHealState(true);
		}
	}
}

void MonsterSpecial::IsCollided(ObjectBase * pObject)
{
	MonsterBase::IsCollided(pObject);
}

bool MonsterSpecial::Run()
{
	MonsterBase::Run();

	m_HealEffect.Run();
	m_pEPN_TI->Blt(m_HealEffect.GetCurAniTexture(), 0, m_ObjectInfo.Pos + EPN_Pos(-55, 55), m_HealEffect.GetCurAniActionRect(), EPN_ARGB(150, 255, 255, 255), 0, EPN_Scaling(0.5f, 0.5f));

	return true;
}
