#include "Item.h"
#include <cstdlib>
#include <ctime>

Item::Item(EPN_Pos Pos, ITEM_TYPE ItemType)
{
	m_ObjectInfo.ObjectType = OBJECT_TYPE::ITEM;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::NEUTRAL;

	switch (ItemType)
	{
	case ITEM_TYPE::RECOVERY:
		m_ItemType = ITEM_TYPE::RECOVERY;
		m_TextureName = "ItemRecovery";
		break;
	case ITEM_TYPE::POWER_UP:
		m_ItemType = ITEM_TYPE::POWER_UP;
		m_TextureName = "ItemPowerUp";
		break;
	case ITEM_TYPE::SPEED_UP:
		m_ItemType = ITEM_TYPE::SPEED_UP;
		m_TextureName = "ItemSpeedUp";
		break;
	case ITEM_TYPE::FIRE:
		m_ItemType = ITEM_TYPE::FIRE;
		m_TextureName = "ItemFire";
		break;
	case ITEM_TYPE::ICE:
		m_ItemType = ITEM_TYPE::ICE;
		m_TextureName = "ItemIce";
		break;
	case ITEM_TYPE::THUNDER:
		m_ItemType = ITEM_TYPE::THUNDER;
		m_TextureName = "ItemThunder";
		break;
	}

	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.Rect = m_pEPN_TI->GetTextureSize(m_TextureName);
	m_ObjectInfo.CollisionRect = m_ObjectInfo.Rect;
}

Item::Item(EPN_Pos Pos)
{
	m_ObjectInfo.ObjectType = OBJECT_TYPE::ITEM;
	m_ObjectInfo.TeamFlag = TEAM_FLAG::NEUTRAL;
	
	/* 아이템 랜덤 설정 */
	int ItemDecide = rand() % 600;
	if (ItemDecide >= 500)
	{
		m_ItemType = ITEM_TYPE::RECOVERY;
		m_TextureName = "ItemRecovery";
	}
	else if (ItemDecide >= 400)
	{
		m_ItemType = ITEM_TYPE::POWER_UP;
		m_TextureName = "ItemPowerUp";
	}
	else if (ItemDecide >= 300)
	{
		m_ItemType = ITEM_TYPE::SPEED_UP;
		m_TextureName = "ItemSpeedUp";
	}
	else if (ItemDecide >= 200)
	{
		m_ItemType = ITEM_TYPE::FIRE;
		m_TextureName = "ItemFire";
	}
	else if (ItemDecide >= 100)
	{
		m_ItemType = ITEM_TYPE::ICE;
		m_TextureName = "ItemIce";
	}
	else
	{
		m_ItemType = ITEM_TYPE::THUNDER;
		m_TextureName = "ItemThunder";
	}
	m_ObjectInfo.Pos = Pos;
	m_ObjectInfo.Rect = m_pEPN_TI->GetTextureSize(m_TextureName);
	m_ObjectInfo.CollisionRect = m_ObjectInfo.Rect;
}

Item::~Item()
{
}

ITEM_TYPE Item::GetItemType()
{
	return m_ItemType;
}

void Item::IsCollided(ObjectBase * pObject)
{
	/* 충돌한 객체가 캐릭터 일때만 처리 */
	if (pObject->GetObjectInfo()->ObjectType == OBJECT_TYPE::CHRACTER)
	{
		Character * m_pCharacter = (Character*)pObject;
		switch (m_ItemType)
		{
		case ITEM_TYPE::RECOVERY:
			m_pCharacter->GetObjectInfo()->Hp += 1;
			break;
		case ITEM_TYPE::POWER_UP:
			if (m_pCharacter->GetObjectInfo()->Atk <= 10)
				m_pCharacter->GetObjectInfo()->Atk += 1;
			break;
		case ITEM_TYPE::SPEED_UP:
			if (m_pCharacter->GetObjectInfo()->Speed <= 5)
				m_pCharacter->GetObjectInfo()->Speed += 0.25f;
			break;
		case ITEM_TYPE::FIRE:
			m_pCharacter->SetCharacterType(CHARACTER_TYPE::FIRE);
			break;
		case ITEM_TYPE::ICE:
			m_pCharacter->SetCharacterType(CHARACTER_TYPE::ICE);
			break;
		case ITEM_TYPE::THUNDER:
			m_pCharacter->SetCharacterType(CHARACTER_TYPE::THUNDER);
			break;
		}
	}
}

void Item::IsSensed(ObjectBase * pObject)
{
}

bool Item::Run()
{
	m_pEPN_TI->Blt(m_TextureName, 0, m_ObjectInfo.Pos, m_ObjectInfo.Rect);
	return true;
}